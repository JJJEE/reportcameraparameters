#include <Http/HttpServer.h>	
#include <Http/HttpStream.h>
#include <Utils/StrUtils.h>
#include <Utils/DBConnection.hh>
#include <XmlParser.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <unistd.h>
#include <stdio.h>
#include <iostream>     // std::cout
#include <algorithm>    // std::transform
#include <stdio.h>
#include <string.h>

//Fichero de configuracion
string confFile  = "CameraParametersBBDDConfig.xml";

const char* readFile(const char* name); 
void processRequest(HttpStream *stream, void *params = NULL );
int canis_inicializado=0;
Canis* myCanis 		= 0;
DBConnection* myDB 	= 0;
int n_peticiones = 0;
int limitepeticiones = 0;

int main(int argc, char **argv)
{
	// Leemos configuracion
	XmlParser* myXML;

	int port	= 0;
	string xmlConf 	= readFile(confFile.c_str());


	myXML=new XmlParser(xmlConf.c_str(), xmlConf.size());
	myXML->addTag("<Service>");
	myXML->addTag("<IOPort>");
	myXML->read();
	string strPort 	= myXML->getTag(0);
	myXML->cleanTag();

	port 	= atoi(strPort.c_str());
	printf("CameraParametersService IOPort: %d\n", port );
	
	myXML->addTag("<Service>");
	myXML->addTag("<IP>");
	myXML->read();
	string ip 	= myXML->getTag(0);
	myXML->cleanTag();


	myXML->addTag("<Service>");
	myXML->addTag("<MaxPeticiones>");
	myXML->read();
	string maxpeticiones 	= myXML->getTag(0);
	myXML->cleanTag();

	if (atoi(maxpeticiones.c_str()) > 0 )
		limitepeticiones 	= atoi(maxpeticiones.c_str());
	else 
		limitepeticiones 	= 0;
	printf("Limite peticiones: %d\n", limitepeticiones );

	delete myXML;

	printf("Server Ip: %s\n", ip.c_str() );



	myCanis = new Canis(confFile.c_str(),false);
	if (myCanis == 0)
		printf("Canis no inizializada");
	myDB 	= new DBConnection(myCanis->getAddress(), myCanis->getType(), myCanis);


	

	// Iniciamos el servicio
	HttpServer*	httpServ;
	IP modIp( ip.c_str() );
	Address servAddr(modIp, port);
	
	
	
	
	httpServ	= new HttpServer( servAddr );
	
	
	
	httpServ->serve(processRequest);


	if (limitepeticiones == 0)  {
		while (1)
			usleep(1000000);
	}
	else  {			
		while (limitepeticiones > n_peticiones) 
			usleep(1000000);
		httpServ->stopServing();
		delete httpServ;
		delete myDB;
		delete myCanis;
	}

}

void processRequest(HttpStream *stream, void *params )
{

	printf( "processRequest -----> \n" );
	string response;
	int len				= 0;
	map <string, string> req	= stream->getRequestData();
	
	// Transformamos todos los caracteres a minusculas para evitar problemas
	map<string,string>::const_iterator it	= req.begin();
	for (; it	!= req.end(); it++)
	{
		string k	= it->first;
		transform(k.begin(), k.end(), k.begin(), ::tolower);
		//cout << "req[" << k << "] = " << it->second << endl;
		req[k] = it->second;
	}
	
	// Parseamos la id de la camara
	string camId 	= req["requeststring"];
	
	camId.erase(0,1); //Borramos la '/'

	int found 	= camId.find(".");
	if( found != string::npos )
	{
		camId.erase(camId.begin()+found,camId.end()); 
	}
	
	
	
	char buffer1[256];
	string dbResponse;
	string dbResponse_1;
	string dbResponse_2;
	string dbResponse_3;

	int count_1 = 0;
	int count_2 = 0;
	int count_3 = 0;
	int count_4 = 0;
	int count_5 = 0;
	int count_6 = 0;
	int count_7 = 0;
	int count_8 = 0;
	int camera_Id_count;
	string content_parameters;
	XmlParser* myXML;



	camera_Id_count =atoi(camId.c_str());
	
	int indice_i;
	int peticion_especial = 0;
	int peticion_especial_completa = 0; 
	int todos_los_datos = 0;
	int mezcla_letras_numeros = 0;
	for (indice_i = 0 ; indice_i < camId.length() ; indice_i++)
		if (!isdigit(camId[indice_i]))  {
			if ((indice_i == 0) && (camId[indice_i] == '*')  && ( camId.length() == 1))  {
				//cout << "length: " << camId.length() << endl;
				todos_los_datos = 1;
			}
			if ((camId[indice_i] == '&') && (indice_i == (camId.length() -1)) && (mezcla_letras_numeros == 0)) {
				peticion_especial = 1;
				found 	= camId.find("&");
				if( found != string::npos ) {
					//cout << "& en posicion final encontrado 1: " << camId.c_str() << endl;
					camId.erase(camId.begin()+found,camId.end()); 
					//cout << "& en posicion final encontrado 2: " << camId.c_str() << endl;
				}
				else
					peticion_especial = 0;
			}
			if ((camId[indice_i] == '?') && (indice_i == (camId.length() -1)) && (mezcla_letras_numeros == 0)) {
				peticion_especial_completa = 1;
				found 	= camId.find("?");
				if( found != string::npos ) {
					//cout << "? en posicion final encontrado 1: " << camId.c_str() << endl;
					camId.erase(camId.begin()+found,camId.end()); 
					//cout << "? en posicion final encontrado 2: " << camId.c_str() << endl;
				}
				else
					peticion_especial_completa = 0;
			}
			mezcla_letras_numeros = 1;
			
		}


	//Valor de ID incongruente(caracteres no numéricos), 0 o mayor que 9999, o más de 5 caracteres
	//if ((0 >= camera_Id_count) || (camera_Id_count>= 10000) || (mezcla_letras_numeros && (peticion_especial == 0)  && (peticion_especial_completa == 0)&& (todos_los_datos == 0)) || (camId.length() >= 5)) {
	if (((0 >= camera_Id_count) && (todos_los_datos == 0)) || (camera_Id_count>= 10000) || (mezcla_letras_numeros && (peticion_especial == 0)  && (peticion_especial_completa == 0)&& (todos_los_datos == 0)) || (camId.length() >= 5)) {
		content_parameters="<error>2</error> <description>id indescifrable , 0 , negativo o mayor de 10000</description>";
		//content_parameters.append("<id>");
		//if (camId.length() !=0)
		//	content_parameters.append( camId.c_str());
		//else 
		//	content_parameters.append( " ");
		//content_parameters.append("</id>");
		map <string, string> hdrs;
		content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>" + content_parameters;
		content_parameters.append( "</cameraparametersbbdd>");
		hdrs[string("Content-Type")]	= string("xml;");
		hdrs[string("Date")]			= " ";
		hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
		hdrs[string("Accept-Ranges")]	= "bytes";
		hdrs[string("Connection")]		= "Keep-Alive";
		hdrs[string("Expires")]			= " ";
		hdrs[string("Pragma")]			= "no-cache";
		hdrs[string("Cache-Control")]	= "no-cache";
		hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
		response	= stream->createResponseHeader(hdrs) + content_parameters;
	
		stream->sendResponse(response);

		
		delete stream;



		//if (myCanis == 0)
		//	printf("Canis borrado 1\n");
		//if (canis_inicializado != 0) 
		//	delete myCanis;
		//else
		//	canis_inicializado = 1;
		n_peticiones ++;
		printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());

		cout << "<------ processRequest()" << endl << endl;
		return;
	}






	//La primera consulta que se hace a la BD se ha de repetir dependiendo del S.O. En freeBSD no es necesario.
	//if (Request_inicializado == 0) {
	//	myDB 	= new DBConnection(myCanis->getAddress(), myCanis->getType(), myCanis);
	//	sprintf (buffer1, "SELECT ip FROM dispositivo WHERE id=%s",camId.c_str());	
	//	dbResponse 	= myDB->query(buffer1);
		
	//	Request_inicializado++;
	//}
	
	string Id_final,Codec_final,H_final,V_final,date_final;
	const char *prueba_char;
	vector <string> totalidad_IDS;
	int indice;

 	if (todos_los_datos == 1) {

		sprintf (buffer1, "SELECT id,codeccam,resxcam,resycam,datecam FROM video ORDER by id");	
		prueba_char = NULL;



		try {

			prueba_char 	= myDB->query(buffer1);
			dbResponse.assign(prueba_char);
			delete [] prueba_char;
		}
		catch (...)  {
			cout << "Error en el acceso a la BD 1" << endl;
			// Preparamos cabeceras XML y enviamos respuesta
			map <string, string> hdrs;
			content_parameters="<error>3</error> <description>BD de camaras desconectada</description>";

			content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>"    + content_parameters;
			content_parameters.append( "</cameraparametersbbdd>");
			hdrs[string("Content-Type")]	= string("xml;");
			hdrs[string("Date")]			= " ";
			hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
			hdrs[string("Accept-Ranges")]	= "bytes";
			hdrs[string("Connection")]		= "Keep-Alive";
			hdrs[string("Expires")]			= " ";
			hdrs[string("Pragma")]			= "no-cache";
			hdrs[string("Cache-Control")]	= "no-cache";
			hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
			response	= stream->createResponseHeader(hdrs) + content_parameters;
	
			stream->sendResponse(response);

			delete stream;
			n_peticiones ++;
			printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());
			cout << "<------ processRequest()" << endl << endl;
			return;
		}


		//cout << "Información leida: " << dbResponse.c_str() << endl ;
		myXML=new XmlParser(dbResponse.c_str(), dbResponse.size());
		myXML->addTag("<row>");
		count_1	= myXML->read();
		if (count_1) {
			for (indice=0 ; indice < count_1 ; indice++)  {
				totalidad_IDS.push_back( myXML->getTag(indice));
				if (indice == 0)
					content_parameters="<cam>";
				else
					content_parameters.append( "<cam>");
				content_parameters.append( totalidad_IDS[indice].c_str());
				content_parameters.append( "</cam>");
			}
			//cout << "count_1: " << count_1 << endl;		
		}
		myXML->cleanTag();
		delete myXML;



	
		// Preparamos cabeceras XML y enviamos respuesta
		map <string, string> hdrs;
		content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>"    + content_parameters;
		content_parameters.append( "</cameraparametersbbdd>");
		hdrs[string("Content-Type")]	= string("xml;");
		hdrs[string("Date")]			= " ";
		hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
		hdrs[string("Accept-Ranges")]	= "bytes";
		hdrs[string("Connection")]		= "Keep-Alive";
		hdrs[string("Expires")]			= " ";
		hdrs[string("Pragma")]			= "no-cache";
		hdrs[string("Cache-Control")]	= "no-cache";
		hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
		response	= stream->createResponseHeader(hdrs) + content_parameters;
	
		stream->sendResponse(response);

		delete stream;
		n_peticiones ++;
		printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());
		cout << "<------ processRequest()" << endl << endl;
		return;
	}


 	if (peticion_especial_completa == 1) {

		sprintf (buffer1, "SELECT id,codec,resx,resy,codeccam,resxcam,resycam,datecam FROM video WHERE id=%s", camId.c_str());	
		prueba_char = NULL;




		try {

			prueba_char 	= myDB->query(buffer1);
			dbResponse.assign(prueba_char);
			delete [] prueba_char;
		}
		catch (...)  {
			cout << "Error en el acceso a la BD 2" << endl;
			// Preparamos cabeceras XML y enviamos respuesta
			map <string, string> hdrs;
			content_parameters="<error>3</error> <description>BD de camaras desconectada</description>";

			content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>"    + content_parameters;
			content_parameters.append( "</cameraparametersbbdd>");
			hdrs[string("Content-Type")]	= string("xml;");
			hdrs[string("Date")]			= " ";
			hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
			hdrs[string("Accept-Ranges")]	= "bytes";
			hdrs[string("Connection")]		= "Keep-Alive";
			hdrs[string("Expires")]			= " ";
			hdrs[string("Pragma")]			= "no-cache";
			hdrs[string("Cache-Control")]	= "no-cache";
			hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
			response	= stream->createResponseHeader(hdrs) + content_parameters;
	
			stream->sendResponse(response);

			delete stream;
			n_peticiones ++;
			printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());
			cout << "<------ processRequest()" << endl << endl;
			return;
		}





		//cout << "Información leida: " << dbResponse.c_str() << endl ;
		myXML=new XmlParser(dbResponse.c_str(), dbResponse.size());
		myXML->addTag("<row>");
		count_1	= myXML->read();
		int Indice_Parametros1;
		if (count_1) {
			content_parameters=myXML->getTag(0);
			//cout << "count_1: " << count_1 << endl;


			//mp4v es sinonimo de mpeg4 y queremos que en ambos casos se muestre mpeg4
			Indice_Parametros1 = content_parameters.find("mp4v");
			if (Indice_Parametros1 != string::npos) {
				content_parameters.replace(Indice_Parametros1,4,"mpeg4");
			}
		
		}
		myXML->cleanTag();
		delete myXML;

		//XmlParser myXML(dbResponse.c_str(), dbResponse.size());
		myXML=new XmlParser(dbResponse.c_str(), dbResponse.size());


		myXML->addTag("<row>");
		myXML->addTag("<id>");
		count_1 	= myXML->read();
		myXML->cleanTag();

		myXML->addTag("<row>");
		myXML->addTag("<codec>");
		count_2 = myXML->read();
		myXML->cleanTag();

	
	
		myXML->addTag("<row>");
		myXML->addTag("<resx>");
		count_3 = myXML->read();
		myXML->cleanTag();
	
	
		myXML->addTag("<row>");
		myXML->addTag("<resy>");
		count_4 = myXML->read();
		myXML->cleanTag();


		myXML->addTag("<row>");
		myXML->addTag("<codeccam>");
		count_5 = myXML->read();
		myXML->cleanTag();

		myXML->addTag("<row>");
		myXML->addTag("<resycam>");
		count_6 = myXML->read();
		myXML->cleanTag();

		myXML->addTag("<row>");
		myXML->addTag("<resxcam>");
		count_7 = myXML->read();
		myXML->cleanTag();

	
		myXML->addTag("<row>");
		myXML->addTag("<datecam>");
		count_8 = myXML->read();
		myXML->cleanTag();
	
		delete myXML;







	
		// Preparamos cabeceras XML y enviamos respuesta
		map <string, string> hdrs;
		if (count_1 && count_2 && count_3 && count_4 && count_5 && count_6 && count_7 && count_8 )
			content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>"    + content_parameters;
		else if (count_1 || count_2 || count_3 || count_4 || count_5 || count_6 || count_7 || count_8)  {
			content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>";
			content_parameters.append("<error>4</error> <description>Informacion de dispositivo incompleta</description>");
			content_parameters.append("<id>");
			content_parameters.append( camId.c_str());
			content_parameters.append("</id>");
		}
		else {
			content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>";
			content_parameters.append("<error>1</error> <description>Dispositivo no disponible en la BD</description>");
			content_parameters.append("<id>");
			content_parameters.append( camId.c_str());
			content_parameters.append("</id>");
		}
		content_parameters.append( "</cameraparametersbbdd>");
		hdrs[string("Content-Type")]	= string("xml;");
		hdrs[string("Date")]			= " ";
		hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
		hdrs[string("Accept-Ranges")]	= "bytes";
		hdrs[string("Connection")]		= "Keep-Alive";
		hdrs[string("Expires")]			= " ";
		hdrs[string("Pragma")]			= "no-cache";
		hdrs[string("Cache-Control")]	= "no-cache";
		hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
		response	= stream->createResponseHeader(hdrs) + content_parameters;
	
		stream->sendResponse(response);

		delete stream;
		n_peticiones ++;
		printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());
		cout << "<------ processRequest()" << endl << endl;
		return;
	}


	if (peticion_especial == 1)  {
		//Fecha ficticia anterior a la actualidad para forzar su actualización por parte de ActualizaBDCamaras
		sprintf (buffer1, "UPDATE video SET date = \'2010-05-25 00:00:00.000\' WHERE id=%s", camId.c_str());
		//cout << "buffer1: " << buffer1 << endl;
		try {
			XML *res = myDB->update(buffer1);
		}
		catch (...)  {
				cout << "Error en el acceso a la BD 3" << endl;
				// Preparamos cabeceras XML y enviamos respuesta
				map <string, string> hdrs;
				content_parameters="<error>3</error> <description>BD de camaras desconectada</description>";

				content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>"    + content_parameters;
				content_parameters.append( "</cameraparametersbbdd>");
				hdrs[string("Content-Type")]	= string("xml;");
				hdrs[string("Date")]			= " ";
				hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
				hdrs[string("Accept-Ranges")]	= "bytes";
				hdrs[string("Connection")]		= "Keep-Alive";
				hdrs[string("Expires")]			= " ";
				hdrs[string("Pragma")]			= "no-cache";
				hdrs[string("Cache-Control")]	= "no-cache";
				hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
				response	= stream->createResponseHeader(hdrs) + content_parameters;
	
				stream->sendResponse(response);

				delete stream;
				n_peticiones ++;
				printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());
				cout << "<------ processRequest()" << endl << endl;
				return;
		}
		int cuenta = 0;
		do {
			usleep(500000);
			sprintf (buffer1, "SELECT date FROM video WHERE id=%s", camId.c_str());
			prueba_char = NULL;



			try {

				prueba_char 	= myDB->query(buffer1);
				dbResponse.assign(prueba_char);
				delete [] prueba_char;
			}
			catch (...)  {
				cout << "Error en el acceso a la BD 4" << endl;
				// Preparamos cabeceras XML y enviamos respuesta
				map <string, string> hdrs;
				content_parameters="<error>3</error> <description>BD de camaras desconectada</description>";

				content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>"    + content_parameters;
				content_parameters.append( "</cameraparametersbbdd>");
				hdrs[string("Content-Type")]	= string("xml;");
				hdrs[string("Date")]			= " ";
				hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
				hdrs[string("Accept-Ranges")]	= "bytes";
				hdrs[string("Connection")]		= "Keep-Alive";
				hdrs[string("Expires")]			= " ";
				hdrs[string("Pragma")]			= "no-cache";
				hdrs[string("Cache-Control")]	= "no-cache";
				hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
				response	= stream->createResponseHeader(hdrs) + content_parameters;
	
				stream->sendResponse(response);

				delete stream;
				n_peticiones ++;
				printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());
				cout << "<------ processRequest()" << endl << endl;
				return;
			}



			myXML=new XmlParser(dbResponse.c_str(), dbResponse.size());
			myXML->addTag("<row>");
			myXML->addTag("<date>");
			count_1 = myXML->read();
			if (count_1)  {
				date_final = myXML->getTag(0);
				//cout << "Date video : " << date_final.c_str() << endl;
		
			}
			myXML->cleanTag();
			delete myXML;
			cuenta ++;
			//cout << "Comparacion: " << date_final.compare(0,4,"2010") << endl;


		} while ((date_final.compare(0,4,"2010") == 0) && (cuenta < 100 )); //Maximo de 100 veces, 50 sg.
		//} while (cuenta < 100 ); //Maximo de 100 veces, 50 sg.
	}


	sprintf (buffer1, "SELECT codeccam,resxcam,resycam,datecam FROM video WHERE id=%s", camId.c_str());
	//dbResponse 	= myDB->query(buffer1);
	//cout << "buffer1: " << buffer1 <<endl;

	prueba_char = NULL;



	try {

			prueba_char 	= myDB->query(buffer1);
			dbResponse.assign(prueba_char);
			delete [] prueba_char;
	}
	catch (...)  {
			cout << "Error en el acceso a la BD 5" << endl;
			// Preparamos cabeceras XML y enviamos respuesta
			map <string, string> hdrs;
			content_parameters="<error>3</error> <description>BD de camaras desconectada</description>";

			content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>"    + content_parameters;
			content_parameters.append( "</cameraparametersbbdd>");
			hdrs[string("Content-Type")]	= string("xml;");
			hdrs[string("Date")]			= " ";
			hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
			hdrs[string("Accept-Ranges")]	= "bytes";
			hdrs[string("Connection")]		= "Keep-Alive";
			hdrs[string("Expires")]			= " ";
			hdrs[string("Pragma")]			= "no-cache";
			hdrs[string("Cache-Control")]	= "no-cache";
			hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
			response	= stream->createResponseHeader(hdrs) + content_parameters;
	
			stream->sendResponse(response);

			delete stream;
			n_peticiones ++;
			printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());
			cout << "<------ processRequest()" << endl << endl;
			return;
	}




	
	// Parseamos el XML de respuesta para leer codec,H,V y date


	//XmlParser myXML(dbResponse.c_str(), dbResponse.size());
	myXML=new XmlParser(dbResponse.c_str(), dbResponse.size());


	myXML->addTag("<row>");
	myXML->addTag("<codeccam>");
	count_1 	= myXML->read();
	
	if (count_1) {
		Codec_final 		= myXML->getTag(0);
		int Indice_Parametros1;

		//mp4v es sinonimo de mpeg4 y queremos que en ambos casos se muestre mpeg4
		Indice_Parametros1 = Codec_final.find("mp4v");
		if (Indice_Parametros1 != string::npos) {
				Codec_final.replace(Indice_Parametros1,4,"mpeg4");
		}
		//cout << "Codec: " << Codec_final.c_str() <<endl;		
	}
	myXML->cleanTag();
	
	
	myXML->addTag("<row>");
	myXML->addTag("<resxcam>");
	count_2 = myXML->read();
	if (count_2)  {
		H_final = myXML->getTag(0);
		//cout << "H: " << H_final.c_str() <<endl;
		
	}
	myXML->cleanTag();
	
	
	myXML->addTag("<row>");
	myXML->addTag("<resycam>");
	count_3 = myXML->read();
	if (count_3)  {
		V_final = myXML->getTag(0);
		//cout << "V: " << V_final.c_str() <<endl;
		
	}
	myXML->cleanTag();
	
	myXML->addTag("<row>");
	myXML->addTag("<datecam>");
	count_4 = myXML->read();
	if (count_4)  {
		date_final = myXML->getTag(0);
		//cout << "date: " << date_final.c_str() << endl;
		
	}
	myXML->cleanTag();
	
	

	if ( count_1 && count_2 && count_3 && count_4 ) {

		content_parameters="<id>";
		content_parameters.append( camId.c_str());
		content_parameters.append( "</id>");
		content_parameters.append("<codeccam>");
		content_parameters.append( Codec_final.c_str());
		content_parameters.append("</codeccam>");
		content_parameters.append("<resxcam>");
		content_parameters.append(H_final.c_str());
		content_parameters.append("</resxcam>");
		content_parameters.append("<resycam>");
		content_parameters.append(V_final.c_str());
		content_parameters.append("</resycam>");
		content_parameters.append("<datecam>");
		content_parameters.append(date_final.c_str());
		content_parameters.append("</datecam>");
	}
	else if (count_1 || count_2 || count_3 || count_4 )  {
		
		content_parameters="<error>4</error> <description>Informacion de dispositivo incompleta</description>";
		content_parameters.append("<id>");
		content_parameters.append( camId.c_str());
		content_parameters.append("</id>");
	}
	else   {
		
		content_parameters="<error>1</error> <description>Dispositivo no disponible en la BD</description>";
		content_parameters.append("<id>");
		content_parameters.append( camId.c_str());
		content_parameters.append("</id>");
	}
	
	delete myXML;
		
	// Preparamos cabeceras XML y enviamos respuesta
	map <string, string> hdrs;
	content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\'no\'\?><cameraparametersbbdd>"    + content_parameters;
	content_parameters.append( "</cameraparametersbbdd>");
	hdrs[string("Content-Type")]	= string("xml;");
	hdrs[string("Date")]			= " ";
	hdrs[string("Server")]			= "Marina Eye-Cam Technologies HttpServer 1";
	hdrs[string("Accept-Ranges")]	= "bytes";
	hdrs[string("Connection")]		= "Keep-Alive";
	hdrs[string("Expires")]			= " ";
	hdrs[string("Pragma")]			= "no-cache";
	hdrs[string("Cache-Control")]	= "no-cache";
	hdrs[string("Content-Length")]	= StrUtils::decToString(content_parameters.length());
	response	= stream->createResponseHeader(hdrs) + content_parameters;
	
	stream->sendResponse(response);

	//delete myDB;
	delete stream;




	//if (myCanis == 0)
	//	printf("Canis borrado 2\n");
	//if (canis_inicializado != 0)
	//	delete myCanis;
	//else
	//	canis_inicializado = 1;
	n_peticiones ++;
	printf( "Petición %d de conexión con ID  --->%s \n" , n_peticiones, camId.c_str());

	cout << "<------ processRequest()" << endl << endl;
	
	
}


const char* readFile(const char* name)
{
	string strFile, line;
	string newline = "\n";
	fstream thisFile;
	thisFile.open(name, ios::in); 	
	if(thisFile.is_open() )
	{
		while( getline(thisFile, line) )
		{
			strFile += line + newline;
		}
	}
	else
	{
		printf("Error abriendo fichero de configuración %s\n", name);
		return 0;
	}
	thisFile.close();

	return strFile.c_str();
}
