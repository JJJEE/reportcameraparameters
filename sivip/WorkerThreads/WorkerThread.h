#ifndef __SIRIUS__BASE__WORKERTHREADS_WORKERTHREAD_H
#define __SIRIUS__BASE__WORKERTHREADS_WORKERTHREAD_H

#include <Threads/Thread.h>
#include <Threads/RWlock.h>
#include <WorkerThreads/WorkUnitQueue.h>
#include <Utils/debugNew.h>

class WorkerThread : public Thread
{
public:
	static int workerThreadCount;
protected:
	static int workingCount;
	static RWLock workingCountLock;

	WorkUnitQueue *wuQueue;
	bool running, working;
	int runningId;	

	bool cancelled;
public:
	static void incWorkingCount();
	static void decWorkingCount();
	static int getWorkingCount();

	WorkerThread();
	WorkerThread(WorkUnitQueue *wuQueue);
	~WorkerThread();
	
	void setWorkQueue(WorkUnitQueue *wuQueue);
	void startRunning();
	//envia el signal cap al thread
	void cancelCurrentWorkUnit();
	//accés a la variable
	void setCancelled(bool cancelled);
	bool isCancelled();
	
	pthread_t getRunningId();
	void stopRunning();
	
	virtual void* execute(int id, void* params);
};

#endif

