#ifndef __SIRIUS__BASE__WORKERTHREADS_WORKUNIT_H
#define __SIRIUS__BASE__WORKERTHREADS_WORKUNIT_H

#include <Sockets/Address.h>
#include <Utils/Timer.h>
#include <Utils/debugNew.h>
//#include <WorkerThreads/WorkerThread.h>

class WorkerThread;
class WorkUnit
{
protected:
	void *workUnitData;
	Address responseAddress;
	Timer creationTimer;
	WorkerThread *assignedThread;
public:
	WorkUnit(void *data, Address responseAddress);
	~WorkUnit();
	void *getData();
	Address getResponseAddress();
	Address *getResponseAddressPtr();
	Timer getTimer();
	long getAgeInMillis();
	long getNanoTime();
	WorkerThread* getAssignedThread();
	void setAssignedThread(WorkerThread* wt);
};

#endif

