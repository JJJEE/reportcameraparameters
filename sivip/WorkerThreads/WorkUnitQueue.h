#ifndef __SIRIUS__BASE__WORKERTHREADS_WORKUNITQUEUE_H
#define __SIRIUS__BASE__WORKERTHREADS_WORKUNITQUEUE_H

#include <util/Queue/Queue.h>
#include <WorkerThreads/WorkUnit.h>

class WorkUnitQueue : public Queue<WorkUnit*>
{
public:
	WorkUnitQueue();
	~WorkUnitQueue();
};

#endif

