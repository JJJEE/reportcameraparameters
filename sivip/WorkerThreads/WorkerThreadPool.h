#ifndef __SIRIUS__BASE__WORKERTHREADS_WORKERTHREADPOOL_H
#define __SIRIUS__BASE__WORKERTHREADS_WORKERTHREADPOOL_H

#include <WorkerThreads/WorkUnitQueue.h>
#include <Utils/debugNew.h>

template <class WT> class WorkerThreadPool
{
protected:
	WorkUnitQueue *wuQueue;
	int nThreads;
	WT** threads;

	static map<string, int> lineIdx;
	static map<string, WT*> thrInfo;
	static int *line;
public:
	static int* getMyDebugPtr();
	string getThreadStates();

	static bool isCancelled();
	static void clearCancelled();

	WorkerThreadPool(int nThreads);
	~WorkerThreadPool();
	
	void startWorkers();
	void queueWorkUnit(WorkUnit *wu);
	int getQueueLength();
};


#endif

