#include <WorkerThreads/WorkUnit.h>
#include <Utils/debugStackTrace.h>

WorkUnit::WorkUnit(void *data, Address responseAddress)
{
	STACKTRACE_INSTRUMENT();
	
	this->workUnitData=data;
	this->responseAddress=responseAddress;
	this->creationTimer.start();
	this->assignedThread=NULL;
}

WorkUnit::~WorkUnit()
{
	STACKTRACE_INSTRUMENT();

	// No cal fer res. El delete de la workUnitData queda a discrecio de qui
	// conegui el tipus
}

void *WorkUnit::getData()
{
	STACKTRACE_INSTRUMENT();

	return this->workUnitData;
}

Address WorkUnit::getResponseAddress()
{
	STACKTRACE_INSTRUMENT();

	return this->responseAddress;
}

Address* WorkUnit::getResponseAddressPtr()
{
	STACKTRACE_INSTRUMENT();

	return &this->responseAddress;
}

Timer WorkUnit::getTimer()
{
	STACKTRACE_INSTRUMENT();

	return this->creationTimer;
}

long WorkUnit::getAgeInMillis()
{
	STACKTRACE_INSTRUMENT();

	return (long)this->creationTimer.time().useconds()/1000L;
}

long WorkUnit::getNanoTime()
{
	STACKTRACE_INSTRUMENT();

	return (long)(this->creationTimer.getStartSeconds()*1000000000.0);
}

WorkerThread* WorkUnit::getAssignedThread()
{
	return this->assignedThread;
}

void WorkUnit::setAssignedThread(WorkerThread* wt)
{
	this->assignedThread = wt;
}
