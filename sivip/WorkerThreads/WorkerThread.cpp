#include <WorkerThreads/WorkerThread.h>
#include <WorkerThreads/WorkUnitQueue.h>
#include <Utils/RPCPacket.h>
#include <Utils/debugStackTrace.h>
#include <Threads/pthread_ops.h>
#include <signal.h>

int WorkerThread::workerThreadCount=0;
int WorkerThread::workingCount=0;
RWLock WorkerThread::workingCountLock;

void WorkerThread::incWorkingCount()
{
	WorkerThread::workingCountLock.wlock();
	WorkerThread::workingCount++;
	WorkerThread::workingCountLock.unlock();
}

void WorkerThread::decWorkingCount()
{
	WorkerThread::workingCountLock.wlock();
	WorkerThread::workingCount--;
	WorkerThread::workingCountLock.unlock();
}

int WorkerThread::getWorkingCount()
{
	WorkerThread::workingCountLock.rlock();
	int wc=WorkerThread::workingCount;
	WorkerThread::workingCountLock.unlock();
	
	return wc;
}

WorkerThread::WorkerThread() : wuQueue(NULL), running(false), cancelled(false)
{
	STACKTRACE_INSTRUMENT();

}

WorkerThread::WorkerThread(WorkUnitQueue *wuQueue) : wuQueue(wuQueue), running(false), cancelled(false)
{
	STACKTRACE_INSTRUMENT();

}

WorkerThread::~WorkerThread()
{
	STACKTRACE_INSTRUMENT();

	if (this->wuQueue!=NULL)
	{
		delete this->wuQueue;
		this->wuQueue=NULL;
	}
}

void WorkerThread::setWorkQueue(WorkUnitQueue *wuQueue)
{
	STACKTRACE_INSTRUMENT();

	if (this->wuQueue!=NULL)
	{
		delete this->wuQueue;
		this->wuQueue=NULL;
	}
	this->wuQueue=wuQueue;
}

void WorkerThread::startRunning()
{
	STACKTRACE_INSTRUMENT();

	this->running=true;
	runningId=this->start();
}

void WorkerThread::cancelCurrentWorkUnit()
{
	this->setCancelled(true);
	if(!this->working)
		cout<<" WARNING: signaling "<<toString(this->getRunningId())<<", but no active WorkUnit"<<endl;
	else
		Thread::signal(runningId, SIGALRM);	
	//thrInfo.find(toString(this->getRunningId()))->second->setCancelled(true);
}

void WorkerThread::setCancelled(bool cancelled)
{
	this->cancelled=cancelled;	
}

bool WorkerThread::isCancelled()
{
	return this->cancelled;	
}

void WorkerThread::stopRunning()
{
	STACKTRACE_INSTRUMENT();

	this->running=false;
}

pthread_t WorkerThread::getRunningId()
{
	return this->getId(runningId);
}

void* WorkerThread::execute(int id, void* params)
{
	STACKTRACE_INSTRUMENT();

	while (this->running && this->wuQueue!=NULL)
	{
		WorkUnit *wu= this->wuQueue->get();
		if (wu==NULL)
			continue;
		RPCPacket *p=(RPCPacket*)wu->getData();
		if (p!=NULL) delete p;
		delete wu;
	}

	return NULL;
}

