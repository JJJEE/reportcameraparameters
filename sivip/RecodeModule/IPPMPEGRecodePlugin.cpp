#include <Plugins/GestorRecodeExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <IPPMPEGRecodePlugin.h>
#include <RecodeModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Timer.h>
#include <iostream>
#include <include/umc_mpeg4_video_decoder.h>
#include <umc/include/umc_video_decoder.h>
#ifndef WIN32
#ifndef WIN32
#include <unistd.h>
#endif
#else
#include <Utils/WindowsDefs.h>
#endif
using namespace std;

/*
IPPMPEGRecodePlugin::Sessio::Sessio()
{
	init();
}
*/

bool UMCError(int err)
{
	switch(err)
	{

		case UMC::UMC_OK:
			return false;
		case UMC::UMC_ERR_INIT: 
			cout<<" err:"<<err<<" UMC_ERR_INIT Initialization failed.  "<<endl;
			break;
		case UMC::UMC_ERR_INVALID_PARAMS: 
			cout<<" err:"<<err<<" UMC_ERR_INVALID_PARAMS Some of the critical parameters have improper value or  "<<endl;
			break;
		case UMC::UMC_ERR_ALLOC: 
			cout<<" err:"<<err<<" UMC_ERR_ALLOC Method is unable to allocate memory required for  "<<endl;
			break;
		case UMC::UMC_ERR_INVALID_STREAM: 
			cout<<" err:"<<err<<" UMC_ERR_INVALID_STREAM Incorrect input data format.  "<<endl;
			cout<<" err:"<<err<<" UMC_ERR_INVALID_STREAM Beginning of the stream used to initialize a derived  "<<endl;
			break;
		case UMC::UMC_ERR_UNSUPPORTED: 
			cout<<" err:"<<err<<" UMC_ERR_UNSUPPORTED Unsupported parameter value was specified.  "<<endl;
			break;
		case UMC::UMC_ERR_NULL_PTR: 
			cout<<" err:"<<err<<" UMC_ERR_NULL_PTR Null pointer is passed to the function.  "<<endl;
			break;
		case UMC::UMC_ERR_NOT_ENOUGH_DATA: 
			cout<<" err:"<<err<<" UMC_ERR_NOT_ENOUGH_DATA Not enough input and/or output data to process data.  "<<endl;
			break;
		case UMC::UMC_ERR_NOT_INITIALIZED: 
			cout<<" err:"<<err<<" UMC_ERR_NOT_INITIALIZED Object is not yet initialized.  "<<endl;
			break;
		case UMC::UMC_ERR_SYNC: 
			cout<<" err:"<<err<<" UMC_ERR_SYNC Required syncronization code was not found.  "<<endl;
			break;
		default: 
			cout<<" err:"<<err<<" Undefined UMC ERROR!!!"<<endl;
			break;
	}
	return true;

}

IPPMPEGRecodePlugin::Sessio::Sessio(RPCodecInfo info):info(info)
{
//	int nth=-1;
//	ippGetNumThreads(&nth);
//	cout << nth << endl;
//	ippSetNumThreads(1);
//	ippGetNumThreads(&nth);
//	cout << nth << endl;
	this->run=true;
	start();
	cout<<"IPPMPEGRecodePlugin::Sessio started"<<endl;
}

void IPPMPEGRecodePlugin::Sessio::stopThread()
{
	this->run=false;
	this->putToInBuf(NULL);
}

RPFrame *IPPMPEGRecodePlugin::Sessio::getFromInBuf()
{
	inCond.lock(); 
	while (inBuf.size()==0)
		inCond.wait();
	RPFrame *f=inBuf.front();
	inBuf.pop_front();
	inCond.unlock();
	
	return f;
}

void IPPMPEGRecodePlugin::Sessio::putToInBuf(RPFrame *f)
{
	inCond.lock();
	inBuf.push_back(f);
	inCond.signal(); 
	inCond.unlock();
}

RPFrame *IPPMPEGRecodePlugin::Sessio::getFromOutBuf()
{
	outCond.lock(); 
	while (outBuf.size()==0)
		outCond.wait();
	RPFrame *f=outBuf.front();
	outBuf.pop_front();
	outCond.unlock();
	
	return f;
}

void IPPMPEGRecodePlugin::Sessio::putToOutBuf(RPFrame *f)
{
	outCond.lock();
	outBuf.push_back(f);
	outCond.signal(); 
	outCond.unlock();
}

void* IPPMPEGRecodePlugin::Sessio::execute(int id, void* param)
{
	cout<<"----MPEGRecMod::execute start---"<<endl;
	int err;
	RPFrame *f=NULL;
	if(info.mode==RPCodecInfo::encode)
	{
		for(int i=0;this->run;i++)
		{
			f=this->getFromInBuf();
	
			if (f!=NULL)
				delete f;
		}
	}
	else
	{
		cout<<"----MPEGRecMod::execute decode"<<endl;

		while (f==NULL || f->width<=0 || f->height<=0 ||
			f->width>RPFrame::maxWidth || f->height>RPFrame::maxHeight)
		{
			if (this->run==false)
				return NULL;

			f=this->getFromInBuf();
		}

		if (this->run==false)
			return NULL;

		cout<<"MPEG4StreamThread: ini"<<endl;
		UMC::MPEG4VideoDecoder dec;
		UMC::VideoDecoderParams vcp;
		UMC::MediaData pIn;
		UMC::VideoData pOut;
		bool inited=false;

		vcp.numThreads=1;
		vcp.pPostProcessing=NULL;
		vcp.lpMemoryAllocator=NULL;
		vcp.lFlags=UMC::FLAG_VDEC_COMPATIBLE|UMC::FLAG_VDEC_REORDER;

		int width =  vcp.info.clip_info.width =  f->width;
		int height = vcp.info.clip_info.height = f->height;

//		cout<<"MPEG4StreamThread: 4 pOut.Init"<<endl;
		err=pOut.Init(width, height, UMC::RGB32); 
		UMCError(err);

//		cout<<"MPEG4StreamThread: 4 pOut Alloc"<<endl;
		err=pOut.Alloc();
		UMCError(err);

		unsigned int nframes=0;

		char *ippOutBuf=(char*)pOut.GetBufferPointer();
		int ippOutLen=pOut.GetDataSize();

		Timer t;
		t.start();
		TimerInstant ti=t.time();
		
		for(int i=0;this->run && f!=NULL;i++)
		{
			ti=t.time();
//			cout << "\tIPPMPEG4StreamThread: from last: " << ti.seconds() << " (" << ti.frames() << ")" << endl << endl;
		
			t.start();
			
			nframes=*((unsigned int*)f->data);
			byte *nextFrame=(byte*)(f->data+((nframes+1)*sizeof(int)));
//			cout<<"MPEG4StreamThread: processing frame group nframes:"<<nframes<<" ptr:"<<(void*)f.data<<" next:"<<(void*)nextFrame<<" received:"<<f.dsize<<endl;
			for(unsigned int j=1;j<=nframes;j++)
			{
				unsigned int frameSize=*((unsigned int*)(f->data+(j*sizeof(int))));
				byte *newFrame=nextFrame;
				nextFrame+=frameSize;
//				cout <<"	MPEG4StreamThread: frame:"<<j<<" size:"<<frameSize<<"  next:"<<(void*)nextFrame<<endl;


				// 20090310: Format nou de frame = AVCodec
				// int nInfos + n infos
				// cada info: int tam + data
				// la primera info ens la saltem pq es "l'extradata"
				unsigned int nInfo=*((unsigned int*)newFrame);
				newFrame+=sizeof(int);
				
				unsigned int extraSize=*((unsigned int*)newFrame);
				newFrame+=sizeof(int)+extraSize;	// Ens saltem extradata

				// I finalment apuntem a la info del frame
				frameSize=*((unsigned int*)newFrame);
				newFrame+=sizeof(int);

				if (frameSize>0)
				{
					pIn.SetBufferPointer(newFrame, frameSize);
					pIn.SetDataSize(frameSize);
					if(!inited)
					{
						if(!f->isKey)
							break;

	//					cout<<"MPEG4StreamThread: decode Init key:"<<f.isKey<<endl;
						int err=dec.Init((UMC::BaseCodecParams *)&vcp);
						if (!UMCError(err))
							inited=true;
					}
	//				else
	//					cout<<"MPEG4StreamThread: decode already Inited"<<endl;
	//				cout<<"MPEG4StreamThread: 4 GetFrame orig frame:"<<frameSize<<endl;
					err=dec.GetFrame(&pIn, &pOut);
					if (UMCError(err))
					{
						cout << "Error in decoding loop. SubFrame " << j << " of " << nframes << " (" << frameSize << " bytes) ----- breaking loop" << endl;

						if (frameSize<1024)
							cout << StrUtils::hexDump(string((char*)newFrame, frameSize));
						break;
					}
				}
			}

			// No copiem TMP pq sempre serà el mateix (les IPP van aixi! :P)
			RPFrame *res=new RPFrame(ippOutLen, f->width, f->height, ippOutBuf, true, true);
			res->bitDepth=32;//pm.pixelSize;

			this->putToOutBuf(res);
			decodedFrames++;

			delete f;
				
			ti=t.time();
			t.start();
			
//			cout << "\tIPPMPEG4StreamThread: frm decd in " << ti.seconds() << " s, " << nframes << " subfrms, " << ti.seconds()/(double)nframes << " s/subfrm (" << ti.frames() << ")" << endl;

			f=this->getFromInBuf();
		}
	}
}


void IPPMPEGRecodePlugin::Sessio::addFrame(RPFrame *f)
{
//	cout<<"MPEGRecMod::addFrame buf:"<<inBuf.size()<<" ses:"<<(void*)this<<" inBuf:"<<(void*)&inBuf<<endl;

	this->putToInBuf(f);

//	inBuf.push_back(f);
//	inCond.signal(); 
}

void IPPMPEGRecodePlugin::Sessio::addFrame(int size, int width, int height, char* data, bool isKey)
{
	addFrame(new RPFrame(size, width, height, data, isKey, true));
}

RPFrame *IPPMPEGRecodePlugin::Sessio::getFrame()
{
	return this->getFromOutBuf();
}


IPPMPEGRecodePlugin::IPPMPEGRecodePlugin(string file):PluginRecode(file), db(socket->getAddr(), type, cn)
{

	cout<<"init:"<<file<< endl;
	FILE *f=fopen(file.c_str(),"rb");

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char *buf2=new char[len];
	fread(buf2,len,1,f);
	fclose(f);

	XML *conf=xmlParser::parse(buf2);
	conf->getNodeData("[0]/ModelConfigFile");

	delete conf;
	delete[] buf2;
}

IPPMPEGRecodePlugin::~IPPMPEGRecodePlugin()
{
}

IPPMPEGRecodePlugin::Sessio* IPPMPEGRecodePlugin::getSession(Address a)
{
	map<string,Sessio*>::iterator i=sessions.find(a.toString());
	
	if (i==sessions.end())
	{
		throw (RPSessionNotStablishedException(0, string("Session no establerta")));
	}
	return i->second;
}

void IPPMPEGRecodePlugin::startSession(RPCodecInfo id, Address *a)
{	
	map<string,Sessio*>::iterator itSes=sessions.find(a->toString());
	
	cout<<endl<<"MPEGRecMod::StartSession:"<<id.name<<" mode:"<<id.mode<<" enc:"<<RPCodecInfo::encode<<" dec:"<<RPCodecInfo::decode <<endl;
	
	if(itSes!=sessions.end())
	{
		throw(RPSessionAlreadyStablishedException(1, " Sessio ja establerta"));
	}

	cout<<"MPEGRecMod::StartSession new session" <<endl;
	Sessio *ses=new Sessio(id);
	cout<<"MPEGRecMod::StartSession /new session :"<<(void*) ses <<endl<<endl;
	sessions[a->toString()]= ses;

}

void IPPMPEGRecodePlugin::endSession(RPCodecInfo id, Address *a)
{
	map<string,Sessio*>::iterator i=sessions.find(a->toString());
	
	cout<<"MPEGRecMod::EndSession:"<<id.name<<endl;
	if(i==sessions.end())
	{
		throw(RPSessionNotStablishedException(0, " Sessio no establerta"));
	}
	
	delete i->second;
	sessions.erase(i);
}

void IPPMPEGRecodePlugin::encodeFrame(RPFrame *f, Address *a)
{
	Sessio *ses=getSession(*a);

	ses->addFrame(f);
}

RPFrame *IPPMPEGRecodePlugin::decodeFrame(RPFrame *f, Address *a)
{
	Sessio *ses=getSession(*a);

	//RPFrame res=f;
	ses->addFrame(f);
	RPFrame *res=ses->getFrame();

	return res;
}

RPFrame *IPPMPEGRecodePlugin::getFrame(Address *a)
{
	Sessio *ses=getSession(*a);

	RPFrame *res=ses->getFrame();

	return res;
}

