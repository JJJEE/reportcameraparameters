#include <Utils/Types.h>
#include <Plugins/PluginControl.h>
#include <Utils/Canis.h>
#include <NULLRecodePlugin.h>
#include <signal.h>
#include <Utils/debugStackTrace.h>
#include <version.h>

#include <signal.h>

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMPALL();
	abort();
}

#endif


int main()
{

#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif
#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
#endif
	bool exc;

	do
	{
		exc=false;
		try
		{
			NULLRecodePlugin plugin("NULLRecodePluginConfig.xml");

			plugin.serve();
		}
		catch (Exception e)
		{
			exc=true;
			cout << "Exception ocurred during initialization: " << e.getMsg() << endl;
		}
	}while(exc);
	

	return 0;
}

