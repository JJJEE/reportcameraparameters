#ifdef __cplusplus
extern "C" {
#endif
#include <jni.h>
#include <base/java/com_eye_cam_sirius_clients_AVCodecRecodePluginJNI.h>
#include <base/java/sun_java2d_SurfaceData.h>
#ifdef __cplusplus
}
#endif
#include <string>
#include "AVCodecRecodePlugin.h"
#ifdef WIN32
#include <Utils/windowsDefs.h>
#endif

#include <Exceptions/NotEnoughMemoryException.h>

#pragma warning ( disable : 4047 )
#pragma warning ( disable : 4133 )

#ifdef __COMPILING_AVCODEC_JNI
#include <version.h>
#endif

JavaVM *_jvm;

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved)
{
	// Ens guardem el punter a la jvm per poder obtenir el JNIEnv quan el
	// necessitem
	_jvm=vm;
	
	// Requerit per tenir acces als DirectByteBuffers etc.
	return JNI_VERSION_1_4;
}
		
JNIEXPORT void JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_addFrame (JNIEnv *env, jobject obj, jlong ses, jint size, jint width, jint height, jbyteArray data, jboolean isKey) 
{
	if (ses==0)
	{
		jclass newExcCls = env->FindClass("java/lang/IllegalArgumentException");
		if (newExcCls == 0)	// No trobem la classe
			return;
		env->ThrowNew(newExcCls, "Session is null!");
	}
	
	int dataLen=env->GetArrayLength(data);
	if (dataLen!=size)
	{
		cout << "addFrame: dataLen does not match size. dataLen: " <<
			dataLen << ", size: " << size << ". Setting size to dataLen..."
			<< endl;
		size=dataLen;
	}
	
	char *ptr;
	ptr = (char*)env->GetPrimitiveArrayCritical(data, 0);

	if(ptr!=NULL)
	{
		((AVCodecRecodePlugin::Sessio*)ses)->addFrame(size, width, height, ptr, isKey);
		env->ReleasePrimitiveArrayCritical(data, ptr, 0);
	}
}

JNIEXPORT jbyteArray JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_getFrame (JNIEnv *env, jobject obj, jlong ses, jbyteArray frameArray) 
{
	if (ses==0)
	{
		jclass newExcCls = env->FindClass("java/lang/IllegalArgumentException");
		if (newExcCls == 0)	// No trobem la classe
			return frameArray;
		env->ThrowNew(newExcCls, "Session is null!");
	}
	
	jboolean copy=false;
	
	RPFrame *frameRes = ((AVCodecRecodePlugin::Sessio*)ses)->getFrame();

	if (frameRes==NULL)
		return NULL;
		
	jbyteArray res=frameArray;
	if (res==NULL)
		res=env->NewByteArray(frameRes->dsize);

	env->SetByteArrayRegion(res, 0, frameRes->dsize, (const jbyte*)frameRes->data);

	delete frameRes;
	return res;
}

JNIEXPORT jintArray JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_getFrameARGB
  (JNIEnv *env, jobject obj, jlong ses, jintArray frameArray)
{
	if (ses==0)
	{
		jclass newExcCls = env->FindClass("java/lang/IllegalArgumentException");
		if (newExcCls == 0)	// No trobem la classe
			return frameArray;
		env->ThrowNew(newExcCls, "Session is null!");
	}
	
	jboolean copy=false;

	RPFrame *frameRes = ((AVCodecRecodePlugin::Sessio*)ses)->getFrame();

	if (frameRes==NULL)
		return NULL;

	jintArray res=frameArray;
	if (res==NULL)
		res=env->NewIntArray(frameRes->dsize>>2);

	jthrowable exc = env->ExceptionOccurred();
	if (exc)
	{
		cout << "\t*** AVCodecJNI::getFrameARGB(): exc raised" << endl;
		cout << "\t*** AVCodecJNI::getFrameARGB(): exc raised" << endl;
        env->ExceptionDescribe();
		return NULL;
	}
	
	if (res==NULL)
	{
		cout << "\t*** AVCodecJNI::getFrameARGB(): res=NULL" << endl;
		return NULL;
	}
//	cout << "\t--> AVCodecJNI::getFrameARGB(): SetIntArrayRegion(): " << (void*)res << ", " << (void*)frameRes->data << endl;
//	cout.flush();
	env->SetIntArrayRegion(res, 0, frameRes->dsize>>2, (const jint*)frameRes->data);
//	cout << "\t<-- AVCodecJNI::getFrameARGB(): SetIntArrayRegion(): " << (void*)res << ", " << (void*)frameRes->data << endl;
//	cout.flush();
	
	delete frameRes;
	return res;
}

JNIEXPORT void JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_stopStreamingThreadNative
  (JNIEnv *env, jobject obj, jlong ses)
{
	if (ses==0)
	{
		jclass newExcCls = env->FindClass("java/lang/IllegalArgumentException");
		if (newExcCls == 0)	// No trobem la classe
			return;
		env->ThrowNew(newExcCls, "Session is null!");
	}
	
	((AVCodecRecodePlugin::Sessio*)ses)->stopThread();


/*	cout<<"delete iBufARGB:"<<((AVCodecRecodePlugin::Sessio*)ses)->imageBufferARGB<<endl;
	if(((AVCodecRecodePlugin::Sessio*)ses)->imageBufferARGB != NULL)
	{
		jobject jo= (jobject) ((AVCodecRecodePlugin::Sessio*)ses)->imageBufferARGB;
		env->DeleteLocalRef(jo);
	}
	jthrowable exc = env->ExceptionOccurred();
	if (exc)
	{
		cout << "\t*** AVCodec::delete iBufARGB(): exc raised" << endl;
        env->ExceptionDescribe();
	}
*/

// Fa que torni a crear la sesió cada cop, no es queda ocupant mem permanentment, peró es poca informació
//	delete ((AVCodecRecodePlugin::Sessio*)ses);
//	ses=NULL;
}


JNIEXPORT jlong JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_initCplusSide( JNIEnv *env, jobject obj, jstring name, jint mode, jint width, jint height) 
{
//	cout<<" initCppSide -> Ref- ini"<<endl;
	return Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_initCplusSideWithRef
		(env, obj, name, mode, width, height, NULL);
}

JNIEXPORT jlong JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_initCplusSideWithRef(JNIEnv *env, jobject obj, jstring name, jint mode, jint width, jint height, jobject buffer)
{
//	cout<<" initCppSideWithRef - ini"<<endl;

	AVCodecRecodePlugin::Sessio *ses=NULL;

//	cout<<" initCppSide - ini getstrlen"<<endl;
	jsize len=env->GetStringUTFLength(name); 
//	cout<<" initCppSide - ini getstrchar"<<endl;
	char *c=(char*)env->GetStringUTFChars(name,0); 
	string codecName(c, len);
//	cout<<" initCppSide - ini new Ses"<<endl;
	try
	{
		void *bufAddr=NULL;
		int bufLen=0;
		if (buffer!=NULL)
		{	
			int bufLenMul=1;
			jclass BufferClass = env->FindClass("Ljava/nio/Buffer;");
			jclass ByteBufferClass = env->FindClass("Ljava/nio/ByteBuffer;");
			jclass IntBufferClass = env->FindClass("Ljava/nio/IntBuffer;");
			if (BufferClass!=NULL && ByteBufferClass!=NULL &&
				IntBufferClass!=NULL)
			{
				if (env->IsInstanceOf(buffer, BufferClass)==JNI_TRUE)
					cout << "AVCodecRecodePluginJNI: DEBUG: buffer is a java.nio.Buffer" << endl;
				if (env->IsInstanceOf(buffer, IntBufferClass)==JNI_TRUE)
					bufLenMul=4;
			}
			else
				cout << "AVCodecRecodePluginJNI: DEBUG: could not load classes"
					<< endl;

			bufAddr=env->GetDirectBufferAddress(buffer);
			bufLen=env->GetDirectBufferCapacity(buffer)*bufLenMul;
//			cout << "AVCodecRecodePluginJNI: buffer:"<<(void*)bufAddr<<" -> "<<bufLen << endl;
			if (bufAddr==NULL || bufLen<=0)
			{
				bufLen=0;
				cout << "AVCodecRecodePluginJNI: Looks like direct buffer "
					"access is not supported by this JVM or buffer is not "
					"a DirectByteBuffer" << endl;
	
				// Posem "this.buffer" de java a NULL, perque quedi clar que
				// no es JavaBuffered (isJavaBuffered() de RecodePluginJNI)
				jfieldID bufferId;
				jclass objClass;
				
				objClass = env->GetObjectClass(obj);
				if (objClass!=NULL)
				{
					bufferId = env->GetFieldID(objClass, "buffer",
						"Ljava/nio/Buffer;");
					if (bufferId!=NULL)
					{
						env->SetObjectField(obj, bufferId, NULL);
					}
				}
			}
			
			cout << "initCplusSideWithRef: buffer @ " << bufAddr << ", " <<
				bufLen << " bytes (" << width << "x" << height << "x4 = " 
				<< (width*height*4) << " calculated bytes)" << endl;
		}
			
		ses=new AVCodecRecodePlugin::Sessio(RPCodecInfo(codecName, mode, width, height), bufAddr, bufLen);
	}
	catch (NotEnoughMemoryException &neme)
	{
		jclass newExcCls = env->FindClass("com/eye_cam/sirius/exceptions/NotEnoughMemoryException");
		if (newExcCls == 0)	// No trobem la classe
			return (jlong)0;
		env->ThrowNew(newExcCls, neme.getMsg().c_str());
	}
//	cout<<" initCppSide - ini release"<<endl;
	env->ReleaseStringUTFChars(name, c);
	
//	cout<<" initCppSide - session:"<<(void*)ses<<" info.name:"<<((AVCodecRecodePlugin::Sessio*)ses)->info.name<<endl;
	return (jlong)((void*)ses);
}

JNIEXPORT jlong JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_initCplusSideWithPtr(JNIEnv *env, jobject obj, jstring name, jint mode, jint width, jint height, jlong bufferPtr)
{
//	cout<<" initCppSideWithPtr - ini"<<endl;
//	cout<<" initCppSide - ini"<<endl;

	AVCodecRecodePlugin::Sessio *ses=NULL;

//	cout<<" initCppSide - ini getstrlen"<<endl;
	jsize len=env->GetStringUTFLength(name); 
//	cout<<" initCppSide - ini getstrchar"<<endl;
	char *c=(char*)env->GetStringUTFChars(name,0); 
	string codecName(c, len);
//	cout<<" initCppSide - ini new Ses"<<endl;
	try
	{
		SurfaceDataOps *ops=(SurfaceDataOps *)bufferPtr;

		if (ops==NULL)
			cout << "initCplusSideWithPtr: buffer is NULL (will use copying"
				"model)" << endl;
		else
		{
			cout << "initCplusSideWithPtr: Testing bufer..." << endl;
			
			SurfaceDataRasInfo lockInfo;
			lockInfo.bounds.x1=0;
			lockInfo.bounds.y1=0;
			lockInfo.bounds.x2=width;
			lockInfo.bounds.y2=height;
			if (ops->Lock(env, ops, &lockInfo, SD_LOCK_RD_WR) != SD_SUCCESS)
			{
				cout << "initCplusSideWithPtr: SurfaceData could not be locked!!!" << endl;
				ops=NULL;
			}
			else
			{
				ops->GetRasInfo(env, ops, &lockInfo);

				void *surface=lockInfo.rasBase;
				if (surface!=NULL)
				{
					if (ops->Release!=NULL)
						ops->Release(env, ops, &lockInfo);
					if (ops->Unlock!=NULL)
						ops->Unlock(env, ops, &lockInfo);
				}
				else
				{
					ops=NULL;
				}
			}
		}

		ses=new AVCodecRecodePlugin::Sessio(RPCodecInfo(codecName, mode, width, height), ops);
		cout << "initCplusSideWithPtr: Inited Session..." << endl;
	}
	catch (NotEnoughMemoryException &neme)
	{
		jclass newExcCls = env->FindClass("com/eye_cam/sirius/exceptions/NotEnoughMemoryException");
		if (newExcCls == 0)	// No trobem la classe
			return (jlong)0;
		env->ThrowNew(newExcCls, neme.getMsg().c_str());
	}
//	cout<<" initCppSide - ini release"<<endl;
	env->ReleaseStringUTFChars(name, c);
	
//	cout<<" initCppSide - session:"<<(void*)ses<<" info.name:"<<((AVCodecRecodePlugin::Sessio*)ses)->info.name<<endl;
	return (jlong)((void*)ses);
}

JNIEXPORT void JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_addFrameRef(JNIEnv *env, jobject obj, jlong ses, jint size, jint width, jint height, jobject inBuffer, jboolean isKey)
{
	if (ses==0)
	{
		jclass newExcCls = env->FindClass("java/lang/IllegalArgumentException");
		if (newExcCls == 0)	// No trobem la classe
			return;
		env->ThrowNew(newExcCls, "Session is null!");
	}
	
	jlong dataLen=env->GetDirectBufferCapacity(inBuffer);
	if (dataLen!=size)
	{
		cout << "addFrameRef: dataLen does not match size. dataLen: " <<
			dataLen << ", size: " << size << ". Setting size to dataLen..."
			<< endl;
		size=dataLen;
	}
	
	// TODO: no val, pq el buffer ha de ser DirectBuffer :P
	// Afegim un frame a la cua sense copiar
	void *data=env->GetDirectBufferAddress(inBuffer);
	((AVCodecRecodePlugin::Sessio*)ses)->addFrame(new RPFrame(size, width, height, data, isKey, true));
}

JNIEXPORT void JNICALL Java_com_eye_1cam_sirius_clients_AVCodecRecodePluginJNI_getFrameARGBRef(JNIEnv *env, jobject obj, jlong ses)
{
	if (ses==0)
	{
		jclass newExcCls = env->FindClass("java/lang/IllegalArgumentException");
		if (newExcCls == 0)	// No trobem la classe
			return;
		env->ThrowNew(newExcCls, "Session is null!");
	}
	
	// Nomes fem el get per saber que s'ha processat, pero les dades ja son
	// al buffer :D
	RPFrame *frameRes = ((AVCodecRecodePlugin::Sessio*)ses)->getFrame();
	delete frameRes;
}


#ifdef __cplusplus
}
#endif
