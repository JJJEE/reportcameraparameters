#pragma once
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginRecode.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/SocketTCP.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <map>
#include <list>
#include <iostream>
#ifdef __cplusplus
extern "C" {
#endif
#include <Quicktime/Movies.h>
#include <Quicktime/MoviesFormat.h>
#include <Quicktime/QuickTimeComponents.h>
#include <Quicktime/QuickTime.h>


#ifdef __cplusplus
}
#endif
using namespace std;

class JPEGRecodePlugin: public PluginRecode
{
		class Sessio
		{
		//	ComponentInstance c;
			public:
				RPCodecInfo info;
				int nframe;
				list<RPFrame*> encodedFrames;
				Condition outCond;
				Sessio(RPCodecInfo info):nframe(0),info(info){};
				Sessio():nframe(0){};
		};

		map<string, Sessio*> sessions;

		
	protected:
		Sessio* getSession(Address a);
		CodecComponent codec;
		CompressorComponent enc;
		DecompressorComponent dec;
		
	public:
		DBGateway db;
		JPEGRecodePlugin(string file);
		~JPEGRecodePlugin();
		
		virtual void startSession(RPCodecInfo id, Address *a);
		virtual void endSession(RPCodecInfo id, Address *a);

		virtual RPFrame *decodeFrame(RPFrame *f, Address *a);
		virtual void encodeFrame(RPFrame *f, Address *a);
		virtual RPFrame *getFrame(Address *a);
};

