#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <Plugins/GestorRecodeExceptions.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/Address.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <RecodeModuleInterface.h>
#include <ModuleAccess/RecodeModuleAccess.h>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <iostream>
#include <version.h>

#include <signal.h>

using namespace std;

int main(int argc, char* arg)
{

	try{
		RPFrame res;
		//		Address a(IP("192.168.0.50"),4242);
		{
			RecodeModuleAccess rma(new Canis("RecodeModuleCanisConfig.xml"));
			DecodeModuleAccess dma(new Canis("../DecodeModule/DecodeModuleCanisConfig.xml"));
			RPCodecInfo jpg("NULL", RPCodecInfo::decode, 128, 128);	
			RPCodecInfo mpg("MPEG",  RPCodecInfo::encode, 128, 128);	
			dma.startSession(26); // RZ50 :P
			cout<<" startSession"<<endl;
			RPSession ses(jpg, mpg);
			rma.startSession(ses);	
			cout<<" /startSession"<<endl;
			dma.setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));

			for(int i=0;i<256;i++)
			{
				IPFrame frame;
				char* buf2;
				int len;
				frame = dma.getCompressedNextFrame();
				rma.processFrame(RPFrame(len, 128, 128, buf2));
				res = rma.getFrame();
				cout<<" result:"<< res.dsize<<" bytes"<<endl;
				
			}


			FILE *fd=fopen("result.raw","w+b");

			fwrite(res.data, 1, res.dsize,fd);
			fclose(fd);

			rma.endSession(ses);	
		}
		cout<<" RAW, reencodem"<<endl;
	}catch(Exception &e)
	{
		cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
	}
}

