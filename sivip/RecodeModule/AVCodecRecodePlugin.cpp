#ifdef WIN32

#define __restrict__

#include <stdint.h>
#include <inttypes.h>
#endif 

#include <Plugins/GestorRecodeExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <RecodeModule/AVCodecRecodePlugin.h>
#include <RecodeModule/RecodeModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <iostream>
#include <Utils/Timer.h>
#include <util/Queue/Queue>

extern "C" {
#include <avcodec.h>
#include <avformat.h>
#include <swscale.h>
}

#if LIBAVCODEC_VERSION_MAJOR < 53

#define AV_PKT_FLAG_KEY PKT_FLAG_KEY
#define AVMEDIA_TYPE_VIDEO CODEC_TYPE_VIDEO

#endif

//#include <umc_video_decoder.h>
#ifndef WIN32
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#else
#include <Utils/WindowsDefs.h>
#endif

using namespace std;

// Ens ve de AVCodecRecodePluginJNI
extern JavaVM *_jvm;


Mutex *AVCodecRecodePlugin::avCodecOpLock=new Mutex(true);

/*
AVCodecRecodePlugin::Sessio::Sessio()
{
	init();
}
*/

bool DecodeNextFrame(AVCodecContext *pCodecCtx, 
    byte *data, int size, AVFrame *pFrame)
{
//	cout << "DNF" << endl;
//	cout << "DNF" << endl;
//	cout << "DNF" << endl;
//	cout << "DNF" << endl;
//	cout << "DNF" << endl;
	
	// static AVPacket packet;
	int bytesDecoded=0;
	int frameFinished=0;
//	int      bytesRemaining=size;
//	uint8_t  *rawData=(uint8_t*)data;

//	cout << "DNF " << (void*)data << ", size: " << size << endl;

	byte *nextFrame=data;
	unsigned int nframes=*((unsigned int*)nextFrame);
//	cout << "DNF nF: " << nframes << endl;
	nextFrame+=sizeof(int);
	
	if (nframes==0)
		return true;
		
	if (nframes>50)
		return false;

	unsigned int extradataSize=*((unsigned int*)nextFrame);

//	cout << "DNF eDsize: " << extradataSize << endl;

	if (extradataSize>128)
		return false;
	
	nextFrame+=sizeof(int);
	byte *extradata=nextFrame;
	nextFrame+=extradataSize;

	if (pCodecCtx->extradata!=NULL && extradataSize!=pCodecCtx->extradata_size)
	{
		delete [] pCodecCtx->extradata;
		pCodecCtx->extradata=NULL;
	}

	//	if (pCodecCtx->extradata==NULL)
	//		pCodecCtx->extradata=new uint8_t[extradataSize];
	//	memmove(pCodecCtx->extradata, extradata, extradataSize);
	//	pCodecCtx->extradata_size=extradataSize;

	// Nomes copiem el primer cop que fem alloc/si canvia de tamany, etc... :D
	// En teoria no canvia i per tant sera nomes el primer cop absolut
	if (pCodecCtx->extradata==NULL)
	{
		pCodecCtx->extradata=new uint8_t[extradataSize];
		memmove(pCodecCtx->extradata, extradata, extradataSize);
		pCodecCtx->extradata_size=extradataSize;
	}

	Timer t;
	TimerInstant ti;
	t.start();

	for(unsigned int j=1;j<nframes;j++)
	{
		unsigned int frameSize=*((unsigned int*)nextFrame);
		nextFrame+=sizeof(int);
		byte *newFrame=nextFrame;
		nextFrame+=frameSize;

//		cout << "dnf: " << j << ", " << nframes << ", " << frameSize << endl; 

		if (frameSize == 0)
		{
			cout << "NOTICE: AVCodecRecodePlugin: frameSize == 0" << endl;
			continue;
		}
		else if (frameSize >= 0x80000000)
		{
			cout << "NOTICE: AVCodecRecodePlugin: frameSize < 0" << endl;
			continue;
		}
		
//		cout << "DecodeNextFrame - data:"
//			<< StrUtils::hexDump(string((char*)newFrame, frameSize));

//		AVCodecRecodePlugin::avCodecOpLock->lock();

#if LIBAVCODEC_VERSION_MAJOR < 53
		bytesDecoded=avcodec_decode_video(pCodecCtx, pFrame,
				&frameFinished, newFrame, frameSize);
#else
		//ffmpeg 0.8.2
		AVPacket avpkt; 
		av_init_packet(&avpkt); 
		avpkt.data = newFrame; 
		avpkt.size = frameSize; 
		bytesDecoded=avcodec_decode_video2(pCodecCtx, pFrame,
				&frameFinished, &avpkt);
#endif

//		AVCodecRecodePlugin::avCodecOpLock->unlock();
		
//		cerr << "DECODED --- " << bytesDecoded << endl;
		// Was there an error?
		if(bytesDecoded < 0)
		{
			cerr << "Error while decoding frame" << endl;
			return false;
		}

//		int bdIts=0;
//		int bdDecOff=0;
//		while (bytesDecoded > 0 && bytesDecoded < frameSize-bdDecOff && bdIts < 5)
//		{
//			bdDecOff+=bytesDecoded;
//			
//			cout << "OJO[" << bdIts << "]: bytesDecoded < frameSize: " << bytesDecoded << " < " << frameSize << endl;
//			bytesDecoded=::dec_decode_video(pCodecCtx, pFrame,
//				&frameFinished, newFrame+bdDecOff, frameSize-bdDecOff);
//
//			cout << "OJO[" << bdIts << "]: bd: " << bytesDecoded << "frameSize - bdDecOff: " << frameSize << " - " << bdDecOff << ": " << (frameSize-bdDecOff) << endl;
//			bdIts++;
//		}

//		bytesRemaining-=bytesDecoded;
//		rawData+=bytesDecoded;

		if(frameFinished)
		{
			ti=t.time();
			t.start();
		}

		if (bytesDecoded==0)
		{
			cerr << "BD 0 !!! Error while decoding frame" << endl;
			AVCodecRecodePlugin::avCodecOpLock->lock();
#if LIBAVCODEC_VERSION_MAJOR < 53
			bytesDecoded=avcodec_decode_video(pCodecCtx, pFrame,
				&frameFinished, newFrame, frameSize);
#else				//ffmpeg 0.8.2
			AVPacket avpkt; 
			av_init_packet(&avpkt); 
			avpkt.data = newFrame; 
			avpkt.size = frameSize; 
			bytesDecoded=avcodec_decode_video2(pCodecCtx, pFrame,
					&frameFinished, &avpkt);
#endif
			AVCodecRecodePlugin::avCodecOpLock->unlock();
			cerr << "POST BD 0 !!! Error while decoding frame" << endl;
		}
	}
	
	return true;
}

AVCodecRecodePlugin::Sessio::Sessio(RPCodecInfo info) :
	info(info), surfaceOps(NULL), usingSurfaceOps(false),
	imageBufferARGB(NULL), imageBufferLength(0)
{
	cout << "AVCodecRecodePlugin::Sessio::Sessio(RPCodec) (" << 
		(imageBufferARGB==NULL?"":"DirectBuffer") << ")" << endl;
	AVCodecRecodePlugin::initAVCodec();

	this->run=true;
	this->thId=-1;
	this->thId=this->start();
}

AVCodecRecodePlugin::Sessio::Sessio(RPCodecInfo info, void *imageBufferARGB,
int imageBufferLength) :
	info(info), surfaceOps(NULL), usingSurfaceOps(false),
	imageBufferARGB(imageBufferARGB), imageBufferLength(imageBufferLength)
{
	cout << "AVCodecRecodePlugin::Sessio::Sessio(RPCodec, buff*, bLen*)(" << 
		(imageBufferARGB==NULL?"":"DirectBuffer") << ")" << endl;
	AVCodecRecodePlugin::initAVCodec();

	this->run=true;
	this->thId=-1;
	this->thId=this->start();
}

AVCodecRecodePlugin::Sessio::Sessio(RPCodecInfo info,
	SurfaceDataOps *surfaceOps) :
	info(info), surfaceOps(surfaceOps), usingSurfaceOps(surfaceOps!=NULL),
	imageBufferARGB(NULL), imageBufferLength(0)
{
	cout << "AVCodecRecodePlugin::Sessio::Sessio(surfaceOps)" << endl;

	AVCodecRecodePlugin::initAVCodec();

	this->run=true;
	this->thId=-1;
	this->thId=this->start();
}
AVCodecRecodePlugin::Sessio::~Sessio()
{
//	cout << "AVCodecRecodePlugin::Sessio::~Sessio() esborrém sessió" << endl;
//	cout<<"~Sessio()  threads: inst:"<<this->runningInstances()<<endl;

//	cout<<"delete sOps"<<endl;
//	delete surfaceOps;


//	cout<<"delete iBufARGB"<<endl;
//	if(imageBufferARGB!=NULL)
//		delete[] (byte*)imageBufferARGB;
}

void AVCodecRecodePlugin::Sessio::stopThread()
{
	this->run=false;
	this->inQ.queue(NULL);
	
	if (this->thId!=-1)
	{
		cout << "\tAVCodecRecodePlugin::Sessio::stopThread(): Joining..." << endl;
		this->join(this->thId);
		cout << "\tAVCodecRecodePlugin::Sessio::stopThread(): Joined" << endl;
	}
	while(inQ.size()>0)
	{
		RPFrame *f=NULL;
		cout<<"\tAVCodecRecodePlugin::Sessio::stopThread(): inQ frames remaining"<<endl;
		f=this->inQ.get();
		if (f!=NULL)
			delete f;
	}
	while(outQ.size()>0)
	{
		RPFrame *f=NULL;
		cout<<"\tAVCodecRecodePlugin::Sessio::stopThread(): outQ frames remaining"<<endl;
		f=this->outQ.get();
		if (f!=NULL)
			delete f;
	}
}

void* AVCodecRecodePlugin::Sessio::execute(int id, void* param)
{
	cout<<"----AVCodecRecMod::execute start---"<<endl;
	int err;
	RPFrame *f=NULL;
	if(info.mode==RPCodecInfo::encode)
	{
		for(int i=0;this->run;i++)
		{
			f=this->inQ.get();
	
			if (f!=NULL)
				delete f;
		}
	}
	else
	{
		// JNI SurfaceData direct access information
		SurfaceDataRasInfo surfLockInfo;
		// JNIEnv
		JNIEnv *jniEnv=NULL;
		if (_jvm==NULL)
		{
			cout << "FATAL: JVM not initialized" << endl;
			// TODO: es segur fer aixo? (en qualsevol cas no hauriem de 
			// fer-ho mai)
			return NULL;
		}		
		_jvm->AttachCurrentThread((void**)&jniEnv, NULL);

		while (this->run && (f==NULL || f->width<=0 || f->height<=0 ||
			f->width>RPFrame::maxWidth || f->height>RPFrame::maxHeight))
		{
			// Si el frame es NULL o xungo, esperem un altre
			if (f!=NULL)
				delete f;

			f=this->inQ.get();
		}

		if (this->run==false)
		{
			_jvm->DetachCurrentThread();
			return NULL;
		}

		AVCodec *pCodec=avcodec_find_decoder(AV_CODEC_ID_MPEG2VIDEO);
		if (!pCodec)
 		   exit(1);
 
		AVCodecContext *pCodecCtx=avcodec_alloc_context3(pCodec);//(CODEC_TYPE_VIDEO);
		
		// Nou 20090505
		avcodec_get_context_defaults3(pCodecCtx,NULL);

		//AVCodec *pCodec=avcodec_find_decoder_by_name(info.name.c_str());

#if LIBAVCODEC_VERSION_MAJOR >= 52
		pCodecCtx->bits_per_coded_sample=av_get_bits_per_sample(pCodec->id);
#else
		pCodecCtx->bits_per_sample=av_get_bits_per_sample(pCodec->id);
#endif
		pCodecCtx->width=f->width;
		pCodecCtx->height=f->height;

		AVCodecRecodePlugin::avCodecOpLock->lock();

		if(avcodec_open2(pCodecCtx, pCodec,NULL)<0)
		{
			AVCodecRecodePlugin::avCodecOpLock->unlock();

			_jvm->DetachCurrentThread();
			throw Exception("Could not open codec");
		}

		AVCodecRecodePlugin::avCodecOpLock->unlock();
		
		pCodecCtx->extradata=NULL;
		pCodecCtx->extradata_size=0;

		// Allocate video frame
		AVFrame *pFrame, *pFrameDeinterlaced, *pFrameRGB;

		pFrame=avcodec_alloc_frame();
		pFrameDeinterlaced=avcodec_alloc_frame();
		pFrameRGB=avcodec_alloc_frame();

		if (pFrame==NULL || pFrameDeinterlaced==NULL || pFrameRGB==NULL)
		{
			if(pFrame!=NULL)
				av_free(pFrame);
			if(pFrameDeinterlaced!=NULL)
				av_free(pFrameDeinterlaced);
			if(pFrameRGB!=NULL)
				av_free(pFrameRGB);

			this->run=false;

			avcodec_close(pCodecCtx);
			av_free(pCodecCtx);

			_jvm->DetachCurrentThread();
			return NULL;
		}
		
		int numBytes=avpicture_get_size(PIX_FMT_RGB32, f->width, f->height);
		int numBytesYUV=avpicture_get_size(pCodecCtx->pix_fmt, f->width, f->height);

		uint8_t *buffer=(byte*)this->imageBufferARGB;
		if (this->usingSurfaceOps)
		{
			// Fem un primer lock per comprovar que tot esta ok :D
			surfLockInfo.bounds.x1=0;
			surfLockInfo.bounds.y1=0;
			surfLockInfo.bounds.x2=f->width;
			surfLockInfo.bounds.y2=f->height;
			if (this->surfaceOps->Lock(jniEnv, this->surfaceOps, &surfLockInfo, SD_LOCK_RD_WR) != SD_SUCCESS)
			{
				//cout << "AVCodecRecodePlugin::execute:new buffer:"<< numBytes<<" SurfaceData could not be locked" << endl;
				this->usingSurfaceOps=false;
				buffer=new uint8_t[numBytes];
			}
			else
			{
				this->surfaceOps->GetRasInfo(jniEnv, this->surfaceOps,
					&surfLockInfo);
				buffer=(uint8_t*)surfLockInfo.rasBase;
				if (buffer==NULL)		// No hauria de passar :'(
				{
					this->usingSurfaceOps=false;
	//			cout << "AVCodecRecodePlugin::execute:new buffer:"<< numBytes<<" (2) surfaceOps , buffer===NULL" << endl;
					buffer=new uint8_t[numBytes];
				}

				if (this->surfaceOps->Release!=NULL)
					this->surfaceOps->Release(jniEnv, this->surfaceOps,
						&surfLockInfo);
	//			else
	//				cout<<"AVCodecRecodePlugin surfaceOps->Release not released"<<endl;

				if (this->surfaceOps->Unlock!=NULL)
					this->surfaceOps->Unlock(jniEnv, this->surfaceOps,
						&surfLockInfo);
	//			else
	//				cout<<"AVCodecRecodePlugin surfaceOps->Unlock not unlocking"<<endl;
			}
		}
		else if (buffer==NULL || this->imageBufferLength<numBytes)
		{
			if (this->imageBufferLength<numBytes)
				cout << "AVCodecRecodePlugin::execute: buffer length "
					"supplied is not sufficient or no buffer supplied: got " <<
					this->imageBufferLength << ", needed " << numBytes << endl;
			this->imageBufferARGB=NULL; 
//			cout << "AVCodecRecodePlugin::execute:new buffer:"<< numBytes<<" (3) supplied buffer, buffer===NULL" << endl;
			buffer=new uint8_t[numBytes];
		}
/*		else
		{
			cout << "AVCodecRecodePlugin::execute: using supplied buffer :"<<(void*)buffer<<"  : "<<
				this->imageBufferLength<<" bytes, needed " << numBytes << endl;
		}

*/		
		uint8_t *bufferPF=new uint8_t[numBytesYUV];
		uint8_t *bufferDeint=new uint8_t[numBytesYUV];

		if (buffer==NULL || bufferPF==NULL || bufferDeint==NULL)
		{
			if (!this->usingSurfaceOps || (buffer!=NULL && this->imageBufferARGB==NULL))
				delete [] buffer;
			if (bufferPF!=NULL)
				delete [] bufferPF;
			if (bufferDeint!=NULL)
				delete [] bufferDeint;
				
			this->run=false;

			avcodec_close(pCodecCtx);
			av_free(pCodecCtx);

			_jvm->DetachCurrentThread();
			return NULL;
		}

		memset(bufferPF, 0, numBytesYUV);
		memset(bufferDeint, 0, numBytesYUV);

		// Assign appropriate parts of buffer to image planes in pFrameRGB
		avpicture_fill((AVPicture *)pFrameRGB, buffer, PIX_FMT_RGB32,
				 f->width, f->height);
		avpicture_fill((AVPicture *)pFrameDeinterlaced, bufferDeint,
			pCodecCtx->pix_fmt, f->width, f->height);
		avpicture_fill((AVPicture *)pFrame, bufferPF,
			pCodecCtx->pix_fmt, f->width, f->height);

		SwsContext *img_convert_ctx=NULL;

		while (this->run && f!=NULL)
		{
			// Fem lock del buffer de java per poder descomprimir
			bool skipFrame=(f->data==NULL);
			bool decoded=false;
			
			if (!skipFrame && this->usingSurfaceOps)
			{
				surfLockInfo.bounds.x1=0;
				surfLockInfo.bounds.y1=0;
				surfLockInfo.bounds.x2=f->width;
				surfLockInfo.bounds.y2=f->height;
				if (this->surfaceOps->Lock(jniEnv, this->surfaceOps, &surfLockInfo, SD_LOCK_RD_WR) == SD_SUCCESS)
				{
					this->surfaceOps->GetRasInfo(jniEnv, this->surfaceOps,
						&surfLockInfo);
					buffer=(uint8_t*)surfLockInfo.rasBase;
					if (buffer==NULL)		// No hauria de passar :'(
						skipFrame=true;
					pFrameRGB->data[0]=buffer;
				}
				else
					skipFrame=true;
			}
			
			if (!skipFrame)
			{
				unsigned int nframes=*((unsigned int*)f->data);
				
				if (nframes<=12000)
				{
					byte *nextFrame=(byte*)((byte*)f->data+((nframes+1)*sizeof(int)));
		
					for(unsigned int j=1;j<=nframes;j++)
					{
						unsigned int frameSize=*((unsigned int*)((byte*)f->data+(j*sizeof(int))));
						byte *newFrame=nextFrame;
						nextFrame+=frameSize;

						decoded=DecodeNextFrame(pCodecCtx, newFrame, frameSize,
							pFrame);
					}
				}
				else
					decoded=false;
								
				if (decoded)
				{
					avpicture_deinterlace((AVPicture*)pFrameDeinterlaced,
						(AVPicture*)pFrame, pCodecCtx->pix_fmt, pCodecCtx->width,
						pCodecCtx->height);
		
					if (img_convert_ctx == NULL)
					{
						img_convert_ctx = sws_getContext(f->width,
							f->height, pCodecCtx->pix_fmt,
							f->width, f->height, PIX_FMT_RGB32,
							SWS_FAST_BILINEAR, NULL, NULL, NULL);
		
						if (img_convert_ctx == NULL)
						{
							cerr << "Cannot initialize the conversion context" << endl;

							avcodec_close(pCodecCtx);
							av_free(pCodecCtx);

							_jvm->DetachCurrentThread();
							return NULL;
						}
					}
					
					sws_scale(img_convert_ctx, pFrameDeinterlaced->data,
						pFrameDeinterlaced->linesize, 0, f->height,
						pFrameRGB->data, pFrameRGB->linesize); 
				}
	
				// Si hem fet el frame i estem fent servir locks...
				if (this->usingSurfaceOps)
				{
					if (this->surfaceOps->Release!=NULL)
						this->surfaceOps->Release(jniEnv, this->surfaceOps,
							&surfLockInfo);
//					else
//						cout<<"AVCodecRecodePlugin surfaceOps->Release(2) not released"<<endl;
					if (this->surfaceOps->Unlock!=NULL)
						this->surfaceOps->Unlock(jniEnv, this->surfaceOps,
							&surfLockInfo);
//					else
//						cout<<"AVCodecRecodePlugin surfaceOps->Release not unlocking"<<endl;
				}
			}
			
			dword len=f->height*f->width*4;	

			RPFrame *res=NULL;
			// Si fem servir acces directe, no retornem dades
			if (this->usingSurfaceOps || this->imageBufferARGB==buffer)
				res=new RPFrame(0, f->width, f->height, NULL, true, true);
			else
				res=new RPFrame(len, f->width, f->height, pFrameRGB->data[0],
					true, false);
			
			if (res!=NULL)
				res->bitDepth=32;

			this->outQ.queue(res);
			decodedFrames++;

			if (f!=NULL)
				delete f;

			f=this->inQ.get();
			
			while (this->run && (f==NULL || f->width<=0 || f->height<=0 ||
				f->width>RPFrame::maxWidth || f->height>RPFrame::maxHeight))
			{
				if (f!=NULL)
					delete f;

				cout << "****************************************************"
					<< endl
					<< "AVCodecRecodePlugin WARNING: Received Invalid Frame!"
					<< endl
					<< "@ " << __func__ << " (" << __FILE__ << ":" <<
						__LINE__ << ")" << endl
					<< "****************************************************"
					<< endl;
					
				f=this->inQ.get();
				
			}
		}
		
		av_free(pFrame);
		av_free(pFrameDeinterlaced);
		av_free(pFrameRGB);

		avcodec_close(pCodecCtx);
//		av_free(pCodec);
		av_free(pCodecCtx);


		if (img_convert_ctx!=NULL)
			sws_freeContext(img_convert_ctx);

		// Nomes borrem el buffer si no es el de Java
		if (!this->usingSurfaceOps && this->imageBufferARGB==NULL)//(buffer != this->imageBufferARGB)
			delete [] buffer;

		delete [] bufferPF;
		delete [] bufferDeint;
		
		_jvm->DetachCurrentThread();
	}

	return NULL;
}


void AVCodecRecodePlugin::Sessio::addFrame(RPFrame *f)
{
//	if(this->runningInstances()==0)
//	{
//		AVCodecRecodePlugin::initAVCodec();
//		cout<<"AVCodecRecodePlugin::Sessio::addFrame thread not found, starting..."<<endl;
//		this->run=true;
//		this->thId=-1;
//		this->thId=this->start();
//	}
	this->inQ.queue(f);

//	inBuf.push_back(f);
//	inCond.signal(); 
}

void AVCodecRecodePlugin::Sessio::addFrame(int size, int width, int height, char* data, bool isKey)
{
	addFrame(new RPFrame(size, width, height, data, isKey));
}

RPFrame *AVCodecRecodePlugin::Sessio::getFrame()
{
	return this->outQ.get();
}


void AVCodecRecodePlugin::initAVCodec()
{
	static bool AVCodecInited=false;

	// Doble-check per evitar el lock en un 99,99% dels casos ;)
	if (!AVCodecInited)
	{
		AVCodecRecodePlugin::avCodecOpLock->lock();
		if (!AVCodecInited)
		{
			cout<<"\tAVCodecRecodePlugin::initAVCodec(): initializing."<<endl;
			avcodec_register_all();
			av_register_all();
			//avcodec_register_all();
			AVCodecInited=true;
		}
		AVCodecRecodePlugin::avCodecOpLock->unlock();
	}
}

AVCodecRecodePlugin::AVCodecRecodePlugin(string file):PluginRecode(file), db(address, type, cn)
{

	cout<<"init:"<<file<< endl;
	FILE *f=fopen(file.c_str(),"rb");

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char *buf2=new char[len];
	fread(buf2,len,1,f);
	fclose(f);

	XML *conf=xmlParser::parse(buf2);
	conf->getNodeData("[0]/ModelConfigFile");

	delete conf;
	delete[] buf2;
}

AVCodecRecodePlugin::~AVCodecRecodePlugin()
{
}

AVCodecRecodePlugin::Sessio* AVCodecRecodePlugin::getSession(Address a)
{
	map<string,Sessio*>::iterator i=sessions.find(a.toString());
	
	if (i==sessions.end())
	{
		throw (RPSessionNotStablishedException(0, string("Session no establerta")));
	}
	return i->second;
}

void AVCodecRecodePlugin::startSession(RPCodecInfo id, Address *a)
{	
	map<string,Sessio*>::iterator itSes=sessions.find(a->toString());
	
	cout<<endl<<"MPEGRecMod::StartSession:"<<id.name<<" mode:"<<id.mode<<" enc:"<<RPCodecInfo::encode<<" dec:"<<RPCodecInfo::decode <<endl;
	
	if(itSes!=sessions.end())
	{
		throw(RPSessionAlreadyStablishedException(1, "Sessio ja establerta"));
	}

	cout<<"MPEGRecMod::StartSession new session" <<endl;
	Sessio *ses=new Sessio(id);
	cout<<"MPEGRecMod::StartSession /new session :"<<(void*) ses <<endl<<endl;
	sessions[a->toString()]= ses;

}

void AVCodecRecodePlugin::endSession(RPCodecInfo id, Address *a)
{
	map<string,Sessio*>::iterator i=sessions.find(a->toString());
	
	cout<<"MPEGRecMod::EndSession:"<<id.name<<endl;
	if(i==sessions.end())
	{
		throw(RPSessionNotStablishedException(0, "Sessio no establerta"));
	}
	
	delete i->second;
	sessions.erase(i);
}

void AVCodecRecodePlugin::encodeFrame(RPFrame *f, Address *a)
{
	Sessio *ses=getSession(*a);

	ses->addFrame(f);
}

RPFrame *AVCodecRecodePlugin::decodeFrame(RPFrame *f, Address *a)
{
	Sessio *ses=getSession(*a);

	//RPFrame res=f;
	ses->addFrame(f);
	RPFrame *res=ses->getFrame();

	return res;
}

RPFrame *AVCodecRecodePlugin::getFrame(Address *a)
{
	Sessio *ses=getSession(*a);

	RPFrame *res=ses->getFrame();

	return res;
}

