#pragma once
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginRecode.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/SocketTCP.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <util/Queue/Queue.h>
#include <map>
#include <list>
#include <iostream>
#include <base/java/sun_java2d_SurfaceData.h>

using namespace std;

class AVCodecRecodePlugin: public PluginRecode
{
public:
	static Mutex *avCodecOpLock;

	class Sessio: public Thread
	{
	public:
		// info MPEGRecPlugin Sessió
		RPCodecInfo info;
		Queue<RPFrame*> inQ, outQ;
		int encodedFrames, decodedFrames;
		SurfaceDataOps *surfaceOps;
		void *imageBufferARGB;
		int imageBufferLength;
		bool usingSurfaceOps;

		bool run;
		int thId;
		
		Sessio(RPCodecInfo info);
		Sessio(RPCodecInfo info, void *imageBufferARGB, int imageBufferLength);
		Sessio(RPCodecInfo info, SurfaceDataOps *surfaceOps);
		~Sessio();
		void init();
		void addFrame(RPFrame *f);
		void addFrame(int size, int width, int height, char* data, bool isKey=true);
		RPFrame *getFrame();
		void* execute(int id, void* param);
		void stopThread();
	};

protected:
	map<string, Sessio*> sessions;

	Sessio* getSession(Address a);
	static void initAVCodec();
	
public:
	DBGateway db;
	AVCodecRecodePlugin(string file);
	~AVCodecRecodePlugin();


	virtual void startSession(RPCodecInfo id, Address *a);
	virtual void endSession(RPCodecInfo id, Address *a);

	virtual RPFrame *decodeFrame(RPFrame *f, Address *a);
	virtual void encodeFrame(RPFrame *f, Address *a);
	virtual RPFrame *getFrame(Address *a);
};

