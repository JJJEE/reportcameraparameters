#pragma once
#include <Utils/debugNew.h>
#include <Threads/Thread.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>
#include <Module/ModuleGestor.h>
#include "RecodeModuleInterface.h"

class RecodeModule:public ModuleGestor
{
	public:
		RecodeModule(Address addr, short type, Canis *cn=NULL);
		RecodeModule(string file);
};

