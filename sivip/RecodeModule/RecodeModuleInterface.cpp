#include <Utils/ServiceException.h>
#include <Utils/SerializedException.h>
#include <RecodeModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <Sockets/SocketException.h>
#include <Utils/Timer.h>
#ifndef WIN32
#include <sys/time.h>
#endif

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

RecodeModuleInterface::RecodeModuleInterface(Address addr, short type, Canis *cn): ModuleInterfaceGestor(addr, type, cn)
{
	numServices=serviceCount;
	services=new serviceDef[serviceCount];
	int id=startSessionServiceId;
	sessionlessId.push_back(id);

	services[startSessionServiceId].call = (DefaultModuleGestorCall)startSession;
	services[endSessionServiceId].call = (DefaultModuleGestorCall)endSession;

	services[processFrameServiceId].call = (DefaultModuleGestorCall)processFrame;
	services[getFrameServiceId].call = (DefaultModuleGestorCall)getFrame;

}

RecodeModuleInterface::~RecodeModuleInterface()
{
}

Gestor* RecodeModuleInterface::createGestor(Address orig, int type, Canis *cn)
{
	return new GestorRecode(orig, type, cn);
}

SerializedException* RecodeModuleInterface::getSessionNotStablished()
{
	RPSessionNotStablishedException sne(0,"Sessió no establerta");
	return sne.serialize();
}

RPCPacket* RecodeModuleInterface::startSession(RecodeModuleInterface *cm, GestorRecode *g, void* params)
{
	char *vpar=(char*)params;
	
	RPSession s;
	s.toLocal(vpar);
	cout<<"RMI::StartSession:"<<s.in.name<<" "<<s.in.width<<","<<s.in.height<<" -> "<<s.out.name<<" "<<s.out.width<<","<<s.out.height<<endl;

	g->startSession(s);
	
	RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}

RPCPacket* RecodeModuleInterface::endSession(RecodeModuleInterface *cm, GestorRecode *g, void* params)
{
	char *vpar=(char*)params;
	
	RPSession s;
	s.toLocal(vpar);

	g->endSession(s);
	
	RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}

RPCPacket* RecodeModuleInterface::processFrame(RecodeModuleInterface *cm, GestorRecode *g, void* params)
{
	char *vpar=(char*)params;
	
	RPFrame f;
	f.toLocal(vpar);
	cout<<"RecModInterface ProcessFrame"<<endl;

	g->processFrame(f);
	cout<<"RecModInterface /ProcessFrame"<<endl;

	//int sz=res.name.length()+res.value.length()+2;

	RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}

RPCPacket* RecodeModuleInterface::getFrame(RecodeModuleInterface *cm, GestorRecode *g, void* params)
{

	cout<<"RecModInterface getFrame"<<endl;
	RPFrame res=g->getFrame();
	cout<<"RecModInterface /getFrame"<<endl;

	//int sz=res.name.length()+res.value.length()+2;
	char *v=(char*)res.toNetwork();

	RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)v, res.serializationSize(), cm->type, 0, true);
	
	delete [] v;
	
	return p;
}

