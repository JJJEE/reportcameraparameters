#pragma once
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginRecode.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/SocketTCP.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <map>
#include <list>
#include <iostream>
using namespace std;

class IPPMPEGRecodePlugin: public PluginRecode
{
	public:
		class Sessio: public Thread
		{
			public:
				// info MPEGRecPlugin Sessió
				RPCodecInfo info;
				list<RPFrame*> inBuf, outBuf;
				Condition inCond, outCond;
				int encodedFrames, decodedFrames;
				
				bool run;

				Sessio(RPCodecInfo info);
			//	Sessio();
				void init();
				void addFrame(RPFrame *f);
				void addFrame(int size, int width, int height, char* data, bool isKey=true);
				RPFrame *getFrame();
				void* execute(int id, void* param);
				void stopThread();
				
				RPFrame *getFromInBuf();
				void putToInBuf(RPFrame *f);
				RPFrame *getFromOutBuf();
				void putToOutBuf(RPFrame *f);
		};

	protected:
		map<string, Sessio*> sessions;

		
		Sessio* getSession(Address a);
		
	public:
		DBGateway db;
		IPPMPEGRecodePlugin(string file);
		~IPPMPEGRecodePlugin();


		virtual void startSession(RPCodecInfo id, Address *a);
		virtual void endSession(RPCodecInfo id, Address *a);

		virtual RPFrame *decodeFrame(RPFrame *f, Address *a);
		virtual void encodeFrame(RPFrame *f, Address *a);
		virtual RPFrame *getFrame(Address *a);
};

