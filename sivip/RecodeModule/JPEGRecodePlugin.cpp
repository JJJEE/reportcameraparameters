#include <Plugins/GestorRecodeExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <JPEGRecodePlugin.h>
#include <RecodeModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

// Quicktime
//
//
//#include <Cocoa/Cocoa.h>
//#include <QTKit/QTKit.h>

//#include "QTReadWriteJPEG.h"
//#include "QTUtilities.h"

//GWorldPtr gGWorld = NULL;      // the GWorld we load the image data into
//WindowPtr gImageWindow = NULL;  // the window we display the image in

//extern OSErr Gestalt(OSType, long*); 

char *QTUtils_ConvertPascalToCString (StringPtr theString)
{
	char     *myString = (char*)malloc(theString[0] + 1);
	short    myIndex = 0;

	for (myIndex = 0; myIndex < theString[0]; myIndex++)
		myString[myIndex] = theString[myIndex + 1];

	myString[theString[0]] = '\0';

	return(myString);
}




JPEGRecodePlugin::JPEGRecodePlugin(string file):PluginRecode(file), db(address, type, cn)
{

	cout<<"init:"<<file<<"--"<< endl;
	FILE *f=fopen(file.c_str(),"rb");

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char buf2[len];
	fread(buf2,len,1,f);
	fclose(f);

	XML *conf=xmlParser::parse(buf2);
	conf->getNodeData("[0]/ModelConfigFile");

	delete conf;

    /* check the version of QuickTime installed */

	cout<<"---------- QTime ----------"<<endl;
	long version;
	OSErr result;
	result = Gestalt(gestaltQuickTime,&version);
	cout<<" result:"<<(int)result<<" noerr:"<<(int) noErr<<" Version:"<<StrUtils::hexToString(version)<<endl;
	cout<<"EnterMovies()"<<endl;
	EnterMovies(); //Inicialitzacio Quicktime



/*	ComponentDescription desc;
	desc.componentType='imco';//imagecompressor
	desc.componentSubType='jpeg';//jpeg
	desc.componentManufacturer='appl';//by Apple :)
	desc.componentFlags=0;
	desc.componentFlagsMask=0;

	cout<<"enc FindNextComp()"<<endl;
	enc=FindNextComponent(0,&desc);
	cout<<"enc OpenComp("<<enc<<")"<<endl;
	ComponentInstance ce, cd;
	OSErr err;
	err=OpenAComponent(enc, &ce);
	cout<<"enc Component Instance c="<<(void*) ce<<endl;
	
	desc.componentType='imdc'; //imagedwcompressor
	desc.componentSubType='jpeg'; //jpeg
	desc.componentManufacturer='appl';//by Apple :)
	desc.componentFlags=0;
	desc.componentFlagsMask=0;
	cout<<"dec FindNextComp()"<<endl;
	dec=FindNextComponent(0,&desc);
	cout<<"dec OpenComp("<<dec<<")"<<endl;
	err=OpenAComponent(dec, &cd);
	cout<<"dec Component Instance c="<<(void*) cd<<endl<<endl;
	if(enc==0 || dec==0 || ce==0 || cd==0)
		exit(0);

	cout<<" getCompInfo"<<endl;
	ComponentDescription aux;
	Handle name, info;
	GetComponentInfo(enc, &aux, name, info, NULL);
	StringPtr sName=(StringPtr )*name;
	cout<<" getCompInfo name:"<<QTUtils_ConvertPascalToCString(sName)<<endl;
	cout<<" getCompInfo name:"<<sName<<endl;
	StringPtr sInfo=(StringPtr )*info;
	cout<<" getCompInfo name:"<<(StringHandle)info<<endl;
	cout<<" getCompInfo name:"<<QTUtils_ConvertPascalToCString((StringPtr)*info)<<endl;
*/
	// Alternativa
	//OpenADefaultComponent('imco','jpeg', &ce);
	//OpenADefaultComponent('imdc','jpeg', &cd);
	

	FindCodec('jpeg', /*bestCompressionCodec*/bestSpeedCodec, &enc, &dec );


	cout<<"----------plugin inicialitzat----------"<<endl;
}

JPEGRecodePlugin::~JPEGRecodePlugin()
{
}

JPEGRecodePlugin::Sessio* JPEGRecodePlugin::getSession(Address a)
{
	//EnterMoviesOnThread(); // per operar dins el thread. Per thread -> per crida. thread a part? :P

	map<string,Sessio*>::iterator i=sessions.find(a.toString());
	
	if (i==sessions.end())
	{
		throw (RPSessionNotStablishedException(0, string("Session no establerta")));
	}

	return i->second;
}

void JPEGRecodePlugin::startSession(RPCodecInfo id, Address *a)
{	
	map<string,Sessio*>::iterator itSes=sessions.find(a->toString());
	
	cout<<"JPEGRecMod::StartSession:"<<id.name<<" mode:"<<id.mode<<" enc:"<<RPCodecInfo::encode<<" dec:"<<RPCodecInfo::decode <<" resol.:"<<id.width<<","<<id.height<<endl;
	
	if(itSes!=sessions.end())
	{
		throw(RPSessionAlreadyStablishedException(1, " Sessio ja establerta"));
	}

	Sessio *ses=new Sessio(id);
	sessions[a->toString()]= ses;

	cout<<"JPEGRecMod::StartSession return"<<id.name<<" mode:"<<id.mode<<" enc:"<<RPCodecInfo::encode<<" dec:"<<RPCodecInfo::decode <<endl;
}

void JPEGRecodePlugin::endSession(RPCodecInfo id, Address *a)
{
	map<string,Sessio*>::iterator i=sessions.find(a->toString());
	
	cout<<"JPEGRecMod::EndSession:"<<id.name<<endl;

	if(i==sessions.end())
	{
		throw(RPSessionNotStablishedException(0, " Sessio no establerta"));
	}
	Sessio *ses=i->second;	
	sessions.erase(i);
	delete ses;

}

RPFrame *JPEGRecodePlugin::decodeFrame(RPFrame *f, Address *a)
{
	cout<<"JPEGRecMod::processFrame in:"<<(void*)a<<endl;
	cout<<"JPEGRecMod::processFrame addr:"<<a->toString()<<endl;
	Sessio *ses=getSession(*a);
	cout<<"JPEGRecMod::processFrame enterMovies"<<endl;
	EnterMoviesOnThread(0); // uops! :)

	cout<<"JPEGRecMod::processFrame: mode:"<<ses->info.mode<<" enc:"<<RPCodecInfo::encode<<" dec:"<<RPCodecInfo::decode <<endl;
	cout<<" :"<<f->dsize<<endl;
	cout<<","<<f->serializationSize()<<endl;
	{
		ImageDescriptionHandle desc;
		PixMapHandle pmh;


		OSErr err;
		ComponentInstance ce, cd;
		//cout<<"JPEGRecMod::decode::processFrame:OpenAComponent"<<endl;
		//err=OpenAComponent(dec, &cd);

		//loadImporterFromDataRef :)
//		PointerDataRefRecord dataPtr;
//		dataPtr.data = f->data;
//		dataPtr.dataLength = f->dsize;

	/*	{
			cout<<"Saving received data :) "<<f->dsize<<endl;
			FILE *fd=fopen("PGMod_encode.jpg","w+b");

			fwrite(f->data, 1, f->dsize,fd);
			fclose(fd);
			
		}*/
		Handle hMovieData = NULL;//NewHandle(sizeof(char*));
		cout<<"JPEGRecMod::decode::processFrame:PtrToHand DataRef -> Hand"<<endl;
		err = PtrToHand( f->data/*&dataPtr*/, &hMovieData, f->dsize);//sizeof(PointerDataRefRecord));
		cout<<"OSErr:"<<err<<endl;
		
		Handle dataRef = NULL;
		cout<<"JPEGRecMod::decode::processFrame:PtrToHand "<<endl;
		err = PtrToHand( &hMovieData, &dataRef, sizeof(Handle) );
		cout<<"OSErr:"<<err<<endl;

/*		cout<<"JPEGRecMod::decode::processFrame:GetGraphicImporter hMovieData:"<<hMovieData<<" dataRef:"<<dataRef<<endl;
		err=GetGraphicsImporterForDataRef(dataRef, HandleDataHandlerSubType, &cd);
		cout<<"OSErr:"<<err<<endl;
*/
	

		GWorldPtr gworld=NULL;
		GDHandle currentDevice;
		GWorldPtr currentGWorld;
	
		cout<<"JPEGRecMod::decode::processFrame:getGWorld"<<endl;
		GetGWorld(&currentGWorld, &currentDevice);
		cout<<"OSErr: no error value"<<endl;

		cout<<"JPEGRecMod::decode::processFrame:openComponent"<<endl;
		err = OpenADefaultComponent(GraphicsImporterComponentType, 'JPEG', &cd);
		cout<<"OSErr:"<<err<<endl;
		
		cout<<"JPEGRecMod::decode::processFrame:GraphicsImportSetDataHandle"<<endl;
		err = GraphicsImportSetDataHandle(cd, hMovieData);
		cout<<"OSErr:"<<err<<endl;

		Rect bounds;

		cout<<"JPEGRecMod::decode::processFrame: GraphicsImportGetNaturalBounds"<<endl;
		err = GraphicsImportGetNaturalBounds(cd, &bounds);
		cout<<"OSErr:"<<err<<endl;

		cout<<"JPEGRecMod::decode::processFrame: NewGWorld bounds:"<<bounds.top<<","<<bounds.left<<","<<bounds.bottom<<","<<bounds.right<<endl;
		int bitDepth=32;
		//err = NewGWorld(&gworld, bitDepth, &bounds, nil, nil, 0);
		err = QTNewGWorld(&gworld, k24RGBPixelFormat, &bounds, nil, nil, 0);
		cout<<"OSErr:"<<err<<endl;

		cout<<"JPEGRecMod::decode::processFrame: GetGWorldPixMap"<<endl;
		PixMapHandle pixels = GetGWorldPixMap(gworld);
		cout<<"OSErr: no error value"<<endl;

//		cout<<"JPEGRecMod::decode::processFrame:LockPixels"<<endl;
		HLock((Handle)pixels);
//		cout<<"OSErr:"<<err<<endl;

		cout<<"JPEGRecMod::decode::processFrame:GraphicsImportSetQuality"<<endl;
		err = GraphicsImportSetQuality(cd, codecLosslessQuality);
		cout<<"OSErr:"<<err<<endl;
		cout<<"JPEGRecMod::decode::processFrame:GraphicsImportSetGWorld"<<endl;
		err = GraphicsImportSetGWorld(cd, gworld, NULL);
		cout<<"OSErr:"<<err<<endl;
		cout<<"JPEGRecMod::decode::processFrame:GraphicsImportSetBoundsRect"<<endl;
		err = GraphicsImportSetBoundsRect(cd, &bounds);
		cout<<"OSErr:"<<err<<endl;
		cout<<"JPEGRecMod::decode::processFrame:GraphicsImportDraw"<<endl;
		err = GraphicsImportDraw(cd);
		cout<<"OSErr:"<<err<<endl;
	//	if(err!=0)
/*		{
			cout<<"Saving received data :) "<<f->dsize<<endl;
			string name;
			if(err!=0)
				name=(string("JPGMod_frame")+StrUtils::decToString(ses->nframe)+string("_petat")+string(".jpg"));
			else
				name=(string("JPGMod_frame")+StrUtils::decToString(ses->nframe)+string(".jpg"));
			FILE *fd=fopen(name.c_str(),"w+b");

			fwrite(f->data, 1, f->dsize,fd);
			fclose(fd);
			
		}

*/
		cout<<"struct PixMap{"<<endl;
		PixMapPtr pmp=*pixels;
		PixMap pm=*pmp;

		cout<<"	Ptr baseAddr = "<<(void*)pm.baseAddr<<" get:"<< (void*) GetPixBaseAddr(pixels)<<endl;

		cout<<"	short rowBytes = "<<(pm.rowBytes & 0x3FFF )<<" get:"<< QTGetPixMapHandleRowBytes(pixels)<<endl;
		cout<<"	Rect bounds = ("<<pm.bounds.top<<","<<pm.bounds.left<<","<<pm.bounds.bottom<<","<<pm.bounds.right<<")"<<endl;
		cout<<"	short pmVersion = "<<pm.pmVersion<<endl;
		cout<<"	short packType = "<<pm.packType<<endl;
		cout<<"	long packSize = "<<pm.packSize<<endl;
		cout<<"	Fixed hRes = "<<pm.hRes<<endl;
		cout<<"	Fixed vRes = "<<pm.vRes<<endl;
		cout<<"	short pixelType = "<<pm.pixelType<<endl;
		cout<<"	short pixelSize = "<<pm.pixelSize<<endl;
		cout<<"	short cmpCount = "<<pm.cmpCount<<endl;
		cout<<"	short cmpSize = "<<pm.cmpSize<<endl;
		cout<<"	OSType pixelFormat = "<<pm.pixelFormat<<endl;
		cout<<"	CTabHandle pmTable = "<<(void*)pm.pmTable<<endl;
		cout<<"	void* pmExt = "<<pm.pmExt<<endl;

		cout<<"};"<<endl;



		RPFrame *resp;
		resp->height=ses->info.height;
		resp->width=ses->info.width;
		int realSize = pm.cmpCount * pm.cmpSize; // Num Canals * bytes/canal
		cout<<" pmh:"<<(void*)pmh<<endl;
		int srcRow=QTGetPixMapHandleRowBytes(&pmp)/*pixels)*/, dstRow=resp->width*realSize/8;//pixelSize/8;
		cout<<" rowbytes:"<<srcRow<<endl;
		resp->dsize=resp->height*dstRow;
		resp->bitDepth=pm.pixelSize;
		cout<<" dsize:"<<resp->dsize<<" = "<<resp->height<<" * "<<dstRow<<endl;
		char buf[resp->dsize];
		cout<<"GetPixBaseAddr"<<endl;
		char *dst=buf, *src=GetPixBaseAddr(pixels);
		cout<<" decode  copy:"<<srcRow<<" rowbytes:"<<dstRow<<" real:"<<realSize<<" pixel:"<<pm.pixelSize<<endl;
		for(int i=0;i<resp->height;i++)
		{
			if(realSize<pm.pixelSize)
			{
				for(int i=0;i<resp->width;i++)
				{
					memmove(dst+(i*realSize/8), src+(i*pm.pixelSize/8)+1, realSize);
				}
			}
			else
			{
				memmove(dst, src, dstRow);
			}
			dst+=dstRow;
			src+=srcRow;
		}
		resp->data=buf;
		HUnlock((Handle)pixels);
		//resp->dsize=pm.bounds.bottom*pm.bounds.right*pm.pixelSize/8;//((*pmh)->cmpCount)*((*pmh)->cmpSize)/8;//((*pmh)->pixelSize);
//		cout<<" dsize:"<<ses->info.height<<" "<<ses->info.width<<" "<<((*pmh)->pixelSize)<<" "<<((*pmh)->cmpCount)<<" "<<((*pmh)->cmpSize)<<endl;
		cout<<" decode return  frame w:"<<resp->width<<" h:"<< resp->height<<" depth:"<< resp->bitDepth<<" size:"<<resp->dsize<<" data:"<<(void*)resp->dsize<<endl;
//		ExitMoviesOnThread(); // uops! :)
		ses->nframe++;
		sessions[a->toString()]=ses;
		return resp;
	
	}

	ExitMoviesOnThread(); // uops! :)
}

void JPEGRecodePlugin::encodeFrame(RPFrame *f, Address *a)
{
	cout<<"JPEGRecMod::encodeFrame in:"<<(void*)a<<endl;
	cout<<"JPEGRecMod::encodeFrame addr:"<<a->toString()<<endl;
	Sessio *ses=getSession(*a);
	cout<<"JPEGRecMod::encodeFrame enterMovies"<<endl;
	EnterMoviesOnThread(0); // uops! :)

	cout<<"JPEGRecMod::encodeFrame: mode:"<<ses->info.mode<<" enc:"<<RPCodecInfo::encode<<" dec:"<<RPCodecInfo::decode <<endl;
	cout<<" :"<<f->dsize<<endl;
	cout<<","<<f->serializationSize()<<endl;
	{
		ImageDescriptionHandle desc;


		OSErr err;
		ComponentInstance ce, cd;

		Handle hMovieData = NULL;//NewHandle(sizeof(char*));
		cout<<"JPEGRecMod::encode::processFrame:PtrToHand DataRef -> Hand"<<endl;
		err = PtrToHand( f->data/*&dataPtr*/, &hMovieData, f->dsize);//sizeof(PointerDataRefRecord));
		cout<<"OSErr:"<<err<<endl;
		
		Handle dataRef = NULL;
		cout<<"JPEGRecMod::encode::processFrame:PtrToHand "<<endl;
		err = PtrToHand( &hMovieData, &dataRef, sizeof(Handle) );
		cout<<"OSErr:"<<err<<endl;

	
		Rect r;
		SetRect(&r, 0, 0, f->width, f->height);
		GWorldPtr gworld=NULL;
		GDHandle currentDevice;
		GWorldPtr currentGWorld;
	
		cout<<"JPEGRecMod::encode::processFrame: OpenADefaultComponent"<<endl;
		err = OpenADefaultComponent( GraphicsExporterComponentType, 'JPEG', &ce ); 
		cout<<"OSErr:"<<err<<endl;

//		cout<<"JPEGRecMod::encode::processFrame:getGWorld"<<endl;
//		//GetGWorld(&currentGWorld, &currentDevice);
//		SGGetGWorld(ce , &currentGWorld, &currentDevice);
//		cout<<"OSErr: no error value"<<endl;

		cout<<"JPEGRecMod::encode::processFrame: NewGWorld bd:"<<f->bitDepth<<" gworld pre:"<< (void*)gworld<<endl;
//		err = NewGWorld(&gworld, f->bitDepth, &r, nil, nil, 0);
		err = QTNewGWorld(&gworld, f->bitDepth, &r, nil, nil, 0);
		cout<<"OSErr:"<<err<<endl;

		cout<<"JPEGRecMod::encode::processFrame:SetGWorld"<<" new gworld:"<< (void*)gworld<<" component:"<<(void*)ce<<endl;
		SGSetGWorld(ce, gworld, NULL);
		cout<<"OSErr: no error value"<<endl;

		cout<<"JPEGRecMod::encode::processFrame: GetGWorldPixMap"<<endl;
		PixMapHandle pixels = GetGWorldPixMap(gworld);
		//PixMapPtr pmp=GetPixBaseAddr(pixels);
		cout<<"OSErr: no error value pixels:"<<(void*)pixels<<endl;



		PixMap pm;

		pm.baseAddr=(char*)f->data;
		pm.rowBytes=f->width*f->bitDepth/8;
		pm.bounds=r;
		pm.pmVersion=0;
		pm.packType=0;
		pm.packSize=0;
		pm.hRes = 0x00480000;// By default, this value is 0x00480000 (for 72 pixels per inch).;
		pm.vRes = 0x00480000;
		pm.pixelType=RGBDirect;
		pm.pixelSize=f->bitDepth;
		pm.cmpCount=4;
		pm.cmpSize=8;
		pm.pixelFormat=k24RGBPixelFormat;
		pm.pmTable=0;
		pm.pmExt=0;

		PixMapPtr pmp=&pm;
		PixMapHandle pmh=&pmp;






/*		bmp= (BitMap *) NewPtrClear(sizeof(BitMap));
		bmp->baseAddr =f->data;
		bmp->bounds = r;
		bmp->rowBytes = f->width*f->bitDepth/8;
		cout<<"JPEGRecMod::encode::processFrame: GetGWorldPixMap "<<endl;
		CopyBits(bmp, GetPortBitMapForCopyBits(gworld), &r, &r, 0, nil);
		cout<<"OSErr: no error value"<<endl;
*/


		cout<<"JPEGRecMod::encode::processFrame: GraphicsExportSetInput"<<"Pixmap"<<endl;//"GWorld"<<endl;
		err = GraphicsExportSetInputPixmap( ce, pmh); 
		cout<<"OSErr:"<<err<<endl;

		cout<<"JPEGRecMod::encode::processFrame: TempNewHandle"<<endl;
		Handle compData= TempNewHandle( 0, &err );
		cout<<"OSErr:"<<err<<endl;


		cout<<"JPEGRecMod::encode::processFrame: GraphicsExportSetOutputHandle"<<endl;
		err = GraphicsExportSetOutputHandle( ce, compData ); 
		cout<<"OSErr:"<<err<<endl;

		//HLock(compData);
		cout<<"JPEGRecMod::encode::processFrame:GraphicsExportSetCompressionQuality "<<endl;
		err=GraphicsExportSetCompressionQuality( ce,codecHighQuality);
		cout<<"OSErr:"<<err<<endl;

		cout<<"JPEGRecMod::encode::processFrame: GraphicsExportDoExport"<<endl;
		unsigned long jpegSize;
		err = GraphicsExportDoExport( ce, &jpegSize ); 
		cout<<"OSErr:"<<err<<" jpegsize:"<<jpegSize<<endl;

	/*	cout<<"JPEGRecMod::encode::processFrame: CloseComponent"<<endl;
		err = CloseComponent( ce ); 
		cout<<"OSErr:"<<err<<endl;
*/

//		cout<<"JPEGRecMod::encode::processFrame: SetGWorld current gw:"<<(void*) currentGWorld<<" dev:"<<currentDevice<<endl;
//		SGSetGWorld(ce, currentGWorld,NULL);// currentDevice);
//		cout<<"OSErr: no error value"<<endl;


		RPFrame *resp=new RPFrame();
		resp->height=f->height;
		resp->width=f->width;
		//int srcRow=QTGetPixMapHandleRowBytes(pixels), dstRow=resp->width*pm.pixelSize/8;
		//char *dst=buf, *src=GetPixBaseAddr(pixels);
		cout<<"OSErr: dsize="<<GetHandleSize(compData)<<"  --  "<<jpegSize<<endl;
		resp->dsize=jpegSize;//GetHandleSize(compData);

		cout<<"OSErr: data:"<<(void*)compData<<" size:"<< resp->dsize <<endl;
		resp->data=*compData;
	//	ExitMoviesOnThread(); // uops! :)
		
		cout<<"encoded push:back size: "<<ses->encodedFrames.size()<<endl;
		ses->encodedFrames.push_back(resp);
		cout<<"encoded push:back signal: "<<ses->encodedFrames.size()<<endl;
		ses->outCond.signal();
		cout<<"encoded end "<<endl;
	
	}

	ExitMoviesOnThread(); // uops! :)
}

RPFrame *JPEGRecodePlugin::getFrame(Address *a)
{
	cout<<"JPEGRecMod::getFrame"<<endl;
	Sessio *ses=getSession(*a);
	cout<<"JPEGRecMod::getFrame lock size:"<<ses->encodedFrames.size()<<endl;
	ses->outCond.lock(); 
	cout<<"JPEGRecMod::getFrame locked size:"<<ses->encodedFrames.size()<<endl;
	while(ses->encodedFrames.size()==0)
	{
		cout<<"JPEGRecMod::getFrame waiting size:"<<ses->encodedFrames.size()<<endl;
		ses->outCond.wait();
	}
	RPFrame *f=ses->encodedFrames.front();
	ses->encodedFrames.pop_front();
	ses->outCond.unlock();

	cout<<"JPEGRecMod::/getFrame"<<endl;

	return f;
}
