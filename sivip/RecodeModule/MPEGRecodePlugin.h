#pragma once
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginRecode.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/SocketTCP.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <map>
#include <list>
#include <iostream>
#ifdef __cplusplus
extern "C" {
#endif
#include <Quicktime/Movies.h>
#include <Quicktime/MoviesFormat.h>
#include <Quicktime/QuickTimeComponents.h>
#include <Quicktime/QuickTime.h>


#ifdef __cplusplus
}
#endif
using namespace std;

class MPEGRecodePlugin: public PluginRecode
{
		class Sessio: public Thread
		{
			public:
				// info MPEGRecPlugin Sessió
				RPCodecInfo info;
				list<RPFrame*> inBuf, outBuf;
				Condition inCond, outCond;
				int encodedFrames, decodedFrames;
				
				//info QuickTime
				ICMCompressionSessionRef compressionSession;
				Handle dataRef;
				Handle hMovieData;
				Movie movie;
				Track track;
				Media media;

				Sessio(RPCodecInfo info);
			//	Sessio();
				void init();
				void addFrame(RPFrame *f);
				RPFrame *getFrame();
				void* execute(int id, void* param);
				static OSStatus compressionCallback(void *encodedFrameOutputRefCon, ICMCompressionSessionRef session,
									OSStatus err, ICMEncodedFrameRef encodedFrame, void *reserved);
		};

		map<string, Sessio*> sessions;

		
	protected:
		Sessio* getSession(Address a);
		
	public:
		DBGateway db;
		MPEGRecodePlugin(string file);
		~MPEGRecodePlugin();


		virtual void startSession(RPCodecInfo id, Address *a);
		virtual void endSession(RPCodecInfo id, Address *a);

		virtual RPFrame *decodeFrame(RPFrame *f, Address *a);
		virtual void encodeFrame(RPFrame *f, Address *a);
		virtual RPFrame *getFrame(Address *a);
};

