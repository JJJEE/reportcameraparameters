#include <Plugins/GestorRecodeExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <MPEGRecodePlugin.h>
#include <RecodeModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

/*
MPEGRecodePlugin::Sessio::Sessio()
{
	init();
}
*/
MPEGRecodePlugin::Sessio::Sessio(RPCodecInfo info):info(info)
{
	cout<<"MPEGRecodePlugin::Sessio new Session"<<endl;
	init();
	cout<<"MPEGRecodePlugin::Sessio start"<<endl;
	start();
	cout<<"MPEGRecodePlugin::Sessio started"<<endl;
}

void MPEGRecodePlugin::Sessio::init()
{
	ICMCompressionSessionOptionsRef compressionOptions;
	OSErr err;
	encodedFrames=0;

	cout<<"	MPEGRecMod::init:newMovie"<<endl;
	movie=NewMovie(0);
	err=GetMoviesError();
	cout<<"	OSErr:"<<err<<endl;

	cout<<"	MPEGRecMod::init:ICMCompressionSessionOptionsCreate"<<endl;
	err = ICMCompressionSessionOptionsCreate(NULL, &compressionOptions);
	cout<<"	OSErr:"<<err<<endl;


	cout<<"	MPEGRecMod::  ----- opcions ------"<<endl;
	err = ICMCompressionSessionOptionsSetAllowTemporalCompression(compressionOptions, true);
	cout<<"	ICMCompressionSessionOptionsSetAllowTemporalCompression OSErr:"<<err<<endl;
	err = ICMCompressionSessionOptionsSetAllowFrameReordering(compressionOptions, true);
	cout<<"	ICMCompressionSessionOptionsSetAllowFrameReordering OSErr:"<<err<<endl;
	// Limitem el key frame rate a 60 segons a 24 fps
	err = ICMCompressionSessionOptionsSetMaxKeyFrameInterval(compressionOptions, 60*24);
	cout<<"	ICMCompressionSessionOptionsSetMaxKeyFrameInterval OSErr:"<<err<<endl;
	// Deixem que el compressor faci una mica el q li roti
	err = ICMCompressionSessionOptionsSetAllowFrameTimeChanges(compressionOptions, true);
	cout<<"	ICMCompressionSessionOptionsSetAllowFrameTimeChanges OSErr:"<<err<<endl;
	// Volem la durada de cada frame especificada
	err = ICMCompressionSessionOptionsSetDurationsNeeded(compressionOptions, true);
	cout<<"	ICMCompressionSessionOptionsSetDurationsNeeded OSErr:"<<err<<endl;
	int avgDataRate=128*1024; // 128 kB/s
	err = ICMCompressionSessionOptionsSetProperty(compressionOptions, kQTPropertyClass_ICMCompressionSessionOptions,
			kICMCompressionSessionOptionsPropertyID_AverageDataRate, sizeof(avgDataRate), &avgDataRate);
	cout<<"	ICMCompressionSessionOptionsSetProperty OSErr:"<<err<<endl;


	ICMEncodedFrameOutputRecord outputRecord;
	outputRecord.encodedFrameOutputCallback = MPEGRecodePlugin::Sessio::compressionCallback;
	outputRecord.encodedFrameOutputRefCon = this;
	outputRecord.frameDataAllocator = NULL;


	cout<<"	MPEGRecMod::decode::ICMCompressionSessionCreate("<<(void*)NULL<<", "<<info.width<<", "<<info.height<<", "<<kMPEG4VisualCodecType<<", "<<600<<", "<<compressionOptions<<", "<<(void*)NULL<<", "<<(void*)&outputRecord<<", "<<(void*)&compressionSession<<")"<<endl;
	err=ICMCompressionSessionCreate(NULL, info.width, info.height, kMPEG4VisualCodecType, 600/*timescale*/, compressionOptions, NULL, &outputRecord, &compressionSession);
	cout<<"	OSErr:"<<err<<endl;


	// Creem la pista
	cout<<"	MPEGRecMod::decode::NewMovieTrack:"<<endl;
	track=NewMovieTrack(movie, info.width<<16, info.height<<16, 0);
	err=GetMoviesError();
	cout<<"	OSErr:"<<err<<endl;

	// Creem la media
	OSType    dataRefType;
	dataRef=NULL;
	hMovieData = NewHandle(0);

	// Construct the Handle data reference
	err = PtrToHand( &hMovieData, &dataRef, sizeof(Handle));

	cout<<"	MPEGRecMod::decode::NewTrackMedia:"<<endl;
	media=NewTrackMedia(track, VideoMediaType, 600,  dataRef,
			HandleDataHandlerSubType);
	err=GetMoviesError();
	cout<<"	OSErr:"<<err<<endl;

	cout<<"	MPEGRecMod::decode::BeginMediaEdits:"<<endl;
	err=BeginMediaEdits(media);
	cout<<"	OSErr:"<<err<<endl;

}

void* MPEGRecodePlugin::Sessio::execute(int id, void* param)
{
	cout<<"----MPEGRecMod::execute start---"<<endl;
	if(info.mode==RPCodecInfo::encode)
	{
		cout<<"----MPEGRecMod::execute encode"<<endl;
		EnterMoviesOnThread(0); // uops! :)
		RPFrame *f;
		OSErr err;

		for(int i=0;;i++)  // TODO: endSession ha de petar el thread :P
		{
			cout<<"	MPEGRecMod::execute lock"<<endl;
			inCond.lock(); 
			cout<<"	MPEGRecMod::execute locked"<<endl;
			while(inBuf.size()==0)
			{
				cout<<"	MPEGRecMod::execute lock wait buf:"<<inBuf.size()<<" ses:"<<(void*)this<<" inBuf:"<<(void*)&inBuf<<endl;
				inCond.wait();
				cout<<"	MPEGRecMod::execute lock waited sz:"<<inBuf.size()<<endl;
			}
			f=inBuf.front();
			inBuf.pop_front();
			inCond.unlock();


			cout<<"	MPEGRecMod::Sessio::execute size:"<<f->dsize<<" w:"<< f->width<<" h:"<< f->height<<" addr:"<< (void*)f->data<<endl;
			CVPixelBufferRef pixelBufferOut;
			cout<<"	MPEGRecMod::Sessio::execute CVPixelBufferCreateWithBytes "<<endl;
			CVReturn cverr=CVPixelBufferCreateWithBytes (    
					NULL,    
					f->width,   
					f->height,   
					k24RGBPixelFormat,//OSType pixelFormatType, 
					f->data,    
					f->width,   
					NULL,//CVPixelBufferReleaseBytesCallback releaseCallback,    
					NULL,//res.data,//void *releaseRefCon, -- no fa falta, tb et passes les dades :P
					NULL,//CFDictionaryRef pixelBufferAttributes,    
					&pixelBufferOut
					);
			cout<<"	CVRet:"<<cverr<<endl;


			cout<<"	MPEGRecMod::Sessio::execute ICMCompressionSessionEncodeFrame"<<endl;
			err=ICMCompressionSessionEncodeFrame(compressionSession, pixelBufferOut, i*(600/15), 600/15, kICMValidTime_DisplayTimeStampIsValid|kICMValidTime_DisplayDurationIsValid, NULL, NULL, NULL);
			cout<<"	OSErr:"<<err<<endl;
			
			delete f;
		}
		ExitMoviesOnThread(); // uops! :)
	}
	else
	{
		/*
		   EnterMoviesOnThread(0); // uops! :)
		   ImageSequence seqID;
		   ImageDescriptionHandle idh = (ImageDescriptionHandle)NewHandle(0);
		   PixMapHandle pmh = GetGWorldPixMap(srcGWorld);

		   CodecQ codecQual = codecNormalQuality;
		   CodecType codecType = kRawCodecType;
		   CompressorComponent cc = bestSpeedCodec;

		   CompressSequenceBegin(&seqID, pmh, NULL, &srcRect, NULL, 24,codecType, cc,codecQual, 0, NULL, 0, 0L, idh);


		   long maxSize;
		   GetMaxCompressionSize(pmh, &srcRect, 24, codecQual, codecType, cc, &maxSize);
		   Ptr dataPtr = NewPtr(maxSize);

		//loop through all frames
		RPFrame f;
		OSErr err;
		for(int i=0;;i++)  // TODO: endSession ha de petar el thread :P
		{
		cout<<"	MPEGRecMod::execute lock"<<endl;
		inCond.lock(); 
		while(inBuf.size()==0)
		{
		cout<<"	MPEGRecMod::execute lock wait buf:"<<inBuf.size()<<" ses:"<<(void*)this<<" inBuf:"<<(void*)&inBuf<<endl;
		inCond.wait();
		}
		f=inBuf.front();
		inBuf.pop_front();
		inCond.unlock();


		//			curMovieTime = frameNum*dur/(frameCount-1);
		short flags = nextTimeMediaSample;
		OSType whichMediaType = VideoMediaType;


		if (frameNum == 0)
		flags |= nextTimeEdgeOK;


		GetMovieNextInterestingTime(srcMovie,flags,1,&whichMediaType,curMovieTim e,0,&curMovieTime,&duration);

		SetMovieTimeValue(srcMovie,curMovieTime);
		MoviesTask(srcMovie,0);

		//get the pixels out of the GWorld and "compress" them into a buffer
		pmh = GetGWorldPixMap(srcGWorld);
		long dataSize;
		OSErr e = CompressSequenceFrame(seqID, pmh, &srcRect, 0L, dataPtr, &dataSize, NULL, NULL);
		if (e != 0) printf("Compression Error %d at frame %d\n", e, frameNum);

		//dataPtr is a char * that now holds the frame data
		}

		ExitMoviesOnThread(); // uops! :)
		*/
/*
		   CodecQ codecQual = codecNormalQuality;
		   CodecType codecType = kRawCodecType;
		   CompressorComponent cc = bestSpeedCodec;

		   CompressSequenceBegin(&seqID, pmh, NULL, &srcRect, NULL, 24,codecType, cc,codecQual, 0, NULL, 0, 0L, idh);


		   long maxSize;
		   GetMaxCompressionSize(pmh, &srcRect, 24, codecQual, codecType, cc, &maxSize);
		   Ptr dataPtr = NewPtr(maxSize);

		//loop through all frames
		RPFrame f;
		OSErr err;
		for(int i=0;;i++)  // TODO: endSession ha de petar el thread :P
		{
		cout<<"	MPEGRecMod::execute lock"<<endl;
		inCond.lock(); 
		while(inBuf.size()==0)
		{
		cout<<"	MPEGRecMod::execute lock wait buf:"<<inBuf.size()<<" ses:"<<(void*)this<<" inBuf:"<<(void*)&inBuf<<endl;
		inCond.wait();
		}
		f=inBuf.front();
		inBuf.pop_front();
		inCond.unlock();


		//			curMovieTime = frameNum*dur/(frameCount-1);
		short flags = nextTimeMediaSample;
		OSType whichMediaType = VideoMediaType;


		if (frameNum == 0)
		flags |= nextTimeEdgeOK;


		GetMovieNextInterestingTime(srcMovie,flags,1,&whichMediaType,curMovieTim e,0,&curMovieTime,&duration);

		SetMovieTimeValue(srcMovie,curMovieTime);
		MoviesTask(srcMovie,0);

		//get the pixels out of the GWorld and "compress" them into a buffer
		pmh = GetGWorldPixMap(srcGWorld);
		long dataSize;
		OSErr e = CompressSequenceFrame(seqID, pmh, &srcRect, 0L, dataPtr, &dataSize, NULL, NULL);
		if (e != 0) printf("Compression Error %d at frame %d\n", e, frameNum);

		//dataPtr is a char * that now holds the frame data
		}

*/
/*		
		EnterMoviesOnThread(0); // uops! :)
		ImageDescriptionHandle idh = (ImageDescriptionHandle)NewHandle(0);
		GWorldPtr srcGWorld;
		PixMapHandle pmh = GetGWorldPixMap(srcGWorld);
		ComponentInstance mc;
		ComponentResult res;
		OSErr err;

		cout<<"MPEGRecMod::OpenADefaultComponent"<<endl;
		err = OpenADefaultComponent( GraphicsExporterComponentType, 'MPEG', &mc ); 
		cout<<"OSErr:"<<err<<endl;
		SGChannel chan; 
		cout<<"MPEGRecMod::newChannel"<<endl;
		res = SGNewChannel(mc, VideoMediaType, &chan);
		cout<<"CompRes:"<<res<<endl;
		cout<<"MPEGRecMod::ChannelUssage"<<endl;
		res = SGSetChannelUsage(mc, seqGrabPreview);
		cout<<"CompRes:"<<res<<endl;
		cout<<"MPEGRecMod::VideoCompressor"<<endl;
		res = SGSetVideoCompressorType(mc, kRawCodecType);
		cout<<"CompRes:"<<res<<endl;
*/		
		cout<<"----MPEGRecMod::execute decode"<<endl;
		EnterMoviesOnThread(0); // uops! :)
		OSErr err;
		RPFrame *f;

		   //ImageSequence seqID;
		   ImageDescriptionHandle idh = (ImageDescriptionHandle)NewHandle(0);
		   //PixMapHandle pmh = GetGWorldPixMap(srcGWorld);
		   {
			   cout<<"	MPEGRecMod::execute lock"<<endl;
			   inCond.lock(); 
			cout<<"	MPEGRecMod::execute locked"<<endl;
			   while(inBuf.size()==0)
			   {
				   cout<<"	MPEGRecMod::execute lock wait buf:"<<inBuf.size()<<" ses:"<<(void*)this<<" inBuf:"<<(void*)&inBuf<<endl;
				   inCond.wait();
				cout<<"	MPEGRecMod::execute lock waited sz:"<<inBuf.size()<<endl;
			   }
			   f=inBuf.front();
			   inBuf.pop_front();
			   inCond.unlock();
		   }
			DecompressorComponent mc;
		   {
			   ComponentDescription cd;
			   cd.componentType = MovieImportType;
			   cd.componentSubType = kQTFileTypeMP4;
			   cd.componentManufacturer = 0;
			   cd.componentFlags = 0;
			   cd.componentFlagsMask = 0;

			   cout<<"MPRP:: FindNextComp"<<endl;
			   mc=FindNextComponent(0, &cd);
			   cout<<"MPRP:: FindNextComp mc:"<<mc<<endl;
		   }

		   CodecQ codecQual = codecNormalQuality;
		   CodecType codecType = kRawCodecType;
		   CompressorComponent cc = bestSpeedCodec;
			ImageSequence seqID;
			ImageDescriptionHandle      mySampleDesc =NULL;
			{
				// create a new sample description

			   cout<<"MPRP:: ImgDescHandle"<<endl;
				mySampleDesc =(ImageDescriptionHandle)
					NewHandleClear(sizeof(ImageDescription));
				if (mySampleDesc ==NULL)
					return(NULL);

				// fill in the fields of the sample description
				(**mySampleDesc).cType =kMPEG4VisualCodecType;//kVideoCodeType;//kRawCodecType;//theEffectType;
				(**mySampleDesc).idSize =sizeof(ImageDescription);
				(**mySampleDesc).hRes =72L << 16;
				(**mySampleDesc).vRes =72L << 16;
				(**mySampleDesc).frameCount =1;
				(**mySampleDesc).depth =0;
				(**mySampleDesc).clutID =-1;

				(**mySampleDesc).vendor =0;//kAppleManufacturer;
				(**mySampleDesc).temporalQuality =codecNormalQuality;
				(**mySampleDesc).spatialQuality =codecNormalQuality;
				(**mySampleDesc).width =f->width;
				(**mySampleDesc).height =f->height;
			   cout<<"MPRP:: ImgDescHandle :"<<mySampleDesc<<endl;
			}
			CGrafPtr savePort, newPort;
			{
			   cout<<"MPRP:: getPort"<<endl;
			  GetPort( &savePort );
			   cout<<"MPRP:: getPort :"<<savePort<<endl;
			  // newPort=(CGrafPtr )NewPtr(sizeof(struct CGrafPort)); 
			 // OpenCPort(newPort);
			   cout<<"MPRP:: newPort"<<endl;
			  newPort=CreateNewPort();
			   cout<<"MPRP:: newPort :"<<newPort<<endl;
			   cout<<"MPRP:: MacSetPort"<<endl;
			   MacSetPort( newPort );
			   cout<<"MPRP:: /MacSetPort"<<endl;
			}
		   cout<<"MPRP:: DecpSeqBgn"<<endl;
		   err=DecompressSequenceBeginS(&seqID, mySampleDesc, (char*)f->data, f->dsize, newPort, NULL, NULL, NULL, transparent, NULL, codecFlagUseImageBuffer, codecNormalQuality, mc);
		   cout<<"MPRP:: DecpSeqBgn err:"<<err<<endl;

		   for(int i=0;;i++)  // TODO: endSession ha de petar el thread :P
		   {
			   ICMCompletionProcRecord cpr;
			   cpr.completionProc;
			   cout<<"MPRP:: DcmpSeqFrame"<<endl;
			   err=DecompressSequenceFrameWhen(seqID, (char*)f->data, f->dsize, 0, 0, NULL, NULL);
			   cout<<"MPRP:: DcmpSeqFrame OSErr:"<<err<<endl;

			   cout<<"MPRP:: GetPortPixMap"<<endl;
			   PixMapHandle pmh=GetPortPixMap(newPort);

				PixMapPtr pmp=*pmh;
				PixMap pm=*pmp;
			   cout<<"MPRP:: GetPortPixMap hand:"<<pmh<<"  ptr:"<<pmp<<endl;

				RPFrame *resp;
				resp->height=f->height;
				resp->width=f->width;
				int realSize = pm.cmpCount * pm.cmpSize; // Num Canals * bytes/canal
				cout<<" pmh:"<<(void*)pmh<<endl;
				int srcRow=QTGetPixMapHandleRowBytes(&pmp)/*pixels)*/, dstRow=resp->width*realSize/8;//pixelSize/8;
				cout<<" rowbytes:"<<srcRow<<endl;
				resp->dsize=resp->height*dstRow;
				resp->bitDepth=pm.pixelSize;
				cout<<" dsize:"<<resp->dsize<<" = "<<resp->height<<" * "<<dstRow<<endl;
				char buf[resp->dsize];
				cout<<"GetPixBaseAddr"<<endl;
				char *dst=buf, *src=GetPixBaseAddr(pmh);//pixels);
				cout<<" decode  copy:"<<srcRow<<" rowbytes:"<<dstRow<<" real:"<<realSize<<" pixel:"<<pm.pixelSize<<endl;
				for(int i=0;i<resp->height;i++)
				{
					if(realSize<pm.pixelSize)
					{
						for(int i=0;i<resp->width;i++)
						{
							memmove(dst+(i*realSize/8), src+(i*pm.pixelSize/8)+1, realSize);
						}
					}
					else
					{
						memmove(dst, src, dstRow);
					}
					dst+=dstRow;
					src+=srcRow;
				}
				resp->data=buf;
				decodedFrames++;
				outBuf.push_back(resp);
				outCond.signal(); 

			   cout<<"	MPEGRecMod::execute lock"<<endl;
			   inCond.lock(); 
			   while(inBuf.size()==0)
			   {
				   cout<<"	MPEGRecMod::execute lock wait buf:"<<inBuf.size()<<" ses:"<<(void*)this<<" inBuf:"<<(void*)&inBuf<<endl;
				   inCond.wait();
			   }
			   f=inBuf.front();
			   inBuf.pop_front();
			   inCond.unlock();
		   }

		ExitMoviesOnThread(); // uops! :)
		
	}
}

OSStatus MPEGRecodePlugin::Sessio::compressionCallback(void *encodedFrameOutputRefCon, ICMCompressionSessionRef session, OSStatus err, ICMEncodedFrameRef encodedFrame, void *reserved)
{
	MPEGRecodePlugin::Sessio *ses=(MPEGRecodePlugin::Sessio *)encodedFrameOutputRefCon;

	ImageDescriptionHandle imageDesc = NULL;

	//	cout<<"--Compression Callback: err:"<<err<<"media:"<<(void*)media<<endl;
	cout<<"--Compression Callback:: ICMEncodedFrameGetImageDescriptioni "<<endl;
	err = ICMEncodedFrameGetImageDescription(encodedFrame, &imageDesc);
	cout<<"--OSErr:"<<err<<endl;
	cout<<" hMovieData:"<<GetHandleSize(ses->hMovieData)<<" dataRef:"<<GetHandleSize(ses->dataRef)<<" hMovieData:"<<(ses->hMovieData)<<" dataRef:"<<(ses->dataRef)<<endl;
	RPFrame *f=new RPFrame();

//	if(ses->encodedFrames==0)
//	{
		/*		OSErr ConvertMovieToDataRef (
				ses->movie,//Movie                theMovie,
				NULL,//Track                onlyTrack,
				movieData,//Handle               dataRef,
				PointerDataHandlerSubType, //OSType               dataRefType,
				OSType               fileType,
				'TVOD',//OSType               creator,
				0,//long                 flags,
				ComponentInstance    userComp );
				*/
	/*	Handle movieData;
		Handle myHandle = NewHandleClear(0);
		DataReferenceRecord    myDataRefRec;

		myDataRefRec.dataRefType = HandleDataHandlerSubType;
		myDataRefRec.dataRef = NewHandle(sizeof(Handle));
		*((Handle *)*(myDataRefRec.dataRef)) = myHandle;
		FSSpec *myFile;
		myFile = (FSSpec *)&myDataRefRec;

		cout <<" FlattenMovieData"<<endl;
		Movie aux= FlattenMovieData (
				ses->movie,	//Movie           theMovie,
				flattenAddMovieToDataFork | flattenFSSpecPtrIsDataRefRecordPtr | flattenForceMovieResourceBeforeMovieData ,	//long            movieFlattenFlags,
				myFile,	//const FSSpec    *theFile,
				'TVOD', 	//OSType          creator,
				smSystemScript,//ScriptCode      scriptTag,
				0);//createMovieFileDeleteCurFile, long            createMovieFileFlags );
		err=GetMoviesError();
		cout<<" OSErr:"<<err<<endl;

	cout<<" myHandle:"<<GetHandleSize(myHandle)<<" dataRef:"<<GetHandleSize(myDataRefRec.dataRef)<<" myHandle:"<<(void*)(myHandle)<<" dataRef:"<<(void*)(myDataRefRec.dataRef)<<endl;

		f.dsize = ICMEncodedFrameGetDataSize(encodedFrame) + GetHandleSize(myHandle);
		char *data=new char[f.dsize];
		cout <<" memcpy 1"<<endl;
		memcpy(data, myHandle, GetHandleSize(myHandle));

				FILE *fd=fopen((string("header.mov")).c_str(),"w+b");
				fwrite(data, 1, GetHandleSize(myHandle),fd);
				fclose(fd);

		memcpy(data+GetHandleSize(myHandle), ICMEncodedFrameGetDataPtr(encodedFrame), ICMEncodedFrameGetDataSize(encodedFrame));
		f.data=data;

				fd=fopen((string("frame.mov")).c_str(),"w+b");
				fwrite(f.data+GetHandleSize(myHandle), 1, ICMEncodedFrameGetDataSize(encodedFrame), fd);
				fclose(fd);

				fd=fopen((string("complert.mov")).c_str(),"w+b");
				fwrite(f.data, 1, f.dsize, fd);
				fclose(fd);

		DisposeHandle(myHandle);
		DisposeMovie(aux);

		{
		cout<<"	MPEGRecMod::decode::processFrame:AddMediaSample"<<endl;
			err = AddMediaSample2(ses->media,
					ICMEncodedFrameGetDataPtr(encodedFrame),
					ICMEncodedFrameGetDataSize(encodedFrame),
					600/15,
					ICMEncodedFrameGetDisplayOffset(encodedFrame),
					(SampleDescriptionHandle)imageDesc,
					1,
					ICMEncodedFrameGetMediaSampleFlags(encodedFrame),
					NULL);
		cout<<"	OSErr:"<<endl<<endl;
		cout<<"	MPEGRecMod::decode::processFrame:ICMCompressionSessionCompleteFrames"<<endl;
		err=ICMCompressionSessionCompleteFrames(ses->compressionSession, true, 0, 0);
		cout<<"	OSErr:"<<endl<<endl;
		cout<<"	MPEGRecMod::decode::processFrame:ICMCompressionSessionRelease"<<endl;
		ICMCompressionSessionRelease(ses->compressionSession);
		cout<<"	no OSErr:"<<endl<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:EndMediaEdits"<<endl;
		err=EndMediaEdits(ses->media);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:ExtendMediaDecodeDurationToDisplayEndTime"<<endl;
		err=ExtendMediaDecodeDurationToDisplayEndTime(ses->media, nil);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:InsertMediaIntoTrack"<<endl;
		err=InsertMediaIntoTrack(ses->track, GetTrackDuration(ses->track), 0, GetMediaDisplayDuration(ses->media), fixed1);
		cout<<"OSErr:"<<err<<endl;


			Handle myHandle = NewHandleClear(0);
			DataReferenceRecord    myDataRefRec;

			myDataRefRec.dataRefType = HandleDataHandlerSubType;
			myDataRefRec.dataRef = NewHandle(sizeof(Handle));
			*((Handle *)*(myDataRefRec.dataRef)) = myHandle;
			FSSpec *myFile;
			myFile = (FSSpec *)&myDataRefRec;

			cout <<" FlattenMovieData 2"<<endl;
			Movie aux= FlattenMovieData (
				ses->movie,	//Movie           theMovie,
				flattenAddMovieToDataFork | flattenFSSpecPtrIsDataRefRecordPtr | flattenForceMovieResourceBeforeMovieData ,	//long            movieFlattenFlags,
				myFile,	//const FSSpec    *theFile,
				'TVOD', 	//OSType          creator,
				smSystemScript,//ScriptCode      scriptTag,
				0);//createMovieFileDeleteCurFile, long            createMovieFileFlags );
			err=GetMoviesError();
			cout<<" OSErr:"<<err<<endl;

			cout<<" myHandle:"<<GetHandleSize(myHandle)<<" dataRef:"<<GetHandleSize(myDataRefRec.dataRef)<<" myHandle:"<<(void*)(myHandle)<<" dataRef:"<<(void*)(myDataRefRec.dataRef)<<endl;

		f.dsize = ICMEncodedFrameGetDataSize(encodedFrame) + GetHandleSize(myHandle);
		char *data=new char[f.dsize];
		cout <<" memcpy 1"<<endl;
		memcpy(data, myHandle, GetHandleSize(myHandle));

				FILE *fd=fopen((string("complert_addSample.mov")).c_str(),"w+b");
				fwrite(data, 1, GetHandleSize(myHandle),fd);
				fclose(fd);

		DisposeHandle(myHandle);
		DisposeMovie(aux);

		}

	}
	else*/
	{
cout<<"	Image Description"<<endl;
cout<<"	long idSize:"<<dec<<(*imageDesc)->idSize<<"	 total size of ImageDescription including extra data ( CLUTs and other per sequence data ) :"<<endl;
cout<<"	CodecType cType:"<<*((char*)&((*imageDesc)->cType))<<*(((char*)&((*imageDesc)->cType))+1)<<*(((char*)&((*imageDesc)->cType))+2)<<*(((char*)&((*imageDesc)->cType))+3)<<"	 what kind of codec compressed this data :"<<endl;
cout<<"	long resvd1:"<<(*imageDesc)->resvd1<<"	 reserved for Apple use :"<<endl;
cout<<"	short resvd2:"<<(*imageDesc)->resvd2<<"	 reserved for Apple use :"<<endl;
cout<<"	short dataRefIndex:"<<(*imageDesc)->dataRefIndex<<"	 set to zero  :"<<endl;
cout<<"	short version:"<<(*imageDesc)->version<<"	 which version is this data :"<<endl;
cout<<"	short revisionLevel:"<<(*imageDesc)->revisionLevel<<"	 what version of that codec did this :"<<endl;
cout<<"	long vendor:"<<*((char*)&((*imageDesc)->vendor))<<*(((char*)&((*imageDesc)->vendor))+1)<<*(((char*)&((*imageDesc)->vendor))+2)<<*(((char*)&((*imageDesc)->vendor))+3)<<"	 whose  codec compressed this data :"<<endl;
cout<<"	CodecQ temporalQuality:"<<(*imageDesc)->temporalQuality<<"	 what was the temporal quality factor  :"<<endl;
cout<<"	CodecQ spatialQuality:"<<(*imageDesc)->spatialQuality<<"	 what was the spatial quality factor :"<<endl;
cout<<"	short width:"<<(*imageDesc)->width<<"	 how many pixels wide is this data :"<<endl;
cout<<"	short height:"<<(*imageDesc)->height<<"	 how many pixels high is this data :"<<endl;
cout<<"	Fixed hRes:"<<(*imageDesc)->hRes<<" >>16:"<< (((*imageDesc)->hRes)>>16) <<"	 horizontal resolution :"<<endl;
cout<<"	Fixed vRes:"<<(*imageDesc)->vRes<<" >>16:"<< (((*imageDesc)->vRes)>>16) <<"	 vertical resolution :"<<endl;
cout<<"	long dataSize:"<<(*imageDesc)->dataSize<<"	 if known, the size of data for this image descriptor :"<<endl;
cout<<"	short frameCount:"<<(*imageDesc)->frameCount<<"	 number of frames this description applies to :"<<endl;
cout<<"	Str31 name:"<<string((char*)&((*imageDesc)->name), 31)<<"--  name of codec ( in case not installed )  :"<<endl;
cout<<"	short depth:"<<(*imageDesc)->depth<<"	 what depth is this data (1-32) or ( 33-40 grayscale ) :"<<endl;
cout<<"	short clutID:"<<(*imageDesc)->clutID<<"	 clut id or if 0 clut follows  or -1 if no clut :"<<endl;
cout<<"	sync sample:"<< ICMEncodedFrameGetMediaSampleFlags(encodedFrame)<<"	sync sample (==key frame?) if ==0"<<endl;
cout<<"	size sample:"<< ICMEncodedFrameGetDataSize(encodedFrame)<<"	sync sample (==key frame?) if ==0"<<endl;



		f->data = (char*)ICMEncodedFrameGetDataPtr(encodedFrame);
		f->dsize = ICMEncodedFrameGetDataSize(encodedFrame);
	}

	if(ICMEncodedFrameGetMediaSampleFlags(encodedFrame)==mediaSampleNotSync)
		f->isKey=false;
	else
		f->isKey=true;
	f->height = ses->info.height;
	f->width = ses->info.width;
	f->bitDepth = 24;

	ses->encodedFrames++;
	ses->outBuf.push_back(f);
	ses->outCond.signal(); 


/*	cout<<"--Compression Callback::AddMediaSample2"<<endl;
	err = AddMediaSample2(*media,
			ICMEncodedFrameGetDataPtr(encodedFrame),
			ICMEncodedFrameGetDataSize(encodedFrame),
			600/15,
			ICMEncodedFrameGetDisplayOffset(encodedFrame),
			(SampleDescriptionHandle)imageDesc,
			1,
			ICMEncodedFrameGetMediaSampleFlags(encodedFrame),
			NULL);
	cout<<"--OSErr:"<<err<<endl;
*/

	// Equivalent al comentari i mes senzill
	//	err=AddMediaSampleFromEncodedFrame(output->media, encodedFrame, nil);
	//	exitIfError(err, "AddMediaSampleFromEncodedFrame error");

	return err;


}


void MPEGRecodePlugin::Sessio::addFrame(RPFrame *f)
{
//	cout<<"MPEGRecMod::addFrame buf:"<<inBuf.size()<<" ses:"<<(void*)this<<" inBuf:"<<(void*)&inBuf<<endl;

	inBuf.push_back(f);
//	cout<<"MPEGRecMod::addFrame signal buf:"<<inBuf.size()<<endl;
	inCond.signal(); 
//	cout<<"MPEGRecMod::/addFrame buf:"<<inBuf.size()<<endl;
}

RPFrame *MPEGRecodePlugin::Sessio::getFrame()
{
//	cout<<"MPEGRecMod::getFrame"<<endl;
	outCond.lock(); 
	while(outBuf.size()==0)
	{
		outCond.wait();
	}
	RPFrame *f=outBuf.front();
	outBuf.pop_front();
	outCond.unlock();

//	cout<<"MPEGRecMod::/getFrame"<<endl;
	return f;
}


MPEGRecodePlugin::MPEGRecodePlugin(string file):PluginRecode(file), db(address, type, cn)
{

	cout<<"init:"<<file<< endl;
	FILE *f=fopen(file.c_str(),"rb");

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char buf2[len];
	fread(buf2,len,1,f);
	fclose(f);

	XML *conf=xmlParser::parse(buf2);
	conf->getNodeData("[0]/ModelConfigFile");

	delete conf;
	EnterMovies(); //Inicialitzacio Quicktime
	cout<<"----------plugin inicialitzat----------"<<endl;
}

MPEGRecodePlugin::~MPEGRecodePlugin()
{
}

MPEGRecodePlugin::Sessio* MPEGRecodePlugin::getSession(Address a)
{
	map<string,Sessio*>::iterator i=sessions.find(a.toString());
	
	if (i==sessions.end())
	{
		throw (RPSessionNotStablishedException(0, string("Session no establerta")));
	}
	return i->second;
}

void MPEGRecodePlugin::startSession(RPCodecInfo id, Address *a)
{	
	map<string,Sessio*>::iterator itSes=sessions.find(a->toString());
	
	cout<<endl<<"MPEGRecMod::StartSession:"<<id.name<<" mode:"<<id.mode<<" enc:"<<RPCodecInfo::encode<<" dec:"<<RPCodecInfo::decode <<endl;
	
	if(itSes!=sessions.end())
	{
		throw(RPSessionAlreadyStablishedException(1, " Sessio ja establerta"));
	}

	cout<<"MPEGRecMod::StartSession new session" <<endl;
	Sessio *ses=new Sessio(id);
	cout<<"MPEGRecMod::StartSession /new session :"<<(void*) ses <<endl<<endl;
	sessions[a->toString()]= ses;

}

void MPEGRecodePlugin::endSession(RPCodecInfo id, Address *a)
{
	map<string,Sessio*>::iterator i=sessions.find(a->toString());
	
	cout<<"MPEGRecMod::EndSession:"<<id.name<<endl;
	if(i==sessions.end())
	{
		throw(RPSessionNotStablishedException(0, " Sessio no establerta"));
	}
	
	delete i->second;
	sessions.erase(i);
}

void MPEGRecodePlugin::encodeFrame(RPFrame *f, Address *a)
{
	cout<<endl<<"MPEGRecMod::processFrame encodeFrame"<<endl;
	Sessio *ses=getSession(*a);

	ses->addFrame(f);
	cout<<"MPEGRecMod::processFrame /encodeFrame"<<endl;
}

RPFrame *MPEGRecodePlugin::decodeFrame(RPFrame *f, Address *a)
{
	Sessio *ses=getSession(*a);

	//RPFrame res=f;
	cout<<"MPEGRecodePlugin::decFrame: addFrame"<<endl;
	ses->addFrame(f);
	cout<<"MPEGRecodePlugin::decFrame: getFrame"<<endl;
	RPFrame *res=ses->getFrame();

	cout<<"MPEGRecMod::decodeFrame  return"<<endl;
	return res;
}

RPFrame *MPEGRecodePlugin::getFrame(Address *a)
{
	cout<<"MPEGRecMod::getFrame"<<endl;
	Sessio *ses=getSession(*a);

	RPFrame *res=ses->getFrame();
	cout<<"MPEGRecMod::/getFrame data:"<<(void*)res->data<<" len:"<<res->dsize<<endl;

	return res;
}

