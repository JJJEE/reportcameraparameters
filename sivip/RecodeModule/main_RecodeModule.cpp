#include <Utils/Types.h>
#include <Plugins/PluginControl.h>
#include <Utils/Canis.h>
#include <RecodeModule.h>
#include <signal.h>
#include <Utils/debugStackTrace.h>
#include <version.h>

#include <signal.h>

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMP();
	abort();
}

#endif


int main()
{

#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif
#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
#endif
	bool exc=false;

	do
	{
		try
		{
#ifdef WIN32
			RecodeModule m("\\\\.PSF\\Untitled\\aena\\devel\\src\\RecodeModule\\RecodeModuleConfig.xml");
#else
			RecodeModule m("RecodeModuleConfig.xml");
			//Canis cn("RecodeModuleConfig.xml");
#endif
			cout<<"serve"<<endl;
			m.serve();
			exc=false;
		}
		catch (Exception e)
		{
			cout << "Exception ocurred during initialization: "<<exc<<"  " << e.getMsg() << endl;
			exc=!exc;
		}
	}while(exc);
	

	return 0;
}

