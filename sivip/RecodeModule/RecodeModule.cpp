#include <XML/XML.h>
#include <XML/xmlNode.h>
#include <XML/xmlParser.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>
#include <Utils/Exception.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <RecodeModule.h>
#include <RecodeModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <iostream>

using namespace std;

RecodeModule::RecodeModule(Address addr, short type, Canis *cn) : ModuleGestor(addr, type, cn)
{
	STACKTRACE_INSTRUMENT();
	__class=string("RecodeModule");
	moduleInterface=(ModuleInterfaceGestor*)new RecodeModuleInterface(this->address, this->type, this->cn);
}

RecodeModule::RecodeModule(string file):ModuleGestor(file) {
	STACKTRACE_INSTRUMENT();
	__class=string("RecodeModule");
	moduleInterface=(ModuleInterfaceGestor*)new RecodeModuleInterface(this->address, this->type, this->cn);
}

