#pragma once
#ifdef LINUX
#include <sys/time.h>
#endif
#include <Utils/debugNew.h>
#include <Endian/Endian.h>
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
#include <Plugins/GestorRecode.h>
#include <ModuleInterface/ModuleInterfaceGestor.h>
#include <string>
#include <map>
#ifdef WIN32
#include <Utils/WindowsDefs.h>
#endif

using namespace std;


class RecodeModuleInterface: public ModuleInterfaceGestor
{
	public:
		static const int serviceCount=4;

		static const int startSessionServiceId=0;
		static const int endSessionServiceId=1;

		static const int processFrameServiceId=2;
		static const int getFrameServiceId=3;

	private:

/*		RPCPacket* (*serveis[NUM_SERVICES])(RecodeModuleInterface*, GestorRecode*, void*);
	  
		Address addr;
		short type;
		class Sessio
		{
			public:
				GestorRecode *g;
				struct timeval creacio;
				Sessio():g(NULL)
				{
					gettimeofday(&creacio,NULL);
				}
				Sessio(GestorRecode *g, struct timeval creacio):g(g),  creacio(creacio)
				{}
		};

		map<string,Sessio> gestors;
*/		
	public:
		RecodeModuleInterface(Address addr, short type, Canis *cn=NULL);
		~RecodeModuleInterface();
		SerializedException* getSessionNotStablished();
		Gestor* createGestor(Address orig, int type, Canis *cn=NULL);
		void deleteGestor(Gestor *g);

		static RPCPacket* startSession(RecodeModuleInterface *cm, GestorRecode *g, void* params);
		static RPCPacket* endSession(RecodeModuleInterface *cm, GestorRecode *g, void* params);

		static RPCPacket* processFrame(RecodeModuleInterface *cm, GestorRecode *g, void* params);
		static RPCPacket* getFrame(RecodeModuleInterface *cm, GestorRecode *g, void* params);
};

