#include <ippcore.h>
#ifdef __cplusplus
extern "C" {
#endif
#ifndef MACOSX
#include <jni.h>
#else
#include <JavaVM/jni.h>
#endif
#include <base/java/com_eye_cam_sirius_clients_MPEGRecodePluginJNI.h>
#ifdef __cplusplus
}
#endif
#include <string>
#include "IPPMPEGRecodePlugin.h"
#ifdef WIN32
#include <Utils/windowsDefs.h>
#endif

#include <Utils/Timer.h>
#include <Utils/StrUtils.h>


#pragma warning ( disable : 4047 )
#pragma warning ( disable : 4133 )

#ifdef __cplusplus
extern "C" {
#endif

		
JNIEXPORT void JNICALL Java_com_eye_1cam_sirius_clients_MPEGRecodePluginJNI_addFrame ( JNIEnv *env, jobject obj, jlong ses, jint size, jint width, jint height, jbyteArray data, jboolean isKey) 
{
//	jclass cls;
//	jmethodID mid;
//	jfieldID fid;
//	int st, ok;
	char *ptr;

//cout<<" addFrame - session:"<<(void*)ses<<" info.name:"<<((IPPMPEGRecodePlugin::Sessio*)ses)->info.name<<endl;

//	jint len = env->GetArrayLength(data); 
	ptr = (char*)env->GetPrimitiveArrayCritical(data, 0);

	if(ptr!=NULL)
	{
//		char *aux=new char[size];
//		memcpy(aux, ptr, size);
//		cout<<" c++ - release"<<endl;
//		cout<<" c++ - ses.addFrame"<<endl;
		((IPPMPEGRecodePlugin::Sessio*)ses)->addFrame(size, width, height, ptr, isKey);
		env->ReleasePrimitiveArrayCritical(data, ptr, 0);
//		cout<<" c++ - return"<<endl;
	}
}

JNIEXPORT jbyteArray JNICALL Java_com_eye_1cam_sirius_clients_MPEGRecodePluginJNI_getFrame (JNIEnv *env, jobject obj, jlong ses, jbyteArray frameArray) 
{
	jboolean copy=false;
	
	RPFrame *frameRes = ((IPPMPEGRecodePlugin::Sessio*)ses)->getFrame();

	jbyteArray res=frameArray;
	if (res==NULL)
		res=env->NewByteArray(frameRes->dsize);

	env->SetByteArrayRegion(res, 0, frameRes->dsize, (const jbyte*)frameRes->data);

	delete frameRes;
	return res;

}

JNIEXPORT jintArray JNICALL Java_com_eye_1cam_sirius_clients_MPEGRecodePluginJNI_getFrameARGB
  (JNIEnv *env, jobject obj, jlong ses, jintArray frameArray)
{
	jboolean copy=false;

	RPFrame *frameRes = ((IPPMPEGRecodePlugin::Sessio*)ses)->getFrame();

	jintArray res=frameArray;
	if (res==NULL)
		res=env->NewIntArray(frameRes->dsize>>2);

	env->SetIntArrayRegion(res, 0, frameRes->dsize>>2, (const jint*)frameRes->data);



/*	static int i=0;
	string fname=string("frame")+StrUtils::decToString(i)+string(".raw");
	cout << "Saving frame to " << fname <<endl;
	FILE *f=fopen(fname.c_str(), "wb");
	fwrite(frameRes->data, frameRes->dsize, 1, f);
	fclose(f);
	i++;
*/
	delete frameRes;
	return res;
}

JNIEXPORT void JNICALL Java_com_eye_1cam_sirius_clients_MPEGRecodePluginJNI_stopStreamingThreadNative
  (JNIEnv *env, jobject obj, jlong ses)
{
	if (ses!=0)
		((IPPMPEGRecodePlugin::Sessio*)ses)->stopThread();
}


JNIEXPORT jlong JNICALL Java_com_eye_1cam_sirius_clients_MPEGRecodePluginJNI_initCplusSide( JNIEnv *env, jobject obj, jstring name, jint mode, jint width, jint height) 
{
//	cout<<" initCppSide - ini"<<endl;
	void *ses=NULL;

//	cout<<" initCppSide - ini getstrlen"<<endl;
	jsize len=env->GetStringUTFLength(name); 
//	cout<<" initCppSide - ini getstrchar"<<endl;
	char *c=(char*)env->GetStringUTFChars(name,0); 
	string codecName(c, len);
//	cout<<" initCppSide - ini new Ses"<<endl;
	ses=new IPPMPEGRecodePlugin::Sessio(RPCodecInfo(codecName, mode, width, height));
//	cout<<" initCppSide - ini release"<<endl;
	env->ReleaseStringUTFChars(name, c);

//	cout<<" initCppSide - session:"<<(void*)ses<<" info.name:"<<((IPPMPEGRecodePlugin::Sessio*)ses)->info.name<<endl;
	return (jlong)ses;
}


#ifdef __cplusplus
}
#endif
