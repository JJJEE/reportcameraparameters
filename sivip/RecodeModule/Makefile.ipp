SRCDIR = ..

SRCFILES = $(filter-out AVCodec%,$(wildcard *.cpp))
OFILES = $(patsubst %.cpp,%.o,$(SRCFILES))
JNI_OFILES = $(OFILES)
#OFILES = $(patsubst %.cpp,%.o,$(SRCFILES))

DEPEND_DIRS_TMP = $(SRCDIR)/base/Utils $(SRCDIR)/base/XML \
				$(MARINAREPOSITORYDIR)/net/Sockets $(SRCDIR)/base/Threads \
				$(SRCDIR)/base/Endian $(SRCDIR)/base/Plugins \
				$(SRCDIR)/base/Http $(SRCDIR)/base/ServiceFinder \
				$(SRCDIR)/base/Module $(SRCDIR)/base/ModuleInterface \
				$(SRCDIR)/ModuleAccess $(SRCDIR)/RecordingModule \
				$(SRCDIR)/MetadataModule $(SRCDIR)/base/AlarmModule\
				$(IPPROOT)/include \
				$(SRCDIR)/ipp_codecs/core/umc $(SRCDIR)/ipp_codecs/core/vm/include $(SRCDIR)/ipp_codecs/codec/mpeg4_dec $(SRCDIR)/ipp_codecs/_bin/osx32_gcc4/obj/ #$(SRCDIR)/ipp_obj 

IPPLIB = \
    -lippdc$(LIB_ARCH) \
    -lippcc$(LIB_ARCH) -lippac$(LIB_ARCH)  \
    -lippsr$(LIB_ARCH) -lippvc$(LIB_ARCH)  \
    -lippj$(LIB_ARCH)  -lippi$(LIB_ARCH)   \
    -lipps$(LIB_ARCH)  -lippsc$(LIB_ARCH)  \
    -lippcv$(LIB_ARCH) -lippcore$(LIB_ARCH)
				
EXTERN_DIRS_OFILES = $(SRCDIR)/ipp_codecs/_bin/osx32_gcc4/obj/

				
DEPEND_DIRS = $(addsuffix /,$(DEPEND_DIRS_TMP))

DEPEND_EXCEPTION_DIRS_TMP = $(SRCDIR)/StreamingModule $(SRCDIR)/base/Exceptions

DEPEND_EXCEPTION_DIRS = $(addsuffix /,$(DEPEND_EXCEPTION_DIRS_TMP))

DEPEND_OBJECTS = $(filter-out main_%,$(OFILES))

include $(SRCDIR)/mk/includes_block1.mk

INCLUDES += -I$(SRCDIR)/DecodeModule/libavcodec/ -I$(SRCDIR)/DecodeModule/libavformat/ -I$(SRCDIR)/DecodeModule/libavutil/ -I$(SRCDIR)/DecodeModule/libswscale/ -I$(SRCDIR)/ipp_codecs/core -I$(SRCDIR)/ipp_codecs/codec/mpeg4_dec

LDFLAGS += -framework Intel_IPP -L$(IPPROOT)/Libraries $(IPPLIB) -framework QuickTime -framework ApplicationServices -framework Carbon -framework Quartz -framework JavaVM -L$(SRCDIR)/DecodeModule/libavcodec/ -L$(SRCDIR)/DecodeModule/libavformat/ -L$(SRCDIR)/DecodeModule/libavutil/ -L$(SRCDIR)/DecodeModule/libswscale/


#<<<<<<< Makefile
CXXFLAGS += -I $(IPPROOT)/include/ -I$(SRCDIR)/DecodeModule -I$(SRCDIR)/ipp_codecs/core/umc/ -I$(SRCDIR)/ipp_codecs/core/umc/include/ -I$(SRCDIR)/ipp_codecs/core/vm/include/ -D LINUX32=1 $(JAVACXXFLAGS) $(JAVAINCLUDES)
#=======
#LDFLAGS += -L$(SRCDIR)/DecodeModule/libavcodec/ -L$(SRCDIR)/DecodeModule/libavformat/ -L$(SRCDIR)/DecodeModule/libavutil/ -L$(SRCDIR)/DecodeModule/libswscale/
#>>>>>>> 1.27


#LDFLAGS += -lguide -L$(SRCDIR)/DecodeModule/libavcodec/ -L$(SRCDIR)/DecodeModule/libavformat/ -L$(SRCDIR)/DecodeModule/libavutil/ -L$(SRCDIR)/DecodeModule/libswscale/

DEPENDENCIES = ../ipp_codecs/_bin slib cpslib $(DEPEND_DIRS) $(DEPEND_EXCEPTION_DIRS) $(MAINBIN)

../ipp_codecs/_bin:
	$(MAKE) -C ../ipp_codecs _bin


ifeq ($(SYSTEM),MACOSX)

LDFLAGS += -framework Intel_IPP -framework QuickTime -framework ApplicationServices -framework Carbon -framework Quartz -framework JavaVM -lguide -L$(IPPROOT)/Libraries $(IPPLIB) -lc -framework ApplicationServices

include $(SRCDIR)/mk/base_all_rule.mk

depend: $(RECURSIVE_DEPEND)
	mkdep $(CXXFLAGS) $(SRCFILES)

else ifneq ($(SYSTEM),BSD)

SRCFILES = IPPMPEGRecodePlugin.cpp IPPMPEGRecodePluginJNI.cpp RecodeModule.cpp RecodeModuleInterface.cpp
OFILES = $(patsubst %.cpp,%.o,$(SRCFILES))
SOFILES = $(wildcard $(IPPROOT)/sharedlib/*.so)

all: slib cpslib 
	@echo "NOTICE: Not running under Mac OS X. Only compiled JNI modules"

depend:

else

SRCFILES = 
OFILES = 
SOFILES = 

all: 
	@echo "NOTICE: Running under BSD. Not compiling"

depend:

endif

.PHONY: slib cpslib

slib: ../ipp_codecs/_bin depdirs depexcdirs objects $(DEPEND_DIRS_OFILES) $(DEPEND_EXCEPTION_DIRS_OFILES) IPPMPEGRecodePluginJNI.o
	g++ $(CXXFLAGS) $(LDFLAGS) -lc -o $(JNIPREFIX)IPPMPEGRecodePluginJNI$(JNISUFFIX) \
	$(filter-out $(DEBUG_FILTER_OUT), \
		$(filter-out main_%, $(filter-out $(addsuffix main_%,$(DEPEND_DIRS)), \
			$(DEPEND_DIRS_OFILES) \
			$(JNI_OFILES) \
			$(filter-out $(addsufix %,$(DEPEND_DIRS)), \
				$(DEPEND_EXCEPTION_DIRS_OFILES) \
			) \
		)) \
	) $(wildcard $(addsuffix *.o,$(EXTERN_DIRS_OFILES))) $(LIBS) $(SOFILES)
		
cpslib: slib
	mkdir -p $(SRCDIR)/base/java/JNI/$(SYSNAME)
	cp $(JNIPREFIX)IPPMPEGRecodePluginJNI$(JNISUFFIX) $(SRCDIR)/base/java/JNI/$(SYSNAME)


include $(SRCDIR)/mk/includes_lastblock.mk
