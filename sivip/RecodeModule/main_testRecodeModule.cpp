#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <Plugins/GestorRecodeExceptions.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Plugins/GestorImageTypes.h>
#include <Sockets/Address.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <RecodeModuleInterface.h>
#include <ModuleAccess/RecodeModuleAccess.h>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <iostream>
#include <version.h>

#include <signal.h>
#ifdef __cplusplus
extern "C" {
#endif
#include <Quicktime/Movies.h>
#include <Quicktime/MoviesFormat.h>
#include <Quicktime/QuickTimeComponents.h>
#include <Quicktime/QuickTime.h>


#ifdef __cplusplus
}
#endif

/*
 *	RecodeModule JPEG->MPEG4
 */

using namespace std;

OSStatus compressionCallback(void *encodedFrameOutputRefCon, ICMCompressionSessionRef session, OSStatus err, ICMEncodedFrameRef encodedFrame, void *reserved)
{
	Media *media=(Media *)encodedFrameOutputRefCon;
		
	ImageDescriptionHandle imageDesc = NULL;

	cout<<"--Compression Callback: err:"<<err<<"media:"<<(void*)media<<endl;
	cout<<"--Compression Callback:: ICMEncodedFrameGetImageDescriptioni *Media:"<<(void*)(*media)<<endl;
	err = ICMEncodedFrameGetImageDescription(encodedFrame, &imageDesc);
		cout<<"OSErr:"<<err<<endl;


	cout<<"--Compression Callback::AddMediaSample2"<<endl;
	err = AddMediaSample2(*media,
			ICMEncodedFrameGetDataPtr(encodedFrame),
			ICMEncodedFrameGetDataSize(encodedFrame),
			600/15,
			ICMEncodedFrameGetDisplayOffset(encodedFrame),
			(SampleDescriptionHandle)imageDesc,
			1,
			ICMEncodedFrameGetMediaSampleFlags(encodedFrame),
			NULL);
		cout<<"--OSErr:"<<err<<endl;

	// Equivalent al comentari i mes senzill
	//	err=AddMediaSampleFromEncodedFrame(output->media, encodedFrame, nil);
	//	exitIfError(err, "AddMediaSampleFromEncodedFrame error");

	return err;

}

int main(int argc, char* arg)
{

	try
	{
		IPFrame f;
		OSErr err;
		{
			DecodeModuleAccess dma(new Canis("../DecodeModule/DecodeModuleCanisConfig.xml"));
			dma.startSession(26); // RZ50 :P
			dma.setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));
			f = dma.getCompressedNextFrame();
			cout<<" RPSession startSession x:"<<f.frameInfo.x<<" y:"<<f.frameInfo.y<<endl;
		
			RecodeModuleAccess rma(new Canis("RecodeModuleCanisConfig.xml"));
			RPCodecInfo jpg("JPEG", RPCodecInfo::decode, f.frameInfo.x, f.frameInfo.y);	
			RPCodecInfo mpg("MPEG4",  RPCodecInfo::encode, f.frameInfo.x, f.frameInfo.y);	
			RPSession ses(jpg, mpg);
			cout<<" RPSession startSession x:"<<f.frameInfo.x<<" y:"<<f.frameInfo.y<<endl;
			rma.startSession(ses);	
			cout<<" RPSession /startSession"<<endl;

				RPFrame rpf=RPFrame(f.frameLength, f.frameInfo.x, f.frameInfo.y, (char*)f.frame);

				cout<<" processFrame"<<endl;
				rma.processFrame(rpf);
				f.frame=NULL;

			DataHandler outDataHandler;
			Movie movie=NewMovie(0);
		FSRef fsRef;
		cout<<"MPEGRecMod::decode::processFrame:FSPathMakeRef"<<endl;
		err=FSPathMakeRef((UInt8*) string("./").c_str(), &fsRef, NULL);
		cout<<"OSErr:"<<err<<endl;

		Handle FSRDataHandle;
		OSType FSRDataType;
		cout<<"MPEGRecMod::decode::processFrame:QTNewDataReferenceFromFSRefCFString"<<endl;
		err=QTNewDataReferenceFromFSRefCFString(&fsRef, CFSTR("test.mov"), 0, &FSRDataHandle, &FSRDataType);
		cout<<"OSErr:"<<err<<endl;

		cout<<"MPEGRecMod::decode::processFrame:CreateMovieStorage"<<endl;
		err = CreateMovieStorage( FSRDataHandle,  FSRDataType, 'TVOD', 0, createMovieFileDeleteCurFile, &outDataHandler, &movie);
		cout<<"OSErr:"<<err<<endl;

		Track track;
		Media media;

		cout<<"MPEGRecMod::decode::NewMovieTrack:"<<endl;
		track=NewMovieTrack(movie, f.frameInfo.x<<16, f.frameInfo.y<<16, 0);
		err=GetMoviesError();
		cout<<"OSErr:"<<err<<endl;

		// Creem la media
		Handle    dataRef = nil;
		Handle    hMovieData = NewHandle(0);
		err = PtrToHand( &hMovieData, &dataRef, sizeof(Handle));
		
		cout<<"MPEGRecMod::decode::NewTrackMedia:"<<endl;
		//media=NewTrackMedia(track, VideoMediaType, 600, 0, 0);
		media=NewTrackMedia(track,  /*kMPEG4VisualCodecType */VideoMediaType, 600, dataRef, HandleDataHandlerSubType);
		err=GetMoviesError();
		cout<<"OSErr:"<<err<<endl;

		cout<<"MPEGRecMod::decode::BeginMediaEdits:"<<endl;
		err=BeginMediaEdits(media);
		cout<<"OSErr:"<<err<<endl;



			for(int i=0;i<16;i++)
			{
				cout<<" getCompressedNextFrame "<<i<<endl;
				f = dma.getCompressedNextFrame();
				RPFrame rpf=RPFrame(f.frameLength, f.frameInfo.x, f.frameInfo.y, (char*)f.frame);
				cout<<" processFrame"<<endl;
				rma.processFrame(rpf);
				f.frame=NULL;
				RPFrame res=rma.getFrame();
				cout<<" write"<<endl;
				//fwrite(res.data, 1, res.dsize,fd);


				/*ImageDescriptionHandle imageDesc = NULL;
				cout<<"--Compression Callback:: ICMEncodedFrameGetImageDescription"<<endl;
				err = ICMEncodedFrameGetImageDescription(encodedFrame, &imageDesc);
				cout<<"OSErr:"<<err<<endl;
*/


				ImageDescriptionHandle idh = NULL ;
				idh = (ImageDescriptionHandle)NewHandleClear(sizeof(ImageDescription));
				ImageDescription desc=(**idh);
				{
					desc.cType=kMPEG4VisualCodecType;//'mp4v';                  /* what kind of codec compressed this data */
					desc.resvd1=0;                 /* reserved for Apple use */
					desc.resvd2=0;                 /* reserved for Apple use */
					desc.dataRefIndex=0;           /* set to zero  */
					desc.version=2;                /* which version is this data */
					desc.revisionLevel=1;          /* what version of that codec did this */
					desc.vendor='appl';                 /* whose  codec compressed this data */
					desc.temporalQuality=codecNormalQuality;//512;        /* what was the temporal quality factor  */
					desc.spatialQuality=codecNormalQuality;//512;         /* what was the spatial quality factor */
					desc.width=res.width;                  /* how many pixels wide is this data */
					desc.height=res.height;                 /* how many pixels high is this data */
					desc.hRes=72<<16;                   /* horizontal resolution */
					desc.vRes=72<<16;                   /* vertical resolution */
					desc.dataSize=0;//res.dsize;               /* if known, the size of data for this image descriptor */
					desc.frameCount=1;             /* number of frames this description applies to */
					//desc.name=;                   /* name of codec ( in case not installed )  */
					sprintf((char*)desc.name,"MPEG-4 Video");
					desc.depth=res.bitDepth;                  /* what depth is this data (1-32) or ( 33-40 grayscale ) */
					desc.clutID=-1;                 /* clut id or if 0 clut follows  or -1 if no clut */
					desc.idSize=159;//sizeof(desc);//159;                 /* total size of ImageDescription including extra data ( CLUTs and other per sequence data ) */
					cout<<" ImDesc size:"<<desc.idSize<<((f.isKey)?" Key":" no Key")<<endl;
					{
						cout<<"	Image Description"<<endl;
						cout<<"	long idSize:"<<dec<<(desc).idSize<<"	 total size of ImageDescription including extra data ( CLUTs and other per sequence data ) :"<<endl;
						cout<<"	CodecType cType:"<<*((char*)&((desc).cType))<<*(((char*)&((desc).cType))+1)<<*(((char*)&((desc).cType))+2)<<*(((char*)&((desc).cType))+3)<<"	 what kind of codec compressed this data :"<<endl;
						cout<<"	long resvd1:"<<(desc).resvd1<<"	 reserved for Apple use :"<<endl;
						cout<<"	short resvd2:"<<(desc).resvd2<<"	 reserved for Apple use :"<<endl;
						cout<<"	short dataRefIndex:"<<(desc).dataRefIndex<<"	 set to zero  :"<<endl;
						cout<<"	short version:"<<(desc).version<<"	 which version is this data :"<<endl;
						cout<<"	short revisionLevel:"<<(desc).revisionLevel<<"	 what version of that codec did this :"<<endl;
						cout<<"	long vendor:"<<*((char*)&((desc).vendor))<<*(((char*)&((desc).vendor))+1)<<*(((char*)&((desc).vendor))+2)<<*(((char*)&((desc).vendor))+3)<<"	 whose  codec compressed this data :"<<endl;
						cout<<"	CodecQ temporalQuality:"<<(desc).temporalQuality<<"	 what was the temporal quality factor  :"<<endl;
						cout<<"	CodecQ spatialQuality:"<<(desc).spatialQuality<<"	 what was the spatial quality factor :"<<endl;
						cout<<"	short width:"<<(desc).width<<"	 how many pixels wide is this data :"<<endl;
						cout<<"	short height:"<<(desc).height<<"	 how many pixels high is this data :"<<endl;
						cout<<"	Fixed hRes:"<<(desc).hRes<<" >>16:"<< (((desc).hRes)>>16) <<"	 horizontal resolution :"<<endl;
						cout<<"	Fixed vRes:"<<(desc).vRes<<" >>16:"<< (((desc).vRes)>>16) <<"	 vertical resolution :"<<endl;
						cout<<"	long dataSize:"<<(desc).dataSize<<"	 if known, the size of data for this image descriptor :"<<endl;
						cout<<"	short frameCount:"<<(desc).frameCount<<"	 number of frames this description applies to :"<<endl;
						cout<<"	Str31 name:"<<string((char*)&((desc).name), 31)<<"--  name of codec ( in case not installed )  :"<<endl;
						cout<<"	short depth:"<<(desc).depth<<"	 what depth is this data (1-32) or ( 33-40 grayscale ) :"<<endl;
						cout<<"	short clutID:"<<(desc).clutID<<"	 clut id or if 0 clut follows  or -1 if no clut :"<<endl;
						cout<<"	sync sample:"<< f.isKey<<" f.isKey"<<endl;
					}
				};

				//ImageDescriptionPtr idp=&desc;
				//ImageDescriptionHandle idh=&idp;

				cout<<"AddMediaSample2 desc:"<<(void*)res.data<<" : "<<res.dsize<<endl;
				err = AddMediaSample2(media,
						(UInt8*)res.data,
						res.dsize,
						600/15,
						0, //ICMEncodedFrameGetDisplayOffset(encodedFrame),
						(SampleDescriptionHandle)idh,
						1,
						(f.isKey)?0:mediaSampleNotSync,
						NULL);
				cout<<"--OSErr:"<<err<<endl;
			}
//			fclose(fd);

			dma.endSession(25);	

		cout<<"	MPEGRecMod::decode::processFrame:EndMediaEdits"<<endl;
		err=EndMediaEdits(media);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:ExtendMediaDecodeDurationToDisplayEndTime"<<endl;
		err=ExtendMediaDecodeDurationToDisplayEndTime(media, nil);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:InsertMediaIntoTrack"<<endl;
		err=InsertMediaIntoTrack(track, GetTrackDuration(track), 0, GetMediaDisplayDuration(media), fixed1);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:AddMovieToStorage"<<endl;
		err=AddMovieToStorage(movie, outDataHandler);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:CloseMovieStorage"<<endl;
		err=CloseMovieStorage(outDataHandler);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:DisposeMovie"<<endl;
		DisposeMovie(movie);
		cout<<"no OSErr:"<<endl;

		}

	}catch(Exception &e)
	{
		cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
	}
}

