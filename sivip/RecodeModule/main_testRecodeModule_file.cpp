#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <Plugins/GestorRecodeExceptions.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Plugins/GestorImageTypes.h>
#include <Sockets/Address.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <RecodeModuleInterface.h>
#include <ModuleAccess/RecodeModuleAccess.h>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <iostream>
#include <version.h>

#include <signal.h>
#ifdef __cplusplus
extern "C" {
#endif
#include <Quicktime/Movies.h>
#include <Quicktime/MoviesFormat.h>
#include <Quicktime/QuickTimeComponents.h>
#include <Quicktime/QuickTime.h>


#ifdef __cplusplus
}
#endif

using namespace std;

OSStatus compressionCallback(void *encodedFrameOutputRefCon, ICMCompressionSessionRef session, OSStatus err, ICMEncodedFrameRef encodedFrame, void *reserved)
{
	Media *media=(Media *)encodedFrameOutputRefCon;
		
	ImageDescriptionHandle imageDesc = NULL;

	cout<<"--Compression Callback: err:"<<err<<"media:"<<(void*)media<<endl;
	cout<<"--Compression Callback:: ICMEncodedFrameGetImageDescriptioni *Media:"<<(void*)(*media)<<endl;
	err = ICMEncodedFrameGetImageDescription(encodedFrame, &imageDesc);
		cout<<"OSErr:"<<err<<endl;

cout<<"	Image Description"<<endl;
cout<<"	long idSize:"<<(*imageDesc)->idSize<<"	 total size of ImageDescription including extra data ( CLUTs and other per sequence data ) :"<<endl;
cout<<"	CodecType cType:"<<(*imageDesc)->cType<<"	 what kind of codec compressed this data :"<<endl;
cout<<"	long resvd1:"<<(*imageDesc)->resvd1<<"	 reserved for Apple use :"<<endl;
cout<<"	short resvd2:"<<(*imageDesc)->resvd2<<"	 reserved for Apple use :"<<endl;
cout<<"	short dataRefIndex:"<<(*imageDesc)->dataRefIndex<<"	 set to zero  :"<<endl;
cout<<"	short version:"<<(*imageDesc)->version<<"	 which version is this data :"<<endl;
cout<<"	short revisionLevel:"<<(*imageDesc)->revisionLevel<<"	 what version of that codec did this :"<<endl;
cout<<"	long vendor:"<<(*imageDesc)->vendor<<"	 whose  codec compressed this data :"<<endl;
cout<<"	CodecQ temporalQuality:"<<(*imageDesc)->temporalQuality<<"	 what was the temporal quality factor  :"<<endl;
cout<<"	CodecQ spatialQuality:"<<(*imageDesc)->spatialQuality<<"	 what was the spatial quality factor :"<<endl;
cout<<"	short width:"<<(*imageDesc)->width<<"	 how many pixels wide is this data :"<<endl;
cout<<"	short height:"<<(*imageDesc)->height<<"	 how many pixels high is this data :"<<endl;
cout<<"	Fixed hRes:"<<(*imageDesc)->hRes<<"	 horizontal resolution :"<<endl;
cout<<"	Fixed vRes:"<<(*imageDesc)->vRes<<"	 vertical resolution :"<<endl;
cout<<"	long dataSize:"<<(*imageDesc)->dataSize<<"	 if known, the size of data for this image descriptor :"<<endl;
cout<<"	short frameCount:"<<(*imageDesc)->frameCount<<"	 number of frames this description applies to :"<<endl;
cout<<"	Str31 name:"<<(*imageDesc)->name<<"	 name of codec ( in case not installed )  :"<<endl;
cout<<"	short depth:"<<(*imageDesc)->depth<<"	 what depth is this data (1-32) or ( 33-40 grayscale ) :"<<endl;
cout<<"	short clutID:"<<(*imageDesc)->clutID<<"	 clut id or if 0 clut follows  or -1 if no clut :"<<endl;



	cout<<"--Compression Callback::AddMediaSample2"<<endl;
	err = AddMediaSample2(*media,
			ICMEncodedFrameGetDataPtr(encodedFrame),
			ICMEncodedFrameGetDataSize(encodedFrame),
			600/15,
			ICMEncodedFrameGetDisplayOffset(encodedFrame),
			(SampleDescriptionHandle)imageDesc,
			1,
			ICMEncodedFrameGetMediaSampleFlags(encodedFrame),
			NULL);
		cout<<"--OSErr:"<<err<<endl;

	// Equivalent al comentari i mes senzill
	//	err=AddMediaSampleFromEncodedFrame(output->media, encodedFrame, nil);
	//	exitIfError(err, "AddMediaSampleFromEncodedFrame error");

	return err;

}

int main(int argc, char* arg)
{

	try{
		IPFrame f;
		OSErr err;
		//		Address a(IP("192.168.0.50"),4242);
		cout<<"MPEGRecMod::decode::EnterMovies"<<endl;
		err=EnterMovies(); //Inicialitzacio Quicktime
		cout<<"OSErr:"<<err<<endl;
		{
			DecodeModuleAccess dma(new Canis("../DecodeModule/DecodeModuleCanisConfig.xml"));
			dma.startSession(26); // RZ50 :P
			dma.setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));
			f = dma.getCompressedNextFrame();
		
			RecodeModuleAccess rma(new Canis("RecodeModuleCanisConfig.xml"));
			RPCodecInfo jpg("JPEG", RPCodecInfo::decode, f.frameInfo.x, f.frameInfo.y);	
			RPCodecInfo mpg("NULL",  RPCodecInfo::encode, f.frameInfo.x, f.frameInfo.y);	
			cout<<" startSession x:"<<f.frameInfo.x<<" y:"<<f.frameInfo.y<<endl;
			RPSession ses(jpg, mpg);
			cout<<" /startSession"<<endl;
			rma.startSession(ses);	



//	cout<<"JPEGRecMod::processFrame enterMovies"<<endl;
//	EnterMoviesOnThread(0); // uops! :)


	
		PixMapHandle pmh;

		Handle hMovieData = NULL;//NewHandle(sizeof(char*));
		cout<<"MPEGRecMod::decode::processFrame:PtrToHand DataRef -> Hand"<<endl;
		err = PtrToHand( f.frame/*&dataPtr*/, &hMovieData, f.frameLength);//sizeof(PointerDataRefRecord));
		cout<<"OSErr:"<<err<<endl;
		
		Handle dataRef = NULL;
		cout<<"MPEGRecMod::decode::processFrame:PtrToHand "<<endl;
		err = PtrToHand( &hMovieData, &dataRef, sizeof(Handle) );
		cout<<"OSErr:"<<err<<endl;

		DataHandler outDataHandler;
		Movie movie;
		ICMCompressionSessionOptionsRef compressionOptions;

		FSRef fsRef;
		cout<<"MPEGRecMod::decode::processFrame:FSPathMakeRef"<<endl;
		err=FSPathMakeRef((UInt8*) string("./").c_str(), &fsRef, NULL);
		cout<<"OSErr:"<<err<<endl;

		Handle FSRDataHandle;
		OSType FSRDataType;
		cout<<"MPEGRecMod::decode::processFrame:QTNewDataReferenceFromFSRefCFString"<<endl;
		err=QTNewDataReferenceFromFSRefCFString(&fsRef, CFSTR("test.mov"), 0, &FSRDataHandle, &FSRDataType);
		cout<<"OSErr:"<<err<<endl;



		cout<<"MPEGRecMod::decode::processFrame:CreateMovieStorage"<<endl;
		err = CreateMovieStorage( FSRDataHandle,  FSRDataType, 'TVOD', 0, createMovieFileDeleteCurFile, &outDataHandler, &movie);
		cout<<"OSErr:"<<err<<endl;

		cout<<"MPEGRecMod::decode::processFrame:"<<endl;
		err = ICMCompressionSessionOptionsCreate(NULL, &compressionOptions);
		cout<<"OSErr:"<<err<<endl;



		cout<<"MPEGRecMod::decode::  ----- opcions ------"<<endl;
		err = ICMCompressionSessionOptionsSetAllowTemporalCompression(compressionOptions, true);
		cout<<" ICMCompressionSessionOptionsSetAllowTemporalCompression OSErr:"<<err<<endl;
		err = ICMCompressionSessionOptionsSetAllowFrameReordering(compressionOptions, true);
		cout<<" ICMCompressionSessionOptionsSetAllowFrameReordering OSErr:"<<err<<endl;
		// Limitem el key frame rate a 60 segons a 24 fps
		err = ICMCompressionSessionOptionsSetMaxKeyFrameInterval(compressionOptions, 60*24);
		cout<<" ICMCompressionSessionOptionsSetMaxKeyFrameInterval OSErr:"<<err<<endl;
		// Deixem que el compressor faci una mica el q li roti
		err = ICMCompressionSessionOptionsSetAllowFrameTimeChanges(compressionOptions, true);
		cout<<" ICMCompressionSessionOptionsSetAllowFrameTimeChanges OSErr:"<<err<<endl;
		// Volem la durada de cada frame especificada
		err = ICMCompressionSessionOptionsSetDurationsNeeded(compressionOptions, true);
		cout<<" ICMCompressionSessionOptionsSetDurationsNeeded OSErr:"<<err<<endl;
		int avgDataRate=128*1024; // 128 kB/s
		err = ICMCompressionSessionOptionsSetProperty(compressionOptions, kQTPropertyClass_ICMCompressionSessionOptions,
				kICMCompressionSessionOptionsPropertyID_AverageDataRate, sizeof(avgDataRate), &avgDataRate);
		cout<<" ICMCompressionSessionOptionsSetProperty OSErr:"<<err<<endl;



//		Movie inputMovie;
//	Rect inputMovieRect;

//		cout<<"MPEGRecMod::decode::NewMovieFromHandle:"<<endl;
//		err=NewMovieFromHandle(&inputMovie, dataRef,newMovieDontResolveDataRefs,NULL);
//		cout<<"OSErr:"<<err<<endl;
//		cout<<"MPEGRecMod::decode::GetMovieBox:"<<endl;
//		GetMovieBox(movie, &inputMovieRect);
//		cout<<"noErr r:"<< inputMovieRect.right<<" l:"<<inputMovieRect.left<<" b:"<<inputMovieRect.bottom<<" t:"<<inputMovieRect.top<<" real x:"<<f.frameInfo.x<<" y:"<<f.frameInfo.y<<endl;
		RPFrame resp;
		resp.height = f.frameInfo.y;// (inputMovieRect.right-inputMovieRect.left);//<<16;
		resp.width = f.frameInfo.x;// (inputMovieRect.bottom-inputMovieRect.top);//<<16;

		Track track;
		Media media;

		ICMEncodedFrameOutputRecord outputRecord;
		outputRecord.encodedFrameOutputCallback = compressionCallback;
		outputRecord.encodedFrameOutputRefCon = &media;
		outputRecord.frameDataAllocator = NULL;
		ICMCompressionSessionRef compressionSession;

			
				
		cout<<"MPEGRecMod::decode::ICMCompressionSessionCreate:"<<endl;
		cout<<" params: ( NULL,"<< resp.width<<","<< resp.height<<","<< kMPEG4VisualCodecType<<","<< 600/*timescale*/<<","<< compressionOptions<<", NULL,"<< &outputRecord<<","<< &compressionSession<<")"<<endl;
		err=ICMCompressionSessionCreate(NULL, resp.width, resp.height, kMPEG4VisualCodecType, 600/*timescale*/, compressionOptions, NULL, &outputRecord, &compressionSession);
		cout<<"OSErr:"<<err<<endl;


		// Creem la pista
		cout<<"MPEGRecMod::decode::NewMovieTrack:"<<endl;
		track=NewMovieTrack(movie, resp.width<<16, resp.height<<16, 0);
		err=GetMoviesError();
		cout<<"OSErr:"<<err<<endl;

		// Creem la media
		cout<<"MPEGRecMod::decode::NewTrackMedia:"<<endl;
		media=NewTrackMedia(track, VideoMediaType, 600, 0, 0);
		err=GetMoviesError();
		cout<<"OSErr:"<<err<<endl;

		cout<<"MPEGRecMod::decode::BeginMediaEdits:"<<endl;
		err=BeginMediaEdits(media);
		cout<<"OSErr:"<<err<<endl;

	




			for(int i=0;i<16;i++)
			{
				cout<<"MPEGRecMod::decode::processFrame:dma.getCompressedNextFrame"<<endl;
				f = dma.getCompressedNextFrame();
				cout<<"	MPEGRecMod::decode::processFrame:ProcessFrame length:"<<f.frameLength<<" x:"<< f.frameInfo.x<<" y:"<< f.frameInfo.y<<" addr:"<< f.frame<<endl;
				RPFrame rpf=RPFrame(f.frameLength, f.frameInfo.x, f.frameInfo.y, (char*)f.frame);
				rma.processFrame(rpf);
				RPFrame res=rma.getFrame();
				FILE *fd=fopen((string("result")+StrUtils::decToString(i)+string(".raw")).c_str(),"w+b");

				fwrite(res.data, 1, res.dsize,fd);
				fclose(fd);
				CVPixelBufferRef pixelBufferOut;
				cout<<"	MPEGRecMod::decode::processFrame:CVPixelBufferCreateWithBytes  frame w:"<<res.width<<" h:"<< res.height<<" depth:"<< res.bitDepth<<" size:"<<res.dsize<<endl;;
				CVReturn cverr=CVPixelBufferCreateWithBytes (    
						NULL,    
						res.width,   
						res.height,   
						k24RGBPixelFormat,//OSType pixelFormatType,    
						(res.data),    
						res.width,   
						NULL,//CVPixelBufferReleaseBytesCallback releaseCallback,    
						NULL,//res.data,//void *releaseRefCon, -- no fa falta, tb et passes les dades :P
						NULL,//CFDictionaryRef pixelBufferAttributes,    
						&pixelBufferOut
						);
				cout<<"	CVRet:"<<cverr<<endl;


				cout<<"	MPEGRecMod::decode::processFrame:FSPathMakeRef"<<endl;
				err=ICMCompressionSessionEncodeFrame(compressionSession, pixelBufferOut, i*(600/15), 600/15, kICMValidTime_DisplayTimeStampIsValid|kICMValidTime_DisplayDurationIsValid, NULL, NULL, NULL);
				cout<<"	OSErr:"<<err<<endl<<endl;
			}

			dma.endSession(25);	

		cout<<"	MPEGRecMod::decode::processFrame:ICMCompressionSessionCompleteFrames"<<endl;
		err=ICMCompressionSessionCompleteFrames(compressionSession, true, 0, 0);
		cout<<"	OSErr:"<<endl<<endl;
		cout<<"	MPEGRecMod::decode::processFrame:ICMCompressionSessionRelease"<<endl;
		ICMCompressionSessionRelease(compressionSession);
		cout<<"	no OSErr:"<<endl<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:EndMediaEdits"<<endl;
		err=EndMediaEdits(media);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:ExtendMediaDecodeDurationToDisplayEndTime"<<endl;
		err=ExtendMediaDecodeDurationToDisplayEndTime(media, nil);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:InsertMediaIntoTrack"<<endl;
		err=InsertMediaIntoTrack(track, GetTrackDuration(track), 0, GetMediaDisplayDuration(media), fixed1);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:AddMovieToStorage"<<endl;
		err=AddMovieToStorage(movie, outDataHandler);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:CloseMovieStorage"<<endl;
		err=CloseMovieStorage(outDataHandler);
		cout<<"OSErr:"<<err<<endl;

		cout<<"	MPEGRecMod::decode::processFrame:DisposeMovie"<<endl;
		DisposeMovie(movie);
		cout<<"no OSErr:"<<endl;
		}


//	ExitMoviesOnThread(); // uops! :)

	ExitMovies(); // uops! :)
	}catch(Exception &e)
	{
		cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
	}
}

