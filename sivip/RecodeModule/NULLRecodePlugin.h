#pragma once
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginRecode.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/SocketTCP.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <map>
#include <list>
#include <iostream>
using namespace std;

class NULLRecodePlugin: public PluginRecode
{
		class Sessio
		{
			public:
				list<RPFrame*> encodedFrames;
				Condition outCond;
				Sessio(){}
		};

		map<string, Sessio*> sessions;

		
	protected:
		Sessio* getSession(Address a);
		
	public:
		DBGateway db;
		NULLRecodePlugin(string file);
		~NULLRecodePlugin();
		

		virtual void startSession(RPCodecInfo id, Address *a);
		virtual void endSession(RPCodecInfo id, Address *a);

		virtual void encodeFrame(RPFrame *f, Address *a);
		virtual RPFrame *decodeFrame(RPFrame *f, Address *a);
		virtual RPFrame *getFrame(Address *a);
};

