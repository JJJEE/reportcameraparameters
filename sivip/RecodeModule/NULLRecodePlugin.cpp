#include <Plugins/GestorRecodeExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <NULLRecodePlugin.h>
#include <RecodeModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
using namespace std;



NULLRecodePlugin::NULLRecodePlugin(string file):PluginRecode(file), db(address, type, cn)
{

	cout<<"init:"<<file<< endl;
	FILE *f=fopen(file.c_str(),"rb");

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char buf2[len];
	fread(buf2,len,1,f);
	fclose(f);

	XML *conf=xmlParser::parse(buf2);
	conf->getNodeData("[0]/ModelConfigFile");

	delete conf;
	cout<<"----------plugin inicialitzat----------"<<endl;
}

NULLRecodePlugin::~NULLRecodePlugin()
{
}

NULLRecodePlugin::Sessio* NULLRecodePlugin::getSession(Address a)
{
	map<string,Sessio*>::iterator i=sessions.find(a.toString());
	
	if (i==sessions.end())
	{
		throw (RPSessionNotStablishedException(0, string("Session no establerta")));
	}
	return i->second;
}

void NULLRecodePlugin::startSession(RPCodecInfo id, Address *a)
{	
	map<string,Sessio*>::iterator itSes=sessions.find(a->toString());
	
	cout<<"NULLRecMod::StartSession:"<<id.name<<endl;
	
	if(itSes!=sessions.end())
	{
		throw(RPSessionAlreadyStablishedException(1, " Sessio ja establerta"));
	}

	Sessio *ses = new Sessio();
	cout<<"NULLRecMod::StartSession:"<<id.name<<" ses:"<<(void*) ses<<endl;
	sessions[a->toString()]=ses;

}

void NULLRecodePlugin::endSession(RPCodecInfo id, Address *a)
{
	map<string,Sessio*>::iterator i=sessions.find(a->toString());
	
	if(i==sessions.end())
	{
		throw(RPSessionNotStablishedException(0, " Sessio no establerta"));
	}
	Sessio *s=i->second;
	
	sessions.erase(i);
	delete s;
}

RPFrame *NULLRecodePlugin::decodeFrame(RPFrame *f, Address *a)
{
	Sessio *ses=getSession(*a);

	cout<<"NULLRecMod::decodeFrame"<<endl;

	return f;
}

void NULLRecodePlugin::encodeFrame(RPFrame *f, Address *a)
{
	Sessio *ses=getSession(*a);

	cout<<"NULLRecMod::encodeFrame ses:"<<(void*)ses<<endl;

	ses->encodedFrames.push_back(f);
	cout<<"NULLRecMod::encodeFrame size:"<<ses->encodedFrames.size()<<endl;
	ses->outCond.signal();
	cout<<"NULLRecMod::encodeFrame"<<endl;

}

RPFrame *NULLRecodePlugin::getFrame(Address *a)
{
	Sessio *ses=getSession(*a);

	cout<<"NULLRecMod::getFrame ses:"<<(void*)ses<<" size:"<<ses->encodedFrames.size()<<endl;
	ses->outCond.lock(); 
	while(ses->encodedFrames.size()==0)
	{
		cout<<"NULLRecMod::getFrame size=0, wait"<<endl;
		ses->outCond.wait();
		cout<<"NULLRecMod::getFrame size:"<<ses->encodedFrames.size()<<endl;
	}
	RPFrame *f=ses->encodedFrames.front();
	ses->encodedFrames.pop_front();
	ses->outCond.unlock();

	cout<<"NULLRecMod::/getFrame"<<endl;

	return f;
}
