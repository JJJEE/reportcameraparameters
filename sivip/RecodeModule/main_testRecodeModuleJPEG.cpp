#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <Plugins/GestorRecodeExceptions.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/Address.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <RecodeModuleInterface.h>
#include <ModuleAccess/RecodeModuleAccess.h>
#include <iostream>
#include <version.h>

#include <signal.h>

using namespace std;

/*
 * RecodeModule JPEG->NULL
 * RecodeModule NULL->JPEG
 */

int main(int argc, char* arg)
{

	try{
		RPFrame res;
		//		Address a(IP("192.168.0.50"),4242);
		{
			RecodeModuleAccess rma(new Canis("RecodeModuleCanisConfig.xml"));
			RPCodecInfo jpg("JPEG", RPCodecInfo::decode, 128, 128);	
			RPCodecInfo mpg("NULL",  RPCodecInfo::encode, 128, 128);	
			cout<<" startSession"<<endl;
			RPSession ses(jpg, mpg);
			cout<<" /startSession"<<endl;
			rma.startSession(ses);	

			string file("test.jpg");
			FILE *f=fopen(file.c_str(),"rb");

			fseek(f,0,SEEK_END);
			int len=ftell(f);
			fseek(f,0,SEEK_SET);

			cout<<"reading:"<<len<<" bytes from "<<file<< endl;
			char buf2[len];
			fread(buf2,len,1,f);
			fclose(f);

			rma.processFrame(RPFrame(len, 128, 128, buf2));
			res= rma.getFrame();

			cout<<" result:"<< res.dsize<<" bytes"<<endl;

			FILE *fd=fopen("result.raw","w+b");

			fwrite(res.data, 1, res.dsize,fd);
			fclose(fd);

			rma.endSession(ses);	
		}
		cout<<" RAW, reencodem"<<endl;
		{
			RecodeModuleAccess rma(new Canis("RecodeModuleCanisConfig.xml"));
			RPCodecInfo mpg("NULL",  RPCodecInfo::decode, 128, 128);	
			RPCodecInfo jpg("JPEG", RPCodecInfo::encode, 128, 128);	
			RPSession ses(mpg, jpg);
			rma.startSession(ses);	

	/*		string file("result.raw");
			FILE *f=fopen(file.c_str(),"rb");

			fseek(f,0,SEEK_END);
			unsigned int len=ftell(f);
			fseek(f,0,SEEK_SET);

			cout<<"reading:"<<len<<" bytes from "<<file<< endl;
			char buf2[len];
			fread(buf2,len,1,f);
			fclose(f);
			
			RPFrame ff(len, 128, 128, buf2);
			ff.bitDepth=res.bitDepth;
	*/		rma.processFrame(res);//ff);
			res=rma.getFrame();//ff);

			cout<<" result:"<< res.dsize<<" bytes"<<endl;

			FILE *fd=fopen("result.jpg","w+b");

			fwrite(res.data, 1, res.dsize,fd);
			fclose(fd);

			rma.endSession(ses);	

		}
	}catch(Exception &e)
	{
		cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
	}
}

