/*
 *  ObjectManager
 *  
 *
 *  Created by David Mar� Larrosa on 17/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __MARINA_EYE_CAM__BASE__OBJECTMANAGER
#define __MARINA_EYE_CAM__BASE__OBJECTMANAGER

#include <ObjectManager/ObjectManager.h>
#include <Exceptions/IdAlreadyInUseException.h>
#include <Exceptions/IdNotFoundException.h>
#include <iostream>
#include <map>
#include <string>

using namespace std;

// Estatiques
template <class T> RWLock ObjectManager<T>::lock;//(true);
template <class T> Mutex ObjectManager<T>::rcMutex;
template <class T> map<string, typename ObjectManager<T>::objInfo> ObjectManager<T>::objects;

// Subclasses
template <class T> ObjectManager<T>::Iterator::Iterator() : lastId(string(""))
{
	this->it=ObjectManager<T>::objects.begin();
}
	
template <class T> bool ObjectManager<T>::Iterator::hasNext()
{
	return this->it!=ObjectManager<T>::objects.end();
}

template <class T> T* ObjectManager<T>::Iterator::next()
{
//	ObjectManager<T>::lock.lock();
	T *ret=this->it->second.obj;
	rcMutex.lock();
	this->it->second.refCount++;
	rcMutex.unlock();
	this->lastId=this->it->first;
	this->it++;
//	ObjectManager<T>::lock.unlock();
	return ret;
}

template <class T> string ObjectManager<T>::Iterator::getLastId()
{
	return this->lastId;
}

// Metodes de la classe
template <class T> string ObjectManager<T>::id(T* o)
{
	return string((char*)o, sizeof(T*));
}

template <class T> T* ObjectManager<T>::create(const string id)
{
	T *o=NULL;

	// Si ja existeix, excepcio
	lock.wlock();
	typename map<string, typename ObjectManager<T>::objInfo>::const_iterator itOb=objects.find(id);
	if (itOb!=objects.end())
	{
		lock.unlock();
		throw IdAlreadyInUseException(0, "Object id already in use");
	}

	// No existeix, la creem :)
	o=new T();
	typename ObjectManager<T>::objInfo oi(o);
	objects[id]=oi;
	lock.unlock();

	return o;
}

template <class T> T* ObjectManager<T>::manage(const string id, T* o, dword refCount)
{
	// Si ja existeix, excepcio
	lock.wlock();
	typename map<string, typename ObjectManager<T>::objInfo>::const_iterator itOb=objects.find(id);
	if (itOb!=objects.end())
	{
		lock.unlock();
		throw IdAlreadyInUseException(0, "Object id already in use");
	}

	// No existeix, la creem :)
	typename ObjectManager<T>::objInfo oi(o, refCount);
	objects[id]=oi;
	lock.unlock();

	return o;
}

template <class T> T* ObjectManager<T>::retain(const string id)
{
	lock.rlock();
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.find(id);
	
	if (itOb==objects.end())
	{
		lock.unlock();
		throw IdNotFoundException(0, string("ObjectManager::retain(): Object not found for specified id ") + id);
	}
	
	rcMutex.lock();
	itOb->second.refCount++;
	rcMutex.unlock();
	lock.unlock();
	return itOb->second.obj;
}

template <class T> T* ObjectManager<T>::retain(const T *obj)
{
	lock.rlock();
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.begin();
	
	while (itOb!=objects.end())
	{
		if (itOb->second.obj==obj)
		{
			rcMutex.lock();
			itOb->second.refCount++;
			rcMutex.unlock();
			lock.unlock();
			return itOb->second.obj;
		}
		
		itOb++;
	}

	lock.unlock();
	throw IdNotFoundException(0, "Object not found for specified pointer");
}

template <class T> void ObjectManager<T>::releaseFromIterator(const string id)
{
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.find(id);
	
	if (itOb==objects.end())
	{
		throw IdNotFoundException(0, string("ObjectManager::release(): Object not found for specified id ") + id);
	}
	
	rcMutex.lock();
	itOb->second.refCount--;
	if (itOb->second.refCount==0)
			cout << __FILE__ << ", line " << __LINE__ << " FATAL: refcount == 0 in releaseFromIterator" << endl;
	rcMutex.unlock();
}

template <class T> void ObjectManager<T>::releaseFromIterator(const T *obj)
{
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.begin();
	
	while (itOb!=objects.end())
	{
		if (itOb->second.obj==obj)
		{
			rcMutex.lock();
			itOb->second.refCount--;
			string id=itOb->first;
			if (itOb->second.refCount==0)
				cout << __FILE__ << ", line " << __LINE__ << " FATAL: refcount == 0 in releaseFromIterator" << endl;
			rcMutex.unlock();
	
			return;
		}
		
		itOb++;
	}

	throw IdNotFoundException(0, "Object not found for specified pointer");
}


template <class T> void ObjectManager<T>::release(const string id)
{
	lock.wlock();
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.find(id);
	
	if (itOb==objects.end())
	{
		lock.unlock();
		throw IdNotFoundException(0, string("ObjectManager::release(): Object not found for specified id ") + id);
	}
	
	rcMutex.lock();
	itOb->second.refCount--;
	if (itOb->second.refCount==0)
	{
		typename ObjectManager<T>::objInfo oi=itOb->second;
		objects.erase(id);
		delete oi.obj;
	}
	rcMutex.unlock();
	
	lock.unlock();
}

template <class T> void ObjectManager<T>::release(const T *obj)
{
	lock.wlock();
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.begin();
	
	while (itOb!=objects.end())
	{
		if (itOb->second.obj==obj)
		{
			rcMutex.lock();
			itOb->second.refCount--;
			string id=itOb->first;
			if (itOb->second.refCount==0)
			{
				typename ObjectManager<T>::objInfo oi=itOb->second;
				objects.erase(id);
				delete oi.obj;
			}
			rcMutex.unlock();
	
			lock.unlock();
			return;
		}
		
		itOb++;
	}

	lock.unlock();
	throw IdNotFoundException(0, "Object not found for specified pointer");
}

template <class T> T* ObjectManager<T>::rename(const string oldId, const string newId, bool overwrite)
{
	lock.wlock();
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.find(oldId);
	
	if (itOb==objects.end())
	{
		lock.unlock();
		throw IdNotFoundException(0, string("ObjectManager::rename(): Object not found for specified id ") + oldId);
	}

	if (!overwrite)
	{
		typename map<string, typename ObjectManager<T>::objInfo>::iterator itNewOb=objects.find(newId);
	
		if (itNewOb!=objects.end())
		{
			lock.unlock();
			throw IdAlreadyInUseException(0, "Object id already in use");
		}
	}
		
	typename ObjectManager<T>::objInfo oi=itOb->second;
	objects.erase(oldId);
	objects[newId]=oi;
	
	lock.unlock();
	return oi.obj;
}

template <class T> dword ObjectManager<T>::getReferenceCount(const string id)
{
	lock.rlock();
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.find(id);
	
	if (itOb==objects.end())
	{
		lock.unlock();
		return 0;
	}
	
	rcMutex.lock();
	dword refCount=itOb->second.refCount;
	rcMutex.unlock();

	lock.unlock();
	
	return refCount;
}

template <class T> dword ObjectManager<T>::getReferenceCount(const T *obj)
{
	lock.rlock();
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.begin();
	
	while (itOb!=objects.end())
	{
		if (itOb->second.obj==obj)
		{
			rcMutex.lock();
			dword refCount=itOb->second.refCount;
			rcMutex.unlock();
			lock.unlock();
			
			return refCount;
		}
		
		itOb++;
	}

	lock.unlock();
	return 0;
}

template <class T> string ObjectManager<T>::getKey(const T *obj)
{
	lock.rlock();
	typename map<string, typename ObjectManager<T>::objInfo>::iterator itOb=objects.begin();
	
	while (itOb!=objects.end())
	{
		if (itOb->second.obj==obj)
		{
			string key =itOb->first;
			lock.unlock();
			
			return key;
		}
		
		itOb++;
	}

	lock.unlock();
	return string("");
}

template <class T> int ObjectManager<T>::numItems()
{
	int res=0;	
	rcMutex.lock();
	res=objects.size();
	rcMutex.unlock();
	return res;
}

template <class T> typename ObjectManager<T>::Iterator ObjectManager<T>::iterator()
{
	ObjectManager<T>::lock.rlock();
//	cout<<" ------------------------------------------------- ObjectManager iterator"<<endl;
	typename ObjectManager<T>::Iterator it;
	return it;
}

template <class T> void ObjectManager<T>::releaseIterator()
{
//	cout<<"ObjectManager release iterator ------------------------------------------------- "<<endl;
	lock.unlock();
}

#endif
