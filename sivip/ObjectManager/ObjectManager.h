/*
 *  ObjectManager.h
 *  
 *
 *  Created by David Mar� Larrosa on 11/05/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __MARINA_EYE_CAM__BASE__OBJECTMANAGER_H
#define __MARINA_EYE_CAM__BASE__OBJECTMANAGER_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Threads/Mutex.h>
#include <Threads/RWlock.h>
#include <map>
#include <string>

using namespace std;

template <class T> class ObjectManager
{
public:
	class objInfo
	{
	public:
		T *obj;
		dword refCount;
		
		objInfo(T *o=NULL, dword refCount=1) : obj(o), refCount(refCount)
		{
		};
	};
	
	class Iterator
	{
		typename map<string, typename ObjectManager<T>::objInfo>::iterator it;
		
		string lastId;
	public:
		Iterator();
	
		bool hasNext();	
		T* next();
		string getLastId();
	};
	
protected:
	static RWLock lock;
	static Mutex rcMutex;
	static map<string, objInfo> objects;
	
public:

	static string id(T* o);

	static T* create(const string id);
	static T* manage(const string id, T *o, dword refCount=1);
	static T* retain(const string id);
	static T* retain(const T *obj);
	static void releaseFromIterator(const string id);
	static void releaseFromIterator(const T*obj);
	static void release(const string id);
	static void release(const T*obj);
	static T* rename(const string oldId, const string newId, bool overwrite=false);
	static dword getReferenceCount(const string id);
	static dword getReferenceCount(const T *obj);
	static string getKey(const T *obj);
	static int numItems();

	static Iterator iterator();
	static void releaseIterator();
};

#endif

