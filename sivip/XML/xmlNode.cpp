#include <XML/xmlNode.h>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

#pragma mark *** xmlNode::Iterator
string xmlNode::Iterator::getTranslatedPath()
{
	string path=this->itPath;
	
	string::size_type nextPos=0;
	for (int i=0; i<this->nIndices; i++)
	{
		string::size_type pos = path.find(string("*"), nextPos);
		nextPos=pos+1;

		string indexStr=string("[") + StrUtils::decToString(this->currentIndices[i]) + string("]");

		path=path.replace(pos, 1, indexStr);
	}
	
	return path;
}

void xmlNode::Iterator::increment()
{
	if (this->currentIncrementingIndex>=0)
		this->currentIndices[this->currentIncrementingIndex]++;
}

void xmlNode::Iterator::enter()
{
	this->currentIncrementingIndex = this->nIndices - 1;
}

void xmlNode::Iterator::toSuperNode()
{
	this->currentIndices[this->currentIncrementingIndex]=0;
	this->currentIncrementingIndex--;
	this->increment();
}

void xmlNode::Iterator::save()
{
	for (int i=0; i<this->nIndices; i++)
		this->savedIndices[i] = this->currentIndices[i];
		
	this->savedCurrentIncrementingIndex = this->currentIncrementingIndex;
}

void xmlNode::Iterator::restore()
{
	for (int i=0; i<this->nIndices; i++)
		this->currentIndices[i] = this->savedIndices[i];
		
	this->currentIncrementingIndex = this->savedCurrentIncrementingIndex;
}

xmlNode::Iterator::Iterator(xmlNode *node, string wildcardPath) : 
	itPath(wildcardPath), node(node), nIndices(0)
{
	// Comptem quants * hi ha al path
	int pLen = this->itPath.length();
	for (int p=0; p<pLen; p++)
		if (this->itPath[p]=='*')
			this->nIndices++;
	
	if (this->nIndices==0)
	{
		this->itPath += string("*");
		this->nIndices=1;
	}
	
	this->currentIndices=new int [this->nIndices];
	this->savedIndices=new int [this->nIndices];

	if (this->currentIndices==NULL || this->savedIndices==NULL)
	{
		// TODO
	}

	for (int i=0; i<this->nIndices; i++)
	{
		this->currentIndices[i]=0;
		this->savedIndices[i]=0;
	}
	
	this->currentIncrementingIndex = this->nIndices - 1;
}

xmlNode::Iterator::~Iterator()
{
	delete [] this->currentIndices;
	delete [] this->savedIndices;
}

bool xmlNode::Iterator::hasNext()
{
	this->save();
	
	while (this->currentIncrementingIndex>=0)
	{
		string path = this->getTranslatedPath();
		
		xmlNode *n = this->node->getNode(path);
		if (n!=NULL)
		{
			this->restore();
			return true;
		}
		else
		{
			this->toSuperNode();
		}
	}
	
	this->restore();
	return false;
}

xmlNode *xmlNode::Iterator::next()
{
	while (this->currentIncrementingIndex>=0)
	{
		string path = this->getTranslatedPath();
		
		xmlNode *n = this->node->getNode(path);
		if (n!=NULL)
		{
			this->enter();
			this->increment();
			return n;
		}
		else
		{
			this->toSuperNode();
		}
	}
	
	return NULL;
}

#pragma mark *** xmlNode
xmlNode::xmlNode(void)
{
	STACKTRACE_INSTRUMENT();
	
}

xmlNode::xmlNode(string name) : nodeName(name)
{
}

xmlNode::xmlNode(string name, string cdata) : nodeName(name), cdata(cdata)
{
}

xmlNode::~xmlNode(void)
{
	STACKTRACE_INSTRUMENT();
	
	list<xmlNode*>::iterator i=this->kids.begin();

	while (i!=this->kids.end())
	{
		xmlNode *n=(*i);
		//cout << "\t\txmlNode::~xmlNode -------- this->kids.erase()" << endl;
		this->kids.erase(i);
		//cout << "\t\txmlNode::~xmlNode -------- delete n" << endl;
		delete n;
		i=this->kids.begin();
	}
}

// retorna el nom del node
string xmlNode::getName(void)
{
	STACKTRACE_INSTRUMENT();
	
	return this->nodeName;
}

// setta el nom del node
void xmlNode::setName(string name)
{
	STACKTRACE_INSTRUMENT();
	
	this->nodeName=name;
}

// retorna la cdata del  node
string xmlNode::getCdata(void)
{
	STACKTRACE_INSTRUMENT();
	
	return this->cdata;
}

// setta la cdata
void xmlNode::setCdata(string cdata)
{
	STACKTRACE_INSTRUMENT();
	
	this->cdata=cdata;
}

// Representacio del node en string
string xmlNode::toString(void)
{
	STACKTRACE_INSTRUMENT();
	
	return this->toString(0);
}

// Representacio del node en string, versio interna
string xmlNode::toString(int indent)
{
	STACKTRACE_INSTRUMENT();
	
//	string nodeNameRecording=string("recording");
//	if(nodeName==nodeNameRecording)
//		cout<<"XMLNode:: toString: begin:"<<(void*)this<<endl;
	string str=string("");
	map<string,string,xmlNode::compStrByPtr> et=xmlNode::encodeTable();
	
	for (int i = 0; i < indent; i++)
		str += string("\t");

//	if(nodeName==nodeNameRecording)
//		cout<<"XMLNode:: toString: this:"<<(void*)this<<endl;
//	cout<< StrUtils::hexDump(string((char*)this, sizeof(xmlNode)))<<endl;
	str+=string("<")+this->nodeName;
//	cout<< StrUtils::hexDump(string((char*)this, sizeof(xmlNode)))<<endl;

//	if(nodeName==nodeNameRecording)
//		cout<<"XMLNode:: toString: params"<<endl;
	for (map <string,string>::const_iterator itPar=params.begin();
		 itPar!=params.end(); itPar++)
	{
		str+=string(" ")+(*itPar).first+string("=\"");
		
		string parVal=(*itPar).second;
		
		for (map<string,string,xmlNode::compStrByPtr>::const_iterator i=et.begin(); i!=et.end(); i++)
		{
			string::size_type pos=parVal.find(i->first, 0);
			// Control d'autocontinguts
			while (pos!=string::npos && parVal.substr(pos, i->second.length())==i->second)
				pos=parVal.find(i->first, pos+1);
			
			while (pos!=string::npos)
			{
				parVal.replace(pos, i->first.length(), i->second);
				pos=parVal.find(i->first, 0);
				
				// Control d'autocontinguts
				while (pos!=string::npos && parVal.substr(pos, i->second.length())==i->second)
					pos=parVal.find(i->first, pos+1);
			}
		}
		
		str+=parVal+string("\"");
	}

//	if(nodeName==nodeNameRecording)
//		cout<<"XMLNode:: toString: kids"<<endl;
	if (this->cdata.length()==0 && this->kids.size()==0)
		str+=string("/>");
	else
	{
		str+=string(">");
		indent++;

		if (this->cdata.length()>0)
		{
//			str += string("\n");
			
			string cd=this->cdata;

//	if(nodeName==nodeNameRecording)
//		cout<<"XMLNode:: toString: cdata"<<endl;
			for (map<string,string,xmlNode::compStrByPtr>::const_iterator i=et.begin(); i!=et.end(); i++)
			{
				string::size_type pos=cd.find(i->first, 0);
				// Control d'autocontinguts
				while (pos!=string::npos && cd.substr(pos, i->second.length())==i->second)
					pos=cd.find(i->first, pos+1);

				while (pos!=string::npos)
				{
					cd.replace(pos, i->first.length(), i->second);
					pos=cd.find(i->first, 0);

					// Control d'autocontinguts
					while (pos!=string::npos && cd.substr(pos, i->second.length())==i->second)
						pos=cd.find(i->first, pos+1);
				}
			}
			
//			for (int i = 0; i < indent; i++)
//				str += string("\t");
//			str += string("---- cData ----\n");

//			for (int i = 0; i < indent; i++)
//				str += string("\t");

			str+=StrUtils::trim(cd);
//			if (this->kids.size()>0)
//				str += string("\n");

//			for (int i = 0; i < indent; i++)
//				str += string("\t");
//			str += string("---- //cData ----");
		}
		
		if (this->kids.size()>0)
		{
			list<xmlNode*>::const_iterator i;
			for (i=this->kids.begin(); i!=this->kids.end(); i++)
			{
				str+=string("\n")+(*i)->toString(indent);
			}
			indent--;
			str+=string("\n");
			for (int i = 0; i < indent; i++)
				str += string("\t");
		}
		else
			indent--;
		
		
		str+=string("</")+this->nodeName+string(">");
	}
//	if(nodeName==nodeNameRecording)
//		cout<<"XMLNode:: toString: end"<<endl;

	return str;
}


// Afegeix un subnode al node actual
void xmlNode::addKid(xmlNode *node)
{
	STACKTRACE_INSTRUMENT();
	
	if (node!=NULL)
		this->kids.push_back(node);
}

// Afegeix un subnode al node actual
void xmlNode::removeKid(xmlNode *node)
{
	STACKTRACE_INSTRUMENT();
	
	if(node!=NULL)
	{
		list<xmlNode*>::iterator i;
		for (i=this->kids.begin(); i!=this->kids.end(); i++)
		{
			if(*i==node)
			{
				i=this->kids.erase(i);
				break;
			}
		}
	}
}


// Retorna la llista de subnodes
list<xmlNode*> xmlNode::getKidList(void)
{
	return this->kids;
}

// Afegeix un parametre al node
void xmlNode::setParam(string param, string val)
{
	STACKTRACE_INSTRUMENT();
	
	params[param]=val;
}

// obte un parametre del node
string xmlNode::getParam(string param)
{
	STACKTRACE_INSTRUMENT();
	
	if (params.find(param)!=params.end())
		return params[param];
	
	return string("");
}

bool xmlNode::checkParam(string param)
{
	STACKTRACE_INSTRUMENT();
	
	return (params.find(param)!=params.end());
}

map<string, string> xmlNode::getAllParams()
{
	STACKTRACE_INSTRUMENT();
	
	return params;
}


// esborra un param
void xmlNode::delParam(string param)
{
	STACKTRACE_INSTRUMENT();
	
	if (params.find(param)!=params.end())
		params[param].erase();
}

void xmlNode::deleteAllParams()
{
	STACKTRACE_INSTRUMENT();
	
	params.clear();
}


// Retorna la taula d'encode
map<string,string,xmlNode::compStrByPtr> xmlNode::encodeTable(void)
{
	map<string,string,xmlNode::compStrByPtr> et;

	et["&"]=string("&amp;");
	et["<"]=string("&lt;");
	et[">"]=string("&gt;");
	et["\'"]=string("&apos;");
	et["\""]=string("&quot;");

	return et;
}

// Retorna la taula de decode
map<string,string,xmlNode::compStrByPtr> xmlNode::decodeTable(void)
{
	map<string,string,xmlNode::compStrByPtr> dt;

	dt["&amp;"]=string("&");
	dt["&lt;"]=string("<");
	dt["&gt;"]=string(">");
	dt["&apos;"]=string("\'");
	dt["&quot;"]=string("\"");

	return dt;
}

xmlNode* xmlNode::copy()
{
	STACKTRACE_INSTRUMENT();
	
	xmlNode *res=new xmlNode(this->nodeName);
	res->setCdata(this->cdata);
	
	for (map <string,string>::const_iterator itPar=params.begin(); itPar!=params.end(); itPar++)
	{
		res->setParam(itPar->first, itPar->second);
	}
	
	list<xmlNode*>::iterator i;
	for (i=this->kids.begin(); i!=this->kids.end(); i++)
	{
		res->addKid((*i)->copy());
	}
	return res;
}


// retorna un punter a un node... Especificat per un path i un index si 
// n'hi ha mes d'un al mateix nivell (p.e. <a><b/><b/><b/><b/></a>)
xmlNode *xmlNode::getNode(string path)
{
	STACKTRACE_INSTRUMENT();
	
	list<string> pathNodes=StrUtils::pathToList(path);

	xmlNode *node=this;
	if (node==NULL)//( || node->getName().length()==0)
	{
		return NULL;
	}

	string p=pathNodes.front();
	
	if (p!=string("[0]") && node->getName()!=p)
	{
		return NULL;
	}

	while (pathNodes.size()>1)
	{
		pathNodes.pop_front();
		p=pathNodes.front();
		
		if(node==NULL)
		{
			cout<<" getKidList: node==NULL "<<path<<" p:"<<p<<endl;
			return NULL;
		}
		if(p[0]=='[')
		{
			//cout<<" getKidList: [int] , "<<path<<endl;
			list<xmlNode*> nl=node->getKidList();

			if (nl.size()==0)
			{
				return NULL;
			}
				
			list<xmlNode*>::iterator nodeList=nl.begin();

			p=p.substr(1,p.length()-1);//eliminem '[' ']'
			int i, idx=atoi(p.c_str());
			for(i=0;i<idx && nodeList!=nl.end();i++)
				nodeList++;
			if(i==idx && nl.size()!=0 && nodeList!=nl.end())
				node=(*nodeList);
			else 
			{
				return NULL;
			}
		}
		else
		{
			int index=0;
			string::size_type cPos=p.find("[");
			if(cPos!=string::npos)
			{
				string aux=p.substr(cPos+1,p.length()-1);
				index=atoi(aux.c_str());
				p=p.substr(0,cPos);
			}
			//cout<<" getKidList: <Name> , "<<path<<endl;
			list<xmlNode*> nl=node->getKidList();

			if (nl.size()==0)
			{
				return NULL;
			}
				
			list<xmlNode*>::iterator nodeList=nl.begin();

			bool found=false;
			while (nodeList!=nl.end())
			{
				if ((*nodeList)->getName()==p)
				{
					node=(*nodeList);
					found=true;
					//if (pathNodes.size()==1)	// Estem a l'ultim node
						index--;
					
					if (/*pathNodes.size()>1 || */index<0)
						break;
				}
				nodeList++;
			}

			if (!found || (/*pathNodes.size()==1 &&*/ index>=0))
			{
				return NULL;
			}
		}
	}

	return node;
}

string xmlNode::getNodeData(string path)
{
	STACKTRACE_INSTRUMENT();
	
	xmlNode *n=getNode(path);
	if(n==NULL)
	{
		return string("");
	}
	return n->getCdata();
}

