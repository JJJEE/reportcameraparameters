#ifndef SIRIUS_BASE_XMLPARSER_H_
#define SIRIUS_BASE_XMLPARSER_H_

#include <Utils/debugNew.h>
#include <XML/XML.h>

class xmlParser
{
public:
	// Parsa una string i mira de retornar un doc XML
	static XML *parse(string str, bool includeTags=true);
	// Parse d'una string que ha de ser un node
	static xmlNode *parseNode(string str, bool includeTags=true);
	// Parse d'un fitxer
	static XML *parseFile(string filename, bool includeTags=true);
	// Parse d'una string que son els params d'un node
	static map <string, string> parseParams(string str);
};
#endif

