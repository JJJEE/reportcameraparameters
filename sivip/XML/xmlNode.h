#ifndef SIRIUS_BASE_XMLNODE_H_
#define SIRIUS_BASE_XMLNODE_H_
#include <Utils/debugNew.h>
#include <Utils/StrUtils.h>
#include <string>
#include <list>
#include <map>
using namespace std;

class xmlNode
{
private:
	// Operador per garantir ordre adequat a la encoding/decoding table pq s'apliqui 
	// en ordre
public:/******/
	static struct compStrByPtr
	{
		public:
		bool operator()(const string &s1, const string &s2) const
		{
			return &s1 > &s2;
		}
	} foo;

	class Iterator
	{
		protected:
			string itPath;
			xmlNode *node;

			int nIndices;
						
			int *currentIndices;
			int currentIncrementingIndex;

			int *savedIndices;
			int savedCurrentIncrementingIndex;
			
		private:
			string getTranslatedPath();
			void increment();
			void enter();
			void toSuperNode();
			void save();
			void restore();
			
		public:
			Iterator(xmlNode *node, string wildcardPath);
			virtual ~Iterator();
			bool hasNext();
			xmlNode *next();
	};

protected:
	string nodeName;		// Nom del node
	string cdata;			// Contingut del node
	list<xmlNode*> kids;	// Nodes fills
	map <string, string> params;		// Parametres del node

	// Representacio del node en string, versio interna
	string toString(int indent);
	
public:
	xmlNode(void);
	xmlNode(string name);
	xmlNode(string name, string cdata);
	~xmlNode(void);
	// retorna el nom del node
	virtual string getName(void);
	// setta el nom del node
	virtual void setName(string name);
	// retorna la cdata del  node
	virtual string getCdata(void);
	// setta la cdata
	virtual void setCdata(string cdata);
	// Representacio del node en string
	string toString(void);
	// Afegeix un subnode al node actual
	void addKid(xmlNode *node);
	// Treu un subnode del node actual
	void removeKid(xmlNode *node);
	// Retorna la llista de subnodes
	list<xmlNode*> getKidList(void);
	// Afegeix un parametre al node
	void setParam(string param, string val);
	// obte un parametre del node
	string getParam(string param);
	// comprova si existeix el paràmetre 
	bool checkParam(string param);
	// obte tots els parametres del node
	map<string, string> getAllParams();
	// esborra un param
	void delParam(string param);
	// esborra tots els params
	void deleteAllParams();
	// Retorna la taula d'encode
	static map<string,string,xmlNode::compStrByPtr> encodeTable(void);
	// Retorna la taula de decode
	static map<string,string,xmlNode::compStrByPtr> decodeTable(void);
	//copia
	xmlNode* copy();

	// retorna un punter a un node... Especificat per un path i un index si 
	// n'hi ha mes d'un al mateix nivell (p.e. <a><b/><b/><b/><b/></a>)
	xmlNode *getNode(string path);
	string getNodeData(string path);
};
#endif
