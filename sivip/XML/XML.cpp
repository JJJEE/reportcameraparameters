#include <XML/XML.h>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
using namespace std;

XML::XML(void)
{
	STACKTRACE_INSTRUMENT();
	
	root=new xmlNode();
	if (root==NULL)
	{
		throw;
	}
}

XML::~XML(void)
{
	STACKTRACE_INSTRUMENT();
	
	if (root!=NULL)
		delete root;
	root=NULL;
}

void XML::setRoot(xmlNode *n)
{
	STACKTRACE_INSTRUMENT();
	
	if (root!=NULL)
		delete root;
		
	root=n;
}

xmlNode* XML::getRoot()
{
	STACKTRACE_INSTRUMENT();
	return root;
}

// Retorna un string que representa l'XML
string XML::toString(void)
{
	STACKTRACE_INSTRUMENT();
	
	string str=string("<?xml version=\"1.0\" ?>\n");
	
	for (list<pair<string, map<string, string> > >::const_iterator lIt=procInsts.begin(); lIt!=procInsts.end(); lIt++)
	{
		str+=string("<?")+(*lIt).first;
		
		map<string, string> attrs=(*lIt).second;
		
		for (map<string,string>::const_iterator mIt=attrs.begin(); mIt!=attrs.end(); mIt++)
			str+=string(" ")+mIt->first+string("=\"")+mIt->second+string("\"");
			
		str+=string(" ?>\n");
	}
	
	str+=root->toString();
	return str;
}

// Afegeix una processing instruction
void XML::addProcessingInstruction(string inst, map<string, string> &attribs)
{
	STACKTRACE_INSTRUMENT();
	
	pair<string, map<string, string> > instruction;
	
	instruction.first=inst;
	instruction.second=attribs;

	procInsts.push_back(instruction);
}


// retorna un punter a un node... Especificat per un path i un index si 
// n'hi ha mes d'un al mateix nivell (p.e. <a><b/><b/><b/><b/></a>)
xmlNode *XML::getNode(string path)
{
	STACKTRACE_INSTRUMENT();
	
	if (root==NULL)
		return NULL;

	return root->getNode(path);
	
//	list<string> pathNodes=StrUtils::pathToList(path);
//
//
//	xmlNode *node=root;
//	if (node==NULL || node->getName().length()==0)
//	{
//		return NULL;
//	}
//
//	string p=pathNodes.front();
//	
//	if (p!=string("[0]") &&node->getName()!=p)
//	{
//		return NULL;
//	}
//
//	while (pathNodes.size()>1)
//	{
//		pathNodes.pop_front();
//		p=pathNodes.front();
//		
//		if(node==NULL)
//		{
//			cout<<" getKidList: node==NULL "<<path<<" p:"<<p<<endl;
//			return NULL;
//		}
//		if(p[0]=='[')
//		{
//			//cout<<" getKidList: [int] , "<<path<<endl;
//			list<xmlNode*> nl=node->getKidList();
//
//			if (nl.size()==0)
//			{
//				return NULL;
//			}
//				
//			list<xmlNode*>::iterator nodeList=nl.begin();
//
//			p=p.substr(1,p.length()-1);//eliminem '[' ']'
//			int i, idx=atoi(p.c_str());
//			for(i=0;i<idx && nodeList!=nl.end();i++)
//				nodeList++;
//			if(i==idx && nl.size()!=0 && nodeList!=nl.end())
//				node=(*nodeList);
//			else 
//			{
//				return NULL;
//			}
//		}
//		else
//		{
//			int index=0;
//			string::size_type cPos=p.find("[");
//			if(cPos!=string::npos)
//			{
//				string aux=p.substr(cPos+1,p.length()-1);
//				index=atoi(aux.c_str());
//				p=p.substr(0,cPos);
//			}
//			//cout<<" getKidList: <Name> , "<<path<<endl;
//			list<xmlNode*> nl=node->getKidList();
//
//			if (nl.size()==0)
//			{
//				return NULL;
//			}
//				
//			list<xmlNode*>::iterator nodeList=nl.begin();
//
//			bool found=false;
//			while (nodeList!=nl.end())
//			{
//				if ((*nodeList)->getName()==p)
//				{
//					node=(*nodeList);
//					found=true;
//					//if (pathNodes.size()==1)	// Estem a l'ultim node
//						index--;
//					
//					if (/*pathNodes.size()>1 || */index<0)
//						break;
//				}
//				nodeList++;
//			}
//
//			if (!found || (/*pathNodes.size()==1 &&*/ index>=0))
//			{
//				return NULL;
//			}
//		}
//	}
//
//	return node;
}



string XML::getNodeData(string path)
{
	STACKTRACE_INSTRUMENT();
	
	xmlNode *n=getNode(path);
	if(n==NULL)
	{
		return string("");
	}
	return n->getCdata();
}

