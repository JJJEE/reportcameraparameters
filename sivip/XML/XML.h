#ifndef SIRIUS_BASE_XML_H_
#define SIRIUS_BASE_XML_H_

#include <Utils/debugNew.h>
#include <XML/xmlNode.h>
#include <list>

using namespace std;

class XML
{
protected:
	xmlNode *root;
	
	list<pair<string, map<string, string> > > procInsts;
	
public:
	// Nodes que estan a l'arrel del document XML

	XML(void);
	~XML(void);
	
	// Assigna un nou node arrel diferent...
	void setRoot(xmlNode *n);

	xmlNode* getRoot();
	
	// Retorna un string que representa l'XML
	string toString(void);
	
	// Afegeix una instruccio de processament (p.e. xml-stylesheet)
	void addProcessingInstruction(string inst, map<string, string> &attribs);
	
	// retorna un punter a un node... Especificat per un path i un index si 
	// n'hi ha mes d'un al mateix nivell (p.e. <a><b/><b/><b/><b/></a>)
	xmlNode *getNode(string path);
	string getNodeData(string path);
};

#endif

