#include <XML/xmlParser.h>
#include <XML/xmlNode.h>
#include <Utils/StrUtils.h>
#include <Utils/Exception.h>
#include <Utils/FileException.h>
#include <iostream>
#include <algorithm>
#include <Utils/debugStackTrace.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

XML *xmlParser::parse(string str, bool includeTags)
{
	XML *xml=new XML();

	string s=StrUtils::trim(str);

	int xmlTag=0;
	string tr("");
	do
	{
		s=s.substr(xmlTag);
		tr=s.substr(0, 5);
		transform(tr.begin(), tr.end(), tr.begin(), ::tolower);
		xmlTag++;
	}while(tr!="<?xml" && s.size()>5);
	
	if (tr!="<?xml")
	{
		delete xml;
		cout<<" Parse 1:"<<str<<endl<<" trim:"<<s<<"tr:-"<<tr<<"- "<<tr.size()<<endl;
		return NULL;
	}
	
	string::size_type endDef=s.find("?>", 5);

	if (endDef==string::npos)
	{
		delete xml;
		cout<<" Parse 2:"<<str<<endl<<" trim:"<<s<<"tr:-"<<tr<<"- "<<tr.size()<<endl;
		return NULL;
	}

	string def=s.substr(0, endDef+2);

	s=s.substr(endDef+2);

	// TODO: Treure mes headers, i guardar-se'ls (de l'estil de <?xml-stylesheet)
	// PARCIAL 20080123 - eliminem tots els <? ?> pero no els guardem
	string::size_type startProcInst=s.find("<?");
	string::size_type endProcInst;
	while (startProcInst!=string::npos)
	{
		endProcInst=s.find("?>", startProcInst+2);
		
		if (endProcInst==string::npos)
		{
			delete xml;
			cout<<" Parse 3: Unclosed Processing instruction (starting at " << s.substr(startProcInst) << ")" << endl;
			return NULL;
		}
		
		s=s.substr(0, startProcInst)+s.substr(endProcInst+2);
		startProcInst=s.find("<?");
	}

	// Eliminem comentaris
	string::size_type startComm=s.find("<!--");
	string::size_type endComm;
	while (startComm!=string::npos)
	{
		endComm=s.find("-->", startComm+4);
		
		if (endComm==string::npos)
		{
			delete xml;
			cout<<" Parse 3: Unclosed comment (starting at " << s.substr(startComm) << endl;
			return NULL;
		}
		
		s=s.substr(0, startComm)+s.substr(endComm+3);
		startComm=s.find("<!--");
	}

	// Parse del node root
	xmlNode *node=xmlParser::parseNode(s, includeTags);

	xml->setRoot(node);

	return xml;
}

// Parse d'una string que ha de ser un node
xmlNode *xmlParser::parseNode(string str, bool includeTags)
{
	xmlNode *currNode=NULL;
	string tag("");
	try
	{
		string s=str;

		string::size_type ini=s.find(string("<"));

		if (ini==string::npos)
		{
			return NULL;
		}

		ini++;

		string::size_type sp=s.find(string(" "), ini);
		string::size_type barra=s.find(string("/"), ini);
		string::size_type tanca=s.find(string(">"), ini);

		if (sp==string::npos) sp=s.length();
		if (barra==string::npos) barra=s.length();
		if (tanca==string::npos) tanca=s.length();

		while(barra<tanca-1 && barra!=string::npos)
			 barra=s.find(string("/"), barra+1);

		if (barra==string::npos) barra=s.length();

		string::size_type end = s.length();

		if (sp<barra && sp<tanca)
			end=sp;
		else if (barra<sp && barra<tanca)
			end=barra;
		else if (tanca<barra && tanca<sp)
			end=tanca;

		tag=s.substr(ini, end-ini);

		// Agafem els params, que parsarem despres...
		string::size_type iniParams=end;
		string::size_type endParams=(barra==tanca-1 && barra>=end?barra:tanca);
		
		string paramsStr=StrUtils::trim(s.substr(iniParams, endParams-iniParams));
		
	//	cout << "paramsStr: " << paramsStr << endl;
		
		map <string, string> xmlNodeParams=xmlParser::parseParams(paramsStr);
		
	//	cout << "Searching For Tag " << tag << endl;

		string tagConts;
		tagConts=StrUtils::getFirstTagContents(s, tag);
		string subTags;
		subTags=StrUtils::getSubTags(tagConts, tag);

	//	cout << "Conts " << tagConts << endl;
	//	cout << "subTags " << subTags << endl;
		
		string currTag=tagConts;

		list <string> subTagList;

		string firstSubTag=StrUtils::getFirstTagContents(subTags);
		
		while (firstSubTag.length()>0)
		{
			subTagList.push_back(firstSubTag);
			subTags.replace(subTags.find(firstSubTag), firstSubTag.length(), string(""));
			currTag.replace(currTag.find(firstSubTag), firstSubTag.length(), string(""));
			firstSubTag=StrUtils::getFirstTagContents(subTags);
		}

	//	cout << "* CurrTag: " << currTag << endl;

		currNode=new xmlNode();
		currNode->setName(tag);
		
		for (map <string, string>::const_iterator itPar=xmlNodeParams.begin();
			 itPar!=xmlNodeParams.end(); itPar++)
			currNode->setParam((*itPar).first, (*itPar).second);
		
		string cd=StrUtils::getFirstTagContents(currTag, false);
	//	cout << "CData: " << cd << endl;
		// Decode de la cdata
		map<string,string,xmlNode::compStrByPtr> dt=xmlNode::decodeTable();

		map<string,string,xmlNode::compStrByPtr>::const_iterator i;

		for (i=dt.begin(); i!=dt.end(); i++)
		{
			string::size_type pos=cd.find(i->first, 0);
			// Control d'autocontinguts
			while (pos!=string::npos && cd.substr(pos, i->second.length())==i->second)
				pos=cd.find(i->first, pos+1);

			while (pos!=string::npos)
			{
				cd.replace(pos, i->first.length(), i->second);
				pos=cd.find(i->first, 0);

				// Control d'autocontinguts
				while (pos!=string::npos && cd.substr(pos, i->second.length())==i->second)
					pos=cd.find(i->first, pos+1);
			}
		}

		string cdTrimmed=StrUtils::trim(cd);
		currNode->setCdata(cdTrimmed);
		
	//	cout << "* subTags: " << endl;
		
		for (list<string>::iterator strIt=subTagList.begin(); strIt!=subTagList.end(); strIt++)
		{
	//		cout << "\t- " << (*strIt) << endl;
			xmlNode *subNode=parseNode((*strIt), includeTags);

			currNode->addKid(subNode);
		}
	}
	catch(Exception e)
	{
		if (currNode!=NULL)
			delete currNode;
			
//		cout<<" ParseNode 2 tag:"<<tag<<" Error:"<<e.getMsg()<<" include:"<<includeTags<<endl;
//		cout<<" ParseNode 2 str: * * * "<<str<<" * * * "<<endl;
		if(includeTags)
		{
			xmlNode *res=parseNode(e.getMsg(), includeTags);
			res->setCdata(string("error :")+tag+string(":")+res->getCdata());
			currNode=res;
		}
		else
		{
			//e.serialize()->materializeAndThrow();
			throw;
		}
	}
	return currNode;
}

XML *xmlParser::parseFile(string file, bool includeTags)
{
	FILE *f=fopen(file.c_str(), "rb");

	if (f==NULL)
		throw FileException(string("File ") + file + string(" not found"));

	fseek(f, 0, SEEK_END);
	int len=ftell(f);
	fseek(f, 0, SEEK_SET);
	
	char *buf = new char[len+1];
	fread(buf, len, 1, f);
	buf[len]='\0';
	fclose(f);

	XML *xml=xmlParser::parse(string(buf, len), includeTags);

	delete[] buf;

	return xml;
}

map <string, string> xmlParser::parseParams(string str)
{
	map <string, string> params;
	
	string paramsStr=StrUtils::trim(str);

	while (paramsStr.length()>0)
	{
		string::size_type igual=paramsStr.find("=");
		
		if (igual==string::npos)
			break;
		
		// agafem el nom
		string nom=StrUtils::trim(paramsStr.substr(0, igual));
		
		string::size_type cometes=paramsStr.find("\"", igual);
		if (cometes==string::npos)
			break;
		string::size_type cometesTanca=paramsStr.find("\"", cometes+1);
		if (cometesTanca==string::npos)
			break;
		
		string val=paramsStr.substr(cometes+1, cometesTanca-cometes-1);
		
		map<string,string,xmlNode::compStrByPtr> dt=xmlNode::decodeTable();
		
		for (map<string,string,xmlNode::compStrByPtr>::const_iterator i=dt.begin(); i!=dt.end(); i++)
		{
			string::size_type pos=val.find(i->first, 0);
			// Control d'autocontinguts
			while (pos!=string::npos && val.substr(pos, i->second.length())==i->second)
				pos=val.find(i->first, pos+1);
			
			while (pos!=string::npos)
			{
				val.replace(pos, i->first.length(), i->second);
				pos=val.find(i->first, 0);
				
				// Control d'autocontinguts
				while (pos!=string::npos && val.substr(pos, i->second.length())==i->second)
					pos=val.find(i->first, pos+1);
			}
		}
		
		params[nom]=val;
		
		paramsStr=StrUtils::trim(paramsStr.substr(cometesTanca+1));
	}

	return params;
}

