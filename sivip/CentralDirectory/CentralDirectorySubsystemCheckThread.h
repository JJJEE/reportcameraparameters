#ifndef __SIRIUS__BASE__CENTRALDIRECTORYSUBSYSTEMCHECKTHREAD_H
#define __SIRIUS__BASE__CENTRALDIRECTORYSUBSYSTEMCHECKTHREAD_H

#include <ModuleInterface/ModuleInterface.h>
#include <Threads/Thread.h>
//#include <CentralDirectory/CentralDirectory.h>

class CentralDirectory; 

class CentralDirectorySubsystemCheckThread: public Thread
{
protected:
	ModuleInterface *modInt;
	CentralDirectory *central;
	bool run;
	int runningId;
	
public:
	 CentralDirectorySubsystemCheckThread(ModuleInterface *intf, CentralDirectory *central);
	
	void startRunning();
	void stopRunning();
	
	void *execute(int id, void *args);
};

#endif
