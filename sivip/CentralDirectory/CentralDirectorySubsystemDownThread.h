#ifndef __SIRIUS__BASE__CENTRALDIRECTORYSUBSYSTEMDOWNTHREAD_H
#define __SIRIUS__BASE__CENTRALDIRECTORYSUBSYSTEMDOWNTHREAD_H

#include <ModuleInterface/ModuleInterface.h>
#include <Threads/Thread.h>

class CentralDirectory; 

class CentralDirectorySubsystemDownThread : public Thread
{
protected:
	ModuleInterface *modInt;
	CentralDirectory *central;
	bool run;
	int runningId;
	
public:
	CentralDirectorySubsystemDownThread(ModuleInterface *intf, CentralDirectory *central);
	
	void startRunning();
	void stopRunning();
	
	void *execute(int id, void *args);
};

#endif
