#ifndef __SIRIUS__BASE__CENTRALDIRECTORY_H
#define __SIRIUS__BASE__CENTRALDIRECTORY_H

#include <Utils/debugNew.h>
#include <Module/Module.h>
#include <CentralDirectory/CentralDirectorySubsystemRegisterThread.h>
#include <CentralDirectory/CentralDirectorySubsystemDownThread.h>
#include <CentralDirectory/CentralDirectorySubsystemCheckThread.h>

class CentralDirectory : public Module
{
	friend class CentralDirectorySubsystemRegisterThread;
	friend class CentralDirectorySubsystemCheckThread;
protected:	
	// Thread de registre de subsistemes
	CentralDirectorySubsystemRegisterThread *subsystemRegisterThread;
	// Thread de deteccio de subsistemes caiguts
	CentralDirectorySubsystemDownThread *subsystemDownThread;
	// Thread de deteccio de subsistemes que no responen
	CentralDirectorySubsystemCheckThread *subsystemCheckThread;
	// subsistemes que no responen
	map<string, bool> deadSubsystems;
	RWLock dssLock;
	
public:
	CentralDirectory(Address cdAddr, short cdType, Canis *cn=NULL);
	CentralDirectory(string xmlFile);
	
	virtual void serveStartUp();	
	virtual void serveShutDown();	
};

#endif
