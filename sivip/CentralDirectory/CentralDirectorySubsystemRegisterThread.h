#ifndef __SIRIUS__BASE__CENTRALDIRECTORYSUBSYSTEMREGISTERTHREAD_H
#define __SIRIUS__BASE__CENTRALDIRECTORYSUBSYSTEMREGISTERTHREAD_H

#include <ModuleInterface/ModuleInterface.h>
//#include <CentralDirectory/CentralDirectory.h>
#include <Threads/Thread.h>

class CentralDirectory; 
class CentralDirectorySubsystemRegisterThread : public Thread
{
protected:
	ModuleInterface *modInt;
	CentralDirectory *central;
	bool run;
	int runningId;
	
public:
	CentralDirectorySubsystemRegisterThread(ModuleInterface *intf, CentralDirectory *central);
	
	void startRunning();
	void stopRunning();
	
	void *execute(int id, void *args);
};

#endif
