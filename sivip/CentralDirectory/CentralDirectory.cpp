#include <CentralDirectory/CentralDirectory.h>
#include <CentralDirectory/CentralDirectoryInterface.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Canis.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <string>
#include <cctype>
#include <algorithm>
#include <iostream>

using namespace std;

CentralDirectory::CentralDirectory(Address cdAddr, short cdType, Canis *cn) : Module(cdAddr, cdType, cn) {
	STACKTRACE_INSTRUMENT();

	__class=string("CentralDirectory");
	moduleInterface=(ModuleInterface*)new CentralDirectoryInterface(this->address, this->type, this->cn);
}

CentralDirectory::CentralDirectory(string xmlFile) : Module(xmlFile) {
	STACKTRACE_INSTRUMENT();

	__class=string("CentralDirectory");
	moduleInterface=(ModuleInterface*)new CentralDirectoryInterface(this->address, this->type, this->cn);
}

void CentralDirectory::serveStartUp()
{
	STACKTRACE_INSTRUMENT();
	
	this->subsystemRegisterThread = 
		new CentralDirectorySubsystemRegisterThread(moduleInterface, this);
	if (this->subsystemRegisterThread==NULL)
	{
		throw NotEnoughMemoryException("Not enough memory to create "
			"subsystem register thread");
	}
	
	this->subsystemDownThread = 
		new CentralDirectorySubsystemDownThread(moduleInterface, this);
	if (this->subsystemDownThread==NULL)
	{
		throw NotEnoughMemoryException("Not enough memory to create "
			"subsystem down thread");
	}

	this->subsystemCheckThread = 
		new CentralDirectorySubsystemCheckThread(moduleInterface, this);
	if (this->subsystemCheckThread ==NULL)
	{
		throw NotEnoughMemoryException("Not enough memory to create "
			"subsystem register thread");
	}

	this->subsystemRegisterThread->startRunning();
	this->subsystemDownThread->startRunning();
	this->subsystemCheckThread->startRunning();
}

void CentralDirectory::serveShutDown()
{
	STACKTRACE_INSTRUMENT();
	
	if (this->subsystemRegisterThread!=NULL)
	{
		this->subsystemRegisterThread->stopRunning();
		delete this->subsystemRegisterThread;
		this->subsystemRegisterThread=NULL;
	}
	
	if (this->subsystemDownThread!=NULL)
	{
		this->subsystemDownThread->stopRunning();
		delete this->subsystemDownThread;
		this->subsystemDownThread=NULL;
	}

	if (this->subsystemCheckThread!=NULL)
	{
		this->subsystemCheckThread->stopRunning();
		delete this->subsystemCheckThread;
		this->subsystemCheckThread=NULL;
	}
}
