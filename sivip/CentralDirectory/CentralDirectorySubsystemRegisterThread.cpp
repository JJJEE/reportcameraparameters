#include <CentralDirectory/CentralDirectorySubsystemRegisterThread.h>
#include <CentralDirectory/CentralDirectory.h>
#include <Module/Module.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Canis.h>
#include <Utils/DBGateway.h>
#include <Utils/StrUtils.h>
#include <Utils/Log.h>
#include <Utils/Timer.h>
#include <stdlib.h>

extern Log *centralDirLog;

CentralDirectorySubsystemRegisterThread::CentralDirectorySubsystemRegisterThread(ModuleInterface *intf, CentralDirectory *central) : modInt(intf), central(central), run(false), runningId(-1)
{
	STACKTRACE_INSTRUMENT();
	
	srand(time(NULL));
}

void CentralDirectorySubsystemRegisterThread::startRunning()
{
	STACKTRACE_INSTRUMENT();
	
	// Ens assegurem que no queda cap executant-se
	this->stopRunning();
	
	// Iniciem un de nou
	this->run=true;
	this->runningId=this->start();
}

void CentralDirectorySubsystemRegisterThread::stopRunning()
{
	STACKTRACE_INSTRUMENT();
	
	this->run=false;
	// Ens assegurem que el thread acaba
	if (this->runningId!=-1)
		this->join(this->runningId);
}

void *CentralDirectorySubsystemRegisterThread::execute(int id, void *args)
{
	STACKTRACE_INSTRUMENT();
	
	list<Canis::subsys> *systems=NULL;
	bool firstRun=true;
	DBGateway *db=NULL;
	bool dbJustCreated=false;
	map<string, bool> *current=NULL, *previous=NULL;
	short interfaceTypeID = -100;
	
	while (this->run)
	{
		while (!Module::isActive())
		{
			if(current != NULL)
			{
				current->clear();
				delete current;
				current = NULL;
			}

			sleep(1);
			
			// Si s'ha activat, ens esperem "per estar segur que som unics"
			// i no registrar coses abans de temps :P
			if (Module::isActive())
				sleep(15);
		}

		if(previous != NULL)
		{
			previous->clear();
			delete previous;
		}
		previous = current;
		current = new map<string, bool>;


		if (systems!=NULL)
		{
			delete systems;
			systems=NULL;
		}
		
		centralDirLog->logLine(string("CenDirSubSysRegThr: --> Starting "
			"iteration"));
	
		centralDirLog->logLine(string("CenDirSubsysRegThr: Getting Canis "
			"copy"));
		systems=this->modInt->getCanis()->getSysCopy();
		centralDirLog->logLine(string("CenDirSubsysRegThr: Got Canis "
			"copy"));
		
		if (db==NULL)
		{
			db=new DBGateway(this->modInt->getAddress(), 
				this->modInt->getType(), this->modInt->getCanis());
			dbJustCreated=true;
		}
		RPC *dbRPC=db->getRPC();

		XML *xml=NULL;
		
		int infobalanceoClean=rand()%1000;

		// Preparem el query de LOCK i clean si cal
		string centralDirectoryLockAndCleanQuery("");

		if (infobalanceoClean<10 || firstRun)
		{
			// Amb un 1% de probabilitats, fem neteja de informacions de
			// balanceig velles

			// Si hem entrat perque es la primera execucio, evitem entrar-hi
			// mes cops pel mateix motiu :)
			firstRun=false;

			centralDirLog->logLine(string("CenDirSubsysRegThr: Cleaning old "
				"infobalanceo entries")); //  (") + t.getStartSecondsString() + string(")"));

			centralDirectoryLockAndCleanQuery+=
				string("DELETE FROM infobalanceo_tmp WHERE "
					"now()-ultimamodificacion>=(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo_tmp);");
		}

		centralDirectoryLockAndCleanQuery+=string(" LOCK TABLE infobalanceo IN EXCLUSIVE MODE; ");

		
		// Preparem el query de sincronitzacio d'infobalanceo
		string centralDirectoryInfoBalanceoSync("SELECT syncInfoBalanceo();");

		// Preparem el query de registre de subsistemes
		string centralDirectoryUpdateQuery=string("SELECT ");

		this->central->dssLock.rlock();
		bool wlocked=false;
		if(interfaceTypeID <0)
		{
			try
			{
				interfaceTypeID = ServiceFinder::resolveSubsystemTypeName("InterfaceModule", this->central->cn);
			}catch(...){}
		}
		for (list<Canis::subsys>::iterator it=systems->begin(); 
			 it!=systems->end(); it++)
		{
			IP subSysIP((*it).ip);
			Address subSysAddr(subSysIP, (*it).port);

			current->insert(pair<string, bool>(subSysAddr.toString(), true));

			word port = (*it).port;
			short subSysType=(*it).type;
			float subSysLoad=(*it).load;

			if (db==NULL)
			{
				centralDirLog->logLine(string("CenDirSubsysRegThr: DBGateway "
					"is NULL - recreating"));
				db=new DBGateway(this->modInt->getAddress(), 
					this->modInt->getType(), this->modInt->getCanis());
				dbJustCreated=true;
			}

			map<string, bool>::iterator ssIt = this->central->deadSubsystems.find(subSysAddr.toString());
//			if(ssIt==this->central->deadSubsystems.end())
//				cout<<"CenDirSubsysRegThr "<<subSysAddr.toString()<<" ssId==map.end()"<<endl;
//			else
//				cout<<"CenDirSubsysRegThr "<<subSysAddr.toString()<<" ssId:"<<ssIt->second<<endl;
			if(ssIt != this->central->deadSubsystems.end() && ssIt->second==true)
			{
				string logLine = string("Canis: not registering:") + 
					StrUtils::decToString(subSysType) + string(" @ ") +
					subSysIP.toString() + string(":") +
					StrUtils::decToString(port)+string(": subsystem not responding");
				centralDirLog->logLine(string("CenDirSubsysRegThr: ") +
						logLine);
			}
			else
			{
				if(ssIt != this->central->deadSubsystems.end() && ssIt->second==false)
				{
					centralDirectoryUpdateQuery += string("clearNonMatchingNetworkNearestSubsys(('"+subSysIP.toString()+string("/")+StrUtils::decToString(this->central->cn->getInterfaceMask())+"')::inet), ");
					if(!wlocked)
					{
						this->central->dssLock.unlock();
						this->central->dssLock.wlock();
						wlocked=true;
					}
					this->central->deadSubsystems.erase(ssIt);
					//this->central->dssLock.unlock();
					//this->central->dssLock.rlock();
				}
				else if( subSysType != interfaceTypeID && previous != NULL && previous->find(subSysAddr.toString()) == previous->end())
				{
					centralDirLog->logLine(
					string("new subsystem found ")+subSysAddr.toString()+string(" :")+string("clearNonMatchingNetworkNearestSubsys(('"+subSysIP.toString()+string("/")+StrUtils::decToString(this->central->cn->getInterfaceMask())+"')::inet), "));
					centralDirectoryUpdateQuery += string("clearNonMatchingNetworkNearestSubsys(('"+subSysIP.toString()+string("/")+StrUtils::decToString(this->central->cn->getInterfaceMask())+"')::inet), ");
				}
				

				string logLine = string("Canis: ") + 
					StrUtils::decToString(subSysType) + string(" @ ") +
					subSysIP.toString() + string(":") +
					StrUtils::decToString(port);

				centralDirLog->logLine(string("CenDirSubsysRegThr: ") +
						logLine);

				//			string q=string("(SELECT * FROM "
				//				"registerSubsystemAndUpdateInfoBalanceo(") + 
				//				StrUtils::decToString(subSysType) + string(", '") +
				//				subSysIP.toString() + string("', ") +
				//				StrUtils::decToString(port) + string(", ") +
				//				StrUtils::floatToString(subSysLoad) + string(")) UNION ");

				string q=string("registerSubsystemAndUpdateInfoBalanceo(") + 
					StrUtils::decToString(subSysType) + string(", '") +
					subSysIP.toString() + string("', ") +
					StrUtils::decToString(port) + string(", ") +
					StrUtils::floatToString(subSysLoad) + string("), ");

				centralDirectoryUpdateQuery+=q;
			}
		}
		this->central->dssLock.unlock();

//		centralDirectoryUpdateQuery+=string("resetAllDeltaCarga()");

		bool beginDone=false, rpcDeleted=false;
		Timer t, tLock;
		t.start();
		tLock.start();

		if (centralDirectoryUpdateQuery.length() > 7)
		{
			centralDirectoryUpdateQuery = centralDirectoryUpdateQuery.substr(0, centralDirectoryUpdateQuery.length()-2);
		
			// Executem el query de registre de subsistemes
			try
			{
				if(dbJustCreated)
				{
					centralDirectoryUpdateQuery += string(", clearNonMatchingNetworkNearestSubsys()");
				}
				xml=db->query(centralDirectoryUpdateQuery, dbRPC);
				beginDone=false;
				dbJustCreated=false;
			}
			catch (Exception &e)
			{
				if (xml!=NULL)
				{
					delete xml;
					xml=NULL;
				}
//				try
//				{
//					if (beginDone)
//						db->rollback(dbRPC);
//				}
//				catch (Exception &rollbackEx)
//				{
//				}
				db->delRPC(dbRPC);
				rpcDeleted=true;
				centralDirLog->logLine(string("CenDirSubsysRegThr: ") +
					e.getClass() + string(": ") + e.getMsg()+string(" in ") +
					centralDirectoryUpdateQuery);
				continue;
			}

			tLock.start();
			// Executem el query de LOCK i clean de subsistemes 
			XML *xmlLock=NULL;
			try
			{
				db->begin(dbRPC);
				beginDone=true;
				xmlLock=db->update(centralDirectoryLockAndCleanQuery, dbRPC);
				centralDirLog->logLine(string("CenDirSubsysRegThr: LOCKED"));
				if (xmlLock!=NULL)
				{
					delete xmlLock;
					xmlLock=NULL;
				}
				xmlLock=db->query(centralDirectoryInfoBalanceoSync, dbRPC);
				db->commit(dbRPC);
				beginDone=false;
			}
			catch (Exception &e)
			{
				try
				{
					if (beginDone)
						db->rollback(dbRPC);
				}
				catch (Exception &rollbackEx)
				{
				}
				db->delRPC(dbRPC);
				rpcDeleted=true;
				beginDone=false;
				centralDirLog->logLine(string("CenDirSubsysRegThr: ")+
					e.getClass()+string(": ")+e.getMsg()+string(" in ")+centralDirectoryLockAndCleanQuery);
				continue;
			}
			
			if (xmlLock!=NULL)
			{
				delete xmlLock;
				xmlLock=NULL;
			}
		}
		double startT=t.getStartSeconds();
		double lockStartT=tLock.getStartSeconds();
		t.start();
		double nowT=t.getStartSeconds();
		centralDirLog->logLine(string("CenDirSubsysRegThr: Iteration ran for ")
			+ StrUtils::floatToString(nowT-startT) + string(" seconds."));
		centralDirLog->logLine(string("CenDirSubsysRegThr: Iteration was locked"
			" for ") + StrUtils::floatToString(nowT-lockStartT) + 
			string(" seconds."));
		
		if (nowT-startT > 7.5)
			centralDirLog->logLine(string("CenDirSubsysRegThr: WARNING: "
				"7.5s limit trespassed!"));

		// No hauria de ser NULL pero comprovem per si de cas
		if (xml!=NULL)
		{
//			int nRows=0;
//			xmlNode *resNode=xml->getNode("/result");
//			if (resNode!=NULL)
//				nRows=atoi(resNode->getParam("numRows").c_str());
//			
//			for (int r=0; r<nRows; r++)
//			{
//				string logLine=xml->getNode(string("/result/[") + 
//					StrUtils::decToString(r) + 
//					string("]/[0]"))->getCdata();
//
//				centralDirLog->logLine(string("CenDirSubsysRegThr: ") +
//					logLine);
//			}
	
			delete xml;
			xml=NULL;
		}

		if (!rpcDeleted)
			db->freeRPC(dbRPC);
		else
		{
			delete db;
			db=NULL;
		}
		
		// Sleep interruptible
		// Fem un sleep en funcio del que triguem a processar el query:
		// 		- Un maxim de 15s
		//		- Un minim de 5s
		//		- El numero de segons que s'ha trigat a processar la info,
		//			sempre que estigui entre 5 i 15.
		// La idea es que com que ara el que es important es que els moduls
		// estiguin updatats els ultims 15s updatats i no els ultims 15s reals
		// no passa res si de tant en tant triguem mes a iterar
		// Ho fem aixi perque si ha trigat en processar-se el query igual es
		// que la BD esta una mica carregada, i li donem temps a processar
		// queries
		for (int s=0; s<15 && (s<(int)(nowT-startT) || s<5) && this->run; s++)
			sleep(1);
	}

	if(current != NULL)
	{
		current->clear();
		delete current;
		current = NULL;
	}

	if(previous != NULL)
	{
		previous->clear();
		delete previous;
		previous=NULL;
	}

	if (db!=NULL)				
		delete db;
		
	return NULL;
}
