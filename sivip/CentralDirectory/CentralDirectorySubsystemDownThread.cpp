#include <CentralDirectory/CentralDirectorySubsystemDownThread.h>
#include <CentralDirectory/CentralDirectory.h>
#include <Module/Module.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Canis.h>
#include <Utils/DBGateway.h>
#include <Utils/StrUtils.h>
#include <Utils/Log.h>
#include <Utils/Timer.h>
#include <stdlib.h>

extern Log *centralDirLog;

CentralDirectorySubsystemDownThread::CentralDirectorySubsystemDownThread(ModuleInterface *intf, CentralDirectory *central) : modInt(intf), central(central), run(false), runningId(-1)
{
	STACKTRACE_INSTRUMENT();
	
	srand(time(NULL));
}

void CentralDirectorySubsystemDownThread::startRunning()
{
	STACKTRACE_INSTRUMENT();
	
	// Ens assegurem que no queda cap executant-se
	this->stopRunning();
	
	// Iniciem un de nou
	this->run=true;
	this->runningId=this->start();
}

void CentralDirectorySubsystemDownThread::stopRunning()
{
	STACKTRACE_INSTRUMENT();
	
	this->run=false;
	// Ens assegurem que el thread acaba
	if (this->runningId!=-1)
		this->join(this->runningId);
}

void *CentralDirectorySubsystemDownThread::execute(int id, void *args)
{
	STACKTRACE_INSTRUMENT();
	
	// Esperem un temps prudencial, perque el thread de registre pugui fer
	// el primer delete i no anunciem subsistemes caiguts que en realitat
	// no ho estan
	sleep(30);
	
	DBGateway *db=NULL;
	AlarmModuleAccess *ama=NULL;
	AMAlarmId ssAid;
	ssAid.devId=-1;
	ssAid.type=AMAlarmId::Filter;
	ssAid.strId="Subsystem down";//"Subsystem of a down"
	ssAid.intId=-1;
	
	while (this->run)
	{
		while (!Module::isActive())
			sleep(1);
	
		Timer t;
		t.start();
		
		centralDirLog->logLine(string("CenDirSubSysDownThr: --> Starting "
			"iteration"));
			
		if (db==NULL)
			db=new DBGateway(this->modInt->getAddress(), 
				this->modInt->getType(), this->modInt->getCanis());
		XML *xml=NULL;
		
		// Comprovem si ha caigut algún servidor
		// TODO 20090729: Entendre, optimitzar i refer, si es pot.
//		string q = string("select serv.ip from (select count(*), ip from (select * from infobalanceo) as ib left outer join subsistema s on (ib.subsistema = s.id) group by ip) AS serv")+ 
//			string(" left outer join (select count(*), ip from (select * from infobalanceo where 'now()'-ultimamodificacion>(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo)) as ib left outer join subsistema s on (ib.subsistema = s.id) group by ip ) AS out ")+ 
//			string("on (serv.ip = out.ip) where serv.count = out.count");

		string q=string("SELECT servout.ip FROM (SELECT COUNT(*) AS servcnt, ip FROM infobalanceo ibserv LEFT OUTER JOIN subsistema sserv ON (ibserv.subsistema = sserv.id), tiposubsistema ts WHERE sserv.tipo = ts.id AND ts.nombre<>'InterfaceModule' AND ts.nombre<>'VideowallModule' GROUP BY ip) AS servall, (SELECT COUNT(*) AS servcnt, sout.ip FROM (SELECT * FROM infobalanceo ibmod WHERE 'now()'-ibmod.ultimamodificacion>(SELECT MIN(now()-ibmod2.ultimamodificacion)+'15 seconds' FROM infobalanceo ibmod2)) AS ibout, subsistema sout, tiposubsistema ts WHERE  ibout.subsistema = sout.id AND sout.tipo = ts.id AND ts.nombre<>'InterfaceModule' AND ts.nombre<>'VideowallModule' GROUP BY sout.ip) AS servout WHERE servall.ip = servout.ip AND servall.servcnt = servout.servcnt;");

		try
		{
			centralDirLog->logLine(string("CenDirSubSysDownThr: Down services "
				"query"));
//			cout<<"checking servers"<<endl;
			xml=db->query(q);
			centralDirLog->logLine(string("CenDirSubSysDownThr: Down services "
				"query DONE"));

//				cout<<"query"<<endl;

			if (xml!=NULL)
			{
				xmlNode *n=xml->getNode("/result");
				if (n!=NULL)
				{
					int rows=atoi((n->getParam("numRows")).c_str());
					for(int i=0;i<rows;i++)
					{
		//				cout<<"row"<<endl;
						n=xml->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/ip"));
						if(n!=NULL)
						{
							string s= n->getCdata();
							int pos=s.find("/");
							if(pos!=string::npos)
							{
								s=s.substr(0,pos);
							}
		
							IP ip(s);
							centralDirLog->logLine(string("CenDirSubSysDownThr: Subsystem down: ") + ip.toString());
		
							if(ama == NULL)
								ama = new AlarmModuleAccess(
									this->modInt->getAddress(), 
									this->modInt->getType(),
									this->modInt->getCanis());
		
							if(!ama->sesStarted())
							{
								try
								{
									cout << "Starting AlarmModuleAccess "
										"session -1" << endl;
									ama->startSession(-1);
								}
								catch(Exception e)
								{
									cout << "Error while starting session "
										"with System Alms: " << e.getClass()
										<< ": " << e.getMsg() << endl;
								}
							}
							if(ama->sesStarted())
							{
								ssAid.strId = ("ModuleAlarm.CentralDirectory.subsystem_down.") + ip.toString() + string(":")+central->getAddress().getIP().toString()+string("|")+StrUtils::decToString(central->getAddress().getPort())+string(":subsystem_down.") + ip.toString();

								AMAlarmValue av;
								av.alarmId=ssAid;
								av.value = 1;//ip.toDWord();
								av.raised = true;
								cout<<" AlarmModuleAccess->setAlarm() "<<ssAid.strId<<" id:"<<av.alarmId.devId<<endl;
								ama->setAlarm(av);
							}
						}
					}
				}
			}
		}
		catch (Exception &e)
		{
			//centralDirLog->logLine(string("CenDirSubsysRegThr: ")+
			//	e.getClass()+string(": ")+e.getMsg()+string(" in ")+q);
			cout<<(string("CenDirSubSysDownThr: ")+
				e.getClass()+string(": ")+e.getMsg()+string(" in ")+q)<<endl;
		}
		
		if (xml!=NULL)
			delete xml;
		
		double startT=t.getStartSeconds();
		t.start();
		double nowT=t.getStartSeconds();
		centralDirLog->logLine(string("CenDirSubSysDownThr: Iteration ran for ")
			+ StrUtils::floatToString(nowT-startT) + string(" seconds."));
		
		if (nowT-startT > 7.5)
			centralDirLog->logLine(string("CenDirSubSysDownThr: WARNING: "
				"7.5s limit trespassed!"));
		
		// Sleepem una estona ;)
		for (int s=0; s<60 && this->run; s++)
			sleep(1);
	}

	if (db!=NULL)				
		delete db;
		
	return NULL;
}
