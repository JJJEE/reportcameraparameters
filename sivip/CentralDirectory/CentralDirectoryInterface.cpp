#include <CentralDirectory/CentralDirectoryInterface.h>
#include <Utils/DBGateway.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Log.h>
#include <Utils/StrUtils.h>
#include <Utils/Exception.h>
#include <Utils/ServiceException.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

extern Log *centralDirLog;

RPCPacket* CentralDirectoryInterface::NoSubsystemFoundException(short type,
	CentralDirectoryInterface *_this, string extraInfo)
{
	ServiceException e(0, string("No Subsystem found: ")+StrUtils::decToString(type)+extraInfo);

	SerializedException *se=e.serialize();
	RPCPacket *res=new RPCPacket(_this->address, RPCPacket::exceptionPacketId,
		se->bytes(), se->size(), ServiceFinder::centralDirectoryTypeId,
		0, true);
	delete se;
	
	if (res==NULL)
		throw (ServiceException(1, "Not enough memory to allocate RCPPacket for NODBGW error response"));

	return res;			
}


RPCPacket* CentralDirectoryInterface::findNearestByIPByTypeId
	(CentralDirectoryInterface *_this, Address *a,
	findBestSubsystemByTypeIdParams *p)
{
	long long maskIP=0;
	for (byte i=0; i<p->nearestByIPMask; i++)
	{
		maskIP<<=1;
		maskIP|=1;
	}
	for (byte i=p->nearestByIPMask; i<32; i++)
	{
		maskIP<<=1;
	}
	
	string maskIPStr=StrUtils::decToString(maskIP);
	IP pIP=IP(p->nearestByIPIP);
	string tipoIdStr=StrUtils::decToString(p->type);
	
	// Variables per emmagatzemar el resultat
	bool nearestExists=false;
	string subsysId;
	dword numItemsGrupo=0;
	bool isCached=false;
	float load=0.0f;
	IP ip;
	word port=0;
	
	// Mirem la cache d'assignacions
	// Obtenim IP:port i carrega per la IP/mascara que estem buscant.
	string nearestByIPQuery=string("SELECT * FROM getNearestSubsys('") + 
		tipoIdStr + string("', '") + pIP.toString() + string("/") +
		StrUtils::decToString(p->nearestByIPMask) + string("');");

	XML *xml=NULL;
	try
	{
		xml=_this->db->query(nearestByIPQuery);
	}
	catch (Exception &e)
	{
		cout << "nearestByIPQuery exception: " << e.getClass() << ": " <<
			e.getMsg() << ", query: " << nearestByIPQuery;
	//	e.serialize()->materializeAndThrow(true);
		throw;
	}
			
	if (xml==NULL ||
		atoi(xml->getNode("/result")->getParam("numRows").c_str())==0)
	{
		if (xml!=NULL)
		{
			delete xml;
			xml=NULL;
		}

		string extraInfo=string(" ") + pIP.toString() + 
			(xml==NULL?string(" (NULL)"):
				xml->getNode("/result")->getParam("numRows"));
		return NoSubsystemFoundException(p->type, _this, extraInfo);
	}	
	
	xmlNode *resNode=xml->getNode("/result/[0]");
	if (resNode==NULL)
	{
		delete xml;
		return NoSubsystemFoundException(p->type, _this, string(" no rows returned ") + pIP.toString());
	}

	xmlNode *numItemsGrupoNode = xml->getNode(string("/result/[0]/numitemsgrupo"));

	if (numItemsGrupoNode==NULL)
	{
		string extraInfo=xml->toString();
		if (xml!=NULL)
		{
			delete xml;
			xml=NULL;
		}
		
		return NoSubsystemFoundException(p->type, _this, extraInfo);
	}
		
	numItemsGrupo=atoi(numItemsGrupoNode->getCdata().c_str());

	if (numItemsGrupo>1)
	{
		// Si n'hi ha mes d'un que es candidat, apliquem
		// l'algorisme de desempat i assignem
		switch (p->nearestByIpUntieAlgorithm)
		{
			case CENTRAL_DIRECTORY:
				// Ja la tenim pq hem ordenat per carrega el query inicial
				subsysId=xml->getNode(string("/result/[0]/id"))->getCdata();
				load=atof(xml->getNode(string("/result/[0]/carga"))->getCdata().c_str());
				ip=IP(xml->getNode(string("/result/[0]/ip"))->getCdata());
				port=atoi(xml->getNode(string("/result/[0]/puerto"))->getCdata().c_str());
				isCached=(xml->getNode(string("/result/[0]/iscached"))->getCdata() == string("t"));
				break;

			case RANDOM:
				{
					string idx=StrUtils::decToString(rand()%numItemsGrupo);
					subsysId=xml->getNode(string("/result/[")+idx+string("]/id"))->getCdata();
					ip=IP(xml->getNode(string("/result/[")+idx+string("]/ip"))->getCdata());
					port=atoi(xml->getNode(string("/result/[")+idx+string("]/puerto"))->getCdata().c_str());
					load=atof(xml->getNode(string("/result/[")+idx+string("]/carga"))->getCdata().c_str());
					isCached=(xml->getNode(string("/result/[")+idx+string("]/iscached"))->getCdata() == string("t"));
				}
				break;

			case ROUND_ROBIN:
				{
					if (_this->rrIndex>=numItemsGrupo)
						_this->rrIndex=0;
						
					string idx=StrUtils::decToString(_this->rrIndex);
					subsysId=xml->getNode(string("/result/[")+idx+string("]/id"))->getCdata();
					ip=IP(xml->getNode(string("/result/[")+idx+string("]/ip"))->getCdata());
					port=atoi(xml->getNode(string("/result/[")+idx+string("]/puerto"))->getCdata().c_str());
					load=atof(xml->getNode(string("/result/[")+idx+string("]/carga"))->getCdata().c_str());
					isCached=(xml->getNode(string("/result/[")+idx+string("]/iscached"))->getCdata() == string("t"));
					_this->rrIndex++;
				}
				break;
				
			case NEAREST_BY_IP_GROUP:
				// El desempat ha de ser amb mascara 32 sempre
				p->nearestByIPIP=p->nearestByIPUntieIP;
				p->nearestByIPMask=32;
				if (xml!=NULL)
				{
					delete xml;
					xml=NULL;
				}
				// Ens fem la crida a nosaltres mateixos aprofitant els
				// params. Com que hem posat la mascara a 32, no pot 
				// haver-hi empats
				return CentralDirectoryInterface::findNearestByIPByTypeId(_this, a, p);
				break;
		}
	}
	else
	{
		subsysId=xml->getNode(string("/result/[0]/id"))->getCdata();
		load=atof(xml->getNode(string("/result/[0]/carga"))->getCdata().c_str());
		ip=IP(xml->getNode(string("/result/[0]/ip"))->getCdata());
		port=atoi(xml->getNode(string("/result/[0]/puerto"))->getCdata().c_str());
		isCached=(xml->getNode(string("/result/[0]/iscached"))->getCdata() == string("t"));
	}

	delete xml;
	xml=NULL;
		
	Endian::to(Endian::xarxa, &load, sizeof(load));
	Address addr(ip, port);
	
	// Updatem la cache de mes proxims
	// No cal comprovar si existeix o no, fem DELETE + INSERT incondicionalment
	
	if (!isCached)
	{
		string updateStates=string("SELECT * FROM setNearestSubsys('") + tipoIdStr
			+ string("', '") + pIP.toString() + string("/")
			+ StrUtils::decToString(p->nearestByIPMask) + string("', '")
			+ subsysId + string("'); ");
		// Tambe updatem deltacarga abans de retornar
		// 20100223: Ja no cal :P (i hem canviat una coma a dalt per punt i coma :))
	//	updateStates+=string("incDeltaCarga('") + ip.toString() +
	//		string("', '") + StrUtils::decToString(port) + string("', '")
	//		+ tipoIdStr + string("', '0.01'); ");
		try
		{
			xml=_this->db->query(updateStates);
		}
		catch (Exception &e)
		{
			cout << "UpdateState exception: " << e.getClass() << ": " <<
				e.getMsg() << ", query: " << updateStates;
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
		
		if (xml!=NULL)		
			delete xml;
		xml=NULL;
	}
		
	
	RPCPacket *res=new RPCPacket(addr,
		RPCPacket::responsePacketId, (byte*)&load, sizeof(load),
		ServiceFinder::centralDirectoryTypeId, p->type, true);

	return res;
}

RPCPacket* CentralDirectoryInterface::findNearestByIPByTypeName
	(CentralDirectoryInterface *_this, Address *a,
	findBestSubsystemByTypeNameParams *p)
{
	long long maskIP=0;
	for (byte i=0; i<p->nearestByIPMask; i++)
	{
		maskIP<<=1;
		maskIP|=1;
	}
	for (byte i=p->nearestByIPMask; i<32; i++)
	{
		maskIP<<=1;
	}
	
	string maskIPStr=StrUtils::decToString(maskIP);

	IP pIP=IP(p->nearestByIPIP);
	string nearestByIPQuery=
		string("SELECT s.id AS id, s.tipo AS tipo, host(s.ip) AS ip, "
			"s.puerto AS puerto, abs((('")
			+ pIP.toString() + string("'::cidr - '0.0.0.0'::cidr)&")
			+ maskIPStr + string(")-((s.ip - '0.0.0.0'::cidr)&")
			+ maskIPStr + string(")) AS grupoip, "
				"(SELECT COUNT(*) "
				"FROM subsistema s2, tiposubsistema ts2, "
				"infobalanceo ib2 WHERE ts2.nombre='") + string(p->typeName) + 
				string("' AND s2.tipo=ts2.id "
				"AND ib2.subsistema=s2.id AND "
				"now()-ib2.ultimamodificacion<(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo) AND abs((('") +
				pIP.toString() + string("'::cidr - '0.0.0.0'::cidr)&")
				+ maskIPStr + string(")-((s2.ip - '0.0.0.0'::cidr)&")
				+ maskIPStr + string("))=abs((('") + pIP.toString() +
				string("'::cidr - '0.0.0.0'::cidr)&") + maskIPStr +
				string(")-((s.ip - '0.0.0.0'::cidr)&")
				+ maskIPStr + string("))) AS numitemsgrupo, "
				"(SELECT SUM(ib2.carga) "
				"FROM infobalanceo ib2, subsistema s2 "
				"WHERE s2.id=ib2.subsistema AND "
				"s2.ip=s.ip) AS carga "
			"FROM subsistema s, tiposubsistema ts, infobalanceo ib "
			"WHERE ts.nombre='") + string(p->typeName) + 
			string("' AND s.tipo=ts.id AND ib.subsistema=s.id AND "
			"now()-ib.ultimamodificacion<(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo) "
			"ORDER BY grupoip, carga");
			
	XML *xml=NULL;
	try
	{
		xml=_this->db->query(nearestByIPQuery);
	}
	catch (Exception &e)
	{
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	
	xmlNode *resNode=NULL;
	string basePath;

	if (xml==NULL ||
		atoi(xml->getNode("/result")->getParam("numRows").c_str())==0)
	{
		ServiceException e(0, string("No Subsystem found: ")+string(p->typeName));

		if (xml!=NULL)
		{
			delete xml;
			xml=NULL;
		}
	
		SerializedException *se=e.serialize();
		RPCPacket *res=new RPCPacket(_this->address, RPCPacket::exceptionPacketId,
			se->bytes(), se->size(), ServiceFinder::centralDirectoryTypeId,
			0, true);
		delete se;
		
		if (res==NULL)
			throw (ServiceException(1, "Not enough memory to allocate RCPPacket for NODBGW error response"));
	
		return res;			
	}	

	resNode=xml->getNode("/result/[0]");
	basePath=string("/result/[0]");
	
	if (resNode==NULL)
	{
		delete xml;
		
		ServiceException e(0, string("No Subsystem found: ")+string(p->typeName));
	
		SerializedException *se=e.serialize();
		RPCPacket *res=new RPCPacket(_this->address, RPCPacket::exceptionPacketId,
			se->bytes(), se->size(), ServiceFinder::centralDirectoryTypeId,
			0, true);
		delete se;
		
		if (res==NULL)
			throw (ServiceException(1, "Not enough memory to allocate RCPPacket for NODBGW error response"));
	
		return res;			
	}	

	string subsysId=xml->getNode(basePath+string("/id"))->getCdata();

	dword grupo=atoi(xml->getNode(basePath+string("/grupoip"))->getCdata().c_str());
	dword numItemsGrupo=atoi(xml->getNode(basePath+string("/numitemsgrupo"))->getCdata().c_str());

	float load=atof(xml->getNode(basePath+string("/carga"))->getCdata().c_str());
	string tipoIdStr=xml->getNode(basePath+string("/tipo"))->getCdata();
	IP ip(xml->getNode(basePath+string("/ip"))->getCdata());
	word port=atoi(xml->getNode(basePath+string("/puerto"))->getCdata().c_str());
	bool nearestExists=false;
	
	if (numItemsGrupo>1)
	{
		// Hi ha mes d'un que pot ser el millor. Mirem la cache d'assignacions
		// Obtenim IP:port i carrega per la IP/mascara que estem buscant.

		string cacheQuery=
			string("SELECT host(s.ip) AS ip, s.puerto, "
				"(SELECT SUM(ib2.carga) "
				"FROM infobalanceo ib2, subsistema s2 "
				"WHERE s2.id=ib2.subsistema AND "
				"s2.ip=s.ip) AS carga "
			"FROM subsistema s, infobalanceo ib, nearestsubsystembyip ns "
			"WHERE s.id=ns.nearestsubsys AND s.id=ib.subsistema AND "
			"now()-ib.ultimamodificacion<(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo) AND "
			"ns.ip='") + pIP.toString() + string("/") +
			StrUtils::decToString(p->nearestByIPMask) + string("' AND "
			"ns.tiposubsistema='") + tipoIdStr + string("'");

		XML *xmlCache=NULL;
		try
		{
			xmlCache=_this->db->query(cacheQuery);
		}
		catch (Exception &e)
		{
			//e.serialize()->materializeAndThrow(true);
			throw;
		}

		if (xmlCache==NULL ||
			atoi(xmlCache->getNode("/result")->getParam("numRows").c_str())==0)
		{
			// Si no hi es a la cache o no es valid en aquest moment, mirem
			// si hi ha alguna altra assignacio per la mateixa IP, amb
			// qualsevol tipus de modul per mantenir el tractament d'una
			// mateixa camera a prop
			cacheQuery=
				string("SELECT host(s.ip) AS ip, s.puerto "
				"FROM subsistema s, infobalanceo ib, nearestsubsystembyip ns "
				"WHERE s.id=ns.nearestsubsys AND s.id=ib.subsistema AND "
				"now()-ib.ultimamodificacion<(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo) AND "
				"ns.ip='") + pIP.toString() + string("/") +
				StrUtils::decToString(p->nearestByIPMask) + string("' LIMIT 1");
			
			if (xmlCache!=NULL)
			{
				delete xmlCache;
				xmlCache=NULL;
			}

			try
			{
				xmlCache=_this->db->query(cacheQuery);
			}
			catch (Exception &e)
			{
				//e.serialize()->materializeAndThrow(true);
				throw;
			}
				
			
			if (xmlCache==NULL ||
				atoi(xmlCache->getNode("/result")->getParam("numRows").c_str())==0)
			{
				// Si no  hi ha cap aplicacio per la mateixa IP, apliquem
				// l'algorisme de desempat i assignem
				switch (p->nearestByIpUntieAlgorithm)
				{
					case CENTRAL_DIRECTORY:
						// Ja la tenim pq hem ordenat per carrega el query inicial
						break;
	
					case RANDOM:
						{
							string idx=StrUtils::decToString(rand()%numItemsGrupo);
							subsysId=xml->getNode(string("/result/[")+idx+string("]/id"))->getCdata();
							ip=IP(xml->getNode(string("/result/[")+idx+string("]/ip"))->getCdata());
							port=atoi(xml->getNode(string("/result/[")+idx+string("]/puerto"))->getCdata().c_str());
							load=atof(xml->getNode(string("/result/[")+idx+string("]/carga"))->getCdata().c_str());
						}
						break;
	
					case ROUND_ROBIN:
						{
							if (_this->rrIndex>=numItemsGrupo)
								_this->rrIndex=0;
								
							string idx=StrUtils::decToString(_this->rrIndex);
							subsysId=xml->getNode(string("/result/[")+idx+string("]/id"))->getCdata();
							ip=IP(xml->getNode(string("/result/[")+idx+string("]/ip"))->getCdata());
							port=atoi(xml->getNode(string("/result/[")+idx+string("]/puerto"))->getCdata().c_str());
							load=atof(xml->getNode(string("/result/[")+idx+string("]/carga"))->getCdata().c_str());
							_this->rrIndex++;
						}
						break;
						
					case NEAREST_BY_IP_GROUP:
						// El desempat ha de ser amb mascara 32 sempre
						p->nearestByIPIP=p->nearestByIPUntieIP;
						p->nearestByIPMask=32;
						if (xmlCache!=NULL)
						{
							delete xmlCache;
							xmlCache=NULL;
						}
						if (xml!=NULL)
						{
							delete xml;
							xml=NULL;
						}
						// Ens fem la crida a nosaltres mateixos aprofitant els
						// params. Com que hem posat la mascara a 32, no pot 
						// haver-hi empats
						return CentralDirectoryInterface::findBestSubsystemByTypeId(_this, a, p);
						break;
				}
			}
			else
			{
				ip=IP(xmlCache->getNode(string("/result/[0]/ip"))->getCdata());
				// No hem de llegir el port, ja el tenim i aquest que llegiriem
				// Seria d'un altre modul!!
//				port=atoi(xmlCache->getNode(string("/result/[0]/puerto"))->getCdata().c_str());
//				load=atof(xmlCache->getNode(string("/result/[0]/carga"))->getCdata().c_str());
//				nearestExists=true;
			}
		}
		else
		{
			ip=IP(xmlCache->getNode(string("/result/[0]/ip"))->getCdata());
			port=atoi(xmlCache->getNode(string("/result/[0]/puerto"))->getCdata().c_str());
			load=atof(xmlCache->getNode(string("/result/[0]/carga"))->getCdata().c_str());
			nearestExists=true;
		}

		if (xmlCache!=NULL)
		{
			delete xmlCache;
			xmlCache=NULL;
		}
	}
	// Si no tenim mes d'un el que tenim ja es el millor :P
		
	if (xml!=NULL)	
	{
		delete xml;
		xml=NULL;
	}
	
	Endian::to(Endian::xarxa, &load, sizeof(load));
		
	Address addr(ip, port);
	
	// Updatem la cache de mes proxims
	if (!nearestExists)
	{
		string nearestExistsQuery=
			string("SELECT COUNT(*) AS cnt "
			"FROM nearestsubsystembyip "
			"WHERE ip='") + pIP.toString() + string("/") +
			StrUtils::decToString(p->nearestByIPMask) + string("' AND "
			"tiposubsistema='") + tipoIdStr + string("'");

		XML *xmlExists=NULL;
		try
		{
			xmlExists=_this->db->query(nearestExistsQuery);
			xmlNode *cntNode=xmlExists->getNode("/result/[0]/cnt");
			if (cntNode!=NULL && cntNode->getCdata()!=string("0"))
				nearestExists=true;

			delete xmlExists;
			xmlExists=NULL;
		}
		catch (Exception &e)
		{
			if (xmlExists!=NULL)
				delete xmlExists;
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
	}
	
	string updateStates;
	if (!nearestExists)
	{
		updateStates=string("INSERT INTO nearestsubsystembyip (ip, "
			"tiposubsistema, nearestsubsys, lastupdate) values ('")
			+ pIP.toString() + string("/")
			+ StrUtils::decToString(p->nearestByIPMask) + string("', '")
			+ tipoIdStr + string("', '")
			+ subsysId + string("', now()); ");
	}
	else
	{
		updateStates=string("UPDATE nearestsubsystembyip SET "
			"lastupdate=now() WHERE ip='") + pIP.toString() + string("/") +
			StrUtils::decToString(p->nearestByIPMask) + string("' AND "
			"tiposubsistema='") + tipoIdStr + string("'; ");
	}
	
	// Updatem deltacarga abans de retornar
//	updateStates+=string("UPDATE infobalanceo SET ")+
//		string("deltacarga=deltacarga+0.01 ")+
//		string("WHERE subsistema=")+
//		string("(SELECT id FROM subsistema WHERE ip='")+
//		ip.toString()+string("' AND puerto='")+
//		StrUtils::decToString(port)+string("' AND tipo='") + 
//		tipoIdStr + string("');");
	updateStates+=string("SELECT incDeltaCarga('") + ip.toString() +
		string("', '") + StrUtils::decToString(port) + string("', '")
		+ tipoIdStr + string("', '0.01'); ");

	try
	{
		xml=_this->db->query(updateStates);
	}
	catch (Exception &e)
	{
		throw e;
	}
	
	if (xml!=NULL)
	{
		delete xml;
		xml=NULL;
	}
	
	RPCPacket *res=new RPCPacket(addr,
		RPCPacket::responsePacketId, (byte*)&load, sizeof(load),
		ServiceFinder::centralDirectoryTypeId, 0, true);

	return res;
}

CentralDirectoryInterface::CentralDirectoryInterface(Address address, short type, Canis *cn) : ModuleInterface(address, type, cn), rrIndex(0)
{
	STACKTRACE_INSTRUMENT();
	
	srand((dword)time(NULL));
	
	this->numServices=CentralDirectoryInterface::serviceCount;
	this->services=new serviceDef[CentralDirectoryInterface::serviceCount];
	
	memset(this->services, 0,
		sizeof(serviceDef)*CentralDirectoryInterface::serviceCount);
	this->services[CentralDirectoryInterface::findBestSubsystemByTypeIdServiceId].call = (DefaultModuleCall)CentralDirectoryInterface::findBestSubsystemByTypeId;

	this->services[CentralDirectoryInterface::findBestSubsystemByTypeNameServiceId].call = (DefaultModuleCall)CentralDirectoryInterface::findBestSubsystemByTypeName;

	this->services[CentralDirectoryInterface::findAllSubsystemsByTypeIdServiceId].call = (DefaultModuleCall)CentralDirectoryInterface::findAllSubsystemsByTypeId;

	this->services[CentralDirectoryInterface::findAllSubsystemsByTypeNameServiceId].call = (DefaultModuleCall)CentralDirectoryInterface::findAllSubsystemsByTypeName;

	this->db=new DBGateway(this->address, this->type, this->cn);
}

CentralDirectoryInterface::~CentralDirectoryInterface()
{
	STACKTRACE_INSTRUMENT();
	
	if (this->db!=NULL)
		delete this->db;
}

RPCPacket* CentralDirectoryInterface::findBestSubsystemByTypeId(CentralDirectoryInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	
	findBestSubsystemByTypeIdParams p(params);
	
	string orderBy("");
	switch (p.algorithm)
	{
		case CENTRAL_DIRECTORY:
			orderBy=string(" ORDER BY carga");
			break;
		
		case ROUND_ROBIN:
			orderBy=string(" ORDER BY s.id");
			break;
	
		case RANDOM:
			orderBy=string(" ORDER BY RANDOM()");
			break;
			
		case NEAREST_BY_IP_GROUP:
			return findNearestByIPByTypeId(_this, a, &p);
	}

	string q=string("SELECT s.tipo AS tipo, host(s.ip) AS ip, s.puerto, ")+
		string("(SELECT SUM(ib2.carga) ")+
		string("FROM infobalanceo ib2, subsistema s2 ")+
		string("WHERE s2.id=ib2.subsistema AND "
		"s2.ip=s.ip) AS carga ")+
		string("FROM tiposubsistema ts, subsistema s, infobalanceo ib ")+
		string("WHERE ts.id='")+StrUtils::decToString(p.type)+
		string ("' AND s.tipo=ts.id AND ib.subsistema=s.id AND 'now()'-ib.ultimamodificacion<(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo)")+
		orderBy;

	XML *xml=NULL;
	try
	{
		xml=_this->db->query(q);
	}
	catch (Exception &e)
	{
		throw e;
	}
	
	xmlNode *resNode=NULL;
	string basePath;

	if (xml!=NULL)
	{
		switch (p.algorithm)
		{
			case CENTRAL_DIRECTORY:
			case RANDOM:
				resNode=xml->getNode("/result/[0]");
				basePath=string("/result/[0]");
				break;
			case ROUND_ROBIN:
				resNode=xml->getNode(string("/result/[")+
									 StrUtils::decToString(_this->rrIndex)+string("]"));
				
				if (resNode==NULL)
				{
					// Si ens hem passat de llarg, donem la volta
					resNode=xml->getNode(string("/result/[0]"));
					_this->rrIndex=0;
				}
		
				basePath=string("/result/[")+StrUtils::decToString(_this->rrIndex)+string("]");
				_this->rrIndex++;
				break;
		}
	}
	
	if (resNode!=NULL)
	{
		float load=atof(xml->getNode(basePath+string("/carga"))->getCdata().c_str());
		Endian::to(Endian::xarxa, &load, sizeof(load));
		
		string tipoIdStr=xml->getNode(basePath+string("/tipo"))->getCdata();
		
		IP ip(xml->getNode(basePath+string("/ip"))->getCdata());
		short port=atoi(xml->getNode(basePath+string("/puerto"))->getCdata().c_str());
		Address addr(ip, port);
		
		// Updatem deltacarga abans de retornar
//		string deltacarga=string("UPDATE infobalanceo SET ")+
//			string("deltacarga=deltacarga+0.01 ")+
//			string("WHERE subsistema=")+
//			string("(SELECT id FROM subsistema WHERE ip='")+
//			ip.toString()+string("' AND puerto='")+
//			StrUtils::decToString(port)+string("' AND tipo='") + 
//			tipoIdStr + string("');");
		string deltacarga=string("SELECT incDeltaCarga('") + ip.toString() +
			string("', '") + StrUtils::decToString(port) + string("', '")
			+ tipoIdStr + string("', '0.01'); ");

		delete xml;
		xml=NULL;
		
		try
		{
			xml=_this->db->query(deltacarga);
		}
		catch (Exception &e)
		{
			throw e;
		}
		
		if (xml!=NULL)
			delete xml;
		
		RPCPacket *res=new RPCPacket(addr,
			RPCPacket::responsePacketId, (byte*)&load, sizeof(load),
			ServiceFinder::centralDirectoryTypeId, 0, true);

		return res;
	}
	
	delete xml;

	ServiceException e(0, string("No Subsystem found: ")+StrUtils::decToString(p.type));

	SerializedException *se=e.serialize();
	RPCPacket *res=new RPCPacket(_this->address, RPCPacket::exceptionPacketId,
		se->bytes(), se->size(), ServiceFinder::centralDirectoryTypeId,
		0, true);
	delete se;
	
	if (res==NULL)
		throw (ServiceException(1, "Not enough memory to allocate RCPPacket for NODBGW error response"));

	return res;
}

RPCPacket* CentralDirectoryInterface::findBestSubsystemByTypeName(CentralDirectoryInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	
	findBestSubsystemByTypeNameParams *p=(findBestSubsystemByTypeNameParams*)params;
	p->toLocalEndian();

	if (_this->db==NULL)
		_this->db=new DBGateway(_this->address, _this->type, _this->cn);
	
	string orderBy("");
	switch (p->algorithm)
	{
		case CENTRAL_DIRECTORY:
			orderBy=string(" ORDER BY carga");
			break;
		
		case ROUND_ROBIN:
			orderBy=string(" ORDER BY s.id");
			break;
	
		case RANDOM:
			orderBy=string(" ORDER BY RANDOM()");
			break;
	}

	string q=string("SELECT s.tipo AS tipo, host(s.ip) AS ip, s.puerto, ")+
		string("(SELECT SUM(ib2.carga) ")+
		string("FROM infobalanceo ib2, subsistema s2 ")+
		string("WHERE s2.id=ib2.subsistema AND "
		"s2.ip=s.ip) AS carga ")+
		string("FROM tiposubsistema ts, subsistema s, infobalanceo ib ")+
		string("WHERE ts.nombre='")+string(p->typeName)+
		string ("' AND s.tipo=ts.id AND ib.subsistema=s.id AND 'now()'-ib.ultimamodificacion<(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo)")+
		orderBy;

	XML *xml=NULL;
	try
	{
		xml=_this->db->query(q);
	}
	catch (Exception &e)
	{
		throw e;
	}
	
	xmlNode *resNode=NULL;
	string basePath;

	if (xml!=NULL)
	{
		switch (p->algorithm)
		{
			case CENTRAL_DIRECTORY:
			case RANDOM:
				resNode=xml->getNode("/result/[0]");
				basePath=string("/result/[0]");
				break;
			case ROUND_ROBIN:
				resNode=xml->getNode(string("/result/[")+
									 StrUtils::decToString(_this->rrIndex)+string("]"));
				
				if (resNode==NULL)
				{
					// Si ens hem passat de llarg, donem la volta
					resNode=xml->getNode(string("/result/[0]"));
					_this->rrIndex=0;
				}
		
				basePath=string("/result/[")+StrUtils::decToString(_this->rrIndex)+string("]");
				_this->rrIndex++;
				break;
		}
	}
	
	if (resNode!=NULL)
	{
		float load=atof(xml->getNode(basePath+string("/carga"))->getCdata().c_str());
		Endian::to(Endian::xarxa, &load, sizeof(load));

		string tipoIdStr=xml->getNode(basePath+string("/tipo"))->getCdata();
		
		IP ip(xml->getNode(basePath+string("/ip"))->getCdata());
		short port=atoi(xml->getNode(basePath+string("/puerto"))->getCdata().c_str());
		Address addr(ip, port);
		
		// Updatem deltacarga abans de retornar
//		string deltacarga=string("UPDATE infobalanceo SET ")+
//			string("deltacarga=deltacarga+0.01 ")+
//			string("WHERE subsistema=")+
//			string("(SELECT id FROM subsistema WHERE ip='")+
//			ip.toString()+string("' AND puerto='")+
//			StrUtils::decToString(port)+string("' AND tipo='") + 
//			tipoIdStr + string("');");
		string deltacarga=string("SELECT incDeltaCarga('") + ip.toString() +
			string("', '") + StrUtils::decToString(port) + string("', '")
			+ tipoIdStr + string("', '0.01'); ");

		delete xml;
		xml=NULL;
		
		try
		{
			xml=_this->db->query(deltacarga);
		}
		catch (Exception &e)
		{
			throw e;
		}
		
		if (xml!=NULL)
			delete xml;
		
		RPCPacket *res=new RPCPacket(addr,
			RPCPacket::responsePacketId, (byte*)&load, sizeof(load),
			ServiceFinder::centralDirectoryTypeId, 0, true);

		return res;
	}
	
	delete xml;

	ServiceException e(0, string("No Subsystem found: ")+string(p->typeName));

	SerializedException *se=e.serialize();
	RPCPacket *res=new RPCPacket(_this->address, RPCPacket::exceptionPacketId,
		se->bytes(), se->size(), ServiceFinder::centralDirectoryTypeId,
		0, true);
	delete se;
	
	if (res==NULL)
		throw (ServiceException(1, "Not enough memory to allocate RCPPacket for NODBGW error response"));

	return res;
}

RPCPacket* CentralDirectoryInterface::findAllSubsystemsByTypeId(CentralDirectoryInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	
	findAllSubsystemsByTypeIdParams p;
	p.toLocal(params);

	if (_this->db==NULL)
		_this->db=new DBGateway(_this->address, _this->type, _this->cn);
	
	string orderBy(" ORDER BY carga");

	string q=string("SELECT s.tipo AS tipo, host(s.ip) AS ip, s.puerto, ")+
		string("(SELECT SUM(ib2.carga) ")+
		string("FROM infobalanceo ib2, subsistema s2 ")+
		string("WHERE s2.id=ib2.subsistema AND "
		"s2.ip=s.ip) AS carga ")+
		string("FROM tiposubsistema ts, subsistema s, infobalanceo ib ")+
		string("WHERE ts.id='")+StrUtils::decToString(p.type)+
		string ("' AND s.tipo=ts.id AND ib.subsistema=s.id AND 'now()'-ib.ultimamodificacion<(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo)")+
		orderBy;

	XML *xml=NULL;
	try
	{
		xml=_this->db->query(q);
	}
	catch (Exception &e)
	{
		throw e;
	}

	xmlNode *resNode=NULL;
	string basePath;

	if (xml!=NULL)
	{
		p.nSubsys=atoi(xml->getNode("/result")->getParam("numRows").c_str());
		p.subsystems=new findAllSubsystemsByTypeIdParams::subsystemDesc[p.nSubsys];
	
		for (dword s=0; s<p.nSubsys; s++)
		{
			basePath=string("/result/[")+StrUtils::decToString(s)+string("]");
			resNode=xml->getNode(basePath);
			
			if (resNode!=NULL)
			{
				p.subsystems[s].load=atof(xml->getNode(basePath+string("/carga"))->getCdata().c_str());
	
				IP ip(xml->getNode(basePath+string("/ip"))->getCdata());
				word port=atoi(xml->getNode(basePath+string("/puerto"))->getCdata().c_str());
				p.subsystems[s].addr=new Address(ip, port);
			}
		}
	
		delete xml;
		
		byte *ssData=(byte*)p.toNetwork();
				
		RPCPacket *res=new RPCPacket(_this->address,
			RPCPacket::responsePacketId, ssData, p.sizeOf(),
			ServiceFinder::centralDirectoryTypeId, 0, true);
	
		delete [] ssData;
		
		return res;
	}

	delete xml;

	ServiceException e(0, string("FindAllSubsystems: Could not find any subsystem of type: ")+StrUtils::decToString(p.type));

	SerializedException *se=e.serialize();
	RPCPacket *res=new RPCPacket(_this->address, RPCPacket::exceptionPacketId,
		se->bytes(), se->size(), ServiceFinder::centralDirectoryTypeId,
		0, true);
	delete se;
	
	if (res==NULL)
		throw (ServiceException(1, "Not enough memory to allocate RCPPacket for NODBGW error response"));

	return res;
}

RPCPacket* CentralDirectoryInterface::findAllSubsystemsByTypeName(CentralDirectoryInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	
	findAllSubsystemsByTypeNameParams p;
	p.toLocal(params);

	if (_this->db==NULL)
		_this->db=new DBGateway(_this->address, _this->type, _this->cn);
	
	string orderBy(" ORDER BY carga");

	string q=string("SELECT s.tipo AS tipo, host(s.ip) AS ip, s.puerto, ")+
		string("(SELECT SUM(ib2.carga) ")+
		string("FROM infobalanceo ib2, subsistema s2 ")+
		string("WHERE s2.id=ib2.subsistema AND "
		"s2.ip=s.ip) AS carga ")+
		string("FROM tiposubsistema ts, subsistema s, infobalanceo ib ")+
		string("WHERE ts.nombre='")+p.type+
		string ("' AND s.tipo=ts.id AND ib.subsistema=s.id AND 'now()'-ib.ultimamodificacion<(SELECT MIN(now()-ultimamodificacion)+'15 seconds' FROM infobalanceo)")+
		orderBy;

	XML *xml=NULL;
	try
	{
		xml=_this->db->query(q);
	}
	catch (Exception &e)
	{
		throw e;
	}

	xmlNode *resNode=NULL;
	string basePath;

	if (xml!=NULL)
	{
		p.nSubsys=atoi(xml->getNode("/result")->getParam("numRows").c_str());
		p.subsystems=new findAllSubsystemsByTypeNameParams::subsystemDesc[p.nSubsys];
	
		for (dword s=0; s<p.nSubsys; s++)
		{
			basePath=string("/result/[")+StrUtils::decToString(s)+string("]");
			resNode=xml->getNode(basePath);
			
			if (resNode!=NULL)
			{
				p.subsystems[s].load=atof(xml->getNode(basePath+string("/carga"))->getCdata().c_str());
	
				IP ip(xml->getNode(basePath+string("/ip"))->getCdata());
				word port=atoi(xml->getNode(basePath+string("/puerto"))->getCdata().c_str());
				p.subsystems[s].addr=new Address(ip, port);
			}
		}
	
		delete xml;
		
		byte *ssData=(byte*)p.toNetwork();
				
		RPCPacket *res=new RPCPacket(_this->address,
			RPCPacket::responsePacketId, ssData, p.sizeOf(),
			ServiceFinder::centralDirectoryTypeId, 0, true);
	
		delete [] ssData;
		
		return res;
	}

	delete xml;

	ServiceException e(0, string("FindAllSubsystems: Could not find any subsystem of type: ")+p.type);

	SerializedException *se=e.serialize();
	RPCPacket *res=new RPCPacket(_this->address, RPCPacket::exceptionPacketId,
		se->bytes(), se->size(), ServiceFinder::centralDirectoryTypeId,
		0, true);
	delete se;
	
	if (res==NULL)
		throw (ServiceException(1, "Not enough memory to allocate RCPPacket for NODBGW error response"));

	return res;
}
