#include <CentralDirectory/CentralDirectorySubsystemCheckThread.h>
#include <CentralDirectory/CentralDirectory.h>
#include <Module/Module.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Canis.h>
#include <Utils/DBGateway.h>
#include <Utils/StrUtils.h>
#include <Utils/Log.h>
#include <Utils/Timer.h>
#include <stdlib.h>

extern Log *centralDirLog;

CentralDirectorySubsystemCheckThread::CentralDirectorySubsystemCheckThread(ModuleInterface *intf, CentralDirectory *central) : modInt(intf), central(central), run(false), runningId(-1)
{
	STACKTRACE_INSTRUMENT();
	
	srand(time(NULL));
}

void CentralDirectorySubsystemCheckThread::startRunning()
{
	STACKTRACE_INSTRUMENT();
	
	// Ens assegurem que no queda cap executant-se
	this->stopRunning();
	
	// Iniciem un de nou
	this->run=true;
	this->runningId=this->start();
}

void CentralDirectorySubsystemCheckThread::stopRunning()
{
	STACKTRACE_INSTRUMENT();
	
	this->run=false;
	// Ens assegurem que el thread acaba
	if (this->runningId!=-1)
		this->join(this->runningId);
}

void *CentralDirectorySubsystemCheckThread::execute(int id, void *args)
{
	STACKTRACE_INSTRUMENT();
	
	list<Canis::subsys> *systems=NULL;

	while (this->run)
	{
		while (!Module::isActive())
		{
			sleep(1);
			// Si s'ha activat, ens esperem "per estar segur que som unics"
			// i no registrar coses abans de temps :P
			if (Module::isActive())
				sleep(15);
		}
			
		Timer t;
		t.start();

		if (systems!=NULL)
		{
			delete systems;
			systems=NULL;
		}
		
		
		centralDirLog->logLine(string("CenDirSubCheckThr: --> Starting "
			"iteration"));
		cout<<"CenDirSubCheckThr: --> Starting iteration"<<endl;
			
	
		systems=this->modInt->getCanis()->getSysCopy();

		for (list<Canis::subsys>::iterator it=systems->begin(); 
			 it!=systems->end(); it++)
		{
			IP ip((*it).ip);
			Address addr(ip, (*it).port);
			short type=(*it).type;
			
			cout<<"CenDirSubCheckThr: --> checking:"<<addr.toString()<<endl;
			if(!ModuleAccess::isAlive(addr, type))
			{
				cout<<"CenDirSubCheckThr: "<<addr.toString()<<" NOT responding"<<endl;
				this->central->dssLock.wlock();
				
				this->central->deadSubsystems[addr.toString()] = true;
				
				this->central->dssLock.unlock();
			}
			else
			{
				cout<<"CenDirSubCheckThr: "<<addr.toString()<<" ok"<<endl;
				this->central->dssLock.rlock();
				map<string, bool>::iterator ssIt = this->central->deadSubsystems.find(addr.toString());
				if(ssIt != this->central->deadSubsystems.end() && ssIt->second==true)
				{
					this->central->dssLock.unlock();
					this->central->dssLock.wlock();
			
					cout<<"CenDirSubCheckThr: "<<addr.toString()<<" ok, lower"<<endl;
					//ssIt->second = false;
					this->central->deadSubsystems[addr.toString()] = false;
				}
				this->central->dssLock.unlock();
			}
		}




		double startT=t.getStartSeconds();
		t.start();
		double nowT=t.getStartSeconds();
		double spentTime = nowT-startT; 
		centralDirLog->logLine(string("CenDirSubSysCheck: Iteration ran for ")
			+ StrUtils::floatToString(spentTime) + string(" seconds."));
		cout<<"CenDirSubSysCheck: Iteration ran for " << StrUtils::floatToString(spentTime)<<" seconds."<<endl; 
		// Sleepem una estona ;)
		for (int s=0; s+spentTime < 120 && this->run; s++)
			sleep(1);
	}

	return NULL;
}
