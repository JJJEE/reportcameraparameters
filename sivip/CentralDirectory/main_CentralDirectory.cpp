#include <version.h>
#include <Utils/Canis.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Types.h>
#include <Utils/Log.h>
#include <Utils/RPCPacket.h>
#include <CentralDirectory/CentralDirectory.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <Utils/DBGateway.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <version.h>
#include <signal.h>
#include <iostream>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

#include <signal.h>

using namespace std;

Log *centralDirLog;

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMP();
	abort();
}

#endif

int main()
{
	cout << siriusRelease << endl;
	cout << "Starting Central Directory" << endl;

#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif
#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
#endif

#ifdef WIN32
	string configFileName=string("\\\\.PSF\\Untitled\\aena\\devel\\src\\Projecte VC++\\Sirius\\debug\\CentralDirectoryConfig.xml"),
#else
	string configFileName=string("CentralDirectoryConfig.xml"),
#endif
		logFileName=string("CentralDirectory.log");

	DBGateway::maxRPC=64;

	centralDirLog=new Log(logFileName);
	
	if (centralDirLog==NULL)
	{
		cout << "FATAL: Log file " << logFileName << 
			" could not be created/opened" << endl;
		return -1;
	}

	FILE *f=fopen(configFileName.c_str(), "rb");
	
	if (f==NULL)
	{
		cout << "FATAL: Configuration file " << configFileName << 
			" could not be opened" << endl;
		return -1;
	}
	
	fseek(f, 0, SEEK_END);
	dword tam=ftell(f);
	fseek(f, 0, SEEK_SET);
	
	char *fcont=new char[tam];

	if (fcont==NULL)
	{
		cout << "FATAL: Not enough memory to read configuration file to memory." << endl;
		return -1;
	}
	
	fread(fcont, tam, 1, f);
	fclose(f);
	
	XML *centralDirConfig=xmlParser::parse(string(fcont, tam));
	
	delete [] fcont;

	// Configuracio del Log
	if (centralDirConfig->getNode("/CentralDirectoryConfig/Log/sizeLimit")!=NULL)
		centralDirLog->limitBySize((dword)atol(centralDirConfig->getNode("/CentralDirectoryConfig/Log/sizeLimit")->getCdata().c_str()));

	if (centralDirConfig->getNode("/CentralDirectoryConfig/Log/timeLimit")!=NULL)
		centralDirLog->limitByTime((dword)atol(centralDirConfig->getNode("/CentralDirectoryConfig/Log/timeLimit")->getCdata().c_str()));

	if (centralDirConfig->getNode("/CentralDirectoryConfig/Log/maxFiles")!=NULL)
		centralDirLog->setMaxRotationFiles((byte)atoi(centralDirConfig->getNode("/CentralDirectoryConfig/Log/maxFiles")->getCdata().c_str()));
	
	centralDirLog->logLine(string("--> ")+siriusRelease);
	centralDirLog->logLine("--> Central Directory starting");
	
	CentralDirectory *cdir=NULL;
	try
	{
		cdir=new CentralDirectory(configFileName);
		
		cout << "CentralDirectory started. Serving..." << endl;
		cdir->serve();
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << endl;
		if (cdir!=NULL)
			delete cdir;
		throw e;
	}
}

