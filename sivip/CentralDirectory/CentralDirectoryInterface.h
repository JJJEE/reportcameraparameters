#ifndef __SIRIUS__BASE__CENTRALDIRECTORYINTERFACE_H
#define __SIRIUS__BASE__CENTRALDIRECTORYINTERFACE_H

#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <ModuleInterface/ModuleInterface.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Utils/DBGateway.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <map>
#include <iostream>

using namespace std;

class CentralDirectoryInterface : public ModuleInterface
{
public:
	static const int serviceCount=4;

	static const int findBestSubsystemByTypeIdServiceId=0;
	static const int findBestSubsystemByTypeNameServiceId=1;
	static const int findAllSubsystemsByTypeIdServiceId=2;
	static const int findAllSubsystemsByTypeNameServiceId=3;

	// TODO 20090507: Implementar NEAREST_BY_IP
	// Algorismes de cerca del millor modul, plugin, etc.
	//	RANDOM: Totalment a l'atzar.
	//	ROUND_ROBIN: Assignacio ciclica d'una llista.
	//	CENTRAL_DIRECTORY: Per carrega
	//	NEAREST_BY_IP_GROUP: Cal subministrar una IP i una netmask (n bits). Es
	//		proporciona el modul mes proper, per agrupacio de xarxa segons
	//		la netmask. A igual agrupacio, s'escull per un segon criteri que
	//		es subministra, que cal que desempati. P.e. sempre desempaten tots
	//		els algorismes anteriors o NEAREST_BY_IP_GROUP/32:
	//			NEAREST_BY_IP_GROUP x.x.x.x/24 + CENTRAL_DIRECTORY
	//			NEAREST_BY_IP_GROUP x.x.x.x/24 + NEAREST_BY_IP_GROUP y.y.y.y/32
	//			etc...
	enum algorithm {RANDOM=(dword)0L, ROUND_ROBIN, CENTRAL_DIRECTORY, NEAREST_BY_IP_GROUP};

	class findBestSubsystemByTypeIdParams
	{
	public:
		CentralDirectoryInterface::algorithm algorithm,
			nearestByIpUntieAlgorithm;
		short type;
		// Usats si l'algorisme principal es NearestByIP
		dword nearestByIPIP;	// Sempre en BigEndian
		byte nearestByIPMask;
		// Usats si el desempat es per NearestByIP
		dword nearestByIPUntieIP;	// Sempre en BigEndian
		
		findBestSubsystemByTypeIdParams()
		{
		}
		
		findBestSubsystemByTypeIdParams(void *buf)
		{
			this->toLocal(buf);
		}
		
		void toLocal(void *buf)
		{
			byte *b=(byte*)buf;
			
			this->algorithm=*(CentralDirectoryInterface::algorithm*)b;
			Endian::from(Endian::xarxa, &this->algorithm,
				sizeof(CentralDirectoryInterface::algorithm));
			b+=sizeof(CentralDirectoryInterface::algorithm);

			this->nearestByIpUntieAlgorithm=*(CentralDirectoryInterface::algorithm*)b;
			Endian::from(Endian::xarxa, &this->nearestByIpUntieAlgorithm,
				sizeof(CentralDirectoryInterface::algorithm));
			b+=sizeof(CentralDirectoryInterface::algorithm);
			
			this->type=*(short*)b;
			Endian::from(Endian::xarxa, &this->type, sizeof(short));
			b+=sizeof(short);
			
			this->nearestByIPIP=*(dword*)b;
			b+=sizeof(dword);
			
			this->nearestByIPMask=*(byte*)b;
			b+=sizeof(byte);
			
			this->nearestByIPUntieIP=*(dword*)b;
			b+=sizeof(dword);
		}
		
		void* toNetwork(void *buf=NULL)
		{
			byte *data=(byte*)buf;
			
			if (data==NULL)
			{
				data=new byte[this->serializationSize()];
				if (data==NULL)
					throw NotEnoughMemoryException("Not enough memory to "
						"allocate findBestSubsystemByTypeIdParams "
						"serialization buffer");
			}
			byte *b=data;
			
			*(CentralDirectoryInterface::algorithm*)b=this->algorithm;
			Endian::from(Endian::xarxa, b,
				sizeof(CentralDirectoryInterface::algorithm));
			b+=sizeof(CentralDirectoryInterface::algorithm);

			*(CentralDirectoryInterface::algorithm*)b=this->nearestByIpUntieAlgorithm;
			Endian::from(Endian::xarxa, b,
				sizeof(CentralDirectoryInterface::algorithm));
			b+=sizeof(CentralDirectoryInterface::algorithm);
			
			*(short*)b=this->type;
			Endian::from(Endian::xarxa, b, sizeof(short));
			b+=sizeof(short);
			
			*(dword*)b=this->nearestByIPIP;
			b+=sizeof(dword);
			
			*(byte*)b=this->nearestByIPMask;
			b+=sizeof(byte);
			
			*(dword*)b=this->nearestByIPUntieIP;
			b+=sizeof(dword);

			return data;
		}
	
		dword serializationSize()
		{
			return sizeof(CentralDirectoryInterface::algorithm)*2+
				sizeof(short)+sizeof(dword)*2+sizeof(byte);
		}
	};
	
	struct findBestSubsystemByTypeNameParams
	{
		CentralDirectoryInterface::algorithm algorithm,
			nearestByIpUntieAlgorithm;;
		DB_VARCHAR(typeName,255);
		// Usats si l'algorisme principal es NearestByIP
		dword nearestByIPIP;	// Sempre en BigEndian
		byte nearestByIPMask;
		// Usats si el desempat es per NearestByIP
		dword nearestByIPUntieIP;	// Sempre en BigEndian
	
		void toLocalEndian()
		{
			Endian::from(Endian::xarxa, &algorithm, sizeof(CentralDirectoryInterface::algorithm));
			Endian::from(Endian::xarxa, &nearestByIpUntieAlgorithm, sizeof(CentralDirectoryInterface::algorithm));
		}
	
		void toNetworkEndian()
		{
			Endian::to(Endian::xarxa, &algorithm, sizeof(CentralDirectoryInterface::algorithm));
			Endian::to(Endian::xarxa, &nearestByIpUntieAlgorithm, sizeof(CentralDirectoryInterface::algorithm));
		}
	
		dword sizeOf()
		{
			return sizeof(algorithm)*2+sizeof(typeName)+sizeof(nearestByIPIP)+
				sizeof(nearestByIPMask)+sizeof(nearestByIPUntieIP);
		}
	};
	
	struct findAllSubsystemsByTypeIdParams
	{
		struct subsystemDesc
		{
			subsystemDesc() : addr(NULL), load(0.0f) {};
			~subsystemDesc()
			{
				STACKTRACE_INSTRUMENT();
			
				if (addr!=NULL)
					delete addr;
			}
			
			Address *addr;
			float load;
		};
	
		short type;
		dword nSubsys;
		subsystemDesc *subsystems;
			
		findAllSubsystemsByTypeIdParams() : type(0), nSubsys(0), subsystems(NULL)
		{
			STACKTRACE_INSTRUMENT();
			
		};
			
		~findAllSubsystemsByTypeIdParams()
		{
			STACKTRACE_INSTRUMENT();
			
			nSubsys=0;
			if (subsystems!=NULL)
			{
				delete [] subsystems;
				subsystems=NULL;
			}
		}
		
		void toLocal(void *data)
		{
			STACKTRACE_INSTRUMENT();
			
			byte *buf=(byte *)data;
			
			type=*((short*)buf);
			Endian::from(Endian::xarxa, &type, sizeof(short));
			buf+=sizeof(short);
					
			nSubsys=*((dword*)buf);
			Endian::from(Endian::xarxa, &nSubsys, sizeof(dword));
			buf+=sizeof(dword);
			
			if (subsystems!=NULL)
				delete [] subsystems;
				
	//		cout << "LBFASBTIP: new ssd" << endl;
			subsystems=new subsystemDesc[nSubsys];
	//		cout << "LBFASBTIP: /new ssd" << endl;
			
			for (dword s=0; s<nSubsys; s++)
			{
				dword ip=*((dword*)buf);
				buf+=sizeof(dword);
				
				word port=*((word*)buf);
				Endian::from(Endian::xarxa, &port, sizeof(word));
				buf+=sizeof(word);
	
	//			cout << "LBFASBTIP: " << s << ".addr " << endl;
				subsystems[s].addr=new Address(IP(ip), port);
	//			cout << "LBFASBTIP: /" << s << ".addr " << endl;
	
				subsystems[s].load=*((float*)buf);
				Endian::from(Endian::xarxa, &subsystems[s].load, sizeof(float));
				buf+=sizeof(float);
			}
		}
	
		void* toNetwork()
		{
			STACKTRACE_INSTRUMENT();
			
			byte *data=new byte[this->sizeOf()];
			byte *buf=data;
			
			*((short*)buf)=type;
			Endian::to(Endian::xarxa, buf, sizeof(short));
			buf+=sizeof(short);
					
			*((dword*)buf)=nSubsys;
			Endian::to(Endian::xarxa, buf, sizeof(dword));
			buf+=sizeof(dword);
			
			for (dword s=0; s<nSubsys; s++)
			{
				*((dword*)buf)=subsystems[s].addr->getIP().toDWord();
				buf+=sizeof(dword);
				
				*((word*)buf)=subsystems[s].addr->getPort();
				Endian::to(Endian::xarxa, buf, sizeof(word));
				buf+=sizeof(word);
	
				*((float*)buf)=subsystems[s].load;
				Endian::to(Endian::xarxa, buf, sizeof(float));
				buf+=sizeof(float);
			}
			
			return data;
		}
		
		dword sizeOf()
		{
			STACKTRACE_INSTRUMENT();
			
			return sizeof(type)+sizeof(nSubsys)+
				(sizeof(dword)+sizeof(word)+sizeof(float))*nSubsys;
		}
	};
	
	struct findAllSubsystemsByTypeNameParams
	{
		struct subsystemDesc
		{
			subsystemDesc() : addr(NULL), load(0.0f) {};
			~subsystemDesc()
			{
				STACKTRACE_INSTRUMENT();
			
				if (addr!=NULL)
					delete addr;
			}
			
			Address *addr;
			float load;
		};
	
		string type;
		dword nSubsys;
		subsystemDesc *subsystems;
	
		findAllSubsystemsByTypeNameParams() : type(string("")), nSubsys(0), subsystems(NULL)
		{
			STACKTRACE_INSTRUMENT();
			
		};

		~findAllSubsystemsByTypeNameParams()
		{
			STACKTRACE_INSTRUMENT();
			
			nSubsys=0;
			if (subsystems!=NULL)
			{
				delete [] subsystems;
				subsystems=NULL;
			}
		}
		
		void toLocal(void *data)
		{
			STACKTRACE_INSTRUMENT();
			
			byte *buf=(byte *)data;
			
			dword typeLen=*((dword*)buf);
			Endian::from(Endian::xarxa, &typeLen, sizeof(dword));
			buf+=sizeof(dword);
			
			type=string((char*)buf, typeLen);
			buf+=typeLen;
					
			nSubsys=*((dword*)buf);
			Endian::from(Endian::xarxa, &nSubsys, sizeof(dword));
			buf+=sizeof(dword);
			
			if (subsystems!=NULL)
				delete [] subsystems;
				
			subsystems=new subsystemDesc[nSubsys];
			
			for (dword s=0; s<nSubsys; s++)
			{
				dword ip=*((dword*)buf);
				buf+=sizeof(dword);
				
				word port=*((word*)buf);
				Endian::from(Endian::xarxa, &port, sizeof(word));
				buf+=sizeof(word);
	
				subsystems[s].addr=new Address(IP(ip), port);
	
				subsystems[s].load=*((float*)buf);
				Endian::from(Endian::xarxa, &subsystems[s].load, sizeof(float));
				buf+=sizeof(float);
			}
		}
	
		void* toNetwork()
		{
			STACKTRACE_INSTRUMENT();
			
			byte *data=new byte[this->sizeOf()];
			byte *buf=data;
			
			*((dword*)buf)=type.length();
			Endian::to(Endian::xarxa, buf, sizeof(dword));
			buf+=sizeof(dword);
			
			memmove(buf, type.c_str(), type.length());
			buf+=type.length();
					
			*((dword*)buf)=nSubsys;
			Endian::to(Endian::xarxa, buf, sizeof(dword));
			buf+=sizeof(dword);
			
			for (dword s=0; s<nSubsys; s++)
			{
				*((dword*)buf)=subsystems[s].addr->getIP().toDWord();
				buf+=sizeof(dword);
				
				*((word*)buf)=subsystems[s].addr->getPort();
				Endian::to(Endian::xarxa, buf, sizeof(word));
				buf+=sizeof(word);
	
				*((float*)buf)=subsystems[s].load;
				Endian::to(Endian::xarxa, buf, sizeof(float));
				buf+=sizeof(float);
			}
			
			return data;
		}
		
		dword sizeOf()
		{
			STACKTRACE_INSTRUMENT();
			
			return sizeof(dword)+type.length()+sizeof(nSubsys)+
				(sizeof(dword)+sizeof(word)+sizeof(float))*nSubsys;
		}
	};
	

protected:
	// DBGateway per les consultes :)
	DBGateway *db;
	// Index per l'algorisme ROUND_ROBIN
	int rrIndex;

	static RPCPacket* NoSubsystemFoundException(short type,
		CentralDirectoryInterface *_this, string extraInfo=string(""));
	static RPCPacket* findNearestByIPByTypeId(CentralDirectoryInterface *_this, Address *a, findBestSubsystemByTypeIdParams *p);
	static RPCPacket* findNearestByIPByTypeName(CentralDirectoryInterface *_this, Address *a, findBestSubsystemByTypeNameParams *p);
	
public:
	CentralDirectoryInterface(Address address, short type, Canis *cn=NULL);
	~CentralDirectoryInterface();

	static RPCPacket* findBestSubsystemByTypeId(CentralDirectoryInterface *_this, Address *a, void *params);
	static RPCPacket* findBestSubsystemByTypeName(CentralDirectoryInterface *_this, Address *a, void *params);
	static RPCPacket* findAllSubsystemsByTypeId(CentralDirectoryInterface *_this, Address *a, void *params);
	static RPCPacket* findAllSubsystemsByTypeName(CentralDirectoryInterface *_this, Address *a, void *params);
};


#endif
