/*
 *  FileException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef __SIRIUS__BASE__UTILS__FILEEXCEPTION_H
#define __SIRIUS__BASE__UTILS__FILEEXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class FileException : public Exception
{
 public:
	FileException(string msg, int code=0);
	FileException(int code, string msg);
	FileException(SerializedException &se);

	~FileException();
	
	virtual string getClass();
};
#endif

