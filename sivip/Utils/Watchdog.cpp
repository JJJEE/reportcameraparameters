#include <Utils/Watchdog.h>
#include <Utils/StrUtils.h>

void WatchdogCallback::watchdogTimeout(int secs)
{
	cout<<endl<<"*********"<<endl<<"*** WatchdogCallback::watchdogTimeout: "<<secs<<" seconds without writing, no watchdog callback set"<<endl<<"*********"<<endl;
}

Watchdog::Watchdog(int seconds, WatchdogCallback *notify):warningSeconds(seconds), lastResetSeconds(0), threadStarted(false), run(false), notify(notify)
{
}

void Watchdog::startCheck()
{
	lockData.lock();
	run=true;
	if(!threadStarted)
	{
		int id = this->start();
		this->detach(id);
		threadStarted = true;
	}
	lockData.unlock();
}

void Watchdog::stopCheck()
{
	lockData.lock();
	run=false;
	lockData.unlock();
}

void *Watchdog::execute(int id, void* args)
{
	lockData.lock();
	while(run)
	{
		lockData.unlock();
		//sleep(1);	
		lockData.lock();
		int time = lastResetSeconds++;
//		cout<<"Watchdog:timer:"<<time<<endl;
		if(time > warningSeconds/3 && time > 1)
		{
			cout<<"Watchdog: WARNING:"<<time<<" seconds since last update (max "<<warningSeconds<<") !!!"<<endl;
		}
		if(time > warningSeconds)
		{
			lockData.unlock();
			cout<<"Watchdog:"<<time<<" seconds spent since last update (>"<<warningSeconds<<"), notifying..."<<endl;
			notify->watchdogTimeout(time);
			lockData.lock();
		}
	}
	threadStarted = false;
	lockData.unlock();

	return NULL;
}

	

int Watchdog::resetTimer()
{
	lockData.lock();
	int res = lastResetSeconds;
	lastResetSeconds=0;
	lockData.unlock();
	return res;
}

