#pragma once

#ifndef __WindowsDefs_h
#define __WindowsDefs_h

#ifdef WIN32
//#pragma message("WinDef->winInc")
#include <Utils/WindowsInclude.h>
//#pragma message("WinDef->time")
#include <time.h>
//#pragma message("WinDef->Types")
#include <Utils/Types.h>
//#pragma message("WinDef->afxSck")
//#include <afxsock.h>         // For socket(), connect(), send(), and recv()
//#pragma message("WinDef->dirct")
#include <direct.h>
//#pragma message("WinDef->str")
#include <string>
//#pragma message("/WinDef")

#define __func__ string("__func__")

using namespace std;
//typedef int time_t;
/*
struct timeval
{ 
	time_t tv_sec; 
	time_t tv_usec; 
};
*/

#define EPOCHFILETIME (116444736000000000LL)

unsigned int sleep(u_int secs);
unsigned int usleep(u_int usecs);
void FILETIMEToTimeval(FILETIME ft, struct timeval *tv);
int gettimeofday(struct timeval *tv, void *timedone);
LPCWSTR StringToLPCWSTR(string s);

//struct tm* localtime_r
	//	(time_t *clock, struct tm* result);
int chdir(char *path);
int mkdir(const char *path, int mode);

#endif

#endif
