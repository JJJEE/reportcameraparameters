#include <Utils/Log.h>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Exception.h>

#include <iostream>

using namespace std;

string Log::getDateLogTag()
{
    time_t t=time(NULL);
	struct tm *tm_t=localtime(&t);
	
	string year=StrUtils::decToString(tm_t->tm_year+1900);
	string month=StrUtils::decToString(tm_t->tm_mon+1);
	string day=StrUtils::decToString(tm_t->tm_mday);
	string hour=StrUtils::decToString(tm_t->tm_hour);
	string min=StrUtils::decToString(tm_t->tm_min);
	string sec=StrUtils::decToString(tm_t->tm_sec);
	
	if (month.length()<2)
		month=string("0")+month;
	if (day.length()<2)
		day=string("0")+day;
	if (hour.length()<2)
		hour=string("0")+hour;
	if (min.length()<2)
		min=string("0")+min;
	if (sec.length()<2)
		sec=string("0")+sec;

	return string("[")+year+string("/")+month+string("/")+
		day+string(" ")+hour+string(":")+min+string(":")+sec+
		string("] ");
}

string Log::formatLogLine(string line)
{
	STACKTRACE_INSTRUMENT();

	
	string logLine=Log::getDateLogTag()+line+string("\n");

	return logLine;
}

FILE *Log::rotateIfNeeded(FILE *logFile)
{
	STACKTRACE_INSTRUMENT();

	if (maxSize!=0)
	{
		// Size Rotation
		// Per si no es "ab" (ho hauria de ser sempre)
		fseek(logFile, 0, SEEK_END);
		dword size=ftell(logFile);
		if (size>=maxSize)
		{
			string logLine=formatLogLine(string("Maximum file size reached. "
												"Rotating"));
			fwrite(logLine.c_str(), logLine.length(), 1, logFile);
			fclose(logFile);
			
			rotateLogFiles(filename);
			// Evitem dobles rotacions :D
			ttlStart=(dword)time(NULL);
			
			FILE *newLogFile=fopen(filename.c_str(), "ab");

			logLine=formatLogLine(string("New log file due to rotation "
										 "due to file size limit."));
			fwrite(logLine.c_str(), logLine.length(), 1, newLogFile);
			fclose(newLogFile);
			
			return newLogFile;
		}
	}

	if (ttl!=0)
	{
		// Time Rotation
		dword t=(dword)time(NULL);
		if (t-ttlStart >= ttl)
		{
			string logLine=formatLogLine(string("Time To Live reached. "
												"Rotating"));
			fwrite(logLine.c_str(), logLine.length(), 1, logFile);
			fclose(logFile);
			
			rotateLogFiles(filename);
			ttlStart=t;
			
			FILE *newLogFile=fopen(filename.c_str(), "ab");

			logLine=formatLogLine(string("New log file due to rotation "
										 "due to TTL limit."));
			fwrite(logLine.c_str(), logLine.length(), 1, newLogFile);
			fclose(newLogFile);
			
			return newLogFile;
		}
	}

	return logFile;
}

void Log::rotateLogFiles(string basename)
{
	STACKTRACE_INSTRUMENT();

	if (maxFiles<2)
		return;
	
	if (maxFiles>2)
		for (int i=maxFiles-2; i>=1; i--)
		{
			rename((basename+string(".")+StrUtils::decToString(i)).c_str(), 
				   (basename+string(".")+StrUtils::decToString(i+1)).c_str());
		}
	
	rename(basename.c_str(), (basename+string(".1")).c_str());
}

Log::Log(string fname, bool logStart)
{
	STACKTRACE_INSTRUMENT();

//	cout << "Log::Log" << endl;
	
	filename=fname;
	rotate=false;
	maxSize=0;
	
	ttl=0;
	ttlStart=(dword)time(NULL);
	
	maxFiles=5;
	
	if (logStart)
		logLine(string("Start Logging"));
}

Log::~Log()
{
	STACKTRACE_INSTRUMENT();

}

void Log::limitBySize(dword size)
{
	STACKTRACE_INSTRUMENT();

	if (size>1024 || size==0)
		maxSize=size;
	else
		maxSize=1024;
}

dword Log::getSizeLimit()
{
	STACKTRACE_INSTRUMENT();

	return maxSize;
}

void Log::limitByTime(dword seconds)
{
	STACKTRACE_INSTRUMENT();

	if (seconds>60 || seconds==0)
		ttl=seconds;
	else
		ttl=60;
}

dword Log::getTimeLimit()
{
	STACKTRACE_INSTRUMENT();

	return ttl;
}

dword Log::getElapsedTimeFromStart()
{
	STACKTRACE_INSTRUMENT();

	return ((dword)time(NULL))-ttlStart;
}

void Log::setMaxRotationFiles(byte nFiles)
{
	STACKTRACE_INSTRUMENT();

	maxFiles=nFiles;
}

byte Log::getMaxRotationFiles()
{
	STACKTRACE_INSTRUMENT();

	return maxFiles;
}

void Log::logLine(string l)
{
	STACKTRACE_INSTRUMENT();

	string logLine=formatLogLine(l);
	
	FILE *logFile=NULL;
	// logat de la linia
	try
	{
		logLock.lock();
		logFile=fopen(filename.c_str(), "ab");
		if (logFile==NULL)
			throw Exception(string("logFile ")+filename+string(" is NULL!!!!")); 		logFile=rotateIfNeeded(logFile);
		fwrite(logLine.c_str(), logLine.length(), 1, logFile);
		fclose(logFile);
		logLock.unlock();
	}
	catch (Exception &e)
	{
		if (logFile!=NULL)
			fclose(logFile);
		cout << e.getClass() << ": " << e.getMsg() << endl;
		logLock.unlock();
	}
	catch (...)
	{
		if (logFile!=NULL)
			fclose(logFile);
		logLock.unlock();
	}
}

