/*
 *  Timer.cpp
 *  
 *
 *  Created by David Marí Larrosa on 03/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Utils/Types.h>
#include <Utils/StrUtils.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#include <Utils/Timer.h>
#endif
#include <stdlib.h>
#include <math.h>
#pragma mark *** Operadors auxiliars
struct timeval operator - (struct timeval a, struct timeval b)
{
	struct timeval res;
	res.tv_sec=a.tv_sec-b.tv_sec;
	res.tv_usec=a.tv_usec-b.tv_usec;
	if (res.tv_usec<0)
	{
		res.tv_sec--;
		res.tv_usec+=1000000;
	}
	
	return res;
}

bool operator < (struct timeval a, struct timeval b)
{
	if(a.tv_sec<b.tv_sec)
		return true;
	if(a.tv_sec==b.tv_sec && a.tv_usec<b.tv_usec)
		return true;
	return false;
}

struct timeval operator + (struct timeval a, struct timeval b)
{
	struct timeval res;
	res.tv_sec=a.tv_sec+b.tv_sec;
	res.tv_usec=a.tv_usec+b.tv_usec;
	if (res.tv_usec>1000000)
	{
		res.tv_sec++;
		res.tv_usec-=1000000;
	}
	
	return a;
}

struct timeval operator * (struct timeval a, float b)
{
	a.tv_sec=(long)(a.tv_sec*b);
	a.tv_usec=(long)(a.tv_usec*b);

	a.tv_sec+=a.tv_usec/1000000;
	a.tv_usec=a.tv_usec%1000000;
	
	return a;
}

#pragma mark *** TimerInstant
TimerInstant::TimerInstant(int fps) : fps(fps)
{
	memset(&tInstant, 0, sizeof(tInstant));
}

TimerInstant::TimerInstant(struct timeval start, struct timeval end, int fps) : fps(fps)
{
	tInstant=end-start;
}

int TimerInstant::useconds()
{
	return tInstant.tv_sec*1000000+tInstant.tv_usec;
}

double TimerInstant::seconds()
{
	return ((double)tInstant.tv_sec)+((double)tInstant.tv_usec)/1000000.0;
}

double TimerInstant::frames()
{
	return (((double)tInstant.tv_sec)+((double)tInstant.tv_usec)/1000000.0)*((double)fps);
}

int TimerInstant::floorFrames()
{
//	return (fps*1000000)/(tInstant.tv_sec*1000000+tInstant.tv_usec);
	// Optem per menys eficient pero mes range-safe :P
	return (int)floor((((double)tInstant.tv_sec)+((double)tInstant.tv_usec)/1000000.0)*((double)fps));
}

int TimerInstant::FPS()
{
	return fps;
}

double TimerInstant::getInstantSeconds()
{
	double s=(double)tInstant.tv_sec + (double)tInstant.tv_usec/1000000.0;
	
	return s;
}

string TimerInstant::getInstant()
{
	string s=StrUtils::decToString(tInstant.tv_sec)+string(".");
	string us=string("00000")+StrUtils::decToString(tInstant.tv_usec);
	
	us=us.substr(us.length()-6);

	return s+us;
}

#pragma mark *** Timer

Timer::Timer(int fps, bool counting) : fps(fps), counting(counting)
{
	if (counting)
		this->start();
}

void Timer::start()
{
	gettimeofday(&tStart, NULL);
	counting=true;
}

TimerInstant Timer::time()
{
	if (counting==true)
		gettimeofday(&tEnd, NULL);
	
	return TimerInstant(tStart, tEnd, fps);
}

void Timer::stop()
{
	gettimeofday(&tEnd, NULL);
	counting=false;
}

void Timer::pause()
{
	stop();
}

void Timer::resume()
{
	counting=true;
}

int Timer::FPS()
{
	return fps;
}

void Timer::setStartSeconds(double s)
{
	tStart.tv_sec=(dword)floor(s);
	tStart.tv_usec=(dword)((s-floor(s))*1000000.0);
}

double Timer::getStartSeconds()
{
	double s=(double)tStart.tv_sec + (double)tStart.tv_usec/1000000.0;
	
	return s;
}

double Timer::getEndSeconds()
{
	double s=(double)tEnd.tv_sec + (double)tEnd.tv_usec/1000000.0;
	
	return s;
}

string Timer::getStartSecondsString()
{
	string s=StrUtils::decToString(tStart.tv_sec)+string(".");
	string us=string("00000")+StrUtils::decToString(tStart.tv_usec);
	
	us=us.substr(us.length()-6);

	return s+us;
}

string Timer::getEndSecondsString()
{
	string s=StrUtils::decToString(tEnd.tv_sec)+string(".");
	string us=string("00000")+StrUtils::decToString(tEnd.tv_usec);
	
	us=us.substr(us.length()-6);

	return s+us;
}
