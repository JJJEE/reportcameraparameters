#include <Utils/StrUtils.h>
#include <Utils/Exception.h>
#include <ctype.h>
#include <iostream>
#include <Utils/debugStackTrace.h>

string* StrUtils::digits=NULL;
string StrUtils::trim(string str)
{
	if (str.length()==0)
	{
		return str;
	}
	
	string::size_type start=0;
	while (start<str.length() && isspace(str[start]))
		start++;

	string::size_type end = str.length()-1;
	while (end>0 && isspace(str[end]))
		end--;

	string result("");
	
	if (end>=start)
		result=str.substr(start, end-start+1);

	return result;
}

// Retorna el primer tag indicat
string StrUtils::getFirstTagContents(string str, string tag, bool includeTags)
{
	string s=trim(str);


	// Trobem el primer inici de tag
	string::size_type openTagStart=s.find(string("<")+tag);

	if (openTagStart==string::npos)
	{
		// No hi ha tags - cadena buida, no es parse error "evidentment" :)
		return string("");
	}

	string::size_type openTagEnd=string::npos;

	string::size_type closeTagStart=string::npos;
	string::size_type closeTagEnd=string::npos;

	// Trobem on s'acaba el tag
	string::size_type off=openTagStart+(string("<")+tag).length();
	int bal=0;
	while (off<s.length() && openTagEnd==string::npos)
	{
		switch (s[off])
		{
		case '>':
			if (bal==0)
			{
				openTagEnd=off;
				if (s[off-1]=='/') //<tag/>
				{
					closeTagStart=openTagStart;
					closeTagEnd=openTagEnd;
				}
			}
			break;

		case '\"':
		case '\'':
			if (bal==0)
				bal=s[off]; //interior string <tag parm="string">
			else if (bal==s[off])
				bal=0;
			off++;
			break;

		default:
			off++;
			break;
		}
	}

	if (openTagEnd==string::npos)
	{
		// No hem trobat el final del tag d'obertura, es un parse error
		if (includeTags)
		{
			throw(Exception("<parseError code=\"0\">Did not find end of opening tag</parseError>"));
		}
		else
		{
			throw(Exception("Parse Error (0): Did not find end of opening tag"));
		}
	}
	

	if (closeTagStart!=string::npos)
	{
		// Tag buit
		if (includeTags)
		{
			return s.substr(openTagStart, openTagEnd-openTagStart+1);
		}
		else
		{
			return string("");
		}
	}

	// El balanceig de tags se'n va cap a l'esquerra
	int tagBal=-1;
	closeTagStart=0;
	string::size_type oTS=string::npos;
	string::size_type cTS=string::npos;

	while(tagBal<0)
	{
			//closeTagStart=s.find(string("</")+tag+string(">"), closeTagStart);
			//cout <<"wh tagbal<0:"<<tagBal<<" off:"<<off<<endl;

			cTS=s.find(string("</")+tag+string(">"), off);

			int offset=off;
			char next;
			do{
				oTS=s.find(string("<")+tag, offset);
				next=s[oTS+tag.length()+1];
				offset=offset+tag.length()+1;
			}while(oTS!=string::npos && next!=' ' && next!='/' && next!='>');
			if(cTS==string::npos)
			{
				// No hem trobat el tag de tancament, es un parse error
				if (includeTags)
				{
					throw(Exception("<parseError code=\"1\">Did not find needed closing tag</parseError>"));
				}
				else
				{
					throw(Exception("Parse Error (1): Did not find needed closing tag"));
				}
				
			}
			if(oTS!=string::npos && oTS<cTS)
			{
				tagBal--;
				off= oTS+(string("<")+tag).length();
			}
			else
			{
				tagBal++;
				off= cTS+(string("</")+tag).length();
			}
	}
	closeTagStart=cTS;
	closeTagEnd=closeTagStart+(string("</")+tag+string(">")).length();

	// Trobem el tancament de tag (si no es tag buit)

	if (includeTags)
	{
		return s.substr(openTagStart, closeTagEnd-openTagStart);
	}
	else
	{
		return s.substr(openTagEnd+1, closeTagStart-openTagEnd-1);
	}
	
}
	/*
	while (tagBal!=0)
	{
		while (tagBal<0)	// Balancejem per la dreta
		{
			closeTagStart=s.find(string("</")+tag+string(">"), closeTagStart);
			if (closeTagStart==string::npos)
			{
				// No hem trobat el tag de tancament, es un parse error
				if (includeTags)
					return string("<parseError code=\"1\">Did not find needed closing tag</parseError>");
				else
					return string("Parse Error (1): Did not find needed closing tag");
			}
			else if (closeTagStart<openTagStart)
			{
				// Hi ha un closing tag de mes
				if (includeTags)
					return string("<parseError code=\"2\">Found extra non-matching closing tag</parseError>");
				else
					return string("Parse Error (2): Found extra non-matching closing tag");
			}

			closeTagEnd=closeTagStart+(string("</")+tag+string(">")).length();
			tagBal++;
		}

		// Aqui tenim la garantia que tagBal es 0 
      // Busquem tags d'obertura abans del tag de ncament, per balanceig

		
		string::size_type oTS;
		int offset=openTagEnd;
		char next;
		do{
			oTS=s.find(string("<")+tag, offset);
			next=s[oTS+tag.length()+1];
			offset=offset+tag.length()+1;
			//if(oTS!=string::npos)
			//	cout <<"-------gftc::oTS, tag:"<<tag<<" trob"<<s.substr(oTS, tag.length()+2)<<"next"<<next<<endl;
		}while(oTS!=string::npos && next!=' ' && next!='/' && next!='>');
		// si el tag es una subcadena d'un fill peta: mirar que el char seguent sigui " ", "/", ">"
		
		if (oTS==string::npos)		// No hi ha tags d'obertura i estem balancejats
		{
			// Retornem el catxo d'string q toca
			if (includeTags)
				return s.substr(openTagStart, closeTagEnd-openTagStart);
			else
				return s.substr(openTagEnd+1, closeTagStart-openTagEnd-1);
		}
		else
			tagBal--;
		else
		{
			cout << " pre while oTS<clTgSt ..."<<endl;
			while (oTS<closeTagStart&&oTS!=string::npos)
			{
				cout << "while oTS<clTgSt ..."<<oTS<<" tag:"<<tag<<" bal"<<bal<<endl;
				cout << "closeTagStart:"<<closeTagStart<<" tagbal:"<<tagBal<<endl;
				//cout << "s:"<<s<<endl;
				//cout << endl;
				//cout << "sbst:"<<s.substr(oTS)<<endl;
				cout << endl<< endl<< endl;
				string::size_type oTE=string::npos;
				string::size_type cTS=string::npos;
				string::size_type cTE=string::npos;
				// Trobem on s'acaba el tag
	
				string::size_type off=oTS+(string("<")+tag).length();
				int bal=0;
				while (off<s.length() && oTE==string::npos)
				{
					switch (s[off])
					{
						case '>':
							if (bal==0)
							{
								oTE=off;
								if (s[off-1]=='/')
								{
									cTS=oTS;
									cTE=oTE;
								}
							}
							off++;
							break;
							
						case '\"':
						case '\'':
							if (bal==0)
								bal=s[off];
							else if (bal==s[off])
								bal=0;
							off++;
							break;
							
						default:
							off++;
							break;
					}
				}
				
				if (oTE==string::npos)
				{
					// No hem trobat el final del tag d'obertura, es un parse error
					if (includeTags)
						return string("<parseError code=\"0\">Did not find end of opening tag</parseError>");
					else
						return string("Parse Error (0): Did not find end of opening tag");
				}
				
				if (cTS!=string::npos)
				{
					// Tag Buit, no alterem tagBal
			//		string::size_type oTS;
					int offset=oTE;
					char next;
					do{
						oTS=s.find(string("<")+tag, offset);
						next=s[oTS+tag.length()+1];
						offset+=tag.length()+1;
					}while(oTS!=string::npos && next!=' ' && next!='/' && next!='>');
					//string::size_type oTS=s.find(string("<")+tag, oTE);
					cout << "CTS!=npos"<<endl;
					continue;
				}
				tagBal--;
			}
		}
		
	}
	*/

// Retorna els subtags del tag complet i unic q rep
string StrUtils::getSubTags(string str, string parentTag)
{
	string parentTagConts=getFirstTagContents(str, parentTag);

	string s=trim(parentTagConts);

	// Trobem el primer inici de tag
	string::size_type openTagStart=0;
	string::size_type openTagEnd=string::npos;

	string::size_type closeTagStart=string::npos;
	string::size_type closeTagEnd=string::npos;

	// Trobem on s'acaba el tag d'obertura
	string::size_type off=openTagStart+(string("<")+parentTag).length();
	int bal=0;
	while (off<s.length() && openTagEnd==string::npos)
	{
		switch (s[off])
		{
		case '>':
			if (bal==0)
			{
				openTagEnd=off;
				if (s[off-1]=='/')
				{
					closeTagStart=openTagStart;
					closeTagEnd=openTagEnd;
				}
			}
			break;

		case '\"':
		case '\'':
			if (bal==0)
				bal=s[off];
			else if (bal==s[off])
				bal=0;
			off++;
			break;

		default:
			off++;
			break;
		}
	}

	if (openTagEnd==string::npos)
	{
		// No hem trobat el final del tag d'obertura, es un parse error
		throw(Exception("<parseError code=\"0\">Did not find end of opening tag</parseError>"));
	}

   if (closeTagStart!=string::npos)
	{
		// Tag buit - No te subtags, cadena buida
		return string("");
	}

	// Si no ha sigut tag buit, la cosa acaba en "</tag>"
	closeTagStart=s.length()-(string("</")+parentTag+string(">")).length();
	closeTagEnd=s.length()-1;

	string data=s.substr(openTagEnd+1, closeTagStart-openTagEnd-1);

	// I finalment trobem les subtags, eliminant les dades de nivell actual.
	string subTags="";
	string sub=" ";
	
	while (sub.length()>0)
	{
		string::size_type ini=data.find(string("<"));

		if (ini==string::npos)
		{
			sub="";
			continue;
		}

		ini++;

		string::size_type sp=data.find(string(" "), ini);
		string::size_type barra=data.find(string("/"), ini);
		string::size_type tanca=data.find(string(">"), ini);

		if (sp==string::npos) sp=s.length();
		if (barra==string::npos) barra=s.length();
		if (tanca==string::npos) tanca=s.length();

		string::size_type end = s.length();

		if (sp<barra && sp<tanca)
			end=sp;
		else if (barra<sp && barra<tanca)
			end=barra;
		else if (tanca<barra && tanca<sp)
			end=tanca;

		string st=data.substr(ini, end-ini);
		
		sub=getFirstTagContents(data, st);
		subTags+=sub;

		int aux=data.find(sub);
		if(aux==string::npos)
			break;
		data.replace(aux, sub.length(), string(""));
	}

	return subTags;
}

// retorna el contingut del primer tag, sigui quin sigui
string StrUtils::getFirstTagContents(string str, bool includeTags)
{
	string s=str;

	string::size_type ini=s.find(string("<"));

	if (ini==string::npos)
	{
		return string("");
	}

	ini++;

	string::size_type sp=s.find(string(" "), ini);
	string::size_type barra=s.find(string("/"), ini);
	string::size_type tanca=s.find(string(">"), ini);

	if (sp==string::npos) sp=s.length();
	if (barra==string::npos) barra=s.length();
	if (tanca==string::npos) tanca=s.length();

	string::size_type end = s.length();

	if (sp<barra && sp<tanca)
		end=sp;
	else if (barra<sp && barra<tanca)
		end=barra;
	else if (tanca<barra && tanca<sp)
		end=tanca;

	string tag=s.substr(ini, end-ini);

	string tagConts=StrUtils::getFirstTagContents(s, tag, includeTags);

	return tagConts;
}

// converteix un char en una cadena de dos codificant en hexa
string StrUtils::decToHex(unsigned char dec)
{
/*	string d2h[16]={string("0"),
		string("1"),
		string("2"),
		string("3"),
		string("4"),
		string("5"),
		string("6"),
		string("7"),
		string("8"),
		string("9"),
		string("a"),
		string("b"),
		string("c"),
		string("d"),
		string("e"),
		string("f")};
*/
	if(digits==NULL)
	{
		digits=new string[16];
		digits[0]=string("0");
		digits[1]=string("1");
		digits[2]=string("2");
		digits[3]=string("3");
		digits[4]=string("4");
		digits[5]=string("5");
		digits[6]=string("6");
		digits[7]=string("7");
		digits[8]=string("8");
		digits[9]=string("9");
		digits[10]=string("a");
		digits[11]=string("b");
		digits[12]=string("c");
		digits[13]=string("d");
		digits[14]=string("e");
		digits[15]=string("f");
	}
	//return d2h[(dec&0xf0)>>4]+d2h[dec&0xf];
	return digits[(dec&0xf0)>>4]+digits[dec&0xf];
}

unsigned char StrUtils::hexToDec(string str)
{
	string s=str;
	if (s.length()>2)
	{
		return 0;
	}
	else if (s.length()==1)
	{
		s=string("0")+s;
	}

	for (string::size_type i=0; i<2; i++)
	{
		if (s[i]>='0' && s[i]<='9')
			s[i]-='0';
		else if (s[i]>='a' && s[i]<='f')
		{
			s[i]-='a';
			s[i]+=10;
		}
		else if (s[i]>='A' && s[i]<='F')
		{
			s[i]-='A';
			s[i]+=10;
		}
		else
			s[i]=0;
	}

	unsigned char val=((unsigned char)s[0]*16)+(unsigned char)s[1];

	return val;
}

// converteix un enter en cadena
string StrUtils::decToString(long long num, bool debug)
{
	if (num==0)
	{
		return string("0");
	}
	
/*	string conv[10]={string("0"),
		string("1"),
		string("2"),
		string("3"),
		string("4"),
		string("5"),
		string("6"),
		string("7"),
		string("8"),
		string("9")};
*/	
	if(digits==NULL)
	{
		digits=new string[16];
		digits[0]=string("0");
		digits[1]=string("1");
		digits[2]=string("2");
		digits[3]=string("3");
		digits[4]=string("4");
		digits[5]=string("5");
		digits[6]=string("6");
		digits[7]=string("7");
		digits[8]=string("8");
		digits[9]=string("9");
		digits[10]=string("a");
		digits[11]=string("b");
		digits[12]=string("c");
		digits[13]=string("d");
		digits[14]=string("e");
		digits[15]=string("f");
	}
	//cout<<" ---------- 2 ------------- decToString:"<<dbg<<endl;
	
	string n=string("");
	
	string sign=string("");
	
	if (num<0)
	{
		sign=string("-");
		num=-num;
	}
	
	while (num>0)
	{
		//n=conv[num%10]+n;
		n=digits[num%10]+n;
		num/=10;
	}
	
	n=sign+n;
	
	return n;
}

// converteix un enter en cadena
string StrUtils::floatToString(float num)
{
	if (num==0)
	{
		return string("0");
	}
	
/*	string conv[10]={string("0"),
		string("1"),
		string("2"),
		string("3"),
		string("4"),
		string("5"),
		string("6"),
		string("7"),
		string("8"),
		string("9")};
	*/
	if(digits==NULL)
	{
		digits=new string[16];
		digits[0]=string("0");
		digits[1]=string("1");
		digits[2]=string("2");
		digits[3]=string("3");
		digits[4]=string("4");
		digits[5]=string("5");
		digits[6]=string("6");
		digits[7]=string("7");
		digits[8]=string("8");
		digits[9]=string("9");
		digits[10]=string("a");
		digits[11]=string("b");
		digits[12]=string("c");
		digits[13]=string("d");
		digits[14]=string("e");
		digits[15]=string("f");
	}
	
	string n=string("");
	
	string sign=string("");
	
	if (num<0)
	{
		sign=string("-");
		num=-num;
	}
	
	int inum=(int)num;
	do
	{
		//n=conv[inum%10]+n;
		n=digits[inum%10]+n;
		inum/=10;
	}
	while (inum>0);

	num=num-((int)num);

    if(num>0.0001)
	{
		n=n+string(".");
		int ni;
		while (num>0.0001)
		{
			//cout<<"floatToString pre string n:"<<n<<" num:"<<num<<" *10:"<<num*10<<" int:"<<((int)(num*10))<<" digits[]:"<<digits[(int)(num*10)]<<endl;
			ni = (int)(num*10);
			num=num*10-(ni);
			if(num==1)
			{
				//          cout<<"floatToString WARNING: num:"<<num<<" ni:"<<ni<<endl;
				num=num-1.0;
				ni++;
				//          cout<<"floatToString          num:"<<num<<" ni:"<<ni<<endl;
			}
			n=n+digits[ni];
		}
	}
	n=sign+n;

	return n;
}



// converteix un enter en cadena
string StrUtils::hexToString(unsigned int num)
{
	if (num==0)
	{
		return string("00");
	}
/*
	string conv[16]={string("0"),
		string("1"),
		string("2"),
		string("3"),
		string("4"),
		string("5"),
		string("6"),
		string("7"),
		string("8"),
		string("9"),
		string("a"),
		string("b"),
		string("c"),
		string("d"),
		string("e"),
		string("f"),
	};
*/	
	if(digits==NULL)
	{
		digits=new string[16];
		digits[0]=string("0");
		digits[1]=string("1");
		digits[2]=string("2");
		digits[3]=string("3");
		digits[4]=string("4");
		digits[5]=string("5");
		digits[6]=string("6");
		digits[7]=string("7");
		digits[8]=string("8");
		digits[9]=string("9");
		digits[10]=string("a");
		digits[11]=string("b");
		digits[12]=string("c");
		digits[13]=string("d");
		digits[14]=string("e");
		digits[15]=string("f");
	}
	
	string n=string("");
	
	while (num>0 || n.length()<2)
	{
		//n=conv[num&0xf]+n;
		n=digits[num&0xf]+n;
		num>>=4;
	}
	
	return n;
}

// converteix una cadena en hexa a un enter
unsigned int StrUtils::stringToHex(string str)
{
	string s=str;

	unsigned int val=0;
	for (string::size_type i=0; i<str.length(); i++)
	{
		if (s[i]>='0' && s[i]<='9')
			s[i]-='0';
		else if (s[i]>='a' && s[i]<='f')
		{
			s[i]-='a';
			s[i]+=10;
		}
		else if (s[i]>='A' && s[i]<='F')
		{
			s[i]-='A';
			s[i]+=10;
		}
		else
			break;
		
		val=(val<<4)+((unsigned int)s[i]);
	}

	return val;
}

string StrUtils::getDateString()
{
    time_t t=time(NULL);
	return StrUtils::toDateString(t);
}

string StrUtils::toDateString(time_t t)
{
	struct tm *tm_t=localtime(&t);
	
	string year=StrUtils::decToString(tm_t->tm_year+1900);
	string month=StrUtils::decToString(tm_t->tm_mon+1);
	string day=StrUtils::decToString(tm_t->tm_mday);
	string hour=StrUtils::decToString(tm_t->tm_hour);
	string min=StrUtils::decToString(tm_t->tm_min);
	string sec=StrUtils::decToString(tm_t->tm_sec);
	
	if (month.length()<2)
		month=string("0")+month;
	if (day.length()<2)
		day=string("0")+day;
	if (hour.length()<2)
		hour=string("0")+hour;
	if (min.length()<2)
		min=string("0")+min;
	if (sec.length()<2)
		sec=string("0")+sec;
	
	return year+string("/")+month+string("/")+
		day+string(" ")+hour+string(":")+min+string(":")+sec;

}

string StrUtils::toSQLTimestamp(struct timeval t)
{
	struct tm *tm_t=localtime((time_t*)&t.tv_sec);
	
	string year=StrUtils::decToString(tm_t->tm_year+1900);
	string month=StrUtils::decToString(tm_t->tm_mon+1);
	string day=StrUtils::decToString(tm_t->tm_mday);
	string hour=StrUtils::decToString(tm_t->tm_hour);
	string min=StrUtils::decToString(tm_t->tm_min);
	string sec=StrUtils::decToString(tm_t->tm_sec);
	
	if (month.length()<2)
		month=string("0")+month;
	if (day.length()<2)
		day=string("0")+day;
	if (hour.length()<2)
		hour=string("0")+hour;
	if (min.length()<2)
		min=string("0")+min;
	if (sec.length()<2)
		sec=string("0")+sec;

	string us=string("00000")+StrUtils::decToString(t.tv_usec);
	us=us.substr(us.length()-6);
	
	
	return year+string("-")+month+string("-")+
		day+string(" ")+hour+string(":")+min+string(":")+
		sec+string(".")+us;

}

// split, a la perl pero sense regex - sep pot ser un caracter o diversos 
// (multiples separadors)
list<string> StrUtils::split(string str, string sep, int nSplits)
{
	int n = str.length();
	int start, stop;
	int splits=0;
	list <string> paraules;
	
	start = str.find_first_not_of(sep);
	while ((start >= 0) && (start < n)) {
		stop = str.find_first_of(sep, start);
		if ((stop < 0) || (stop > n)) stop = n;
		paraules.push_back(str.substr(start, stop - start));
		splits++;
		
		if (nSplits>0 && splits>=nSplits)
		{
			paraules.push_back(str.substr(stop+1));
			return paraules;
		}
		
		start = str.find_first_not_of(sep, stop+1);
	}
	
	return paraules;
}

string StrUtils::readLine(Socket *s)
{
	char buf[512];
	int p=0, totalRead=0;
	string str("");
	string::size_type lineEnd;
	
	do	
	{
		// Lectura de l'ultim que haviem peekat i que no era valid
		if (p>0)
		{
			// p hauria de valdre al mateix abans i despres, pro mai se sap...
			p=s->read(buf, p);
			totalRead+=p;
		}
		
		p=s->peek(buf, 512);
		
		str+=string(buf, p);
	}
	while (str.length()<2 || (lineEnd=str.find("\r\n"))==string::npos);

	// Ultima lectura	
	str=str.substr(0, lineEnd+2);
	s->read(buf, lineEnd+2-totalRead);
	
	return str;
}

// Converteix un path en una llista de noms de nodes
list<string> StrUtils::pathToList(string path)
{
	list<string> pathNodes;

	while (path.length()>0)
	{
		string::size_type sepPos=path.find("/");

		if (sepPos==string::npos)
		{
			pathNodes.push_back(path);
			path=string("");
			continue;
		}

		string pathComponent=path.substr(0, sepPos);
		if (pathComponent.length()>0)
			pathNodes.push_back(pathComponent);
		path=path.substr(sepPos+1);
	}

	return pathNodes;
}

// Fa un dump en hexa de l'string
string StrUtils::hexDump(string str )
{
	string hex("");
	int len=str.length();
	int i;
	for (i=0; i<len; i++)
	{
		if ((i%16)==0)
		{
			if (i!=0)
			{
				hex+=string("    ");
				for (int j=i-16; j<i; j++)
				{
					hex+=(str[j]>=32 && ((byte)str[j]<=127)?str[j]:'.');
				}
			}
			
			hex+=string("\n");
		}
		else
			hex+=string(" ");

		hex+=StrUtils::hexToString((byte)str[i]);
	}

	for (; (i%16)!=0; i++)
	{
		hex+=string("   ");
	}
	hex+=string("    ");
	for (int j=i-16; j<i && j<len; j++)
	{
		hex+=(str[j]>=32 && ((byte)str[j]<=127)?str[j]:'.');
	}
	
	hex+=string("\n");
	return hex;
}

string StrUtils::replace(string str, string search, string replacement)
{
	string::size_type pos=str.find(search);
	while (pos!=string::npos)
	{
		str.replace(pos, search.length(), replacement);
		pos=str.find(search);
	}
	
	return str;
}

// true si la cadena comença per ...
bool StrUtils::startsWith(string str, string search, bool caseInsensitive/*=false*/)
{
	return (str.substr(0, search.length())==search);
}

// true si la cadena acaba per ...
bool StrUtils::endsWith(string str, string search, bool caseInsensitive/*=false*/)
{
	return (str.substr(str.length()-search.length(), search.length())==search);
}


#ifdef WIN32
string StrUtils::errorString(DWORD err)
{
	LPTSTR s;
	//CString s;
	if(FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		err,
		0,
		(LPTSTR)&s,
		0,
		NULL) == 0)
	{ 	
		return string("Error desconegut");
	}

//	string res;
//#ifdef UNICODE
	wstring w((TCHAR*)s);
	string res(w.begin(), w.end());
//	LPTSTR p = _tcschr(s,_T('\0'));
//	string res((char*)(LPCTSTR)s.GetBuffer());
//	::LocalFree(s);
//	::LocalFree(p);
//	res=*((string*)s);
	return res;
} // ErrorString
#endif




