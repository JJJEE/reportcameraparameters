#include <Utils/WindowsDefs.h>
#ifdef WIN32


LPCWSTR StringToLPCWSTR(string s)
{
	int len = s.size()+1;
	wchar_t *wText = new wchar_t[len];
	memset(wText,0,len);
	::MultiByteToWideChar(CP_ACP, NULL, s.c_str(), -1, wText,len );
	return wText;
}
unsigned int sleep(u_int secs)
{
	Sleep(secs*1000);
	return 0;
}

unsigned int usleep(u_int usecs)
{
	Sleep(usecs/1000);
	return 0;
}


void FILETIMEToTimeval(FILETIME ft, struct timeval *tv)
{
	LARGE_INTEGER li;
	__int64 t;

	li.LowPart=ft.dwLowDateTime;
	li.HighPart=ft.dwHighDateTime;
	t  = li.QuadPart; /* In 100-nanosecond intervals */
	t -= EPOCHFILETIME; /* Offset to the Epoch time */
	t /= 10; /* In microseconds */
	tv->tv_sec=(long)(t / 1000000);
	tv->tv_usec=(long)(t % 1000000);
}

int gettimeofday(struct timeval *tv, void *timedone)
{
/*	FILETIME ft;
	if(tv==NULL)
		return -1;
	GetSystemTimeAsFileTime(&ft);
	FILETIMEToTimeval(ft,tv);
	return 0;
*/
	union {
	    long long ns100; /*time since 1 Jan 1601 in 100ns units */
		FILETIME ft;
	} now;

    GetSystemTimeAsFileTime( &(now.ft) );
    tv->tv_usec=(long)((now.ns100 / 10LL) % 1000000LL );
    tv->tv_sec= (long)((now.ns100-(116444736000000000LL))/10000000LL);
	struct timeval tv2;
	FILETIME ft2;
    GetSystemTimeAsFileTime(&ft2);
	FILETIMEToTimeval(ft2,&tv2);
	return 0;

}

/*
struct tm* localtime_r(time_t *clock, struct tm *result)
{
	struct tm *res=localtime(clock);
	if(result!=NULL)
		memcpy(result, res, sizeof(struct tm));
	return res;
}*/

int chdir(char *path)
{
	return _chdir(path);
}

int mkdir(const char *path, int mode)
{
	return _mkdir(path);
}

#endif
