#ifndef __SIRIUS__BASE__UTILS__EXCEPTION_H
#define __SIRIUS__BASE__UTILS__EXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/SerializedException.h>

using namespace std;

class Exception
{
private:
	int code;
	string msg;
	
public:
	Exception(string msg, int code=0);
	Exception(int code, string msg);
	Exception(SerializedException &se);
	
	~Exception();
	int getCode();
	string getMsg();   
	virtual string getClass();
	SerializedException *serialize();
};
#endif
