#include <Utils/DBConnection.hh>
#include <Utils/DBGateway.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <Utils/ServiceException.h>
#include <stdio.h>
#include <string.h>

DBConnection::DBConnection(Address origen, short type, Canis *cn):DBGateway(origen, type, cn)
{

}


DBConnection::~DBConnection()
{

}

const char* DBConnection::ask(string s,  RPC *dbRPC, int callId, Address *keepAliveAddr)
{
	STACKTRACE_INSTRUMENT();
	
	//	cout<<" DBGW:: call -> findDB()"<<endl;
	findDB(); 
	byte* c		= (byte*) s.c_str();
	dword len	= s.length();
	bool newRPC	= (dbRPC==NULL);

	RPCPacket p(origen, callId, c, len, type, -2);

	int dbfail=0;
	while (true)
	{
		RPCPacket *res	= NULL;
		try
		{
			if(newRPC && dbRPC == NULL)
			{
				dbRPC	= getRPC();
				if( dbRPC == NULL )
					return NULL;
			}

			res=dbRPC->call(p, keepAliveAddr);

			if(newRPC && dbRPC!=NULL)  {
				//freeRPC(dbRPC);
				delRPC(dbRPC);
			}
			string resp((char*)res->getData(), (size_t)res->getSize());

			delete res;
			res=NULL;
		

			return resp.c_str();
		}
		catch (ServiceException &se)
		{
			cout << "DBConnection::call ServiceException:" << se.getClass() << ": " << se.getMsg()<<endl;
						
			// Si es una ServiceException probablement ve d'algun error amb
			// el query i no te sentit reintentar res.
			if(newRPC && dbRPC!=NULL)
				freeRPC(dbRPC);

			if(res != NULL)
				delete res;
			res=NULL;
			throw se;
		}
		catch (Exception &e)
		{
			if(res != NULL)
				delete res;
			res=NULL;
			
			if(dbRPC == NULL && (e.getCode()==2 || e.getMsg()==string("Unable to create socket")))
			{
				//e.serialize()->materializeAndThrow(true);
				throw;
			}

			if(++dbfail==2)
			{
				dbMutex.lock();
				if(dbRPC != NULL && db == dbRPC->getDefaultDestination())
				{
					Address ant = db;
					Address dba;
					db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
					dbMutex.unlock();
					findDB();
					cout << "DBConnection::call error, searching for new DB (from:"<<ant.toString()<<" to:"<<db.toString()<<", exception " << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				//	cout<<" query:"<<s<<endl;
				}
				else
				{
					dbMutex.unlock();
					cout << "DBConnection::call error, NOT searching for new DB: already changed ( new:"<<db.toString()<<", from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				}

				if(newRPC && dbRPC!=NULL)
				{
					delRPC(dbRPC);
					dbRPC=NULL;
					newRPC=true;
				}
				throw e;
//				return NULL;
			}
			else
			{
				cout << "DBConnection::call error, retry("<<dbfail<<"):" << e.getClass() << ": " << e.getMsg()
					<< endl;

			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBConnection::call, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
	};
	
	return NULL;
}

const char* DBConnection::query(string query)
{
	this->findDB();

	RPC *rpc	= NULL;
	rpc 		= this->getRPC();

	

	if ( rpc == NULL)
	{
		cout << "DBConnection::query(): Error accediendo a la base de datos" << endl;
		return 0;
	}

	//m_dbGateway->disableTransactionMode(rpc);
	this->begin(rpc);


	string response = this->ask(query, rpc, this->queryServiceId);

	char* resp = new char[response.size()+1];
	memcpy(resp, response.c_str(), response.size()+1);
	if(rpc!=NULL)  {
				//freeRPC(rpc);
				delRPC(rpc);
	}


	return resp;
}

XML* DBConnection::queryXML(string query)
{
	this->findDB();
	RPC *rpc	= NULL;
	rpc 		= this->getRPC();
	
	if ( rpc == NULL)
	{
		cout << "DBConnection::query(): Error accediendo a la base de datos" << endl;
		return 0;
	}

	//m_dbGateway->disableTransactionMode(rpc);
	this->begin(rpc);

	return this->call(query, rpc, this->queryServiceId);
}

void DBConnection::command(string order)
{
	RPC rpc(cn->getDBAddress());
	RPCPacket pIns(cn->getAddress(), 1, (byte*)order.c_str(), order.length(), 0,-2, true);

	RPCPacket *pResIns 	= 	rpc.call(pIns);

	delete pResIns;
}
