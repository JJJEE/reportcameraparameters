#ifndef __SIRIUS__BASE__UTILS__REDIRECTCALLEXCEPTION_H
#define __SIRIUS__BASE__UTILS__REDIRECTCALLEXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>
#include <Sockets/Address.h>

using namespace std;

class RedirectCallException : public Exception
{
	Address addr;

 public:
	RedirectCallException(string msg, int code=0);
	RedirectCallException(int code, string msg);
	RedirectCallException(SerializedException &se);
	RedirectCallException(string msg, Address addr, int code=0);

	~RedirectCallException();

	Address getAddress();
	
	virtual string getClass();
};

#endif
