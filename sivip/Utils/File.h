/*
 *  File.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__UTILS__FILE_H
#define __SIRIUS__BASE__UTILS__FILE_H

//#pragma message("File.h")
#include <Threads/Thread.h>
#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Utils/FileException.h>
#include <string>
#include <stdio.h>
#include <fcntl.h>
#ifndef WIN32
#include <unistd.h>
#else
#include <Utils/WindowsDefs.h>
#endif
#include <sys/types.h>

using namespace std;
#ifdef WIN32
#define NULL_HANDLE NULL
#else
#define NULL_HANDLE -1
#endif

class File
{
protected:
#ifdef WIN32
	HANDLE f;
#else
public://TODO treure, es un test
	int f;
#endif
	fileOffset currentOff;
	Mutex opLock;
	
	static int accesModeToOpenMode(const dword mode, const bool newFileRW);
	
public:
	static const dword readAccessMode=1;
	static const dword writeAccessMode=2;
	static const dword appendAccessMode=4;

	dword accessMode;
	string filename;
	static string dirSep;
	
#ifdef WIN32
	enum seekType {SEEK_FROMSTART=FILE_BEGIN, SEEK_FROMCURRENT=FILE_CURRENT, SEEK_FROMEND=FILE_END};
#else
	enum seekType {SEEK_FROMSTART=SEEK_SET, SEEK_FROMCURRENT=SEEK_CUR, SEEK_FROMEND=SEEK_END};
#endif
	
	File(const string filename, const dword mode=File::readAccessMode, const bool newFileRW=false, const bool errorOut=true);
	virtual ~File();

	virtual int read(void *data, dword size=1);
	virtual int write(const void *data, dword size=1);
	virtual fileOffset tell();
	virtual fileOffset currentOffset();
	// Per casos xungus de deteccio de tipus, ja que fileOffset es 64bit
	virtual void seek(int off, int from=File::SEEK_FROMSTART);
	virtual void seek(fileOffset off, int from=File::SEEK_FROMSTART);
	virtual fileOffset size();

	virtual string getFileName();

	virtual void diskOperationLock();
	virtual void diskOperationUnlock();
};

#endif
