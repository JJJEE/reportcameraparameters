#include <Utils/CheckPoint.h>
#include <string>

using namespace std;

CheckPoint::CheckPoint(string file, dword line)
{
	this->file=file;
	this->line=line;
}

CheckPoint::~CheckPoint()
{
}

void CheckPoint::checkPoint(dword l)
{
	this->line=l;
}

string CheckPoint::getCheckPointFile()
{
	return this->file;
}

dword CheckPoint::getCheckPointLine()
{
	return this->line;
}

