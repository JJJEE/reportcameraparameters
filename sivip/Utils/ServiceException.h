#ifndef __SIRIUS__BASE__UTILS__SERVICEEXCEPTION_H
#define __SIRIUS__BASE__UTILS__SERVICEEXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class ServiceException : public Exception
{
 public:
	ServiceException(string msg, int code=0);
	ServiceException(int code, string msg);
	ServiceException(SerializedException &se);

	~ServiceException();
	
	virtual string getClass();
};

#endif
