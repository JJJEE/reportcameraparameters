/*
 *  SerializedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 15/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Utils/SerializedException.h>
#include <Utils/Exception.h>
#include <Sockets/IPException.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/RPCException.h>
#include <Utils/ServiceException.h>
#include <Plugins/GestorException.h>
#include <Plugins/GestorImageExceptions.h>
#include <Plugins/GestorControlExceptions.h>
#include <Plugins/GestorRecodeExceptions.h>
#include <Utils/CacheException.h>
//#include <Utils/CacheManagerException.h>
#include <Utils/FileException.h>
#include <Utils/RedirectCallException.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <RecordingModule/RecordingFileException.h>
#include <RecordingModule/RecordingModuleException.h>
#include <RecordingModule/RecordingModuleSessionAlreadyStablishedException.h>
#include <RecordingModule/RecordingModuleSessionNotStablishedException.h>
#include <RecordingModule/MaxActiveRecordingsReachedException.h>
#include <StreamingModule/SMSessionAlreadyStablishedException.h>
#include <AlarmModule/AlarmModuleException.h>
#include <Exceptions/CameraMalfunctionException.h>
#include <Exceptions/CameraNotConnectableException.h>
#include <Exceptions/DeviceNotFoundException.h>
#include <Exceptions/FileNotOpenException.h>
#include <Exceptions/InconsistentStateException.h>
#include <Exceptions/InvalidChannelException.h>
#include <Exceptions/InvalidParameterException.h>
#include <Exceptions/MetadataModuleSessionNotStablishedException.h>
#include <Exceptions/MetadataModuleSessionAlreadyStablishedException.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Exceptions/UnknownClassException.h>
#include <Exceptions/IdAlreadyInUseException.h>
#include <Exceptions/IdNotFoundException.h>
#include <Exceptions/UnknownSerializedException.h>
#include <Exceptions/PMSessionAlreadyStablishedException.h>
#include <Exceptions/PMSessionNotStablishedException.h>
#include <Exceptions/PMNoServicePermissionException.h>
#include <Module/CancelledCallException.h>
#include <Endian/Endian.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <string>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>

using namespace std;

SerializedException::SerializedException(dword nB, byte *b, bool copy)
{

	this->nBytes=nB;
	this->copied=copy;
	
	if (copy)
	{
		this->bytesPtr=new byte[this->nBytes];
		if (this->bytesPtr==NULL)
		{
			this->copied=false;
			this->bytesPtr=b;
			throw (Exception(string("Not enough memory to copy, just referenced")));
		}
		
		memmove(this->bytesPtr, b, nB);
	}
	else
		this->bytesPtr=b;
}


SerializedException::SerializedException(SerializedException &se)
{
//	cout<<"SerializedException::SerializedException(&se)"<<endl;
	this->nBytes=se.nBytes;

	// Quan fem una copia, fem una copia, "recoi" ;)
	this->copied=true;
	this->bytesPtr=new byte[nBytes];
	if (this->bytesPtr==NULL)
	{
		this->copied=false;
		this->bytesPtr=se.bytesPtr;
		throw (Exception(string("Not enough memory to copy, just referenced")));
	}
	memmove(this->bytesPtr, se.bytesPtr, this->nBytes);
}

SerializedException::~SerializedException()
{
	if (this->copied)
		delete [] this->bytesPtr;
}

byte *SerializedException::bytes()
{
	return bytesPtr;
}

dword SerializedException::size()
{
	return nBytes;
}

string SerializedException::getClass()
{
	Endian e;
	
	int *len=(int*)bytesPtr;
	char *name=(char*)(len+1);
	
	int lenEndian=*len;
	
	e.desendiana(&lenEndian, sizeof(int));
	
//	cout << "SE::getClass: " << lenEndian << " " << name << endl; 

	string c("");
	if(lenEndian<=nBytes)
		c=string(name, lenEndian);
	else
	{
		c=string(name, nBytes);
//		cout<<"SerializedException::getClass: error: Class size > numBytes :"<<endl;
//		cout<<"                               "<<lenEndian<<" > "<<nBytes<<"  =  "<<endl;
//		cout<<StrUtils::hexDump(c)<<endl;
	}
	return c;
}

int SerializedException::getCode()
{
	// Copiat de  --- Exception::Exception(SerializedException &se)
	Endian e;

	byte *bytes=bytesPtr;
	dword nB=size();
	
	if (nB<sizeof(int)*3) // Minim els 3 ints (codi + 2 de longituds)
	{
		return -1;
	}
	
	int *typeLen=(int*)bytes;
	e.desendiana(typeLen, sizeof(int));

	if (*typeLen+sizeof(int)*3>nB)
	{
		return -1;
	}
	
	char *str=(char*)(typeLen+1);
	
	int *codi=(int*)(str+*typeLen);

	e.desendiana(codi, sizeof(int));
	
	return (*codi);
}

string SerializedException::getMsg()
{
	// Copiat de  --- Exception::Exception(SerializedException &se)
	Endian e;

	byte *bytes=bytesPtr;
	dword nB=size();
	
	if (nB<sizeof(int)*3) // Minim els 3 ints (codi + 2 de longituds)
	{
		 return string("");
	}
	
	int *typeLen=(int*)bytes;
	e.desendiana(typeLen, sizeof(int));

	if (*typeLen+sizeof(int)*3>nB)
	{
		 return string("");
	}
	
	char *str=(char*)(typeLen+1);
	
	int *codi=(int*)(str+*typeLen);
	int *msgLen=(int*)(codi+1);

	if((((char*)msgLen)-((char*)bytes))>nB) //estém fora del buffer
		return string("");

	e.desendiana(codi, sizeof(int));
	e.desendiana(msgLen, sizeof(int));
	
	if (*msgLen+*typeLen+sizeof(int)*3>nB)
	{
		 return string("");
	}
	
	str=(char*)(msgLen+1);
	string msg(str, *msgLen);
	
	return msg;
//	cout<<" New Exception from SE:"<<code<<":"<<msg<<endl;
}

void SerializedException::materializeAndThrow(bool deleteThis)
{
	// TODO: delete this i fer els getClass sobre se?
	SerializedException se=(*this);
	
	try
	{
		if (se.getClass()==string("Exception"))
			throw (Exception(se));
		else if (se.getClass()==string("IPException"))
			throw (IPException(se));
		else if (se.getClass()==string("SocketException"))
			throw (SocketException(se));
		else if (se.getClass()==string("SocketTimeoutException"))
			throw (SocketTimeoutException(se));
		else if (se.getClass()==string("RPCException"))
			throw (RPCException(se));
		else if (se.getClass()==string("ServiceException"))
			throw (ServiceException(se));
		else if (se.getClass()==string("GestorException"))
			throw (GestorException(se));
		else if (se.getClass()==string("ControlPluginException"))
			throw (ControlPluginException(se));
		else if (se.getClass()==string("CPInvalidParamException"))
			throw (CPInvalidParamException(se));
		else if (se.getClass()==string("CPSessionAlreadyStablishedException"))
			throw (CPSessionAlreadyStablishedException(se));
		else if (se.getClass()==string("CPSessionNotStablishedException"))
			throw (CPSessionNotStablishedException(se));
		else if (se.getClass()==string("ImagePluginException"))
			throw (ImagePluginException(se));
		else if (se.getClass()==string("IPInvalidParamException"))
			throw (IPInvalidParamException(se));
		else if (se.getClass()==string("IPSessionAlreadyStablishedException"))
			throw (IPSessionAlreadyStablishedException(se));
		else if (se.getClass()==string("IPSessionNotStablishedException"))
			throw (IPSessionNotStablishedException(se));
		else if (se.getClass()==string("CacheException"))
			throw (CacheException(se));
		//	else if (se.getClass()==string("CacheManagerException"))
		//		throw (CacheManagerException(se));
		else if (se.getClass()==string("FileException"))
			throw (FileException(se));
		else if (se.getClass()==string("RecordingModuleException"))
			throw (RecordingModuleException(se));
		else if (se.getClass()==string("RecordingModuleSessionNotStablishedException"))
			throw (RecordingModuleSessionNotStablishedException(se));
		else if (se.getClass()==string("RecordingModuleSessionAlreadyStablishedException"))
			throw (RecordingModuleSessionAlreadyStablishedException(se));
		else if (se.getClass()==string("MaxActiveRecordingsReachedException"))
			throw (MaxActiveRecordingsReachedException(se));
		else if (se.getClass()==string("RecordingFileException"))
			throw (RecordingFileException(se));
		else if (se.getClass()==string("RecordingFileChunkException"))
			throw (RecordingFileChunkException(se));
		else if (se.getClass()==string("RecodePluginException"))
			throw (RecodePluginException(se));
		else if (se.getClass()==string("RPInvalidParamException"))
			throw (RPInvalidParamException(se));
		else if (se.getClass()==string("RPSessionAlreadyStablishedException"))
			throw (RPSessionAlreadyStablishedException(se));
		else if (se.getClass()==string("RPSessionNotStablishedException"))
			throw (RPSessionNotStablishedException(se));
		else if (se.getClass()==string("SMSessionAlreadyStablishedException"))
			throw (SMSessionAlreadyStablishedException(se));
		else if (se.getClass()==string("DeviceNotFoundException"))
			throw (DeviceNotFoundException(se));
		else if (se.getClass()==string("FileNotOpenException"))
			throw (FileNotOpenException(se));
		else if (se.getClass()==string("InconsistentStateException"))
			throw (InconsistentStateException(se));
		else if (se.getClass()==string("InvalidChannelException"))
			throw (InvalidChannelException(se));
		else if (se.getClass()==string("MetadataModuleSessionAlreadyStablishedException"))
			throw (MetadataModuleSessionAlreadyStablishedException(se));
		else if (se.getClass()==string("MetadataModuleSessionNotStablishedException"))
			throw (MetadataModuleSessionNotStablishedException(se));
		else if (se.getClass()==string("AlarmModuleException"))
			throw (AlarmModuleException(se));
		else if (se.getClass()==string("AMInvalidParamException"))
			throw (AMInvalidParamException(se));
		else if (se.getClass()==string("AMSessionAlreadyStablishedException"))
			throw (AMSessionAlreadyStablishedException(se));
		else if (se.getClass()==string("AMSessionNotStablishedException"))
			throw (AMSessionNotStablishedException(se));
		else if (se.getClass()==string("UnknownClassException"))
			throw (UnknownClassException(se));
		else if (se.getClass()==string("NotEnoughMemoryException"))
			throw (NotEnoughMemoryException(se));
		else if (se.getClass()==string("CameraMalfunctionException"))
			throw (CameraMalfunctionException(se));
		else if (se.getClass()==string("CameraNotConnectableException"))
			throw (CameraNotConnectableException(se));
		else if (se.getClass()==string("RedirectCallException"))
			throw (RedirectCallException(se));
		else if (se.getClass()==string("IdNotFoundException"))
			throw (IdNotFoundException(se));
		else if (se.getClass()==string("IdAlreadyInUseException"))
			throw (IdAlreadyInUseException(se));
		else if (se.getClass()==string("CancelledCallException"))
			throw (CancelledCallException(se));
		else if (se.getClass()==string("InvalidParameterException"))
			throw (InvalidParameterException(se));
		else if (se.getClass()==string("PMSessionAlreadyStablishedException"))
			throw (PMSessionAlreadyStablishedException(se));
		else if (se.getClass()==string("PMSessionNotStablishedException"))
			throw (PMSessionNotStablishedException(se));
		else if (se.getClass()==string("PMNoServicePermissionException"))
			throw (PMNoServicePermissionException(se));

		string unknownExcMsg=string("Unknown SerializedException (") +
			se.getClass() + string("): ") + se.getMsg();

		throw UnknownSerializedException(unknownExcMsg);

	}
	catch(Exception &e)
	{
		if (deleteThis)
			delete this;
		throw;
	}
}

