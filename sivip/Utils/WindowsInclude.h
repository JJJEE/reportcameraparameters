//#pragma once
#ifndef __SIRIUS_WINDOWSINCLUDE_H
#define __SIRIUS_WINDOWSINCLUDE_H

//#pragma message("WinInc")
#ifdef WIN32

typedef unsigned int u_int;


//#pragma message("WinInc SecAtl")
#ifndef _SECURE_ATL
#define _SECURE_ATL 1
#endif

//#pragma message("WinInc VC_ext")
#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Excluir material rara vez utilizado de encabezados de Windows
#endif

//#pragma message("WinInc winver")
#ifndef WINVER				// Permitir el uso de características específicas de Windows XP o posterior.
#define WINVER 0x0501		// Cambiar para establecer el valor apropiado para otras versiones de Windows.
#endif

//#pragma message("WinInc winnt")
#ifndef _WIN32_WINNT		// Permitir el uso de características específicas de Windows XP o posterior.                   
#define _WIN32_WINNT 0x0501	// Cambiar para establecer el valor apropiado para otras versiones de Windows.
#endif						

//#pragma message("WinInc win")
#ifndef _WIN32_WINDOWS		// Permitir el uso de características específicas de Windows 98 o posterior.
#define _WIN32_WINDOWS 0x0410 // Cambiar para establecer el valor apropiado para Windows Me o posterior.
#endif

//#pragma message("WinInc ie")
#ifndef _WIN32_IE			// Permitir el uso de características específicas de IE 6.0 o posterior.
#define _WIN32_IE 0x0600	// Cambiar para establecer el valor apropiado para otras versiones de IE.
#endif


//#pragma message("WinInc ATL_EXP_CN")
#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// Algunos constructores CString serán explícitos

// Desactiva la ocultación de MFC para algunos mensajes de advertencia comunes y, muchas veces, omitidos de forma consciente
//#pragma message("WinInc AFX_ALL")
#define _AFX_ALL_WARNINGS

//#include <afxwin.h>         // Componentes principales y estándar de MFC
//#include <afxext.h>         // Extensiones de MFC
//#pragma message("WinInc afx.h")
#include <afx.h>
#include <ws2tcpip.h>


//#include <afxdisp.h>        // Clases de automatización de MFC


//#ifndef _AFX_NO_OLE_SUPPORT
//#include <afxdtctl.h>		// Compatibilidad MFC para controles comunes de Internet Explorer 4
//#endif
//#ifndef _AFX_NO_AFXCMN_SUPPORT
//#include <afxcmn.h>			// Compatibilidad MFC para controles comunes de Windows
//#endif // _AFX_NO_AFXCMN_SUPPORT
//#pragma message("WinInc afxsock.h")
//#include <afxsock.h>
#include <WinSock2.h>
//#include <foo>
	

//#pragma message("WinInc endif1")
#endif

//#pragma message("WinInc endif2")
#endif
