#ifndef __SIRIUS__BASE__UTILS__RPCEXCEPTION_H
#define __SIRIUS__BASE__UTILS__RPCEXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class RPCException : public Exception
{
 public:
	RPCException(string msg, int code=0);
	RPCException(int code, string msg);
	RPCException(SerializedException &se);

	~RPCException();
	
	virtual string getClass();
};

#endif

