/*
 *  FileException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Utils/FileException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

FileException::FileException(string msg, int code): Exception(msg, code)
{

}

FileException::FileException(int code, string msg): Exception(msg, code)
{

}

FileException::FileException(SerializedException &se): Exception(se)
{

}

FileException::~FileException()
{
   
}

string FileException::getClass()
{
	string c=string("FileException");
	return c;
}

