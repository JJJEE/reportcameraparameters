/*
 *  PtrPool.h
 *  
 *
 *  Created by David Marí Larrosa on 27/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__UTILS__PTRPOOL_H
#define __SIRIUS__BASE__UTILS__PTRPOOL_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Threads/Thread.h>
#include <map>

using namespace std;

class PtrPool
{
	class poolInfo
	{
		public:
			bool occupied;
	};
	
	Mutex poolLock;
	map<void*, poolInfo> pool;
	
public:
	PtrPool(string name=string(""));
	~PtrPool();
	
	void add(void *ptr, bool occupied=false);
	void remove(void *ptr);

	void* get();
	void release(void *ptr);
	
	dword size();
	int numFree();
};
#endif
