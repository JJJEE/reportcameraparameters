/*
 *  RecordingFileBlockCache.h
 *  
 *
 *  Created by David Marí Larrosa on 26/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__UTILS__CACHE_H
#define __SIRIUS__BASE__UTILS__CACHE_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Utils/Timer.h>
#include <Threads/Mutex.h>

typedef void (*CacheLRUCallback)(void*, void*);

class Cache
{
public:
	static const dword defaultSize=25;
	// Tamany maxim permes resultant d'un merge en bytes
	static const fileOffset mergeMaxSize=1048576;
	
	enum operation {read=0, write};
	
	struct block
	{
		Cache::operation operation;
		fileOffset offset;
		Timer *lastUse;
		fileOffset size;
		void *data;
		
		block() : offset(0), lastUse(NULL), size(0), data(NULL) {};
	};
	
protected:
	virtual block* getFreeBlock();  // data==NULL
	virtual block* searchBlock(fileOffset off, dword size);
	virtual void optimize(); 
	
	CacheLRUCallback lruCallback;
	
	Mutex blocksLock;
	dword nBlocks;
	block *blocks;
	
	qword nHits, nMisses;
	dword nextOptimize;
	
public:
	Cache(CacheLRUCallback callback, dword nBlocks=Cache::defaultSize);
	~Cache();
	
	virtual void add(void *data, dword size, fileOffset off, Cache::operation operation=Cache::read);
	virtual void* get(fileOffset off, dword size);
	virtual void empty();
	virtual void flush();
	
	virtual qword getHits();
	virtual qword getMisses();
	virtual float getHitRatio();
};
#endif

