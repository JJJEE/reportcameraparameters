/*
 *  CachedFile.cpp
 *  
 *
 *  Created by David Marí Larrosa on 11/03/08.
 *  Copyright 2008 Marina Eye-Cam Technologies. All rights reserved.
 *
 */

#include <Utils/CachedFile.h>
//#include <Utils/CacheManager>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <Exceptions/IdNotFoundException.h>
#include <Exceptions/IdAlreadyInUseException.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <iostream>
#include <map>
#include <list>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

map<Cache*,CachedFile*> cfFromCache;
map<Cache*,Mutex*> olFromCache;

Mutex CachedFile::constructorMutex;

void CachedFile::cacheFlushWrite(Cache *c, Cache::block *bl)
{
	CachedFile *cf=cfFromCache[c];
	if (cf==NULL)
	{
		//cout << "cfFromCache[c] is NULL!!!!" << endl;
		return;
	}
	
	try
	{
		//cout << "[FLUSH-" << bl->size << "] " << cf->getFileName() << endl;
		cf->diskOperationLock();
		fileOffset off=cf->tell();
		cf->uncachedSeek(bl->offset, File::SEEK_FROMSTART);
		cf->uncachedwrite(bl->data, bl->size);
		cf->uncachedSeek(off, File::SEEK_FROMSTART);
		cf->diskOperationUnlock();
	}
	catch (Exception &e)
	{
		cf->diskOperationUnlock();
		cout << "FATAL in CachedFile::cacheFlushWrite: " << e.getClass() << ": " << e.getMsg() << endl;
		abort();
	}
}

int CachedFile::uncachedwrite(void *data, dword size)
{
	cout<<" CachedFile::uncachedwrite :: writing:"<<size<<" bytes at :"<< cacheOff<<StrUtils::hexDump(string((char*) data, size));
	return File::write(data, size);
}


CachedFile::CachedFile(const string filename, const bool readOnly, const bool newFileRW) : 
File(filename, (readOnly?File::readAccessMode:File::readAccessMode|File::writeAccessMode), newFileRW),
	nHits(0), nMisses(0), cacheOff(0)
{
}

CachedFile::~CachedFile()
{
	diskOperationLock();

	map<int,block*>::iterator it;
	for(it=cache.begin(); it != cache.end(); ++it)
	{
		block *b=it->second;
	cout<<"freeBlock :: delete & write: "<<filename<<" segment:"<<it->first<<" type:"<<b->operation<<endl;
	cout<<"                       data: "<<b->data <<" size:"<<b->size<<" offset:"<<b->offset<<endl;
		if(b->operation==OP_WRITE)
		{
			try
			{
				this->diskOperationLock();
				File::seek(b->offset);
				File::write(b->data, b->size);
				this->diskOperationUnlock();
			}
			catch (Exception &e)
			{
				this->diskOperationUnlock();
				cout << "FATAL in CachedFile::~CachedFile: " << e.getClass() << ": " << e.getMsg() << endl;
			}
		}
		delete[] b->data;
		delete b;
	}

	diskOperationUnlock();
}

void CachedFile::freeBlock()
{
	//cout<<"CachedFile::freeBlock"<<endl;
	map<int, block*>::iterator iter;
	double maxSecs=0.0;
	int bIdx=-1;

	for( iter = cache.begin(); iter != cache.end(); ++iter )
	{
		iter->second->lastUse.pause();
	}

	for( iter = cache.begin(); iter != cache.end(); ++iter )
	{
		block *b = iter->second;
		TimerInstant blockTI = b->lastUse.time();
		double blockSecs = blockTI.seconds();
		
		if (blockSecs>maxSecs)
		{
			bIdx=iter->first;
			maxSecs=blockSecs;
		}
	}
	
	for( iter = cache.begin(); iter != cache.end(); ++iter )
	{
		iter->second->lastUse.resume();
	}

	if(bIdx!=-1)
	{
		iter=cache.find(bIdx);
		block *b=iter->second;
	//cout<<"freeBlock :: delete & write: "<<filename<<" segment:"<<bIdx<<" type:"<<b->operation<<endl;
	//cout<<"                       data: "<<b->data <<" size:"<<b->size<<" offset:"<<b->offset<<endl;
		cache.erase(iter);

		if(b->operation==OP_WRITE)
		{
			try
			{
				this->diskOperationLock();
				File::seek(b->offset);
				File::write(b->data, b->size);
				this->diskOperationUnlock();
			}
			catch (Exception &e)
			{
				this->diskOperationUnlock();
				cout << "FATAL in CachedFile::freeBlock: " << e.getClass() << ": " << e.getMsg() << endl;
				abort();
			}
		}
		delete[] b->data;
		b->data=NULL;
		delete b;
	}
}

int CachedFile::read(void *in, dword size)
{
	//cout<<"CachedFile::read:"<<size<<" off:"<<cacheOff<<endl;
	byte *data=(byte*)in;

	this->diskOperationLock();

	int firstBlock=cacheOff/blockSize;
	int currentBlock=firstBlock;
	int readSize=0;

	while((currentBlock*blockSize) < (cacheOff+size)) // while((inici block) < (final del segment))
	{
		map<int, block*>::iterator it= cache.find(currentBlock);
		if(it!=cache.end()) // block a la cache...
		{
			//cout<<"CachedFile::read:a la cache"<<endl;
			block *b=it->second;
			int remaining=size-readSize;
			int blockBegin=(cacheOff+readSize)%blockSize;
			

			if((blockBegin + remaining) <= blockSize) //final del tros llegit dins el block //(escriptura dins block) + (bytes pendents d'escriure) <=tamany block
			{
				if( (blockBegin + remaining) > b->size)
				{
					//cout<<"CachedFile::read: ampliem block:"<<blockBegin+remaining<<endl;
					byte *newData=new byte[blockBegin+remaining];
			//		//cout<<"CachedFile::read: memmove:"<<b->size<<endl;
					memmove(newData, b->data, b->size);	
			//		//cout<<"CachedFile::read: seek:"<<currentBlock*blockSize + b->size<<endl;
					File::seek(currentBlock*blockSize + b->size);
			//		//cout<<"CachedFile::read: read:"<<(blockBegin + remaining) - b->size<<endl;
					int r=File::read(newData+b->size, (blockBegin + remaining) - b->size);
			//		//cout<<"CachedFile::read: - read:"<<r<<endl;
					delete[] b->data;
					b->data=newData;
					b->size=blockBegin+remaining;
			//		//cout<<"CachedFile::read: - b size:"<<(void*)b->data<<":"<<b->size<<endl;
				}
				//cout<<"CachedFile::read: hit, end read - bgn:"<<blockBegin<<" remain:"<<remaining<<" blk->sz:"<<b->size<<" readSize:"<<readSize<<" requested:"<<size<<endl;
				memmove(data+readSize, b->data+blockBegin, remaining);
				readSize+=remaining;
			}
			else
			{
				if(b->size<blockSize) //serà un block complert
				{
					//cout<<"CachedFile::read: llegint block complert -  acabem de llegir block:"<<blockBegin+remaining<<endl;
					byte *newData=new byte[blockSize];
					memmove(newData, b->data, b->size);	
					File::seek(currentBlock*blockSize + b->size);
					File::read(newData+b->size, blockSize - b->size);
					if(b->data!=NULL)
						delete[] b->data;
					b->data=newData;
					b->size=blockSize;
				}
				//cout<<"CachedFile::read: llegint block complert:- bgn:"<<blockBegin<<" remain:"<<remaining<<" blk->sz:"<<b->size<<" readSize:"<<readSize<<" requested:"<<size<<endl;
				memmove(data+readSize, b->data+blockBegin, blockSize-blockBegin);
				readSize+=blockSize-blockBegin; 
			}
			nHits++;
			b->lastUse.stop();
			b->lastUse.start();
		//	//cout<<"CachedFile::read - Oper"<<endl;
		//	b->operation=OP_READ;
		}
		else
		{
			int remaining=size-readSize;
			int blockBegin=(cacheOff+readSize)%blockSize;

			nMisses++;
			if(cache.size()>cacheNumBlocks)
				freeBlock();
			block *n=new block();
			n->offset=currentBlock*blockSize;
			n->operation=OP_READ;

	//		if(blockBegin+remaining < blockSize)
	//			n->size=blockBegin+remaining;//size-readSize;
	//		else
				n->size=blockSize;

			//cout<<" CachedFile::read: miss . new block(2):"<<n->offset<<" sz:"<<n->size<<" blkBeg:"<<blockBegin<<"remain:"<<remaining<<" req:"<<size<<endl;
			n->data=new byte[n->size];
			File::seek(n->offset);
			n->size=File::read(n->data, n->size);

			if(blockBegin+remaining < n->size)
			{
				memmove(data+readSize, n->data+blockBegin, remaining);
				readSize+=remaining;
			}else
			{	memmove(data+readSize, n->data+blockBegin, n->size-blockBegin);
				readSize+=n->size-blockBegin;
			}
			n->lastUse.start();

			cache.insert(make_pair(currentBlock, n));

		}
		currentBlock++;
	}

	cacheOff+=readSize;
	this->diskOperationUnlock();
	//cout<<"CachedFile::read - return "<<readSize<<endl;
	return readSize;
}

int CachedFile::write(void *in, dword size)
{
	//cout<<"CachedFile::write"<<endl;
	byte *data=(byte*)in;


	int n=-1;
	this->diskOperationLock();

	int firstBlock=cacheOff/blockSize;
	int currentBlock=firstBlock;
	int writeSize=0;

	//cout<<"CachedFile :: write offset:"<<cacheOff<<" first block:"<<firstBlock<<" size:"<<size<<" file:"<<filename<<endl;

	while((currentBlock*blockSize) < (cacheOff+size)) // while((inici block) < (final del segment))
	{
		//cout<<"loooooop: first:"<<firstBlock<<" curr:"<<currentBlock<<" write:"<<writeSize<<" requested:"<<size<<endl;
		map<int, block*>::iterator it= cache.find(currentBlock);
		if(it!=cache.end()) // block a la cache...
		{
			block *b=it->second;
			int remaining=size-writeSize;
			int blockBegin=(cacheOff+writeSize)%blockSize;
			
			if((blockBegin + remaining) <= blockSize) //final del tros llegit dins el block //(escriptura dins block) + (bytes pendents d'escriure) <=tamany block
			{
				if( ((blockBegin + remaining)) > b->size)
				{
					byte *newData=new byte[(blockBegin+remaining)];
					if(((cacheOff+writeSize)%blockSize) >0) // escrivim passat inici block
					{
						memmove(newData, b->data, b->size);	// copiem
					}
					if(b->data!=NULL)
						delete[] b->data;
					b->data=newData;
					b->size=((blockBegin) + (remaining));
				}
				//cout<<"CachedFile::write hit: write end :"<<b->offset<<" blockBgn:"<<blockBegin<<"  remn:"<<remaining<<"  wsize"<<writeSize<<endl;
				memmove(b->data + (blockBegin) , data+writeSize , remaining);
				writeSize += remaining;
			}
			else
			{
				if(b->size<blockSize) //serà un block complert
				{
					byte *newData=new byte[blockSize];

					if(blockBegin >0)
					{
						memmove(newData, b->data, b->size);	
					}
					if(b->data!=NULL)
						delete[] b->data;
					b->data=newData;
					b->size=blockSize;
				}
				//cout<<"CachedFile::write hit: write to end block :"<<b->offset<<" blockBgn:"<<blockBegin<<"  remn:"<<remaining<<"  wsize"<<writeSize<<" writing:"<<blockSize - blockBegin<<endl;
				memmove(b->data + blockBegin , data+writeSize , blockSize - blockBegin);
				writeSize += blockSize - blockBegin;//(size-writeSize);
			}
			nHits++;
			b->lastUse.stop();
			b->lastUse.start();
			b->operation=OP_WRITE;
		}
		else
		{
			int remaining=size-writeSize;
			int blockBegin=(cacheOff+writeSize)%blockSize;

			nMisses++;
			if(cache.size()>cacheNumBlocks)
				freeBlock();
		block *n=new block();
		n->offset=currentBlock*blockSize;
		n->operation=OP_WRITE;
	
//			if(cacheOff + size > n->offset+blockSize )
//				n->size=blockSize;
//			else
//				n->size=(cacheOff + size) % blockSize;
		int fSize=File::tell();
//			if(blockBegin+remaining < blockSize)
//				n->size=blockBegin+remaining;//size-readSize;
//			else
		int rEnd=0;
		if(blockBegin+remaining>blockSize)
		{
			n->size=blockSize;
		}
		else if(fSize > n->offset+blockBegin+remaining)
		{
			n->size=fSize-n->offset;
		}
		else
		{
			n->size=blockBegin+remaining;
		}

		//cout<<"CachedFile::write  new block(4):"<<n->offset<<" sz:"<<n->size<<" op:"<<n->operation<<endl<<" remaining: size:"<<size<<" - wsize:"<<writeSize<<" = "<<remaining<<endl;
			n->data=new byte[n->size];
			if((blockBegin)>0 || n->size > blockBegin+remaining)
			{
				File::seek(n->offset);
				//cout<<"CachedFile::write nb(4) read:"<<n->offset<<"->"<<blockBegin<<endl;
				File::read(n->data, n->size);//blockBegin);
			}

			if(blockBegin+remaining < n->size)//blockSize)
			{
				//cout<<"CachedFile::write all remaining nb(4) mmove:data+"<<blockBegin<<",data+"<<writeSize<<", remn:"<<remaining<<endl;
				memmove(n->data + blockBegin, data+writeSize, remaining);
				writeSize+=remaining;//n->size;
			}else
			{
				//cout<<"CachedFile::write to end block nb(4) mmove:data+"<<blockBegin<<",data+"<<writeSize<<", sz:"<<n->size<<"-"<<blockBegin<<endl;
				memmove(n->data + blockBegin, data+writeSize, n->size - blockBegin);
				writeSize+=n->size-blockBegin;
			}
			n->lastUse.start();

			cache.insert(make_pair(currentBlock, n));
		}
		currentBlock++;
	}

	cacheOff+=writeSize;
	this->diskOperationUnlock();
	return writeSize;
}

// En alguns casos el compilador no detectara be el tipus, aixi que cal tenir preparat un int (4 bytes) ;)
void CachedFile::seek(int off, int from)
{
	//cout<<"CachedFile::seek(int)"<<endl;
	CachedFile::seek((fileOffset)off, from);
}
	
void CachedFile::seek(fileOffset off, int from)
{
	//cout<<"CachedFile::seek(off) : "<<off<<endl;
	cacheOff=off;
//	File::seek(off, from);
}

void CachedFile::uncachedSeek(int off, int from)
{
	//cout<<"CachedFile::seek(int)"<<endl;
	CachedFile::uncachedSeek((fileOffset)off, from);
}
	
void CachedFile::uncachedSeek(fileOffset off, int from)
{
	//cout<<"CachedFile::seek(off) : "<<off<<endl;
	File::seek(off, from);
}

qword CachedFile::getHits()
{
	return nHits;
}

qword CachedFile::getMisses()
{
	return nMisses;
}

float CachedFile::getHitRatio()
{
	if (nHits+nMisses==0)
		return 1.0f;
		
	return ((float)nHits)/((float)(nHits+nMisses));
}

