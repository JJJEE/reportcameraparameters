/*
 *  File.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */
#include <Utils/WindowsInclude.h>
//#include <afxwin.h>         // Componentes principales y estándar de MFC
#include <Utils/File.h>
#include <iostream>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

#ifdef WIN32
string File::dirSep("\\");
#else
string File::dirSep("/");
#endif


int File::accesModeToOpenMode(const dword mode, const bool newFileRW)
{
	int flags=0;
	
#ifdef WIN32
/*	if (mode&(File::readAccessMode|File::writeAccessMode))
		flags|=O_RDWR;
	else*/ 
	if (mode&File::readAccessMode)
		flags|=GENERIC_READ;
	if (mode&File::writeAccessMode)
		flags|=GENERIC_WRITE;

//	if (mode&File::appendAccessMode)
//		flags|=O_APPEND;
#else
	if (mode&(File::readAccessMode|File::writeAccessMode))
		flags|=O_RDWR;
	else if (mode&File::readAccessMode)
		flags|=O_RDONLY;
	else if (mode&File::writeAccessMode)
		flags|=O_WRONLY;

	if (mode&File::appendAccessMode)
		flags|=O_APPEND;
		
	if (newFileRW)
		flags|=O_CREAT|O_TRUNC;
#endif		
	return flags;
}


File::File(const string filename, const dword mode, const bool newFileRW, const bool errorOut) : f(NULL_HANDLE), filename(filename), accessMode(mode), 
	currentOff(0), opLock(true)
{
	// cout << "--> File::File" << endl;
#ifdef WIN32
	this->f=::CreateFile(StringToLPCWSTR(this->filename),
							File::accesModeToOpenMode(this->accessMode, newFileRW),
							NULL,
							NULL,
							(newFileRW)?CREATE_ALWAYS:NULL,
							NULL,
							NULL);
#else
	this->f=::open(this->filename.c_str(), File::accesModeToOpenMode(this->accessMode, newFileRW), 0640);
//	cout << "File open: " << this->f <<" Access:"<<this->accessMode<<" read 1, write 2, appnd 3"<< endl;
#endif	
	if (this->f==NULL_HANDLE)
	{
		if(errorOut)
		{
			cout<<"throw(FileException(1, \"File could not be opened/created\")): "
				<< filename << " mode: " << this->accessMode << " rw: " 
				<< newFileRW << " AM2OM: " << 
				File::accesModeToOpenMode(this->accessMode, newFileRW) 
				<< " errno: " << errno << endl;
		}
		throw(FileException(1, string("File ") + filename +
			string(" could not be opened/created")));
	}
	
	// cout << "<-- File::File" << endl;
}

File::~File()
{
	if (this->f!=NULL_HANDLE)
	{
#ifdef WIN32
		::CloseHandle(this->f);
#else
//		cout << "~File close " << this->f << endl;
		::close(this->f);
//		cout << "  ~File closed " << this->f << endl;
#endif
	}
	this->f=NULL_HANDLE;
}

int File::read(void *data, dword size)
{
	//cout<<"File::read:"<<(void*)data<<","<<size<<endl;
	diskOperationLock();
#ifdef WIN32
	DWORD n;
#else
	int n=0;
#endif
	try
	{
#ifdef WIN32
		::ReadFile(this->f, data, size,&n,NULL);
#else
		n=::read(this->f, data, size);
#endif
		//cout<<"-File read:"<<n<<endl;
		if (n>0)
			currentOff+=n;
	}
	catch (...)
	{
		diskOperationUnlock();
		throw;
	}
	diskOperationUnlock();
	return n;
}

int File::write(const void *data, dword size)
{
//	cout<<"File::write data:"<<(void*)data<<" size:"<<size<<endl;
	diskOperationLock();
#ifdef WIN32
	DWORD n;
#else
	int n=0;
#endif
	try
	{
#ifdef WIN32
		::WriteFile(this->f, data, size,&n,NULL);
#else
		n=::write(this->f, data, size);
#endif
	}
	catch (...)
	{
		diskOperationUnlock();
		throw;
	}
	if (n>0)
		currentOff+=n;
	else if(n<0)
		cout<<"error writing to file fd:"<<this->f<<", errno "<<errno<<endl;
	diskOperationUnlock();
	return n;
}

fileOffset File::tell()
{
	//cout<<"File::tell"<<endl;
	diskOperationLock();
	try
	{
#ifdef WIN32
		currentOff=::SetFilePointer(this->f, 0, 0, FILE_CURRENT);
#else
		currentOff=::lseek(this->f, 0, SEEK_CUR);
#endif
	}
	catch (...)
	{
		diskOperationUnlock();
		throw;
	}
	diskOperationUnlock();
	return currentOff;
}

fileOffset File::currentOffset()
{
	//cout<<"File::Offset"<<endl;
	return currentOff;
}

// En alguns casos el compilador no detectara be el tipus, aixi que cal tenir preparat un int (4 bytes) ;)
void File::seek(int off, int from)
{
	//cout<<"File::seek(int)"<<endl;
//	diskOperationLock();
	seek((fileOffset)off, from);
//	diskOperationUnlock();
}
	
void File::seek(fileOffset off, int from)
{
	//cout<<"File::seek(off)"<<endl;
	diskOperationLock();
	try
	{
#ifdef WIN32
		currentOff=::SetFilePointer(this->f, off, 0, from);
		if (currentOff==INVALID_SET_FILE_POINTER)
#else
		currentOff=::lseek(this->f, off, from);
//		cout << "File::seek fd:"<<this->f<<" off: " << off << ", curr: " << currentOff << endl;
		if (currentOff==-1)
#endif
		{
			cout << "File::seek error:"<<errno << endl;
			throw FileException(0, "seek error");
		}
	}
	catch (...)
	{
		diskOperationUnlock();
		throw;
	}
	diskOperationUnlock();
}

fileOffset File::size()
{
	//cout << "File::size()" << endl;
	diskOperationLock();
	fileOffset s;
	try
	{
		fileOffset pos=tell();
		seek(0, File::SEEK_FROMEND);
		s=tell();
		seek(pos, File::SEEK_FROMSTART);
	}
	catch (...)
	{
		diskOperationUnlock();
		throw;
	}
	diskOperationUnlock();
	return s;	
}

string File::getFileName()
{
	return this->filename;
}

void File::diskOperationLock()
{
	opLock.lock();
}

void File::diskOperationUnlock()
{
	opLock.unlock();
}
