#ifndef __SIRIUS__BASE__UTILS__DBConnection_H
#define __SIRIUS__BASE__UTILS__DBConnection_H

#include <Utils/DBGateway.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>

class DBConnection: public DBGateway
{
	public:	
	DBConnection(Address origen, short type, Canis *cn);
	virtual ~DBConnection();
	const char* query(string query);
	XML* queryXML(string query);
	void command(string order);

	protected:
	const char* ask(string s, RPC *dbRPC, int callId=0, Address *keepAliveAddr=NULL);

};

#endif
