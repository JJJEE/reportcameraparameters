#ifndef __SIRIUS__BASE__UTILS__RPCPACKET_H
#define __SIRIUS__BASE__UTILS__RPCPACKET_H

#include <Utils/debugNew.h>
#include <Sockets/Address.h>
#include <Utils/Types.h>
#include <Endian/Endian.h>
#include <Exceptions/NotEnoughMemoryException.h>

class RPCPacket
{
	bool copied;
	bool deletable;

	public:
		static const word responsePacketId=(word)-1;
		static const word exceptionPacketId=(word)-2;
		static const word keepAlivePacketId=(word)-3;
		static const word segmentedPacketId=(word)-4;
		static const word redirectionPacketId=(word)-5;
	
		class segmentationInfo
		{
		public:
			dword offset;
			dword nPackets;
			dword totalSize;
			
			segmentationInfo() : offset(0), nPackets(0), totalSize(0) {};
			segmentationInfo(void *p)
			{
				this->toLocal(p);
			}
			
			void toLocal(void *p)
			{
				byte *b=(byte*)p;

				this->offset=*(dword*)b;
				Endian::from(Endian::xarxa, &this->offset, sizeof(dword));
				b+=sizeof(dword);
				
				this->nPackets=*(dword*)b;
				Endian::from(Endian::xarxa, &this->nPackets, sizeof(dword));
				b+=sizeof(dword);
				
				this->totalSize=*(dword*)b;
				Endian::from(Endian::xarxa, &this->totalSize, sizeof(dword));
				b+=sizeof(dword);
			};
			
			void* toNetwork(void *p)
			{
				byte *data=(byte*)p;
				if (data==NULL)
				{
					data=new byte[this->serializationSize()];
					if (data==NULL)
						throw NotEnoughMemoryException("Not enough memory to "
							"allocate for RPCPacket::segmentationInfo "
							"serialization");
				}
				
				byte *b=data;

				*(dword*)b=this->offset;
				Endian::to(Endian::xarxa, b, sizeof(dword));
				b+=sizeof(dword);
				
				*(dword*)b=this->nPackets;
				Endian::to(Endian::xarxa, b, sizeof(dword));
				b+=sizeof(dword);
				
				*(dword*)b=this->totalSize;
				Endian::to(Endian::xarxa, b, sizeof(dword));
				b+=sizeof(dword);
				
				return data;
			};

			dword serializationSize()
			{
				return sizeof(dword)*3;
			}
		};
	
		Address *a;
		short origen, desti;
		word id;
		dword size;
		byte *data;

	public:
		static const dword hdrSize=16;
		RPCPacket(Address addr, word idCall, byte *data=NULL, dword size=0, short origen=0, short desti=0, bool copy=false);

		RPCPacket(byte* header);
		RPCPacket(const RPCPacket& p);
		virtual ~RPCPacket();

		dword getSize();
		void setSize(dword size);
		short getDestType();
		void setDestType(short desti);
		short getOrType();
		void setOrType(short origen);
		byte* getData();
		void setData(byte* data, bool copy=false);
		void setData(byte* data, dword size, bool copy=false);
		word getCall();
		void setCall(word id);
		Address getAddress();
		
		byte* packet();
		byte* packet(byte *packetBuf, int *pBufSize=NULL, bool copyData=true);
		byte* packet(int *psize);
		
		bool usingLocalCopy();
		void setDeletable(bool del);
		
		segmentationInfo *extractSegmentationInfo();
		
		// Assemblatge parcial d'un paquet
		bool addToAssembledPacketBuffer(void* assyBuf, dword bufLen);
};
#endif
