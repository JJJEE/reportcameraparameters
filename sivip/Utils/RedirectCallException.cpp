#include <Utils/RedirectCallException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

#include <string>
using namespace std;

RedirectCallException::RedirectCallException(string msg, int code): Exception(msg, code)
{

}

RedirectCallException::RedirectCallException(int code, string msg): Exception(msg, code)
{

}

RedirectCallException::RedirectCallException(SerializedException &se): Exception(se)
{

}

RedirectCallException::RedirectCallException(string msg, Address addr, int code): Exception(msg, code), addr(addr)
{
}

RedirectCallException::~RedirectCallException()
{
   
}

Address RedirectCallException::getAddress()
{
	return addr;
}

string RedirectCallException::getClass()
{
	string c=string("RedirectCallException");
	return c;
}

