#include <Utils/ServiceException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

ServiceException::ServiceException(string msg, int code): Exception(msg, code)
{

}

ServiceException::ServiceException(int code, string msg): Exception(msg, code)
{

}

ServiceException::ServiceException(SerializedException &se): Exception(se)
{

}

ServiceException::~ServiceException()
{
   
}

string ServiceException::getClass()
{
	string c=string("ServiceException");
	return c;
}

