/*
 *  debugNew.cpp
 *  
 *
 *  Created by David Marí Larrosa on 06/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "debugNew.h"

#ifndef WIN32

#include <signal.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <sys/time.h>
#include <stdlib.h>
#include <map>
#include <iostream>
#include <Utils/Types.h>
#include <Utils/debugStackTrace.h>
#include <pthread.h>
#include <assert.h>

using namespace std;

struct __newInfo
{
	__newInfo() : size(0), ptr(NULL), arrayAlloc(false), trace(NULL)
	{
		gettimeofday(&timestamp, NULL);
	};

	dword size;			// Tamany d'alloc demanat
	void *ptr;			// Punter retornat pel malloc
	bool arrayAlloc;	// Era normal o era [] ? 
	char *trace;		// Una string que representa l'StackTrace quan es va fer el new
	struct timeval timestamp;	// Moment del new.
};

struct __newInfoListNode : public __newInfo
{
	__newInfoListNode() : __newInfo(), next(NULL) {};

	__newInfoListNode *next;
};

typedef __newInfoListNode __newInfoList;  

__newInfoList memInfo;
//Mutex __mutex_debugNew;
// Fem servir pthread directament, entre d'altres pq Mutex fa uns news...
pthread_mutex_t __mutex_debugNew;

char hex16[16] = {'0', '1', '2', '3', '4', '5', '6', '7',
					'8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

void init_new();

void* operator new(std::size_t s) throw()
{
	STACKTRACE_INSTRUMENT();
	init_new();

//	cerr << "new requested, size " << s << endl;
	__newInfoListNode *info=(__newInfoListNode *)malloc(sizeof(__newInfoListNode));

	if (info==NULL)
	{
		return NULL;
	}
	
	gettimeofday(&info->timestamp, NULL);
	info->ptr=malloc(s);

	if (info->ptr==NULL)
	{
		free(info);
		return NULL;
	}
		
	info->size=s;
	info->arrayAlloc=false;
	info->trace=STACKTRACE_METOSTRING();

	int lockRes=pthread_mutex_lock(&__mutex_debugNew);
	assert(lockRes==0);

	info->next=memInfo.next;
	memInfo.next=info;

	lockRes=pthread_mutex_unlock(&__mutex_debugNew);
	assert(lockRes==0);
	
	return info->ptr;
}

void* operator new[](std::size_t s) throw()
{
	STACKTRACE_INSTRUMENT();
	init_new();

//	cerr << "new[] requested, size " << s << endl;
	__newInfoListNode *info=(__newInfoListNode *)malloc(sizeof(__newInfoListNode));

	if (info==NULL)
	{
		return NULL;
	}
		
	gettimeofday(&info->timestamp, NULL);
	info->ptr=malloc(s);

	if (info->ptr==NULL)
	{
		free(info);
		return NULL;
	}
		
	info->size=s;
	info->arrayAlloc=true;
	info->trace=STACKTRACE_METOSTRING();
	
	int lockRes=pthread_mutex_lock(&__mutex_debugNew);
	assert(lockRes==0);

	info->next=memInfo.next;
	memInfo.next=info;
	
	lockRes=pthread_mutex_unlock(&__mutex_debugNew);
	assert(lockRes==0);

	return info->ptr;
}

void operator delete(void* p)
{
	STACKTRACE_INSTRUMENT();
	init_new();
	if (p==NULL)
	{
		cerr << "delete of a NULL pointer " << p << endl;
#ifdef WITH_DEBUG_STACKTRACE
		cerr << "Stack Trace (if filled): ";
		STACKTRACE_DUMP();
		cerr << endl;
#endif
		return;
	}
		
	int lockRes=pthread_mutex_lock(&__mutex_debugNew);
	assert(lockRes==0);

	__newInfoListNode *itMem=memInfo.next;
	__newInfoListNode *prevItMem=&memInfo;

	while (itMem!=NULL)
	{
		if (itMem->ptr==p)
			break;
		itMem=itMem->next;
		prevItMem=prevItMem->next;
	}
	
	if (itMem==NULL)
	{
		lockRes=pthread_mutex_unlock(&__mutex_debugNew);
		assert(lockRes==0);
		cerr << "delete of a non-allocated pointer " << p << endl;
#ifdef WITH_DEBUG_STACKTRACE
		cerr << "Stack Trace (if filled): ";
		STACKTRACE_DUMP();
		cerr << endl;
#endif
		return;
	}
	
	if (itMem->arrayAlloc)
	{
		lockRes=pthread_mutex_unlock(&__mutex_debugNew);
		assert(lockRes==0);
		cerr << "delete of an array pointer " << p << " (" << itMem->size << " bytes)" << endl;
#ifdef WITH_DEBUG_STACKTRACE
		cerr << "Stack Trace (if filled): ";
		STACKTRACE_DUMP();
		cerr << endl;
#endif
		return;
	}

	free(itMem->ptr);
	if (itMem->trace!=NULL)
		free(itMem->trace);
	prevItMem->next=itMem->next;
	free(itMem);
	lockRes=pthread_mutex_unlock(&__mutex_debugNew);
	assert(lockRes==0);
}

void operator delete[](void* p)
{
	STACKTRACE_INSTRUMENT();
	init_new();
	if (p==NULL)
	{
		cerr << "delete[] of a NULL pointer " << p << endl;
#ifdef WITH_DEBUG_STACKTRACE
		cerr << "Stack Trace (if filled): ";
		STACKTRACE_DUMP();
		cerr << endl;
#endif
		return;
	}
		
	int lockRes=pthread_mutex_lock(&__mutex_debugNew);
	assert(lockRes==0);

	__newInfoListNode *itMem=memInfo.next;
	__newInfoListNode *prevItMem=&memInfo;

	while (itMem!=NULL)
	{
		if (itMem->ptr==p)
			break;
		itMem=itMem->next;
		prevItMem=prevItMem->next;
	}
	
	if (itMem==NULL)
	{
		lockRes=pthread_mutex_unlock(&__mutex_debugNew);
		assert(lockRes==0);
		cerr << "delete[] of a non-allocated pointer " << p << endl;
#ifdef WITH_DEBUG_STACKTRACE
		cerr << "Stack Trace (if filled): " << endl;
		STACKTRACE_DUMP();
		cerr << endl;
#endif
		return;
	}
	
	if (!itMem->arrayAlloc)
	{
		lockRes=pthread_mutex_unlock(&__mutex_debugNew);
		assert(lockRes==0);
		cerr << "delete[] of non-array pointer " << p << " (" << itMem->size << " bytes)" << endl;
#ifdef WITH_DEBUG_STACKTRACE
		cerr << "Stack Trace (if filled): ";
		STACKTRACE_DUMP();
		cerr << endl;
#endif
		return;
	}

	free(itMem->ptr);
	if (itMem->trace!=NULL)
		free(itMem->trace);
	prevItMem->next=itMem->next;
	free(itMem);
	lockRes=pthread_mutex_unlock(&__mutex_debugNew);
	assert(lockRes==0);
}

void dumpAllocatedMemoryMap()
{
	init_new();
	int lockRes=pthread_mutex_lock(&__mutex_debugNew);
	assert(lockRes==0);

	char timeBuf[64];

	struct timeval ts;
	gettimeofday(&ts, NULL);
	ctime_r(&ts.tv_sec, timeBuf);
	cerr << "Memory map dump @ " << timeBuf << endl;

	dword i=0;
	__newInfoListNode *itMem=memInfo.next;
	dword totalSize=0;
	while (itMem!=NULL)
	{
		cerr << (i+1) << ".- " << itMem->size << " bytes @ " << itMem->ptr << ", flags: ";
		
		memset(timeBuf, 0, 64);
		ctime_r(&itMem->timestamp.tv_sec, timeBuf);
		
		if (itMem->arrayAlloc)
			cerr << "[] - TS: " << timeBuf << endl;
		else
			cerr << ".. - TS: " << timeBuf << endl;

#ifdef WITH_DEBUG_STACKTRACE
		cerr << "\tTrace:" << endl << itMem->trace << endl;
#endif	

		if (itMem->size <= 160)
		{
			char *ptr=(char*)itMem->ptr;
			dword p=0;
			dword lastP2=0;
			for (p=0; p<itMem->size; p++)
			{
				if (p!=0 && (p&0xf)==0)
				{
					cerr << "\t";
					lastP2=p;
					for (dword p2=p-16; p2<p; p2++)
					{
						if (ptr[p2]>=32 && ptr[p2]<=127)
							cerr << ptr[p2];
						else
							cerr << ".";
					}
					cerr << endl;
				}
				cerr << hex16[(ptr[p]>>4)&0xf] << hex16[ptr[p]&0xf] << " ";
			}
			for (dword p2=p%16; p2<16; p2++)
				cerr << "   ";

			cerr << "\t";
			for (dword p2=lastP2; p2<itMem->size; p2++)
			{
				if (ptr[p2]>=32 && ptr[p2]<=127)
					cerr << ptr[p2];
				else
					cerr << ".";
			}
			cerr << endl;
		}
		
		totalSize+=itMem->size;
		itMem=itMem->next;
		i++;
	}

	cerr << i << " total entries, " << totalSize << " total bytes" << endl;

	lockRes=pthread_mutex_unlock(&__mutex_debugNew);
	assert(lockRes==0);
}

void sigusr1(int)
{
	dumpAllocatedMemoryMap();
}

bool inited=false;

void init_new()
{
	if (!inited)
	{
		inited=true;
		signal(SIGUSR1, sigusr1);
		
		int mres=pthread_mutex_init(&__mutex_debugNew, NULL);
		assert(mres==0);
		
	}
}

#endif