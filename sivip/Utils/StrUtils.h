#ifndef __SIRIUS__BASE__UTILS__STRUTILS_H
#define __SIRIUS__BASE__UTILS__STRUTILS_H

#include <Sockets/SocketUDP.h>
#include <Utils/debugNew.h>
#include <string>
#include <list>
//#include </usr/include/time.h>

using namespace std;

class StrUtils
{
	static string *digits;
public:
	static string trim(string str);
	// Retorna la primera cadena entre encL i encR de manera balancejada
	static string getFirstTagContents(string str, string tag, bool includeTags=true);
	// Retorna els subtags del tag complet i unic q rep
	static string getSubTags(string tag, string parentTag);
	// retorna el contingut del primer tag, sigui quin sigui
	static string getFirstTagContents(string str, bool includeTags=true);
	// converteix un char en una cadena de dos codificant en hexa
	static string decToHex(unsigned char dec);
	// converteix una cadena de 2 chars en hexa en una altra que es el caracter que toca
	static unsigned char hexToDec(string str);
	// converteix un enter en cadena
	static string decToString(long long num, bool debug=false);
	// converteix un enter en cadena
	static string floatToString(float num);
	// converteix un enter en cadena, en hexa
	static string hexToString(unsigned int num);
	// converteix una cadena en hexa a un enter
	static unsigned int stringToHex(string str);
	//string amb la data actual
	static string getDateString();
	//string amb la data indicada 
	static string toDateString(time_t t);
	//string amb la data indicada, format de timestamp SQL 
	static string toSQLTimestamp(struct timeval t);
	// split, a la perl pero sense regex - sep pot ser un caracter o diversos 
	// (multiples separadors)
	static list<string> split(string str, string sep, int nSplits=-1);
	// Llegeix una linia d'un socket (TCP o UDP, da mastah herencia :P)
	static string readLine(Socket *s);
	// Converteix un path en una llista de noms de nodes
	static list<string> pathToList(string path);
	// Fa un dump en hexa de l'string
	static string hexDump(string str);
	// replace d'una cadena per un altre, en totes les seves aparicions
	static string replace(string str, string search, string replacement);
	// true si la cadena comença per ...
	// TODO: caseInsensitive no afecta el resultat
	static bool startsWith(string str, string search, bool caseInsensitive=false);
	// true si la cadena acaba per ...
	// TODO: caseInsensitive no afecta el resultat
	static bool endsWith(string str, string search, bool caseInsensitive=false);
#ifdef WIN32
	//missatge d'error de Win
	static string StrUtils::errorString(DWORD err);
#endif
};

#endif
