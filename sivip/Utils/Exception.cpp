#include <Utils/Exception.h>
#include <Endian/Endian.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

Exception::Exception(string msg, int code)
{
//	cout<<" New Exception:"<<code<<":"<<msg<<endl;
   this->msg=msg;
   this->code=code;
}

Exception::Exception(int code, string msg)
{
//	cout<<" New Exception:"<<code<<":"<<msg<<endl;
   this->code=code;
   this->msg=msg;
}

Exception::Exception(SerializedException &se)
{
	// Serialitzacio:
	// int -> Longitud del tipus d'excepcio
	// char[] -> Tipus d'excepcio
	// int -> codi
	// int -> longitud del missatge
	// char [] -> missatge
	// total = sizeof(int)*3+missatge.length+tipus.length
//	cout<<" New Exception from SE:"<<endl;

//	Endian e;

	byte *bytes=se.bytes();
	dword nB=se.size();
	
	if (nB<sizeof(int)*3) // Minim els 3 ints (codi + 2 de longituds)
	{
		throw Exception(0, "SerializedException is 0 bytes long");
	}

	byte *b=bytes;

	int classLen=*(int*)b;
	Endian::from(Endian::xarxa, &classLen, sizeof(int));
	b+=sizeof(int);

	string type((char*)b, classLen);
	b+=classLen;
	
	this->code=*(int*)b;
	Endian::from(Endian::xarxa, &this->code, sizeof(int));
	b+=sizeof(int);
	
	int msgLen=*(int*)b;
	Endian::from(Endian::xarxa, &msgLen, sizeof(int));
	b+=sizeof(int);

	string msg((char*)b, msgLen);
	b+=classLen;

	this->msg=msg;
	
//	int *typeLen=(int*)bytes;
//	e.desendiana(typeLen, sizeof(int));
//
//	if (*typeLen+sizeof(int)*3>nB)
//	{
//		throw Exception(0, "Invalid type length: Out of bounds");
//	}
//	
//	char *str=(char*)(typeLen+1);
//	
//	string type(str, *typeLen);
//	
////	if (type!=this->getClass())
////		throw Exception(0, string("Invalid Serialized Exception type ")+type+string(". I am ")+this->getClass());
//	
//	// El codi i la longitud els podem llegir feliçment, pq ja tenim garantit per les condicions de dalt q no son fora del buffer
//	int *codi=(int*)(str+*typeLen);
//	int *msgLen=(int*)(codi+1);
//
//	e.desendiana(codi, sizeof(int));
//	e.desendiana(msgLen, sizeof(int));
//	
//	if (*msgLen+*typeLen+sizeof(int)*3>nB)
//	{
//		throw Exception(0, "Invalid message length: Out of bounds");
//	}
//	
//	str=(char*)(msgLen+1);
//	string msg(str, *msgLen);
//	
//	this->code=*codi;
//	this->msg=msg;
//	cout<<" New Exception from SE:"<<code<<":"<<msg<<endl;
}

Exception::~Exception()
{
   
}

int Exception::getCode()
{
   return (this->code);
}

string Exception::getMsg()
{
   return (this->msg);
}

string Exception::getClass()
{
	string c=string("Exception");
	return c;
}

SerializedException *Exception::serialize()
{
	// Serialitzacio:
	// int -> Longitud del tipus d'excepcio
	// char[] -> Tipus d'excepcio
	// int -> codi
	// int -> longitud del missatge
	// char [] -> missatge
	// total = sizeof(int)*3+missatge.length+tipus.length
	
	
	string classe=this->getClass();
	dword nBytes=sizeof(int)*3+this->msg.length()+classe.length();
	
	
	byte *serialization=new byte[nBytes];
	if (serialization==NULL)
	{
		throw Exception(0, "Not enough memory to allocate serialization buffer");
	}

	byte *b=serialization;
	
	*(int*)b=classe.length();
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	memmove(b, classe.c_str(), classe.length());
	b+=classe.length();
		
	*(int*)b=this->code;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	*(int*)b=this->msg.length();
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	memmove(b, this->msg.c_str(), this->msg.length());
	b+=this->msg.length();

	SerializedException *se=new SerializedException(nBytes, serialization);
	
	delete [] serialization;

	if (se==NULL)
	{
		throw Exception(0, "Not enough memory to allocate SerializedException");
	}
	
	return se;
}

