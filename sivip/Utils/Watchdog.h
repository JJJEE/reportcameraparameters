#ifndef __SIRIUS__BASE__UTILS__WATCHDOG_H
#define __SIRIUS__BASE__UTILS__WATCHDOG_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Threads/Thread.h>
#include <Threads/RWlock.h>

using namespace std;


class WatchdogCallback
{
public:
	virtual void watchdogTimeout(int secs);
};

class Watchdog: protected Thread
{
protected:
	
	Mutex lockData;
	int warningSeconds, lastResetSeconds;
	bool threadStarted, run;
	WatchdogCallback *notify;

	virtual void *execute(int id, void *args);

public:
	Watchdog(int warningSeconds, WatchdogCallback *notify);
	void startCheck();
	void stopCheck();
	int resetTimer();
};
#endif
