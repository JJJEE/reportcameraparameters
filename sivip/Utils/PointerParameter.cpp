#include <Utils/PointerParameter.h>
#include <Utils/debugStackTrace.h>
#include <Utils/debugNew.h>

PointerParameter::PointerParameter() : localAddress(this), remoteAddress(NULL)
{
	STACKTRACE_INSTRUMENT();
}

PointerParameter::PointerParameter(void *local, void *remote) : localAddress(local), remoteAddress(remote)
{
	STACKTRACE_INSTRUMENT();
}

void PointerParameter::toogleMode()
{
	void *remote=this->remoteAddress;
	this->remoteAddress=this->localAddress;
	this->localAddress=remote;
}

void PointerParameter::setAddress(void *local)
{
	STACKTRACE_INSTRUMENT();
	this->localAddress=local;
}

void *PointerParameter::getLocalAddress()
{
	STACKTRACE_INSTRUMENT();
	return this->localAddress;
}

void *PointerParameter::getRemoteAddress()
{
	STACKTRACE_INSTRUMENT();
	return this->remoteAddress;
}
