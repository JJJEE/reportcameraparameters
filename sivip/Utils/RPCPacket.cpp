//#include <Plugin.h>
#include <Utils/Types.h>
#include <Endian/Endian.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPCException.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <iostream>
#include <Utils/debugStackTrace.h>

using namespace std;

RPCPacket::RPCPacket(Address addr, word idCall, byte *data, dword size, short origen, short desti, bool copy)
:id(idCall), size(size), data(data), origen(origen), desti(desti), copied(copy), deletable(false)
{
	STACKTRACE_INSTRUMENT();

	a=new Address(addr);
	if (a==NULL)
	{
		cout << "FATAL: RPCException(\"Not enough memory for RPCPacket address allocation\")" << endl;
		throw RPCException("Not enough memory for RPCPacket address allocation");
	}
		
	if (copied)
	{
		this->data=new byte[size];
		if (this->data==NULL)
		{
			this->data=data;
			this->copied=false;
			throw RPCException("Not enough memory for RPCPacket data allocation");
		}
		
		memmove(this->data, data, size);
	}
}

RPCPacket::RPCPacket(byte* header) : data(NULL), copied(false), deletable(false)
{
	STACKTRACE_INSTRUMENT();

	byte *b=header;

	dword ip = *((dword*)b);
	b+=sizeof(dword);
	
	word port = *((word*)b);
	Endian::from(Endian::xarxa, &port, sizeof(word));
	b+=sizeof(word);
		
	IP ipTmp(ip);
	this->a=new Address(ipTmp, port);
	if (this->a==NULL)
	{
		cout << "FATAL: RPCException(\"Not enough memory for RPCPacket address allocation\")" << endl;
		throw RPCException("Not enough memory for RPCPacket address allocation");
	}
	
	this->origen = *((word*)b);
	Endian::from(Endian::xarxa, &this->origen, sizeof(word));
	b+=sizeof(word);
	
	this->desti = *((word*)b);
	Endian::from(Endian::xarxa, &this->desti, sizeof(word));
	b+=sizeof(word);
	
	this->id = *((word*)b);
	Endian::from(Endian::xarxa, &this->id, sizeof(word));
	b+=sizeof(word);
	
	this->size = *((dword*)b);
	Endian::from(Endian::xarxa, &this->size, sizeof(dword));
	b+=sizeof(dword);
}

RPCPacket::RPCPacket(const RPCPacket& p)
{
	STACKTRACE_INSTRUMENT();

	this->a=new Address((*p.a));
	if (this->a==NULL)
	{
		cout << "FATAL: RPCException(\"Not enough memory for RPCPacket address allocation\")" << endl;
		throw RPCException("Not enough memory for RPCPacket address allocation");
	}
	this->origen=p.origen;
	this->desti=p.desti;
	this->id=p.id;
	this->size=p.size;
	this->copied=p.copied;
	this->deletable=p.deletable;

	if (this->deletable)
	{
		// Com que pot ser que tingui deletable i no sigui copia, fem copia...
		this->copied=true;
		this->deletable=false;
	}

	if (!this->copied)
		this->data=p.data;
	else
	{
		this->data=new byte[this->size];
		if (this->data==NULL)
		{
			this->data=data;
			this->copied=false;
			throw RPCException("Not enough memory for RPCPacket data allocation - Referenced");
		}
		memmove(this->data, p.data, this->size);
	}

}

RPCPacket::~RPCPacket()
{
	STACKTRACE_INSTRUMENT();

	if (this->a!=NULL)
		delete this->a;
	this->a=NULL;
	
	if (copied || deletable)
	{
		if (this->data!=NULL)
			delete [] this->data;
		this->data=NULL;
	}
	
	// Reset
	this->deletable=false;
	this->copied=false;
	
}

short RPCPacket::getDestType()
{
	STACKTRACE_INSTRUMENT();

	return desti;
}

void RPCPacket::setDestType(short desti)
{
	STACKTRACE_INSTRUMENT();

	this->desti=desti;
}

short RPCPacket::getOrType()
{
	STACKTRACE_INSTRUMENT();

	return origen;
}

void RPCPacket::setOrType(short origen)
{
	STACKTRACE_INSTRUMENT();

	this->origen=origen;
}


dword RPCPacket::getSize()
{
	STACKTRACE_INSTRUMENT();

	return size;
}

void RPCPacket::setSize(dword size)
{
	STACKTRACE_INSTRUMENT();

	this->size=size;
}

byte* RPCPacket::getData()
{
	STACKTRACE_INSTRUMENT();

	return data;
}

void RPCPacket::setData(byte* data, bool copy)
{
	this->setData(data, this->size, copy);
}

void RPCPacket::setData(byte* data, dword size, bool copy)
{
	STACKTRACE_INSTRUMENT();

	// cout << "--> RPCPacket::setData() " << endl;
	if (this->copied || this->deletable)
	{
		if (this->data!=NULL)
			delete [] this->data;
		this->data=NULL;
	}
	
	// Reset
	this->deletable=false;
	this->copied=copy;
	
	this->size=size;
	
	if (this->copied)
	{
		if (this->size>=16777216)
		{
			cout << "RPCPacket::setData: Piligro! Size " << this->getSize() << endl;
			throw RPCException(0, "Piligro big sais");
		}
		
		this->data=new byte[this->size];
		if (this->data==NULL)
		{
			this->data=data;
			this->copied=false;
			throw RPCException("Not enough memory for RPCPacket data allocation - Referenced");
		}
		
		memmove(this->data, data, this->size);
	}
	else
		this->data=data;

	// cout << "<-- RPCPacket::setData() " << endl;
}

word RPCPacket::getCall()
{
	STACKTRACE_INSTRUMENT();

	return id;
}

void RPCPacket::setCall(word id)
{
	STACKTRACE_INSTRUMENT();

	this->id=id;
}

Address RPCPacket::getAddress()
{
	STACKTRACE_INSTRUMENT();
	
	if(this->a!=NULL)
		return *(this->a);
	else
		return Address();
}

byte* RPCPacket::packet()
{
	STACKTRACE_INSTRUMENT();

	return packet(NULL, NULL);
}

byte* RPCPacket::packet(byte *packetBuf, int *pBufSize, bool copyData)
{
	STACKTRACE_INSTRUMENT();

	byte *send=packetBuf;
	
	// Revisar la semantica d'aquesta condicio i el que fa... No pot generar
	// leaks??
	if (pBufSize!=NULL && *pBufSize<RPCPacket::hdrSize+size)
		send=new byte[size+RPCPacket::hdrSize];
	
	if (send==NULL)
		send=new byte[size+RPCPacket::hdrSize];
	
	byte *snd=send;
	
	*(dword*)snd = a->getIP().toDWord();
	snd += sizeof(dword);
	*(word*)snd =  a->getPort();
	Endian::to(Endian::xarxa, snd, sizeof(word));
	snd += sizeof(word);
	*(short*)snd = origen;
	Endian::to(Endian::xarxa, snd, sizeof(short));
	snd += sizeof(short);

	//servei
	*(short*)snd = desti;
	Endian::to(Endian::xarxa, snd, sizeof(short));
	snd += sizeof(short);
	*(word*)snd = id;
	Endian::to(Endian::xarxa, snd, sizeof(word));
	snd += sizeof(word);
	
	*(dword*)snd = size;
	Endian::to(Endian::xarxa, snd, sizeof(dword));
	snd += sizeof(dword);

	if (copyData)
		memcpy(snd, data, size);

	if (pBufSize!=NULL)
		*pBufSize=size+RPCPacket::hdrSize;
		
	return send;
}

byte* RPCPacket::packet(int *psize)
{
	STACKTRACE_INSTRUMENT();

	return packet(NULL, psize);
}

bool RPCPacket::usingLocalCopy()
{
	STACKTRACE_INSTRUMENT();

	return copied;
}

void RPCPacket::setDeletable(bool del)
{
	STACKTRACE_INSTRUMENT();

	deletable=del;
}

RPCPacket::segmentationInfo *RPCPacket::extractSegmentationInfo()
{
	STACKTRACE_INSTRUMENT();

	if (id!=RPCPacket::segmentedPacketId)
		throw RPCException("Not a segmented packet");

	RPCPacket::segmentationInfo *segInfo=new RPCPacket::segmentationInfo(data);
	if (segInfo==NULL)
		throw NotEnoughMemoryException("Not enough memory to allocate for "
		"RPCPacket::segmentationInfo");

	return segInfo;
}

// Assemblatge parcial d'un paquet
bool RPCPacket::addToAssembledPacketBuffer(void* assyBuf, dword bufLen)
{
	STACKTRACE_INSTRUMENT();

	if (this->id!=RPCPacket::segmentedPacketId)
		throw RPCException("Not a segmented packet");

	if (this->data==NULL)
		throw RPCException("FATAL: Data is NULL in a segmented Packet");
	
	byte* buf=(byte*)assyBuf;
	
	RPCPacket::segmentationInfo *segInfo=this->extractSegmentationInfo();
	byte *pktData=this->data+sizeof(RPCPacket::segmentationInfo);

//	cout << "RPCPacket::aTAP: " << (void*)buf << ", " << (void*)segInfo << ", " << segInfo->offset << ", " << segInfo->totalSize << ", " << segInfo->nPackets << ", " << this->getSize() << " -- " << (void*)(buf+segInfo->offset) << ", " << (void*)pktData << ", " << min(this->getSize()-segInfo->serializationSize(), segInfo->totalSize-segInfo->offset) << " / " << (void*)(buf+segInfo->totalSize) << endl;
	
	bool result=true;

	if (segInfo->totalSize != bufLen)
	{
		cout << "WARNING: segInfo->totalSize (" << segInfo->totalSize
			<< ") != bufLen (" << bufLen << "), packet may not be of this "
			"stream. NOT ASSEMBLING!" << endl;
		result=false;
	}
	else if (segInfo->offset > bufLen)
	{
		cout << "WARNING: segInfo->offset (" << segInfo->offset
			<< ") > bufLen (" << bufLen << "), packet may not be of this "
			"stream. NOT ASSEMBLING!" << endl;
		result=false;
	}
	else
		memmove(buf+segInfo->offset, pktData,
			min(this->getSize()-segInfo->serializationSize(),
				segInfo->totalSize-segInfo->offset));

	delete segInfo;
	
	return result;
}
