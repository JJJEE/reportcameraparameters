#include <Utils/Canis.h>
#include <Utils/Exception.h>
#include <Utils/FileException.h>
#include <Utils/StrUtils.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Endian/Endian.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>
#include <Utils/Timer.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <string.h>
#include <stdio.h>
#include <ServiceFinder/ServiceFinder.h>

#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

#ifdef WIN32
#include <Utils/WindowsDefs.h>
//#include <windows.h>
#endif

#pragma mark *** Estatiques
#if defined(BSD) || defined(MACOSX)
int Canis::ncpuMIB[2]={CTL_HW, HW_NCPU};
#else
int Canis::ncpuMIB[2]={0, 0};
#endif
int Canis::ncpu=1;

#pragma mark *** Metodes
void Canis::cpuCountInit()
{
#if defined(BSD) || defined(MACOSX)
	size_t ncpuLen=sizeof(int);
	sysctl(ncpuMIB, 2, &ncpu, &ncpuLen, NULL, 0);
#endif /* BSD */

#ifdef LINUX 
	ncpu=(int)sysconf(_SC_NPROCESSORS_CONF);
#endif /* LINUX */

#ifdef WIN32
	SYSTEM_INFO sinfo;
	GetSystemInfo(&sinfo);
	ncpu=(int)sinfo.dwNumberOfProcessors;
#endif /* WIN32 */
}

Canis::Canis(Address announce, short type, bool discoverOnly, byte ifaceMask) : mCastAddr(Address(IP("224.42.42.42"), 4242)), type(type), discoverOnly(discoverOnly), interfaceMask(ifaceMask), lastCleanupTime(0)
{
	if (discoverOnly)
		canisType=Canis::startListen;
	else
		canisType=Canis::startListen||Canis::startAnnounce;
		
	cpuCountInit();

//	IP mult("224.42.42.42");
//	Address a(mult,4242);
	c=new SocketUDP(mCastAddr, SOCK_CONNECT);

	this->announce = new Address(announce); 

	try{
		c->setTimeout(2000);
	}catch(Exception &e){}
	
//	sysLock=new RWLock();

	runSend=false;
	runRecv=false;
	sendID=0;
	recvID=0;
	
	load=0;
	db=NULL;
	multicastInt=NULL;
	interval=5;
	startRecv();
	if(!discoverOnly)
		startSend();
}

Canis::Canis(Address announce, string type, bool discoverOnly, byte ifaceMask) : mCastAddr(Address(IP("224.42.42.42"), 4242)), discoverOnly(discoverOnly), interfaceMask(ifaceMask), lastCleanupTime(0)
{
	if (discoverOnly)
		canisType=Canis::startListen;
	else
		canisType=Canis::startListen||Canis::startAnnounce;
		
	cpuCountInit();
	cout << "Canis: detected Especial" << ncpu << " cpus" << endl;
	
//	IP mult("224.42.42.42");
//	Address a(mult,4242);
	c=new SocketUDP(mCastAddr, SOCK_CONNECT);

	this->announce = new Address(announce); 

	try
	{
		c->setTimeout(2000);
	}
	catch(Exception &e)
	{
	}

//	sysLock=new RWLock();

	runSend=false;
	runRecv=false;
	sendID=0;
	recvID=0;

	load=0;
	db=NULL;
	multicastInt=NULL;
	interval=5;
	
	startRecv();
	//this->type=getType(type);
	int retryCount=3;
	while (retryCount>0)
	{
		try
		{
			//this->type=ServiceFinder::registerSubsystemTypeName(type, this, false);
			this->type=ServiceFinder::registerSubsystemTypeName(type, this, true);
			retryCount=0;
		}
		catch (SocketTimeoutException &ste)
		{
			retryCount--;
		}
	}
	
	if(!discoverOnly)
		startSend();
		
//	cout<<"~Canis end"<<endl;
}

Canis::Canis(string file, bool discoverOnly) : mCastAddr(Address(IP("224.42.42.42"), 4242)), discoverOnly(discoverOnly), lastCleanupTime(0)
{
	printf("Canis iniciando\n");
	if (discoverOnly)
		canisType=Canis::startListen;
	else
		canisType=Canis::startListen||Canis::startAnnounce;
		
	cpuCountInit();
	cout << "Canis: detected " << ncpu << " cpus" << endl;
	
//	sysLock=new RWLock();
	runSend=false;
	runRecv=false;
	sendID=0;
	recvID=0;
	load=0;
	db=NULL;

	FILE *f=fopen(file.c_str(),"rb");

	if (f==NULL)
		throw FileException(string("File ")+file+string(" not found"));

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char *buf=new char[len+1];
	fread(buf,len,1,f);
	buf[len]='\0';
	fclose(f);

	XML *config=xmlParser::parse(string(buf, len));

	delete[] buf;
	xmlNode *n;

	if (config->getNode("/canisConfig")!=NULL)
	{
		// ***
		// *** Versio inicial dels fitxers de config.
		// ***

		n = config->getNode("/canisConfig/Announce/IP");
		if(n==NULL)
		{
			throw(Exception("1Arxiu de configuració incorrecte"));
		}
		
		IP ip(n->getCdata().c_str());

		n = config->getNode("/canisConfig/Announce/Port");
		if(n==NULL)
		{
			throw(Exception("2Arxiu de configuració incorrecte"));
		}
		
		int port = atoi(n->getCdata().c_str());
		announce = new Address(ip, port);

		n = config->getNode("/canisConfig/MulticastInterface/IP");
		if(n==NULL)
		{
			multicastInt=NULL;
		}else
		{
			IP ip(n->getCdata().c_str());
			// TODO: L'adreça i el port no tenen res a veure :P
			// DONE: Separat a la versio estesa dels arxius de config.
			n = config->getNode("/canisConfig/MulticastInterface/Port");
			if(n==NULL)
			{
				throw(Exception("3Arxiu de configuració incorrecte"));
			}
			
			int port = atoi(n->getCdata().c_str());
			multicastInt = new Address(ip, port);
		}
		
		// Ja podem crear el socket pq l'adreça no pot canviar (el format de
		// config no ho permet)
		c=new SocketUDP(mCastAddr, SOCK_CONNECT, multicastInt);
	
		try{
			c->setTimeout(2000);
		}catch(Exception &e){}


		n = config->getNode("/canisConfig/SecondsBetweenPackets");
		if(n==NULL)
			interval=5;
		else
			interval=atoi(n->getCdata().c_str());
		
		n = config->getNode("/canisConfig/SubsystemType");
		if(n==NULL)
		{
			throw(Exception("4Arxiu de configuració incorrecte"));
		}
		
		char *p;
		string s=n->getCdata();
		const char *cs=s.c_str();

		type = strtol(cs, &p, 10);

		startRecv();
		if(p==cs)//no es un int
		{
			//this->type=getType(cs);
			int retryCount=3;
			while (retryCount>0)
			{
				try
				{
					this->type=ServiceFinder::registerSubsystemTypeName(s, this, true);
					retryCount=0;
				}
				catch (SocketTimeoutException &ste)
				{
					retryCount--;
				}
			}
		}

		cout << "Canis::Canis(xml) subsytemType (/CanisConfig/SSType)"<<s<<" (int):"<<type<<" this->type:"<<this->type << endl;
	}
	else if (config->getNode("/[0]/Canis")!=NULL)
	{
		// ***
		// *** Configuracio com a part d'un fitxer de config amb mes informacio.
		// ***
		n = config->getNode("/[0]/Service/IP");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Service/IP"));
		}
		
		IP ip(n->getCdata().c_str());

		n = config->getNode("/[0]/Service/InterfaceMask");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Service/InterfaceMask"));
		}
		
		this->interfaceMask = (byte)atoi(n->getCdata().c_str());

		n = config->getNode("/[0]/Service/Port");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Service/Port"));
		}
		
		int port = atoi(n->getCdata().c_str());
		announce = new Address(ip, port);

		n = config->getNode("/[0]/Canis/Port");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Canis/Port"));
		}
		
		port = atoi(n->getCdata().c_str());

		n = config->getNode("/[0]/Canis/MulticastGroup");
		if(n==NULL)
		{
			IP mCastGroup("224.42.42.42");
			mCastAddr=Address(mCastGroup, port);
		}
		else
		{	
			IP mCastGroup(n->getCdata().c_str());
			mCastAddr=Address(mCastGroup, port);
		}

		n = config->getNode("/[0]/Canis/MulticastInterface/IP");
		if(n==NULL)
			// Si no tenim IP de multicast interface, per defecte agafem la de servei
			multicastInt = new Address(ip, port);
		else
		{
			IP ipMulti(n->getCdata().c_str());
			multicastInt = new Address(ipMulti, port);
		}

		// Ja podem crear el socket
		c=new SocketUDP(mCastAddr, SOCK_CONNECT, multicastInt);
	
		try{
			c->setTimeout(2000);
		}catch(Exception &e){}

		
		n = config->getNode("/[0]/Canis/SecondsBetweenPackets");
		if(n==NULL)
			interval=5;
		else
			interval=atoi(n->getCdata().c_str());
		
		n = config->getNode("/[0]/Service/SubsystemType");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Service/SubsystemType"));
		}
		
		char *p;
		string aux=n->getCdata();
		const char *cs=aux.c_str();

		type = strtol(cs, &p, 10);

		startRecv();
		if(p==cs)//no es un int
		{
			//this->type=getType(cs);
			int retryCount=3;
			while (retryCount>0)
			{
				try
				{
					this->type=ServiceFinder::registerSubsystemTypeName(aux, this, true);
					retryCount=0;
				}
				catch (SocketTimeoutException &ste)
				{
					retryCount--;
				}
			}
		}
		cout<<" New Canis type:"<<this->type<<":"<<announce->toString() << "(" << mCastAddr.toString() << ")" <<endl;
	}
	else
	{
		throw(Exception("Unrecognised config file"));
	}
	
	
	if(!discoverOnly)
		startSend();
	delete config;

}

Canis::Canis(string file, int startType): canisType(startType), discoverOnly((startType&Canis::startAnnounce)==0), lastCleanupTime(0)
{
	cpuCountInit();
//	cout << "Canis: detected " << ncpu << " cpus" << endl;
	

//	sysLock=new RWLock();
	runSend=false;
	runRecv=false;
	sendID=0;
	recvID=0;
	load=0;
	db=NULL;

	FILE *f=fopen(file.c_str(),"rb");

	if (f==NULL)
		throw FileException(string("File ")+file+string(" not found"));

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char *buf=new char[len+1];
	fread(buf,len,1,f);
	buf[len]='\0';
	fclose(f);

	XML *config=xmlParser::parse(string(buf, len));

	delete[] buf;
	xmlNode *n;

	if (config->getNode("/canisConfig")!=NULL)
	{
		// ***
		// *** Versio inicial dels fitxers de config.
		// ***

		n = config->getNode("/canisConfig/Announce/IP");
		if(n==NULL)
		{
			throw(Exception("1Arxiu de configuració incorrecte"));
		}
		
		IP ip(n->getCdata().c_str());

		n = config->getNode("/canisConfig/Announce/Port");
		if(n==NULL)
		{
			throw(Exception("2Arxiu de configuració incorrecte"));
		}
		
		int port = atoi(n->getCdata().c_str());
		announce = new Address(ip, port);

		n = config->getNode("/canisConfig/MulticastInterface/IP");
		if(n==NULL)
		{
			multicastInt=NULL;
		}else
		{
			IP ip(n->getCdata().c_str());
			// TODO: L'adreça i el port no tenen res a veure :P
			// DONE: Separat a la versio estesa dels arxius de config.
			n = config->getNode("/canisConfig/MulticastInterface/Port");
			if(n==NULL)
			{
				throw(Exception("3Arxiu de configuració incorrecte"));
			}
			
			int port = atoi(n->getCdata().c_str());
			multicastInt = new Address(ip, port);
		}
		
		// Ja podem crear el socket pq l'adreça no pot canviar (el format de
		// config no ho permet)
		c=new SocketUDP(mCastAddr, SOCK_CONNECT, multicastInt);
	
		try{
			c->setTimeout(2000);
		}catch(Exception &e){}


		n = config->getNode("/canisConfig/SecondsBetweenPackets");
		if(n==NULL)
			interval=5;
		else
			interval=atoi(n->getCdata().c_str());
		
		n = config->getNode("/canisConfig/SubsystemType");
		if(n==NULL)
		{
			throw(Exception("4Arxiu de configuració incorrecte"));
		}
		
		char *p;
		string s=n->getCdata();
		const char *cs=s.c_str();

		type = strtol(cs, &p, 10);

		startRecv();
		if(p==cs)//no es un int
		{
			//this->type=getType(cs);
			this->type=ServiceFinder::registerSubsystemTypeName(s, this, true);
		}
	}
	else if (config->getNode("/[0]/Canis")!=NULL)
	{
		// ***
		// *** Configuracio com a part d'un fitxer de config amb mes informacio.
		// ***
		n = config->getNode("/[0]/Service/IP");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Service/IP"));
		}
		
		IP ip(n->getCdata().c_str());

		n = config->getNode("/[0]/Service/InterfaceMask");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Service/InterfaceMask"));
		}
		
		this->interfaceMask = (byte)atoi(n->getCdata().c_str());

		n = config->getNode("/[0]/Service/Port");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Service/Port"));
		}
		
		int port = atoi(n->getCdata().c_str());
		announce = new Address(ip, port);

		n = config->getNode("/[0]/Canis/Port");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Canis/Port"));
		}
		
		port = atoi(n->getCdata().c_str());

		n = config->getNode("/[0]/Canis/MulticastGroup");
		if(n==NULL)
		{
			IP mCastGroup("224.42.42.42");
			mCastAddr=Address(mCastGroup, port);
		}
		else
		{	
			IP mCastGroup(n->getCdata().c_str());
			mCastAddr=Address(mCastGroup, port);
		}

		n = config->getNode("/[0]/Canis/MulticastInterface/IP");
		if(n==NULL)
			// Si no tenim IP de multicast interface, per defecte agafem la de servei
			multicastInt = new Address(ip, port);
		else
		{
			IP ipMulti(n->getCdata().c_str());
			multicastInt = new Address(ipMulti, port);
		}

		// Ja podem crear el socket
		c=new SocketUDP(mCastAddr, SOCK_CONNECT, multicastInt);
	
		try{
			c->setTimeout(2000);
		}catch(Exception &e){}

		
		n = config->getNode("/[0]/Canis/SecondsBetweenPackets");
		if(n==NULL)
			interval=5;
		else
			interval=atoi(n->getCdata().c_str());
		
		n = config->getNode("/[0]/Service/SubsystemType");
		if(n==NULL)
		{
			throw(Exception("Configuration file missing Service/SubsystemType"));
		}
		
		char *p;
		string aux=n->getCdata();
		const char *cs=aux.c_str();

		type = strtol(cs, &p, 10);

		startRecv();
		if(p==cs)//no es un int
		{
			//this->type=getType(cs);
			int retryCount=3;
			while (retryCount>0)
			{
				try
				{
					this->type=ServiceFinder::registerSubsystemTypeName(aux, this, true);
					retryCount=0;
				}
				catch (SocketTimeoutException &ste)
				{
					retryCount--;
				}
			}
		}
		cout<<" New Canis type:"<<this->type<<":"<<announce->toString() << "(" << mCastAddr.toString() << ")" <<endl;
	}
	else
	{
		throw(Exception("Unrecognised config file"));
	}
	
	if (canisType&Canis::startAnnounce)
		startSend();
	delete config;
}

Canis::~Canis()
{
	stopRecv();
	stopSend();
	// Me cagun la HOSTIA!!! Aixo ha d'estar amb joinAll pq si no es fa abans
	// el delete del thread que l'exitThread en segons quins casos :P
	joinAll();
//	detachAll();
	
	delete c;
	delete announce;
	
	this->resetList();
	
//	delete sysLock;
	if(db!=NULL)
		delete db;
	if(multicastInt!=NULL)
		delete multicastInt;
}

void Canis::startSend()
{
	cout<<" canis runsend()"<<endl;
	if(!runSend)
	{
		canisType|=Canis::startAnnounce;
		discoverOnly=false;
		runSend=true;
		int *i=(int*)1;
		sendID=start(i, Thread::OnExceptionRestart);
		//detach(id);
	}
}

void Canis::stopSend()
{
	cout<<" canis stopsend()"<<endl;
	canisType&=~Canis::startAnnounce;
	runSend=false;
}

bool Canis::isSending()
{
	return this->runSend;
}

void Canis::startRecv()
{
	if(!runRecv)
	{
		canisType|=Canis::startListen;
		runRecv=true;
		int *i=0;
		recvID=start(i, Thread::OnExceptionRestart);
	//	detach(id);
	}
}

void Canis::stopRecv()
{
	canisType&=~Canis::startListen;
	runRecv=false;
}

bool Canis::isReceiving()
{
	return this->runRecv;
}

Address Canis::getAddress()
{
	return *announce;
}

void Canis::setType(short type)
{
	this->type=type;
}

short Canis::getType()
{
	return this->type;
}

short Canis::getType(string stype)
{

	dbCond.lock();
	while(db==NULL) 
		dbCond.wait();
	dbCond.unlock();

	string s("SELECT * FROM tiposubsistema WHERE nombre='");
	s+=stype;
	s+=string("'");
	
	RPCPacket p(*announce, 0, (byte*)s.c_str(), s.length(), 0,-2, true);

	RPC rpc(*db);
	RPCPacket *pck=rpc.call(p);
	string res((char*)(pck->getData()),pck->getSize());
	cout << "Canis::getType query:"<<s<<" to:"<<db->toString()<<endl<<"res:"<<res<< endl;
	delete pck;
	
	XML* xml=xmlParser::parse(res);	
	
	xmlNode *n=xml->getNode("/result");
	if (n==NULL || n->getParam("isException")==string("true"))
	{
		string message;
		
		if (n==NULL)
			message=string("Invalid result obtained from DBGateway");
		else
			message=n->getCdata();
			
		throw Exception(message);
	}
	
	n=xml->getNode("/result/row/id");
	if(n==NULL)
	{
		delete xml;
		// L'inserim
		s=string("INSERT INTO tiposubsistema (nombre) VALUES ('")+stype+string("')");
		RPCPacket pIns(*announce, 1, (byte*)s.c_str(), s.length(), 0,-2, true);
		RPCPacket *pResIns=rpc.call(pIns);
		delete pResIns;
		
		pck=rpc.call(p);
		string res((char*)(pck->getData()),pck->getSize());
		delete pck;

		cout << "res: " << res << endl;

		xml=xmlParser::parse(res);	

		n=xml->getNode("/result");
		if (n==NULL || n->getParam("isException")==string("true"))
		{
			string message;
			
			if (n==NULL)
				message=string("Invalid result obtained from DBGateway");
			else
				message=n->getCdata();
				
			throw Exception(message);
		}
		
		n=xml->getNode("/result/row/id");
		
		if (n==NULL)
		{
			delete xml;
			cout<<" select:"<<s<<endl;
			cout<<" resposta:"<<xml->toString()<<endl;
			throw(Exception("Error: Tipus incorrecte:"+stype));
		}
	}

	int idType=atoi(n->getCdata().c_str());		
	delete xml;
	return idType;
}

void Canis::setLoad(float load)
{
	this->load=load;
}

byte Canis::getInterfaceMask()
{
	return this->interfaceMask;
}

Address Canis::getDBAddress()
{
	return *db;
}


void Canis::debug_list()
{
	list<subsys> *l=getSysCopy();
	
	cout<<" -----------canis------------"<<endl;
	while(!l->empty())
	{
		subsys p=l->front();
		l->pop_front();
		cout<<" Subsys - ip:"<<IP(p.ip).toString()<<" port:"<<p.port<<" type:"<< p.type<<endl;
	}
	delete l;
	cout<<"          ---------"<<endl;

}

list<Canis::subsys>* Canis::getSysCopy()
{
	sysLock.rlock();
	list<subsys> *copy=new list<subsys>;//(systems);
	try
	{

		map<string,subsys>::const_iterator i;
//		subsys s;
		for (i=systems.begin(); i!=systems.end(); i++)
		{
//			cout<<"sybsys 1"<<endl;
//			s=*i;
//			cout<<"sybsys 2"<<endl;
			copy->push_back((*i).second);
//			cout<<"sybsys 3"<<endl;
		}
	}catch(...){cout<<" getSysCopy Exception"<<endl;}
	sysLock.unlock();
	return copy;
}

list<Canis::subsys> Canis::getTypeList(short type)
{
	sysLock.rlock();

	list<subsys> res;
	try
	{
		map<string, subsys>::const_iterator i;
		for (i=this->systems.begin(); i!=this->systems.end(); i++)
		{
			if((*i).second.type==type)
				res.push_back((*i).second);
		}
	}catch(...){cout<<" getTypeList Exception"<<endl;}

	sysLock.unlock();
	return res;
}

void Canis::resetList()
{
	sysLock.wlock();
	systems.clear();
	sysLock.unlock();
}

void* Canis::execute(int id, void *params)
{
	if(((int *)params)==0)
	{
		while(runRecv)
		{
			try{
				recvThread();
			}catch(...){}
			//cout<<" *********************** Canis Recv Thread exit recv:"<<runRecv<<"******************"<<endl;
		}
	}else{
		while(runSend)
		{
			try{
				sendThread();
			}catch(...){}
			//cout<<" *********************** Canis Send Thread exit send:"<<runSend<<"******************"<<endl;
		}
	}
	return NULL;
}

void Canis::recvThread()
{
//	Endian e;

	byte buf[hdrSize];
	int rd;
	int notfound=0;
	int selfIP=-1, selfPort=-1;
	bool found;
//	list<subsys> *l, nova;
	subsys s, me;
	time_t t;
	//char ctime_buf[32];
	Address from;

	if(announce!=NULL)
	{
		selfIP=announce->getIP().toDWord();
		selfPort=announce->getPort();
	}

	while(runRecv)
	{
		try
		{
			rd=c->read(buf, hdrSize, &from);
//			cout << "Canis recv from: " << from.toString() << endl;

		}catch(SocketException &e)
		{
			SocketUDP *aux=c;
//			IP mult("224.42.42.42");
//			Address a(mult,4242);
			c=new SocketUDP(mCastAddr, SOCK_CONNECT);
			if (c==NULL)
				c=aux;
			else
				delete aux;
				
			try{
				c->setTimeout(2000);
			}catch(Exception &e){}
			continue;
		}

		if(rd<hdrSize || !runRecv)
			continue;

		Endian::from(Endian::xarxa, (buf+4), sizeof(word));
		Endian::from(Endian::xarxa, (buf+6), sizeof(word));
		Endian::from(Endian::xarxa, (buf+8), sizeof(float));

		s.ip=*((dword*)buf);
		s.port=*((word*)(buf+4));
		s.type=*((short*)(buf+6));
		s.load=*((float*)(buf+8));
		s.last=time(NULL);
		
		found=false;
		
		string key=StrUtils::decToString(s.ip)+string(":")+StrUtils::decToString(s.port)+string(":")+StrUtils::decToString(s.type);
		
		sysLock.wlock();
		// Comprovem si ja hi era o era nou... (per comprovacions posteriors)
		if (systems.find(key)!=systems.end())
			found=true;
		systems[key]=s;
		sysLock.unlock();

		// Netegem els mes vells de 15s, cada 1s
		bool cleanedup=systemsCleanup(15, 1);
		
		// Comprovem si el nostre ha caducat, si hem fet neteja, per tant
		// com a molt un cop per segon...
		if (cleanedup)
		{
			string meKey=StrUtils::decToString(selfIP)+string(":")+StrUtils::decToString(selfPort)+string(":")+StrUtils::decToString(this->type);
			sysLock.rlock();
			map<string,subsys>::iterator meIt=systems.find(meKey);
			if (meIt!=systems.end())
			{
				me=(*meIt).second;
			}
			sysLock.unlock();
		
			int meT = (time(NULL) - me.last);
			//if (found && !discoverOnly && selfIP!=-1 && me.ip==selfIP && me.port==selfPort && meT > interval+2)
			if (found && this->isSending() && selfIP!=-1 && me.ip==selfIP && me.port==selfPort && meT > interval+2) // isSending, per que no es queixi pel prioFailover
			{
				// selfTimeout! Fa mes de dos segons que no sabem res de
				// nosaltres mateixos...
				cout << " ------------- Canis self not found, interval: "
					<< interval << endl;
			}
		}
		
		if(!found) //nova entrada
		{
			if(s.type==DBType)
			{
				Address *oldDB=db;

				db=new Address(IP(s.ip), s.port);
				dbCond.broadcast();

				if(oldDB!=NULL)
					delete oldDB;
			}
		}
	}
}

void Canis::sendThread()
{
	byte buf[hdrSize];
	time_t t;
	char ctime_buf[32];
	unsigned int aux=0;
	Timer timer; 
	TimerInstant ti; 
	
	*((dword*)buf)=announce->getIP().toDWord();
	*((word*)(buf+4))=announce->getPort();
	
	Endian::from(Endian::xarxa, (buf+4), sizeof(word));
	
	timer.start();
	while(runSend)
	{
		try
		{
#if defined(BSD) || defined(MACOSX)
			getrusage(RUSAGE_SELF, &procRsrcUsage);
#elif defined(LINUX)
			// Ojo Braga: A linux els threads son processos ;)
			getrusage(RUSAGE_CHILDREN, &procRsrcUsage);
#elif defined(WIN32)
			GetProcessTimes(GetCurrentProcess(), &dummyTime1, &dummyTime2, &procSysTime, &procUserTime);
#endif /* WIN32 */
		
			if (ti.seconds()==0.0)
				load=0.0;
			else
			{
				double secs=ti.seconds();
#ifndef WIN32
				TimerInstant *uTime=new TimerInstant(lastProcRsrcUsage.ru_utime, procRsrcUsage.ru_utime);
				TimerInstant *sTime=new TimerInstant(lastProcRsrcUsage.ru_stime, procRsrcUsage.ru_stime);
#elif defined(WIN32)
				struct timeval lastUser, currentUser, lastSys, currentSys;
				FILETIMEToTimeval(procSysTime, &currentSys);
				FILETIMEToTimeval(lastProcSysTime, &lastSys);
				FILETIMEToTimeval(procUserTime, &currentUser);
				FILETIMEToTimeval(lastProcUserTime, &lastUser);
				TimerInstant *uTime=new TimerInstant(lastUser, currentUser);
				TimerInstant *sTime=new TimerInstant(lastSys, currentSys);
#endif /* WIN32 */
				
				if (uTime!=NULL && sTime!=NULL)
				{
					double uSecs=uTime->seconds();
					double sSecs=sTime->seconds();
				
					double procSecs=uSecs+sSecs;

					if (secs!=0.0)
						load=(float)(procSecs/secs);
					else
						load=0.5;

					if (ncpu>0)
						load/=ncpu;

//					cout << "+++++ Canis: secs: " << secs << ", procSecs: " << procSecs << ", load: " << load << " (" << ncpu << " cpus)" << endl;
				}

				if (uTime!=NULL)
					delete uTime;
				
				if (sTime!=NULL)
					delete sTime;
				
			}

#ifndef WIN32
			memmove(&lastProcRsrcUsage, &procRsrcUsage, sizeof(struct rusage));
#elif defined(WIN32)
			memmove(&lastProcSysTime, &procSysTime, sizeof(FILETIME));
			memmove(&lastProcUserTime, &procUserTime, sizeof(FILETIME));
#endif /* WIN32 */
		
		
			*((short*)(buf+6))=type;
			*((float*)(buf+8))=load;

			Endian::from(Endian::xarxa, (buf+6), sizeof(word));
			Endian::from(Endian::xarxa, (buf+8), sizeof(float));

			c->write(buf,hdrSize);	
			t=time(NULL);
			ctime_r(&t, ctime_buf);
			//cout<<" ----- Canis Send  run:"<<runSend<<"- "/*<<ctime_buf*/<<endl;//<<" interval: "<<interval<<endl;
			aux=(unsigned int)interval;
			while(aux)
				aux=sleep(aux);
			//cout<<" ----- Canis end sleep ---"<<endl<<" interval: "<<interval<<endl;
			ti=timer.time();
			timer.start();
		}
		catch(Exception &e)
		{
			cout<<"Canis::Send exception: "<<e.getClass()<<": "<<e.getMsg()<<endl;
		}
		catch(...)
		{
			cout<<"Canis::Send unknown exception"<<endl;
		}
	}
}

// void Canis::setSysList(list<subsys> l)
// {
// 	sysLock.wlock();
// 
// //	while(!systems.empty())
// //	{
// //		systems.pop_front();
// //	}
// 	systems.clear();
// 	
// 	while(!l.empty())
// 	{
// 		subsys s=l.front();
// 		systems.push_back(s);
// 		l.pop_front();
// 	}
// 	sysLock.unlock();
// }

bool Canis::systemsCleanup(int maxAgeSeconds, int minPeriod)
{
	map<string, subsys>::iterator sIt;

	int now = time(NULL);
	if ((now - this->lastCleanupTime) >= minPeriod)
	{
		sysLock.wlock();
		for (sIt=this->systems.begin(); sIt!=this->systems.end(); sIt++)
		{
			try
			{
				subsys p=(*sIt).second;
				int t=(now - p.last);
				if(t >= 15)
					this->systems.erase(sIt);
			}
			catch (...) {};
		}
		sysLock.unlock();
		this->lastCleanupTime=now;
		return true;
	}
	return false;
}
