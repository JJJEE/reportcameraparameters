#ifndef __SIRIUS__BASE__UTILS__CANIS_H
#define __SIRIUS__BASE__UTILS__CANIS_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Threads/Thread.h>
#include <Threads/RWlock.h>
#include <Threads/Condition.h>
#include <Sockets/SocketUDP.h>
#include <list>
#include <map>
#include <string>
#include <time.h>
#include <sys/types.h>
//#include </usr/include/time.h>

#ifndef WIN32
#include <sys/sysctl.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#endif
#include <stdio.h>
#ifdef WIN32
#include <Utils/WindowsDefs.h>
//#include <windows.h>
#endif

using namespace std;

class Canis : public Thread
{
public:
	static const int startListen=1;
	static const int startAnnounce=2;

protected:
	byte interfaceMask;
	Address mCastAddr;
	SocketUDP *c;
	static const int hdrSize=12;
	static const short DBType=-2;
	int interval;
	bool runSend, runRecv;
	int sendID, recvID;
	bool discoverOnly;
	int canisType;
	
	Address *announce, *db, *multicastInt;
	Condition dbCond;
	short type;
	float load;
	
#ifndef WIN32
	struct rusage lastProcRsrcUsage, procRsrcUsage;
#elif defined(WIN32)
	FILETIME dummyTime1, dummyTime2, procSysTime, procUserTime, lastProcSysTime, lastProcUserTime;
#endif /* WIN32 */
	static int ncpuMIB[2];
	static int ncpu;

public:
	class subsys
	{
		public:
			dword ip;
			word port;
			short type;
			float load;
			time_t last;
			subsys()
			{}
			subsys(const Canis::subsys &s)
			{
				ip=s.ip;
				port=s.port;
				type=s.type;
				load=s.load;
				last=s.last;
			}
	};
protected:

	map<string, subsys> systems;
	RWLock sysLock;
	int lastCleanupTime;
	
	static void cpuCountInit();

public:
	Canis(Address announce, short type=0, bool discoverOnly=false, byte ifaceMask=24);
	Canis(Address announce, string type, bool discoverOnly=false, byte ifaceMask=24);
	Canis(string file, bool discoverOnly=false);
	Canis(string file, int startType);
	~Canis();
	void startSend();
	void stopSend();
	bool isSending();
	void startRecv();
	void stopRecv();
	bool isReceiving();
	Address getAddress();
	void setType(short type);
	short getType();
	short getType(string stype);
	void setLoad(float load);
	byte getInterfaceMask();
	Address getDBAddress();

	void debug_list();

	list<subsys>* getSysCopy();
	list<subsys> getTypeList(short type);
	
	void resetList();
	
private:
	void* execute(int id, void* params);
	void recvThread();
	void sendThread();
//	void setSysList(list<subsys> l);
	bool systemsCleanup(int maxAgeSeconds, int minPeriod=1);
};


#endif
