#include <Utils/RPCException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

RPCException::RPCException(string msg, int code): Exception(msg, code)
{

}

RPCException::RPCException(int code, string msg): Exception(msg, code)
{

}

RPCException::RPCException(SerializedException &se): Exception(se)
{

}

RPCException::~RPCException()
{
   
}

string RPCException::getClass()
{
	string c=string("RPCException");
	return c;
}

