/*
 *  RecordingFileBlockCache.cpp
 *  
 *
 *  Created by David Marí Larrosa on 26/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Utils/Cache.h>
#include <Utils/CacheException.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

#include <iostream>

using namespace std;

Cache::block* Cache::getFreeBlock()
{
	for (dword b=0; b<nBlocks; b++)
	{
		if (blocks[b].data==NULL)
		{
			return &blocks[b];
		}
	}
	
	// optimitzem, a veure si ens estalviem de treure blocs de cache
//	cout << "pre optimize nBlocks " << nBlocks << endl;
	optimize();
//	cout << "post optimize nBlocks " << nBlocks << endl;

	// Si n'hi ha algun de buit ja el retornem
	for (dword b=0; b<nBlocks; b++)
	{
//		cout << "looking post optimized blocks " << b << " (" << (blocks[b].data==NULL?"empty":"full") << ")" << endl;
		if (blocks[b].data==NULL)
		{
			return &blocks[b];
		}
	}
	
	// Si no, aplicar LRU per buidar un bloc
//	cout << "LRU!!" << endl;
	for (dword b=0; b<nBlocks; b++)
	{
//		cout << "pause: " << b << endl;
		blocks[b].lastUse->pause();
	}
		
	Cache::block *bl=blocks;
	double maxSecs=0.0;
	
	for (dword b=0; b<nBlocks; b++)
	{
//		cout << "lru search " << b << endl;
		TimerInstant blockTI=blocks[b].lastUse->time();
		double blockSecs=blockTI.seconds();
		
		if (blockSecs>maxSecs)
		{
			bl=&blocks[b];
			maxSecs=blockSecs;
		}
	}
	
	for (dword b=0; b<nBlocks; b++)
	{
//		cout << "resume: " << b << endl;
		blocks[b].lastUse->resume();
	}
	
	// Alliberem el block
	if (bl->operation==Cache::write)
	{
//		cout << "lrucallback" << endl;
		lruCallback(this, bl);
//		cout << "ret lrucallback" << endl;
	}
	
	if (bl->data!=NULL)
	{
//		cout << "lru cleanup" << endl;
	
		delete [] (byte*)bl->data;
		delete bl->lastUse;
	}

	memset(bl, 0, sizeof(Cache::block));

//	cout << "<-- getFreeBlock" << endl;
	return bl;	
}

Cache::block* Cache::searchBlock(fileOffset off, dword size)
{	
	dword bestBl=nBlocks;
	for (dword b=0; b<nBlocks; b++)
	{
		if (blocks[b].data!=NULL)	// El bloc conte dades
		{
			if (off>=blocks[b].offset && off+size<=blocks[b].offset+blocks[b].size)
			{
				// Estem dins :)
				// Comprovem si el q tenim es de write i si tenim un de read (el de write,
				// com que modifica, es preferible, encara q sigui mes vell...), o si els
				// dos son de write o altres casos complexos
				if (bestBl<nBlocks)
				{
					if (blocks[bestBl].operation==blocks[b].operation)
					{
						// Els dos son de write o de read, agafar el mes recent
						TimerInstant tiBest=blocks[bestBl].lastUse->time();
						TimerInstant tiCurr=blocks[b].lastUse->time();
						
						if (tiCurr.seconds()<tiBest.seconds())
							bestBl=b;
					}
					else if (blocks[b].operation==Cache::write)
					{
						// Actual de write, l'altre, per eliminacio de read
						bestBl=b;
					}
					// Si no es cap dels dos, ja tenim el millor a bestBl
				}
				else // Primer block bo que trobem
					bestBl=b;
			}
		}
	}
	
	if (bestBl==nBlocks)
	{
		nMisses++;
		return NULL;
	}
	
	nHits++;
	blocks[bestBl].lastUse->stop();
	blocks[bestBl].lastUse->start();
	return &blocks[bestBl];
}

void Cache::optimize()
{
//	cout << "Cache::optimize()" << endl;
	
	// Evitem que s'optimitzi mes del compte :P
	this->nextOptimize=Cache::defaultSize*3;

	bool modified=true;
	// Optimitzacio de la cache. Es fan N passades fins que no s'hagi fet cap canvi a la cache:
	// a) Ajuntar reads i/o writes amb reads i/o writes que es puguin sequenciar.
	// b) Evitar solapaments entre reads i writes. Preferencia per writes.
	// c) Convertir writes en reads, escrivint-los i conservant-los com a read. Nomes en el cas
	//		que poguessin caure en el grup a) si fossin del mateix tipus
	dword totalFreed=0;
	dword nIt=0;
	// Fem un minim de dues iteracions, per donar preferencia al join de tipus iguals. Aixi, ens assegurem que quan
	// es converteix un write en read desant-lo a disc, ja s'ha joinat tot el possible i nomes fem un write.
	while (modified || nIt<=1)
	{
		nIt++;
		modified=false;
		for (dword b=0; b<nBlocks; b++)
		{
			Cache::block *bl=&blocks[b];
//			cout << "b: " << b << endl;
			if (bl->data!=NULL && bl->size!=0)
			{
//				cout << "\tb: " << b << " inside" << endl;
				for (dword b2=b+1; b2<nBlocks; b2++)
				{
					Cache::block *bl2=&blocks[b2];
//					cout << "\tb2: " << b2 << endl;
					
					if (bl2->data!=NULL && bl2->size!=0)
					{
//						cout << "\t\tb2: " << b2 << " inside" << endl;
						// Bloc 2 "darrera" del bloc 1 (inclus amb overlapping)
						if (bl2->offset>=bl->offset && bl2->offset<=bl->offset+bl->size)
						{
							if (bl->operation==bl2->operation && bl2->offset-bl->offset+bl2->size<=mergeMaxSize)
							{
//								cout << "\t\t" << b << " + " << b2 << " nBl " << nBlocks << endl;
								// a) Merges
								fileOffset blockSize=bl2->offset-bl->offset+bl2->size;
								// Pot ser q bl2 estigui totalment contingut
								if (blockSize<bl->size)
									blockSize=bl->size;
								byte *dades=new byte[blockSize];
								if (dades==NULL)
									continue;
								
//								cout << "\t\tmm blsz " << blockSize << " nBl " << nBlocks << endl;
								memmove(dades, bl->data, bl->size);
//								cout << "\t\tmm 1 nBl " << nBlocks << endl;
								memmove(dades+(bl2->offset-bl->offset), bl2->data, bl2->size);
//								cout << "\t\tmm 2 nBl " << nBlocks << endl;
								
								byte *oldData=(byte*)bl->data;
								bl->data=dades;
								bl->size=blockSize;
//								cout << "Cache::optimize delete 1" << endl;
								if (oldData!=NULL)
								{
//									cout << "\t\td bl a nBl " << nBlocks << endl;
									delete [] oldData;
//									cout << "\t\td bl a nBl " << nBlocks << endl;
								}
//								cout << "Cache::optimize delete 2" << endl;
								if (bl2->data!=NULL && bl2->size!=0)
								{
//									cout << "\t\td bl2 a nBl " << nBlocks << endl;
									delete [] (byte*)bl2->data;
//									cout << "\t\td bl2 b nBl " << nBlocks << endl;
									delete bl2->lastUse;
//									cout << "\t\td bl2 c nBl " << nBlocks << endl;
								}
								memset(bl2, 0, sizeof(Cache::block));
								bl->lastUse->stop();
								bl->lastUse->start();

//								cout << "Cache::optimize merge, final size: " << bl->size << endl;

								totalFreed++;
								modified=true;
							}
							else if (nIt>1 && totalFreed*10<nBlocks && bl->operation!=bl2->operation && bl2->offset-bl->offset+bl2->size<=mergeMaxSize)
							{
								// b) Solapaments amb tipus diferents...
								// Nomes treballem els tipus diferents a partir de la segona iteracio i si
								// s'ha buidat menys d'un 10% de cache amb l'optimitzacio => Hi ha poca contiguitat de
								// dades, i fent aixo aconseguirem buidar una mica mes, si es q es pot
								if (bl->operation==Cache::write)
								{
									// bl es write, fem un offset enrera de les dades de bl2
									fileOffset extraOffset=bl->offset+bl->size-bl2->offset;
									memmove(bl2->data, ((byte*)bl2->data)+extraOffset, bl2->size-extraOffset);
									bl2->size-=extraOffset;
									
									// c) convertim el write en read i a la seguent iteracio podra joinar-se
									// Com que ja haurem fet els joins de tipus iguals, el callback es fa un sol cop
									// per cada segment contigu
									lruCallback(this, bl);
									bl->operation=Cache::read;
								}
								else
								{
									// bl2 es write, com que esta darrera, nomes cal encongir bl
									bl->size=bl2->offset-bl->offset;

									// c) convertim el write en read i a la seguent iteracio podra joinar-se
									// Com que ja haurem fet els joins de tipus iguals, el callback es fa un sol cop
									// per cada segment contigu
									lruCallback(this, bl2);
									bl2->operation=Cache::read;
								}

								modified=true;
							}
						}
						// Bloc 1 "darrera" del bloc 2 (inclus amb overlapping)
						else if (bl->offset>=bl2->offset && bl->offset<=bl2->offset+bl2->size)
						{
							// a) Merges
							if (bl->operation==bl2->operation && bl->offset-bl2->offset+bl->size<=mergeMaxSize)
							{
								fileOffset blockSize=bl->offset-bl2->offset+bl->size;
								// Pot ser q bl estigui totalment contingut
								if (blockSize<bl2->size)
									blockSize=bl2->size;
							
								byte *dades=new byte[blockSize];
								if (dades==NULL)
									continue;
								
								memmove(dades, bl2->data, bl2->size);
								memmove(dades+(bl->offset-bl2->offset), bl->data, bl->size);
								
								byte *oldData=(byte*)bl->data;
								bl->data=dades;
								bl->size=blockSize;
								bl->offset=bl2->offset;
//								cout << "Cache::optimize delete 3" << endl;
								if (oldData!=NULL)
									delete [] oldData;
//								cout << "Cache::optimize delete 4" << endl;
								if (bl2->data!=NULL && bl2->size!=0)
								{
									delete [] (byte*)bl2->data;
									delete bl2->lastUse;
								}
								memset(bl2, 0, sizeof(Cache::block));
								bl->lastUse->stop();
								bl->lastUse->start();

//								cout << "Cache::optimize merge, final size: " << bl->size << endl;

								totalFreed++;
								modified=true;
							}
							else if (nIt>1 && totalFreed*10<nBlocks && bl->operation!=bl2->operation && bl->offset-bl2->offset+bl->size<=mergeMaxSize)
							{
								// b) Solapaments amb tipus diferents...
								// Nomes treballem els tipus diferents a partir de la segona iteracio i si
								// s'ha buidat menys d'un 10% de cache amb l'optimitzacio => Hi ha poca contiguitat de
								// dades, i fent aixo aconseguirem buidar una mica mes, si es q es pot
								if (bl2->operation==Cache::write)
								{
									// bl2 es write, fem un offset enrera de les dades de bl
									fileOffset extraOffset=bl2->offset+bl2->size-bl->offset;
									memmove(bl->data, ((byte*)bl->data)+extraOffset, bl->size-extraOffset);
									bl->size-=extraOffset;

									// c) convertim el write en read i a la seguent iteracio podra joinar-se
									// Com que ja haurem fet els joins de tipus iguals, el callback es fa un sol cop
									// per cada segment contigu
									lruCallback(this, bl2);
									bl2->operation=Cache::read;
								}
								else
								{
									// bl es write, com que esta darrera, nomes cal encongir bl2
									bl2->size=bl->offset-bl2->offset;

									// c) convertim el write en read i a la seguent iteracio podra joinar-se
									// Com que ja haurem fet els joins de tipus iguals, el callback es fa un sol cop
									// per cada segment contigu
									lruCallback(this, bl);
									bl->operation=Cache::read;
								}

								modified=true;
							}
						}
					}
				}
			}
			
			if (bl->operation==Cache::write && bl->size>((Cache::mergeMaxSize*10)/8))
			{
				// Si esta ple per sobre el 80% ja li fem el flush
				lruCallback(this, bl);
				bl->operation=Cache::read;
			}
		}
//		cout << "modified: " << modified << endl;
	}
//	cout << "<-- optimize()" << endl;
}

Cache::Cache(CacheLRUCallback callback, dword nBlocks) : nBlocks(nBlocks), lruCallback(callback),
	nHits(0), nMisses(0)
{
	this->nextOptimize=Cache::defaultSize*3;
	this->blocks=new Cache::block[this->nBlocks];
	
	if (this->blocks==NULL)
		throw CacheException(0, "Not enough memory to allocate blocks for cache");
		
	memset(this->blocks, 0, sizeof(Cache::block)*this->nBlocks);
}

Cache::~Cache()
{
	if (this->blocks!=NULL)
	{
		empty();
		delete [] this->blocks;
		this->blocks=NULL;
	}
}

void Cache::add(void *data, dword size, fileOffset off, Cache::operation operation)
{
	blocksLock.lock();
	Cache::block *bl=getFreeBlock();
//	cout << "add post getFree" << endl;
	
	bl->operation=operation;
	bl->size=size;
	bl->offset=off;
	bl->lastUse=new Timer();
	bl->lastUse->stop();	
	bl->lastUse->start();	
	bl->data=new byte[size];
	if (bl->data==NULL)
	{
		blocksLock.unlock();
		throw CacheException(0, "Not enough memory to allocate data");
	}
	memmove(bl->data, data, size);
	
	this->nextOptimize--;
	if (this->nextOptimize==0)
		optimize();
	blocksLock.unlock();
}

void* Cache::get(fileOffset off, dword size)
{
	blocksLock.lock();
	Cache::block *bl=searchBlock(off, size);
	
	if (bl==NULL)
	{
		blocksLock.unlock();
		return NULL;
	}
	
	fileOffset blOff=off-bl->offset;
	
	byte *data=new byte[size];
	cout<<" get: "<<size<<"    "<<(void*)data<<endl;
	if (data==NULL)
	{
		blocksLock.unlock();
		throw CacheException(0, "Not enough memory to allocate data");
	}
	
	memmove(data, ((byte*)bl->data)+blOff, size);
	
	blocksLock.unlock();
	return data;
}

void Cache::flush()
{
	blocksLock.lock();
	for (dword b=0; b<nBlocks; b++)
	{
		if (blocks[b].data!=NULL && blocks[b].operation==Cache::write)
		{
			lruCallback(this, &blocks[b]);
			blocks[b].operation=Cache::read;
		}
	}
	
//	nHits=0;
//	nMisses=0;
	blocksLock.unlock();
}

void Cache::empty()
{
	blocksLock.lock();
	for (dword b=0; b<nBlocks; b++)
	{
		if (blocks[b].data!=NULL)
		{
			if (blocks[b].operation==Cache::write)
			{
				lruCallback(this, &blocks[b]);
				blocks[b].operation=Cache::read;
			}
	
			delete [] (byte*)blocks[b].data;
			delete blocks[b].lastUse;
			
			memset(&blocks[b], 0, sizeof(Cache::block));
		}
	}
	
	nHits=0;
	nMisses=0;
	blocksLock.unlock();
}

qword Cache::getHits()
{
	return nHits;
}

qword Cache::getMisses()
{
	return nMisses;
}

float Cache::getHitRatio()
{
	if (nHits+nMisses==0)
		return 1.0f;
		
	return ((float)nHits)/((float)(nHits+nMisses));
}

