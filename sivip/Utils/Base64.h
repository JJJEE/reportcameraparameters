#ifndef __SIRIUS__BASE__UTILS__BASE64_H
#define __SIRIUS__BASE__UTILS__BASE64_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <string>
using namespace std;

string base64encode(string decoded);
string base64decode(string encoded);

#endif
