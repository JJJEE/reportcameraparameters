#ifndef __SIVIP__BASE__UTILS__DEVICEAUTHENTICATION_H
#define __SIVIP__BASE__UTILS__DEVICEAUTHENTICATION_H

#include <XML/XML.h>
#include <XML/xmlNode.h>
#include <Utils/StrUtils.h>
#include <Exceptions/InvalidParameterException.h>

#include <string>

using namespace std;

class DeviceAuthentication
{
public:
	struct AuthInfo
	{
		string username, password;
	};
	
protected:
	map<int, DeviceAuthentication::AuthInfo> authById;
	
public:
	DeviceAuthentication(xmlNode *config);
	virtual ~DeviceAuthentication();
	
	DeviceAuthentication::AuthInfo getAuthByCamId(int id);
	
};

#endif
