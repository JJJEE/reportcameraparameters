#ifndef __SIRIUS__BASE__UTILS__RPC_H
#define __SIRIUS__BASE__UTILS__RPC_H

//#include <Sockets/SocketUDP.h>
#include <Utils/debugNew.h>
//#include <Sockets/Address.h>
//#include <Threads/Mutex.h>
#include <Threads/Thread.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPCException.h>

#include <map>
#include <string>
#include <list>

using namespace std;

class Gestor;
class Mutex;
class SocketUDP;
class Socket;
class RPC
{
	friend class Gestor;

protected:
	//Address orig;
	SocketUDP *fwdSocket;
	bool asyncPending;
	int timeoutms;

	static int maxPk;
	// Timeout per rebre paquets d'un mateix origen. Si en un aquest temps (ms)
	// no rebem paquets d'un mateix origen, timeout :)
	static int defaultOriginTimeout;
	static int maxOriginTimeout;

	static Mutex *packetsLock;
	static map <string, map <string, list<RPCPacket*>* >* >
		packetsBySourceByLocalAddress;

	static map<string,list<RPCPacket*>* > * 
		getPacketsForLocalAddress(Address *la);
	static list<RPCPacket*>*
		getPacketsForLocalAddressAndSource(Address *la, Address *src);
	static void queuePacketForLocalAddressAndSource(RPCPacket *p, Address *la,
		Address *src);

	// Enviar un paquet, segmentant si cal, sense mes connotacions.
	static void sendPacket(SocketUDP &s, RPCPacket &r, Address *to=NULL);
	
public:
	SocketUDP *socket;
	RPC(Address addr);
	~RPC();
	//void* execute(int id, void *params);

	// Timeouts
	void setTimeout(int ms);
	int getTimeout();
	
	// Crida sincrona estandar
	RPCPacket* call(RPCPacket &r, Address *fwdKeepAlive=NULL);

	// Recepcio d'un RPCPacket individual, independentment de si es un tros d'un RPCPacket mes gran segmentat o no.
	static RPCPacket* receiveIndividualPacket(Socket *s, Address *from=NULL);

	// Recepcio d'una crida
	static RPCPacket* receivePacket(Socket *s, Address *from=NULL, Socket **fwdSocket=NULL, Address *fwdKeepAlive=NULL, int sourceTimeout=0);

	// Enviar resposta
	static void sendResponse(RPCPacket &r, Address *to, SocketUDP *s=NULL);

	// Crida asincrona estandar
	void callAsync(RPCPacket &r);
	RPCPacket* getAsyncResult(Address *fwdKeepAlive=NULL);
	void ignoreAsyncResult();
	
	// keepAlives
	static void keepAlive(Address *addr, short type);

	// redireccions
	static void redirect(Address *who, Address *where);

	// Canvi de destinacio de les crides
	void setDefaultDestination(Address &a);
	Address getDefaultDestination();
	
	// Ajust de tamanys de buffer
	virtual dword setRecvBuffer(dword bytes);
	virtual dword setSendBuffer(dword bytes);
	Address getLocalAddr();
};
#endif
