#include <Utils/Types.h>
#include <Utils/RPC.h>
#include <Utils/SerializedException.h>
#include <Utils/StrUtils.h>
#include <Utils/RedirectCallException.h>
#include <Sockets/SocketTimeoutException.h>
#include <iostream>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <Utils/debugStackTrace.h>
#ifdef WIN32
#include <Utils/windowsDefs.h>
#endif
#ifdef RPC_DEBUG
#include <WorkerThreads/WorkerThreadPool.h>
#include <WorkerThreads/WorkerThreadPool>
#include <Module/Module.h>
#endif //RPC_DEBUG
using namespace std;

#pragma mark *** Estatiques
int RPC::maxPk=-1;
int RPC::defaultOriginTimeout=4000;
int RPC::maxOriginTimeout=15000;
Mutex *RPC::packetsLock=new Mutex();
map <string, map <string, list<RPCPacket*>* >* > 
	RPC::packetsBySourceByLocalAddress;

map<string,list<RPCPacket*>* > * RPC::getPacketsForLocalAddress(Address *la)
{
	RPC::packetsLock->lock();
	map <string, map <string, list<RPCPacket*>* >* >::iterator mIt;
	mIt=RPC::packetsBySourceByLocalAddress.find(la->toString());
	
	if (mIt == RPC::packetsBySourceByLocalAddress.end())
	{
		RPC::packetsLock->unlock();
		return NULL;
	}
	
	RPC::packetsLock->unlock();
	return mIt->second;
}

list<RPCPacket*>* RPC::getPacketsForLocalAddressAndSource
	(Address *la, Address *src)
{
	map<string,list<RPCPacket*>* > *pksForLocalAddr = 
		RPC::getPacketsForLocalAddress(la);
		
	if (pksForLocalAddr == NULL)
		return NULL;

	RPC::packetsLock->lock();
	
	map<string,list<RPCPacket*>* >::iterator mIt;

	mIt=pksForLocalAddr->find(src->toString());
	
	if (mIt == pksForLocalAddr->end())
	{
		RPC::packetsLock->unlock();
		return NULL;
	}
	
	RPC::packetsLock->unlock();
	return mIt->second;
}

void RPC::queuePacketForLocalAddressAndSource(RPCPacket *p, Address *la,
	Address *src)
{
	RPC::packetsLock->lock();
	map <string, list<RPCPacket*>* > *pksForLocalAddr=NULL;
	list<RPCPacket*> *pksForLocalAddrAndSource=NULL;
	
	map <string, map <string, list<RPCPacket*>* >* >::iterator mIt;
	mIt=RPC::packetsBySourceByLocalAddress.find(la->toString());
	
	if (mIt == RPC::packetsBySourceByLocalAddress.end())
	{
		pksForLocalAddr = new map <string, list<RPCPacket*>* >();
		RPC::packetsBySourceByLocalAddress[la->toString()] = pksForLocalAddr;
	}
	else
		pksForLocalAddr = mIt->second;
	
	map <string, list<RPCPacket*>* >::iterator srcIt;
	srcIt=pksForLocalAddr->find(src->toString());

	if (srcIt == pksForLocalAddr->end())
	{
		pksForLocalAddrAndSource = new list<RPCPacket*>();
		(*pksForLocalAddr)[src->toString()]=pksForLocalAddrAndSource;
	}
	else
		pksForLocalAddrAndSource = srcIt->second;
	
	pksForLocalAddrAndSource->push_back(p);
	
	RPC::packetsLock->unlock();
}

#pragma mark *** Metodes
// Enviar un paquet
void RPC::sendPacket(SocketUDP &s, RPCPacket &r, Address *to)
{
	STACKTRACE_INSTRUMENT();
	
	byte *pkt=r.packet();
	dword dataSize=r.getSize()+RPCPacket::hdrSize;
	if (RPC::maxPk<0)
		RPC::maxPk=s.getMaxPacketSize();
//	cout  << "RPC::sendPacket:" << dataSize <<" bytes, max:"<<RPC::maxPk<< endl;
	if (dataSize > RPC::maxPk)
	{
//		cout  << "dataSize (" << dataSize << ") too big, segmenting " << endl;

		// A segmentar
		RPCPacket::segmentationInfo segInfo;
		dword sizePerPacket=RPC::maxPk-RPCPacket::hdrSize-segInfo.serializationSize();
		segInfo.nPackets=dataSize/sizePerPacket+((dataSize%sizePerPacket)!=0?1:0);
		segInfo.totalSize=dataSize;
//		byte *segData=new byte[sizePerPacket+sizeof(segInfo)];
//
//		RPCPacket r2(*r.a, RPCPacket::segmentedPacketId, segData, sizePerPacket+sizeof(segInfo), r.origen, r.desti);
//		byte *pkt2=r2.packet();
//			
//		delete [] segData;

		byte *segData=NULL;
		// Com que no copia no passa res pq segData sigui NULL
		RPCPacket r2(*r.a, RPCPacket::segmentedPacketId, segData, sizePerPacket+segInfo.serializationSize(), r.origen, r.desti);
		// Com que no copia no passa res pq segData sigui NULL
		byte *pkt2=r2.packet(NULL, NULL, false);
		
		segData=pkt2+RPCPacket::hdrSize;
		
//		cout  << "Segmenting in " << sizePerPacket << " bytes chunks" << endl;
		
		for (int i=0; i<segInfo.nPackets; i++)
		{
			segInfo.offset=i*sizePerPacket;
			dword size=(i<segInfo.nPackets-1?sizePerPacket:segInfo.totalSize-segInfo.offset);
			
			dword segSize=size+segInfo.serializationSize()+RPCPacket::hdrSize;

			// Nomes capçalera
			r2.setSize(size+segInfo.serializationSize());
			pkt2=r2.packet(pkt2, NULL, false);
	
			// cout  << "offset " << segInfo.offset << endl; 
			segInfo.toNetwork(segData);
			// cout  << "offset N " << segInfo.offset << endl; 
//			memmove(segData, &segInfo, sizeof(segInfo));
			// cout  << "offset N " << segInfo.offset << endl; 
//			segInfo.toLocal();
			// cout  << "offset " << segInfo.offset << endl; 
			// cout  << "RPC::sendResponse memmove offset " << segInfo.offset << endl;
			memmove(segData+segInfo.serializationSize(), pkt+segInfo.offset, size);
			// cout  << "RPC::sendResponse done memmove offset " << segInfo.offset << endl;
			
//			 cout  << "RPC::sendResponse writing paket "<<i<<"/"<<segInfo.nPackets<<" : " << segSize << " bytes" << endl;
			int written=0;
			if (to!=NULL)
				written=s.write(*to, pkt2, segSize);
			else
				written=s.write(pkt2, segSize);
			if(written<segSize)
			{
				cout  << "RPC::sendPacket error writing packet segment "<<i<<"/"<<segInfo.nPackets<<" packet , "<<written<<"/"<< segSize << " bytes" << endl;
			}
//			cout  << "RPC::sendResponse wrote " << written << " bytes" << endl;
			
			if((i%(40960/RPC::maxPk))==0 && i!=0)
//			if((i%(10240/RPC::maxPk))==0 && i!=0)
			{//	cout<<" usleep i:"<<i<<endl;
				usleep(5000);		// 5 millis, per buidar buffers
			}// cout  << "RPC::sendResponse deleted pkt2" << endl;
		}
//		if(segInfo.nPackets>1)
//		{
//			cout  << "RPC::sendResponse segmented packet written "<<segInfo.nPackets<<"  =  "<<dataSize<<" bytes" << endl;
//		}
		
		delete [] pkt2;
//		delete [] segData;
	}
	else
	{
//		cout<<" Send Response:";
//		cout<<StrUtils::hexDump(string((char*)r.getData(), r.getSize()))<<endl;
//		cout<< r.a->toString()<<" origen:"<<r.origen<<" dest:"<<r.desti<<" id:"<<r.id<<" size:"<<r.size<<endl<<endl;
		if (to!=NULL)
			s.write(*to, pkt, r.getSize()+RPCPacket::hdrSize);
		else
			s.write(pkt, r.getSize()+RPCPacket::hdrSize);
	}
	delete [] pkt;
}

RPC::RPC(Address addr) : asyncPending(false), timeoutms(15000), fwdSocket(NULL)
{
	STACKTRACE_INSTRUMENT();
	
	socket=new SocketUDP(addr);


	if (socket==NULL)
		throw RPCException(string("Not enough memory"));
}

RPC::~RPC()
{
	STACKTRACE_INSTRUMENT();
	
	if (fwdSocket!=NULL)
		delete fwdSocket;
	
	socket->closeSocket();
	delete socket;
}

void RPC::setTimeout(int ms)
{
	STACKTRACE_INSTRUMENT();
	
	this->timeoutms=ms;
}

int RPC::getTimeout()
{
	STACKTRACE_INSTRUMENT();
	
	return this->timeoutms;
}

//void* Gestor::execute(int id, void *params)
RPCPacket* RPC::call(RPCPacket &r, Address *fwdKeepAlive)
{
	STACKTRACE_INSTRUMENT();
#ifdef RPC_DEBUG
	int *debug = WorkerThreadPool<ModuleServiceThread>::getMyDebugPtr();
	bool doDebug = (debug != NULL);
#endif //RPC_DEBUG
	
#ifdef RPC_DEBUG
	if(doDebug)
		*debug = 10000;
#endif //RPC_DEBUG
	 //cout  << "    RPC::call 1"<< endl;
	callAsync(r);
	 //cout  << "    RPC::call 2"<< endl;
	
	RPCPacket *res;
	try
	{
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 20000;
#endif //RPC_DEBUG
		//cout<<" RPC::getAsyncResult"<<endl;;
		res=getAsyncResult(fwdKeepAlive);
		//cout<<" RPC::getAsyncResult getIP: " << res->a->getIP().toString()<<endl;
	}
	catch (std::exception &stde)
	{
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 30000;
#endif //RPC_DEBUG
		asyncPending=false;
		//e.serialize()->materializeAndThrow(true);
		cout<<"getAR Exception:"<<stde.what()<<endl;
		throw;
	}
	catch(Exception &e)
	{
		cout<<"RPC:: catch e"<<endl;
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 30000;
#endif //RPC_DEBUG
		asyncPending=false;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
//	 cout  << "    RPC::call 3"<< endl;
#ifdef RPC_DEBUG
	if(doDebug)
		*debug = 40000;
#endif //RPC_DEBUG
	return res;
}

// Recepcio d'un RPCPacket individual, independentment de si es un tros d'un RPCPacket mes gran segmentat o no.
RPCPacket* RPC::receiveIndividualPacket(Socket *s, Address *from)
{
	STACKTRACE_INSTRUMENT();

#ifdef RPC_DEBUG
	int *debug = WorkerThreadPool<ModuleServiceThread>::getMyDebugPtr();
	bool doDebug = (debug != NULL);
#endif //RPC_DEBUG
	
	RPCPacket *p=NULL;
	byte *pktBuf=NULL;
	byte *hdrBuf=NULL;

	hdrBuf=new byte[RPCPacket::hdrSize];
	if (hdrBuf==NULL)
	{
		throw (RPCException("Not enough memory to receive packet in header allocation"));
	}

	int peekRes=0;

#ifdef RPC_DEBUG
	if(doDebug)
		*debug = 10000000;
#endif //RPC_DEBUG
	
	try
	{
//		cout  << "RPC::receiveIndividualPacket checkpoint 2: peek" << endl;
		if ((peekRes=s->peek(hdrBuf, RPCPacket::hdrSize))==RPCPacket::hdrSize)
		{
//			cout  << "RPC::receiveIndividualPacket checkpoint 3" << endl;
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 20000000;
#endif //RPC_DEBUG
		
			p=new RPCPacket(hdrBuf);
			if (p==NULL)
			{
				delete [] hdrBuf;
				hdrBuf=NULL;
				throw (RPCException("Not enough memory to receive packet"));
			}
		
			delete [] hdrBuf;
			hdrBuf=NULL;
			
			dword pkDataSize=p->getSize();
			if (pkDataSize >= 16777216)
			{
				delete p;
				p=NULL;
	//			cout << "RPC::receiveIndividualPacket: Piligro! Size " << p->getSize() << endl;
				throw RPCException(0, "Piligro big sais");
			}
	
			pktBuf=new byte[RPCPacket::hdrSize+pkDataSize];
	
			if (pktBuf==NULL)
			{
				delete p;
				p=NULL;
				throw (RPCException("Not enough memory to receive packet"));
			}
			
			int expectedRBytes=RPCPacket::hdrSize+pkDataSize;
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 30000000;
#endif //RPC_DEBUG
//			cout  << "RPC::receiveIndividualPacket checkpoint 4 read" << endl;
			int rBytes=s->read(pktBuf, expectedRBytes, from);
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 40000000;
#endif //RPC_DEBUG
			
			if (rBytes!=expectedRBytes)
			{
				cout << "RPC::receiveIndividualPacket: read " << rBytes << " bytes, expected " << expectedRBytes << endl;
			}		
	
			if (pkDataSize > 0)
				p->setData(pktBuf+RPCPacket::hdrSize, pkDataSize, true);
	
			delete [] pktBuf;
			pktBuf=NULL;
		}
		else if (peekRes<0)
		{
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 50000000;
#endif //RPC_DEBUG
			delete [] hdrBuf;
			hdrBuf=NULL;
//			cout  << "RPC::receiveIndividualPacket peta el peeeeeeeeeeek" << endl;
			throw (RPCException(string("Invalid peek receiving RPCPacket (errno ")+StrUtils::decToString(s->getLastErrno())+string(", ret ")+StrUtils::decToString(peekRes)+string(")")));
		}
		else
		{
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 60000000;
#endif //RPC_DEBUG
			cout << "Strange RPCPacket received. length: " << StrUtils::decToString(peekRes) << endl;
			int rBytes=s->read(hdrBuf, RPCPacket::hdrSize, from);
			cout << "Read " << StrUtils::decToString(rBytes) << "bytes" << endl;
			cout << StrUtils::hexDump(string((char*)hdrBuf, rBytes)) << endl;
			cout << "Retrying recursively..." << endl;
			delete [] hdrBuf;
			hdrBuf=NULL;
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 70000000;
#endif //RPC_DEBUG
			p=RPC::receiveIndividualPacket(s, from);
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 80000000;
#endif //RPC_DEBUG
		}
	}
	catch (Exception &e)
	{
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 90000000;
#endif //RPC_DEBUG
		cout << __FILE__ << "(line " << __LINE__ << "): " << e.getClass() <<
			": " << e.getMsg() << endl;
		if (hdrBuf!=NULL)
		{
			cout << __FILE__ << "(line " << __LINE__ << "): hdrBuf!=NULL"
				<< endl;
			delete [] hdrBuf;
			hdrBuf=NULL;
		}

		if (p!=NULL)
		{
			delete p;
			p=NULL;
		}

		if (pktBuf!=NULL)
		{
			delete [] pktBuf;
			pktBuf=NULL;
		}
		
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 95000000;
#endif //RPC_DEBUG
		//e.serialize()->materializeAndThrow(true);
		throw;
	}

#ifdef RPC_DEBUG
	if(doDebug)
		*debug = 99000000;
#endif //RPC_DEBUG
	
	return p;
}

// Recepcio d'una crida
RPCPacket* RPC::receivePacket(Socket *s, Address *from, Socket **fwdSocket, Address *fwdKeepAlive, int sourceTimeout)
{
	STACKTRACE_INSTRUMENT();
	
#ifdef RPC_DEBUG
	int *debug = WorkerThreadPool<ModuleServiceThread>::getMyDebugPtr();
	bool doDebug = (debug != NULL);
#endif //RPC_DEBUG
	
#ifdef RPC_DEBUG
	if(doDebug)
		*debug = 1000000;
#endif //RPC_DEBUG
//	 cout  << "RPC::receivePacket checkpoint 0" << endl;
	RPCPacket *p=NULL;
	byte *pktBuf=NULL;
	byte *response=NULL;
	byte *segData=NULL;
	dword pktsRecvd=0;
	dword nPktsToReceive=1;
	dword currentSegDataLen=0;
	int size=0;
	
	Address recvFrom;
	if (from==NULL)
		from = &recvFrom;

	dword originTimeoutMS=sourceTimeout;
	if (originTimeoutMS==0)
		originTimeoutMS=s->getTimeout();
	if (originTimeoutMS==0 || originTimeoutMS>maxOriginTimeout)
		originTimeoutMS = maxOriginTimeout; //deixém el per defecte
		
	Timer originTimeout;
	string currentOrigin("");

	list<RPCPacket *> *currentPacketQueue=NULL;
	
	// Comencem mirant si hi ha algun packet pel nostre socket a la cua
	Address sockLocalAddr=s->getLocalAddr();
	//return NULL;
//		cout << "RPC::receivePacket[" << sockLocalAddr.toString() << "] - receiving " << endl;
#ifdef RPC_DEBUG
	if(doDebug)
		*debug = 1500000;
#endif //RPC_DEBUG
	map<string,list<RPCPacket*>* > *socketPackets = 
		RPC::getPacketsForLocalAddress(&sockLocalAddr);
	
//	cout<<"socketPackets:"<<(void*)socketPackets<<endl;
	if (socketPackets!=NULL)
	{
		// Iterem per veure si trobem un origen que tingui packets...
		map<string,list<RPCPacket*>* >::iterator sPkIt;

		for (sPkIt=socketPackets->begin(); sPkIt!=socketPackets->end(); sPkIt++)
		{
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 2000000;
#endif //RPC_DEBUG
			currentPacketQueue = sPkIt->second;
		
			if (currentPacketQueue!=NULL && !currentPacketQueue->empty())
			{
#ifdef RPC_DEBUG
				if(doDebug)
					*debug = 2100000;
#endif //RPC_DEBUG
				currentOrigin=sPkIt->first;

				// L'origen que tenim l'hem llegit de la llista i l'hem
				// d'aplicar a from :P
				list<string> comps = StrUtils::split(currentOrigin,
					string(":"));
					
//				cout << "Got origin: " << currentOrigin << ", " <<
//					comps.size() << " components" << endl;
				if (comps.size() == 2)
				{
//					cout << "Setting origin to " << currentOrigin << endl;
					string ip=comps.front();
					word port=(word)atoi(comps.back().c_str());
					
					Address orig(IP(ip), port);
					*from = orig;
					
					// Si ens han passat from per parametre, ens guardem tb
					// a recvFrom, per fer el filtrat de recepcio
					if (from != &recvFrom)
						recvFrom = orig;
				}

				break;
			}
			else
				currentPacketQueue=NULL;
		}
		
//		if (currentOrigin.length()>0)
//			cout << "RPC::receivePacket[" << sockLocalAddr.toString()
//				<< "] Using queue from " << currentOrigin << ", "
//				<< currentPacketQueue->size() << " packets" << endl;
	}
	
	// el keepalive ja es fa amb el comptador, ja que si el paquet quye rebem es de keepalive, pos no incrementem
	try
	{
		originTimeout.start();
		while (pktsRecvd<nPktsToReceive)
		{
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 3000000;
#endif //RPC_DEBUG
			TimerInstant originTimeoutElapsed = originTimeout.time();
//			cout  << "RPC::receivePacket[" << sockLocalAddr.toString() << "] - "
//				<< pktsRecvd << "/"
//				<< nPktsToReceive << " - oT: " << originTimeoutElapsed.seconds()
//				<< endl;
			if (originTimeoutMS > 0
				&& currentOrigin.length() > 0
				&& ((int)(originTimeoutElapsed.seconds()*1000.0)) > originTimeoutMS)
			{
				// Si ens passem mes de RPC::defaultOriginTimeout milisegons
				// sense rebre un paquet pel nostre origen, malament :P
				cout << string("SocketTimeoutException: Timeout while waiting "
					"for packets from ") << currentOrigin << endl;
#ifdef RPC_DEBUG
				if(doDebug)
					*debug = 3100000;
#endif //RPC_DEBUG
				throw SocketTimeoutException(string("Timeout while waiting for"
					" packets from ") + currentOrigin);
			}
		
//			cout  << "RPC::receivePacket checkpoint 2 ("<< pktsRecvd << "/" << nPktsToReceive << ")" << endl;
		
			// Primer mirem si tenim paquets esperant a la cua
			RPCPacket *queuePacket=NULL;

			RPC::packetsLock->lock();
			if (currentPacketQueue!=NULL && !currentPacketQueue->empty())
			{
#ifdef RPC_DEBUG
				if(doDebug)
					*debug = 3200000;
#endif //RPC_DEBUG
				queuePacket = currentPacketQueue->front();
				currentPacketQueue->pop_front();
			}
			RPC::packetsLock->unlock();
			
			if (queuePacket!=NULL)
			{
#ifdef RPC_DEBUG
				if(doDebug)
					*debug = 3300000;
#endif //RPC_DEBUG
				p=queuePacket;
//				cout << "RPC::receivePacket[" << sockLocalAddr.toString() <<
//					"] - receiving from queue ("<< currentOrigin << ")" << endl;
				originTimeout.start();
				if (currentOrigin != from->toString())
				{
#ifdef RPC_DEBUG
					if(doDebug)
						*debug = 3400000;
#endif //RPC_DEBUG
					// L'origen que tenim l'hem llegit de la llista i l'hem
					// d'aplicar a from :P
					list<string> comps = StrUtils::split(currentOrigin,
						string(":"));
						
					if (comps.size() == 2)
					{
//						cout << "Setting origin to " << currentOrigin << endl;
						string ip=comps.front();
						word port=(word)atoi(comps.back().c_str());
						
						Address orig(IP(ip), port);
						*from = orig;
					
						// Si ens han passat from per parametre, ens guardem tb
						// a recvFrom, per fer el filtrat de recepcio
						if (from != &recvFrom)
							recvFrom = orig;
					}
				}
			}
			else
			{
				p=NULL;
//				cout << "RPC::receivePacket[" << sockLocalAddr.toString()
//					<< "] - receiving from socket (" << currentOrigin << ")"
//					<< endl;
#ifdef RPC_DEBUG
				if(doDebug)
					*debug = 3500000;
#endif //RPC_DEBUG
				p=RPC::receiveIndividualPacket(s, &recvFrom);
//				cout<<"rpc::receivePacker recvd:"<<(void*)p<<endl;
#ifdef RPC_DEBUG
				if(doDebug)
					*debug = 3550000;
#endif //RPC_DEBUG
	
				if (p==NULL)
				{
//					cout << "RPC::receivePacket[" << sockLocalAddr.toString()
//						<< "] - receiving from socket ("
//						<< currentOrigin << ") - continue" << endl;
					continue;
				}
				
				// Comprovem origens, sempre que el paquet no sigui keepalive
				// Si encara no tenim un origen, settem
				//Si el paquet no ve d'on toca, l'encuem, sempre que no sigui
				// un KeepAlive
//				cout<<"rpc::receivePacker recvd id:"<<p->id<<endl;
				if (p->id != RPCPacket::keepAlivePacketId)
				{
//				cout<<"rpc::receivePacker recvd origin:"<<p->id<<endl;
					if (currentOrigin.length()==0)
					{
#ifdef RPC_DEBUG
						if(doDebug)
							*debug = 3600000;
#endif //RPC_DEBUG
//						cout << "RPC::receivePacket["
//							<< sockLocalAddr.toString()
//							<< "] - receiving from socket (" << currentOrigin
//							<< ") - currentOrigin.length()==0" << endl;

						// Si ens l'han passat per parametre, apliquem :P
						if (from != &recvFrom)
							*from = recvFrom;
							
						currentOrigin = recvFrom.toString();
						originTimeout.start();
					}
					else if (recvFrom.toString() != currentOrigin)
					{
#ifdef RPC_DEBUG
						if(doDebug)
							*debug = 3700000;
#endif //RPC_DEBUG
//						cout << "RPC::receivePacket["
//							<< sockLocalAddr.toString()
//						<< "] - receiving from socket (" << currentOrigin
//							<< ") - currentOrigin != from ("
//							<< from->toString() << ")" << endl;
						RPC::queuePacketForLocalAddressAndSource(p, 
							&sockLocalAddr, &recvFrom);
						// I a esperar un altre paquet
						p=NULL;
						continue;
					}
					else
					{
#ifdef RPC_DEBUG
						if(doDebug)
							*debug = 3800000;
#endif //RPC_DEBUG
//						cout << "RPC::receivePacket["
//							<< sockLocalAddr.toString()
//							<< "] - receiving from socket (" << currentOrigin
//							<< ") - currentOrigin == from ("
//							<< from->toString() << endl;
						// Quan tenim origen per primer cop o un paquet de
						// l'origen que estem esperant, reiniciem el timer
						originTimeout.start();
						// Obtenim un punter a la llista per si ens venen
						// paquets d'altres threads que estiguin rebent
						currentPacketQueue =
					RPC::getPacketsForLocalAddressAndSource(&sockLocalAddr,
								&recvFrom);
					}
				}
			}
				
			switch (p->id)
			{
				case RPCPacket::keepAlivePacketId:					
					// Si rebem un keepalive hem de reiniciar el timeout
#ifdef RPC_DEBUG
					if(doDebug)
						*debug = 3900000;
#endif //RPC_DEBUG
					if(pktsRecvd == 0)
						originTimeout.start();

					if(fwdKeepAlive!=NULL && fwdSocket!=NULL)
					{
//						 cout  << "\tforwarding keepAlive" << endl;
					
						if (*fwdSocket!=NULL && (*fwdSocket)->getAddr().toString()!=fwdKeepAlive->toString())
						{
							(*fwdSocket)->closeSocket();
							delete *fwdSocket;
							*fwdSocket=NULL;
						}
						
						if (*fwdSocket==NULL)
							*fwdSocket=new SocketUDP(*fwdKeepAlive);
							
						byte *b=p->packet(&size);
						(*fwdSocket)->write(b, size);
						delete [] b;
					}
					delete p;
					p=NULL;
					break;
	
				case RPCPacket::redirectionPacketId:
					{
						Address addr=*p->a;
		
						delete p;
						p=NULL;

//						cout<<"RPCPacket::redirectionPacketId received, throwing rcExc"<<endl;
						throw RedirectCallException(string("Redirect: ")+addr.toString(), addr);
					}
					break;
				
				case RPCPacket::segmentedPacketId:
				{
#ifdef RPC_DEBUG
					if(doDebug)
						*debug = 3910000;
#endif //RPC_DEBUG
//					cout  << "RPC::receivePacket checkpoint 4" << endl;
		
					RPCPacket::segmentationInfo *segInfo=p->extractSegmentationInfo();
	
					// Sembla que de vegades rebem packets invalids (que no son
					// segmentats, o que venen amb dades malament (igual no venen
					// de l'origen correcte o alguna cosa...)
					// Fem alguns controls, en 64 bits per si tenim valors grans
					qword totSegSize=segInfo->totalSize;
					qword segOff=segInfo->offset;
					qword nPkt=segInfo->nPackets;
					
					if (nPkt >= 0x3000 // mes de 12288 paquets?!?!? (>16MB dades)
						|| segOff + (qword)p->getSize() - 
						(qword)segInfo->serializationSize() > totSegSize // Offset invalid
						)
					{
						cout << "RPC::receivePacket: Invalid segmented packet "
							"received. IGNORING. (" << pktsRecvd << "/" <<
							nPktsToReceive << ")" << endl;
				
						cout << "RPCPacket::receivePacket: " << (void*)segInfo << ", " << segInfo->offset << ", " << segInfo->totalSize << ", " << segInfo->nPackets << ", " << p->getSize()-segInfo->serializationSize() << endl;
						
						if (segInfo!=NULL)
							delete segInfo;
						segInfo=NULL;

						if (p!=NULL)
							delete p;
						p=NULL;
					}
					else
					{
						if (pktsRecvd==0)
						{
//							cout  << "allocing " << segInfo->totalSize << " bytes for full packet" << endl;
							currentSegDataLen=segInfo->totalSize;
							segData=new byte[segInfo->totalSize];
							if (segData==NULL)
							{
								if (p!=NULL)
									delete p;
								p=NULL;
										
								if (segInfo!=NULL)
									delete segInfo;
								segInfo=NULL;

								throw (RPCException("Not enough memory to receive segmented packet"));
							}
							else
								memset(segData, 0, segInfo->totalSize);
						}
		
						// Intentem assemblar. Si el paquet es d'un altre
						// stream, i no assembla, no el comptem.
#ifdef RPC_DEBUG
						if(doDebug)
							*debug = 3920000;
#endif //RPC_DEBUG
						if (p->addToAssembledPacketBuffer(segData,
								currentSegDataLen))
						{
							nPktsToReceive=segInfo->nPackets;
							pktsRecvd++;
						}
						
//						cout  << "RPC::receivePacket checkpoint 4.9 ("<< pktsRecvd << "/" << nPktsToReceive << "), packet size:"<<p->getSize() <<" p:"<<(void*)p<<" segData:"<<(void*) segData<<" offset:"<<segInfo->offset<< endl;
		
						if (segInfo!=NULL)
							delete segInfo;
						segInfo=NULL;
						if (p!=NULL)
							delete p;
						p=NULL;
					}
				}
//				cout  << "RPC::receivePacket checkpoint 4.10 ("<< pktsRecvd << "/" << nPktsToReceive << ")" << endl;
				break;
					
	//			default:
				case RPCPacket::responsePacketId:
				case RPCPacket::exceptionPacketId:
//					cout  << "\tnPackets 1 - default" << endl;
#ifdef RPC_DEBUG
					if(doDebug)
						*debug = 3930000;
#endif //RPC_DEBUG
					if (nPktsToReceive == 1)
						pktsRecvd++;
					break;
	
				default:
//					cout  << "\tnPackets 1 - default" << endl;
#ifdef RPC_DEBUG
					if(doDebug)
						*debug = 3940000;
#endif //RPC_DEBUG
					if (nPktsToReceive == 1)
						pktsRecvd++;
					break;
			}
				
//			 cout  << "RPC::receivePacket checkpoint 5" << endl;
		}
	
//		cout  << "RPC::receivePacket checkpoint 6:("<< pktsRecvd << "/" << nPktsToReceive << ")" << endl;
//		cout  << "RPC::receivePacket checkpoint 6 sd" << endl;
//		cout  << "RPC::receivePacket checkpoint 6.5 sd:"<<(void*)segData << endl;
			
		if (segData!=NULL)
		{
//			 cout  << "RPC::receivePacket checkpoint 7" << endl;
#ifdef RPC_DEBUG
			if(doDebug)
				*debug = 4000000;
#endif //RPC_DEBUG
			p=new RPCPacket(segData);
			if (p==NULL)
				throw (RPCException("Not enough memory to assemble segmented packet"));
			
			dword dataSize=p->getSize();
//			cout << "segData - " << p->a->toString() << ", " << p->id << ", " << p->size << ", " << p->origen << ", " << p->desti << endl;
//			cout << "pre-setData segData" << endl;
			p->setData(segData+RPCPacket::hdrSize, dataSize, true);
//			cout << "post-setData segData" << endl;
			
			delete [] segData;
		}
//			 cout  << "RPC::receivePacket checkpoint 8" << endl;
	}
	catch (Exception &e)
	{
//			 cout  << "RPC::receivePacket Exception checkpoint" << endl;
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 5000000;
#endif //RPC_DEBUG
		if (p!=NULL)
			delete p;
		p=NULL;

#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 6000000;
#endif //RPC_DEBUG
		if (segData!=NULL)
			delete [] segData;
		segData=NULL;
		
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 7000000;
#endif //RPC_DEBUG
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
//	cout<<"rpc::recvPck p:"<<(void*)p<<endl;
//	cout<<"rpc::recvPck sz:"<<p->getSize()<<":"<<(void*)p->getData()<<endl;
//	cout<<"rpc::recvPck return:"<<endl;
	return p;
}


// Enviar una resposta
void RPC::sendResponse(RPCPacket &r, Address *to, SocketUDP *s)
{
	STACKTRACE_INSTRUMENT();
	
	if (s==NULL)
	{
		Address a(to);
		SocketUDP s2(a);
		RPC::sendPacket(s2, r);
		s2.closeSocket();
	}	
	else
		RPC::sendPacket(*s, r, to);
}

void RPC::callAsync(RPCPacket &r)
{
	STACKTRACE_INSTRUMENT();
	
	if (asyncPending)
	{
		throw RPCException(1,string("Asynchronous call already pending"));
	}
	
	// Timeout
	socket->setTimeout(timeoutms);
	
	sendPacket(*socket, r);

//	socket->setTimeout(0);

	asyncPending=true;
}

RPCPacket* RPC::getAsyncResult(Address *fwdKeepAlive)
{
	STACKTRACE_INSTRUMENT();
#ifdef RPC_DEBUG
	int *debug = WorkerThreadPool<ModuleServiceThread>::getMyDebugPtr();
	bool doDebug = (debug != NULL);
#endif //RPC_DEBUG
	
#ifdef RPC_DEBUG
	if(doDebug)
		*debug = 100000;
#endif //RPC_DEBUG
	
	if (!asyncPending)
	{
		throw RPCException(string("No asynchronous call pending"));
	}
		
	// Timeout
	socket->setTimeout(this->timeoutms);
	
	RPCPacket *res=NULL;

	asyncPending=false;
	try
	{
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 200000;
#endif //RPC_DEBUG
		//cout<<"RPC::getAsyncResult->receivePacket"<<endl;
		res=RPC::receivePacket(socket, NULL, (Socket**)&fwdSocket, fwdKeepAlive);
		//cout<<"RPC::getAsyncResult<-receivePacket"<<endl;
	}
	catch(Exception &e)
	{
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 300000;
#endif //RPC_DEBUG
		//e.serialize()->materializeAndThrow(true);
		throw;
	}

	// Mirem si es un error i fem la conversio pertinent a excepcio
	if (res->id==RPCPacket::exceptionPacketId)
	{
		//cout<<"RPC::getAsyncResult -> exception"<<endl;
#ifdef RPC_DEBUG
		if(doDebug)
			*debug = 400000;
#endif //RPC_DEBUG
		//cout<<"RPC::getAsyncResult -> exception data(size)-(data): "<< res->getSize() <<"-"<< res->getData() <<endl;
		for (int i=0; i<res->getSize(); i++){
			cout<<*(res->getData()+i);
		}
		cout<<endl;
		SerializedException se(res->getSize(), res->getData());
		delete res;
		se.materializeAndThrow();
	}
	//cout<<"RPC::getAsyncResult -> return"<<endl;

#ifdef RPC_DEBUG
	if(doDebug)
		*debug = 500000;
#endif //RPC_DEBUG
	return res;
}

void RPC::ignoreAsyncResult()
{
	STACKTRACE_INSTRUMENT();
	
	if (!asyncPending)
	{
		throw RPCException(string("No asynchronous call pending"));
	}
	
	asyncPending=false;
}

void RPC::keepAlive(Address *addr, short type)
{
	STACKTRACE_INSTRUMENT();
	
	RPCPacket r(*addr, RPCPacket::keepAlivePacketId, NULL, 0, type, 0);

	int size;
	
	byte *b=r.packet(&size);
	if (b==NULL)
	{
		throw RPCException(string("Not enough memory"));
	}

	SocketUDP s(*addr);
	s.write(b, size);

	s.closeSocket();
	delete [] b;
}

void RPC::redirect(Address *who, Address *where)
{
	STACKTRACE_INSTRUMENT();
	
	RPCPacket r(*where, RPCPacket::redirectionPacketId, NULL, 0, 0, 0);

	int size;
	
	byte *b=r.packet(&size);
	if (b==NULL)
	{
		throw RPCException(string("Not enough memory"));
	}

	SocketUDP s(*who);
	s.write(b, size);

	s.closeSocket();
	delete [] b;
}

void RPC::setDefaultDestination(Address &a)
{
	if (this->socket!=NULL)
	{
		this->socket->setDefaultDestination(a);
	}
}

Address RPC::getDefaultDestination()
{
	if (this->socket!=NULL)
	{
		return this->socket->getAddr();
	}

	return Address();
}

dword RPC::setRecvBuffer(dword bytes)
{
	if (this->socket!=NULL)
		return this->socket->setRecvBuffer(bytes);
		
	return bytes;
}

dword RPC::setSendBuffer(dword bytes)
{
	if (this->socket!=NULL)
		return this->socket->setSendBuffer(bytes);
		
	return bytes;
}

Address RPC::getLocalAddr()
{
	if (this->socket!=NULL)
		return this->socket->getLocalAddr();
		
	return Address();
}
