#ifndef TYPES_H_
#define TYPES_H_

#include <sys/types.h>
#include <stdint.h>

// Tipus enters

#if !defined(_UINT8_T) && !defined(_UINT8_T_DECLARED)
typedef unsigned char uint8_t;
#endif
#if !defined(_UINT16_T) && !defined(_UINT16_T_DECLARED)
typedef unsigned short uint16_t;
#endif
#if !defined(_UINT32_T) && !defined(_UINT32_T_DECLARED) && !defined(_STDINT)
// TODO: dcasale 25092014: Comento el codigo porque no encuentra la variable 'LINUX'
//#ifdef LINUX
typedef unsigned int uint32_t;
//#else
//typedef unsigned long uint32_t;
//#endif

#endif
#if !defined(_UINT64_T) && !defined(_UINT64_T_DECLARED)
typedef unsigned long long uint64_t;
#endif

#if !defined(_INT8_T) && !defined(_INT8_T_DECLARED)
typedef signed char int8_t;
#endif
#if !defined(_INT16_T) && !defined(_INT16_T_DECLARED)
typedef signed short int16_t;
#endif
#if !defined(_INT32_T) && !defined(_INT32_T_DECLARED) && !defined(_STDINT)
// TODO: dcasale 25092014: Comento el codigo porque no encuentra la variable 'LINUX'
//#ifdef LINUX
typedef signed int int32_t;
//#else
//typedef signed long int32_t;
//#endif
#endif

#if !defined(_INT64_T) && !defined(_INT64_T_DECLARED)
typedef signed long long int64_t;
#endif

typedef uint8_t byte;
typedef uint16_t word;
typedef uint32_t dword;
typedef uint64_t qword;

#ifndef WIN32
typedef int SOCKET;
#endif
typedef off_t fileOffset;

#define SWAP_INT(x,y) (x^=y,y^=x,x^=y)
#define ABS(x) ((x)<0?(-(x)):(x))
#define MIN(x,y) ((x<y)?(x):(y))
#define MAX(x,y) ((x>y)?(x):(y))
#define forever for(;;)

// Tipus de dades de la BD
#define DB_VARCHAR(_VAR,_SIZE) char _VAR[_SIZE+1]

#define RETRY_TIME          30
#define RETRY_TIME_FACT     2
#define RETRY_TIME_MAX      (1024*60)

enum SType {SOCK_CONNECT=0, SOCK_SERVE};
enum SProto {PROTO_TCP=0, PROTO_UDP};

#define CODEC_FOURCC(_c0,_c1,_c2,_c3) ((((dword)_c0)<<24)|(((dword)_c1)<<16)|(((dword)_c2)<<8)|((dword)_c3))

#endif
