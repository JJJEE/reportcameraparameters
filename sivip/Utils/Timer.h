/*
 *  Timer.h
 *  
 *
 *  Created by David Marí Larrosa on 03/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__UTILS__TIMER_H
#define __SIRIUS__BASE__UTILS__TIMER_H
//#pragma message("timer.h->dbgNw")

#include <Utils/debugNew.h>
//#pragma message("timer.h->time")
#ifndef WIN32
#include <sys/time.h>
#else
//#pragma message("timer.h->winDef")
#include <Utils/WindowsDefs.h>
//#include <afxsock.h>
#endif

//#pragma message("timer.h->str")
#include <string>
//#pragma message("/timer.h")

using namespace std;

class TimerInstant
{
protected:
	int fps;
	struct timeval tInstant;
	
public:
	TimerInstant(int fps=25);
	TimerInstant(struct timeval start, struct timeval end, int fps=25);
	
	int useconds();
	double seconds();
	double frames();
	int floorFrames();
	int FPS();
	double getInstantSeconds();
	string getInstant();
};

class Timer
{
protected:

	bool counting;
	int fps;
	struct timeval tStart, tEnd;

public:
	Timer(int fps=25, bool counting=false);

	void start();
	TimerInstant time();
	void stop();
	void pause();
	void resume();
	int FPS(); 
	void setStartSeconds(double s);
	double getStartSeconds();
	double getEndSeconds();
	string getStartSecondsString();
	string getEndSecondsString();
};
#endif
