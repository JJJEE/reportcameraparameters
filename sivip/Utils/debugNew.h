/*
 *  debugNew.h
 *  
 *
 *  Created by David Marí Larrosa on 06/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef __SIRIUS__BASE__UTILS__DEBUGNEW_H
#define __SIRIUS__BASE__UTILS__DEBUGNEW_H

	#ifdef WITH_DEBUG_NEW

		void dumpAllocatedMemoryMap();

		#define NEW_DUMPALLOCATEDMEMORYMAP() dumpAllocatedMemoryMap()

	#else /* WITH_DEBUG_NEW */

		#define NEW_DUMPALLOCATEDMEMORYMAP() ;

	#endif /* WITH_DEBUG_NEW */

#endif
