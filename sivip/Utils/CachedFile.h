/*
 *  CachedFile.h
 *  
 *
 *  Created by David Marí Larrosa on 11/03/08.
 *  Copyright 2008 Marina Eye-Cam Technologies. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__UTILS__CACHEDFILE_H_
#define __SIRIUS__BASE__UTILS__CACHEDFILE_H_

#include <Utils/File.h>
#include <Utils/Types.h>
#include <Threads/Mutex.h>
#include <Endian/Endian.h>
#include <Utils/debugNew.h>
#include <Utils/Cache.h>
//#include <Utils/CacheManager.h>
#include <map>
#include <stdio.h>
#include <sys/types.h>

class CachedFile : public File
{
public:
	static void cacheFlushWrite(Cache *c, Cache::block *bl);

protected:

public://TODO:fora
	
	enum operation {OP_READ=0, OP_WRITE};
	
	static const int blockSize = 256*1024;//16;
	static const int cacheMBSize = 16;
	static const int cacheNumBlocks = cacheMBSize*((1024*1024)/blockSize);

	class block
	{
		public:
			CachedFile::operation operation;
			fileOffset offset;
			Timer lastUse;
			fileOffset size;
			byte *data;

			block() : offset(0), lastUse(NULL), size(0), data(NULL) {};
	};

public://TODO:fora
	map<int, block*> cache;
	
	Mutex blocksLock;
	dword nBlocks;
	//block *blocks;
	
	qword nHits, nMisses;

	// Cache obtinguda del CacheManager
	
	static Mutex constructorMutex;
	
	virtual int uncachedwrite(void *data, dword size=1);

	virtual void freeBlock();
	
//public:
	fileOffset cacheOff;

	CachedFile(const string filename, const bool readOnly=true, const bool newFileRW=false);
	~CachedFile();

	virtual int read(void *data, dword size=1);
	virtual int write(void *data, dword size=1);
	// Per casos xungus de deteccio de tipus, ja que fileOffset es 64bit
	virtual void seek(int off, int from=File::SEEK_FROMSTART);
	virtual void seek(fileOffset off, int from=File::SEEK_FROMSTART);	
	virtual void uncachedSeek(int off, int from=File::SEEK_FROMSTART);
	virtual void uncachedSeek(fileOffset off, int from=File::SEEK_FROMSTART);	
	
//	virtual Cache *getCache();
	virtual qword getHits();
	virtual qword getMisses();
	virtual float getHitRatio();

};
#endif
