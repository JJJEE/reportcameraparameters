/*
 *  debugStackTrace.cpp
 *  
 *
 *  Created by David Marí Larrosa on 07/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef WITH_DEBUG_STACKTRACE
#define WITH_DEBUG_STACKTRACE
#include <Utils/debugStackTrace.h>
#undef WITH_DEBUG_STACKTRACE
#else /* WITH_DEBUG_STACK_TRACE */
#include <Utils/debugStackTrace.h>
#endif /* WITH_DEBUG_STACKTRACE */


#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

#include <iostream>
#include <signal.h>
#include <pthread.h>

using namespace std;

#pragma mark Variables estatiques
pthread_mutex_t *StackTrace::hashMutex=NULL;
StackTrace::stackTraceNode **StackTrace::hashTable=NULL;

#pragma mark Metodes

void StackTrace::sigusr2(int)
{
	StackTrace::dumpAll();
}

dword StackTrace::thHash(pthread_t th)
{
	return ((qword)th)%StackTrace::hashSize;
};

void StackTrace::push(const char *fn, const char *file, int line, bool isExtra)
{
//	cout << "StackTrace::push " << fn << ", " << file << ", " << line << endl;
	pthread_t me=pthread_self();
	qword hashIdx=thHash(me);
	
	// cerr << "pthread_mutex_lock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_lock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
	pthread_mutex_lock(&hashMutex[hashIdx]);
	stackTraceNode *node=hashTable[hashIdx];
	stackTraceNode *prevNode=NULL;

	if (node!=NULL)
	{
		while (node!=NULL && node->thId!=me)
		{
			prevNode=node;
			node=node->hashNext;
		}
	
		if (node!=NULL)
		{
			prevNode=node->stackNext;
			node->stackNext=(stackTraceNode*)malloc(sizeof(stackTraceNode));
			memset(node->stackNext, 0, sizeof(stackTraceNode));
			node=node->stackNext;
			node->stackNext=prevNode;
		}
		else
		{
			prevNode->hashNext=(stackTraceNode*)malloc(sizeof(stackTraceNode));
			memset(prevNode->hashNext, 0, sizeof(stackTraceNode));
			node=prevNode->hashNext;
			node->thId=me;
			prevNode=node->stackNext;
			node->stackNext=(stackTraceNode*)malloc(sizeof(stackTraceNode));
			memset(node->stackNext, 0, sizeof(stackTraceNode));
			node=node->stackNext;
			node->stackNext=prevNode;
		}
	}
	else
	{
		node=(stackTraceNode*)malloc(sizeof(stackTraceNode));
		memset(node, 0, sizeof(stackTraceNode));
		node->thId=me;
		hashTable[hashIdx]=node;
		prevNode=node->stackNext;
		node->stackNext=(stackTraceNode*)malloc(sizeof(stackTraceNode));
		memset(node->stackNext, 0, sizeof(stackTraceNode));
		node=node->stackNext;
		node->stackNext=prevNode;
	}
	
	if (!isExtra || prevNode==NULL)
		node->popCount=1;
	else if (prevNode!=NULL)
		node->popCount=prevNode->popCount+1;
		
	node->function=(char*)malloc(strlen(fn)+1);
	strcpy(node->function, fn);
	node->file=(char*)malloc(strlen(file)+1);
	strcpy(node->file, file);
	node->line=line;
	node->thId=me;

	// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
	pthread_mutex_unlock(&hashMutex[hashIdx]);
}

void StackTrace::pop()
{
	int popCount=2;
	
	while (popCount>1)
	{
		pthread_t me=pthread_self();
		qword hashIdx=thHash(me);
		
		// cerr << "pthread_mutex_lock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_lock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
	pthread_mutex_lock(&hashMutex[hashIdx]);
		stackTraceNode *node=hashTable[hashIdx];
		
		if (node==NULL)
		{
			// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
	pthread_mutex_unlock(&hashMutex[hashIdx]);
			return;
		}
	
		stackTraceNode *prevNode=NULL;
		
		while (node!=NULL && node->thId!=me)
		{
			prevNode=node;
			node=node->hashNext;
		}
	
		if (node==NULL)
		{
			// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_unlock(&hashMutex[hashIdx]);
			return;
		}
		
		stackTraceNode *prevHashNode=prevNode;
	
		if (node->stackNext==NULL)
		{
			if (prevHashNode!=NULL)
				prevHashNode->hashNext=node->hashNext;
			else
				hashTable[hashIdx]=node->hashNext;
			free(node);
			// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
	pthread_mutex_unlock(&hashMutex[hashIdx]);
			return;
		}
		
		prevNode=node->stackNext;
		node->stackNext=prevNode->stackNext;
		
		free(prevNode->function);
		free(prevNode->file);
		popCount=prevNode->popCount;
		free(prevNode);
		
		if (node->stackNext==NULL)
		{
			if (prevHashNode!=NULL)
				prevHashNode->hashNext=node->hashNext;
			else
				hashTable[hashIdx]=node->hashNext;
			free(node);
		}
	
		// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_unlock(&hashMutex[hashIdx]);
	}
}

StackTrace::StackTrace(const char *file, int line, const char *fn)
{
	signal(SIGUSR2, StackTrace::sigusr2);

	// Com que representa que la primera crida que es fara sera a main()
	// i encara no hi haura mes d'un thread, no ens preocupem per la
	// concurrencia...
	
	if (hashMutex==NULL)
	{
		hashMutex=(pthread_mutex_t*)malloc(sizeof(pthread_mutex_t)*StackTrace::hashSize);

		if (hashMutex==NULL)
		{
			cerr << "FATAL: could not allocate data structures for StackTrace" << endl;
			exit(-1);
		}

		for (qword i=0; i<StackTrace::hashSize; i++)
		{
			if (pthread_mutex_init(&hashMutex[i], NULL)!=0)
			{
				cerr << "FATAL: Could not init data structures for StackTrace" << endl;
				exit(-1);
			}
		}
	}
	
	if (hashTable==NULL)
	{
		hashTable=(stackTraceNode**)malloc(sizeof(stackTraceNode*)*StackTrace::hashSize);
		if (hashTable==NULL)
		{
			cerr << "FATAL: could not allocate data structures for StackTrace" << endl;
			exit(-1);
		}
		memset(hashTable, 0, sizeof(stackTraceNode*)*StackTrace::hashSize);
	}
	
	StackTrace::push(fn, file, line);
}

StackTrace::~StackTrace()
{
	StackTrace::pop();
}	

void StackTrace::dump(ostream &stream)
{
	pthread_t me=pthread_self();

	stream << "*** StackTrace dump for thread " << me << ":" << endl;

	qword hashIdx=thHash(me);

	// cerr << "pthread_mutex_lock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_lock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
	pthread_mutex_lock(&hashMutex[hashIdx]);
	stackTraceNode *node=hashTable[hashIdx];
	
	if (node==NULL)
	{
		stream << "No stack trace for thread at hash table" << endl;
		// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_unlock(&hashMutex[hashIdx]);
		return;
	}

	stackTraceNode *prevNode=NULL;
	
	while (node!=NULL && node->thId!=me)
	{
//		stream << "searching " << me << ", " << node->thId << endl;
	
		prevNode=node;
		node=node->hashNext;
	}

	if (node==NULL || node->stackNext==NULL)
	{
		stream << "Empty stack trace for thread" << endl;
		// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_unlock(&hashMutex[hashIdx]);
		return;
	}
			
	node=node->stackNext;

	while (node!=NULL)
	{
		stream << node->function << " (" << node->file << ", line " << node->line << ")" << endl;
		node=node->stackNext;
	}
	// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
	pthread_mutex_unlock(&hashMutex[hashIdx]);
}

void StackTrace::dumpAll(ostream &stream)
{
	stream << endl << "*** StackTrace dump for all threads ***" << endl;
	for (int hashIdx=0; hashIdx<StackTrace::hashSize; hashIdx++)
	{
		// cerr << "pthread_mutex_lock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_lock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_lock(&hashMutex[hashIdx]);
		stackTraceNode *nodeHash=hashTable[hashIdx];

		while (nodeHash!=NULL)
		{
			stackTraceNode *node=nodeHash;
						
			stream << endl << "StackTrace dump for thread " << node->thId << ":" << endl;

			if (node->stackNext==NULL)
			{
				stream << "\tEmpty stack trace for thread" << endl;
				nodeHash=nodeHash->hashNext;
				continue;
			}
					
			node=node->stackNext;

			while (node!=NULL)
			{
				stream << "\t" << node->function << " (" << node->file << ", line " << node->line << ") - " << node->popCount << endl;
				node=node->stackNext;
			}
			
			nodeHash=nodeHash->hashNext;
		}
		// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_unlock(&hashMutex[hashIdx]);
	}
}

char* StackTrace::toString()
{
	char *buf=NULL;
	char *aux=NULL;
	char *aux2=NULL;
	pthread_t me=pthread_self();
	
	buf=(char*)malloc(256);
	memset(buf, 0, 256);
	sprintf(buf, "\n*** StackTrace dump for all threads ***\n");
	
	for (int hashIdx=0; hashIdx<StackTrace::hashSize; hashIdx++)
	{
		// cerr << "pthread_mutex_lock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_lock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_lock(&hashMutex[hashIdx]);
		stackTraceNode *nodeHash=hashTable[hashIdx];

		while (nodeHash!=NULL)
		{
			stackTraceNode *node=nodeHash;
						
			aux=(char*)malloc(1024);
			memset(aux, 0, 1024);
			if (node->thId==me)
				sprintf(aux, "StackTrace dump for me (thread %p):\n", node->thId);
			else
				sprintf(aux, "StackTrace dump for thread %p:\n", node->thId);

			if (node->stackNext==NULL)
			{
				sprintf(aux+strlen(aux), "\tEmpty stack trace for thread\n");
				aux2=(char*)malloc(strlen(buf)+strlen(aux)+1);
				memset(aux2, 0, strlen(buf)+strlen(aux)+1);
				memmove(aux2, buf, strlen(buf));
				memmove(aux2+strlen(buf), aux, strlen(aux)+1);
				free(buf);
				buf=aux2;
				nodeHash=nodeHash->hashNext;
				free(aux);
				continue;
			}
					
			node=node->stackNext;

			while (node!=NULL)
			{
				sprintf(aux+strlen(aux), "\t%s (%s, line %u) - %d\n", node->function, node->file, node->line, node->popCount);
				aux2=(char*)malloc(strlen(buf)+strlen(aux)+1);
				memset(aux2, 0, strlen(buf)+strlen(aux)+1);
				memmove(aux2, buf, strlen(buf));
				memmove(aux2+strlen(buf), aux, strlen(aux)+1);
				free(buf);
				buf=aux2;
				node=node->stackNext;
				memset(aux, 0, 1024);
			}
			free(aux);
			
			nodeHash=nodeHash->hashNext;
		}
		// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_unlock(&hashMutex[hashIdx]);
	}

	return buf;
}

char* StackTrace::meToString()
{
	char *buf=NULL;
	char *aux=NULL;
	char *aux2=NULL;
	pthread_t me=pthread_self();
	
	buf=(char*)malloc(1024);
	memset(buf, 0, 1024);
	
	for (int hashIdx=0; hashIdx<StackTrace::hashSize; hashIdx++)
	{
		// cerr << "pthread_mutex_lock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_lock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_lock(&hashMutex[hashIdx]);
		stackTraceNode *nodeHash=hashTable[hashIdx];

		while (nodeHash!=NULL)
		{
			stackTraceNode *node=nodeHash;
						
			if (node->thId==me)
			{
				aux=(char*)malloc(1024);
				memset(aux, 0, 1024);
				
				sprintf(aux, "StackTrace dump for me (thread %p):\n", node->thId);

				aux2=(char*)malloc(strlen(buf)+strlen(aux)+1);
				memset(aux2, 0, strlen(buf)+strlen(aux)+1);
				memmove(aux2, buf, strlen(buf));
				memmove(aux2+strlen(buf), aux, strlen(aux)+1);
				free(buf);
				buf=aux2;
				memset(aux, 0, 1024);

				if (node->stackNext==NULL)
				{
					sprintf(aux, "\tEmpty stack trace for thread\n");
					aux2=(char*)malloc(strlen(buf)+strlen(aux)+1);
					memset(aux2, 0, strlen(buf)+strlen(aux)+1);
					memmove(aux2, buf, strlen(buf));
					memmove(aux2+strlen(buf), aux, strlen(aux)+1);
					free(buf);
					free(aux);
					buf=aux2;
					nodeHash=nodeHash->hashNext;
					continue;
				}
						
				node=node->stackNext;

				while (node!=NULL)
				{
					sprintf(aux, "\t%s (%s, line %u) - %d\n", node->function, node->file, node->line, node->popCount);
					aux2=(char*)malloc(strlen(buf)+strlen(aux)+1);
					memset(aux2, 0, strlen(buf)+strlen(aux)+1);
					memmove(aux2, buf, strlen(buf));
					memmove(aux2+strlen(buf), aux, strlen(aux)+1);
					free(buf);
					buf=aux2;
					node=node->stackNext;
					memset(aux, 0, 1024);
				}
				free(aux);
				
				// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
				pthread_mutex_unlock(&hashMutex[hashIdx]);
				return buf;
			}

			nodeHash=nodeHash->hashNext;
		}
		// cerr << "pthread_mutex_unlock(&hashMutex[hashIdx])... "; cerr.flush(); if (pthread_mutex_unlock(&hashMutex[hashIdx])!=0) cerr << " error!!!!" << endl; else cerr << "ok." << endl;
		pthread_mutex_unlock(&hashMutex[hashIdx]);
	}

	return buf;
}

