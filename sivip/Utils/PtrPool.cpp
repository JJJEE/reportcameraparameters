/*
 *  PtrPool.cpp
 *  
 *
 *  Created by David Marí Larrosa on 27/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Utils/WindowsInclude.h>
#include "PtrPool.h"
#include <iostream>

PtrPool::PtrPool(string name)
{
//	cout<<"PtrPool::PtrPool "<<name<<" lock mi:"<<(void*)poolLock.mi<<" m:"<<(void*)poolLock.mi->m<<endl;
}

PtrPool::~PtrPool()
{

}

void PtrPool::add(void *ptr, bool occupied)
{
	poolLock.lock();
	map<void*,poolInfo>::iterator itpool;
	int sz=pool.size();
	bool found=false;
	if(sz!=0)
	{
		itpool=pool.find(ptr);
		if (itpool!=pool.end())
			found=true;
	}
	if(!found)
	{
		poolInfo pi;
		pi.occupied=occupied;
		
		pool[ptr]=pi;
		// TODO: que COI es aquest codi de sota i pq la linia de dalt estava
		// comentada?
//		pair<void*,poolInfo> pr=make_pair(ptr,pi);
//		pool.insert(pr);
	}
	poolLock.unlock();
}

void PtrPool::remove(void *ptr)
{
	poolLock.lock();
	map<void*,poolInfo>::iterator itpool=pool.find(ptr);
	if (itpool!=pool.end() && !(*itpool).second.occupied)
		pool.erase(itpool);
	poolLock.unlock();
}

void* PtrPool::get()
{
	void *ptr=NULL;
	poolLock.lock();
	int sz=pool.size();
	if(sz==0)
	{
		poolLock.unlock();
		return NULL;
	}
	map<void*,poolInfo>::iterator itpool=pool.begin();

	while (itpool!=pool.end() && (*itpool).second.occupied)
		itpool++;
		
	if (itpool!=pool.end())
	{
		ptr=(*itpool).first;
		(*itpool).second.occupied=true;
	}
	poolLock.unlock();
	return ptr;
}

void PtrPool::release(void *ptr)
{
	poolLock.lock();
	map<void*,poolInfo>::iterator itpool=pool.find(ptr);
	if (itpool!=pool.end())
		(*itpool).second.occupied=false;
	poolLock.unlock();
}

dword PtrPool::size()
{
	poolLock.lock();
	dword s=pool.size();
	poolLock.unlock();
	
	return s;
}

int PtrPool::numFree()
{
	poolLock.lock();
	int num=0;
	for(map<void*,poolInfo>::iterator i=pool.begin();i!=pool.end(); i++)
		if(!((*i).second.occupied))
			num++;
		
	poolLock.unlock();
	return num;
}
