 /*
 *  DBGateway.h
 * 
 *	Xixa per poder fer queries mes sezillament a la BD
 *
 *  Created by David Marí Larrosa on 24/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef __SIRIUS__BASE__UTILS__DBGATEWAY_H
#define __SIRIUS__BASE__UTILS__DBGATEWAY_H

#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/Canis.h>
#include <ServiceFinder/ServiceFinder.h>
#include <XML/XML.h>
#include <Threads/Mutex.h>
#include <map>

using namespace std;

class DBGatewayService
{
	public:
		static const int queryServiceId=0;
		static const int updateServiceId=1;
		static const int beginServiceId=2;
		static const int commitServiceId=3;
		static const int rollbackServiceId=4;
};

class DBGateway
{
	public:
		static const int queryServiceId=0;
		static const int updateServiceId=1;
		static const int beginServiceId=2;
		static const int commitServiceId=3;
		static const int rollbackServiceId=4;

		static const int defaultMaxRPC=20;
		static int maxRPC;

	protected:
		Address origen;
//		RPC *dbRPC;
//		Mutex callMutex;
		Condition RPCPoolCond;
		PtrPool dbRPCPool;
		short type;

		Mutex txModeLock;
		map<RPC*, bool> txModeForRPC;

		string getDate();
	public:
		Canis *cn;
		Address db;
		Mutex dbMutex;

		DBGateway(Address origen, short type, Canis *cn, Address db=Address());
		DBGateway(string file, Canis *cn=NULL);
		~DBGateway();
		void findDB();
		XML* call(string s, int callId=0, RPC *dbRPC=NULL, Address *keepAliveAddr=NULL);
		XML* call(string s, RPC *dbRPC, int callId=0, Address *keepAliveAddr=NULL);
		XML* query(string s, RPC *dbRPC=NULL, Address *keepAliveAddr=NULL);
		XML* update(string s, RPC *dbRPC=NULL, Address *keepAliveAddr=NULL);
		void begin(RPC *dbRPC=NULL);
		void commit(RPC *dbRPC=NULL);
		void rollback(RPC *dbRPC=NULL);
		RPC* getRPC(Address *keepAliveAddr=NULL, int retries=10);
		void freeRPC(RPC*);
		void delRPC(RPC*);
		
		void enableTransactionMode(RPC *dbRPC);
		void disableTransactionMode(RPC *dbRPC);
		bool isInTransactionMode(RPC *dbRPC);
		
		void setConfigParam(int rootID, string path, string value, RPC* rpc=NULL);
		string getConfigParam(int rootID, string path, RPC* rpc=NULL);
		int getConfigNode(int rootID, string path, RPC* rpc=NULL);
		bool checkConfigParam(int rootID, string path, RPC* rpc=NULL);
		
		void setModelConfigParam(string fabricante, string modelo, string path, string value, RPC* rpc=NULL);
		void setDeviceConfigParam(int id, string path, string value, RPC* rpc=NULL);
		void setSubsystemConfigParam(string ssName, Address ssAddr, string path, string value, RPC* rpc=NULL);

		string getModelConfigParam(string fabricante, string modelo, string path, RPC* rpc=NULL);
		string getDeviceConfigParam(int id, string path, RPC* rpc=NULL);
		string getSubsystemConfigParam(string ssName, Address ssAddr, string path, RPC* rpc=NULL);

		int getModelConfigNode(string fabricante, string modelo, string path, RPC* rpc=NULL);
		int getDeviceConfigNode(int rootID, string path, RPC* rpc=NULL);
		int getSubsystemConfigNode(int rootID, string path, RPC* rpc=NULL);

		bool checkModelConfigParam(string fabricante, string modelo, string path, RPC* rpc=NULL);
		bool checkDeviceConfigParam(int id, string path, RPC* rpc=NULL);
		bool checkSubsystemConfigParam(int id, string path, RPC* rpc=NULL);

		void deleteConfig(int rootID, RPC* rpc=NULL);

		void deleteConfig(int rootID, string path, RPC* rpc=NULL);

		void deleteDeviceConfig(int id, string path, RPC* rpc=NULL);
		void deleteSubsystemConfigParam(string ssName, Address ssAddr, string p, RPC* rpc=NULL);

		bool logEvent(string missatge, int nivell, RPC *dbRPC=NULL);
		bool logAlarm(string missatge, int dispositiu, string valor, string cond,  int nivell, RPC *dbRPC=NULL);
		bool logDev(string missatge, int dispositiu, int nivell, RPC *dbRPC=NULL);
		bool logDev(string missatge, int dispositiu, string nivell, RPC *dbRPC=NULL);
		
		long createRoot(string table, string column, string idCol, string idVal, RPC *dbRPC=NULL);
		
		long getUnregDataRoot(string name, string ip, int port, RPC *dbRPC=NULL);
};
#endif
