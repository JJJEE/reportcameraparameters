/*
 *  SerializedException.h
 *  
 *
 *  Created by David Marí Larrosa on 15/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__UTILS__SERIALIZEDEXCEPTION_H
#define __SIRIUS__BASE__UTILS__SERIALIZEDEXCEPTION_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <string>

using namespace std;

class SerializedException
{
private:
	bool copied;
protected:
	dword nBytes;
	byte *bytesPtr;

public:
	SerializedException(dword nB, byte *b, bool copy=true);
	SerializedException(SerializedException &se);
	~SerializedException();
	
	byte *bytes();
	dword size();
	
	virtual string getClass();
	virtual int getCode();
	virtual string getMsg();
	virtual void materializeAndThrow(bool deleteThis=false);	
};
#endif

