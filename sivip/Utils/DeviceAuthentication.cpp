#include <Utils/DeviceAuthentication.h>
#include <XML/xmlParser.h>
#include <stdlib.h>

DeviceAuthentication::DeviceAuthentication(xmlNode *config)
{
	string extFilename=config->getNodeData("/[0]/DeviceAuthentication/File");
	if (extFilename.length()>0)
	{
		// Ho agafem d'un fitxer extern
		try
		{
			XML *extConf = xmlParser::parseFile(extFilename);
			config = extConf->getRoot();
		}
		catch (...)
		{
		}
	}

	DeviceAuthentication::AuthInfo ai;
	ai.username=config->getNodeData("/[0]/DeviceAuthentication/Global/User");
	ai.password=config->getNodeData("/[0]/DeviceAuthentication/Global/Password");

	if (ai.username.length()>0)
	{
		this->authById[-1] = ai;
	}
	
	xmlNode::Iterator daIt(config, string("/[0]/DeviceAuthentication/Device*"));
	while (daIt.hasNext())
	{
		xmlNode *n=daIt.next();

		string devIdStr = n->getParam("id");
		int devId=0;
		if (devIdStr.length()>0)
		{
			devId=atoi(devIdStr.c_str());
			ai.username=n->getNodeData("/Device/User");
			ai.password=n->getNodeData("/Device/Password");
			if (ai.username.length()>0)
			{
				this->authById[devId] = ai;
			}
		}
	}	
}

DeviceAuthentication::~DeviceAuthentication()
{
}

DeviceAuthentication::AuthInfo DeviceAuthentication::getAuthByCamId(int id)
{
	map<int, DeviceAuthentication::AuthInfo>::iterator a=this->authById.find(id);
	if (a==this->authById.end())
	{
		a = this->authById.find(-1);

		if (a==this->authById.end())
		{
			throw InvalidParameterException(string("No authentication information for device with id ") + StrUtils::decToString(id));
		}
		
		return a->second;
	}

	return a->second;
}
