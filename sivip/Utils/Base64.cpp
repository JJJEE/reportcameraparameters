#include <Utils/Base64.h>
#include <Utils/debugStackTrace.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

//#include "log.h"

//#include <string>


char base64table[64]={
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 
	'w', 'x', 'y', 'z', '0', '1', '2', '3', 
	'4', '5', '6', '7', '8', '9', '+', '/', 
};

string base64encode(string decoded)
{
	dword len=decoded.length();
	const char *str=decoded.c_str();
	
	if (len==0)			// Como minimo codificamos 1 byte :P
		return string("");

	dword paq=len/3;
	byte padd=(byte)(len%3);

	dword outlen=paq*4+(padd>0?4:0);

	char *out=new char[outlen+1];

	if (out==NULL)
	{
		return string("");
	}
	
	memset(out, 0, outlen+1);

	// Codificacion
	dword i,j;
	for (i=0, j=0; i<len; i+=3, j+=4)
	{
		out[j]=base64table[(str[i]>>2)&63];
		out[j+1]=base64table[((str[i]&3)<<4)|((str[i+1]>>4)&15)];
		out[j+2]=base64table[((str[i+1]&15)<<2)|((str[i+2]>>6)&3)];
		out[j+3]=base64table[str[i+2]&63];
	}


	// si hay padds, codificar los bytes sobrantes y a�adir padding...
	switch (padd)
	{
		case 1:
			i-=3;
			j-=4;
			out[j]=base64table[(str[i]>>2)&63];
			out[j+1]=base64table[((str[i]&3)<<4)];
			out[j+2]='=';
			out[j+3]='=';
			break;

		case 2:
			i-=3;
			j-=4;
			out[j]=base64table[(str[i]>>2)&63];
			out[j+1]=base64table[((str[i]&3)<<4)|((str[i+1]>>4)&15)];
			out[j+2]=base64table[(str[i+1]&15)<<2];
			out[j+3]='=';
			break;
	}

	string ret=string(out, outlen);
	delete [] out;

	return ret;
}

string base64decode(string encoded)
{
	dword len=encoded.length();
	const char *str=encoded.c_str();
	
	if (len<4 || (len%4)!=0)			// Debemos descodificar multiplos de 4, y como minimo 4
	{
		return string("");
	}
	
	dword paq=len/4;

	byte padd=0;
	if (str[len-1]=='=')
		padd++;
	if (str[len-2]=='=')
		padd++;

	// Puede haber hasta dos paddings :P
	dword outlen=paq*3+(3-padd);

					//__FILE=//__FILE__;
					//__LINE=//__LINE__;
	char *out=new char[outlen+1];

	if (out==NULL)
	{
		return string("");
	}
	
	memset(out, 0, outlen+1);

	// montamos una tabla de consulta para la descodificacion
	char base64decodetable[256];
	memset(base64decodetable, 0, 256);

	for (char p=0; p<64; p++)
	{
		base64decodetable[base64table[p]]=p;
	}

	// Descodificacion
	dword i,j;
	unsigned char buf[4];
	for (i=0, j=0; i<len; i+=4, j+=3)
	{
		buf[0]=base64decodetable[str[i]];
		buf[1]=base64decodetable[str[i+1]];
		buf[2]=base64decodetable[str[i+2]];
		buf[3]=base64decodetable[str[i+3]];

		out[j]=(buf[0]<<2)|(buf[1]>>4);
		out[j+1]=(buf[1]<<4)|(buf[2]>>2);
		out[j+2]=(buf[2]<<6)|(buf[3]);
	}

	// si hay padds, ver cuantos son y actuar en consecuencia...
	switch (padd)
	{
		case 1:		// Hay 3 bytes de base64 -> salen 2 bytes descodificados
			i-=4;
			j-=3;
			buf[0]=base64decodetable[str[i]];
			buf[1]=base64decodetable[str[i+1]];
			buf[2]=base64decodetable[str[i+2]];

			out[j]=(buf[0]<<2)|(buf[1]>>4);
			out[j+1]=(buf[1]<<4)|(buf[2]>>2);
			break;

		case 2:		// Hay 2 bytes de base64 -> sale 1 byte descodificado
			i-=4;
			j-=3;
			buf[0]=base64decodetable[str[i]];
			buf[1]=base64decodetable[str[i+1]];

			out[j]=(buf[0]<<2)|(buf[1]>>4);
			break;
	}

	string ret=string(out, outlen);
	delete [] out;

	return ret;
}

