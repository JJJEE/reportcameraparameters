#ifndef __SIRIUS__BASE__UTILS__CHECKPOINT_H
#define __SIRIUS__BASE__UTILS__CHECKPOINT_H

#include <Utils/Types.h>
#include <string>

using namespace std;

class CheckPoint
{
protected:
	string file;
	dword line;

public:
	CheckPoint(string file, dword line);
	~CheckPoint();
	
	void checkPoint(dword l);
	string getCheckPointFile();
	dword getCheckPointLine();
};

#ifdef WITH_CHECKPOINT

#define INIT_CHECKPOINT()	CheckPoint __ckpt__(__FILE__, __LINE__)
#define CHECKPOINT()	__ckpt__.checkPoint(__LINE__)
#define CHECKPOINT_FILE()	__ckpt__.getCheckPointFile()
#define CHECKPOINT_LINE()	__ckpt__.getCheckPointLine()
#define CHECKPOINT_TOSTRING()	(string (" (from ") + \
						__ckpt__.getCheckPointFile() + \
						string(", line ") + \
						StrUtils::decToString(__ckpt__.getCheckPointLine()) + \
						string(")"))

#else

#define INIT_CHECKPOINT()
#define CHECKPOINT()
#define CHECKPOINT_FILE() string("")
#define CHECKPOINT_LINE() string("")
#define CHECKPOINT_TOSTRING() string("")
#endif

#endif
