#ifndef __SIRIUS__BASE__UTILS__LOG_H
#define __SIRIUS__BASE__UTILS__LOG_H

#include <Utils/debugNew.h>
#include <stdio.h>
#include <time.h>
#include <string>
#include <Utils/Types.h>
#include <Threads/Mutex.h>
#include <Logger/Logger.h>
	
using namespace std;

class Log
{
protected:
	string filename;
	
	bool rotate;
	dword maxSize;	// Tamany maxim permes del log
	dword ttl;		// Temps de vida del log
	dword ttlStart;

	byte maxFiles;
	
	Mutex logLock;
	
	FILE *rotateIfNeeded(FILE *f);
	void rotateLogFiles(string basename);
	
public:
	Log(string fname, bool logStart=false);
	~Log();

	static string getDateLogTag();
	static string formatLogLine(string line);

	void limitBySize(dword size);
	dword getSizeLimit();
	
	void limitByTime(dword seconds);
	dword getTimeLimit();
	dword getElapsedTimeFromStart();

	void setMaxRotationFiles(byte nFiles);
	byte getMaxRotationFiles();

	void logLine(string l);
};
#endif
