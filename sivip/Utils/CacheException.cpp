/*
 *  CacheException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Utils/CacheException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

CacheException::CacheException(string msg, int code): Exception(msg, code)
{

}

CacheException::CacheException(int code, string msg): Exception(msg, code)
{

}

CacheException::CacheException(SerializedException &se): Exception(se)
{

}

CacheException::~CacheException()
{
   
}

string CacheException::getClass()
{
	string c=string("CacheException");
	return c;
}

