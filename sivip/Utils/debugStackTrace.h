/*
 *  debugStackTrace.h
 *  
 *
 *  Created by David Marí Larrosa on 07/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__UTILS__DEBUGSTACKTRACE_H
#define __SIRIUS__BASE__UTILS__DEBUGSTACKTRACE_H

#ifdef WITH_DEBUG_STACKTRACE

#include <Utils/Types.h>
#include <Utils/debugNew.h>
// Per fer servir mutex a baix nivell
#include <pthread.h>
#include <string>
#include <list>
#include <iostream>

using namespace std;

class StackTrace
{
public:
	struct stackTraceEntry
	{
		char *function;
		char *file;
		int line;
		// Comptador per saber quants pops hem de fer en fer delete.
		int popCount;		
	};
	
protected:
	// 1021 pq es primer i tal :D
	static const qword hashSize=1021;
	
	static void sigusr2(int);
	static dword thHash(pthread_t th);

	struct stackTraceNode : public stackTraceEntry
	{
		pthread_t thId;
		stackTraceNode *hashNext;
		stackTraceNode *stackNext;
	};
	
	static pthread_mutex_t *hashMutex;
	static stackTraceNode **hashTable;

public:
	static void push(const char *fn, const char *file="nofile", int line=0,
		bool isExtra=false);
	static void pop();

	StackTrace(const char *file, int line, const char *fn);
	~StackTrace();
	
	static void dump(ostream &stream=cerr);
	static void dumpAll(ostream &stream=cerr);
	static char* toString();
	static char* meToString();
};

#define STACKTRACE_INSTRUMENT() StackTrace __stInst__(__FILE__, __LINE__, __func__)
#define STACKTRACE_EXTRATRACE() __stInst__.push("\t\t", __FILE__, __LINE__, true)
#define STACKTRACE_DUMP() StackTrace::dump()
#define STACKTRACE_DUMPALL() StackTrace::dumpAll()
#define STACKTRACE_TOSTRING() StackTrace::toString()
#define STACKTRACE_METOSTRING() StackTrace::meToString()

#else /* !WITH_DEBUG_STACKTRACE */

#define STACKTRACE_INSTRUMENT() ;
#define STACKTRACE_EXTRATRACE() ;
#define STACKTRACE_DUMP() ;
#define STACKTRACE_DUMPALL() ;
#define STACKTRACE_TOSTRING() NULL
#define STACKTRACE_METOSTRING() NULL

#endif /* WITH_DEBUG_STACKTRACE */

#endif
