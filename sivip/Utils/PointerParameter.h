#ifndef __SIRIUS__BASE__PLUGINS__POINTERPARAMETER_H
#define __SIRIUS__BASE__PLUGINS__POINTERPARAMETER_H

#include <Utils/Types.h>
#include <stdio.h>

class PointerParameter
{
protected:
	void *localAddress;
	void *remoteAddress;

public:
	PointerParameter();
	PointerParameter(void *local, void *remote=NULL);
	
	virtual void toogleMode();
	void setAddress(void *local);
	
	void *getLocalAddress();
	void *getRemoteAddress();

	// No la volem abstracta!
//	virtual void* toNetwork() = 0;
//	virtual void toLocal(void *data) = 0;
//	virtual dword size() = 0;
};

#endif
