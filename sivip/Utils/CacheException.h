/*
 *  CacheException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__UTILS__CACHEEXCEPTION_H_
#define __SIRIUS__BASE__UTILS__CACHEEXCEPTION_H_

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class CacheException : public Exception
{
 public:
	CacheException(string msg, int code=0);
	CacheException(int code, string msg);
	CacheException(SerializedException &se);

	~CacheException();
	
	virtual string getClass();
};
#endif
