/*
 *  DBGateway.cpp
 *  
 *
 *  Created by David Marí Larrosa on 24/03/06
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "DBGateway.h"
#include <Sockets/SocketTimeoutException.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <iostream>
#include <string.h>
#include <stdio.h>

#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <Utils/debugStackTrace.h>
#ifdef WIN32
#include <Utils/windowsDefs.h>
#else
#ifndef WIN32
#include <unistd.h>
#endif
#endif

using namespace std;

#pragma mark *** Estatiques
int DBGateway::maxRPC=DBGateway::defaultMaxRPC;

#pragma mark *** DBGateway

DBGateway::DBGateway(Address origen, short type, Canis *cn, Address db):origen(origen), type(type), cn(cn), db(db)
{
	STACKTRACE_INSTRUMENT();
	
	//	if(db.getIP().toDWord()==0)
	//		dbRPC=NULL;
	//	else
	//		dbRPC= new RPC(db);
}


DBGateway::~DBGateway()
{
	STACKTRACE_INSTRUMENT();
	
	//	if(dbRPC!=NULL)
	//		delete dbRPC;
	RPC *r=(RPC*)dbRPCPool.get();
	while(r!=NULL)
	{
		this->delRPC(r);
		r=(RPC*)dbRPCPool.get();
	}
}

void DBGateway::findDB()
{
	STACKTRACE_INSTRUMENT();

	while(db.getIP().toDWord()==0)
	{
		try
		{
					//cout<<" DBGW:: findDB -> getBestSS()"<<endl;
			RPCPacket *plugin=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn, true);
			if(plugin==NULL)
				continue;
			db=*plugin->a;


			delete plugin;
						//cout<<" DBGW:: findDB -> new RPC()"<<endl;
			//		if(dbRPC!=NULL)
			//			delete dbRPC;
			//		dbRPC= new RPC(db);
		}
		catch(/*Service*/Exception &e) //NODBGW
		{
					//cout<<"ServExc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			sleep(1);
		}
	}
	//	cout<<"DBGw::Found DB :"<<db.getIP().toString()<<endl;
}


RPC* DBGateway::getRPC(Address *keepAliveAddr, int retries)
{
	STACKTRACE_INSTRUMENT();
	
	retries=4;
//	cout<<" getRPC addr:"<<keepAliveAddr<<" retr:"<<retries<<endl;
	RPCPoolCond.lock();
	RPC *res=NULL;
	try
	{
//		cout<<"[DBGateway::getRPC]: pool size: " << dbRPCPool.size()
//			<< ", free: " << dbRPCPool.numFree() << ", max:" << maxRPC << endl;
		res=(RPC*)dbRPCPool.get();

		while(res==NULL && dbRPCPool.size()> maxRPC)
		{
			//		cout<<" ==== getRPC: wait RPC size:"<<(void*)res<<" size:"<<dbRPCPool.size()<<" free:"<<dbRPCPool.numFree()<<" max:"<<maxRPC<<endl;
			retries--;
#ifndef WIN32
			cout<< "[thread " << Thread::getId() << "] Wait RPC retries remaining == " << retries << " size:"<<dbRPCPool.size()<<" max:"<<maxRPC <<endl;
#else
			cout<< "Wait RPC retries remaining == " << retries << " size:"<<dbRPCPool.size()<<endl;
#endif
			if(retries==0)
			{
				RPCPoolCond.unlock();
				return NULL;
			}

			struct timespec t;
			t.tv_sec=time(NULL)+4;
			t.tv_nsec=0;
			if(RPCPoolCond.wait(t))
			{
			//	cout<<" timeout cua getRPC addr:"<<keepAliveAddr<<" retr:"<<retries<<endl;
				if(keepAliveAddr!=NULL)
				{
			//		cout<<"DBGW::getRPC keepalive cua getRPC ->"<<keepAliveAddr->toString()<<endl;
					RPC::keepAlive(keepAliveAddr, 0);
				}
			}
			res=(RPC*)dbRPCPool.get();
			if(res != NULL)
				cout<<"getRPC: got RPC after waiting, remaining:"<<retries<<" free:"<<dbRPCPool.numFree()<<" size:"<<dbRPCPool.size()<<" max:"<<maxRPC<<endl;
		}

		//	cout<<" ==== getRPC: new RPC?:"<<(void*)res<<" free:"<<dbRPCPool.numFree()<<" max:"<<maxRPC<<endl;
		if(res==NULL)
		{
			//			cout<<" ==== getRPC: new RPC size:"<<dbRPCPool.size()<<" max:"<<maxRPC<<endl;
			findDB();
			res=new RPC(db);
			enableTransactionMode(res);
			dbRPCPool.add(res,true);
		}
		else	// Ens assegurem que treballem sobre el DBGW per defecte
			res->setDefaultDestination(db);
			
		//	cout<<" ==== getRPC return:"<<(void*)res<<" free:"<<dbRPCPool.numFree()<<endl;
		RPCPoolCond.unlock();
	}catch(Exception &e)
	{
		try{ RPCPoolCond.unlock(); }catch(Exception &e){}
		//e.serialize()->materializeAndThrow(true);
		throw;
	}catch(...)
	{
		try{ RPCPoolCond.unlock(); }catch(Exception e){}
	}
	return res;
}

void DBGateway::freeRPC(RPC* rpc)
{
	STACKTRACE_INSTRUMENT();
	
	try
	{
		RPCPoolCond.lock();
		//cout<<" ==== freeRPC:"<<(void*)rpc<<" free:"<<dbRPCPool.numFree()<<" size:"<<dbRPCPool.size()<<endl;
		dbRPCPool.release(rpc);	
		//cout<<" ==== freed RPC: freed:"<<dbRPCPool.numFree()<<" size:"<<dbRPCPool.size()<<endl;
		RPCPoolCond.signal();
		RPCPoolCond.unlock();
	}catch(Exception e)
	{
		try{ RPCPoolCond.unlock(); }catch(Exception e){}
		//e.serialize()->materializeAndThrow(true);
		throw;
	}catch(...)
	{
		try{ RPCPoolCond.unlock(); }catch(Exception e){}
	}

}

void DBGateway::delRPC(RPC* rpc)
{
	STACKTRACE_INSTRUMENT();
	
	try
	{
		RPCPoolCond.lock();
		//cout<<" ==== delRPC:"<<(void*)rpc<<" free:"<<dbRPCPool.numFree()<<" size:"<<dbRPCPool.size()<<endl;
		dbRPCPool.release(rpc);	
		dbRPCPool.remove(rpc);	
		//cout<<" ==== deleted RPC: free:"<<dbRPCPool.numFree()<<" size:"<<dbRPCPool.size()<<endl;
		RPCPoolCond.signal();
		RPCPoolCond.unlock();
		if(rpc!=NULL)
			delete rpc;
	}catch(Exception e)
	{
		cout<<" ==== delRPC exception: "<<e.getClass()<<":"<<e.getMsg()<<", free:"<<dbRPCPool.numFree()<<" size:"<<dbRPCPool.size()<<endl;
		try{ RPCPoolCond.unlock(); }catch(Exception e){}
		//e.serialize()->materializeAndThrow(true);
		throw;
	}catch(...)
	{
		try{ RPCPoolCond.unlock(); }catch(Exception e){}
	}

}

void DBGateway::enableTransactionMode(RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	txModeLock.lock();
	txModeForRPC[dbRPC]=true;
	txModeLock.unlock();
}

void DBGateway::disableTransactionMode(RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	txModeLock.lock();
	txModeForRPC[dbRPC]=false;
	txModeLock.unlock();
}

bool DBGateway::isInTransactionMode(RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	if (dbRPC==NULL)
		return false;

	txModeLock.lock();
	map<RPC*,bool>::iterator txModeIt=txModeForRPC.find(dbRPC);
	if (txModeIt == txModeForRPC.end() || txModeIt->second == false)
	{
		txModeLock.unlock();
		return false;
	}
	txModeLock.unlock();
	return true;
}

XML* DBGateway::call(string s, RPC *dbRPC, int callId, Address *keepAliveAddr)
{
	STACKTRACE_INSTRUMENT();
	
	return call(s, callId, dbRPC, keepAliveAddr);
}


XML* DBGateway::call(string s, int callId, RPC *dbRPC, Address *keepAliveAddr)
{
	STACKTRACE_INSTRUMENT();
	
	//	cout<<" DBGW:: call -> findDB()"<<endl;
	findDB(); // si la Addr de la BD != 0, no fa res :P
	byte* c=(byte*)s.c_str();
	dword len=s.length();
	bool newRPC=(dbRPC==NULL);

	RPCPacket p(origen, callId, c, len, type, -2);

	//	cout<<"DBGw::startSession call:"<<(char*)p.data<<endl;
	int dbfail=0;
	while (true)
	{
		RPCPacket *res=NULL;
		try
		{
			//			cout<<" -- DBGW: call lock"<<endl;
			if(newRPC && dbRPC==NULL)
			{
				//				cout<<" call::getRPC"<<endl;
				dbRPC=getRPC();
				if(dbRPC==NULL)
					return NULL;
			}
//			cout<<"DBGw:: RPC Call len:"<<len<<" new:"<<newRPC<<" rpc:"<<dbRPC->getLocalAddr().toString()<<" timeout:"<<dbRPC->getTimeout()<<endl;


			//cout << "DBGTWAY call" << endl;
			res=dbRPC->call(p, keepAliveAddr);
//			cout<<"DBGw:: RPC Called resp size:"<<res->getSize()<<" rpc:"<<dbRPC->getLocalAddr().toString()<<" type:"<<res->id<<endl;

			//cout << "DBGTWAY 2" << endl;

//			cout<<" -- DBGW: call unlock"<<endl;
			if(newRPC && dbRPC!=NULL)
				freeRPC(dbRPC);
				//delRPC(dbRPC);
		
			string resp((char*)res->getData(), (size_t)res->getSize());
//			cout<<" resp:"<<resp<<endl;
			delete res;
			res=NULL;
			
			XML *xmlRet=xmlParser::parse(resp);
			//			cout<<"DBGw:: RPC return"<<endl;
			return xmlRet;
		}
		catch (ServiceException &se)
		{
			cout << "DBGateway::call ServiceException:" << se.getClass() << ": " << se.getMsg()<<endl;
						
			// Si es una ServiceException probablement ve d'algun error amb
			// el query i no te sentit reintentar res.
			if(newRPC && dbRPC!=NULL)
				freeRPC(dbRPC);

			if(res != NULL)
				delete res;
			res=NULL;
			throw se;
		}
		catch (Exception &e)
		{
			if(res != NULL)
				delete res;
			res=NULL;
			
			if(dbRPC == NULL && (e.getCode()==2 || e.getMsg()==string("Unable to create socket")))
			{
				//e.serialize()->materializeAndThrow(true);
				throw;
			}

			if(++dbfail==2)
			{
				dbMutex.lock();
				if(dbRPC != NULL && db == dbRPC->getDefaultDestination())
				{
					Address ant = db;
					Address dba;
					db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
					dbMutex.unlock();
					findDB();
					cout << "DBGateway::call error, searching for new DB (from:"<<ant.toString()<<" to:"<<db.toString()<<", exception " << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				//	cout<<" query:"<<s<<endl;
				}
				else
				{
					dbMutex.unlock();
					cout << "DBGateway::call error, NOT searching for new DB: already changed ( new:"<<db.toString()<<", from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				}

				if(newRPC && dbRPC!=NULL)
				{
					delRPC(dbRPC);
					dbRPC=NULL;
					newRPC=true;
				}
				throw e;
//				return NULL;
			}
			else
			{
				cout << "DBGateway::call error, retry("<<dbfail<<"):" << e.getClass() << ": " << e.getMsg()
					<< endl;

			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::call, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
	};
	
	return NULL;
}

XML* DBGateway::query(string s, RPC *dbRPC, Address *keepAliveAddr)
{
	STACKTRACE_INSTRUMENT();
	
	return this->call(s, DBGateway::queryServiceId, dbRPC, keepAliveAddr);
}

XML* DBGateway::update(string s, RPC *dbRPC, Address *keepAliveAddr)
{
	STACKTRACE_INSTRUMENT();
	
	return this->call(s, DBGateway::updateServiceId, dbRPC, keepAliveAddr);
}

void DBGateway::begin(RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	findDB();
	bool newRPC=(dbRPC==NULL);

	RPCPacket p(origen, DBGatewayService::beginServiceId, NULL, 0, type,  -2);

	int dbfail=0;
	RPCPacket *res= NULL;
	do
	{
		try
		{
			//			cout<<" -- DBGW: begin lock"<<endl;
			if(newRPC || dbRPC==NULL)
			{
				//				cout<<" begin::getRPC"<<endl;
				dbRPC=getRPC();
				if(dbRPC==NULL)
					throw(SocketTimeoutException(0,""));
			}
			//			cout<<" -- DBGW: begin locked"<<endl;
			res = dbRPC->call(p);
			//			cout<<" -- DBGW: begin unlock"<<endl;
			if(newRPC)
			{
				if(dbRPC!=NULL)
					freeRPC(dbRPC);
			}
			//			cout<<" -- DBGW: begin unlocked"<<endl;
			if(res!=NULL)
				delete res;
			break;
		}
		catch(SocketTimeoutException &e)
		{
			cout<<"DBGW.begin:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			/*	if(++dbfail==3)
				{
				delRPC(dbRPC);
				dbRPC=NULL;
				newRPC=true;
				db=Address();	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
				findDB();
				}*/

			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				dbMutex.lock();
				if(dbRPC != NULL && db == dbRPC->getDefaultDestination())
				{
					Address dba;
					db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
					dbMutex.unlock();
					findDB();
					cout << "DBGateway::begin error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				//	cout<<" query:"<<s<<endl;
				}
				else
				{
					dbMutex.unlock();
					cout << "DBGateway::begin error, NOT searching for new DB: already changed (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				}

				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
//				Address dba;
//				db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
//				findDB();
//				cout << "DBGateway::begin error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
//					<< endl;
				throw(e); //canviar :P
			}

			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::begin, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(RPCException &e)
		{
			cout<<"DBGW.begin:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
				throw(e); //canviar :P
			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::begin, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(SocketException &e)
		{
			cout<<"DBGW.begin:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				dbMutex.lock();
				if(dbRPC != NULL && db == dbRPC->getDefaultDestination())
				{
					Address dba;
					db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
					dbMutex.unlock();
					findDB();
					cout << "DBGateway::begin error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				//	cout<<" query:"<<s<<endl;
				}
				else
				{
					dbMutex.unlock();
					cout << "DBGateway::begin error, NOT searching for new DB: already changed (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				}

				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
//				Address dba;
//				db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
//				findDB();
//				cout << "DBGateway::begin error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
//					<< endl;
				throw(e); //canviar :P
			}

			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::begin, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(Exception &e)
		{
			cout<<"DBGW.begin:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			//			cout<<" -- DBGW: begin unlock exc"<<endl;
			if(res!=NULL)
				delete res;
			res=NULL;
			if(newRPC)
			{
				if(dbRPC!=NULL)
					freeRPC(dbRPC);
			}
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
	}while(true);

	//	cout<<"DBGw:: /beginTrans:"<<endl;
}

void DBGateway::commit(RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	

	RPCPacket p(origen, DBGatewayService::commitServiceId, NULL, 0, type,  -2);

	bool newRPC=(dbRPC==NULL);
	int dbfail=0;

	RPCPacket *res=NULL;
	do
	{
		try
		{
			//			cout<<" -- DBGW: begin lock"<<endl;
			if(newRPC || dbRPC==NULL)
			{
				//				cout<<" commit::getRPC"<<endl;
				dbRPC=getRPC();
				if(dbRPC==NULL)
					throw(SocketTimeoutException(0,""));
			}
			//			cout<<" -- DBGW: begin locked"<<endl;
			res=dbRPC->call(p);
			//			cout<<" -- DBGW: begin unlock"<<endl;
			if(newRPC)
			{
				if(dbRPC!=NULL)
					freeRPC(dbRPC);
			}
			//			cout<<" -- DBGW: begin unlocked"<<endl;
			if(res!=NULL)
				delete res;
			break;
		}
		catch(SocketTimeoutException &e)
		{
			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				dbMutex.lock();
				if(dbRPC != NULL && db == dbRPC->getDefaultDestination())
				{
					Address dba;
					db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
					dbMutex.unlock();
					findDB();
					cout << "DBGateway::commit error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				//	cout<<" query:"<<s<<endl;
				}
				else
				{
					dbMutex.unlock();
					cout << "DBGateway::commit error, NOT searching for new DB: already changed (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				}

				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
//				Address dba;
//				db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
//				findDB();
//				cout << "DBGateway::begin error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
//					<< endl;
				//e.serialize()->materializeAndThrow(true);
				throw;
			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::commit, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(RPCException &e)
		{
			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
				//e.serialize()->materializeAndThrow(true);
				throw;
			//	throw(e); //canviar :P
			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::commit, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(SocketException &e)
		{
			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				dbMutex.lock();
				if(dbRPC != NULL && db == dbRPC->getDefaultDestination())
				{
					Address dba;
					db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
					dbMutex.unlock();
					findDB();
					cout << "DBGateway::commit error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				//	cout<<" query:"<<s<<endl;
				}
				else
				{
					dbMutex.unlock();
					cout << "DBGateway::commit error, NOT searching for new DB: already changed (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				}

				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
//				Address dba;
//				db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
//				findDB();
//				cout << "DBGateway::begin error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
//					<< endl;
				//e.serialize()->materializeAndThrow(true);
				throw;
			//	throw(e); //canviar :P
			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::commit, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(Exception &e)
		{
			if(res!=NULL)
				delete res;
			res=NULL;
			cout<<" -- DBGW: commit exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			if(newRPC)
			{
				if(dbRPC!=NULL)
					freeRPC(dbRPC);
			}
			return;
			//	e.serialize()->materializeAndThrow(true);
		}
	}while(true);

}

void DBGateway::rollback(RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	

	RPCPacket p(origen, DBGatewayService::rollbackServiceId, NULL, 0, type,  -2);

	bool newRPC=(dbRPC==NULL);
	int dbfail=0;
	RPCPacket *res=NULL;
	do
	{
		try
		{
			//			cout<<" -- DBGW: begin lock"<<endl;
			if(newRPC || dbRPC==NULL)
			{
				//				cout<<" rollback::getRPC"<<endl;
				dbRPC=getRPC();
				if(dbRPC==NULL)
					throw(SocketTimeoutException(0,""));
			}
			//			cout<<" -- DBGW: begin locked"<<endl;
			res=dbRPC->call(p);
			//			cout<<" -- DBGW: begin unlock"<<endl;
			if(newRPC)
			{
				if(dbRPC!=NULL)
					freeRPC(dbRPC);
			}
			//			cout<<" -- DBGW: begin unlocked"<<endl;
			if(res!=NULL)
				delete res;
			break;
		}
		catch(SocketTimeoutException &e)
		{
			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				dbMutex.lock();
				if(dbRPC != NULL && db == dbRPC->getDefaultDestination())
				{
					Address dba;
					db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
					dbMutex.unlock();
					findDB();
					cout << "DBGateway::rollback error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				//	cout<<" query:"<<s<<endl;
				}
				else
				{
					dbMutex.unlock();
					cout << "DBGateway::rollback error, NOT searching for new DB: already changed (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				}

				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
//				Address dba;
//				db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
//				findDB();
//				cout << "DBGateway::begin error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
//					<< endl;
				throw(e); //canviar :P
			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::rollback, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(RPCException &e)
		{
			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
				throw(e); //canviar :P
			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::rollback, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(SocketException &e)
		{
			if(res!=NULL)
				delete res;
			res=NULL;
			if(++dbfail==2)
			{
				dbMutex.lock();
				if(dbRPC != NULL && db == dbRPC->getDefaultDestination())
				{
					Address dba;
					db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
					dbMutex.unlock();
					findDB();
					cout << "DBGateway::rollback error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				//	cout<<" query:"<<s<<endl;
				}
				else
				{
					dbMutex.unlock();
					cout << "DBGateway::rollback error, NOT searching for new DB: already changed (from:" << e.getClass() << ": " << e.getMsg()<<")"
						<< endl;
				}

				if(newRPC)
				{
					if(dbRPC!=NULL)
						delRPC(dbRPC);
				}
//				Address dba;
//				db=dba;	//busquem la BD un cop, només tornem a passar pel CentralDir si petavaris cops 
//				findDB();
//				cout << "DBGateway::begin error, searching for new DB (from:" << e.getClass() << ": " << e.getMsg()<<")"
//					<< endl;
				throw(e); //canviar :P
			}
			if(dbRPC != NULL && db != dbRPC->getDefaultDestination())
			{
				cout << "DBGateway::rollback, DB changed, from:" << dbRPC->getDefaultDestination().toString()  << " to:"<<db.toString() << endl;
				dbRPC->setDefaultDestination(db);
				dbfail=0;
			}
		}
		catch(Exception &e)
		{
			if(res!=NULL)
				delete res;
			res=NULL;
			//			cout<<" -- DBGW: commit unlock exc"<<endl;
			if(newRPC)
			{
				if(dbRPC!=NULL)
					freeRPC(dbRPC);
			}
			return;
			//e.serialize()->materializeAndThrow(true);
		}
	}while(true);

}


void DBGateway::setDeviceConfigParam(int id, string p, string value, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	//	cout<<"SetDCP:"<<id<<" : "<<p<<" = "<<value<<endl;
	list<string> path=StrUtils::pathToList(p);

	if(path.size()==0)
	{
		throw Exception(0,"Invalid ConfigParam path");
	}

	//	cout<<" setDevCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC(NULL, -1);
	if(dbRPC==NULL)
		return;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))
			begin(dbRPC);

		string s = string("SELECT * FROM setDeviceConfigParamByPath('") + StrUtils::decToString(id) +string("', '")+ p+string("', '")+ value +string("')"); 
		//		cout<<" -- DBGW: setDeviceConfigParam call : "<<s<<endl;
		string nodeid;

		paramNode=call(s, 0, dbRPC);
		//		cout<<" -- DBGW: setDeviceConfigParam res : "<<endl;//paramNode->toString()<<endl;
		//	xmlNode *n=paramNode->getNode("/result/row/[1]");

		if (isInTransactionMode(dbRPC))
			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
		if(paramNode!=NULL)
			delete paramNode;
	}
	catch(Exception &e)
	{
		if(paramNode!=NULL)
			delete paramNode;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
	//	e.serialize()->materializeAndThrow(true);
		throw;
	}
	//	cout<<"commited "<<value<<endl;
}


void DBGateway::setModelConfigParam(string fabricante, string modelo, string p, string value, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	//cout<<"SetMCP:"<<fabricante<<":"<<modelo<<" : "<<p<<" = "<<value<<endl;
	list<string> path=StrUtils::pathToList(p);

	if(path.size()==0)
	{
		throw Exception(0,"Invalid ConfigParam path");
	}

	//	cout<<" setModCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC(NULL, -1);
	if(dbRPC==NULL)
		return;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))
			begin(dbRPC);

		string s = string("SELECT * FROM setModelConfigParamByPath('") + fabricante +string("', '") + modelo +string("', '")+ p+string("', '")+ value +string("')"); 
		//		cout<<" -- DBGW: setModelConfigParam res : "<<s<<endl;
		string nodeid;

		paramNode=call(s, 0, dbRPC);
		//		cout<<" -- DBGW: setModelConfigParam res : "<<endl;//paramNode->toString()<<endl;

		if (isInTransactionMode(dbRPC))
			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
		if(paramNode != NULL)
			delete paramNode;
	}
	catch(Exception &e)
	{
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			try
			{	
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	//	cout<<"commited "<<value<<endl;
}

void DBGateway::setSubsystemConfigParam(string ssName, Address ssAddr, string p, string value, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	//cout<<"SetMCP:"<<fabricante<<":"<<modelo<<" : "<<p<<" = "<<value<<endl;
	list<string> path=StrUtils::pathToList(p);

	if(path.size()==0)
	{
		throw Exception(0,"Invalid ConfigParam path");
	}

	//	cout<<" setModCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC(NULL, -1);
	if(dbRPC==NULL)
		return;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))
			begin(dbRPC);

		// TODO 20081209: fer be el tema de les '' :)
		string s = string("SELECT * FROM setSubsystemConfigParamByPath('") + ssName +string("', '") + ssAddr.getIP().toString() +string("', '")+ StrUtils::decToString(ssAddr.getPort()) +string("', '")+ p+string("', ")+ (value.length()==0?"NULL":string("'")+value+string("'")) +string(")"); 
//		cout<<" -- DBGW: "<<s<<endl;
		//	cout<<" -- DBGW: setModelConfigParam res : "<<s<<endl;

		paramNode=call(s, 0, dbRPC);
		//		cout<<" -- DBGW: setModelConfigParam res : "<<endl;//paramNode->toString()<<endl;

		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	//	cout<<"commited "<<value<<endl;
}


int DBGateway::getConfigNode(int rootID, string p, RPC* dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	//	cout<<" getConfigParam:"<<p<<endl;
	list<string> path=StrUtils::pathToList(p);
	XML *paramNode=NULL;
	xmlNode *n=NULL;
	string s;

	//	cout<<" getConfigNode ini:"<<p<<endl;

	string nodeid=StrUtils::decToString(rootID);
	//	cout<<" checkModCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return -1;
	//	cout<<" -- DBGW: CheckModelConfigParam locked"<<endl;
	bool check;
	

	try
	{
		if (isInTransactionMode(dbRPC))
			begin(dbRPC);

		string s = string("SELECT * FROM getConfigNodeByPath('")+ StrUtils::decToString( rootID);
		if(p[0]=='/')
			s+=string("', '")+ p +string("')"); 
		else
			s+=string("', '/")+ p +string("')"); 
		//		cout<<" -- DBGW: checkModelConfigParam call : "<<s<<endl;
		string nodeid;

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))
				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return -1;
		}
		//		cout<<" -- DBGW: checkModelConfigParam res : "<<paramNode->toString()<<endl;
		xmlNode *n=paramNode->getNode("/result/row/[0]");
		if(n==NULL)
		{
			if (isInTransactionMode(dbRPC))
				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			if(paramNode != NULL)
				delete paramNode;
			paramNode = NULL;
			return -1;
		}
		if (isInTransactionMode(dbRPC))
			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
		int res = atoi(n->getCdata().c_str());
		if(paramNode!=NULL)
			delete paramNode;
		return res;
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: CheckModelConfigParam unlock exc"<<endl;
		if(paramNode!=NULL)
			delete paramNode;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
	//	e.serialize()->materializeAndThrow(true);
		throw;
	}

}

bool DBGateway::checkConfigParam(int rootID, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	//	cout<<" getConfigParam:"<<p<<endl;
	list<string> path=StrUtils::pathToList(p);
	XML *paramNode=NULL;
	xmlNode *n=NULL;
	string s;

	string nodeid=StrUtils::decToString(rootID);
	while(path.size()>1)
	{
		string node=path.front();

		//		cout<<" getConfigParam node:"<<node<<endl;
		s = string("SELECT id FROM nodoconfig WHERE padre=")+nodeid+(" AND nombre='") + node+ string("'"); 
		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			return false;
		}
		n=paramNode->getNode("/result/row/id");

		if(n==NULL)
		{
			delete paramNode;
			return false;
		}

		nodeid=string(n->getCdata());
		delete paramNode;
		path.pop_front();
	}


	string node=StrUtils::trim(path.front());
	//	cout<<" getConfigParam last node pare:"<<nodeid<<" node:"<<node<<endl;
	s = string("SELECT valor FROM nodoconfig WHERE padre=") + nodeid + (" AND nombre='") + node+ string("'"); 
	paramNode=call(s, 0, dbRPC);
	if(paramNode==NULL)
	{
		return false;
	}
	n=paramNode->getNode("/result/row/valor");
	//	cout<<"getCPDB:: "<<s<<"  -  "<<paramNode->toString()<<endl;

	if(n==NULL) 
	{
		delete paramNode;
		return false;
	}
	string res=n->getCdata();
	delete paramNode;
	return true;
}


string DBGateway::getModelConfigParam(string fabricante, string modelo, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	list<string> path=StrUtils::pathToList(p);
	string configParam;

	if(path.size()==0)
	{
		throw Exception(0,"Invalid ConfigParam path");
	}
	//	cout<<" getModCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return string("");
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string s = string("SELECT * FROM getModelConfigParamByPath('")+ fabricante +string("', '")+ modelo +string("', '")+ p +string("')"); 
		string nodeid;

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return string("");
		}
		xmlNode *n=paramNode->getNode("/result/row/value");
		if(n!=NULL)
			configParam=n->getCdata();
		else 
			configParam=string("");
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
//		e.serialize()->materializeAndThrow(true);
		throw;
	}
	return configParam;
}
/*
string DBGateway::getProfileConfigParam(int id, RPC *dbRPC)
{
	string configparam;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return string("");
	
	try
	{
		if(transactionMode)
			begin(dbRPC);
		
		string s= string("SELECT * FROM getAllConfigParamsForProfile('")+ StrUtils::decToString(id)+string("')");
		string nodeid;
		
		paramNode=call(s,0,dbRPC);
		if(paramNode==NULL)
		{
			if(transactionMode)
				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return string("");
		}
		xmlNode *n=paramNode->getNode("/result/row/value");
		if(n!=NULL)
			configParam=n->getCdata();
		else
			configParam=string("");
		if(transactionMode)
			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);			
	}
	catch(Exception &e)
	{
		if (isInTransactionMode(dbRPC))

			rollback(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return configParam;	
}
*/

string DBGateway::getDeviceConfigParam(int id, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	//	cout<<" -- DBGW: GetDeviceConfigParam"<<endl;
	list<string> path=StrUtils::pathToList(p);
	string configParam;
	if(path.size()==0)
	{
		throw Exception(0,"Invalid ConfigParam path");
	}
	//	cout<<" getDevCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return string("");
	XML *paramNode=NULL;
	try
	{
//		cout<<" getDeviceConfigParam:: begin? transactionMode:"<<isInTransactionMode(dbRPC)<<" rpc:"<<dbRPC->getLocalAddr().toString()<<" new:"<<newrpc<<endl;
		if (isInTransactionMode(dbRPC))
		{
			begin(dbRPC);
		}
		string s = string("SELECT * FROM getDeviceConfigParamByPath('") + StrUtils::decToString(id) +string("', '")+ p+string("')"); 
		string nodeid;

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
//			cout<<" getDeviceConfigParam:: paramNode == NULL rollback? transactionMode:"<<isInTransactionMode(dbRPC)<<" rpc:"<<dbRPC->getLocalAddr().toString()<<" new:"<<newrpc<<endl;
			if (isInTransactionMode(dbRPC))
			{
				rollback(dbRPC);
			}
			if(newrpc)
				freeRPC(dbRPC);
			return string("");
		}
		xmlNode *n=paramNode->getNode("/result/row/value");
		if(n!=NULL)
			configParam=n->getCdata();
		else 
			configParam=string("");
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
//		cout<<" getDeviceConfigParam:: got value commit? transactionMode:"<<isInTransactionMode(dbRPC)<<" rpc:"<<dbRPC->getLocalAddr().toString()<<" new:"<<newrpc<<" timeout:"<<dbRPC->getTimeout()<<endl;
		if (isInTransactionMode(dbRPC))
		{
			commit(dbRPC);
		}
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		cout<<" getDeviceConfigParam::exception:"<<e.getClass()<<"::"<<e.getMsg()<<" rpc:"<<dbRPC->getLocalAddr().toString()<<" transactionMode:"<<isInTransactionMode(dbRPC)<<" new:"<<newrpc<<endl;
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return configParam;
}

string DBGateway::getSubsystemConfigParam(string ssName, Address ssAddr, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	list<string> path=StrUtils::pathToList(p);
	string configParam;

	if(path.size()==0)
	{
		throw Exception(0,"Invalid ConfigParam path");
	}
	//	cout<<" getModCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return string("");
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string s = string("SELECT * FROM getSubsystemConfigParamByPath('")+  ssName +string("', '") + ssAddr.getIP().toString() +string("', '")+ StrUtils::decToString(ssAddr.getPort()) +string("', '")+ p +string("')"); 
		string nodeid;

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return string("");
		}
		xmlNode *n=paramNode->getNode("/result/row/value");
		if(n!=NULL)
			configParam=n->getCdata();
		else 
			configParam=string("");
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return configParam;
}

int DBGateway::getDeviceConfigNode(int id, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	list<string> path=StrUtils::pathToList(p);
	int res;
	//	cout<<" -- DBGW: GetDevConfigNode lock"<<endl;
	bool newrpc=(dbRPC==NULL);
	//	cout<<" getDevCNode::getRPC"<<endl;
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return -1;
	//	cout<<" -- DBGW: GetDevConfigNode locked"<<endl;
	XML *paramNode=NULL;
	try
	{

		string s = string("Select d.raizconfig FROM dispositivo d WHERE d.id=") + StrUtils::decToString(id); 

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if(newrpc)
				freeRPC(dbRPC);
			return -1;
		}
		xmlNode *n=paramNode->getNode("/result/row/raizconfig");
		if(n==NULL || n->getCdata()==string(""))
		{
			cout<<"error from db at getDeviceConfigNode: "<<s<<":"<<p<<endl<<paramNode->toString()<<endl;
			if(paramNode != NULL)
				delete paramNode;
			paramNode = NULL;
			//			cout<<" -- DBGW: GetDevConfigNode unlock"<<endl;
			if(newrpc)
				freeRPC(dbRPC);
			return -1;
		}

		if(path.size()==0)
		{
			//			cout<<" -- DBGW: GetDevConfigNode unlock"<<endl;
			if(newrpc)
				freeRPC(dbRPC);
			int res=atoi(n->getCdata().c_str());
			if(paramNode != NULL)
				delete paramNode;
			paramNode = NULL;
			return res;
		}

		string nodeid(n->getCdata());
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		res=getConfigNode(atoi(nodeid.c_str()), p, dbRPC);
		//		cout<<" -- DBGW: GetDevConfigNode unlock"<<endl;
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: GetDevConfigNode unlock exc"<<endl;
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return res;
}

int DBGateway::getModelConfigNode(string fabricante, string modelo, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	list<string> path=StrUtils::pathToList(p);
	int res;

	//	cout<<" -- DBGW: GetModelConfigNode lock"<<endl;
	//	cout<<" getModCNode::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return -1;
	//	cout<<" -- DBGW: GetModelConfigNode locked"<<endl;
	XML *paramNode=NULL;
	try
	{
		string s = string("SELECT m.raizinfo FROM modelo m, fabricante f WHERE m.fabricante=f.id AND f.nombre='") + fabricante + string("' AND m.codigo ='") + modelo + string("'");

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if(newrpc)
				freeRPC(dbRPC);
			return -1;
		}
		xmlNode *n=paramNode->getNode("/result/row/raizinfo");
		if(n==NULL)
		{
			cout<<"error from db at getModelConfigNode: "<<s<<":"<<endl<<paramNode->toString()<<endl;
			if(paramNode != NULL)
				delete paramNode;
			paramNode = NULL;
			if(newrpc)
				freeRPC(dbRPC);
			return -1;
		}
		s=n->getCdata();
		if(s==string(""))
		{
			if(paramNode != NULL)
				delete paramNode;
			paramNode = NULL;
			//			cout<<" -- DBGW: GetModelConfigNode unlock"<<endl;
			if(newrpc)
				freeRPC(dbRPC);
			return -1;
		}

		if(path.size()==0)
		{
			//			cout<<" -- DBGW: GetModelConfigNode unlock"<<endl;
			if(paramNode != NULL)
				delete paramNode;
			paramNode = NULL;
			if(newrpc)
				freeRPC(dbRPC);
			return atoi(s.c_str());
		}

		int nodeid=atoi(s.c_str());
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		res=getConfigNode(nodeid, p, dbRPC);
		//		cout<<" -- DBGW: GetModelConfigNode unlock"<<endl;
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: GetModelConfigNode unlock exc"<<endl;
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return res;
}


bool DBGateway::checkModelConfigParam(string fabricante, string modelo, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	list<string> path=StrUtils::pathToList(p);

	//	cout<<" -- DBGW: CheckModelConfigParam lock"<<endl;
	//	cout<<" checkModCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return false;
	//	cout<<" -- DBGW: CheckModelConfigParam locked"<<endl;
	bool check;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string s = string("SELECT * FROM checkModelConfigParamByPath('")+ fabricante +string("', '")+ modelo;
		if(p[0]=='/')
			s+=string("', '")+ p +string("')"); 
		else
			s+=string("', '/")+ p +string("')"); 
		//		cout<<" -- DBGW: checkModelConfigParam call : "<<s<<endl;
		string nodeid;

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))
				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		//		cout<<" -- DBGW: checkModelConfigParam res : "<<paramNode->toString()<<endl;
		xmlNode *n=paramNode->getNode("/result/row/[0]");
		if(n==NULL)
		{
			if(paramNode != NULL)
				delete paramNode;
			paramNode = NULL;
			if (isInTransactionMode(dbRPC))
				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		else 
			if(n->getCdata()== string("true"))
			{
				if(paramNode != NULL)
					delete paramNode;
				paramNode = NULL;
				if (isInTransactionMode(dbRPC))

					commit(dbRPC);
				if(newrpc)
					freeRPC(dbRPC);
				return true;
			}
			else
			{
				if(paramNode != NULL)
					delete paramNode;
				paramNode = NULL;
				if (isInTransactionMode(dbRPC))

					commit(dbRPC);
				if(newrpc)
					freeRPC(dbRPC);
				return false; 
			}
				if(paramNode != NULL)
					delete paramNode;
				paramNode = NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: CheckModelConfigParam unlock exc"<<endl;
				if(paramNode != NULL)
					delete paramNode;
				paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
			rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return false;
}

bool DBGateway::checkDeviceConfigParam(int id, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();

	list<string> path=StrUtils::pathToList(p);

	//	cout<<" -- DBGW: CheckDevConfigParam lock"<<endl;
	//	cout<<" checkDevCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return false;
	//	cout<<" -- DBGW: CheckDevConfigParam locked"<<endl;
	bool check;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string s = string("SELECT * FROM checkDeviceConfigParamByPath('") + StrUtils::decToString(id);
		if(p[0]=='/')
			s+=string("', '")+ p +string("')"); 
		else
			s+=string("', '/")+ p +string("')"); 
		//		cout<<" -- DBGW: checkDeviceConfigParam call : "<<s<<endl;
		string nodeid;

		//	cout<<" checkDevCP::getRPC 1 *:"<<(void*)dbRPC<<endl;
		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			//	cout<<" checkDevCP::freeRPC 1:"<<(void*)dbRPC<<endl;
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		//		cout<<" -- DBGW: checkDeviceConfigParam res : "<<paramNode->toString()<<endl;
		//	cout<<" checkDevCP::getRPC 1.5"<<endl;
		xmlNode *n=paramNode->getNode("/result/row/[0]");
		//	cout<<" checkDevCP::getRPC 2"<<endl;
		if(n==NULL)
		{
			if(paramNode != NULL)
				delete paramNode;
			paramNode = NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		else 
			if(n->getCdata()== string("true"))
			{
				if(paramNode != NULL)
					delete paramNode;
				paramNode = NULL;
				if (isInTransactionMode(dbRPC))

					commit(dbRPC);
				if(newrpc)
					freeRPC(dbRPC);
				return true;
			}
			else
			{
				if(paramNode != NULL)
					delete paramNode;
				paramNode = NULL;
				if (isInTransactionMode(dbRPC))

					commit(dbRPC);
				if(newrpc)
					freeRPC(dbRPC);
				return false; 
			}
		//	cout<<" checkDevCP::getRPC 3"<<endl;
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: CheckDevConfigParam unlock exc"<<endl;
		//	cout<<" checkDevCP::getRPC 4"<<endl;
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	//	cout<<" checkDevCP::getRPC 5"<<endl;
	return false;
}

void DBGateway::deleteConfig(int rootID, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	

	list<string> erase;
	string node = StrUtils::decToString(rootID);
	string s;
	//	cout<<" deleteConfig::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC(NULL,-1);
	if (isInTransactionMode(dbRPC))

		begin(dbRPC);

	try
	{
		s=string("Delete from nodoconfig where id=") + node;
		XML *res=call(s, 1, dbRPC);
		if(res!=NULL)
			delete res;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
	}catch(...)
	{
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
	}
	if(newrpc)
		freeRPC(dbRPC);
	//	cout<<" deleted"<<endl;
}




void DBGateway::deleteConfig(int rootID, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	list<string> path=StrUtils::pathToList(p);
	XML *paramNode=NULL;
	xmlNode *n=NULL;
	string s;

	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC(NULL,-1);

	try
	{
		if (isInTransactionMode(dbRPC))
			begin(dbRPC);
		
		string nodeid=StrUtils::decToString(rootID);
		if(path.size()==0)
		{
			s = string("select * FROM nodoconfig WHERE id=") + StrUtils::decToString(rootID); 
			//		cout<<" deleteconfig root:"<<s<<endl;
			paramNode=call(s, 0, dbRPC);

			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return;
		}

		while(path.size()>1)
		{
			string node=path.front();

			s = string("SELECT id FROM nodoconfig WHERE padre=")+nodeid+(" AND nombre='") + node+ string("'"); 
			paramNode=call(s, 0, dbRPC);
			if(paramNode==NULL)
			{
				if (isInTransactionMode(dbRPC))

					rollback(dbRPC);
				if(newrpc)
					freeRPC(dbRPC);
				return;
			}
			n=paramNode->getNode("/result/row/id");

			if(n==NULL)
			{
				delete paramNode;
				paramNode=NULL;
				if (isInTransactionMode(dbRPC))

					rollback(dbRPC);
				if(newrpc)
					freeRPC(dbRPC);
				return;
			}

			nodeid=string(n->getCdata());
			delete paramNode;
			paramNode=NULL;
			path.pop_front();
		}

		string node=StrUtils::trim(path.front());
		s = string("delete FROM nodoconfig WHERE padre=") + nodeid + (" AND nombre='") + node+ string("'"); 
		//	cout<<" deleteconfig path:"<<p<<" : "<<s<<endl;
		paramNode=call(s, 1, dbRPC);
		//	n=paramNode->getNode("/result/row/valor");
		//	cout<<"getCPDB:: "<<paramNode->toString()<<endl;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: CheckDevConfigParam unlock exc"<<endl;
		//	cout<<" checkDevCP::getRPC 4"<<endl;
		if(paramNode != NULL)
			delete paramNode;
		paramNode = NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}

	if(paramNode!=NULL)
		delete paramNode;
}

void DBGateway::deleteDeviceConfig(int id, string p, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	list<string> path=StrUtils::pathToList(p);

	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC(NULL, -1);
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string s = string("Select d.raizconfig FROM dispositivo d WHERE d.id=") + StrUtils::decToString(id); 

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return;
		}
		xmlNode *n=paramNode->getNode("/result/row/raizconfig");
		if(n==NULL || n->getCdata()==string(""))
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			//			cout<<" -- DBGW: delDevConfig unlock"<<endl;
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return;
		}

		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		//		cout<<" -- DBGW: delDevConfig unlock"<<endl;
		if(newrpc)
			freeRPC(dbRPC);

		string nodeid(n->getCdata());
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		deleteConfig(atoi(nodeid.c_str()), p, dbRPC);
	}
	catch(Exception &e)
	{
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		//		cout<<" -- DBGW: delDevConfig unlock exc"<<endl;
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
}

void DBGateway::deleteSubsystemConfigParam(string ssName, Address ssAddr, string p, RPC* dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	//cout<<"SetMCP:"<<fabricante<<":"<<modelo<<" : "<<p<<" = "<<value<<endl;
	list<string> path=StrUtils::pathToList(p);

	if(path.size()==0)
	{
		throw Exception(0,"Invalid ConfigParam path");
	}

	//	cout<<" setModCP::getRPC"<<endl;
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC(NULL, -1);
	if(dbRPC==NULL)
		return;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string s = string("DELETE FROM nodoconfig WHERE id=(SELECT id FROM getSubsystemConfigParamByPath('") + ssName +string("', '") + ssAddr.getIP().toString() +string("', '")+ StrUtils::decToString(ssAddr.getPort()) +string("', '")+ p+string("'))"); 
//		cout<<" -- DBGW: "<<s<<endl;
		//	cout<<" -- DBGW: setModelConfigParam res : "<<s<<endl;

		paramNode=call(s, 1, dbRPC);
		//		cout<<" -- DBGW: setModelConfigParam res : "<<endl;//paramNode->toString()<<endl;

		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	//	cout<<"commited "<<value<<endl;
}

string DBGateway::getDate()
{
	STACKTRACE_INSTRUMENT();
	
		struct timeval tv;
		gettimeofday(&tv, NULL);
		time_t tvsec=(time_t)tv.tv_sec;
		struct tm *t=localtime(&tvsec);
		char cdata[64];
		float secs=((float)t->tm_sec)+(((float)tv.tv_usec)/1000000);

		sprintf(cdata, "%i-%02i-%02i %02i:%02i:%06.3f",t->tm_year+1900, t->tm_mon+1, t->tm_mday, t->tm_hour, t->tm_min, secs);// t->tm_sec+1, tv.tv_usec/1000);
		return string(cdata);
}

bool DBGateway::logEvent(string missatge, int nivell, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	

	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return false;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string data=getDate();
		// '")+data+string("'
		string s = string("SELECT * FROM loglineevent((select now())::timestamp, '")+ missatge+string("', '")+StrUtils::decToString(nivell)+string("')");

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		xmlNode *n=paramNode->getNode("/result/row/[0]");
		if(n==NULL)
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		else 
		//	if(n->getCdata()== string("true"))
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return true;
		}

		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: CheckModelConfigParam unlock exc"<<endl;
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return false;
}


bool DBGateway::logAlarm(string missatge, int dispositiu, string valor, string cond,  int nivell, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	

	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return false;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string data=getDate();
		//		'")+data+string("'
		string s = string("SELECT * FROM loglinealarma((select now())::timestamp, '")+ missatge+string("', '")+ StrUtils::decToString(dispositiu)+string("', '")+valor+string("', '")+cond+string("', '")+StrUtils::decToString(nivell)+string("')");

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		xmlNode *n=paramNode->getNode("/result/row/[0]");
		if(n==NULL)
		{
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			return false;
		}
		else 
		//	if(n->getCdata()== string("true"))
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return true;
		}
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		//		cout<<" -- DBGW: CheckModelConfigParam unlock exc"<<endl;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return false;
}


bool DBGateway::logDev(string missatge, int dispositiu, int nivell, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	

	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return false;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))
			begin(dbRPC);

		string data=getDate();
		// '")+data+string("'
		string s = string("SELECT * FROM loglinedisp((select now())::timestamp, '")+ missatge+string("', '")+ StrUtils::decToString(dispositiu)+string("', '")+StrUtils::decToString(nivell)+string("')");

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))
				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		xmlNode *n=paramNode->getNode("/result/row/[0]");
		if(n==NULL)
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		else 
		//	if(n->getCdata()== string("true"))
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return true;
		}
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: CheckModelConfigParam unlock exc"<<endl;
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return false;
}

bool DBGateway::logDev(string missatge, int dispositiu, string  nivell, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	

	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return false;
	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string data=getDate();
		// '")+data+string("'
		string s = string("SELECT * FROM loglinedisp((select now())::timestamp, '")+ missatge+string("', '")+ StrUtils::decToString(dispositiu)+string("',( select id from loglevel where nombre='")+nivell+string("'))");

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		xmlNode *n=paramNode->getNode("/result/row/[0]");
		if(n==NULL)
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		else 
		//	if(n->getCdata()== string("true"))
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return true;
		}
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: CheckModelConfigParam unlock exc"<<endl;
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return false;
}

long DBGateway::createRoot(string table, string column, string idCol, string idVal, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return false;

	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);
		string s=string("UPDATE ")+table+string(" SET ")+column+string("=nextval('nodoconfig_id_seq'::regclass) WHERE ")+idCol+string("='")+idVal+string("';"); 
		paramNode=call(s, 1, dbRPC);

		if(paramNode!=NULL)
			delete paramNode;
			
		s = string("SELECT ")+column+string(" FROM ")+table+string(" WHERE ")+idCol+string("='")+idVal+string("';");
			
		paramNode=call(s, 0, dbRPC);
		
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);

		if (paramNode==NULL)
			return 0;
		
		xmlNode *n=paramNode->getNode(string("/result/[0]/")+column);
		long rootID=atol(n->getCdata().c_str());
		
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
			
		return rootID;			
	}
	catch(Exception &e)
	{
		//		cout<<" -- DBGW: CheckModelConfigParam unlock exc"<<endl;
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return 0;
}

long DBGateway::getUnregDataRoot(string name, string ip, int port, RPC *dbRPC)
{
	STACKTRACE_INSTRUMENT();
	
	long root=-1;
	
	bool newrpc=(dbRPC==NULL);
	if(newrpc)
		dbRPC=getRPC();
	if(dbRPC==NULL)
		return false;

	XML *paramNode=NULL;
	try
	{
		if (isInTransactionMode(dbRPC))

			begin(dbRPC);

		string s = string("SELECT * FROM getUnregDataRoot('") + name + string("', '") + ip + string("', '") + StrUtils::decToString(port) + string("')"); 

		paramNode=call(s, 0, dbRPC);
		if(paramNode==NULL)
		{
			if (isInTransactionMode(dbRPC))

				rollback(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		xmlNode *n=paramNode->getNode("/result/row/[0]");
		if(n==NULL)
		{
			if(paramNode!=NULL)
				delete paramNode;
			paramNode=NULL;
			if (isInTransactionMode(dbRPC))

				commit(dbRPC);
			if(newrpc)
				freeRPC(dbRPC);
			return false;
		}
		else 
			root=atol(n->getCdata().c_str());

		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))

			commit(dbRPC);
		if(newrpc)
			freeRPC(dbRPC);
	}
	catch(Exception &e)
	{
		if(paramNode!=NULL)
			delete paramNode;
		paramNode=NULL;
		if (isInTransactionMode(dbRPC))
			try
			{
				rollback(dbRPC);
			}catch(Exception &e)
			{}
		if(newrpc)
			freeRPC(dbRPC);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	return root;
}
