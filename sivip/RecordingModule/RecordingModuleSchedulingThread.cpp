#include <Utils/StrUtils.h>
#ifdef WIN32
#include <Utils/WindowsDefs.h>
#endif
#include <RecordingModule/RecordingModuleSchedulingThread.h>
#include <ModuleAccess/RecordingModuleAccess.h>
#include <ModuleAccess/MetadataModuleAccess.h>
#include <RecordingModule/RecordingModuleInterface.h>
#include <RecordingModule/RecordingModuleTypes.h>
#include <RecordingModule/MaxActiveRecordingsReachedException.h>
#include <Plugins/GestorImageExceptions.h>
#include <Plugins/GestorControlExceptions.h>
#include <Plugins/GestorControlExceptions.h>
#include <Utils/Exception.h>
#include <Sockets/SocketTCP.h>
#include <Exceptions/MetadataModuleSessionNotStablishedException.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

RecordingModuleSchedulingThread::Hour::Hour(): hour(0), 
mins(0),
sec(0)
{}

RecordingModuleSchedulingThread::Hour::Hour(string desc): hour(0), mins(0), sec(0)
{
	int i=desc.find(":");

	if(i!=string::npos)
	{
		hour=atoi(desc.substr(0,i).c_str());
		desc=desc.substr(i+1);
		i=desc.find(":");
		if(i!=string::npos)
		{
			mins=atoi(desc.substr(0,i).c_str());
			desc=desc.substr(i+1);
			sec=atoi(desc.c_str());
		}
	}

	if(!(hour==24 && mins==0 && sec==0)) //cas especial: volém tenir 0:00:00 i 24:00:00
	{
		mins+=sec/60;
		sec=sec%60;

		hour+=mins/60;
		mins=mins%60;

		hour=hour%24;
	}
}

RecordingModuleSchedulingThread::Hour::Hour(int h, int m, int s): hour(h), mins(m), sec(s)
{
	if(!(hour==24 && mins==0 && sec==0)) //cas especial: volém tenir 0:00:00 i 24:00:00
	{
		mins+=sec/60;
		sec=sec%60;

		hour+=mins/60;
		mins=mins%60;

		hour=hour%24;
	}
}

RecordingModuleSchedulingThread::Hour RecordingModuleSchedulingThread::Hour::operator+ (Hour b)
{
	Hour res;
	res.hour = hour+b.hour;
	res.mins = mins+b.mins;
	res.sec = sec+b.sec;

	res.mins+=res.sec/60;
	res.sec=res.sec%60;

	res.hour+=res.mins/60;
	res.mins=res.mins%60;

	res.hour=res.hour%24;
	if(res.hour==0 && res.mins==0 && res.sec==0 && (hour+b.hour>0))
		res.hour=24;
	return res;
}

RecordingModuleSchedulingThread::Hour  RecordingModuleSchedulingThread::Hour::operator- (Hour b)
{
//	cout<< endl<< "   		Hour operator - op this:"<<toString()<<"  -  "<<b.toString()<<endl;
	Hour res;
	res.hour = hour-b.hour;
	res.mins = mins-b.mins;
	res.sec = sec-b.sec;

//	cout<< "   		Hour operator - op res ini:"<<res.toString()<<endl;
	if(res.sec<0)
	{
		res.mins+=(res.sec/60)-1;
		res.sec=res.sec%60;
		res.sec= 60+res.sec;
	}
	else
	{
		res.mins+=res.sec/60;
		res.sec=res.sec%60;
	}

//	cout<< "   		Hour operator - op res sec:"<<res.toString()<<endl;

	if(res.mins<0)
	{
		res.hour+=(res.mins/60)-1;
		res.mins=res.mins%60;
		res.mins= 60+res.mins;
	}
	else
	{
		res.hour+=res.mins/60;
		res.mins=res.mins%60;
	}

//	cout<< "   		Hour operator - op res mins:"<<res.toString()<<endl;

	res.hour=res.hour%24;
	if(res.hour<0)
		res.hour=24+res.hour;

//	cout<< "   		Hour operator - op res:"<<res.toString()<<endl;

	return res;
}

bool RecordingModuleSchedulingThread::Hour::operator< (Hour b)
{
	if(hour<b.hour)
		return true;
	if(hour==b.hour && mins<b.mins)
		return true;
	if(hour==b.hour && mins==b.mins && sec<b.sec)
		return true;

	return false;
}

bool RecordingModuleSchedulingThread::Hour::operator== (const Hour b) const
{
	return (hour==b.hour && mins==b.mins && sec==b.sec);
}

string RecordingModuleSchedulingThread::Hour::toString()
{
	char buf[16];
	sprintf(buf, "%.2i:%.2i:%.2i", hour, mins, sec);
	return string(buf);
}

string RecordingModuleSchedulingThread::Hour::toContString()
{
	char buf[16];
	sprintf(buf, "%.2i%.2i", hour, mins);
	return string(buf);
}

int RecordingModuleSchedulingThread::Hour::secs()
{
	return hour*3600+mins*60+sec;
}


int RecordingModuleSchedulingThread::Hour::waitTime(int dayMask, struct tm t)
{
//	cout<< "   Schedule operator - op t.h:"<<t.tm_hour<<" t.m:"<<t.tm_min<<" t.s:"<<t.tm_sec<<endl;
	int weekday=t.tm_wday;
	Hour h(t.tm_hour, t.tm_min, t.tm_sec);

/*	if(weekday==0) //0-Dg - 6-Ds
		weekday=7; 
	weekday--;
*/
//	char buf[64];
//	strftime(buf, 64, "%d-%m-%Y %T", &t);

	int mask=1;
	mask=mask << weekday; 
//	cout<< "   Schedule operator -   sched:"<<start.toString()<<"   time:"<<buf<<"	mask:"<<StrUtils::hexToString(mask)<<" DayMask:"<<StrUtils::hexToString(dayMask)<<endl;
	if((dayMask & mask)!=0)// 0x01 Dg 0x02 Dl 0x04 Dm 0x08 Dc 0x10Dj 0x20Dv 0x40 Ds ?
	{
//		cout<< "   Schedule operator -  schedulat pel mateix dia"<<endl;
		if(h < *this)
		{
//			cout<< "   Schedule operator -  mateix dia:"<<(start-h).secs()<<endl;
			return (*this-h).secs();	
		}
//		cout<< "   Schedule operator -  mateix dia - ja ha passat la gravació"<<endl;
	}

	for(int i=0;i<7;i++)
	{
		mask=mask<<1;
		if(mask>0x40)
			mask=1;

//	cout<< "   Schedule operator -  iter:"<<i<<"mask:"<<StrUtils::hexToString(mask)<<" DayMask:"<<StrUtils::hexToString(dayMask)<<endl;
		if((dayMask & mask)!=0)
		{
			if(h < *this)
			{
//				cout<< "   Schedule operator -  dia despr:"<<i<<" hora:"<<(*this-h).secs()<<"  ret:"<<(i+1)*24*3600+(*this-h).secs()<<endl;
				return (i+1)*24*3600+(*this-h).secs();	
			}
			else
			{
//				cout<< "   Schedule operator -  dia abans:"<<i<<" hora:"<<(*this-h).secs()<<"  ret:"<<i*24*3600+(*this-h).secs()<<endl;
				return i*24*3600+(*this-h).secs();	
			}
		}
	}
	return 7*24*3600;
}

RecordingModuleSchedulingThread::Schedule::Schedule()
{
	dayMask=0;
	fps=0.0;
	active=false;
	found=false;
	alarm=NULL;
}

RecordingModuleSchedulingThread::Schedule::Schedule(const Schedule &s)
{
	this->start = s.start;
	this->end = s.end;
	this->dayMask = s.dayMask;
	this->fps = s.fps;
	this->devId = s.devId;
	this->active = s.active;
	this->found = s.found;
	this->type = s.type; //0:Normal Recording, 1: monitoring, 2:alarm
	this->dist = s.dist;
//	this->alarm = s.alarm;
	if(s.alarm != NULL)
	{
		s.alarm->mref.lock();
		this->alarm=s.alarm;	
		this->alarm->nrefs++;
		s.alarm->mref.unlock();
	}
	else
		this->alarm=NULL;
}

RecordingModuleSchedulingThread::Schedule::~Schedule()
{
	if(alarm != NULL)
	{
		alarm->mref.lock();
		alarm->nrefs--;
		if(alarm->nrefs<=0)
		{
			alarm->mref.unlock();
			delete alarm;
		}
		else
			alarm->mref.unlock();
	}
}

bool RecordingModuleSchedulingThread::Schedule::operator==(const Schedule &s) const
{
	bool alm=true;

//	cout<<" operator==:"<< (void*) (this->alarm) <<" "<<(void*) (s.alarm)<<endl; 

	if(this->alarm!=NULL && s.alarm!=NULL)
		alm = (*(this->alarm) == *(s.alarm)); 
	else
		if(this->alarm!=NULL || s.alarm!=NULL)
			return false;

	return this->start == s.start && 
		this->end == s.end && 
		this->dayMask==s.dayMask && 
//		this->fps==s.fps && 
		this->devId==s.devId && 
//		this->active==s.active && 
		this->type==s.type && alm;
}

time_t RecordingModuleSchedulingThread::Schedule::operator+ (const time_t t)
{

	int inc=this->start.waitTime(dayMask, (*localtime(&t)));

//	cout <<"operator + inc:"<<inc<<" t:"<<(long)t<<endl;
	return t+inc;
}


RMSServerId::RMSServerId(Address a):addr(a)
{
}

RMSServerId::RMSServerId(char* buf)
{
	this->toLocal(buf);
}

void RMSServerId::toLocal(char* buf)
{
	Endian e;
	char *p=buf;

	dword ip=*((dword*)p);
	p+=4;
	word port=*((word*)p);
	e.desendiana(&port, sizeof(word));
	addr=Address(IP(ip), port);

}

char* RMSServerId::toNetwork(char* buf)
{
	Endian e;
	int sz=this->size();
	char *v;
	if(buf==NULL)
		v=new char[sz];
	else
		v=buf;
	char *p=v;
	memset(v, 0, sz);

	*((dword*)p)=addr.getIP().toDWord();
	p+=4;
	*((word*)(p))=addr.getPort();
	e.endiana(p, sizeof(word));

	return v;
}

int RMSServerId::size()
{
	return 6;
}



RecordingModuleSchedulingThreadInterface::RecordingModuleSchedulingThreadInterface(Address moduleAddress, short type, Canis *cn, RecordingModuleSchedulingThread *sched) : ModuleInterface(moduleAddress, type, cn), sched(sched)
{
	cout<<"RecordingModuleSchedulingThreadInterface::RecordingModuleSchedulingThreadInterface:"<<moduleAddress.toString()<<endl;
	numServices=RecordingModuleSchedulingThreadInterface::serviceCount;
	services=new serviceDef[RecordingModuleSchedulingThreadInterface::serviceCount];
	
	memset(services, 0, sizeof(serviceDef)*RecordingModuleSchedulingThreadInterface::serviceCount);
	cout<<"RecordingModuleSchedulingThreadInterface services "<<RecordingModuleSchedulingThreadInterface::checkRecordingServersServiceId<<" = "<<(void*)RecordingModuleSchedulingThreadInterface::checkRecordingServers<<endl;
	services[RecordingModuleSchedulingThreadInterface::checkRecordingServersServiceId].call=(DefaultModuleCall)RecordingModuleSchedulingThreadInterface::checkRecordingServers;
	services[RecordingModuleSchedulingThreadInterface::checkSchedulesForServerServiceId].call=(DefaultModuleCall)RecordingModuleSchedulingThreadInterface::checkSchedulesForServer;
}

RPCPacket* RecordingModuleSchedulingThreadInterface::checkRecordingServers(RecordingModuleSchedulingThreadInterface *_this, Address *a, void *params)
{

	cout<<"RecordingModuleSchedulingThreadInterface::checkRecordingServers"<<endl;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		NULL, 0, _this->type, 0, true);
	_this->sched->checkLocationsPending=true;
	return pk;
}

RPCPacket* RecordingModuleSchedulingThreadInterface::checkSchedulesForServer(RecordingModuleSchedulingThreadInterface *_this, Address *a, void *params)
{
	RMSServerId p((char*)params);
	cout<<"RecordingModuleSchedulingThreadInterface::checkSchedulesForServer:"<<p.addr.toString()<<endl;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		NULL, 0, _this->type, 0, true);
	_this->sched->recStarted=true;
	return pk;
}


RecordingModuleSchedulingThread::RecordingModuleSchedulingThread(string configFile):Module(configFile),
	cmaLock(true), dmaLock(true), schedulingThreadActive(false), checkingThreadActive(false),
	starterThreads(0), endThreads(0), checkLocationsPending(false), recStarted(false)
{

	STACKTRACE_INSTRUMENT();
cout<<"new SchThreads"<<endl;

	st = new SchedulerThreads(this);

cout<<"ini cma"<<endl;
	cma=new ControlModuleAccess(cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
cout<<"ini dma"<<endl;
	rmstDma=new DecodeModuleAccess(cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
//	ama=new AlarmModuleAccess(rmi->getAddress(), rmi->getType(), rmi->getCanis());
cout<<"ini dbgw"<<endl;
	dbGW=new DBGateway(address, type, cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
cout<<"ini am"<<endl;
	am=new AlarmManager(address.getIP());	



	struct timeval date;
	gettimeofday(&date, NULL);
	int secs=date.tv_sec;
	localtime_r((const time_t*)&secs, &lastRecCheck);

	__class=string("RecordingModuleScheduler");
	this->moduleInterface=(ModuleInterface*)new RecordingModuleSchedulingThreadInterface(this->address, this->type, this->cn, this);
}

RecordingModuleSchedulingThread::~RecordingModuleSchedulingThread()
{
cout<<"WARNING::RecordingModuleSchedulingThread::~RecordingModuleSchedulingThread()"<<endl;
	schedulingThreadActive=false;
	checkingThreadActive=false;
	delete cma;
	delete rmstDma;
//	delete ama;
	delete dbGW;
	delete am;
}

void RecordingModuleSchedulingThread::serveStartUp()
{
	cout<<"******** ["<<StrUtils::getDateString()<<"] RecordingModuleSchedulingThread::serveStartUp ******"<<endl;
	this->waitActive();
	cout << "active, load Sched..." << endl;
	bool loaded=false;
	while(!loaded)
	{
		try
		{
			this->loadAllSchedules(); //si llença una excepció, ja es reiniciarà :P
			loaded=true;
		}catch(Exception e)
		{
			cout <<"RMSchedulingThread::loadAllSchedules Exception: "<< e.getClass() << ": " << e.getMsg() << endl;
			loaded=false;
		} catch(std::exception &stde)
		{
			cout <<"RMSchedulingThread::loadAllSchedules Exception: "<< stde.what() << endl;
			loaded=false;
		}
	}

	RecordingModuleSchedulingThread::SchedulerThreads::threadCall *c=new RecordingModuleSchedulingThread::SchedulerThreads::threadCall();
	c->id=2;
	c->data=NULL;
	this->st->start(c, Thread::Thread::OnExceptionRestart);

	//sched.recordThread();
	RecordingModuleSchedulingThread::SchedulerThreads::threadCall *c2=new RecordingModuleSchedulingThread::SchedulerThreads::threadCall();
	c2->id=0;
	c2->data=NULL;
	this->st->start(c2, Thread::Thread::OnExceptionRestart);
}

void RecordingModuleSchedulingThread::serveShutDown()
{
	cout<<"******** ["<<StrUtils::getDateString()<<"] RecordingModuleSchedulingThread::serveShutDown ******"<<endl;
	this->schedulingThreadActive=false;
	this->checkingThreadActive=false;
	this->startThreadLock.broadcast();
	this->endThreadLock.broadcast();
	this->am->endMonitoring();

//	 for (map<int, CamInfo*>::iterator it=cams.begin() ; it != cams.end(); it++ )
//	{
//		cout<<" stopping camera:"<<it->first<<endl;
//		it->second->startThreadLock.broadcast();
//		it->second->endThreadLock.broadcast();
//	}
	 for (list<Schedule>::iterator it=recordings.begin() ; it != recordings.end(); it++ )
		 it->active=false;
}

void RecordingModuleSchedulingThread::servePause()
{
	cout<<"******** ["<<StrUtils::getDateString()<<"] RecordingModuleSchedulingThread::servePause ******"<<endl;
	this->schedulingThreadActive=false;

	this->startThreadLock.broadcast();
	this->endThreadLock.broadcast();
	this->am->endMonitoring();

	for (list<Schedule>::iterator it=recordings.begin() ; it != recordings.end(); it++ )
		 it->active=false;
}

bool RecordingModuleSchedulingThread::waitActive()
{
	if(!Module::isActive())
	{
		cout<<"RecordingCheckThr: serveShutDown"<<endl;
		this->servePause();	
		while(!Module::isActive())
			sleep(1);
		return true;
	}
	return false;
}

void RecordingModuleSchedulingThread::loadAllSchedules()
{
	//string s("Select d.id, d.ip, d.puerto, m.codigo FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='SONY'");
	//string s("Select d.id, f.nombre, m.codigo FROM dispositivo d, fabricante f, modelo m WHERE d.fabricante=f.id AND d.modelo=m.id");
	string s("SELECT d.id AS disp, d.ip AS ip, n.id AS nodo, f.nombre, m.codigo FROM nodoconfig n, dispositivo d, fabricante f, modelo m  WHERE n.nombre='Schedules' AND d.raizconfig=n.padre AND d.fabricante=f.id AND d.modelo=m.id ORDER BY n.id");

	recLock.wlock();
	try
	{
		cout<<"["<<StrUtils::getDateString()<<"] load cam list"<<endl;
		XML *devInfo=dbGW->call(s, 0);

		xmlNode *n=devInfo->getNode("/result");
		int numCams= atoi((n->getParam("numRows")).c_str());

		//CamInit ci;
		cout<<" loading "<<numCams<<" Cams  Schedules:"<<recordings.size()<<endl;
//		cout<<" devInf:"<<devInfo->toString()<<endl;
		unsigned int maskIP=0;
		int m=cn->getInterfaceMask();
		cout<<" cn->gIfM():"<<m<<endl;
		for (byte i=0; i<m; i++)
		{
			maskIP<<=1;
			maskIP|=1;
		}
		for (byte i=m; i<32; i++)
		{
			maskIP<<=1;
		}
		Endian::from(Endian::xarxa, &maskIP, sizeof(unsigned int));
		IP serv(this->address.getIP().toDWord() & maskIP);
		cout<<"serv:"<<this->address.getIP().toString()<<" = "<<serv.toString()<<endl;
		bool error=false;
	
		list<string> queries;
		string q("");
		int nCamsQ = 0;
		for(int i=0;i<numCams;i++)
		{
//			cout<<" loading cam "<<i<<" / "<<numCams<<endl;
			n=devInfo->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/disp"));
			if(n==NULL)
			{
				if(!error)
					cout<<" ERROR while loading schedules:"<<devInfo->toString()<<endl;
				error=true;
				throw(Exception("Error while loading Schedules"));
				continue;
			}
			int id= atoi(n->getCdata().c_str());
			n=devInfo->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/nombre"));
			if(n==NULL)
			{
				if(!error)
					cout<<" ERROR while loading schedules:"<<devInfo->toString()<<endl;
				error=true;
				throw(Exception("Error while loading Schedules"));
				continue;
			}
			string f=n->getCdata();
			n=devInfo->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/codigo"));
			if(n==NULL)
			{
				if(!error)
					cout<<" ERROR while loading schedules:"<<devInfo->toString()<<endl;
				error=true;
				throw(Exception("Error while loading Schedules"));
				continue;
			}
			string m=n->getCdata();
			n=devInfo->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/nodo"));
			if(n==NULL)
			{
				if(!error)
					cout<<" ERROR while loading schedules:"<<devInfo->toString()<<endl;
				error=true;
				throw(Exception("Error while loading Schedules"));
				continue;
			}
			string nodo=n->getCdata();
			n=devInfo->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/ip"));
			if(n==NULL)
			{
				if(!error)
					cout<<" ERROR while loading schedules:"<<devInfo->toString()<<endl;
				error=true;
				throw(Exception("Error while loading Schedules"));
				continue;
			}
			string sip=n->getCdata();
			int pos=sip.find("/");
			if(pos != string::npos)
				sip = sip.substr(0, pos);
			IP ip(sip);
			IP mip(ip.toDWord()& maskIP);
			unsigned int dist = serv.toDWord() - mip.toDWord();
Endian::from(Endian::xarxa,&dist,sizeof(unsigned int));
//			cout<<" Cam:"<<id<<" n:"<<f<<" ip:"<<ip.toString()<<" cn:"<<cn->getInterfaceMask()<<" "<<hex<<":"<<ip.toDWord()<<" & "<<maskIP<<dec<<" "<<serv.toString()<<" -> "<<mip.toString()<<" dist:"<<dist<<endl;
		/*	CamInfo *cOld=NULL;		
			map<int, CamInfo*>::iterator cIt=cams.find(id);
			if (cIt!=cams.end())
			{
				if (cIt->second!=NULL)
					cOld=cIt->second;
			}
			cams[id] = new CamInfo(id, f, m, this);
			if (cOld!=NULL)
				delete cOld;
		*/	
			map<int, CamInfo*>::iterator cIt=cams.find(id);
			if (cIt==cams.end())
			{
				cams[id] = new CamInfo(id, ip, f, m, this, dist);
			}
			else if(cIt->second->ip.toDWord() != ip.toDWord())
			{
				cout<<" cam ip changed:"<<id<<" from:"<<cIt->second->ip.toString()<<" to:"<<ip.toString()<<endl;
				cams[id]->ip = ip;
			}
			q += string("select '")+StrUtils::decToString(id)+string("' as devId, * from getconfigparamrecursive(")+nodo+string(", '') union ");
			nCamsQ++;
			if(nCamsQ >=maxCamsQuery)
			{
				q=q.substr(0, q.length()-7);
				queries.push_back(q);
				nCamsQ = 0;
				q=string("");
			}
		}

		if(q.length()>0)
		{
			q=q.substr(0, q.length()-7);
			queries.push_back(q);
			nCamsQ = 0;
			q=string("");
		}
//		cout<<" load cams configs"<<q<<endl;

		for(list<Schedule>::iterator it=recordings.begin();it!=recordings.end();it++)
		{
			it->found=false;
		}
		
		cout<<" load cams configs query, num calls:"<<queries.size()<<endl;
		
	 	for (list<string>::iterator it=queries.begin() ; it != queries.end(); it++ )
		{
			string query = *it;
			XML *devsCfg=dbGW->query(query);

			//			cout<<" query res:"<<devsCfg->toString()<<endl;
			xmlNode *root=devsCfg->getNode("/result");

			if(root == NULL)
			{
				cout<<" Error while loading cams Schedules:"<<devsCfg->toString()<<endl;
				throw(Exception("Error while loading Schedules"));
			}

			int numCfg= atoi((root->getParam("numRows")).c_str());
			cout<<" load cams configs result :"<<numCfg<<" rows returned"<<endl;

			//CamInit ci;
			xmlNode *r=NULL;
			string currentDev=string("");
			XML *devXML=NULL;
			list<xmlNode*> l = root->getKidList();
			//for(int i=0;i<numCfg;i++)
			if(l.size() == 0)
			{
				cout<<" error parsing configNodes:"<<devsCfg->toString()<<endl;
				throw(Exception("Error while loading Schedules"));
			}

			for(list<xmlNode*>::iterator it=l.begin();it!=l.end();it++)
			{
				xmlNode *curr = *it;
				n=curr->getNode(string("[0]/devid"));// /row/devid
				if(n != NULL)
				{
					string d=n->getCdata();
					if(d != currentDev)
					{
						if(devXML != NULL)
						{
							int id= atoi(currentDev.c_str());
							cout<<" loadSched:"<<id<<endl;

							unsigned int dist=65536;
							map<int, CamInfo*>::iterator i=cams.find(id);

							if (i!=cams.end())
							{
								CamInfo *ci=i->second;
								dist = ci->dist;
							}
							loadSchedules(id, devXML, dist);


							//					devXML->setRoot(NULL);
							delete devXML; // tb petarà els de devsCfg afegits
						}
						devXML = new XML();
						currentDev=d;
					}
					root->removeKid(curr);
					devXML->getRoot()->addKid(curr);
				}
				else
				{
					cout<<" error parsing configNodes:"<<devsCfg->toString()<<endl;
					throw(Exception("Error while loading Schedules"));
				}
			}
			//i l'ultim :P
			if(devXML != NULL)
			{
				int id= atoi(currentDev.c_str());
				cout<<" loadSched:"<<id<<endl;

				unsigned int dist=65536;
				map<int, CamInfo*>::iterator i=cams.find(id);

				if (i!=cams.end())
				{
					CamInfo *ci=i->second;
					dist = ci->dist;
				}

				loadSchedules(id, devXML, dist);
				//					devXML->setRoot(NULL);
				delete devXML;
			}

			cout<<" remain:"<<devsCfg->toString()<<endl;
			if(devsCfg != NULL)
				delete devsCfg;
		}
		if (devInfo!=NULL)
			delete devInfo;
	/*		
		{
			try
			{	
				loadSchedules(id);
			}
			catch(Exception &e)
			{
				//no s'ha trobat la camera?
				cout<<"CMI:: LoadSched error device:"<<id<<" Except: "<<e.getClass()<<":"<<e.getMsg()<<endl;
			}
		}
	*/	

		for(list<Schedule>::iterator it=recordings.begin();it!=recordings.end();it++)
		{
			if(!(it->found))
			{
				cout<<"loadAllSchedules : Schedule not found in DB: stopping and deleting from list device:"<<it->devId<<" active:"<<it->active<<"  type "<<((it->type==0)?string("sched"):string("alarm"))<<endl;
				if(it->active)
				{
					it->active=false;
					if(it->type==2)
					{
						endAlarmRecording(*it);
					}
					else
					{
						endRecording(*it);
					}
				}
				it=recordings.erase(it);
			}
//			else
//				cout<<"Sched dev Id:"<<it->devId<<"  "<<it->start.toString()<<" - "<<it->end.toString()<<"   in:"<<it->dayMask<<" dist:"<<it->dist<<endl;
		}
		cout<<"loaded, total:"<<recordings.size()<<" Schedules"<<endl;
	}catch(Exception &e)
	{
//		if (e.getMsg().find("commands ignored until end of transaction block")!=string::npos || e.getMsg().find("Already inside a transaction")!=string::npos)
//		{
//			try
//			{
//				cout << "loadSched: -> rollback" << endl;
//				dbGW->rollback();
//				cout << "loadSched: -> rollback ok" << endl;
//			}catch(Exception ex)
//			{
//				cout << "loadSched rollback exception : " << ex.getClass() << ": " << ex.getMsg() << endl;
//			}
//		}
//		else if (e.getMsg().find("Not inside a transaction")!=string::npos)
//		{
//			try
//			{
//				cout << "loadSched: -> begin" << endl;
//				dbGW->begin();
//				cout << "loadSched: -> begin ok" << endl;
//			}catch(Exception ex)
//			{
//				cout << "loadSched begin exception : " << ex.getClass() << ": " << ex.getMsg() << endl;
//			}
//		}
//
//		dbGW->enableTransactionMode();
//
		cout << "loadAllSchedules loadAllSched exception: " << e.getClass() << ": " << e.getMsg() << endl;

		recLock.unlock();
		//e.serialize()->materializeAndThrow(true);
		throw;
	} catch(std::exception &stde)
	{
		cout << "loadAllSchedules loadAllSched std::exception: " << stde.what() << endl;

		recLock.unlock();
		throw;
	}
	recLock.unlock();
}

void RecordingModuleSchedulingThread::loadSchedules(int id, XML *cfg, unsigned int dist)
{
//	int nSched=atoi(dbGW->getDeviceConfigParam(id, string("/Schedules")).c_str());

	cout<<"  load Schedules device:"<<id<<endl;

	int i=1; 
	while(loadSchedule(id, i, cfg, dist))
		i++;
}

bool RecordingModuleSchedulingThread::loadSchedule(int devId, int recId, XML *cfg, unsigned int dist)
{
	string path=string("/Schedules/")+StrUtils::decToString(recId)+string("/");

	cout<<endl<<" ---------------------  load Schedule dev:"<<devId<<" rec:"<<recId<<" cfg:"<<(void*)cfg<<endl;
//	cout<<" cfg:"<<cfg->toString()<<endl;


//	int id=dbGW->getDeviceConfigNode(devId, path);
//	cout<<"node"<<endl;
//	if(id == -1)
//	{
//		cout<<"  Schedule dev:"<<devId<<" rec:"<<recId<<" not found"<<endl;
//		return false;	
//	}
//	string q = string("SELECT * FROM getconfigparamrecursive(")+StrUtils::decToString(id)+string(",'')");
//	XML *res=dbGW->call(q, 0);
//	cout<<"data"<<endl;
//cout<<" res:"<<res->toString()<<endl;
//	xmlNode *n=res->getNode("/result");
//	if(n==NULL)
//	{
//		cout<<"  Schedule dev:"<<devId<<" rec:"<<recId<<" query error:"<<res->toString()<<endl;
//		delete res;
//		return;	
//	}

	xmlNode *n=cfg->getRoot();
	cout<<"loadSched rows "<<(void*)n<<endl;
	int nrows=n->getKidList().size();
	string start("");
	string duration("");
	string week("");
	string fps("");
	string mon("");
	string fpsAlm=string("");
	string almPreDur("");
	string almfps("");
	string almDur("");

		cout<<"loadSched rows:"<<nrows<<" "<<n->getName()<<endl;//" "<<n->toString()<<endl;
		for(int i=0;i<nrows;i++)
		{
			string nom=cfg->getNode("/[0]/["+StrUtils::decToString(i)+"]/name")->getCdata();
			string val=cfg->getNode("/[0]/["+StrUtils::decToString(i)+"]/value")->getCdata();
//			cout<<" dev:"<<devId<<" rec:"<<recId<<" nom:"<<nom<<" : "<<val<<endl;
			if(nom.find(path+string("NormalRecording/fps"))!=string::npos)
				fps = val;
			else if(nom.find(path+string("Monitoring/fps"))!=string::npos)
				mon = val;
			else if(nom.find(path+string("AlarmRecording/fps"))!=string::npos)
				fpsAlm = val;
			else if(nom.find(path+string("AlarmRecording/Pre/Duration"))!=string::npos)
				almPreDur = val;
			else if(nom.find(path+string("AlarmRecording/Pre/fps"))!=string::npos)
				almfps = val;
			else if(nom.find(path+string("AlarmRecording/Duration"))!=string::npos)
				almDur = val;
			else if(nom.find(path+string("Start"))!=string::npos)
				start = val;
			else if(nom.find(path+string("Duration"))!=string::npos) //deixar després de l'AR/Dur
				duration = val;
			else if(nom.find(path+string("Weekdays"))!=string::npos)
				week = val;
		}

	//start per temes de client, que a vegades no inserta be
	if(/*start == string("") || */duration == string("") || week == string("") ||
		(fps == string("") && almfps==string("")))
	{
		cout<<"Sched dev:"<<devId<<" rec:"<<recId<<" not found s:"<<start<<" d:"<<duration<<" w:"<<week<<" fps: (n:"<<fps<<",a:"<<almfps<<")"<<endl;
		return false;
	}

	RecordingModuleSchedulingThread::Schedule s;

	s.start = RecordingModuleSchedulingThread::Hour(start);

	RecordingModuleSchedulingThread::Hour dur(duration);
	s.end=s.start+dur;

	s.dayMask=strtol(week.c_str(),NULL,16);
//	cout<< endl;
//	cout<<"New Scheduling:"<<week<<" : "<< s.dayMask<<endl;

	s.devId=devId;
	cout<<"New Scheduling dev Id:"<<s.devId<<"  "<<s.start.toString()<<" - "<<s.end.toString()<<"   in:"<<s.dayMask<<endl;
	if(fps!=string("") || mon!=string(""))
	{
		s.fps=max(atof(fps.c_str()), atof(mon.c_str()));

//		cout<<" ------ Normal Scheduling path:"<<path<<" fps:"<<fps<<"   mon:"<<mon<<endl;
		s.type=0;
		s.dist=dist;
		this->addSchedule(s);
	}

	if(fpsAlm!=string(""))
	{
		s.fps=atof(fpsAlm.c_str());

		s.alarm=new RecordingModuleSchedulingThread::alarmData();

		cout<<" ------ alarm recording:"<<fpsAlm<<" path:"<<path<<" alm:"<<(void*)s.alarm<<endl;
		s.alarm->pre =0; 
		if(almPreDur!=string(""))
			s.alarm->pre = atoi(almPreDur.c_str());

		s.alarm->fpsPre = 25.0;
		if(almfps!=string(""))
			s.alarm->fpsPre = atoi(almfps.c_str());

		int time=0;
		if(almDur!=string(""))
		{
			almDur = n->getCdata();

			int pos=almDur.find(string(":"));
			if(pos!=string::npos) //hh:mm:ss
			{
				time = 60 * atoi(almDur.substr(0,pos).c_str());

				almDur=almDur.substr(pos+1);
				pos=almDur.find(string(":"));
				if(pos!=string::npos)
				{
					time=time*60; // hores
					time +=  atoi(almDur.substr(0,pos).c_str())*60;
					almDur=almDur.substr(pos+1);
				}
				time += atoi(almDur.c_str());
			}
		}
		s.alarm->post = time;

		cout<<" ------ alarm recording path:"<<path<<" post:"<<s.alarm->post<<endl;
		s.type=2;
		s.dist=dist;
		this->addSchedule(s);
	}

	return true;
}


void* RecordingModuleSchedulingThread::SchedulerThreads::execute(int, void* call)
{
//	sleep(1);
	threadCall *crida=(threadCall*) call;
	cout<<"  ----- execute:"<<crida->id<<" data:"<<(void*)crida->data<<endl;
	switch(crida->id)
	{
		case 0:
			if(!rmst->schedulingThreadActive && Module::isActive())
			{
				cout<<" -------------- Starting scheduling thread -----------"<<endl;
				rmst->schedulingThreadActive=true;
				cout << "start"<<endl;
				rmst->recordThread();
			}
			else
				cout<<" ************** Scheduling thread already started ***********"<<endl;
			break;
		case 1:
		{
			Schedule *s=(Schedule*)crida->data;
			cout<<" -------------- Starting alarm recording thread ----------- s->id:"<<s->devId<<endl;
			if(s!=NULL && Module::isActive())
				rmst->AlarmRecording(*s);
			if(crida->data!=NULL)
			{
				delete (Schedule*) crida->data;
				crida->data=NULL;
			}
			break;
		}
		case 2:
		{
			if(!rmst->checkingThreadActive)
			{
				cout<<" -------------- Starting check thread -----------"<<endl;
				rmst->checkingThreadActive=true;
				rmst->recordCheckingThread();
			}
			else
				cout<<" ************** Checking thread already started ***********"<<endl;
			break;
		}
	}
	delete crida;
	return NULL;
}

void RecordingModuleSchedulingThread::endAlarmRecording(Schedule s)
{
	cout<<"------------RMSchedulingThread:: end alarm recording----------"<<endl;

	s.alarm->stop=true;
}


void RecordingModuleSchedulingThread::endRecording(Schedule s)
{
	endRecording(s.devId);	
}

void RecordingModuleSchedulingThread::endRecording(int devId)
{
	map<int, CamInfo*>::iterator i=cams.find(devId);

	if (i!=cams.end())
	{
		CamInfo *ci=i->second;
		ci->endRecording();
	}
	else
	{
		cout<<"ModuleSchedulingThread::endRecording error: cam"<<devId<<" not found"<<endl;
		//starterThreads--;
	}
}

void RecordingModuleSchedulingThread::endRecordingCall(int devId)
{
	cout<<"------------RMSchedulingThread:: end recording "<<devId<<" ----------"<<endl;
	RecordingModuleAccess rma(devId, cn->getInterfaceMask(), cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
//	startSession(&rma, devId);
	{
		IPCodecInfo codec;

		RMStartSessionParams ssParams;
		ssParams.devId=devId;
		ssParams.date=new RecordingFileDateMetadataChunk();
		ssParams.date->setCurrentDate();
		ssParams.recType=RecordingFileHeaderChunk::REC_SCHEDULED;
		ssParams.fourcc=codec.fourcc;
		ssParams.fpswhole=1;
		ssParams.fpsfrac=0;//mili fps
		ssParams.isRecoded=false;
		ssParams.isCyclic=false;

		try
		{
			//cout<<"													startSession endRec"<<endl;
			rma.startSession(ssParams);
		}
		catch (Exception &e)
		{
			cout <<"Error:RMSchedulingThread::startSession: "<< e.getClass() << ": " << e.getMsg() << endl;
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
	}

//	RecordingModuleInterface::stopRecordingStreamParams strsp;
	try
	{
		cout << "SCPST::endRec stop recording stream " << endl;
		//plg->setMetadataValue(devId, "ScheduledRecording","");
		cmaLock.lock();
		try{
			cma->startSession(devId);
			cma->setMetadataValue("ScheduledRecording","");
			cma->endSession(devId);
		}catch(Exception e)
		{}
		cmaLock.unlock();
		//rma->stopRecordingStream(strsp);
		rma.stopRecordingStream();
	}
	catch (Exception &e)
	{
		if(e.getMsg()!=string("No stream being recorded")) //si ja estava parat, ignorem:P
		{
			cout <<"SCPST::endRec  StopRecordingStream: "<< e.getClass() << ": " << e.getMsg() << endl;

			RMEndSessionParams ep;
			ep.devId=devId;
			try
			{
				cout << "RMST end Session " << endl;
				rma.endSession(ep);
			}
			catch (Exception &e) { }

			//e.serialize()->materializeAndThrow(true);
			throw;
		}
	}

	RMEndSessionParams ep;
	ep.devId=devId;
	try
	{
		cout << "RMST end Session " << endl;
		rma.endSession(ep);
	}
	catch (Exception &e)
	{
		if(e.getMsg().find("Session not stablished")==string::npos) 
		{
			cout <<"RMST::endSession: "<< e.getClass() << ": " << e.getMsg() << endl;
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
	}

}


void RecordingModuleSchedulingThread::startAlarmRecording(Schedule next)
{
	cout<<" --- ------ ---------- start Alarm Recording ("<<next.devId<<")"<<endl;
	SchedulerThreads::threadCall *c=new SchedulerThreads::threadCall();
	c->id=1;
	c->data=new Schedule(next);

	next.alarm->stop=false;

	st->start(c, Thread::Thread::OnExceptionRestart);
}


void RecordingModuleSchedulingThread::AlarmRecording(Schedule next)
{
//	cout<<"*************** alarm recording :"<< next.devId<<" "<<next.type<<" "<<next.active<<" "<<next.fps<<" "<<next.dayMask<<endl;
	DecodeModuleAccess dma(cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
	MetadataModuleAccess *mma=NULL;//(cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
	AlarmModuleAccess ama(address, type, cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
	int bsize=(int)(next.alarm->pre*next.alarm->fpsPre);

//	cout<<" start alarm recording dma.startSession sec:"<<next.alarm->pre<<" fps:"<<next.alarm->fpsPre<<" Buffer:"<<bsize<<endl;
	IPCodecInfo codec;
	try{
		cout<<"RMST:AlmRec dma.startSession "<<next.devId<<endl;
		dma.startSession(next.devId);
		cout<<"RMST:AlmRec getCodecInUse "<<endl;
		codec=dma.getCodecInUse();
		cout<<"RMST:AlmRec setStream "<<endl;
		//dma.endSession(next.devId);
		dma.setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));
	}catch(Exception &e)
	{
		cout<<"RMST:AlmRec::DecModAcces exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;

	}

/*	if(cams[next.devId]->fabricante == string("SONY"))
		try{
			mma=new MetadataModuleAccess(cn);
			cout<<" mma.startSession "<<next.devId<<endl;
			mma->startSession(next.devId);
//			cout<<" mma.started "<<next.devId<<endl;
		}catch(Exception &e)
		{
			cout<<"RecModSchThr::MetadataModAcces.sses exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		}
*/
	//plg->setMetadataValue(next.devId, "AlarmRecording","true");
/*	cmaLock.lock();
	try{
		cma->startSession(next.devId);
		cma->setMetadataValue("AlarmRecording","true");
		cma->endSession(next.devId);
	}catch(Exception e)
	{}
	cmaLock.unlock();
*/
	RecordingModuleAccess rma(next.devId, cn->getInterfaceMask(), cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());

	cout<<"** RMST:AlmRec start alarm recording:"<<(void*)next.alarm<<endl;
	AMAlarmId AlmId;
	AlmId.devId=next.devId;
	AlmId.type=AMAlarmId::Filter;
	AlmId.intId=-1;
	AlmId.strId="MotionAlarm";

	AMAlarmConfig AlmCfg;
	AlmCfg.alarmId=AlmId;
	AlmCfg.data.baseMethod=Alarm::VALUE_DIFFERENT;
	AlmCfg.data.intBaseValue=AlmCfg.data.doubleBaseValue=0;

	try{
	cout<<"** RMST:AlmRec am.Monitor"<<endl;
		am->monitorAlarms();
	AlmCfg.addr=am->getAddr();
	cout<<"** RMST:AlmRec ama.startSession :"<<(next.devId)<<endl;
		ama.startSession(next.devId);
	cout<<"** RMST:AlmRec ama.setMgr"<<endl;
		ama.setManager(am);
	
	cout<<"** RMST:AlmRec ama.configureAlarm(motion) send to:"<<AlmCfg.addr.toString()<<endl;
		ama.configureAlarm(AlmCfg);

	cout<<"** RMST:AlmRec am.end"<<endl;


	}catch(Exception &e)
	{
				cout<<"** RMST:AlmRec::AlmModAcces exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
	}
	while(!next.alarm->stop && Module::isActive()) //fi gravació? :P
	{
		try
		{
			cout<<"** RMST:AlmRec Alarm Recording int stop: "<<next.alarm->stop<<endl;
			int bpos=0;
			int loop=0;
			//		string value("");
			while(!next.alarm->stop && Module::isActive())
			{
				/*	cmaLock.lock();
					try{
					cma->startSession(next.devId);
					value=cma->getMetadataValue("ActivityDetected");
					cma->endSession(next.devId);

					}catch(Exception e)
					{}
					cmaLock.unlock();
					cout<<" Alarm::getMetaDataValue:"<<value<<endl;
					if(value=="true")
					break;
					*/
				if(am->getAlarmStatus(AlmId))
				{
					break;
				}

				if(loop%20==0) //1sec
				{
					cout<<"** RMST:AlmRec loop 20 id:"<<next.devId<<" : "<<cams[next.devId]->fabricante<<endl;
					if(cams[next.devId]->fabricante == string("Mitsubishi"))
					{
						try
						{
			//				cout<<"dma.gcnf"<<endl;
							dma.getCompressedNextFrame();// mantenim stream obert
						}	
						catch(IPSessionNotStablishedException &e)
						{
							cout <<"** RMST:AlmRec ::startSession: IPSesNS "<< e.getClass() << ": " << e.getMsg() << endl;
							try{
								dma.startSession(next.devId);
								cout<<" setStream "<<endl;
								dma.setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));
							}catch(Exception &e)
							{
								cout <<"RMSchedulingThread::startSession - 2: "<< e.getClass() << ": " << e.getMsg() << endl;
							}
						}
						catch(CPSessionNotStablishedException &e)
						{
							cout <<"** RMST:AlmRec ::startSession: CPSesNS "<< e.getClass() << ": " << e.getMsg() << endl;
							try{
								dma.startSession(next.devId);
								cout<<" setStream "<<endl;
								dma.setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));
							}catch(Exception e)
							{
								cout <<"** RMST:AlmRec ::startSession - 2: "<< e.getClass() << ": " << e.getMsg() << endl;
							}
						}
						catch(Exception &e)
						{
							cout <<"** RMST:AlmRec RMSchedulingThread::startSession: "<< e.getClass() << ": " << e.getMsg() << endl;
						}
					}
					else if(cams[next.devId]->fabricante == string("SONY"))
					{
						try
						{
							if(mma == NULL || mma->devId == 0xffffffff)
							{
								try{
									if(mma == NULL)
										mma=new MetadataModuleAccess(cn);
									cout<<"** RMST:AlmRec  mma.startSession 2 "<<next.devId<<endl;
									mma->startSession(next.devId);
//									cout<<" mma.started "<<next.devId<<endl;
								}catch(Exception &e)
								{
									cout<<"** RMST:AlmRec MetadataModAcces.sses exc 2 :"<<e.getClass()<<":"<<e.getMsg()<<endl;
									//e.serialize()->materializeAndThrow(true);
									throw;
								}
							}
//							cout<<" -- mma.getObj"<<endl;
							MetadataModuleTypes::getObjectMetadataParams* p=NULL;
							if(mma != NULL)
								p=mma->getObjectMetadata();// mantenim stream obert
//							cout<<" mma.getObjmetaData -- delete "<<(void*)p<<endl;
							if(p!=NULL)
								delete p;
						}
						catch(MetadataModuleSessionNotStablishedException &e)
						{
							cout <<"** RMST:AlmRec mma.getObjMetadata:sesNotSt. "<< e.getClass() << ": " << e.getMsg() <<" startSession:"<<next.devId<< endl;
							try{
								mma->startSession(next.devId);
								cout<<"** RMST:AlmRec RMSchedulingThread::mma.getObjMetadata:sesNotSt. , started Session with:"<<next.devId<<endl;
							}catch(Exception &e)
							{
								cout <<"** RMST:AlmRec RMSchedulingThread::mma.startSession - 2: "<< e.getClass() << ": " << e.getMsg() << endl;
							}
						}
						catch(IdNotFoundException &e)
						{
							cout <<"** RMST:AlmRec mma.getObjMetadata:id not foud. "<< e.getClass() << ": " << e.getMsg() << endl;
							try{
								mma->startSession(next.devId);
								cout<<"** RMST:AlmRec mma.getObjMetadata:id not found. , started Session with:"<<next.devId<<endl;
							}catch(Exception &e)
							{
								cout <<"** RMST:AlmRec mma.startSession id not found - 2: "<< e.getClass() << ": " << e.getMsg() << endl;
							}
						}
						catch(Exception &e)
						{
							cout <<"** RMST:AlmRec mma.getObjMetadata: "<< e.getClass() << ": " << e.getMsg() << endl;
						}

					}
				}
				
				if(loop%500==0) //25sec ... i el primer loop
				{
					cmaLock.lock();
					try{
						cma->startSession(next.devId);
						cma->setMetadataValue("AlarmRecording", "");
						cma->endSession(next.devId);
					}catch(Exception &e)
					{}
					cmaLock.unlock();
				}
				loop++;
				if(loop%30==0)
				{
					cout<<"** RMST:AlmRec Recording no alm :"<<next.devId<<endl;
//					cout<<"            : "<<next.devId<<" "<<next.type<<" "<<next.active<<" "<<next.fps<<" "<<next.dayMask<<"   id:"<<this->getId()<<endl;
				}
				usleep(50000); //0.05 sec resposta
			}

			if(next.alarm->stop || !Module::isActive())
				break;
			cout<<"** RMST:AlmRec alarm received, start recording device:"<<next.devId<<" ---------------"<<endl;

			RMStartSessionParams ssParams;

			ssParams.devId=next.devId;
			ssParams.date=new RecordingFileDateMetadataChunk();
			ssParams.date->setCurrentDate();
			ssParams.recType=RecordingFileHeaderChunk::REC_ALARM;
			ssParams.fourcc=codec.fourcc;
			ssParams.fpswhole=(int)next.fps;
			ssParams.fpsfrac=(int)((next.fps-(int)next.fps)*1000.0);//mili fps
			ssParams.isRecoded=false;
			ssParams.isCyclic=false;
//	cout<<"alarm recording 3"<<endl;

			try
			{
				//cout<<"													startSession alm"<<endl;
				rma.startSession(ssParams);
			}
			catch (Exception &e)
			{
				cout <<"** RMST:AlmRec Error:rma.startSession: "<< e.getClass() << ": " << e.getMsg() << endl;
				if(e.getMsg().find(string("Session already started")) == string::npos && e.getMsg().find(string("Stream already being recorded")) == string::npos)
				{
					//e.serialize()->materializeAndThrow(true);
					throw;
				}
			}

//			RecordingModuleInterface::startRecordingStreamParams srsp;
			try
			{
				rma.startRecordingStream();
			}
			catch (Exception &e)
			{
					cout <<"** RMST:AlmRec Error:rma.StartRecording: "<< e.getClass() << ": " << e.getMsg() << endl;
				if(e.getClass()!= string("RecordingModuleException") || e.getMsg().find(string("Stream already being recorded")) == string::npos)//(e.getMsg() != string("Stream already being recorded"))
				{
					//e.serialize()->materializeAndThrow(true);
					throw;
				}
			}
			cmaLock.lock();
			try{
				cma->startSession(next.devId);
				cma->setMetadataValue("AlarmRecording","true");
				cma->endSession(next.devId);
			}catch(Exception &e)
			{}
			cmaLock.unlock();


			//		cout<<"  RMSchedulingThread:: Recording alarm started, waiting post:"<<next.alarm->post<<endl;

			Timer t;
			t.start();
			unsigned long usecsRemaining;
			do
			{
				TimerInstant ti=t.time();
				usecsRemaining=(/*(unsigned long)next.alarm->post*/5*1000000)-(unsigned long)ti.useconds();
				cout<<"** RMST:AlmRec spent time: "<<ti.useconds()<<" sleep:"<<usecsRemaining<<endl;//<<"       : "<<next.devId<<" "<<next.type<<" "<<next.active<<" "<<next.fps<<" "<<next.dayMask<<"   id:"<<this->getId()<<endl;
				if(usecsRemaining > 5* 1000000.0)
				{
					cout<<" --- ti.usecs:"<<ti.useconds()<<endl;
					break;
				}

				if(usecsRemaining > 500000.0)
				{
					cout<<"** RMST:AlmRec sleep "<< usecsRemaining<<" : "<< usecsRemaining/1000000<<" f:"<<cams[next.devId]->fabricante <<endl;
					if(cams[next.devId]->fabricante == string("Mitsubishi"))
					{
						try
						{
							cout<<"** RMST:AlmRec dma.gcnf"<<endl;
							dma.getCompressedNextFrame();// mantenim stream obert
						}	
						catch(IPSessionNotStablishedException &e)
						{
							cout <<"** RMST:AlmRec ::startSession: IPSesNS "<< e.getClass() << ": " << e.getMsg() << endl;
							try{
								dma.startSession(next.devId);
								cout<<"** RMST:AlmRec  setStream "<<endl;
								dma.setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));
							}catch(Exception e)
							{
								cout <<"RMSchedulingThread::startSession - 2: "<< e.getClass() << ": " << e.getMsg() << endl;
							}
						}
						catch(CPSessionNotStablishedException &e)
						{
							cout <<"** RMST:AlmRec ::startSession: CPSesNS "<< e.getClass() << ": " << e.getMsg() << endl;
							try{
								dma.startSession(next.devId);
								cout<<"** RMST:AlmRec  setStream "<<endl;
								dma.setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));
							}catch(Exception e)
							{
								cout <<"** RMST:AlmRec ::startSession - 2: "<< e.getClass() << ": " << e.getMsg() << endl;
							}
						}
						catch(Exception &e)
						{
							cout <<"** RMST:AlmRec ::startSession: "<< e.getClass() << ": " << e.getMsg() << endl;
						}
					}
					else if(cams[next.devId]->fabricante == string("SONY"))
					{
						try
						{
							if(mma == NULL || mma->devId == 0xffffffff)
							{
								try
								{
									if(mma == NULL)
										mma=new MetadataModuleAccess(cn);
									cout<<"** RMST:AlmRec mma.startSession 1 "<<next.devId<<endl;
									mma->startSession(next.devId);
//									cout<<" mma.started "<<next.devId<<endl;
								}catch(Exception &e)
								{
									cout<<"** RMST:AlmRec MetadataModAcces.sses 1 exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
									//e.serialize()->materializeAndThrow(true);
									throw;
								}
							}
//							cout<<" -- mma.getObj"<<endl;
							MetadataModuleTypes::getObjectMetadataParams* p=NULL;
							if(mma != NULL)
								p=mma->getObjectMetadata();// mantenim stream obert
//							cout<<" mma.getObjmetaData -- delete "<<(void*)p<<endl;
							if(p!=NULL)
								delete p;
						}
						catch(MetadataModuleSessionNotStablishedException &e)
						{
							cout <<"** RMST:AlmRec mma.getObjMetadata:sesNotSt. "<< e.getClass() << ": " << e.getMsg() << endl;
							try{
								mma->startSession(next.devId);
								cout<<"** RMST:AlmRec mma.getObjMetadata:sesNotSt. , started Session with:"<<next.devId<<endl;
							}catch(Exception &e)
							{
								cout <<"** RMST:AlmRec mma.startSession - 2: "<< e.getClass() << ": " << e.getMsg() << endl;
							}
						}
						catch(IdNotFoundException &e)
						{
							cout <<"** RMST:AlmRec mma.getObjMetadata:id not foud. "<< e.getClass() << ": " << e.getMsg() << endl;
							try{
								mma->startSession(next.devId);
								cout<<"** RMST:AlmRec mma.getObjMetadata:id not found. , started Session with:"<<next.devId<<endl;
							}catch(Exception &e)
							{
								cout <<"** RMST:AlmRec mma.startSession id not found - 2: "<< e.getClass() << ": " << e.getMsg() << endl;
							}
						}
						catch(Exception &e)
						{
							cout <<"** RMST:AlmRec mma.getObjMD: "<< e.getClass() << ": " << e.getMsg() << endl;
						}

					}

					sleep(1);
				//	usleep(500000.0);
				}else
				{
				//	cout<<"usleep usecs"<<endl;
					usleep(usecsRemaining);
				}
				if(am->getAlarmStatus(AlmId)) // post segons a partir del fi de l'Alarma
				{
					cout<<"** RMST:AlmRec t.start()"<<endl;
					t.start();
				}
			/*	else
				{
					cout<<"** RMST:AlmRec almStat:"<<endl;
	list<AMAlarmValue> ret = am->getActiveAlarms();
	list<AMAlarmValue>::iterator it = ret.begin();

	for (; it!=ret.end(); it++)
		cout<<"** RMST:AlmRec "<<((AMAlarmValue)*it).alarmId.devId<<" "<<((AMAlarmValue)*it).alarmId.strId<<" "<<((AMAlarmValue)*it).value<<endl;
				}*/
			}while (usecsRemaining>0);


			cout<<"** RMST:AlmRec end Alarm Recording -------------"<<endl;
			//plg->setMetadataValue(next.devId, "AlarmRecording", "");
			//		cout<<"cma.lock();"<<endl;
			cmaLock.lock();
			//		cout<<"cma.locked"<<endl;
			try{
				cma->startSession(next.devId);
				cma->setMetadataValue("AlarmRecording", "");
				cma->endSession(next.devId);
			}catch(Exception &e)
			{}
			//		cout<<"cma.unlock();"<<endl;
			cmaLock.unlock();
			//		cout<<"cma.unlocked;"<<endl;

//			RecordingModuleInterface::stopRecordingStreamParams strsp;
			try
			{
				cout<<"** RMST:AlmRec rma.stopRecStr;"<<endl;
				rma.stopRecordingStream();
				cout<<"** RMST:AlmRec rma.endSesParms;"<<endl;
			}
			catch (Exception &e)
			{
				if(e.getMsg()!=string("No stream being recorded")) //si ja estava parat, ignorem:P
				{
					cout <<"** RMST:AlmRec endAlmRec  StopRecordingStream: "<< e.getClass() << ": " << e.getMsg() << endl;
					//e.serialize()->materializeAndThrow(true);
					throw;
				}
			}

			try
			{
				RMEndSessionParams ep;
				ep.devId=next.devId;
				cout<<"** RMST:AlmRec rma.endSes;"<<endl;
//			cout<<"													endSession Alarm"<<endl;
				rma.endSession(ep);
				cout<<"** RMST:AlmRec /rma.endSes;"<<endl;
			}
			catch (Exception &e) { }

			cout<<"** RMST:AlmRec end Alarm Recording -- restart "<<next.alarm->stop<<endl;
		}catch(Exception &e)
		{
			cout<<"** RMST:AlmRec ::Scheduled alarm Recording Thread exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			//abort();
		}
	}

	next.alarm->stop= true;
//	ama.removeAlarm(AlmCfg);

	try
	{
		ama.endSession(next.devId);
	}catch(Exception e)
	{}

	try
	{
		dma.endSession(next.devId);
	}catch(Exception e)
	{}
	cout<<"----------- RMST:AlmRec end Alarm Recording ----------"<<endl;
	if(mma!=NULL)
	{
		try
		{
			mma->endSession(next.devId);
		}catch(Exception e)
		{}
		delete mma;
	}

//	dma.endSession(next.devId);
//	cma->endSession(next.devId);
}


void RecordingModuleSchedulingThread::startRecording(Schedule next)
{ 
	cout<<"RecordingModuleSchedulingThread::startRecording : "<<next.devId<<endl;
	map<int, CamInfo*>::iterator i=cams.find(next.devId);

	if (i!=cams.end())
	{
		CamInfo *ci=i->second;
//	cout<<"RecordingModuleSchedulingThread::startRecording ci->startRec "<<endl;
		ci->startRecording(next);
	}
	else
	{
		cout<<"oduleSchedulingThread::startRecording error: cam"<<next.devId<<" not found"<<endl;
	}
}

IPCodecInfo RecordingModuleSchedulingThread::getCodecForDevice(int devId)
{
	IPCodecInfo codec;
	dmaLock.lock();
	try{
		rmstDma->startSession(devId);
		codec=rmstDma->getCodecInUse();
		rmstDma->endSession(devId);
	}
	catch (Exception &e)
	{
		cout <<"RMST: getCodecForDevice Exception:"<< e.getClass() << ": " << e.getMsg() << endl;
		dmaLock.unlock();
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	dmaLock.unlock();
	return codec;	
}

void RecordingModuleSchedulingThread::startRecordingCall(Schedule next)
{
	IPCodecInfo codec;
	string debugStr;

/*	dmaLock.lock();
//	debugStr = string("RecordingModuleSchedulingThread::startRecording :") + StrUtils::decToString(next.devId);
	cout << debugStr << endl;
	debugStr = string("===== dma:") + StrUtils::decToString(next.devId);
	cout << debugStr << endl;
	try{
		rmstDma->startSession(next.devId);
		codec=rmstDma->getCodecInUse();
		rmstDma->endSession(next.devId);
	}
	catch (Exception &e)
	{
		dmaLock.unlock();
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	debugStr = string("===== /dma:") + StrUtils::decToString(next.devId);
	cout << debugStr << endl;
	dmaLock.unlock();
*/
	debugStr = string("[") + StrUtils::getDateString() + string("]") + string("startRecordingCall::Checking connection to:") + StrUtils::decToString(next.devId) + string(":") + cams[next.devId]->ip.toString();
	cout << debugStr << endl;
	try
	{
		Address a(cams[next.devId]->ip, 80);
//		debugStr = string("sock:") + a.toString();
//		cout << debugStr << endl;
		SocketTCP s(a);
//		debugStr = string("sock.timeout:");
//		cout << debugStr << endl;
		s.setTimeout(1000);
//		debugStr = string("sock.establish:") + s.getAddr().toString();
//		cout << debugStr << endl;
		s.establish();
//		debugStr = string("sock.close");
//		cout << debugStr << endl;
		s.closeSocket();
		debugStr = string("startRecordingCall::Checked connection to:") + StrUtils::decToString(next.devId) + string(":") + cams[next.devId]->ip.toString() + string(", checking DecodeModule");
		cout << debugStr << endl;
	}
	catch (Exception &ex)
	{
		debugStr = string("[") + StrUtils::getDateString() + string("]") + string("error: no connection to ") + StrUtils::decToString(next.devId) + string(":") + cams[next.devId]->ip.toString() + string(" ex:") + ex.getClass() + string(":") + ex.getMsg();
		cout << debugStr << endl;
/*		try
		{
			s.closeSocket();
		}
		catch (Exception &ex)
		{}
*/		//ex.serialize()->materializeAndThrow(true);
		throw;
	}
	DecodeModuleAccess dma(cn);
	debugStr = string("startRecordingCall::") + StrUtils::decToString(next.devId) + string(" dma.startSes");
	cout << debugStr << endl;
	dma.startSession(next.devId);
	try
	{
		debugStr = string("startRecordingCall::") + StrUtils::decToString(next.devId) + string(" dma.getCodecinUse");
		cout << debugStr << endl;
		codec=dma.getCodecInUse();
		debugStr = string("DecodeModuleAccess::getCodecInUse dev:") + StrUtils::decToString(next.devId) + string(" 4cc:") + string((char*)&codec.fourcc, sizeof(codec.fourcc)) + string(" qual:") + StrUtils::decToString(codec.quality) + string(" br:") +  StrUtils::decToString(codec.bitrate);
		cout << debugStr << endl;
	}
	catch (Exception &e)
	{
		debugStr =  string("[") + StrUtils::getDateString() + string("]") + string("DecodeModuleAccess::getCodecInUse exception dev:") + StrUtils::decToString(next.devId) + string(":") + e.getClass() + string(":") + e.getMsg();
		cout << debugStr << endl;
		try
		{
			dma.endSession(next.devId);
		}
		catch (Exception &ex)
		{
			debugStr = string("DecodeModuleAccess::getCodecInUse exception endSes Exc dev:") + StrUtils::decToString(next.devId) + string(":") + ex.getClass() + string(":") + ex.getMsg();
			cout << debugStr << endl;
		}
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	try
	{
		debugStr = string("startRecordingCall::") + StrUtils::decToString(next.devId) + string(" dma.endSes");
		cout << debugStr << endl;
		dma.endSession(next.devId);
		debugStr = string("startRecordingCall::") + StrUtils::decToString(next.devId) + string(" dma.endSes");
		cout << debugStr << endl;
	}
	catch (Exception &ex)
	{
		debugStr = string("DecodeModuleAccess::dma endSes:") + StrUtils::decToString(next.devId) + string(":") + ex.getClass() + string(":") + ex.getMsg();
		cout << debugStr << endl;
	}

	RMStartSessionParams ssParams;
	ssParams.devId=next.devId;
	ssParams.date=new RecordingFileDateMetadataChunk();
	ssParams.date->setCurrentDate();
	ssParams.recType=RecordingFileHeaderChunk::REC_SCHEDULED;
	ssParams.fourcc=codec.fourcc;
	ssParams.fpswhole=(int)next.fps;
	ssParams.fpsfrac=(int)((next.fps-(int)next.fps)*1000.0);//mili fps
	ssParams.isRecoded=false;
	ssParams.isCyclic=false;

	RecordingModuleAccess rma(next.devId, cn->getInterfaceMask(), cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
	debugStr = string("----------- [") + StrUtils::getDateString() + string("] RMSchedulingThread:: Start Recording device:") + StrUtils::decToString(next.devId) + string(" ---------- ");
	cout << debugStr << endl;
	try
	{
//		debugStr = string("rma.sses");
//		cout << debugStr << endl;
	debugStr = string("startSession start Sched Rec ") + StrUtils::decToString(next.devId) + string(" sses device:") + StrUtils::decToString(ssParams.devId);
	cout << debugStr << endl;
		rma.startSession(ssParams);
//		debugStr = string("rma.sses started");
//		cout << debugStr << endl;
	}
	catch (Exception &e)
	{
		debugStr = string("Error:RMSchedulingThread::startSession: ") + StrUtils::decToString(next.devId) + string(" : ") + e.getClass() + string( ": " ) + e.getMsg();
		cout << debugStr << endl;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}

//	RecordingModuleInterface::startRecordingStreamParams srsp;
	try
	{
		debugStr = string("rma.startRecStr --> ") + StrUtils::decToString(next.devId);
		cout << debugStr << endl;
		rma.startRecordingStream();
		debugStr = string("rma.startRecStr <-- ") + StrUtils::decToString(next.devId);
		cout << debugStr << endl;
		//cma->setMetadataValue("ScheduledRecording","true");
//		plg->setMetadataValue(next.devId, "ScheduledRecording","true");
		string value("");
		cmaLock.lock();
		try{
			cma->startSession(next.devId);
			cma->setMetadataValue("ScheduledRecording","true");
			cma->endSession(next.devId);
		}catch(Exception e)
		{}
		cmaLock.unlock();
	}
	catch(MaxActiveRecordingsReachedException &e)
	{
		cout<<"["<<StrUtils::getDateString()<<"] RecordingModule::StartRecording MaxActiveRecordingsReachedException received"<<endl;
		//	select * FROM getSubsysByDistance('10.93.56.128', 23, 'RecordingModule', 0, 99999);
		bool started=false;
		string q("select id, host(ip) as ip, puerto FROM getSubsysByDistance('"+cams[next.devId]->ip.toString()+"', "+StrUtils::decToString(cn->getInterfaceMask())+", 'RecordingModule', 0, 99999);");
		try
		{
			XML *serv=dbGW->query(q);
			xmlNode *n=serv->getNode("/result");
			int nRows= atoi((n->getParam("numRows")).c_str());
			cout<<"MaxActiveRecordingsReachedException:"<<q<<" trying:"<<nRows<<" servers"<<endl;
			for(int i=0;i<nRows && !started;i++)
			{
				xmlNode *ipNode=serv->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/ip"));
				xmlNode *portNode=serv->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/puerto"));
				xmlNode *idNode=serv->getNode(string("/result/[")+StrUtils::decToString(i)+string("]/id"));
				if(ipNode != NULL && portNode != NULL && idNode != NULL)
				{

					cout<<"MaxActiveRecordingsReachedException: dev:"<<next.devId<<" ip:"<<(ipNode->getCdata())<<" port:"<<portNode->getCdata()<<endl;
					try
					{
						Address best(IP(ipNode->getCdata()), atoi(portNode->getCdata().c_str()));
						cout<<"["<<StrUtils::getDateString()<<"] MaxActiveRecordingsReachedException: starting new recording stream for:"<<next.devId<<" @"<<best.toString()<<endl;
						RecordingModuleAccess rma2(best, cn);
						try
						{
							rma2.startSession(ssParams);
						}
						catch(Exception &e)
						{
							cout<<"MaxActiveRecordingsReachedException: exception while starting new recording stream for:"<<next.devId<<" @"<<best.toString()<<" startSession:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
							cout<<"sses rma2 ->continue"<<endl;
							continue;
						}

						try
						{
							rma2.startRecordingStream();
							started=true;
							string ssId = idNode->getCdata();
							string ipMask = cams[next.devId]->ip.toString()+"/"+StrUtils::decToString(cn->getInterfaceMask());
							setRecordingModuleForDevice(ipMask, ssId);
						}
						catch(Exception &e)
						{
							cout<<"MaxActiveRecordingsReachedException: exception while starting new recording stream for:"<<next.devId<<" @"<<best.toString()<<" started:"<<started<<" startRecStream:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
							if(e.getClass() == string("RecordingModuleException") && e.getMsg().find(string("Stream already being recorded")) != string::npos)//(e.getMsg() != string("Stream already being recorded"))
							{
								started=true;
								string ssId = idNode->getCdata();
								string ipMask = cams[next.devId]->ip.toString()+"/"+StrUtils::decToString(cn->getInterfaceMask());
								setRecordingModuleForDevice(ipMask, ssId);
							}
						}

						try
						{
							RMEndSessionParams ep;
							ep.devId=next.devId;
							rma2.endSession(ep);
						}
						catch(Exception &e)
						{
							cout<<"MaxActiveRecordingsReachedException: exception while ending session at new recording stream for:"<<next.devId<<" @"<<best.toString()<<" endSession:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
						}
						if(started)
							cout<<"MaxActiveRecordingsReachedException: started new recording stream for:"<<next.devId<<" @"<<best.toString()<<endl;
					}
					catch(Exception &e)
					{
						cout<<"MaxActiveRecordingsReachedException: exception while creating new RecordingModuleAccess for:"<<next.devId<<" startSession:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
						continue;
					}
				}
			}
		}
		catch(Exception &e)
		{
			cout<<"MaxActiveRecordingsReachedException: getSubsysByDistance exception :"<<next.devId<<" q:"<<q<<" endSession:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
		}
	}
	catch (Exception &e)
	{
		debugStr =  string("[") + StrUtils::getDateString() + string("]") + string("Error:RecordingModule::StartRecording exception:") + StrUtils::decToString(next.devId) + string(" ") + e.getClass() + string( ": " ) + e.getMsg();
		cout << debugStr << endl;
		RMEndSessionParams ep;
		ep.devId=next.devId;
		//	debugStr = string("rma.endSes");
//	cout << debugStr << endl;
		rma.endSession(ep);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	debugStr = string("rma.endSes --> ") + StrUtils::decToString(next.devId);
	cout << debugStr << endl;
	RMEndSessionParams ep;
	ep.devId=next.devId;
//	debugStr = string("rma.endSes");
//	cout << debugStr << endl;
	rma.endSession(ep);
	debugStr = string("rma.endSes <-- ") + StrUtils::decToString(next.devId);
	cout << debugStr << endl;

}


void RecordingModuleSchedulingThread::checkRecordings()
{	//timeout -> comprovem que les coses encara estiguin gravant :P
	cout<<"RMSchedulingThread:: checkRecordings"<<endl;

	recLock.rlock();
	try
	{

		RMListRecordingsParams lrp;
		struct timeval date;
		struct tm t;//(const time_t*)(&aux))

		gettimeofday(&date, NULL);
#ifdef WIN32
		unsigned long long secs=(date.tv_sec);
		struct tm *ptm;
		ptm= localtime((const time_t*)&secs);
		t= *ptm;
#else
		t= *localtime((const time_t*)&date.tv_sec);
#endif
		lrp.absStartDate.secs=date.tv_sec-60;//les ultimes només que les cams poden fer talls molt llargs
		lrp.absStartDate.millis=date.tv_usec/1000;
		lrp.absStartDate.isValid=true;
//		lrp.absEndDate.secs=date.tv_sec+120; //les ultimes només
//		lrp.absEndDate.millis=date.tv_usec/1000;
//		lrp.absEndDate.isValid=true;
		lrp.absEndDate.isValid=false;
//		lrp.absStartDate.isValid=false;
		//	lrp.nRecsFilter=0;
		//	lrp.nRecordings=0;
		//	lrp.recordings=NULL;
		lrp.nRecsFilter=1;
		lrp.nRecordings=0;
		lrp.recordings=new RMListRecordingsParams::recording[1];
		cout<<"["<<StrUtils::getDateString()<<"] RMSchedulingThread: checking Recordings "<<endl;

		lrp.recordings[0].id=0xffffffffffffffffll; //qualsevol
		lrp.recordings[0].devId=0xffffffff;
		lrp.recordings[0].recType=RecordingFileHeaderChunk::REC_SCHEDULED;
	//	lrp.recordings[0].startDate.isValid=false;
	//	lrp.recordings[0].endDate.isValid=false;
			lrp.recordings[0].endDate.isValid=false;
			lrp.recordings[0].startDate.isValid=false;
//			lrp.recordings[0].startDate.secs=date.tv_sec-60; //les ultimes només
//			lrp.recordings[0].startDate.millis=date.tv_usec/1000;
//			lrp.recordings[0].startDate.isValid=true;
//			lrp.recordings[0].endDate.secs=date.tv_sec+60; //les ultimes només
//			lrp.recordings[0].endDate.millis=date.tv_usec/1000;
//			lrp.recordings[0].endDate.isValid=true;
			
		RMListRecordingsParams *recs;
		RecordingModuleAccess rma(cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());
		try{
			recs=rma.listRecordings(lrp);
		}catch(Exception &e)
		{
			cout<<" listRecordings exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
		cout<<"RMSchedulingThread:: checkRecordings 2, recs:"<<(void*)recs<<endl;
		lrp.recordings=NULL;
//		cout<<"comprovem schedules programats (tipus programats:"<<RecordingFileHeaderChunk::REC_SCHEDULED<<")  recordings programats ControlModule :"<<recordings.size()<<endl;
		//comprovem que els schedules que haurien d'estar actius, ho estàn
		for(list<Schedule>::iterator it=recordings.begin(); it!=recordings.end(); it++)
		{

			int tstart;
			tstart = it->start.waitTime(it->dayMask, t); 
			int tend;
			tend = it->end.waitTime(it->dayMask, t); 
//			cout<< "  Schedule dev:"<<it->devId<<":"<<tstart<< "->"<<tend<<"  ("<<it->start.toString()<<" -> "<<it->end.toString()<<") type:"<<it->type<<endl;

			/*		struct timeval date;
					gettimeofday(&date, NULL);
					timespec wait;
					wait.tv_sec=date.tv_sec+tstart;
					wait.tv_nsec=0;
					struct tm w=*localtime((const time_t*)&(wait.tv_sec));
					cout<<"  start:"<<wait.tv_sec<<" == "<<w.tm_mday<<"/"<<w.tm_mon<<"/"<<w.tm_year+1900<<" "<<w.tm_hour<<":"<<w.tm_min<<endl;
					wait.tv_sec=date.tv_sec+tend;
					wait.tv_nsec=0;
					w=*localtime((const time_t*)&(wait.tv_sec));
					cout<<"  end:"<<wait.tv_sec<<" == "<<w.tm_mday<<"/"<<w.tm_mon<<"/"<<w.tm_year+1900<<" "<<w.tm_hour<<":"<<w.tm_min<<endl;
					*/
			bool dia = (it->start.secs()==0 && it->end.secs() == 24*3600); // de 00:00:00 a 24:00:00
			int weekday=t.tm_wday;
			dia = dia && ((((int)1)<<weekday) & it->dayMask);
			if(tend<tstart || dia) // hauria de gravar
			{
				cout<< "	  hauria d'estar gravant, busquem id:"<<it->devId<<" type:"<<it->type<<"..."<<endl;
				if(it->type==0) // normal(scheduled) recording
				{
					bool found=false;
					cout<< "		recs:"<<recs->nRecordings<<endl;
					for(int recIdx=0;recIdx<recs->nRecordings;recIdx++)
					{
		//				cout<< "		rec:"<<recIdx<<" end:"<<recs->recordings[recIdx].endDate.isValid<<" -- dev:"<< recs->recordings[recIdx].id<<" req:"<<it->devId<<endl;
		//				cout<< "		rec:"<<recIdx<<" end:"<<recs->recordings[recIdx].endDate.isValid<<" endTime:"<<recs->recordings[recIdx].endDate.secs<<":"<<recs->recordings[recIdx].endDate.millis<<endl;
						
						if(recs->recordings[recIdx].devId!=it->devId || recs->recordings[recIdx].endDate.isValid)
						{
							continue;
						}

						if(recs->recordings[recIdx].fps.whole != (int)it->fps )//(|| recs->recordings[recIdx].fps.frac != (it->fps - (int)it->fps))
						{
							cout <<" FPS Changed, current:"<< recs->recordings[recIdx].fps.whole<<"."<<recs->recordings[recIdx].fps.frac<<" to "<<it->fps<<endl;
							try
							{
								endRecordingCall(recs->recordings[recIdx].devId);
								found=false;
								try{
									cout<< " FPS Changed, iniciem dev:"<<it->devId<<endl;
									startRecordingCall(*it);
									found=true;
								}catch(Exception &e)
								{
									cout<<" -- FPS changed, startRecordingCall "<<it->devId<<" exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
									found=false;
								}
							}
							catch(Exception &e)
							{
								cout<<" -- FPS changed, endRecordingCall "<<it->devId<<" exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
								found=true;
							}
						}
						else
							found=true;
						cout<< "		trobat per dev:"<<it->devId<<endl;
						break;
					}
					if(!found) //no esta gravant
					{
						try{
							cout<< "		no trobat, iniciem dev:"<<it->devId<<endl;
							startRecording(*it);
						}catch(Exception &e){}
					}
					it->active=true;
				}
				else if(it->type == 2)
				{
					if(it->active==false)
					{
						it->active=true;
						cout<<"---------- checkRecordings : start Alarm Recording ---------- "<<it->devId<< endl;
						startAlarmRecording(*it);
					}		
				}
			}
			else
			{	//no hauria de gravar --> comprovém alarmes aquí 
				if(it->type == 2 && it->active)
				{
					cout<<"---------- checkRecordings : end Alarm Recording ---------- "<< it->devId<<endl;
					it->active=false;
					endAlarmRecording(*it);
				}
				else
					it->active=false;
			}
		}
		cout<<" -- comprovats programats -- (recs:"<<(void*)recs<<")"<<endl;
		cout<<"comprovem actius:"<<recs->nRecordings<<endl;
		//TODO: omprovar alarmes --> al Thread d'alarmes, cada X minuts d'alarma *continuada*
		//comprovém que el s schedules que estàn actius estàn realment programats
		for(int recIdx=0;recIdx<recs->nRecordings;recIdx++)
		{	//mirem si realment hauria d'estar gravant
//			cout<<" comprovant actius - rec "<<recIdx<<" / "<<recs->nRecordings<<"   recs:"<<(void*)recs->recordings<<endl;
			if(recs->recordings[recIdx].endDate.isValid)
			{
				cout<<" gravacio finalitzada:"<<recs->recordings[recIdx].devId<<" type:"<<recs->recordings[recIdx].recType<<endl;
				continue;
			}
			bool found=false;
			cout<<" gravacio activa:"<<recs->recordings[recIdx].devId<<" type:"<<recs->recordings[recIdx].recType<<endl;
			c.lock();
			for(list<Schedule>::iterator it=recordings.begin(); it!=recordings.end(); it++)
			{
				int tstart;
				tstart = it->start.waitTime(it->dayMask, t); 
				int tend;
				tend = it->end.waitTime(it->dayMask, t); 
				bool dia = (it->start.secs()==0 && it->end.secs() == 24*3600); // de 00:00:00 a 24:00:00
				int weekday=t.tm_wday;
			//	dia = dia && (tend <= 24*3600);	//avui
				dia = dia && ((((int)1)<<weekday) & it->dayMask);
				if((tend<tstart || dia) && it->devId==recs->recordings[recIdx].devId) // hauria de gravar
				{
					cout<< "   trobat entre schedules programats:"<<tstart<< "->"<<tend<<"  ("<<it->start.toString()<<" -> "<<it->end.toString()<<") type:"<<it->type<<endl;
					it->active=true;
					found=true;
					break;
				}
			}	
			c.unlock();
			if(!found) 
			{//no hi ha cap schedule que estaria actiu :D
				try
				{
					cout<<"    no schedulada, parem: "<<recs->recordings[recIdx].devId<<" recs:"<<recordings.size()<<endl;
					//rma->startSession();
					endRecording(recs->recordings[recIdx].devId);
				}catch(Exception &e){}
				//next->active=false;
			}
		}
		
		delete recs;
		cout<<"["<<StrUtils::getDateString()<<"] fi comprovacio"<<endl;
	}catch(Exception e)
	{
		recLock.unlock();
		//e.serialize()->materializeAndThrow(true);
		throw;
	}catch(std::exception stde)
	{
		recLock.unlock();
		throw;
	}
	recLock.unlock();
}


void RecordingModuleSchedulingThread::checkRecordingLocations()
{

	RMListRecordingsParams lrp;// = new RecordingModuleInterface::listRecordingsParams();
	struct timeval date;
	struct tm t;//(const time_t*)(&aux))

	gettimeofday(&date, NULL);
#ifdef WIN32
	unsigned long long secs=(date.tv_sec);
	struct tm *ptm;
	ptm= localtime((const time_t*)&secs);
	t= *ptm;
#else
	t= *localtime((const time_t*)&date.tv_sec);
#endif


	try
	{
		string s("SELECT * FROM clearBogusNearestSubsys()");
		//				cout<<"searching for best subSystem, call:"<<s<<endl;
		XML *res=dbGW->call(s, 0);
		if(res!=NULL)
			delete res;
	}
	catch (Exception &e)
	{
		cout <<"clearBogusNearestSubsys Exception: " << e.getClass() << ": " << e.getMsg() << endl;
	}

	lrp.absStartDate.secs=date.tv_sec-30;//les ultimes només 3 min, que les cams poden fer talls molt llargs
	lrp.absStartDate.millis=date.tv_usec/1000;
	lrp.absStartDate.isValid=true;
	lrp.absEndDate.isValid=false;

	lrp.nRecsFilter=1;
	lrp.nRecordings=0;
	lrp.recordings=new RMListRecordingsParams::recording[1];
	cout<<"["<<StrUtils::getDateString()<<"] RMSchedulingThread:: checkRecLocations 1"<<endl;

	lrp.recordings[0].id=0xffffffffffffffffll; //qualsevol
	lrp.recordings[0].devId=0xffffffff;
	lrp.recordings[0].recType=RecordingFileHeaderChunk::REC_SCHEDULED;
	lrp.recordings[0].endDate.isValid=false;
	lrp.recordings[0].startDate.isValid=false;
	cout<<"RMSchedulingThread:: lrp recs:"<<(void*) lrp.recordings<<endl;
	cout<<"RMSchedulingThread:: lrp addr:"<< lrp.recordings[0].rmAddress.toString()<<endl;


	RecordingModuleAccess rma(cn);//(rmi->getAddress(), rmi->getType(), rmi->getCanis());

	list<RecordingModuleAccess::subsystemRecordingList> *listRecs=rma.listRecordingsBySubsystem(lrp);
	lrp.recordings=NULL;

	
	for(list<RecordingModuleAccess::subsystemRecordingList>::iterator it = listRecs->begin(); it != listRecs->end(); it++)
	{
		RMListRecordingsParams *recs = it->listRecs;
		Address act = it->server; 
	//	cout<<" ********** "<<act.toString()<<" with:"<<endl;//it->listRecs->nRecordings<<" *it->listRecs:"<<(void*)it->listRecs<<endl;
		int remove=0;
		for(int recIdx=0; recIdx<it->listRecs->nRecordings; recIdx++)
		{
			int devId = it->listRecs->recordings[recIdx].devId;
			IP dev = cams[devId]->ip;

			//			RPCPacket *p = ServiceFinder::getNearestSubsys("RecordingModule", dev, cn->getInterfaceMask(), cn);

			string ipMask=dev.toString()+"/"+StrUtils::decToString(cn->getInterfaceMask());
			try
			{
				string s("SELECT * FROM getNearestSubsysWithFilter('"+StrUtils::decToString(ServiceFinder::resolveSubsystemTypeName("RecordingModule", cn))
						+ "', '"+ipMask+"','"+ipMask+"');");
//				cout<<"searching for best subSystem, call:"<<s<<endl;
				XML *serv=dbGW->call(s, 0);
//				cout<<"searching for best subSystem, res:"<<serv->toString()<<endl;

				xmlNode *ipNode=serv->getNode(string("/result/[0]/ip"));
				xmlNode *portNode=serv->getNode(string("/result/[0]/puerto"));
				xmlNode *idNode=serv->getNode(string("/result/[0]/id"));
				xmlNode *cached=serv->getNode(string("/result/[0]/iscached"));
				if(ipNode != NULL && portNode != NULL && idNode != NULL)
				{
					Address best(IP(ipNode->getCdata()), atoi(portNode->getCdata().c_str()));
					//cout<<"checkRecordingLocations device:"<<devId<<"	Recording at:"<<act.toString()<<", assigned at:"<<best.toString()<<" cached:"<<(cached==NULL?string("NULL"):cached->getCdata())<<endl;
					if(act.getIP().toDWord() != best.getIP().toDWord())
					{

						//							cout<<" ** ";
						cout<<"checkRecordingLocations device:"<<devId<<"	Recording at:"<<act.toString()<<", assigned at:"<<best.toString()<<" cached:"<<(cached==NULL?string("NULL"):cached->getCdata())<<endl;
						string ssId = idNode->getCdata();

						//posém el nou Rec. al setNearest i petém assigs, perqué pilli bé el DecMod/SIP
						setRecordingModuleForDevice(ipMask, ssId);

						//parém
						try
						{
							cout<<"RMA startSesParms"<<endl;
							RMStartSessionParams ssParams;
							ssParams.devId=devId;
							ssParams.date=new RecordingFileDateMetadataChunk();
							ssParams.date->setCurrentDate();
							ssParams.recType=RecordingFileHeaderChunk::REC_SCHEDULED;
							ssParams.fourcc=getCodecForDevice(devId).fourcc;
							ssParams.fpswhole=it->listRecs->recordings[recIdx].fps.whole;//(int)next.fps;
							ssParams.fpsfrac=it->listRecs->recordings[recIdx].fps.frac;//(int)((next.fps-(int)next.fps)*1000.0);//mili fps
							ssParams.isRecoded=false;
							ssParams.isCyclic=false;

							cout<<"RMA endSesParms"<<endl;
							RMEndSessionParams ep;
							ep.devId=devId;

							cout<<"RMA create:"<<best.toString()<<endl;
							RecordingModuleAccess rma2(best, cn);
							cout<<"assignat startSession ... "<<endl;
							rma2.startSession(ssParams);
							try
							{
								cout<<"startLocalRecording ... "<<endl;
								rma2.startLocalRecordingStream();
							}
							catch(MaxActiveRecordingsReachedException &e)
							{
								//no es pot reassignar -> torném a posar l'actual com a bo
								cout <<"MaxActiveRecordingsReachedException while startLocalRecStr dev:"<<devId<<":"<<e.getMsg()<<endl;
								setRecordingModuleForDevice(ipMask, act);
								//e.serialize()->materializeAndThrow(true);
								throw;
							}
							catch (Exception &e)
							{
								if(e.getClass()!= string("RecordingModuleException") || e.getMsg().find(string("Stream already being recorded")) == string::npos)//(e.getMsg() != string("Stream already being recorded"))
								{
									try
									{
										cout <<"checkRecordingLocations error while starting local Recording: "<< e.getClass() << ": " << e.getMsg() << endl;
										rma2.endSession(ep);
									}
									catch (Exception &e)
									{}
									
									setRecordingModuleForDevice(ipMask, act);
									
									//e.serialize()->materializeAndThrow(true);
									throw;
								}
								cout<<"Already being Recorded ... ";
							}
							cout<<"endSession"<<endl;
							rma2.endSession(ep);



							RecordingModuleAccess rma(act, cn);
							cout<<"actual startSession ... :"<<act.toString()<<endl;
							cout.flush();

							rma.startSession(ssParams);
							try
							{
								cout<<"stopRecording ... "<<endl;
								cout.flush();
								rma.stopRecordingStream();
							}
							catch (Exception &e)
							{
								cout <<"checkRecordingLocations error while moving recording: stopRecordingStream() "<< e.getClass() << ": " << e.getMsg() << endl;
								try
								{
									rma.endSession(ep);
								}
								catch (Exception &e)
								{}
								//e.serialize()->materializeAndThrow(true);
								throw;
							}
							cout<<"endSession "<<endl;
							rma.endSession(ep);
							//i engeguém
						}
						catch (Exception &e)
						{
							cout <<"checkRecordingLocations error while moving recording: "<< e.getClass() << ": " << e.getMsg() << endl;
						}

						remove++;
					}
					else if(cached!=NULL && cached->getCdata() == string("f"))
					{
						cout<<"checkRecordingLocations device:"<<devId<<"	already recording at:"<<act.toString()<<", but not cached"<<endl;
						string ssId = idNode->getCdata();
						setRecordingModuleForDevice(ipMask, ssId);
					}
					//		cout<<endl;
				}
				else
					cout <<"checkRecordingLocations error in getNearestSubsysWithFilter: "<<  serv->toString() << endl;
				delete serv;
			}
			catch (Exception &e)
			{
				cout <<"checkRecordingLocations Exception in getNearestSubsysWithFilter: " << e.getClass() << ": " << e.getMsg() << endl;
			}
		}
		cout<<"["<<StrUtils::getDateString()<<"] ********** "<<remove<<" moved from "<<act.toString()<<endl;
//		cout<<" ********** "<<act.toString()<<" with:"<<it->listRecs->nRecordings<<", "<<remove<<" to remove"<<endl;
		delete it->listRecs;
//		cout<<"**"<<endl;
	}
	
	
//	cout<<"RMSchedulingThread:: lrp addr:"<< lrp.recordings[0].rmAddress.toString()<<endl;
//	cout<<"checkRecordingLocations done deletes"<<endl;
//	delete [] lrp.recordings;
//	cout<<" del 2"<<endl;
	//delete listRecs;
//	cout<<"checkRecordingLocations done null"<<endl;
//	lrp.recordings= NULL;
//	cout<<"checkRecordingLocations done dele lrp"<<endl;
//	delete lrp;
	cout<<"checkRecordingLocations done"<<endl;
}


void RecordingModuleSchedulingThread::setRecordingModuleForDevice(string ipMask, Address ss)
{
	string q = ("select id from subsistema where host(ip) = '"+ss.getIP().toString()+"' AND puerto = "+StrUtils::decToString(ss.getPort())+";");
	try
	{
		XML *res = dbGW->query(q);
		xmlNode *n=res->getNode(string("/result/[0]/id"));
		if(n != NULL)
		{
			string ssId = n->getCdata();
			setRecordingModuleForDevice(ipMask, ssId);
		}
		delete res;
	}
	catch (Exception &e)
	{
		cout << "setRecordingModuleForDevice - setNearestSubsys exception while trying to get subsys id: " << e.getClass() << ": " <<
			e.getMsg() << ", query: " << q<<endl;
	}
}

void RecordingModuleSchedulingThread::setRecordingModuleForDevice(string ipMask, string ssId)
{

	string updateStates=string("SELECT * FROM setNearestSubsys('") + StrUtils::decToString(ServiceFinder::resolveSubsystemTypeName("RecordingModule", cn))
		+ string("', '") + ipMask + string("', '") + ssId + string("');");
	try
	{
		XML *res = dbGW->query(updateStates);
		delete res;
	}
	catch (Exception &e)
	{
		cout << "setRecordingModuleForDevice - setNearestSubsys exception: " << e.getClass() << ": " <<
			e.getMsg() << ", query: " << updateStates<<endl;
	}

	updateStates=string("DELETE FROM nearestsubsystembyip where ip = '") + ipMask + string("' AND tiposubsistema != '") + StrUtils::decToString(ServiceFinder::resolveSubsystemTypeName("RecordingModule", cn)) + string("';");
	try
	{
		cout<<"checkRecordingLocations device:"<<ipMask<<" deleting nearestByIP cache query:"<<updateStates<<endl;
		XML *res = dbGW->update(updateStates);
		cout<<"checkRecordingLocations device:"<<ipMask<<" deleted nearestByIP cache:"<<res->toString()<<endl;
		delete res;
	}
	catch (Exception &e)
	{
		cout << "delete from nearestsubsystembyip exception: " << e.getClass() << ": " <<
			e.getMsg() << ", query: " << updateStates<<endl;
	}
}


void RecordingModuleSchedulingThread::recordThread()
{
	cout<<"--- RecordingModuleSchedulingThread::recordThread() started ---"<<endl;
	while(schedulingThreadActive)
	{
		try
		{
			Schedule *next;
			bool startNext=false;

			struct timeval date;
			gettimeofday(&date, NULL);

			struct tm t;//(const time_t*)(&aux))
			unsigned long long l=date.tv_sec;
			struct tm *tmp;
			tmp=localtime((const time_t*)&l);
			t=*tmp;
			cout<<endl<<endl<<" gettimeofday/localtime::"<<date.tv_sec<<" tm:"<<t.tm_sec<<" "<<t.tm_wday<<" "<<t.tm_mday<<endl;

			int mins = 7*24*3600+1;
			startNext=false;
			
			
			next=NULL;
			cout<<"RMSchedulingThread:: find next recording total:"<<recordings.size()<<endl;
			recLock.rlock();
			try
			{
				for(list<Schedule>::iterator it=recordings.begin(); it!=recordings.end(); it++)
				{
					int tstart;
					tstart = it->start.waitTime(it->dayMask, t); 
				//	cout<<"tstart:"<<tstart<<" dayMask:"<<it->dayMask<<endl;
					int tend;
					tend = it->end.waitTime(it->dayMask, t); 
				//	cout<<"tend:"<<tend<<" dayMask:"<<it->dayMask<<endl;
					bool dia = (it->start.secs()==0 && it->end.secs() == 24*3600); // de 00:00:00 a 24:00:00
					int weekday=t.tm_wday;
					dia = dia && ((((int)1)<<weekday) & it->dayMask);
					cout<<" "<<it->devId<<" dia "<<dia<<" start:"<<it->start.secs()<<" end:"<<it->end.secs()<<" wDay:"<<weekday<<" msk:"<<hex<< (((int)1)<<weekday)<<" mask:"<<it->dayMask<<dec<<endl;
					if (!dia)// les de tot el dia ja ho farà el checkRecordings( || dia) //hauria d'estar gravant
					{
						if(tend<tstart)// les de tot el dia ja ho farà el checkRecordings( || dia) //hauria d'estar gravant
						{
							if(tend < mins)
							{
								mins=tend;	//esperem per parar
								next=&(*it);
								startNext=false;
							}
						}
						else
						{
							if(tstart < mins)
							{
								mins=tstart;
								next=&(*it);
								startNext=true;
							}
//							cout<<"   sched -  Not Active::"<<tstart<< " end:"<<tend<<"  sched:"<<it->start.toString()<<" -> "<<it->end.toString()<<" type:"<<it->type<<" mins:"<<mins<<endl;
						}
					}
/*					if(tend<tstart && !dia)// les de tot el dia ja ho farà el checkRecordings( || dia) //hauria d'estar gravant
					{
						if(!it->active)
						{
							mins=0;
							next=&(*it);
							startNext=true;
//							cout<<"      sched - err -  Ja hauria d'estar gravant::"<<tstart<< " end:"<<tend<<endl;
							//pels checkRecordings, no hauria de fer falta, peró ja que hi som...
							break; // s'ha d'activar de seguida
						}
						else if(tend < tstart)//per no tallar a les 12
						{
							mins=tend;	//esperem per parar
							next=&(*it);
							startNext=false;
//							cout<<"   sched -      Active::"<<tstart<<" end:"<<tend<<"  sched:"<<it->start.toString()<<" -> "<<it->end.toString()<<" type:"<<it->type<<" mins:"<<mins<<endl;
						}
					}
					else if(tend>tstart) //parém el seguent
					{
						if(!it->active)
						{
							mins=tstart;
							next=&(*it);
							startNext=true;
//							cout<<"   sched -  Not Active::"<<tstart<< " end:"<<tend<<"  sched:"<<it->start.toString()<<" -> "<<it->end.toString()<<" type:"<<it->type<<" mins:"<<mins<<endl;
						}
						else
						{
							mins=0;
							next=&(*it);
							startNext=false;
							cout<<"      sched - err - encara esta gravant::"<<tstart<< " end:"<<tend<<endl;
							//pels checkRecordings, no hauria de fer falta, peró ja que hi som...
							//break; // s'ha d'activar de seguida
						}
					}
					//else tstart==tend, serà una programada que passa per les 12, no ens interessa :P
				*/
				}
			}catch(Exception e)
			{
				recLock.unlock();
				//e.serialize()->materializeAndThrow(true);
				throw;
			}
			recLock.unlock();

			gettimeofday(&date, NULL);
			timespec wait;
			wait.tv_sec=date.tv_sec+mins;
			wait.tv_nsec=0;
			struct tm w=*localtime((const time_t*)&(wait.tv_sec));
			if(next!=NULL)
				cout<<"RMSchedulingThread:: found next recording: "<<next->devId <<" "<<next->start.toString()<<" end:"<<next->end.toString()<<" sleep:"<<mins<<"   -  start:"<<startNext <<" wait::"<<wait.tv_sec<<" == "<<w.tm_mday<<"/"<<w.tm_mon<<"/"<<w.tm_year<<" "<<w.tm_hour<<":"<<w.tm_min<<endl;
			else
				cout<<" RMSchedulingThread:: no Schedules present"<<endl;
			c.lock();
			if(!schedulingThreadActive)
			{
				c.unlock();
				break;
			}
			if(mins>=0 && mins<=60) 
			{
				cout<<"  schedule ----- Waiting "<<mins<<endl;
				if(c.wait(wait))
				{
					cout<<"  schedule ----- wait timeout "<<mins<<endl;
					c.unlock();
					if(!schedulingThreadActive)
						break;
					if(next!=NULL)
					{
						if(startNext)
						{
							if(next->active==false)
							{
								next->active=true;
								if(next->type==2)
								{
									cout<<" --- ------ ---------- start Alarm Recording ---------- ------ --- "<<next->devId<< endl;
									startAlarmRecording(*next);
								}
								else
								{
									cout<<" --- ------ ------------- start Recording ------------- ------ --- "<<endl;
									startRecording(*next);
								}
							}
						}
						else
						{
							next->active=false;
							if(next->type==2)
							{
								cout<<" --- ------ ---------- end Alarm Recording ---------- ------ --- "<<endl;
								endAlarmRecording(*next);
							}
							else
							{
								cout<<" --- ------ ------------- end Recording ------------- ------ --- "<<endl;
								endRecording(*next);
							}
						}
					}
				}
				else
				{
					cout<<"  schedule ----- wait signal "<<mins<<endl;
					c.unlock();
				}
			}
			else
			{
				cout<<"  schedule ----- Waiting 60s "<<endl;
				timespec wait;
				wait.tv_sec=date.tv_sec+60;//+600; //TODO
				wait.tv_nsec=0;
				struct tm w=*localtime((const time_t*)&(wait.tv_sec));
				if(!c.wait(wait))
				{	//ens han fet un signal -> nou schedule afegit
					cout<<"RMSchedulingThread:: reschedule"<<endl;
					c.unlock();
				}
				else
				{	//timeout -> comprovem que les coses encara estiguin gravant :P
					cout<<"RMSchedulingThread:: re-check schedules"<<endl;
					c.unlock();
					if(!schedulingThreadActive)
						break;
				}
				cout<<"  schedule ----- Waiting 60s checked "<<endl;
			}
		}
		catch (Exception &e)
		{
			cout <<"Caught exception while starting/stopping recording:"<< e.getClass() << ": " << e.getMsg() << endl;
		}
		catch (...)
		{
			cout <<"Caught unknown exception while starting/stopping recording" << endl;
		}
	}
	cout<<"--- RecordingModuleSchedulingThread::recordThread() stopping ---"<<endl;
}

void RecordingModuleSchedulingThread::recordCheckingThread()
{
	cout<<"--- RecordingModuleSchedulingThread::recordCheckingThread() started ---"<<endl;
	if(!this->waitActive())
	{
		try
		{
			checkRecordings(); //es mes eficient que el bucle del principi del while :P
		}catch(Exception &e)
		{
			cout<<"RecordingModuleSchedulingThread::recordCheckingThread() starting checkRecordings() exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		}
		catch(std::exception &stde)
		{
			cout <<"RecordingModuleSchedulingThread::recordCheckingThread() starting checkRecordings() Caught std::exception:"<< stde.what() << endl;
		}
		catch (...)
		{
			cout <<"RecordingModuleSchedulingThread::recordCheckingThread() starting checkRecordings()  Caught unknown exception:" << endl;
		}
	}
	Timer t;
	while(checkingThreadActive)
	{
		t.start();
		try
		{
			
			//Reassignació gravacions
		
 			struct timeval date;
			struct tm tm;
			gettimeofday(&date, NULL);

			int secs=date.tv_sec;
			localtime_r((const time_t*)&secs, &tm);
	//		if(checkLocationsPending)//(|| tm.tm_hour % 6 == 0 && (lastRecCheck.tm_mday != tm.tm_mday || lastRecCheck.tm_hour != tm.tm_hour))
			if(checkLocationsPending || lastRecCheck.tm_hour != tm.tm_hour)
			{
				cout<<"RecordingCheckThr: Checking recording locations :"<<tm.tm_mday<<" . "<<tm.tm_hour<<":"<<tm.tm_min<<":"<<tm.tm_sec<<" , pending:"<<checkLocationsPending<<endl;
				try
				{
					checkRecordingLocations();
//					cout<<"lastCheck"<<endl;
					lastRecCheck = tm;
//					cout<<"/lastCheck"<<endl;
					checkLocationsPending=false;
				}catch(Exception &e)
				{
					cout<<" checkRecordingLocations exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
				}
			}

			if(this->waitActive())
				continue;

			cout<<"RecordingCheckThr: loadAllSched"<<endl;
			loadAllSchedules();
	
			if(this->waitActive()) //hem parat -> recarreguém Scheds
				continue;

			if(!this->schedulingThreadActive) //ens assegurém que estàn carregats els Sched Actuals
			{
				cout<<"RecordingCheckThr: start Scheduling Thread"<<endl;
				RecordingModuleSchedulingThread::SchedulerThreads::threadCall *c2=new RecordingModuleSchedulingThread::SchedulerThreads::threadCall();
				c2->id=0;
				c2->data=NULL;
				this->st->start(c2, Thread::Thread::OnExceptionRestart);
			}
		
			cout<<"RecordingCheckThr: checkRec"<<endl;
			checkRecordings();

			if(this->waitActive())
				continue;

			recStarted=false;

			TimerInstant ti=t.time();
			int sleepTime=20-ti.seconds(); //per si les comprovacions triguen poc (instal·lacions "petites")
			cout<<"RecordingCheckThr: elapsed:"<<ti.seconds()<<endl;
			while(sleepTime>0 && !recStarted)
			{
				/*
				cout<<"RecordingCheckThr: sleep:"<<sleepTime<<endl;
				sleep(min(sleepTime,10));
				sleepTime -= 10;
				*/
				if(sleepTime%5==0)
					cout<<"RecordingCheckThr: sleep:"<<sleepTime<<endl;
				sleep(1);
				sleepTime --;

				if(this->waitActive())
					break;
			}
			if(recStarted && sleepTime > 0)
				cout << "["<<StrUtils::getDateString()<<"] Recording Module [re]started, re-checking recordings"<<endl;


		}
		catch (Exception &e)
		{
			cout <<"Caught exception in recordCheckingThread:"<< e.getClass() << ": " << e.getMsg() << endl;
		}
		catch(std::exception &stde)
		{
			cout <<"Caught std::exception in recordCheckingThread:"<< stde.what() << endl;
		}
		catch (...)
		{
			cout <<"Caught unknown exception in recordCheckingThread" << endl;
		}
	}
}

void RecordingModuleSchedulingThread::addSchedule(Schedule s)
{
	cout<<"RMSchedulingThread::AddSchedule: start:"<<s.start.toString()<<" end:"<<s.end.toString()<<" fps:"<<s.fps<<" type:"<<s.type<<" dist:"<<s.dist<<endl;
	if(s.type==2)
		cout<<" --------- RMSchedulingThread::Alarm Schedule ------- "<<endl;
	bool found = false;
	list<Schedule>::iterator insert = recordings.end();
	for(list<Schedule>::iterator it=recordings.begin();it!=recordings.end();it++)
	{
//		cout<<"                    AddSchedule: present :"<<it->start.toString()<<" end:"<<it->end.toString()<<" fps:"<<it->fps<<" type:"<<it->type<<endl;
		if(s == *it)
		{
//			cout<<"                       AddSchedule equal, not adding"<<endl;
			found=true;
			it->found=true;
			if(s.fps != it->fps)
				it->fps = s.fps;
			break;
		}
		else if(it->dist <= s.dist)
		{
			insert = it;
		}
	}

	c.lock();
	if(!found)
	{
		s.found=true;
//		cout<<"                       AddSchedule not found, adding :"<<recordings.size()<<endl;
		if(insert == recordings.end())
			recordings.push_front(s);//back(s);
		else
		{
			insert++;
			recordings.insert(insert, s);
		}
//		cout<<"                       AddSchedule not found, added :"<<recordings.size()<<endl;
//		c.signal();
		c.unlock();
	}
	else
		c.unlock();
}

void RecordingModuleSchedulingThread::removeSchedules(int id)
{
	cout<<"RMSchedulingThread:: Remove all Schedules"<<endl;
	c.lock();
	//while(!recordings.empty())
	for(list<Schedule>::iterator it=recordings.begin();it!=recordings.end();it++)
	{
		try
		{
			Schedule s=*it;
			if(s.devId==id)
			{
				if(s.active)
				{
					if(s.type==2)
					{
						endAlarmRecording(s);
					}
					else
					{
						endRecording(s);
					}
				}
				//	if(s.alarm!=NULL)
				// TODO		delete s.alarm;
				it=recordings.erase(it);
			}
		}catch(Exception e)
		{
			cout<<" RecordingModuleSchedulingThread::removeSchedules exc:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
		}
	}

	c.signal();
	c.unlock();
}

bool RecordingModuleSchedulingThread::recording(int id)
{
//	cout<<"RMSchedulingThread:: recording(id)"<<endl;
	c.lock();
	//while(!recordings.empty())
	for(list<Schedule>::iterator it=recordings.begin();it!=recordings.end();it++)
	{
		Schedule s=*it;
		if(s.devId==id && s.active)
		{	
			c.unlock();
			return true;
		}
	}
	c.unlock();
	return false;
}

void RecordingModuleSchedulingThread::stop()
{
	cout<<" -------------- stoping scheduling thread -----------"<<endl;
	c.lock();
	schedulingThreadActive=false;
	checkingThreadActive=false;
	c.broadcast();
	c.unlock();
}

