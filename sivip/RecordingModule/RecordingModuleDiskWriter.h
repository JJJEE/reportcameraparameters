#ifndef __SIRIUS__RECORDINGMODULE__RECORDINGMODULEDISKWRITER_H
#define __SIRIUS__RECORDINGMODULE__RECORDINGMODULEDISKWRITER_H

#include <Threads/Thread.h>
#include <Threads/RWlock.h>
#include <Utils/Watchdog.h>
#include <RecordingModule/RecordingModuleDiskWriterEntryGrouper.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <map>
#include <list>

using namespace std;

class RecordingModuleDiskWriter : public Thread
{
public:
	// BUGFIX 20100706: Afegim aquesta constant per evitar que les esperes
	// per tancar el fitxer siguin mes llargues del compte (fins ara,
	// maxEmptyCount = 901 * 10 que tenia sentit quan feiem 10 escriptures
	// per segon, pero ara que se'n fa 1, es quedaven oberts els fitxers
	// 10 cops mes temps del desitjat.
	static const int writesPerSecond=1;
	
	// 20090819
	// Workaround de tancat de fitxers que fan que peti el modul (aparentment
	// quan es peten els indexos).
	// No tanquem el fitxer fins que no porta > 15 minuts sense incorporar-hi
	// informacio :P
	static const int maxEmptyCount=960 * RecordingModuleDiskWriter::writesPerSecond;

protected:
	map<string, RecordingModuleDiskWriterEntryGrouper*> entryLists;
	map<string, RecordingFile*> recordingFiles;
	map<string, RecordingFileHeaderChunk*> headers;
	// Comptador per veure quants cops es troba buida una cua. Si passa de
	// maxEmptyCount, es tanca el fitxer, etc.
	map<string, int> entryListEmptyCounters;
	Watchdog *notify;
	
	RWLock *entryListsRW;
	
	bool running, flush;
	int runningId;
	
public:
	RecordingModuleDiskWriter(Watchdog *notify=NULL);
	virtual ~RecordingModuleDiskWriter();
	
	RecordingFileHeaderChunk *getHeaderForFile(string file);
	RecordingFileHeaderChunk *createHeaderForFile(string file, fileOffset off,
		dword reservedSize, dword fourcc, 
		RecordingFileHeaderChunk::recType recType, word fpswhole, word fpsfrac);
	
	RecordingFile *getRecordingFile(string file, bool readOnly=false);
	// Obte una llista dels fitxers que tenen dades pendents d'escriure
	list<string> getAvailableFiles();
	// Obte una llista dels fitxers oberts
	list<string> getOpenFiles(list<string> exclude);
	RecordingModuleDiskWriterEntryGrouper* getEntriesForFile(string file);
	float addEntryToFile(string file, RecordingModuleDiskWriterEntry *entry);
	void wlock();
	void rlock();
	void unlock();
	
	virtual void startRunning();
	virtual void stopRunningAndFlush();
	virtual void *execute(int id, void *args);
	
	virtual int countEmpty(string file);
	virtual void setCountEmpty(string file, int count);
	virtual void resetCountEmpty(string file);
	virtual void cleanupFile(string file);
	virtual void writeGroups(string file, 
		list<RecordingModuleDiskWriterEntryGroup*> &groups);
	virtual void flushFile(string file);
	virtual void flushAllGroups();

	float calcFullFactorForFile(string file);
	
	// Funcions unsafe
	virtual int unsafeGetCountEmpty(string file);	

};

#endif
