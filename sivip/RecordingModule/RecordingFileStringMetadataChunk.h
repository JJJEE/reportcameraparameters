/*
 *  RecordingFileStringMetadataChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */


#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <map>
#include <string>

using namespace std;

class RecordingFileStringMetadataChunk : public RecordingFileMetadataChunk
{
	string name, value;
	
public:
	RecordingFileStringMetadataChunk();
	virtual ~RecordingFileStringMetadataChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	virtual void setMetadata(string name, string value);
	// retorna map["name"]=name i map["value"]=value
	virtual map<string,string> getMetadata();
};

