#include <version.h>
#include <DecodeModule/DecodeModule.h>
#include <Utils/RPC.h>
//#include <CentralDirectory/CentralDirectoryService.h>
#include <ServiceFinder/ServiceFinder.h>
#include <RecordingModule/RecordingModuleInterface.h>
#include <Utils/ServiceException.h>
#include <Plugins/GestorImageTypes.h>
#include <Utils/Timer.h>
#include <Utils/Types.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <RecordingModule/RecordingFileFrameChunk.h>
#include <RecordingModule/RecordingFileStringMetadataChunk.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>

#include <signal.h>

using namespace std;

struct timeval tini, tend;
int currFrame=0;

#define DEFAULT_ID 10
#define N_CALLS 1
#define N_FRAMES 25

int main(int argc, char *argv[])
{
	int devId=DEFAULT_ID;
	int nCalls;
	int nFrames;

	if (argc>1)
		devId=atoi(argv[1]);

	if (argc>2)
		nCalls=atoi(argv[2]);
	else
		nCalls=N_CALLS;

	if (argc>3)
		nFrames=atoi(argv[3]);
	else
		nFrames=N_FRAMES;

	Canis cn(Address(IP("0.0.0.0"), 0), 0);
	RPCPacket *centralDir=ServiceFinder::getBestCentralDirectory(&cn, Address(IP("0.0.0.0"), 0));
	RPCPacket *decodeModule=ServiceFinder::getBestSubsys(string("DecodeModule"), &cn);
	RPCPacket *recordingModule=ServiceFinder::getBestSubsys(string("RecordingModule"), &cn);
	
	if (decodeModule==NULL || recordingModule==NULL)
		return -1;
		
	cout << "Best subsystem found for type DecodeModule: " << decodeModule->a->toString() << endl;
	cout << "Best subsystem found for type RecordingModule: " << recordingModule->a->toString() << endl;

	RPC rpcDM(*decodeModule->a);
	RPC rpcRM(*recordingModule->a);
	
	// Prova getDeviceInfo
	cout << "getting device information..." << endl;
	
	IPDeviceInfo device;
	device.id=devId;
	
	char *deviceBuf=device.toNetwork();
	
	RPCPacket *pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::getDeviceInformationServiceId, (byte*)deviceBuf,
				device.size(), 0, decodeModule->origen);

	RPCPacket *devInfo=NULL;
	try
	{
		devInfo=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.getDeviceInformation)" << endl;
		return -1;
	}
	
	if (devInfo==NULL)
		return -1;
	
	delete [] deviceBuf;	

	device.toLocal((char*)devInfo->getData());
	
	cout << "Info for device " << device.id << ": " << endl
		<< "Make: " << device.make << endl 
		<< "Model: " << device.model << endl;

	delete pk;

	// Prova startSession
	cout << "starting session..." << endl;
	
	IPDeviceID deviceId;
	deviceId.id=devId;
	
	deviceId.toNetworkEndian();
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::startSessionServiceId, (byte*)&deviceId,
				sizeof(deviceId), 0, decodeModule->origen);

	RPCPacket *startSession=NULL;
	try
	{
		startSession=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.startSession)" << endl;
		return -1;
	}
	
	if (startSession==NULL)
		return -1;
	
	cout << "session started" << endl;

	// Prova setFrameBufferSize
	cout << "setting frame buffer size..." << endl;
	IPFrameBufferSize fbSize;
	fbSize.nFrames=24;
	fbSize.toNetworkEndian();
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::setFrameBufferSizeServiceId, (byte*)&fbSize,
				sizeof(fbSize), 0, decodeModule->origen);

	RPCPacket *setFBSize=NULL;
	try
	{
		setFBSize=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.setFrameBufferSize)" << endl;
		return -1;
	}

	if (setFBSize==NULL)
		return -1;
		
//	cout << "setFBSize response id: " << setFBSize->id << endl;
	
//	cout << "frame buffer size set to 24 frames" << endl;

	// Prova getFrameBufferSize
//	cout << "start: " << gettimeofday(&tini, NULL) << endl;
//	cout << "start: " << tini.tv_sec << "." << tini.tv_usec << endl;

	Timer t;
	
	t.start();

	for (int i=0; i<nCalls; i++)
	{
		cout << "getting frame buffer size..." << endl;
		IPFrameBufferSize fbSize;
		
		pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::getFrameBufferSizeServiceId, NULL,
					0, 0, decodeModule->origen);

		RPCPacket *getFBSize=NULL;
		try
		{
			getFBSize=rpcDM.call(*pk);
		}
		catch (Exception &e)
		{
			cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.getFrameBufferSize)" << endl;
			return -1;
		}
		
		if (getFBSize==NULL)
			return -1;
		
		memmove(&fbSize, getFBSize->getData(), getFBSize->getSize());
		
		fbSize.toLocalEndian();
		
		cout << "Frame Buffer size: " << fbSize.nFrames << " frames" << endl;
	}

//	cout << "end: " << gettimeofday(&tend, NULL) << endl;
//	cout << "end: " << tend.tv_sec << "." << tend.tv_usec << endl;
	
//	double ini=((double)tini.tv_sec*1000000.0+(double)tini.tv_usec);
//	double fi=((double)tend.tv_sec*1000000.0+(double)tend.tv_usec);

//	cout << "temps: " << fi << " - " << ini << endl;
	
//	double temps=(fi-ini)/1000000.0;

	TimerInstant ti=t.time();
	
	cout << "getFrameBufferSize called " << nCalls << " times" << endl 
		<< "Total time: " << ti.seconds() << "s" << endl
		<< "Avg. time/call: " << ti.seconds()/(double)nCalls << "s" << endl
		<< "Speed: " << (double)nCalls/ti.seconds() << " calls/s" << endl
		<< "Number of frames @ "<< ti.FPS() << "fps: " << ti.frames() << " frames" << endl;
	
	// Prova getCodecInUse
	cout << "geting codec in use..." << endl;
	IPCodecInfo codecInfo;
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::getCodecInUseServiceId, NULL,
				0, 0, decodeModule->origen);

	RPCPacket *getCodecInUse=NULL;
	try
	{
		getCodecInUse=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.setFrameBufferSize)" << endl;
		return -1;
	}

	if (getCodecInUse==NULL)
		return -1;
		
	memmove(&codecInfo, getCodecInUse->getData(), getCodecInUse->getSize());
	
	// El fourcc en network order es com esta be per fer una string
	string fourccStr((char*)&codecInfo.fourcc, sizeof(codecInfo.fourcc));
	
	codecInfo.toLocalEndian();
				
	cout << "Codec In Use: " << fourccStr
		<< ", quality: " << codecInfo.quality << ", bitrate: " << codecInfo.bitrate << endl;
	
/*	// Prova getCompressedNextFrame
	cout << "getting frame in NOSTREAM mode..." << endl;
	IPFrame frame;
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::getCompressedNextFrameServiceId, NULL,
				0, 0, decodeModule->origen);

	RPCPacket *getCompressedNextFrame=NULL;
	try
	{
		getCompressedNextFrame=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.getCompressedNextFrame (NOSTREAM))" << endl;
		return -1;
	}

	if (getCompressedNextFrame==NULL)
		return -1;
		
	frame.toLocal((char*)getCompressedNextFrame->getData());
				
	cout << "Frame received. Length " << frame.frameLength << "bytes, "
		<< frame.frameInfo.x << "x" << frame.frameInfo.y << ", " << frame.frameInfo.bpp << "bpp." << endl;
		
	char aux[16];
	sprintf(aux, "%.03d", currFrame);
	string fname=string("testFrame_")+string(aux)+string(".jpg");
	cout << "Saving frame to " << fname << "... "; cout.flush();
	FILE *f=fopen(fname.c_str(), "wb");
	fwrite(frame.frame, frame.frameLength, 1, f);
	fclose(f);
	cout << "saved." << endl;
	currFrame++;
*/

	// Prova setStreamingMode
	cout << "Setting streaming mode to STREAM..." << endl;
	IPStreamingMode mode=STREAM;
	
	Endian::to(Endian::xarxa, &mode, sizeof(mode));
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::setStreamingModeServiceId, (byte*)&mode,
				sizeof(mode), 0, decodeModule->origen);

	RPCPacket *setStreamingMode=NULL;
	try
	{
		setStreamingMode=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.setStreamingMode)" << endl;
		return -1;
	}

	if (setStreamingMode==NULL)
		return -1;
		
	// Prova RecordingModule::startSession
	cout << "Starting session with RecordingModule..." << endl;
	RecordingModuleInterface::startSessionParams ssParams;
	
	ssParams.devId=devId;
	ssParams.date=new RecordingFileDateMetadataChunk();
	ssParams.date->setCurrentDate();
	ssParams.recType=RecordingFileHeaderChunk::REC_MANUAL;
	ssParams.fourcc=codecInfo.fourcc;
	ssParams.fpswhole=25;
	ssParams.fpsfrac=0;
	ssParams.isRecoded=false;
	ssParams.isCyclic=false;
	
	dword ssParamsLen;
	void *ssParamsData=ssParams.toNetwork(&ssParamsLen);
	
//	cout << "Sending: " << StrUtils::hexDump(string((char*)ssParamsData, ssParamsLen)) << endl;

	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::startSessionServiceId, (byte*)ssParamsData,
				ssParamsLen, 0, recordingModule->origen, true);

	delete [] (byte*)ssParamsData;

	RPCPacket *RMstartSession=NULL;
	try
	{
		RMstartSession=rpcRM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.setStreamingMode)" << endl;
		return -1;
	}

	if (RMstartSession==NULL)
		return -1;
	
	delete pk;
	
	cout << "Started Recording Module session" << endl;	

	// mes proves getCompressedNextFrame
	cout << "getting frames in STREAM mode..." << endl;
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::getCompressedNextFrameServiceId, NULL,
				0, 0, decodeModule->origen);

	// Crea el fitxer on gravarem, borrant-lo si existeix
/*	RecordingFile *rf=new RecordingFile(string("DecodeModuleFrames.srf"), false, true);
	RecordingFileHeaderChunk *rfhc=new RecordingFileHeaderChunk();
	rfhc->setFourCC(CODEC_FOURCC('j','p','e','g'));
	rfhc->setVersion(0,100);
	rfhc->setRecType(RecordingFileHeaderChunk::REC_MANUAL);
	rfhc->setFPS(25);

	void *rfhcdata=rfhc->getData();
	rf->writeBlock(rfhcdata, rfhc->size());
	delete [] (byte*)rfhcdata;
*/
	for (int i=0; i<nFrames; i++)
	{
		cout << "Frame " << currFrame << " of " << nFrames << ": "; cout.flush();
		t.start();
		RPCPacket *getCompressedNextFrame=NULL;
		try
		{
			getCompressedNextFrame=rpcDM.call(*pk);
		}
		catch (Exception &e)
		{
			cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.getCompressedNextFrame)" << endl;
			return -1;
		}

		if (getCompressedNextFrame==NULL)
			return -1;
		
		IPFrame frame;
		
//		cout << "frame RPC Packet: " << getCompressedNextFrame->a->toString() << ", "
//									<< getCompressedNextFrame->origen << ", "
//									<< getCompressedNextFrame->id << ", "
//									<< getCompressedNextFrame->desti << ", "
//									<< getCompressedNextFrame->size << ", "
//									<< (void*)getCompressedNextFrame->data << endl;
				
		frame.toLocal((char*)getCompressedNextFrame->getData());

		ti=t.time();
					
		cout << ti.seconds() << "s, (" << ti.frames() << "frames), " << frame.frameLength << "bytes, "
			<< frame.frameInfo.x << "x" << frame.frameInfo.y << ", " << frame.frameInfo.bpp << "bpp." << endl;
			
		if (ti.useconds()<40000)
			usleep(40000-ti.useconds());
			
		
		/*char aux[16];
		sprintf(aux, "%.03d", currFrame);
		string fname=string("testFrame_")+string(aux, strlen(aux))+string(".jpg");
		cout << "Saving frame to " << fname << "... "; cout.flush();
		FILE *f=fopen(fname.c_str(), "wb");
		fwrite(frame.frame, frame.frameLength, 1, f);
		fclose(f);
		cout << "saved." << endl;
		*/

		RecordingFileFrameChunk *frameChunk=new RecordingFileFrameChunk();

/*		cout << "Saving frame to recording file..." << endl;
		frameChunk->setFrame(frame.frame, frame.frameLength);
		void *framedata=frameChunk->getData();
		rf->writeBlock(framedata, frameChunk->size());
		delete [] (byte*)framedata;
		delete frameChunk;
*/

		cout << "Sending frame to Recording Module... "; cout.flush();
		frameChunk->setFrame(frame.frame, frame.frameLength);

		cout << "Preparing... "; cout.flush();

		RecordingModuleInterface::saveFrameParams fp;
		RecordingFileStringMetadataChunk *isKey=new RecordingFileStringMetadataChunk();
		isKey->setMetadata(string("isKeyframe"), (frame.isKey?string("true"):string("false")));
		frameChunk->addSubChunk(isKey);
		fp.chunk=frameChunk;

		cout << "Getting... "; cout.flush();

		dword fParamsLen;
		void *fParamsData=fp.toNetwork(&fParamsLen);
		
//		cout << fParamsLen << " bytes... " <<
//			StrUtils::hexDump(string((char*)fParamsData, fParamsLen)); cout.flush();
		
		RPCPacket *pk2=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::saveFrameServiceId, (byte*)fParamsData,
					fParamsLen, 0, recordingModule->origen, true);

		delete [] (byte*)fParamsData;

		cout << "Sending... "; cout.flush();
		
		RPCPacket *RMsaveFrame=NULL;
		try
		{
			RMsaveFrame=rpcRM.call(*pk2);
		}
		catch (Exception &e)
		{
			cout << e.getClass() << ": " << e.getMsg() << " (recordingModule.saveFrame)" << endl;
			return -1;
		}

		if (RMsaveFrame==NULL)
			return -1;
		
		cout << "ok" << endl; cout.flush();

		// No cal, ja es fa el delete quan es peta frameParams
//		delete frameChunk;

		currFrame++;
	}
	
//	delete rf;

	// Per la prova de lectura de frames
	RecordingFileDateMetadataChunk currSecsDate;
	double currSecs=currSecsDate.microtime();


	// Prova endSession
	cout << "ending Decode Module session..." << endl;
	deviceId.id=1;
	
	deviceId.toNetworkEndian();
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::endSessionServiceId, (byte*)&deviceId,
				sizeof(deviceId), 0, decodeModule->origen);

	RPCPacket *endSession=NULL;
	try
	{
		endSession=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.endSession)" << endl;
		return -1;
	}
	
	if (endSession==NULL)
		return -1;
	
	cout << "session ended" << endl;

	delete pk;

	// Prova d'obtencio d'alguns frames :)
	cout << "trying to get some frames from recording module..." << endl;
	
	// La meitat dels q hem gravat ja fara el fet
	for (dword recModFrame=0; recModFrame<nFrames/2; recModFrame++)
	{
		RecordingModuleInterface::readFrameParams rfp;
		rfp.chunk=new RecordingFileFrameChunk();
	
		RecordingFileDateMetadataChunk *frameDate=(RecordingFileDateMetadataChunk*)rfp.chunk->getSubChunk(0);
		double secs=currSecs-((double)nFrames)/25.0;
		secs+=((double)recModFrame)/25.0;
		frameDate->setDate((qword)floor(secs), (word)((secs-floor(secs))*1000.0));

		cout.fill('0');
		
		cout << "set seconds to " << frameDate->getSecs() << ".";
		cout.width(3);
		cout << frameDate->getMillis() << endl;
		cout.width(0);

		dword rfParamsLen;
		void *rfParamsData=rfp.toNetwork(&rfParamsLen);
		
		pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::readFrameServiceId,
		(byte*)rfParamsData, rfParamsLen, 0, recordingModule->origen, true);

		RPCPacket *RMreadFrame=NULL;
		try
		{
			RMreadFrame=rpcRM.call(*pk);
		}
		catch (Exception &e)
		{
			cout << e.getClass() << ": " << e.getMsg() << " (recordingModule.readFrame)" << endl;
			return -1;
		}

		if (RMreadFrame==NULL)
			return -1;

		delete pk;
		
		cout << "got frame " << recModFrame << ", saving... Chunks:"<<endl;


		byte *data=RMreadFrame->getData();
		dword size=*(dword*)data;
		Endian::from(Endian::xarxa, &size, sizeof(dword));
		RecordingFileFrameChunk *recvFrame=(RecordingFileFrameChunk *)RecordingFileChunk::newChunk(data+sizeof(dword),
			size, Endian::xarxa);




		dword nch=recvFrame->getNSubChunks();
		for (dword i=0; i<nch; i++)
		{
			RecordingFileChunk *ch=recvFrame->getSubChunk(i);
			cout<<" -- chunk:"<<ch->getClass()<<endl;
		}


		
		char framename[255];
		sprintf(framename, "RMframe_%.06d.jpg", recModFrame);
		File f(string(framename), File::writeAccessMode, true);
		f.write(recvFrame->getFrame(), recvFrame->getFrameSize());

		cout << "ok" << endl;
		
//		cout << StrUtils::hexDump(string((char*)RMreadFrame->getData(), RMreadFrame->getSize())) << endl;
		
	}	
	cout << "stopped getting frames" << endl;
	
	// Prova endSession (RecordingModule)
	cout << "ending Recording Module session..." << endl;
	RecordingModuleInterface::endSessionParams ep;
	ep.devId=devId;

	ep.toNetworkEndian();
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::endSessionServiceId, (byte*)&ep,
				ep.size(), 0, recordingModule->origen, true);

	RPCPacket *RMendSession=NULL;
	try
	{
		RMendSession=rpcRM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (recordingModule.endSession)" << endl;
		return -1;
	}

	if (RMendSession==NULL)
		return -1;
	
	cout << "RMsession ended" << endl;

	delete pk;

	return 0;
}

