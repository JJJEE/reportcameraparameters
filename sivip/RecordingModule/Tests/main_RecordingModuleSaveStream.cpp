#include <version.h>
#include <DecodeModule/DecodeModule.h>
#include <Utils/RPC.h>
//#include <CentralDirectory/CentralDirectoryService.h>
#include <ServiceFinder/ServiceFinder.h>
#include <RecordingModule/RecordingModuleInterface.h>
#include <Utils/ServiceException.h>
#include <Plugins/GestorImageTypes.h>
#include <Utils/Timer.h>
#include <Utils/Types.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <RecordingModule/RecordingFileFrameChunk.h>
#include <RecordingModule/RecordingFileStringMetadataChunk.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>

#include <signal.h>

using namespace std;

struct timeval tini, tend;
int currFrame=0;

#define DEFAULT_ID 10
#define N_FRAMES 25

int main(int argc, char *argv[])
{
	int devId=DEFAULT_ID;
	int nFrames;

	if (argc>1)
		devId=atoi(argv[1]);

	if (argc>2)
		nFrames=atoi(argv[2]);
	else
		nFrames=N_FRAMES;

	Canis cn(Address(IP("0.0.0.0"), 0), 0);
	RPCPacket *centralDir=ServiceFinder::getBestCentralDirectory(&cn, Address(IP("0.0.0.0"), 0));
	RPCPacket *decodeModule=ServiceFinder::getBestSubsys(string("DecodeModule"), &cn);
	RPCPacket *recordingModule=ServiceFinder::getBestSubsys(string("RecordingModule"), &cn);
	
	if (decodeModule==NULL || recordingModule==NULL)
		return -1;
		
	cout << "Best subsystem found for type DecodeModule: " << decodeModule->a->toString() << endl;
	cout << "Best subsystem found for type RecordingModule: " << recordingModule->a->toString() << endl;

	RPC rpcDM(*decodeModule->a);
	RPC rpcRM(*recordingModule->a);
	
	// Prova startSession
	cout << "starting session..." << endl;
	
	IPDeviceID deviceId;
	deviceId.id=devId;
	
	deviceId.toNetworkEndian();
	
	RPCPacket *pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::startSessionServiceId, (byte*)&deviceId,
				sizeof(deviceId), 0, decodeModule->origen);

	RPCPacket *startSession=NULL;
	try
	{
		startSession=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.startSession)" << endl;
		return -1;
	}
	
	if (startSession==NULL)
		return -1;
	
	cout << "session started" << endl;

	// Prova getCodecInUse
	cout << "geting codec in use..." << endl;
	IPCodecInfo codecInfo;
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::getCodecInUseServiceId, NULL,
				0, 0, decodeModule->origen);

	RPCPacket *getCodecInUse=NULL;
	try
	{
		getCodecInUse=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.setFrameBufferSize)" << endl;
		return -1;
	}

	if (getCodecInUse==NULL)
		return -1;
		
	memmove(&codecInfo, getCodecInUse->getData(), getCodecInUse->getSize());
	
	// El fourcc en network order es com esta be per fer una string
	string fourccStr((char*)&codecInfo.fourcc, sizeof(codecInfo.fourcc));
	
	codecInfo.toLocalEndian();
				
	cout << "Codec In Use: " << fourccStr
		<< ", quality: " << codecInfo.quality << ", bitrate: " << codecInfo.bitrate << endl;
	
	// Prova endSession
	cout << "ending Decode Module session..." << endl;
	deviceId.id=1;
	
	deviceId.toNetworkEndian();
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), DecodeModuleInterface::endSessionServiceId, (byte*)&deviceId,
				sizeof(deviceId), 0, decodeModule->origen);

	RPCPacket *endSession=NULL;
	try
	{
		endSession=rpcDM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.endSession)" << endl;
		return -1;
	}
	
	if (endSession==NULL)
		return -1;
	
	cout << "session ended" << endl;

	delete pk;

	// Prova RecordingModule::startSession
	cout << "Starting session with RecordingModule..." << endl;
	RecordingModuleInterface::startSessionParams ssParams;
	
	ssParams.devId=devId;
	ssParams.date=new RecordingFileDateMetadataChunk();
	ssParams.date->setCurrentDate();
	ssParams.recType=RecordingFileHeaderChunk::REC_MANUAL;
	ssParams.fourcc=codecInfo.fourcc;
	ssParams.fpswhole=25;
	ssParams.fpsfrac=0;
	ssParams.isRecoded=false;
	ssParams.isCyclic=false;
	
	dword ssParamsLen;
	void *ssParamsData=ssParams.toNetwork(&ssParamsLen);
	
//	cout << "Sending: " << StrUtils::hexDump(string((char*)ssParamsData, ssParamsLen)) << endl;

	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::startSessionServiceId, (byte*)ssParamsData,
				ssParamsLen, 0, recordingModule->origen, true);

	delete [] (byte*)ssParamsData;

	RPCPacket *RMstartSession=NULL;
	try
	{
		RMstartSession=rpcRM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (RecordingModule::startSession)" << endl;
		return -1;
	}

	if (RMstartSession==NULL)
		return -1;
	
	delete pk;
	
	cout << "Started Recording Module session" << endl;	
	
	// Prova de gravacio directa a decode module, amb streaming :)
	cout << "starting recording stream in recording module..." << endl;
	RecordingModuleInterface::startRecordingStreamParams srsp;
	srsp.toNetworkEndian();
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::startRecordingStreamServiceId, (byte*)&srsp,
				sizeof(srsp), 0, recordingModule->origen, true);

	RPCPacket *RMstartRecordingStream=NULL;
	try
	{
		RMstartRecordingStream=rpcRM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (recordingModule.startRecordingStream)" << endl;
		return -1;
	}

	if (RMstartRecordingStream==NULL)
		return -1;
	
	cout << "recording stream started" << endl;

	delete pk;
	
	cout << "press return to stop recording..." << endl;
	Timer recTimer;
	recTimer.start();
	getchar();
	recTimer.stop();
	TimerInstant recTime=recTimer.time();
	
	// Per la prova de lectura de frames
	RecordingFileDateMetadataChunk currSecsDate;
	double currSecs=currSecsDate.microtime();

	cout << "stopping recording stream in recording module..." << endl;
	RecordingModuleInterface::stopRecordingStreamParams strsp;
	strsp.toNetworkEndian();
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::stopRecordingStreamServiceId, (byte*)&strsp,
				sizeof(strsp), 0, recordingModule->origen, true);

	RPCPacket *RMstopRecordingStream=NULL;
	try
	{
		RMstopRecordingStream=rpcRM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (recordingModule.stopRecordingStream)" << endl;
		return -1;
	}

	if (RMstopRecordingStream==NULL)
		return -1;
	

	cout << "Getting recorded segments..." << endl;
	RecordingModuleInterface::listRecordingsParams lrp;
	void *lrpData=lrp.toNetwork();
	
	// i FILTREm!!
	lrp.nRecsFilter=1;
	lrp.recordings=new RecordingModuleInterface::listRecordingsParams::recording;
	lrp.recordings[0].id=0xffffffffffffffffll;
	lrp.recordings[0].devId=devId;
	lrp.recordings[0].recType=RecordingFileHeaderChunk::REC_HOWMANY;

//	cout << "data: " << lrpData << " size " << lrp.size() << endl;
//	cout << StrUtils::hexDump(string((char*)lrpData, lrp.size())) << endl;
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::listRecordingsServiceId, 
			(byte*)lrpData, lrp.size(), 0, recordingModule->origen, true);

	delete [] (byte*)lrpData;

	RPCPacket *RMlistRecordings=NULL;
	try
	{
		RMlistRecordings=rpcRM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (recordingModule.stopRecordingStream)" << endl;
		return -1;
	}

	if (RMlistRecordings==NULL)
		return -1;
	
	delete pk;
	
	cout << "got recordings... " << endl;

	byte *data=RMlistRecordings->getData();
	lrp.toLocal(data);

	cout.fill('0');
	cout.width(3);
	cout << "Found " << lrp.nRecordings << " recordings, spanning from " << 
		lrp.absStartDate.secs << "." << lrp.absStartDate.millis << " to " <<
		lrp.absEndDate.secs << "." << lrp.absEndDate.millis << "." << endl;

	for (qword i=0; i<lrp.nRecordings; i++)
	{
		cout << "Recording number " << i << ": device number " << lrp.recordings[i].devId  << ", type " << 
			lrp.recordings[i].recType << endl;
		cout.width(3);
		cout << "\tfrom " << lrp.recordings[i].startDate.secs << "." << lrp.recordings[i].startDate.millis << " to " <<
			lrp.recordings[i].endDate.secs << "." << lrp.recordings[i].endDate.millis << endl;
		cout.width(0);
	}	
		
	cout << "finished listing" << endl;

	// Prova d'obtencio d'alguns frames :)
	cout << "trying to get some frames from recording module..." << endl;
	
	nFrames=(int)(25.0*recTime.seconds());
	
	// La meitat dels q hem gravat ja fara el fet
	for (dword recModFrame=0; recModFrame<nFrames; recModFrame++)
	{
		RecordingModuleInterface::readFrameParams rfp;
		rfp.chunk=new RecordingFileFrameChunk();
	
		RecordingFileDateMetadataChunk *frameDate=(RecordingFileDateMetadataChunk*)rfp.chunk->getSubChunk(0);
		double secs=currSecs-((double)nFrames)/25.0;
		secs+=((double)recModFrame)/25.0;
		frameDate->setDate((qword)floor(secs), (word)((secs-floor(secs))*1000.0));
		
		cout << "set seconds to " << frameDate->getSecs() << ".";
		cout.fill('0');
		cout.width(3);
		cout << frameDate->getMillis() << "... ";
		cout.width(0);

		dword rfParamsLen;
		void *rfParamsData=rfp.toNetwork(&rfParamsLen);
		
		pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::readFrameServiceId,
		(byte*)rfParamsData, rfParamsLen, 0, recordingModule->origen, true);

		RPCPacket *RMreadFrame=NULL;
		try
		{
			RMreadFrame=rpcRM.call(*pk);
		}
		catch (Exception &e)
		{
			cout << e.getClass() << ": " << e.getMsg() << " (recordingModule.readFrame)" << endl;
			return -1;
		}

		if (RMreadFrame==NULL)
			return -1;

		delete pk;
		
		cout << "got frame " << recModFrame << ", saving... ";

		byte *data=RMreadFrame->getData();
		dword size=*(dword*)data;
		Endian::from(Endian::xarxa, &size, sizeof(dword));
		RecordingFileFrameChunk *recvFrame=(RecordingFileFrameChunk *)RecordingFileChunk::newChunk(data+sizeof(dword),
			size, Endian::xarxa);
		
		char framename[255];
		sprintf(framename, "RMframe_%.06d.jpg", recModFrame);
		File f(string(framename), File::writeAccessMode, true);
		f.write(recvFrame->getFrame(), recvFrame->getFrameSize());

		cout << "ok" << endl;
		
//		cout << StrUtils::hexDump(string((char*)RMreadFrame->getData(), RMreadFrame->getSize())) << endl;
		
	}	
	cout << "stopped getting frames" << endl;

	
	// Prova endSession (RecordingModule)
	cout << "ending Recording Module session..." << endl;
	RecordingModuleInterface::endSessionParams ep;
	ep.devId=devId;

	ep.toNetworkEndian();
	
	pk=new RPCPacket(Address(IP("192.168.0.128"), 12345), RecordingModuleInterface::endSessionServiceId, (byte*)&ep,
				ep.size(), 0, recordingModule->origen, true);

	RPCPacket *RMendSession=NULL;
	try
	{
		RMendSession=rpcRM.call(*pk);
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << " (recordingModule.endSession)" << endl;
		return -1;
	}

	if (RMendSession==NULL)
		return -1;
	
	cout << "RMsession ended" << endl;

	delete pk;

	return 0;
}

