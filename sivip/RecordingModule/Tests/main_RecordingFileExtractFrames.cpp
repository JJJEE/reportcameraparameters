#include <version.h>
#include <Utils/Types.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileException.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <RecordingModule/RecordingFileFrameChunk.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <RecordingModule/RecordingFileDateMetadataChunk.h>
#include <RecordingModule/RecordingFilePTZMetadataChunk.h>
#include <RecordingModule/RecordingFileSegmentMetadataChunk.h>
#include <RecordingModule/RecordingFileStringMetadataChunk.h>
#include <RecordingModule/RecordingFileIndexChunk.h>
#include <RecordingModule/RecordingFileFrameInfoMetadataChunk.h>
#include <iostream>

#include <signal.h>

using namespace std;

#define INIT_RMCHUNK(_NAME,_VAR) _NAME *_VAR=new _NAME(); if (_VAR!=NULL) delete _VAR;


int main(int argc, char *argv[])
{
	{
		INIT_RMCHUNK(RecordingFileHeaderChunk, b);
		INIT_RMCHUNK(RecordingFileFrameChunk, c);
		INIT_RMCHUNK(RecordingFileMetadataChunk, d);
		INIT_RMCHUNK(RecordingFileDateMetadataChunk, e);
		INIT_RMCHUNK(RecordingFilePTZMetadataChunk, f);
		INIT_RMCHUNK(RecordingFileSegmentMetadataChunk, g);
		INIT_RMCHUNK(RecordingFileStringMetadataChunk, h);
		INIT_RMCHUNK(RecordingFileIndexChunk, i);
		INIT_RMCHUNK(RecordingFileFrameInfoMetadataChunk, j);
	}

	string fname=string("DecodeModuleFrames.srf");
	if (argc>1)
		fname=string(argv[1]);
	
	RecordingFile *rf;
	
	try
	{
		// Obre el ja creat
		cout << "Opening file in read mode" << endl;
		rf=new RecordingFile(fname);
			
		cout << "Reading RFHC" << endl;
		dword blockSize;
		void *rfhcdata=rf->readBlock(&blockSize);
		cout << "Read header block, " << blockSize << " bytes" << endl;

		cout << "File Endian: " << Endian::endianToString(rf->getEndian()) << endl;

		cout << "Creating chunk..." << endl;
		RecordingFileHeaderChunk *ch=(RecordingFileHeaderChunk *)RecordingFileChunk::newChunk(rfhcdata, blockSize, rf->getEndian());
		
		cout << "read " << ch->getClass() << ", " << ch->size() << endl;
		cout << "fourcc " << ch->getFourCC() << ", FPS " << ch->getFPS() << 
			", Version: " << ch->getVersion() << endl;

//		cout << endl << "Extracting alternate frames..." << endl;
		cout << endl << "Extracting frames..." << endl;
		cout << "Searching for SegmentInfo" << endl;
		
		dword nch=ch->getNSubChunks();
		RecordingFileSegmentMetadataChunk *segChunks[2]={NULL, NULL};
		for (dword i=0; i<nch; i++)
		{
			RecordingFileChunk *segch=ch->getSubChunk(i);
			if (segch->getClass()==string("RecordingFileSegmentMetadataChunk"))
			{
				if (segChunks[0]==NULL)
					segChunks[0]=(RecordingFileSegmentMetadataChunk*)segch;
				else
				{
					segChunks[1]=(RecordingFileSegmentMetadataChunk*)segch;
					break;	// Ja estem :)
				}
			}
		}
		
		// Els segments nomes tenen un subchunk...
		if (((RecordingFileDateMetadataChunk*)segChunks[0]->getSubChunk(0))->microtime() >
				((RecordingFileDateMetadataChunk*)segChunks[1]->getSubChunk(0))->microtime())
		{
			// Li donem  la volta.
			RecordingFileSegmentMetadataChunk *tmp=segChunks[0];
			segChunks[0]=segChunks[1];
			segChunks[1]=tmp;
		}
		
		dword fr=0;
		for (int i=0; i<2; i++)
		{
			fileOffset frameOff=segChunks[i]->getFirstFrameOffset();
			dword nf=segChunks[i]->getNFrames();
			
			for (dword f=0; f<nf; f++, fr++)
			{
				cout << "segment " << i << " frame " << f << ", global frame " << fr << endl;
				void *framedata;
				if (f==0)
					framedata=rf->readBlock(&blockSize, frameOff, true);
				else
					framedata=rf->readBlock(&blockSize);
					
				RecordingFileChunk *ch=RecordingFileChunk::newChunk(framedata, blockSize, rf->getEndian());
			
				if (ch->getClass()==string("RecordingFileFrameChunk"))
				{
					RecordingFileFrameChunk *frch=(RecordingFileFrameChunk *)ch;
					cout << "Found frame number " << fr << endl;

					char framename[255];
					sprintf(framename, "extractedframe_%.06d.jpg", fr);
					File f(string(framename), File::writeAccessMode, true);
					f.write(frch->getFrame(), frch->getFrameSize());
				}
			}
		}

		delete rf;
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << endl;
		cout << "No more blocks to extract at offset " << rf->tell() << endl;
		
		cout << "Cache hit ratio: " << rf->getCacheHitRatio() << endl;
		cout << "Cache hits: " << rf->getCacheHits() << endl;
		cout << "Cache misses: " << rf->getCacheMisses() << endl;
		delete rf;
	}
	return 0;
}

