#include <version.h>
#include <Utils/Types.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <iostream>

#include <signal.h>

using namespace std;

int main()
{
	try
	{
		// Crea i peta vells
		RecordingFile *rf=new RecordingFile(string("test_file.srf"), false, true);

		cout << "File created, creating RFHC" << endl;
		RecordingFileHeaderChunk *rfhc=new RecordingFileHeaderChunk();
		rfhc->setFourCC(CODEC_FOURCC('j','p','e','g'));
		rfhc->setVersion(0,100);
		rfhc->setRecType(RecordingFileHeaderChunk::REC_MANUAL);
		rfhc->setFPS(25);
		
		cout << "RFHC created, saving to file (" << rfhc->size() << " bytes)" << endl;
		void *rfhcdata=rfhc->getData();
		rf->writeBlock(rfhcdata, rfhc->size());

		delete [] (byte*)rfhcdata;
		delete rf;

		// Obre el ja creat
		cout << "Opening file again, in read mode" << endl;
		rf=new RecordingFile(string("test_file.srf"));
			
		cout << "Reading RFHC" << endl;
		dword blockSize;
		rfhcdata=rf->readBlock(&blockSize);
		cout << "Read header block, " << blockSize << " bytes" << endl;
		RecordingFileChunk *ch=RecordingFileChunk::newChunk(rfhcdata, blockSize, rf->getEndian());
		
		delete rf;

		cout << "read " << ch->getClass() << ", " << ch->size() << endl;
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << endl;
	}
	return 0;
}

