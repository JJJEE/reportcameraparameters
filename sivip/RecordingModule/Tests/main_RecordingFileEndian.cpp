#include <version.h>
#include <Utils/Types.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <iostream>

#include <signal.h>

using namespace std;

int main(int argc, char *argv[])
{
	string fname=string("test_file.srf");
	if (argc>1)
		fname=string(argv[1]);
		
	try
	{
		RecordingFileHeaderChunk *dummy=new RecordingFileHeaderChunk();
		delete dummy;
		// Obre el ja creat
		cout << "Opening file in read mode" << endl;
		RecordingFile *rf=new RecordingFile(fname);
			
		cout << "Reading RFHC" << endl;
		dword blockSize;
		void *rfhcdata=rf->readBlock(&blockSize);
		cout << "Read header block, " << blockSize << " bytes" << endl;

		cout << "File Endian: " << Endian::endianToString(rf->getEndian()) << endl;

		cout << "Creating chunk..." << endl;
		RecordingFileHeaderChunk *ch=(RecordingFileHeaderChunk *)RecordingFileChunk::newChunk(rfhcdata, blockSize, rf->getEndian());
		

		delete rf;

		cout << "read " << ch->getClass() << ", " << ch->size() << endl;
		cout << "fourcc " << ch->getFourCC() << ", FPS " << ch->getFPS() << 
			", Version: " << ch->getVersion() << endl;
	}
	catch (Exception &e)
	{
		cout << e.getClass() << ": " << e.getMsg() << endl;
	}
	return 0;
}

