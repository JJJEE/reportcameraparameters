/*
 *  RecordingModuleSessionAlreadyStablishedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingModuleSessionAlreadyStablishedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

RecordingModuleSessionAlreadyStablishedException::RecordingModuleSessionAlreadyStablishedException(string msg, int code): Exception(msg, code)
{

}

RecordingModuleSessionAlreadyStablishedException::RecordingModuleSessionAlreadyStablishedException(int code, string msg): Exception(msg, code)
{

}

RecordingModuleSessionAlreadyStablishedException::RecordingModuleSessionAlreadyStablishedException(SerializedException &se): Exception(se)
{

}

RecordingModuleSessionAlreadyStablishedException::~RecordingModuleSessionAlreadyStablishedException()
{
   
}

string RecordingModuleSessionAlreadyStablishedException::getClass()
{
	string c=string("RecordingModuleSessionAlreadyStablishedException");
	return c;
}

