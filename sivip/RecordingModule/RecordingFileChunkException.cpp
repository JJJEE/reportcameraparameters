/*
 *  RecordingFileChunkException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

RecordingFileChunkException::RecordingFileChunkException(string msg, int code): Exception(msg, code)
{

}

RecordingFileChunkException::RecordingFileChunkException(int code, string msg): Exception(msg, code)
{

}

RecordingFileChunkException::RecordingFileChunkException(SerializedException &se): Exception(se)
{

}

RecordingFileChunkException::~RecordingFileChunkException()
{
   
}

string RecordingFileChunkException::getClass()
{
	string c=string("RecordingFileChunkException");
	return c;
}

