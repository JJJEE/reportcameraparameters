#include <RecordingModule/RecordingModuleDiskWriterEntryGroup.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Utils/StrUtils.h>
#include <Threads/Thread.h>
#include <RecordingModule/RecordingFileFrameChunk.h>
#include <Utils/Log.h>
#include <string.h>

#pragma mark ***
#pragma mark *** RecordingModuleDiskWriterEntryGroup
#pragma mark ***

#pragma mark *** Constructores
RecordingModuleDiskWriterEntryGroup::RecordingModuleDiskWriterEntryGroup() :
	offset(0), size(0)
{
	STACKTRACE_INSTRUMENT();
}

RecordingModuleDiskWriterEntryGroup::~RecordingModuleDiskWriterEntryGroup()
{
	STACKTRACE_INSTRUMENT();
//	list<RecordingModuleDiskWriterEntry*>::iterator eIt;
//	for (eIt=entries.begin(); eIt!=entries.end(); eIt++)
//	{
//		RecordingModuleDiskWriterEntry *ent=*eIt;
//#ifndef WIN32
//		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" [~RecordingModuleDiskWriterEntryGroup] ENTRIES ENUM ") << (ent->getChunk()!=NULL?ent->getChunk()->getClass()+string(" "):" ") << (void*)ent->getChunk() << " " << ((RecordingFileFrameChunk*)ent->getChunk())->getFrame() << endl;
//#endif	
//	}
//
//	eIt=entries.begin();

	list<RecordingModuleDiskWriterEntry*>::iterator eIt=entries.begin();
	while (eIt!=entries.end())
	{
		RecordingModuleDiskWriterEntry *ent=*eIt;
//#ifndef WIN32
//		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" [~RecordingModuleDiskWriterEntryGroup] DELETE ") << (ent->getChunk()!=NULL?ent->getChunk()->getClass()+string(" "):" ") << (void*)ent->getChunk() << " " << ((RecordingFileFrameChunk*)ent->getChunk())->getFrame() << endl;
//#endif	
		delete ent;
		
		eIt=this->entries.erase(eIt);
	}
}

#pragma mark *** Metodes
bool RecordingModuleDiskWriterEntryGroup::addEntry(
	RecordingModuleDiskWriterEntry *entry)
{
	STACKTRACE_INSTRUMENT();
	if (this->getDataLength()==0)
	{
		this->offset=entry->getOffset();
		this->size=entry->getSize() + sizeof(dword);
		this->entries.push_back(entry);
//		string msg=string("\nGroup::addEntry: 1st: ") +
//			StrUtils::decToString(this->size) + string(" bytes @ ") +
//			StrUtils::decToString(this->offset) + string("\n");
//		cout << msg << endl;
		return true;
	}
	
	list<RecordingModuleDiskWriterEntry*>::iterator eIt;
	for (eIt=entries.begin(); eIt!=entries.end(); eIt++)
	{
		RecordingModuleDiskWriterEntry *ent=*eIt;
		
		// Si son consecutius, d'una manera o una altra...
		if (entry->getOffset() == (ent->getOffset() + ent->getSize() + sizeof(dword)))
		{
			this->size+=entry->getSize() + sizeof(dword);
//			string msg=string("\nGroup::addEntry: (") +
//				StrUtils::decToString(entry->getOffset()) + string(", ") +
//				StrUtils::decToString(entry->getSize()) + string(") + (") +
//				StrUtils::decToString(ent->getOffset()) + string(", ") +
//				StrUtils::decToString(ent->getSize()) + string(") = (") +
//				StrUtils::decToString(this->offset) + string(", ") +
//				StrUtils::decToString(this->size) + string(")");
//			cout << msg;
//			cout.flush();
			this->entries.push_back(entry);
			return true;
		}
		else if (ent->getOffset() == (entry->getOffset()+entry->getSize() + sizeof(dword)))
		{
			this->offset=entry->getOffset();
			this->size+=entry->getSize() + sizeof(dword);
//			string msg=string("\nGroup::addEntry: (") +
//				StrUtils::decToString(entry->getOffset()) + string(", ") +
//				StrUtils::decToString(entry->getSize()) + string(") + (") +
//				StrUtils::decToString(ent->getOffset()) + string(", ") +
//				StrUtils::decToString(ent->getSize()) + string(") = (") +
//				StrUtils::decToString(this->offset) + string(", ") +
//				StrUtils::decToString(this->size) + string(")");
//			cout << msg;
//			cout.flush();
			this->entries.push_back(entry);
			return true;
		}
		else if (ent->getOffset() == entry->getOffset()
			&& ent->getSize() == entry->getSize()
			&& ent->getChunk() == entry->getChunk())
		{
			// Si son el mateix chunk, no hem de fer res amb offsets i tamany.
			// Ens assegurem pero que com a minim la entry nova queda marcada
			// com a no delete
			entry->setDeleteAtDestructor(false);
//			string msg=string("\nGroup::addEntry: (") +
//				StrUtils::decToString(entry->getOffset()) + string(", ") +
//				StrUtils::decToString(entry->getSize()) + string(") + (") +
//				StrUtils::decToString(ent->getOffset()) + string(", ") +
//				StrUtils::decToString(ent->getSize()) + string(") = (") +
//				StrUtils::decToString(this->offset) + string(", ") +
//				StrUtils::decToString(this->size) + string(") [ALREADY "
//				"CONTAINED]");
//			cout << msg;
//			cout.flush();

			// No l'afegim, pero com que retornem true, fem el delete de
			// l'entry ja.
			delete entry;
			return true;
		}
	}
	
	return false;
}

bool RecordingModuleDiskWriterEntryGroup::joinGroup(
	RecordingModuleDiskWriterEntryGroup *group)
{
	STACKTRACE_INSTRUMENT();
	// Si son consecutius, d'una manera o una altra...
	bool add=false;
	if (this->offset==(group->offset + group->size))
	{
		this->offset = group->offset;
		this->size += group->size;
		add=true;
	}
	else if (group->offset == (this->offset + this->size))
	{
		this->size += group->size;
		add=true;
	}
	
	if (add)
	{
		list<RecordingModuleDiskWriterEntry*>::iterator eIt=group->entries.begin();
		while (eIt!=group->entries.end())
		{
			RecordingModuleDiskWriterEntry *entry=*eIt;
			this->entries.push_back(entry);
			eIt=group->entries.erase(eIt);
		}
	}
	
	return add;
}

fileOffset RecordingModuleDiskWriterEntryGroup::getOffset()
{
	STACKTRACE_INSTRUMENT();
	return this->offset;
}

void *RecordingModuleDiskWriterEntryGroup::getData()
{
	STACKTRACE_INSTRUMENT();
	byte *data = new byte[this->getDataLength()];
	
	if (data==NULL)
		throw NotEnoughMemoryException("Not enough memory to group "
			"RecordingModuleDiskWriterEntries");
		
	list<RecordingModuleDiskWriterEntry*>::iterator eIt;
	for (eIt=entries.begin(); eIt!=entries.end(); eIt++)
	{
		RecordingModuleDiskWriterEntry *entry=*eIt;
		void *chData=entry->getChunk()->getData(entry->getChunk()->getEndian());
		dword chLen=entry->getSize();
		dword chOff = (dword)(entry->getOffset() - this->getOffset());

//	if (entry->getChunk()!=NULL && entry->getChunk()->getClass()==string("RecordingFileFrameChunk"))
//	{
//#ifndef WIN32
//		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" [RecordingModuleDiskWriterEntryGroup] WRITING ") << (entry->getChunk()!=NULL?entry->getChunk()->getClass()+string(" "):" ") << (void*)entry->getChunk() << " " << ((RecordingFileFrameChunk*)entry->getChunk())->getFrame() << endl;
//		STACKTRACE_DUMP();
//#endif	
//	}
//

		// TODO 20090806: Aqui hauriem de saber l'endian del fitxer!!
		memmove(&data[chOff], &chLen, sizeof(dword));
		memmove(&data[chOff+sizeof(dword)], chData, chLen);
			
		delete [] (byte*)chData;
	}
	
	return data;
}

dword RecordingModuleDiskWriterEntryGroup::getDataLength()
{
	STACKTRACE_INSTRUMENT();
	return this->size;
}

RecordingFileChunk *RecordingModuleDiskWriterEntryGroup::getChunkForOffset(fileOffset off)
{
	STACKTRACE_INSTRUMENT();

	RecordingModuleDiskWriterEntry *ent=this->getEntryForOffset(off);
	if (ent!=NULL)
		return ent->getChunk();
		
	return NULL;
}

RecordingModuleDiskWriterEntry *RecordingModuleDiskWriterEntryGroup::getEntryForOffset(fileOffset off)
{
	STACKTRACE_INSTRUMENT();

	if (this->getOffset() <= off
		&& this->getOffset() + this->getDataLength() > off)
	{
		list<RecordingModuleDiskWriterEntry*>::iterator eIt;
		for (eIt=this->entries.begin(); eIt!=this->entries.end(); eIt++)
		{
			RecordingModuleDiskWriterEntry *ent = *eIt;
			
			if (ent->getOffset() <= off
				&& ent->getOffset() + ent->getSize() + sizeof(dword) > off)
			{
				// Com que el retornem cap a fora, evitem que es borri.
				// ATENCIO: SERA RESPONSABILITAT DEL RECEPTOR COMPROVAR SI HA
				// DE FER EL DELETE O ENCARA NO S'HA DUMPAT.
				ent->setDeleteAtDestructor(false);
				return ent;
			}
		}
		
	}
	
	return NULL;
}

dword RecordingModuleDiskWriterEntryGroup::getEntryCount()
{
	STACKTRACE_INSTRUMENT();
	return this->entries.size();
}

bool RecordingModuleDiskWriterEntryGroup::isDelayed()
{
	STACKTRACE_INSTRUMENT();
	// Es delayed si totes les entries son delayed!
	bool delayed=true;
	list<RecordingModuleDiskWriterEntry*>::iterator eIt;
	for (eIt=entries.begin(); eIt!=entries.end() && delayed; eIt++)
	{
		RecordingModuleDiskWriterEntry *entry=*eIt;
		// El primer cop que sigui false fara parar el bucle :P
		delayed = entry->isDelayed();
	}
	
	return delayed;
}

bool RecordingModuleDiskWriterEntryGroup::isReady()
{
	STACKTRACE_INSTRUMENT();
	// Un grup esta llest per ser escrit si:
	//		- No es delayed
	//		- Es delayed pero te un minim de entryReadyLimitCount chunks o 
	//			maxGroupLength bytes
	//		- Es delayed pero te una edat igual o superior a
	//			RecordingModuleDiskWriterEntry.maxAge segons
	
	bool ready=!this->isDelayed();
	
	list<RecordingModuleDiskWriterEntry*>::iterator eIt;
	for (eIt=entries.begin(); eIt!=entries.end() && !ready; eIt++)
	{
		RecordingModuleDiskWriterEntry *entry=*eIt;
		
		ready=(entry->getAge() >= RecordingModuleDiskWriterEntry::maxAge
			|| this->getEntryCount() >=
					RecordingModuleDiskWriterEntryGroup::entryReadyLimitCount
			|| this->getDataLength() >=
					RecordingModuleDiskWriterEntryGroup::maxGroupLength);
	
	}
	
	return ready;
}
