/*
 *  RecordingFileAlarmMetadataChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileAlarmMetadataChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <math.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

RecordingFileAlarmMetadataChunk::RecordingFileAlarmMetadataChunk() 
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileAlarmMetadataChunk::newChunk;
	className=getClass();

	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// sizeof(byte) pel nombre d'entrades de la llista
	// La resta autoexplicatius (o no, pero poc important si ho son o no)
	chunkBaseSize=sizeof(dword)+sizeof(byte)+className.length()+sizeof(nSubChunks)+sizeof(byte);
}

RecordingFileAlarmMetadataChunk::~RecordingFileAlarmMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileChunk* RecordingFileAlarmMetadataChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);
	
//	if (chunkClass!=getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	RecordingFileAlarmMetadataChunk *resChunk=new RecordingFileAlarmMetadataChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

	resChunk->chunkSize=size;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	
	chunkBytes+=chunkTypeLen;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (resChunk->nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
		
	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

//	if (dataSize!=chunkSizeCheck)
//		throw RecordingFileChunkException(0, resChunk->getClass()+string(": Invalid data size (")+
//											StrUtils::decToString(dataSize)+string("/")+
//											StrUtils::decToString(chunkSizeCheck)+string(")"));

	byte nAlarms = *((byte*)chunkBytes);
	chunkBytes+=sizeof(byte);

	for(int i=0;i<nAlarms;i++)
	{
		AMAlarmValue av;
		av.toLocal((char*)chunkBytes, 0);
		chunkBytes+=av.size();
		resChunk->setAlarm(av);

		if (chunkBytes-chunkBytesBase>size)
			throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	}	

	return (RecordingFileChunk*)resChunk;
}

void* RecordingFileAlarmMetadataChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	byte *data=new byte[chunkSize];
	byte *chunkBytes=data;
	
	if (data==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
		
	byte chunkTypeLen=getClass().length();
	*chunkBytes=chunkTypeLen;
	chunkBytes++;
	
	memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
	chunkBytes+=chunkTypeLen;
	
	if (nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
	*((dword*)chunkBytes)=nSubChunks;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	*((dword*)chunkBytes)=this->size();//chunkSizeCheck;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((byte*)chunkBytes)=activeAlarms.size();
	Endian::to(dstEndian, chunkBytes, sizeof(float));
	chunkBytes+=sizeof(byte);
	
	for(list<AMAlarmValue>::iterator it=activeAlarms.begin(); it!=activeAlarms.end(); it++)
	{
		it->toNetwork((char*)chunkBytes);	
		chunkBytes+=it->size();
	}

	
	return data;
}

string RecordingFileAlarmMetadataChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileAlarmMetadataChunk";
	return c;
}

void RecordingFileAlarmMetadataChunk::setAlarm(AMAlarmValue av)
{
	STACKTRACE_INSTRUMENT();
	for(list<AMAlarmValue>::iterator it=activeAlarms.begin(); it!=activeAlarms.end(); it++)
	{
		if((*it) == av)
			return;
	}
	activeAlarms.push_back(av);
}

void RecordingFileAlarmMetadataChunk::endAlarm(AMAlarmId aid)
{
	STACKTRACE_INSTRUMENT();
	for(list<AMAlarmValue>::iterator it=activeAlarms.begin(); it!=activeAlarms.end(); it++)
	{
		if((*it) == aid)
		{
			activeAlarms.erase(it);
			return;
		}
	}
}

bool RecordingFileAlarmMetadataChunk::getAlarmStatus(AMAlarmId aid)
{
	STACKTRACE_INSTRUMENT();
	for(list<AMAlarmValue>::iterator it=activeAlarms.begin(); it!=activeAlarms.end(); it++)
	{
		if((*it) == aid)
		{
			return it->raised;
		}
	}
	return false;
}

double RecordingFileAlarmMetadataChunk::getAlarmValue(AMAlarmId aid)
{
	STACKTRACE_INSTRUMENT();
	for(list<AMAlarmValue>::iterator it=activeAlarms.begin(); it!=activeAlarms.end(); it++)
	{
		if((*it) == aid)
		{
			return it->value;
		}
	}
	return NAN;
}

dword RecordingFileAlarmMetadataChunk::size()
{
	STACKTRACE_INSTRUMENT();
	int size=chunkBaseSize;
	for(list<AMAlarmValue>::iterator it=activeAlarms.begin(); it!=activeAlarms.end(); it++)
	{
		size+=it->size();
	}
	return size;
}
