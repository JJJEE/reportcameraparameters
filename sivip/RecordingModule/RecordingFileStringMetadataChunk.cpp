/*
 *  RecordingFileStringMetadataChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileStringMetadataChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

RecordingFileStringMetadataChunk::RecordingFileStringMetadataChunk(): name(string("")), value(string(""))
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileStringMetadataChunk::newChunk;
	
	className=getClass();
	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// La resta autoexplicatius (o no, pero poc important si ho son o no)
	chunkSize=sizeof(dword)+sizeof(byte)+className.length()+sizeof(nSubChunks)+sizeof(dword)*2+name.length()+value.length();
}

RecordingFileStringMetadataChunk::~RecordingFileStringMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileChunk* RecordingFileStringMetadataChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);
	
//	if (chunkClass!=getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	RecordingFileStringMetadataChunk *resChunk=new RecordingFileStringMetadataChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

	resChunk->chunkSize=size;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	
	chunkBytes+=chunkTypeLen;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (resChunk->nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
		
	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase+dataSize>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	dword nameLen=*((dword*)chunkBytes);
	Endian::from(endian, &nameLen, sizeof(dword));
	chunkBytes+=sizeof(dword);
		
	if (chunkBytes-chunkBytesBase+nameLen>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	resChunk->name=string((char*)chunkBytes, nameLen);
	chunkBytes+=nameLen;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	dword valueLen=*((dword*)chunkBytes);
	Endian::from(endian, &valueLen, sizeof(dword));
	chunkBytes+=sizeof(dword);
		
	if (chunkBytes-chunkBytesBase+valueLen>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->value=string((char*)chunkBytes, valueLen);
	chunkBytes+=valueLen;

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	return (RecordingFileChunk*)resChunk;
}

void* RecordingFileStringMetadataChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	byte *data=new byte[chunkSize];
	byte *chunkBytes=data;
	
	if (data==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
		
	byte chunkTypeLen=getClass().length();
	*chunkBytes=chunkTypeLen;
	chunkBytes++;
	
	memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
	chunkBytes+=chunkTypeLen;
	
	if (nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
	*((dword*)chunkBytes)=nSubChunks;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	*((dword*)chunkBytes)=sizeof(dword)*2+name.length()+value.length();
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((dword*)chunkBytes)=name.length();
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	memmove(chunkBytes, name.c_str(), name.length());
	chunkBytes+=name.length();
	
	*((dword*)chunkBytes)=value.length();
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	memmove(chunkBytes, value.c_str(), value.length());

	return data;
}

string RecordingFileStringMetadataChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileStringMetadataChunk";
	return c;
}


void RecordingFileStringMetadataChunk::setMetadata(string name, string value)
{
	STACKTRACE_INSTRUMENT();
	dword prevLen=this->name.length()+this->value.length();
	dword currLen=name.length()+value.length();

	this->name=name;
	this->value=value;

	chunkSize+=currLen-prevLen;
}

map<string,string> RecordingFileStringMetadataChunk::getMetadata()
{
	STACKTRACE_INSTRUMENT();
	map <string, string> metadata;
	
	metadata["name"]=name;
	metadata["value"]=value;
	
	return metadata;
}

