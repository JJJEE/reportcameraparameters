
#include <RecordingModule/RecordingModuleInterface.h>
#include <RecordingModule/RecordingModuleException.h>
#include <RecordingModule/RecordingModuleSessionAlreadyStablishedException.h>
#include <RecordingModule/RecordingModuleSessionNotStablishedException.h>
#include <RecordingModule/MaxActiveRecordingsReachedException.h>
#include <RecordingModule/RecordingModuleDiskWriter.h>
#include <RecordingModule/RecordingFileFrameIndices.h>
#include <RecordingModule/RecordingFileException.h>
#include <DecodeModule/DecodeModule.h>
#include <ModuleAccess/RecordingModuleSchedulerAccess.h>
#include <Plugins/GestorImageTypes.h>
#include <Plugins/GestorImageExceptions.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/SerializedException.h>
#include <Utils/RedirectCallException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <Utils/RPC.h>
#include <Utils/Timer.h>
#include <Utils/CheckPoint.h>
#include <Utils/Log.h>
#include <Plugins/GestorControlTypes.h>
#include <SessionManager/SessionManager>
#include <Exceptions/NullPointerException.h>
#include <Exceptions/IdNotFoundException.h>
#include <Exceptions/IdAlreadyInUseException.h>
#include <Exceptions/CameraNotConnectableException.h>
#ifndef WIN32
#include <unistd.h>
#include <dirent.h>
#else
#include <Utils/WindowsDefs.h>
#endif
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <iomanip>
#include <errno.h>
//#include <RecordingModule/RecordingModuleSchedulingThread.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define __STOP_SERVICE_ON_ALERT 1

using namespace std;

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

// ***
// *** RecordingModuleInterface::startSessionParams
// ***
#pragma mark *** RecordingModuleInterface::startSessionParams
RMStartSessionParams::RMStartSessionParams() : devId(0),
	date(NULL), fpswhole(25), fpsfrac(0), isRecoded(false), isCyclic(false),
	recType(RecordingFileHeaderChunk::REC_MANUAL), fourcc(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}
	
// Constructora per copia
RMStartSessionParams::RMStartSessionParams(const RMStartSessionParams &p):date(NULL)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->copy(&p);
}	

RMStartSessionParams::RMStartSessionParams(const void *b):date(NULL)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMStartSessionParams::~RMStartSessionParams()
{
	if (this->date!=NULL)
		delete this->date;
}

void RMStartSessionParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	if (this->date!=NULL)
	{
		delete this->date;
		this->date=NULL;
	}
	const byte *buf=(byte*)b;

	devId=*((int*)buf);
	Endian::from(Endian::xarxa, &devId, sizeof(devId));
	buf+=sizeof(int);
	
	dword chunkSize=*((dword*)buf);
	Endian::from(Endian::xarxa, &chunkSize, sizeof(chunkSize));
	buf+=sizeof(dword);

	date=(RecordingFileDateMetadataChunk *)RecordingFileChunk::newChunk(buf, chunkSize, Endian::xarxa);
	buf+=date->size();
	
	fpswhole=*((word*)buf);
	Endian::from(Endian::xarxa, &fpswhole, sizeof(fpswhole));
	buf+=sizeof(word);

	fpsfrac=*((word*)buf);
	Endian::from(Endian::xarxa, &fpsfrac, sizeof(fpsfrac));
	buf+=sizeof(word);

	isRecoded=*((bool*)buf);
	buf+=sizeof(bool);

	isCyclic=*((bool*)buf);
	buf+=sizeof(bool);
	
	recType=*((RecordingFileHeaderChunk::recType*)buf);
	Endian::from(Endian::xarxa, &recType, sizeof(RecordingFileHeaderChunk::recType));
	buf+=sizeof(RecordingFileHeaderChunk::recType);

	fourcc=*((dword*)buf);
	Endian::from(Endian::xarxa, &fourcc, sizeof(dword));
	buf+=sizeof(dword);
}

void* RMStartSessionParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	if(this->date==NULL)
	{
		cout<<"RecordingModuleInterface::startSessionParams::toNetwork this->date==NULL"<<endl;
		this->date = new RecordingFileDateMetadataChunk();
	}


	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RecordingModuleInterface::startSessionParams "
				"serialization buffer");
	}
	
	byte *buf=data;
	void *dataCh=date->getData(Endian::xarxa);
	
	*((int*)buf)=devId;
	Endian::to(Endian::xarxa, buf, sizeof(int));
	buf+=sizeof(int);


	*((dword*)buf)=date->size();
	Endian::to(Endian::xarxa, buf, sizeof(dword));
	buf+=sizeof(dword);
	memmove(buf, dataCh, date->size());
	delete [] (byte*)dataCh;
	buf+=date->size();

	*((word*)buf)=fpswhole;
	Endian::to(Endian::xarxa, buf, sizeof(word));
	buf+=sizeof(word);

	*((word*)buf)=fpsfrac;
	Endian::to(Endian::xarxa, buf, sizeof(word));
	buf+=sizeof(word);
		
	*((bool*)buf)=isRecoded;
	buf+=sizeof(bool);

	*((bool*)buf)=isCyclic;
	buf+=sizeof(bool);

	*((RecordingFileHeaderChunk::recType*)buf)=recType;
	Endian::to(Endian::xarxa, buf, sizeof(RecordingFileHeaderChunk::recType));
	buf+=sizeof(RecordingFileHeaderChunk::recType);

	*((dword*)buf)=fourcc;
	Endian::to(Endian::xarxa, buf, sizeof(dword));
	buf+=sizeof(dword);

	return data;
}

dword RMStartSessionParams::serializationSize()
{
	STACKTRACE_INSTRUMENT();

	if(this->date==NULL)
	{
		cout<<"RecordingModuleInterface::startSessionParams::serializationSize this->date==NULL"<<endl;
		this->date = new RecordingFileDateMetadataChunk();
	}

	return sizeof(int) + sizeof(dword) + date->size() + sizeof(word)*2 + sizeof(bool)*2 + sizeof(dword) + sizeof(RecordingFileHeaderChunk::recType);
}

void RMStartSessionParams::copy(const RMStartSessionParams *p)
{
	STACKTRACE_INSTRUMENT();
	this->devId=p->devId;
	if(this->date != NULL)
	{
		delete this->date;
		this->date=NULL;
	}
	
	if(p->date != NULL)
	{
		Endian::endianType currEndian=p->date->getEndian();
		void *dateCh=p->date->getData(p->date->getEndian());
	
		date=(RecordingFileDateMetadataChunk *)RecordingFileChunk::newChunk(dateCh, p->date->size(), currEndian);
		
		delete [] (byte*)dateCh;
	}
	else
		this->date = NULL;

	fpswhole=p->fpswhole;
	fpsfrac=p->fpsfrac;
	isRecoded=p->isRecoded;
	isCyclic=p->isCyclic;
	recType=p->recType;
	fourcc=p->fourcc;
}


// ***
// ***RMEndSessionParams
// ***
#pragma mark ***RMEndSessionParams
RMEndSessionParams::RMEndSessionParams() : devId(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMEndSessionParams::RMEndSessionParams(const void *b)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMEndSessionParams::~RMEndSessionParams()
{
}

void RMEndSessionParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	this->devId=*((int*)b);
	Endian::from(Endian::xarxa, &this->devId, sizeof(devId));
}

void* RMEndSessionParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMEndSessionParams "
				"serialization buffer");
	}
	
	byte *buf=data;
	
	*((dword*)buf)=devId;
	Endian::to(Endian::xarxa, buf, sizeof(devId));
	buf+=sizeof(devId);
		
	return data;
}

dword RMEndSessionParams::serializationSize()
{
	STACKTRACE_INSTRUMENT();
	return sizeof(dword);
}


// ***
// ***RMSaveFrameParams
// ***
#pragma mark ***RMSaveFrameParams
RMSaveFrameParams::RMSaveFrameParams() : chunk(NULL)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMSaveFrameParams::RMSaveFrameParams(const void *b) : chunk(NULL)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMSaveFrameParams::~RMSaveFrameParams() 
{
	STACKTRACE_INSTRUMENT();
	if (chunk!=NULL)
	{
		delete chunk;
		chunk=NULL;
	}
}

void RMSaveFrameParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();

	if (chunk!=NULL)
		delete chunk;
		
	byte *buf=(byte*)b;
	
	dword chunkSize=*((dword*)buf);
	Endian::from(Endian::xarxa, &chunkSize, sizeof(chunkSize));
	buf+=sizeof(dword);

	chunk=(RecordingFileFrameChunk *)RecordingFileChunk::newChunk(buf, chunkSize, Endian::xarxa);
}

void* RMSaveFrameParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();
	INIT_CHECKPOINT();
//			cout << endl << "saveFrameParams::toNetwork ch 1" << endl;

	byte *data=(byte*)b;
	dword allocBytes=0;
	
	try
	{
		CHECKPOINT();
		if (data==NULL)
		{
			allocBytes=this->serializationSize();
			data=new byte[allocBytes];
			if (data==NULL)
				throw NotEnoughMemoryException("Not enough memory to allocate"
					"RMSaveFrameParams "
					"serialization buffer");
		}
		
		CHECKPOINT();
		void *dataCh=chunk->getData(Endian::xarxa);

		CHECKPOINT();
		
		byte *buf=data;
	
		CHECKPOINT();
		*((dword*)buf)=chunk->size();
		Endian::to(Endian::xarxa, buf, sizeof(dword));
		buf+=sizeof(dword);
	
		CHECKPOINT();
		memmove(buf, dataCh, chunk->size());
		CHECKPOINT();
		delete [] (byte*)dataCh;
	}
	catch (std::exception &stde)
	{
		cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << stde.what() << " alloc'ing " << allocBytes << " bytes" << endl;

#ifdef WITH_DEBUG_GDB
		abort();
#endif
		
		throw stde;
	}
	return data;
}

dword RMSaveFrameParams::serializationSize()
{
	STACKTRACE_INSTRUMENT();
	return chunk->size()+sizeof(dword);
}

// ***
// ***RMReadFrameParams
// ***
#pragma mark ***RMReadFrameParams
RMReadFrameParams::RMReadFrameParams() : chunk(NULL)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMReadFrameParams::~RMReadFrameParams() 
{
	STACKTRACE_INSTRUMENT();
	if (chunk!=NULL)
		delete chunk;
}

void RMReadFrameParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	if (chunk!=NULL)
		delete chunk;
		
	byte *buf=(byte*)b;
	dword chunkSize=*((dword*)buf);
	Endian::from(Endian::xarxa, &chunkSize, sizeof(chunkSize));
	buf+=sizeof(dword);

	chunk=(RecordingFileFrameChunk *)RecordingFileChunk::newChunk(buf, chunkSize, Endian::xarxa);
}

void* RMReadFrameParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMReadFrameParams "
				"serialization buffer");
	}

	byte *buf=data;
	void *dataCh=chunk->getData(Endian::xarxa);
	
	*((dword*)buf)=chunk->size();
	Endian::to(Endian::xarxa, buf, sizeof(dword));
	buf+=sizeof(dword);
	memmove(buf, dataCh, chunk->size());
	delete [] (byte*)dataCh;
		
	return data;
}

dword RMReadFrameParams::serializationSize()
{
	STACKTRACE_INSTRUMENT();

	if (this->chunk!=NULL)
		return this->chunk->size() + sizeof(dword);
	else
		return sizeof(dword);
}



// ***
// ***RMSetTimeoutParams
// ***
#pragma mark ***RMSetTimeoutParams

RMSetTimeoutParams::RMSetTimeoutParams() : ms(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMSetTimeoutParams::RMSetTimeoutParams(const void *b) : ms(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMSetTimeoutParams::~RMSetTimeoutParams()
{
}

void RMSetTimeoutParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;
	
	this->ms=*(dword*)buf;
	Endian::from(Endian::xarxa, &ms, sizeof(dword));
	buf+=sizeof(dword);
}

void *RMSetTimeoutParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMSetTimeoutParams "
				"serialization buffer");
	}

	byte *buf=data;
	
	*((dword*)buf)=this->ms;
	Endian::to(Endian::xarxa, buf, sizeof(dword));
	buf+=sizeof(dword);
		
	return data;
}

dword RMSetTimeoutParams::serializationSize()
{
	return sizeof(dword);
}

// ***
// ***RMSkipFramesParams
// ***
#pragma mark ***RMSkipFramesParams
RMSkipFramesParams::RMSkipFramesParams() : nFrames(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMSkipFramesParams::RMSkipFramesParams(const void *b)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMSkipFramesParams::~RMSkipFramesParams()
{
}

void RMSkipFramesParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;
	
	this->nFrames=*(int*)buf;
	Endian::from(Endian::xarxa, &this->nFrames, sizeof(int));
	buf+=sizeof(int);
}

void *RMSkipFramesParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMSkipFramesParams "
				"serialization buffer");
	}

	byte *buf=data;
	
	*((int*)buf)=this->nFrames;
	Endian::to(Endian::xarxa, buf, sizeof(int));
	buf+=sizeof(int);
		
	return data;
}

dword RMSkipFramesParams::serializationSize()
{
	return sizeof(int);
}


// ***
// ***RMListRecordingsParams
// ***
#pragma mark ***RMListRecordingsParams
RMListRecordingsParams::RMListRecordingsParams() : nRecsFilter(0), nRecordings(0), recordings(NULL)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	
	absStartDate.isValid=false;
	absEndDate.isValid=false;
}

RMListRecordingsParams::RMListRecordingsParams(void *b) : nRecsFilter(0), nRecordings(0), recordings(NULL)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	
	this->toLocal(b);
}

RMListRecordingsParams::~RMListRecordingsParams()
{
	STACKTRACE_INSTRUMENT();
	if (recordings!=NULL)
	{
		delete [] recordings;
	}
}


void RMListRecordingsParams::toLocal(void *b)
{
	STACKTRACE_INSTRUMENT();
	byte *buf=(byte*)b;
	
	nRecsFilter=*((qword*)buf);
	Endian::from(Endian::xarxa, &nRecsFilter, sizeof(qword));
	buf+=sizeof(qword);

	nRecordings=*((qword*)buf);
	Endian::from(Endian::xarxa, &nRecordings, sizeof(qword));
	buf+=sizeof(qword);

	absStartDate.isValid=*((bool*)buf);
	buf+=sizeof(bool);
	
	absStartDate.secs=*((qword*)buf);
	Endian::from(Endian::xarxa, &absStartDate.secs, sizeof(qword));
	buf+=sizeof(qword);
	
	absStartDate.millis=*((word*)buf);
	Endian::from(Endian::xarxa, &absStartDate.millis, sizeof(word));
	buf+=sizeof(word);

	absEndDate.isValid=*((bool*)buf);
	buf+=sizeof(bool);
	
	absEndDate.secs=*((qword*)buf);
	Endian::from(Endian::xarxa, &absEndDate.secs, sizeof(qword));
	buf+=sizeof(qword);
	
	absEndDate.millis=*((word*)buf);
	Endian::from(Endian::xarxa, &absEndDate.millis, sizeof(word));
	buf+=sizeof(word);
	
	qword nRecs=nRecsFilter;
	if (nRecs==0)
		nRecs=nRecordings;
		
	if (recordings!=NULL)
	{
		delete [] recordings;
		recordings=NULL;
	}

	if (nRecs>0)
	{
		recordings=new recording[nRecs];
		
		for (qword i=0; i<nRecs; i++)
		{
			dword ip=*((dword*)buf);
			buf+=sizeof(dword);

			word port=*((word*)buf);
			Endian::from(Endian::xarxa, &port, sizeof(word));
			buf+=sizeof(word);

			recordings[i].rmAddress=Address(IP(ip), port);
	
			recordings[i].id=*((qword*)buf);
			Endian::from(Endian::xarxa, &recordings[i].id, sizeof(qword));
			buf+=sizeof(qword);

			recordings[i].devId=*((dword*)buf);
			Endian::from(Endian::xarxa, &recordings[i].devId, sizeof(dword));
			buf+=sizeof(dword);
			
			recordings[i].recType=*((RecordingFileHeaderChunk::recType*)buf);
			Endian::from(Endian::xarxa, &recordings[i].recType, sizeof(RecordingFileHeaderChunk::recType));
			buf+=sizeof(RecordingFileHeaderChunk::recType);
			
			recordings[i].fps.whole=*((word*)buf);
			Endian::from(Endian::xarxa, &recordings[i].fps.whole, sizeof(word));
			buf+=sizeof(word);

			recordings[i].fps.frac=*((word*)buf);
			Endian::from(Endian::xarxa, &recordings[i].fps.frac, sizeof(word));
			buf+=sizeof(word);

			recordings[i].startDate.isValid=*((bool*)buf);
			buf+=sizeof(bool);
			
			recordings[i].startDate.secs=*((qword*)buf);
			Endian::from(Endian::xarxa, &recordings[i].startDate.secs, sizeof(qword));
			buf+=sizeof(qword);
			
			recordings[i].startDate.millis=*((word*)buf);
			Endian::from(Endian::xarxa, &recordings[i].startDate.millis, sizeof(word));
			buf+=sizeof(word);

			recordings[i].endDate.isValid=*((bool*)buf);
			buf+=sizeof(bool);
			
			recordings[i].endDate.secs=*((qword*)buf);
			Endian::from(Endian::xarxa, &recordings[i].endDate.secs, sizeof(qword));
			buf+=sizeof(qword);
			
			recordings[i].endDate.millis=*((word*)buf);
			Endian::from(Endian::xarxa, &recordings[i].endDate.millis, sizeof(word));
			buf+=sizeof(word);
		}
	}
}

void *RMListRecordingsParams::toNetwork(void *data)
{
	STACKTRACE_INSTRUMENT();
	byte *b=(byte*)data;
	if (b==NULL)
	{
		b=new byte[this->serializationSize()];
		if (b==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMListRecordingsParams "
				"serialization buffer");
	}
	byte *buf=b;
	
	*((qword*)buf)=nRecsFilter;
	Endian::to(Endian::xarxa, buf, sizeof(qword));
	buf+=sizeof(qword);

	*((qword*)buf)=nRecordings;
	Endian::to(Endian::xarxa, buf, sizeof(qword));
	buf+=sizeof(qword);

	*((bool*)buf)=absStartDate.isValid;
	buf+=sizeof(bool);
	
	*((qword*)buf)=absStartDate.secs;
	Endian::to(Endian::xarxa, buf, sizeof(qword));
	buf+=sizeof(qword);
	
	*((word*)buf)=absStartDate.millis;
	Endian::to(Endian::xarxa, buf, sizeof(word));
	buf+=sizeof(word);

	*((bool*)buf)=absEndDate.isValid;
	buf+=sizeof(bool);
	
	*((qword*)buf)=absEndDate.secs;
	Endian::to(Endian::xarxa, buf, sizeof(qword));
	buf+=sizeof(qword);
	
	*((word*)buf)=absEndDate.millis;
	Endian::to(Endian::xarxa, buf, sizeof(word));
	buf+=sizeof(word);
	
	qword nRecs=nRecsFilter;
	if (nRecs==0)
		nRecs=nRecordings;
					
	for (qword i=0; i<nRecs; i++)
	{
		*((dword*)buf)=recordings[i].rmAddress.getIP().toDWord();
		buf+=sizeof(dword);

		*((word*)buf)=recordings[i].rmAddress.getPort();
		Endian::to(Endian::xarxa, buf, sizeof(word));
		buf+=sizeof(word);

		*((qword*)buf)=recordings[i].id;
		Endian::to(Endian::xarxa, buf, sizeof(qword));
		buf+=sizeof(qword);

		*((dword*)buf)=recordings[i].devId;
		Endian::to(Endian::xarxa, buf, sizeof(dword));
		buf+=sizeof(dword);
		
		*((RecordingFileHeaderChunk::recType*)buf)=recordings[i].recType;
		Endian::to(Endian::xarxa, buf, sizeof(RecordingFileHeaderChunk::recType));
		buf+=sizeof(RecordingFileHeaderChunk::recType);
		
		*((word*)buf)=recordings[i].fps.whole;
		Endian::to(Endian::xarxa, buf, sizeof(word));
		buf+=sizeof(word);
		
		*((word*)buf)=recordings[i].fps.frac;
		Endian::to(Endian::xarxa, buf, sizeof(word));
		buf+=sizeof(word);
		
		*((bool*)buf)=recordings[i].startDate.isValid;
		buf+=sizeof(bool);
		
		*((qword*)buf)=recordings[i].startDate.secs;
		Endian::to(Endian::xarxa, buf, sizeof(qword));
		buf+=sizeof(qword);
		
		*((word*)buf)=recordings[i].startDate.millis;
		Endian::to(Endian::xarxa, buf, sizeof(word));
		buf+=sizeof(word);

		*((bool*)buf)=recordings[i].endDate.isValid;
		buf+=sizeof(bool);
		
		*((qword*)buf)=recordings[i].endDate.secs;
		Endian::to(Endian::xarxa, buf, sizeof(qword));
		buf+=sizeof(qword);
		
		*((word*)buf)=recordings[i].endDate.millis;
		Endian::to(Endian::xarxa, buf, sizeof(word));
		buf+=sizeof(word);
	}
	
	return b;
}

qword RMListRecordingsParams::serializationSize()
{
	STACKTRACE_INSTRUMENT();
	qword nRecs=nRecsFilter;
	if (nRecs==0)
		nRecs=nRecordings;

	return sizeof(qword)+sizeof(qword)+
		(sizeof(bool)+sizeof(qword)+sizeof(word))*2+
		(sizeof(dword)+sizeof(word)+sizeof(qword)+sizeof(dword)+sizeof(RecordingFileHeaderChunk::recType)+sizeof(word)*2+
			2*(sizeof(bool)+sizeof(qword)+sizeof(word)))*nRecs;
}


// ***
// ***RMIsRecordingParams
// ***
#pragma mark ***RMIsRecordingParams
RMIsRecordingParams::RMIsRecordingParams()
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMIsRecordingParams::RMIsRecordingParams(void *b)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	
	this->toLocal(b);
}

RMIsRecordingParams::~RMIsRecordingParams()
{
}

void RMIsRecordingParams::toLocal(void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;

	this->isRecording=*(bool*)buf;
	buf+=sizeof(bool);

	this->isOwnSession=*(bool*)buf;
	buf+=sizeof(bool);
	
	this->fpswhole=*(word*)buf;
	Endian::from(Endian::xarxa, &this->fpswhole, sizeof(word));
	buf+=sizeof(word);

	this->fpsfrac=*(word*)buf;
	Endian::from(Endian::xarxa, &this->fpsfrac, sizeof(word));
	buf+=sizeof(word);
}

void* RMIsRecordingParams::toNetwork(void *data)
{
	STACKTRACE_INSTRUMENT();
	byte *b=(byte*)data;
	if (b==NULL)
	{
		b=new byte[this->serializationSize()];
		if (b==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMIsRecordingParams "
				"serialization buffer");
	}
	byte *buf=b;

	*((word*)buf)=this->isRecording;
	buf+=sizeof(bool);

	*((word*)buf)=this->isOwnSession;
	buf+=sizeof(bool);

	*((word*)buf)=this->fpswhole;
	Endian::to(Endian::xarxa, buf, sizeof(word));
	buf+=sizeof(word);
	
	*((word*)buf)=this->fpsfrac;
	Endian::to(Endian::xarxa, buf, sizeof(word));
	buf+=sizeof(word);
	
	return b;
}
		
dword RMIsRecordingParams::serializationSize()
{
	return 2*sizeof(bool)+2*sizeof(word);
}



// ***
// ***RMGetKiBLimitParams
// ***
#pragma mark ***RMGetKiBLimitParams
RMGetKiBLimitParams::RMGetKiBLimitParams() : KiB(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMGetKiBLimitParams::RMGetKiBLimitParams(const void *b)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMGetKiBLimitParams::~RMGetKiBLimitParams()
{
}

void RMGetKiBLimitParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;
	
	this->KiB=*(fileOffset*)buf;
	Endian::from(Endian::xarxa, &this->KiB, sizeof(fileOffset));
	buf+=sizeof(fileOffset);
}

void* RMGetKiBLimitParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *data=(byte*)b;	
	if (data==NULL)
	{
		//
		//MOD:
		//try
		//{	
			//delete data;
			data=new byte[this->serializationSize()];
			
		//}
		//catch(bad_alloc &e)
		//{			
			//*data=5;
			//printf("RMGetKiBLimitParams data pointer: %x\n", data);
			if (data==NULL){
				throw NotEnoughMemoryException("Not enough memory to allocate RMGetKiBLimitParams serialization buffer");
			}
		//}	
	}

	byte *buf=data;
	
	*((fileOffset*)buf)=this->KiB;
	Endian::to(Endian::xarxa, buf, sizeof(fileOffset));
	buf+=sizeof(fileOffset);
		
	return data;
}

dword RMGetKiBLimitParams::serializationSize()
{
	return sizeof(fileOffset);
}

// ***
// ***RMSetKiBLimitParams
// ***
#pragma mark ***RMSetKiBLimitParams
RMSetKiBLimitParams::RMSetKiBLimitParams() : KiB(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMSetKiBLimitParams::RMSetKiBLimitParams(const void *b)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMSetKiBLimitParams::~RMSetKiBLimitParams()
{
}

void RMSetKiBLimitParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;
	
	this->KiB=*(fileOffset*)buf;
	Endian::from(Endian::xarxa, &this->KiB, sizeof(fileOffset));
	buf+=sizeof(fileOffset);
}

void* RMSetKiBLimitParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMSetKiBLimitParams "
				"serialization buffer");
	}

	byte *buf=data;
	
	*((fileOffset*)buf)=this->KiB;
	Endian::to(Endian::xarxa, buf, sizeof(fileOffset));
	buf+=sizeof(fileOffset);
		
	return data;
}

dword RMSetKiBLimitParams::serializationSize()
{
	return sizeof(fileOffset);
}

// ***
// *** RMGetFrameLimitParams
// ***
#pragma mark *** RMGetFrameLimitParams
RMGetFrameLimitParams::RMGetFrameLimitParams() : nFrames(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMGetFrameLimitParams::RMGetFrameLimitParams(const void *b) : nFrames(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMGetFrameLimitParams::~RMGetFrameLimitParams()
{
}

void RMGetFrameLimitParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;
	
	this->nFrames=*(dword*)buf;
	Endian::from(Endian::xarxa, &this->nFrames, sizeof(dword));
	buf+=sizeof(dword);
}

void* RMGetFrameLimitParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMGetFrameLimitParams "
				"serialization buffer");
	}

	byte *buf=data;
	
	*((dword*)buf)=this->nFrames;
	Endian::to(Endian::xarxa, buf, sizeof(dword));
	buf+=sizeof(dword);
		
	return data;
}

dword RMGetFrameLimitParams::serializationSize()
{
	return sizeof(dword);
}

// ***
// ***RMSetFrameLimitParams
// ***
#pragma mark ***RMSetKiBLimitParams
RMSetFrameLimitParams::RMSetFrameLimitParams() : nFrames(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMSetFrameLimitParams::RMSetFrameLimitParams(const void *b)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMSetFrameLimitParams::~RMSetFrameLimitParams()
{
}

void RMSetFrameLimitParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;
	
	this->nFrames=*(fileOffset*)buf;
	Endian::from(Endian::xarxa, &this->nFrames, sizeof(fileOffset));
	buf+=sizeof(fileOffset);
}

void* RMSetFrameLimitParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMSetFrameLimitParams "
				"serialization buffer");
	}

	byte *buf=data;
	
	*((dword*)buf)=this->nFrames;
	Endian::to(Endian::xarxa, buf, sizeof(dword));
	buf+=sizeof(dword);
		
	return data;
}

dword RMSetFrameLimitParams::serializationSize()
{
	return sizeof(dword);
}

// ***
// ***RMGetCyclicParams
// ***
#pragma mark ***RMGetCyclicParams
RMGetCyclicParams::RMGetCyclicParams() : isCyclic(false)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMGetCyclicParams::RMGetCyclicParams(const void *b)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMGetCyclicParams::~RMGetCyclicParams()
{
}

void RMGetCyclicParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;
	
	this->isCyclic=*(bool*)buf;
	buf+=sizeof(bool);
}

void* RMGetCyclicParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMGetCyclicParams "
				"serialization buffer");
	}

	byte *buf=data;
	
	*((bool*)buf)=this->isCyclic;
	buf+=sizeof(bool);
		
	return data;
}

dword RMGetCyclicParams::serializationSize()
{
	return sizeof(bool);
}

// ***
// ***RMSetCyclicParams
// ***
#pragma mark ***RMSetCyclicParams
RMSetCyclicParams::RMSetCyclicParams() : isCyclic(false)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
}

RMSetCyclicParams::RMSetCyclicParams(const void *b)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::initChunks();
	this->toLocal(b);
}

RMSetCyclicParams::~RMSetCyclicParams()
{
}

void RMSetCyclicParams::toLocal(const void *b)
{
	STACKTRACE_INSTRUMENT();
	
	byte *buf=(byte*)b;
	
	this->isCyclic=*(bool*)buf;
	buf+=sizeof(bool);
}

void* RMSetCyclicParams::toNetwork(void *b)
{
	STACKTRACE_INSTRUMENT();

	byte *data=(byte*)b;
	if (data==NULL)
	{
		data=new byte[this->serializationSize()];
		if (data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				"RMSetCyclicParams "
				"serialization buffer");
	}

	byte *buf=data;
	
	*((bool*)buf)=this->isCyclic;
	buf+=sizeof(bool);
		
	return data;
}

dword RMSetCyclicParams::serializationSize()
{
	return sizeof(bool);
}


