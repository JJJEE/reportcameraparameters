#ifndef __SIRIUS__RECORDINGMODULE__RECORDINGMODULEDISKWRITERENTRY_H
#define __SIRIUS__RECORDINGMODULE__RECORDINGMODULEDISKWRITERENTRY_H

#include <Utils/Timer.h>
#include <RecordingModule/RecordingFileChunk.h>

#include <map>
#include <list>

using namespace std;

class RecordingModuleDiskWriterEntry
{
public:
	static const double maxAge;
	
protected:
	// Offset dins el fitxer
	fileOffset offset;
	// Tamany de la informacio
	dword size;
	// Chunk
	RecordingFileChunk *chunk;
	// Endarrereix l'escriptura al maxim, fins que no queden mes blocs
	// agrupables o l'edat del bloc es massa gran.
	bool delayedWrite;
	// Temps que fa que s'ha creat l'entrada
	Timer age;
	
	// Saber si hem de fer el delete del chunk quan destruim l'objecte
	bool deleteAtDestructor;

public:
	RecordingModuleDiskWriterEntry();
	RecordingModuleDiskWriterEntry(fileOffset offset, dword size,
		RecordingFileChunk *chunk, bool delayedWrite=false,
		bool deleteAtDestructor=true);
	virtual ~RecordingModuleDiskWriterEntry();

	fileOffset getOffset();
	dword getSize();
	RecordingFileChunk *getChunk();
	void setDeleteAtDestructor(bool del);
	bool isDelayed();
	double getAge();
};

#endif
