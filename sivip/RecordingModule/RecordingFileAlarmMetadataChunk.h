#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <AlarmModule/AlarmModuleTypes.h>

class RecordingFileAlarmMetadataChunk : public RecordingFileMetadataChunk
{
	dword chunkBaseSize;
	
	
public:
	list<AMAlarmValue> activeAlarms;

	RecordingFileAlarmMetadataChunk();
	virtual ~RecordingFileAlarmMetadataChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual dword size();
	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	virtual void setAlarm(AMAlarmValue av);
	virtual void endAlarm(AMAlarmId av);
	virtual bool getAlarmStatus(AMAlarmId aId);
	virtual double getAlarmValue(AMAlarmId aId);
};

