#pragma once
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Module/Module.h>
#include <ModuleAccess/RecordingModuleAccess.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <ModuleAccess/AlarmModuleAccess.h>
//#include <ControlModule/SonyControlplugin.h>
#include <Sockets/SocketTimeoutException.h>
#include <RecordingModule/RecordingModuleException.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <map>
#include <list>
#include <iostream>
#include <string>
//#include <RecordingModule/RecordingModuleInterface.h>

//using namespace std;
class RecordingModuleSchedulingThread;


class RMSServerId
{
	public:
		Address addr;
		RMSServerId(Address);
		RMSServerId(char* buf);
		void toLocal(char* buf);
		char* toNetwork(char* buf=NULL);
		int size();
};

class RecordingModuleSchedulingThreadInterface: public ModuleInterface
{
	public:
		static const int serviceCount=2;

		static const int checkRecordingServersServiceId=0;
		static const int checkSchedulesForServerServiceId=1;
	
		RecordingModuleSchedulingThread *sched;
		
		static RPCPacket* checkRecordingServers(RecordingModuleSchedulingThreadInterface *_this, Address *a, void *params);
		static RPCPacket* checkSchedulesForServer(RecordingModuleSchedulingThreadInterface *_this, Address *a, void *params);
		RecordingModuleSchedulingThreadInterface(Address moduleAddress, short type, Canis *cn, RecordingModuleSchedulingThread *sched);
};

class RecordingModuleSchedulingThread: public Module
{
	protected:
		class SchedulerThreads: public Thread
		{ 
			public:
				RecordingModuleSchedulingThread *rmst;
				class threadCall
				{
					public:
						int id;
						void* data;
						threadCall(int id, void* data):id(id), data(data){}
						threadCall():id(0), data(NULL){}
				};
				
				SchedulerThreads(RecordingModuleSchedulingThread *rmst):rmst(rmst)
				{}

				void* execute(int, void*);
		};

		SchedulerThreads *st;
		string configFile;
//		RecordingModuleInterface *rmi;

//		RecordingModuleAccess *rma;
//		AlarmModuleAccess *ama;
		ControlModuleAccess *cma;
		Mutex cmaLock, dmaLock;//, amaLock, dmaLock;
		DecodeModuleAccess *rmstDma;
		AlarmManager *am;
		DBGateway *dbGW;
//		int id;
		struct tm lastRecCheck;

		Address addr;

	public:
		bool checkLocationsPending, recStarted;

		class Hour
		{
			public:
				int hour;
				int mins;
				int sec;

				Hour();
				Hour(string desc); //hora en "HH:MM:SS"
				Hour(int h, int m, int s);
				Hour operator+ (Hour b);
				Hour operator- (Hour b);
				int waitTime(int dayMask, struct tm t);
				bool operator< (Hour b);
				bool operator== (const Hour b) const;
				string toString();
				string toContString();
				int secs();
		};
		

		class alarmData
		{
			public:
				int pre;
				float fpsPre;
				int post;
				bool stop;
				int nrefs;
				Mutex mref;
				alarmData():stop(false)
				{
					nrefs=1;
				}
				alarmData(const alarmData &ad):stop(false)
				{
					pre = ad.pre;
					fpsPre = ad.fpsPre;
					post = ad.post;
					nrefs=1;
				}
				bool operator==(const alarmData &ad) const
				{
					return pre==ad.pre && fpsPre==ad.fpsPre && post == ad.post;
				}
		};

		class Schedule
		{
			public:
				Hour start, end;
				int dayMask;
				float fps;
				int devId;
				bool active, found; //found->per temes de neteja
				int type; //0:Normal(sched) Recording, 1: monitoring, 2:alarm
				unsigned int dist;
				alarmData *alarm;
				
				Schedule();	
				Schedule(const Schedule &s);	
				~Schedule();	
//				time_t operator+ (struct timespec tm);
				bool operator==(const Schedule &s) const;
				time_t operator+ (const time_t t);
		};

		bool schedulingThreadActive;
		bool checkingThreadActive;
		Condition c;

		list<Schedule> recordings;
		RWLock recLock;



		class CamInfo:public Thread
		{
			public:
			int id;
			IP ip;
			string fabricante, modelo;
			unsigned int dist;

			RecordingModuleSchedulingThread *rmst;
			int serverError;
			unsigned long lastCall;

			bool startingRec, endingRec;
			Mutex infoMutex;
			RecordingModuleSchedulingThread::Schedule sSched;
			

			CamInfo(int id, IP ip, string fab, string mod, RecordingModuleSchedulingThread *rmst, unsigned int dist):
				infoMutex(), startingRec(false), endingRec(false), serverError(0), lastCall(0), dist(dist)
			{
				this->id=id;
				this->ip=ip;
				this->fabricante=fab;
				this->modelo=mod;
				this->rmst=rmst;
			}

			void startRecording(Schedule next)
			{
				infoMutex.lock();
				try
				{
					if(!startingRec)
					{
						startingRec=true;
						sSched=next;
						infoMutex.unlock();
						
						this->start((void*)0);
						this->detach();
					}
					else
					{
				//		cout<<" ci:"<<id<<" startRecording already starting"<<endl;
						infoMutex.unlock();
					}
				}catch(...)
				{
					if(infoMutex.isHeldByCurrentThread())
					{
						infoMutex.unlock();
					}
				}
			}

			void endRecording()
			{
				infoMutex.lock();
				try
				{
					if(!endingRec)
					{
						endingRec=true;
						infoMutex.unlock();

						this->start((void*)1);
						this->detach();
					}
					else
						infoMutex.unlock();
				}catch(...)
				{
					if(infoMutex.isHeldByCurrentThread())
						infoMutex.unlock();
				}
			}

			bool waitStarting()
			{
//				cout<<" cam::exec start lock:"<<id<<endl;
				rmst->startThreadLock.lock();
					cerr<<" Waiting for start:"<<id<<" starter:"<<rmst->starterThreads<<" max:"<<rmst->maxStarterThreads<<endl;
//				cout<<" cam::exec start locked:"<<id<<endl;
				while(rmst->starterThreads >= rmst->maxStarterThreads)
				{
//					struct timespec t;
//					t.tv_sec=time(NULL)+20; //mai mes de 10 min, por si los mutex
//					t.tv_nsec=0;
//					if(rmst->startThreadLock.wait(t))
//						break;

//						cout<<" cam::exec waiting for start :"<<id<<endl;
					rmst->startThreadLock.wait();
					if(!Module::isActive())
					{
						rmst->startThreadLock.unlock();
						cout<<" aborting StartRecordingStream due to serveShutDown()"<<endl;
						return false;
					}
				}
//				cout<<" cam::exec got starting:"<<id<<endl;
				rmst->starterThreads++;
				rmst->startThreadLock.unlock();
				return true;
			}

			void releaseStarting()
			{
				rmst->startThreadLock.lock();
				rmst->starterThreads--;
			//		cerr<<" started:"<<id<<" starterThr:"<<rmst->starterThreads<<endl;
				rmst->startThreadLock.signal();
				rmst->startThreadLock.unlock();
			}

			bool waitEnding()
			{
				rmst->endThreadLock.lock();
				while(rmst->endThreads >= rmst->maxEndThreads)
				{
//					struct timespec t;
//					t.tv_sec=time(NULL)+20;
//					t.tv_nsec=0;
					rmst->endThreadLock.wait();//(t);
//						cout<<" cam::exec waiting for end:"<<id<<endl;
					if(!Module::isActive())
					{
						cout<<" aborting StopRecordingStream due to serveShutDown()"<<endl;
						rmst->endThreadLock.unlock();
						return false;
					}
				}
				rmst->endThreads++;
				rmst->endThreadLock.unlock();
				return true;
			}

			void releaseEnding()
			{
				rmst->endThreadLock.lock();
				rmst->endThreads--;
				rmst->endThreadLock.signal();
				rmst->endThreadLock.unlock();
			}

			int waitTime()
			{
				if(serverError < 2)
					return 0; 
				else if(serverError < 8)
					return (serverError-1)*30; //60-90-120...
				else
					return 600;
			}

			void* execute(int, void* parm)
			{
				int i=(int)parm;
				string debugStr("");
				if(i==0)
				{
					if(!this->waitStarting())
					{
						debugStr = string(" CamInfo::execute:") + StrUtils::decToString(id) + string(" !waitstarting, server stopped");
						cout << debugStr << endl;
						infoMutex.lock(true);
						startingRec=false;
						infoMutex.unlock();
						return NULL;
					}
					debugStr = string(" starting:") + StrUtils::decToString(id);
					cout << debugStr << endl;
					try
					{
						rmst->startRecordingCall(sSched);

						unsigned long t= time(NULL);

						if(t < lastCall + waitTime() + 60) // si es la segona crida en menys de 5 minuts (mes el temps d'espera (max 10m))
						{
							serverError ++;						//, considerém que hi ha hagut algún problema  (no s'ha iniciat?)
							debugStr = string("CamInfo::execute startRecordingCall ") + StrUtils::decToString(id) + string(" WARNING: startRecordingCall() too close together, serverError:")+StrUtils::decToString(serverError);
							cout << debugStr << endl;
						}else
						{
							if(serverError > 0)
							{
								debugStr = string("CamInfo::execute startRecordingCall ") + StrUtils::decToString(id) + string(" startRecCall received after some time, resseting serverError from:")+StrUtils::decToString(serverError);
								cout << debugStr << endl;
							}
							serverError = 0;					//si ha passat "prou de temps" des de l'ultima crida, dirém que està tot be
						}

						lastCall = t;
						debugStr = string(" started:") + StrUtils::decToString(id)+ string(" serverError:")+StrUtils::decToString(serverError);;

						cout << debugStr << endl;
					}catch(SocketTimeoutException e)
					{
						debugStr = string("CamInfo::execute startRecordingCall ") + StrUtils::decToString(id) + string(" Timeout exception:") + e.getClass() + string(":") + e.getMsg();
						cout << debugStr << endl;
						serverError++;
					}
					//					catch(RecordingModuleException rme)
					//					{
					//						if(rme.getMsg().find(string("already being recorded")))
					//					}
					catch(Exception &e)
					{
						serverError=0;
						debugStr = string("CamInfo::execute startRecordingCall ") + StrUtils::decToString(id) + string(" exc:") + e.getClass() + string(":") + e.getMsg();
						cout << debugStr << endl;
					} catch(...)
					{
						serverError=0;
						debugStr = string("CamInfo::execute startRecordingCall ") + StrUtils::decToString(id) + string(" unexpected exc");
						cout << debugStr << endl;
					}
					this->releaseStarting();

					if(serverError> 0)
					{
						debugStr = string("CamInfo::execute startRecordingCall ") + StrUtils::decToString(id) + string(" server error, wait:") + StrUtils::decToString(waitTime()) + string(" sec.");
						cout << debugStr << endl;
						//sleep(min(serverError, 5)*60); //després del release i abans del starting=false :P
						sleep(waitTime()); //després del release i abans del starting=false :P
					}

					infoMutex.lock(true);
					startingRec=false;
					infoMutex.unlock();
				}
				else
				{
					if(!this->waitEnding())
					{
						infoMutex.lock();
						endingRec=false;
						infoMutex.unlock();
						debugStr = string(" CamInfo::execute:") + StrUtils::decToString(id) + string(" !waitstarting, server stopped");
						cout << debugStr << endl;
						return NULL;
					}
					debugStr = string(" ending:") + StrUtils::decToString(id);
					cout << debugStr << endl;
					try
					{
						rmst->endRecordingCall(id);
						debugStr = string(" ended:") + StrUtils::decToString(id);
						cout << debugStr << endl;
					}
					catch(Exception &e)
					{
						serverError=0;
						debugStr = string("CamInfo::execute endRecordingCall ") + StrUtils::decToString(id) + string(" exc:") + e.getClass() + string(":") + e.getMsg();
						cout << debugStr << endl;
					} catch(...)
					{
						serverError=0;
						debugStr = string("CamInfo::execute endRecordingCall ") + StrUtils::decToString(id) + string(" unexpected exc");
						cout << debugStr << endl;
					}
					this->releaseEnding();

					infoMutex.lock();
					endingRec=false;
					infoMutex.unlock();
				}
				return NULL;
			}
		};

		map<int, CamInfo*> cams;
		int starterThreads;
		static const int maxStarterThreads=30;
		Condition startThreadLock;

		int endThreads;
		static const int maxEndThreads=30;
		Condition endThreadLock;

		static const int maxCamsQuery=15;


		struct frameInfo
		{
			int size;
			int x;
			int y;
			int bpp;
			int quality;
			int bitrate;
			bool isKey;
			void* frame; 
		};

		bool loadSchedule(int devId, int recId, XML *config, unsigned int dist);
		IPCodecInfo getCodecForDevice(int devId);
		void setActive(bool act);

	public:
		RecordingModuleSchedulingThread(string configFile);
		~RecordingModuleSchedulingThread();

		virtual void serveStartUp();	
		virtual void serveShutDown();	

		void servePause();	
		bool waitActive();//bool waited

		void loadAllSchedules();
		void loadSchedules(int id, XML *config, unsigned int dist);

		void startRecording(Schedule next);
		void startRecordingCall(Schedule next);
		void endRecording(Schedule end);
		void endRecording(int devId);
		void endRecordingCall(int devId);
		void startSession(RecordingModuleAccess *rma, int devId);
		void startAlarmRecording(Schedule next);
		void AlarmRecording(Schedule next);
		void endAlarmRecording(Schedule end);
		void addSchedule(Schedule);
		void removeSchedules(int id);
		void checkRecordings();
		void checkRecordingLocations();

		void setRecordingModuleForDevice(string ipMask, Address ss);
		void setRecordingModuleForDevice(string ipMask, string ssId);

		void recordThread();
		void recordCheckingThread();
		void stop();
		bool recording(int id);
};

