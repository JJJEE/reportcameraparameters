/*
 *  RecordingFileHeaderChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 19/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <RecordingModule/RecordingFileSegmentMetadataChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

RecordingFileHeaderChunk::RecordingFileHeaderChunk() : verHi(0), verLo(0), 
	fpsHi(25), fpsLo(0), fourcc(0), recordingType(REC_SCHEDULED),
	firstIndexOffset(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileHeaderChunk::newChunk;
	className=getClass();
	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// La resta autoexplicatius
	chunkSize=sizeof(dword)+sizeof(byte)+className.length()+sizeof(nSubChunks)+chunkSizeCheck;
	// TODO: falta afegir subchunks "per defecte"
	RecordingFileSegmentMetadataChunk *seg=new RecordingFileSegmentMetadataChunk();
	seg->setDate(0, 0);
	seg->setFirstFrameOffset(0);
	addSubChunk(seg);
	RecordingFileSegmentMetadataChunk *seg2=new RecordingFileSegmentMetadataChunk();
	seg2->setDate(0, 0);
	seg2->setFirstFrameOffset(0);
	addSubChunk(seg2);
}

RecordingFileHeaderChunk::~RecordingFileHeaderChunk()
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileChunk* RecordingFileHeaderChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;

	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);
	
//	if (chunkClass!=getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	RecordingFileHeaderChunk *resChunk=new RecordingFileHeaderChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	// Li traiem els subchunks q pugui portar per defecte
	for (int i=resChunk->nSubChunks-1; i>=0; i--)
	{
		resChunk->delSubChunk(i); 
	}

	resChunk->chunkSize=size;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	
	chunkBytes+=chunkTypeLen;
	
	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (resChunk->nSubChunks!=0)
	{
		if (resChunk->subchunks!=NULL)
			delete [] resChunk->subchunks;
		resChunk->subchunks=new RecordingFileChunk*[resChunk->nSubChunks];
		if (resChunk->subchunks==NULL)
			throw RecordingFileChunkException(0, "Not enough memory to allocate subchunks");
		
	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
		for (dword c=0; c<resChunk->nSubChunks; c++)
		{
			dword subChunkSize=*((dword*)chunkBytes);
			Endian::from(endian, &subChunkSize, sizeof(dword));
			chunkBytes+=sizeof(dword);
			
			// cout << "subchunksize: " << subChunkSize << endl;
			
			resChunk->subchunks[c]=RecordingFileChunk::newChunk(chunkBytes, subChunkSize, endian);
			chunkBytes+=subChunkSize;
		}
	}
	
	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	if (dataSize!=chunkSizeCheck)
		throw RecordingFileChunkException(0, resChunk->getClass()+string(": Invalid data size (")+
											StrUtils::decToString(dataSize)+string("/")+
											StrUtils::decToString(chunkSizeCheck)+string(")"));
	
	resChunk->verHi=*((word*)chunkBytes);
	Endian::from(endian, &resChunk->verHi, sizeof(word));
	chunkBytes+=sizeof(word);
	
	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->verLo=*((word*)chunkBytes);
	Endian::from(endian, &resChunk->verLo, sizeof(word));
	chunkBytes+=sizeof(word);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	resChunk->fpsHi=*((word*)chunkBytes);
	Endian::from(endian, &resChunk->fpsHi, sizeof(word));
	chunkBytes+=sizeof(word);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	resChunk->fpsLo=*((word*)chunkBytes);
	Endian::from(endian, &resChunk->fpsLo, sizeof(word));
	chunkBytes+=sizeof(word);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	// cout << "RecordingFileHeaderChunk::newChunk 1" << endl;
	
	resChunk->fourcc=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->fourcc, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->recordingType=*((recType*)chunkBytes);
	Endian::from(endian, &resChunk->recordingType, sizeof(recType));
	chunkBytes+=sizeof(recType);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->firstIndexOffset=*((fileOffset*)chunkBytes);
	Endian::from(endian, &resChunk->firstIndexOffset, sizeof(fileOffset));
	chunkBytes+=sizeof(fileOffset);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, string("Invalid chunk - Out of bounds (last). Read ")+
											StrUtils::decToString(chunkBytes-chunkBytesBase)+
											string(", size ")+
											StrUtils::decToString(size));

	return (RecordingFileChunk*)resChunk;
}

void* RecordingFileHeaderChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
//	cout << getClass() << "::getData() 1" << endl;
	byte *data=new byte[chunkSize];
	byte *chunkBytes=data;
	
//	cout << getClass() << "::getData() 2" << endl;
	if (data==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
		
//	cout << getClass() << "::getData() 3" << endl;
	byte chunkTypeLen=getClass().length();
	*chunkBytes=chunkTypeLen;
	chunkBytes++;
	
//	cout << getClass() << "::getData() 4" << endl;
	memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
	chunkBytes+=chunkTypeLen;
	
//	cout << getClass() << "::getData() 5" << endl;
	*((dword*)chunkBytes)=nSubChunks;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

//	cout << getClass() << "::getData() 6-" << nSubChunks << endl;
	for (dword c=0; c<nSubChunks; c++)
	{
//		cout << getClass() << "::getData() 6." << c << endl;
		
		dword subchunkSize=subchunks[c]->size();
		*((dword*)chunkBytes)=subchunkSize;
		Endian::to(dstEndian, chunkBytes, sizeof(dword));
		chunkBytes+=sizeof(dword);

//		cout << getClass() << "::getData() 6." << c << " - 2" << endl;

		byte *subchunkData=(byte*)subchunks[c]->getData(dstEndian);
		memmove(chunkBytes, subchunkData, subchunkSize);

//		cout << getClass() << "::getData() 6." << c << " - 3" << endl;

		delete [] subchunkData;
		chunkBytes+=subchunkSize;
	}
	
//	cout << getClass() << "::getData() 7" << endl;
	*((dword*)chunkBytes)=chunkSizeCheck;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

//	cout << getClass() << "::getData() 8" << endl;
	*((word*)chunkBytes)=verHi;
	Endian::to(dstEndian, chunkBytes, sizeof(word));
	chunkBytes+=sizeof(word);
	
//	cout << getClass() << "::getData() 9" << endl;
	*((word*)chunkBytes)=verLo;
	Endian::to(dstEndian, chunkBytes, sizeof(word));
	chunkBytes+=sizeof(word);
	
//	cout << getClass() << "::getData() 10" << endl;
	*((word*)chunkBytes)=fpsHi;
	Endian::to(dstEndian, chunkBytes, sizeof(word));
	chunkBytes+=sizeof(word);
	
//	cout << getClass() << "::getData() 11" << endl;
	*((word*)chunkBytes)=fpsLo;
	Endian::to(dstEndian, chunkBytes, sizeof(word));
	chunkBytes+=sizeof(word);
	
//	cout << getClass() << "::getData() 12" << endl;
	*((dword*)chunkBytes)=fourcc;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
//	cout << getClass() << "::getData() 13" << endl;
	*((recType*)chunkBytes)=recordingType;
	Endian::to(dstEndian, chunkBytes, sizeof(recType));
	chunkBytes+=sizeof(recType);

	*((fileOffset*)chunkBytes)=firstIndexOffset;
	Endian::to(dstEndian, chunkBytes, sizeof(fileOffset));
	chunkBytes+=sizeof(fileOffset);

	return data;
}

string RecordingFileHeaderChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileHeaderChunk";
	return c;
}

void RecordingFileHeaderChunk::setVersion(word whole, word frac)
{
	STACKTRACE_INSTRUMENT();
	verHi=whole;
	verLo=frac;
}

word RecordingFileHeaderChunk::getVersionWhole()
{
	STACKTRACE_INSTRUMENT();
	return verHi;
}

word RecordingFileHeaderChunk::getVersionFrac()
{
	STACKTRACE_INSTRUMENT();
	return verLo;
}

float RecordingFileHeaderChunk::getVersion()
{
	STACKTRACE_INSTRUMENT();
	return ((float)verHi)+((float)verLo)/1000.0f;
}

void RecordingFileHeaderChunk::setFPS(word whole, word frac)
{
	STACKTRACE_INSTRUMENT();
	fpsHi=whole;
	fpsLo=frac;
}

word RecordingFileHeaderChunk::getFPSWhole()
{
	STACKTRACE_INSTRUMENT();
	return fpsHi;
}

word RecordingFileHeaderChunk::getFPSFrac()
{
	STACKTRACE_INSTRUMENT();
	return fpsLo;
}

float RecordingFileHeaderChunk::getFPS()
{
	STACKTRACE_INSTRUMENT();
	return ((float)fpsHi)+((float)fpsLo)/1000.0f;
}

void RecordingFileHeaderChunk::setFourCC(dword fourcc)
{
	STACKTRACE_INSTRUMENT();
	this->fourcc=fourcc;
}

dword RecordingFileHeaderChunk::getFourCC()
{
	STACKTRACE_INSTRUMENT();
	return fourcc;
}

void RecordingFileHeaderChunk::setRecType(recType type)
{
	STACKTRACE_INSTRUMENT();
	this->recordingType=recordingType;
}

RecordingFileHeaderChunk::recType RecordingFileHeaderChunk::getRecType()
{
	STACKTRACE_INSTRUMENT();
	return this->recordingType;
}

void RecordingFileHeaderChunk::setFirstIndexOffset(fileOffset off)
{
	STACKTRACE_INSTRUMENT();
	this->firstIndexOffset=off;
}

fileOffset RecordingFileHeaderChunk::getFirstIndexOffset()
{
	STACKTRACE_INSTRUMENT();
	return this->firstIndexOffset;
}

