/*
 *  RecordingFileException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

RecordingFileException::RecordingFileException(string msg, int code): Exception(msg, code)
{

}

RecordingFileException::RecordingFileException(int code, string msg): Exception(msg, code)
{

}

RecordingFileException::RecordingFileException(SerializedException &se): Exception(se)
{

}

RecordingFileException::~RecordingFileException()
{
   
}

string RecordingFileException::getClass()
{
	string c=string("RecordingFileException");
	return c;
}

