#include <RecordingModule/RecordingModuleSchedulingThread.h>
#include <Utils/Types.h>
#include <Utils/Canis.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <stdio.h>
#include <signal.h>

#include <signal.h>

#include <version.h>

RecordingModuleSchedulingThread *sched=NULL;
#ifndef WIN32
void sigattention(int sig)
{
	cout << "Signal: " << sig << endl;

	if (sig==SIGSEGV || sig==SIGBUS || sig==SIGINT)
	{
		NEW_DUMPALLOCATEDMEMORYMAP();
		STACKTRACE_DUMPALL();
	}
	abort();
}

void sigusr(int sig)
{
	cout << "SIGUSR:" << sig << endl;
	if(sched != NULL)
		sched->checkLocationsPending=true;
}
#endif

int main()
{
#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGINT, sigattention);
	signal(SIGSEGV, sigattention);
	signal(SIGBUS, sigattention);
#endif
#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigattention);
	signal(SIGSEGV, sigattention);
	signal(SIGBUS, sigattention);
#endif

	signal(SIGUSR1, sigusr);
	signal(SIGUSR2, sigusr);

	STACKTRACE_INSTRUMENT();

	cout << siriusRelease << endl;
	try
	{
		sched = new RecordingModuleSchedulingThread("RecordingModuleSchedulerConfig.xml");
//		sched = new RecordingModuleSchedulingThread(this);//(configFile);
	
		cout << "Starting service..." << endl;
		//sched.start(new RecordingModuleSchedulingThread::threadCall(0, NULL), Thread::RESTART);
		//sched.execute(-1, new RecordingModuleSchedulingThread::threadCall(0, NULL));
		//sched.scheduling = true;

/*		//sched.serveStartUp() ja ho inicia tot


		RecordingModuleSchedulingThread::SchedulerThreads::threadCall *c=new RecordingModuleSchedulingThread::SchedulerThreads::threadCall();
		c->id=2;
		c->data=NULL;
		sched.st->start(c, Thread::RESTART);

		//sched.recordThread();
		RecordingModuleSchedulingThread::SchedulerThreads::threadCall *c2=new RecordingModuleSchedulingThread::SchedulerThreads::threadCall();
		c2->id=0;
		c2->data=NULL;
		sched.st->start(c2, Thread::RESTART);
*/
		
		cout << "sched.serve()"<< endl;
		sched->serve();
		cout << "service stopped (?)" << endl;
	}
	catch (Exception e)
	{
		cout << "Exception ocurred during initialization: " << e.getMsg() << endl;
	}
	if(sched != NULL)
	{
		delete sched;
		sched=NULL;
	}
	return 0;
}

