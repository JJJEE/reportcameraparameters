#ifndef __SIRIUS__RECORDINGMODULE__RECORDINGFILEFRAMEINDEX_H
#define __SIRIUS__RECORDINGMODULE__RECORDINGFILEFRAMEINDEX_H

// Encapsula tots els index chunks d'un RecordingFile, de manera que puguin
// gestionar-se globalment

#include <Threads/RWlock.h>
#include <RecordingModule/RecordingFileIndexChunk.h>
#include <RecordingModule/RecordingModuleDiskWriter.h>

#include <map>
#include <list>

using namespace std;

class RecordingFileFrameIndex
{
public:
	
protected:
	// Guarda tots els index chunks
	list<RecordingFileIndexChunk*> indexChunks;
	// Referencia els index chunks per offset al fitxer
	map<fileOffset, RecordingFileIndexChunk*> indexChunksByOffset;
	// Fitxer al que correspon la col·leccio de chunks
	string file;
	// Lock per concurrencia
	RWLock indexChunksRW;
	
	// Punter al DiskWriter per poder-hi enviar coses
	RecordingModuleDiskWriter *writer;
	
	// Envia un IndexChunk al DiskWriter
	
public:
	RecordingFileFrameIndex(RecordingModuleDiskWriter *writer);
	RecordingFileFrameIndex(RecordingModuleDiskWriter *writer,
		string file);	// Per llegir-ho d'un fitxer preexistent
	virtual ~RecordingFileFrameIndex();

	// Obte l'offset dins el fitxer d'un chunk
	fileOffset getChunkOffset(RecordingFileIndexChunk *chunk);

	// Obte un chunk per offset dins el fitxer
	RecordingFileIndexChunk *getChunkByOffset(fileOffset off);
	// Obte un chunk per index dins la llista
	RecordingFileIndexChunk *getChunkByIndex(dword index);
	// Obte un chunk per index absolut de frame
	RecordingFileIndexChunk *getChunkByAbsoluteFrame(dword absFrame);
	// Obte un chunk per proximitat de data i l'index de frame mes proper
	RecordingFileIndexChunk *getChunkByNearestDate(double seconds,
		dword *nearestFrAbsIndex);
	// Crea un nou index chunk i l'enllaça
	RecordingFileIndexChunk *linkNewIndexChunk(fileOffset offset);
	// Crea un el primer chunk i l'enllaça a la capçalera. Crea la capçalera
	// si cal
	RecordingFileIndexChunk *linkFirstIndexChunk(fileOffset hdrOffset,
		dword reservedSize, dword fourcc,
		RecordingFileHeaderChunk::recType recType, word fpswhole, word fpsfrac);
	// Obte la llista de chunks
	list<RecordingFileIndexChunk*> *getIndexChunkList();
	
	dword getLastFrameIndex();
	// Obte l'offset d'un frame
	fileOffset getAbsoluteFrameOffset(dword absFrame);
	
	// Set del valor de file. Res mes.
	void setFile(string file);
	
	void wlock();
	void rlock();
	void unlock();
};

#endif
