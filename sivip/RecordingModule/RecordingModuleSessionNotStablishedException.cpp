/*
 *  RecordingModuleSessionNotStablishedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingModuleSessionNotStablishedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

RecordingModuleSessionNotStablishedException::RecordingModuleSessionNotStablishedException(string msg, int code): Exception(msg, code)
{

}

RecordingModuleSessionNotStablishedException::RecordingModuleSessionNotStablishedException(int code, string msg): Exception(msg, code)
{

}

RecordingModuleSessionNotStablishedException::RecordingModuleSessionNotStablishedException(SerializedException &se): Exception(se)
{

}

RecordingModuleSessionNotStablishedException::~RecordingModuleSessionNotStablishedException()
{
   
}

string RecordingModuleSessionNotStablishedException::getClass()
{
	string c=string("RecordingModuleSessionNotStablishedException");
	return c;
}

