
#pragma once

#include <Utils/debugNew.h>
#include <ModuleInterface/ModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <Endian/Endian.h>
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Utils/Timer.h>
#include <Utils/Watchdog.h>
#include <Utils/StrUtils.h>
#include <Threads/Mutex.h>
#include <Threads/Condition.h>
#include <Threads/RWlock.h>
//#include <RecordingModule/RecordingModuleInterface.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileException.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <RecordingModule/RecordingFileFrameChunk.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <RecordingModule/RecordingFileDateMetadataChunk.h>
#include <RecordingModule/RecordingFilePTZMetadataChunk.h>
#include <RecordingModule/RecordingFileSegmentMetadataChunk.h>
#include <RecordingModule/RecordingFileStringMetadataChunk.h>
#include <RecordingModule/RecordingFileIndexChunk.h>
#include <RecordingModule/RecordingFileFrameInfoMetadataChunk.h>
#include <RecordingModule/RecordingFileAlarmMetadataChunk.h>
#include <RecordingModule/RecordingModuleDiskWriter.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Exceptions/InvalidStateException.h>
#include <ObjectManager/ObjectManager.h>
//#include <ObjectManager/ObjectManager>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <string>
#include <map>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;


//class RecordingModuleInterface;
class RMStartSessionParams
{
	public:
		int devId;
		RecordingFileDateMetadataChunk *date;
		word fpswhole, fpsfrac;
		bool isRecoded, isCyclic;
		RecordingFileHeaderChunk::recType recType;
		dword fourcc;

		RMStartSessionParams();
		RMStartSessionParams(const void *b);
		RMStartSessionParams(const RMStartSessionParams &p);
		~RMStartSessionParams();

		void toLocal(const void *b);
		void* toNetwork(void *b=NULL);
		dword serializationSize();

	protected:		
		void copy(const RMStartSessionParams *p);
};

class RMEndSessionParams
{
	public:
		int devId;

		RMEndSessionParams();
		RMEndSessionParams(const void *b);
		~RMEndSessionParams();

		void toLocal(const void *b);
		void* toNetwork(void *b=NULL);
		dword serializationSize();

		//	protected:		
		//		void copy(const endSession *p);
};

class RMSaveFrameParams
{
	public:
		RecordingFileFrameChunk *chunk;

//		static const int SERVICE_ID=RecordingModuleInterface::saveFrameServiceId;

		RMSaveFrameParams();
		RMSaveFrameParams(const void *b);
		~RMSaveFrameParams();
		void toLocal(const void *b);
		void* toNetwork(void *b=NULL);
		dword serializationSize();
};

class RMReadFrameParams
{
	public:
		RecordingFileFrameChunk *chunk;

//		static const int SERVICE_ID=RecordingModuleInterface::readFrameServiceId;

		RMReadFrameParams();
		RMReadFrameParams(const void *b);
		~RMReadFrameParams();
		void toLocal(const void *b);
		void* toNetwork(void *b=NULL);
		dword serializationSize();
};

class RMSetTimeoutParams
{
	public:
		dword ms;

//		static const int SERVICE_ID=RecordingModuleInterface::setTimeoutServiceId;

		RMSetTimeoutParams();
		RMSetTimeoutParams(const void *b);
		~RMSetTimeoutParams();

		void toLocal(const void *b);
		void *toNetwork(void *n);
		dword serializationSize();
};

class RMSkipFramesParams
{
	public:
		int nFrames;

//		static const int SERVICE_ID=RecordingModuleInterface::skipFramesServiceId;

		RMSkipFramesParams();
		RMSkipFramesParams(const void *b);
		~RMSkipFramesParams();

		void toLocal(const void *b);
		void *toNetwork(void *n);
		dword serializationSize();
};

class RMStartRecordingStreamParams
{
	public:
//		static const int SERVICE_ID=RecordingModuleInterface::startRecordingStreamServiceId;

		RMStartRecordingStreamParams()
		{
			STACKTRACE_INSTRUMENT();
			RecordingFileChunk::initChunks();
		}

		~RMStartRecordingStreamParams();
};

class RMStopRecordingStreamParams
{
	public:
//		static const int SERVICE_ID=RecordingModuleInterface::stopRecordingStreamServiceId;

		RMStopRecordingStreamParams()
		{
			STACKTRACE_INSTRUMENT();
			RecordingFileChunk::initChunks();
		}

		~RMStopRecordingStreamParams();
};

struct RMDate
{
	bool isValid;
	qword secs;
	word millis;
};

class RMListRecordingsParams
{
	public:
		// 
		// Si recsFilter te valor!=0, es la longitud de recordings, si no, ho es nRecordings
		qword nRecsFilter;	// En input permet especificar n filtres de recordings (0=cap filtre)
		qword nRecordings;	// En input permet limitar el nombre de recordings retornats (0=no limit)

		RMDate absStartDate, absEndDate;	// En input retorna tots els que solapen

		struct recording
		{
			Address rmAddress;		// Adreça del recordingModule on esta la gravacio - forma part de l'ID!!

			qword id;	// En input permet demanar nomes un recording concret
			dword devId;		// En input permet demanar nomes els recordings d'un device
			RecordingFileHeaderChunk::recType recType;		// En input, filtra per tipus de gravacio
			struct
			{
				word whole, frac;
			} fps;

			RMDate startDate, endDate; 
		} *recordings;

//		static const int SERVICE_ID=RecordingModuleInterface::listRecordingsServiceId;

		RMListRecordingsParams();
		RMListRecordingsParams(void *b);
		~RMListRecordingsParams();


		void toLocal(void *b);
		void *toNetwork(void *data=NULL);
		qword serializationSize();
};

struct RMIsRecordingParams
{
	bool isRecording;
	bool isOwnSession;
	word fpswhole, fpsfrac;

//	static const int SERVICE_ID=RecordingModuleInterface::isRecordingServiceId;

	RMIsRecordingParams();
	RMIsRecordingParams(void *b);
	~RMIsRecordingParams();
	void toLocal(void *b);
	void *toNetwork(void *b=NULL);
	dword serializationSize();		
};

struct RMGetKiBLimitParams
{
	fileOffset KiB;

//	static const int SERVICE_ID=RecordingModuleInterface::getKiBLimitServiceId;

	RMGetKiBLimitParams();
	RMGetKiBLimitParams(const void *b);
	~RMGetKiBLimitParams();

	void toLocal(const void *b);
	void *toNetwork(void *b=NULL);
	dword serializationSize();
};

struct RMSetKiBLimitParams
{
	fileOffset KiB;

//	static const int SERVICE_ID=RecordingModuleInterface::setKiBLimitServiceId;

	RMSetKiBLimitParams();
	RMSetKiBLimitParams(const void *b);
	~RMSetKiBLimitParams();

	void toLocal(const void *b);
	void *toNetwork(void *b=NULL);
	dword serializationSize();
};

struct RMGetFrameLimitParams
{
	dword nFrames;

//	static const int SERVICE_ID=RecordingModuleInterface::getFrameLimitServiceId;

	RMGetFrameLimitParams();
	RMGetFrameLimitParams(const void *b);
	~RMGetFrameLimitParams();

	void toLocal(const void *b);
	void *toNetwork(void *b=NULL);
	dword serializationSize();
};

struct RMSetFrameLimitParams
{
	dword nFrames;

//	static const int SERVICE_ID=RecordingModuleInterface::setFrameLimitServiceId;

	RMSetFrameLimitParams();
	RMSetFrameLimitParams(const void *b);
	~RMSetFrameLimitParams();

	void toLocal(const void *b);
	void *toNetwork(void *b=NULL);
	dword serializationSize();
};

struct RMGetCyclicParams
{
	bool isCyclic;

//	static const int SERVICE_ID=RecordingModuleInterface::getCyclicServiceId;

	RMGetCyclicParams();
	RMGetCyclicParams(const void *b);
	~RMGetCyclicParams();

	void toLocal(const void *b);
	void *toNetwork(void *b=NULL);
	dword serializationSize();
};

struct RMSetCyclicParams
{
	bool isCyclic;

//	static const int SERVICE_ID=RecordingModuleInterface::setCyclicServiceId;

	RMSetCyclicParams();
	RMSetCyclicParams(const void *b);
	~RMSetCyclicParams();

	void toLocal(const void *b);
	void *toNetwork(void *b=NULL);
	dword serializationSize();
};

