/*
 *  RecordingFilePTZMetadataChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>

class RecordingFilePTZMetadataChunk : public RecordingFileMetadataChunk
{
	static const dword chunkSizeCheck=sizeof(float)*3;
	
	float x, y, z;
	
public:
	RecordingFilePTZMetadataChunk();
	virtual ~RecordingFilePTZMetadataChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	virtual void setPTZ(float x, float y, float z);
	virtual float getP();
	virtual float getT();
	virtual float getZ();
};

