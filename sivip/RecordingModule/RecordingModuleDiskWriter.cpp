#include <RecordingModule/RecordingModuleDiskWriter.h>
#include <RecordingModule/RecordingFileFrameIndices.h>
#include <RecordingModule/RecordingModuleInterface.h>
#include <Utils/Timer.h>
#include <Utils/StrUtils.h>
#include <Threads/Thread.h>

#pragma mark *** RecordingModuleDiskWriter ---------------------------

#pragma mark *** Constructores

RecordingModuleDiskWriter::RecordingModuleDiskWriter(Watchdog *notify) : running(false), notify(notify),
	flush(false), runningId(-1)
{
	entryListsRW= new RWLock();
	STACKTRACE_INSTRUMENT();
}

RecordingModuleDiskWriter::~RecordingModuleDiskWriter()
{
	STACKTRACE_INSTRUMENT();
	this->wlock();
	try
	{
		map<string, RecordingModuleDiskWriterEntryGrouper*>::iterator eIt;
		for (eIt=this->entryLists.begin(); eIt!=this->entryLists.end(); eIt++)
		{
			if (eIt->second!=NULL)
				delete eIt->second;

			this->entryLists.erase(eIt);
		}

		// TODO 20090805: mes neteja
	}
	catch(...)
	{
	//	this->unlock();
	//	throw;
	}
	
	this->unlock();
	delete entryListsRW;
}

#pragma mark *** Metodes

RecordingFileHeaderChunk *RecordingModuleDiskWriter::getHeaderForFile(
	string file)
{
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	STACKTRACE_INSTRUMENT();
	RecordingFileHeaderChunk *rfhc=NULL;
	
	this->rlock();
	try
	{
		map<string, RecordingFileHeaderChunk*>::iterator hIt =
			this->headers.find(file);

		if (hIt!=this->headers.end())
			rfhc=hIt->second;

	}
	catch(std::exception &stde)
	{
		cout<<" RecordingModuleDiskWriter std::exception in getHeaderForFile("<<__LINE__<< ") "<< stde.what() << endl;
		this->unlock();
		throw; //les tirem totes cap amunt (fins a RecModInt)
	}
	catch(...)
	{
		cout<<" RecordingModuleDiskWriter unknown exception in getHeaderForFile("<<__LINE__<< ")" << endl;
		this->unlock();
		throw;
	}
	this->unlock();
	
	return rfhc;
}

RecordingFileHeaderChunk *RecordingModuleDiskWriter::createHeaderForFile(
	string file, fileOffset off, dword reservedSize, dword fourcc,
	RecordingFileHeaderChunk::recType recType, word fpswhole, word fpsfrac)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileHeaderChunk *rfhc=NULL;

	this->wlock();

	try
	{
		// Ens assegurem que no hi es
		map<string, RecordingFileHeaderChunk*>::iterator hIt =
			this->headers.find(file);

		if (hIt!=this->headers.end())
			rfhc=hIt->second;
		else
		{
			rfhc=new RecordingFileHeaderChunk();
			rfhc->setFourCC(fourcc);
			rfhc->setVersion(0,100);
			rfhc->setRecType(recType);
			rfhc->setFPS(fpswhole, fpsfrac);

			// Eliminem subchunks de segment, per estar DEPRECATED
			rfhc->delSubChunk((dword)0);
			rfhc->delSubChunk((dword)0);

			rfhc->setFirstIndexOffset(off + reservedSize);

			this->headers[file] = rfhc;
		}

	}catch(Exception &e)
	{
		cout<<" RecordingModuleDiskWriter Exception in setHeaderForFile("<<__LINE__<< ") "<<e.getClass()<<"::"<<e.getMsg() << endl;
		this->unlock();
		throw;
	}
	catch(std::exception &stde)
	{
		cout<<" RecordingModuleDiskWriter std::exception in setHeaderForFile("<<__LINE__<< ") "<< stde.what() << endl;
		this->unlock();
		throw;
	}
	catch(...)
	{
		cout<<" RecordingModuleDiskWriter unknown exception in setHeaderForFile("<<__LINE__<< ")" << endl;
		this->unlock();
		throw;
	}
	this->unlock();
	
	return rfhc;
}

RecordingFile *RecordingModuleDiskWriter::getRecordingFile(string file, bool readOnly)
{
	STACKTRACE_INSTRUMENT();

	// Primer de tot evitem que s'elimini :)
	this->resetCountEmpty(file);

	// 20110629: És absurd fer un wlock per fer només una cerca el 99,99% dels
	// cops :)
//	this->wlock();
	this->rlock();
	
	RecordingFile *rf=NULL;
	try
	{
		map<string, RecordingFile*>::iterator rfIt =
			this->recordingFiles.find(file);

		bool newFile=false;
		if (rfIt==this->recordingFiles.end())
		{
			// 20110629: Si no l'hem trobat, promocionem i recheck
			this->unlock();
			this->wlock();

			rfIt = this->recordingFiles.find(file);
			if (rfIt==this->recordingFiles.end())
			{
				// Provem a obrir-lo => Si ja existeix, append.
				try
				{
					rf=new RecordingFile(file, false, newFile);
				}
				catch (Exception &re)
				{
					if (readOnly)
					{
			//			this->unlock();
						//re.serialize()->materializeAndThrow(true);
						throw;
					}
	
					// Si no obre, es que no existeix, el creem NOU
					try
					{
						newFile=true;
						rf=new RecordingFile(file, false, newFile);
					}
					catch (Exception &re)
					{
						newFile=false;
			//			this->unlock();
						//re.serialize()->materializeAndThrow(true);
						throw;
					}
				}
	
				this->recordingFiles[file] = rf;
			}
			else
				rf = rfIt->second;			
		}
		else
			rf = rfIt->second;
	}
	catch(std::exception &stde)
	{
		cout<<" RecordingModuleDiskWriter std::exception in getRecordingFile("<<__LINE__<< ") "<< stde.what() << endl;
		this->unlock();
		throw;
	}
	catch(...)
	{
		this->unlock();
		throw;
	}
	
	this->unlock();

	return rf;
}

list<string> RecordingModuleDiskWriter::getAvailableFiles()
{
	STACKTRACE_INSTRUMENT();
	list<string> files;

	this->rlock();
	try
	{
		map<string, RecordingModuleDiskWriterEntryGrouper*>::iterator elIt;

		for (elIt=this->entryLists.begin(); elIt!=this->entryLists.end(); elIt++)
		{
			files.push_back(elIt->first);
		}
	}
	catch(std::exception &stde)
	{
		cout<<" RecordingModuleDiskWriter std::exception in getAvailableFiles("<<__LINE__<< ") "<< stde.what() << endl;
		this->unlock();
		throw;
	}
	catch(...)
	{
		cout<<" RecordingModuleDiskWriter unknown exception in getAvailableFiles("<<__LINE__<< ")" << endl;
		this->unlock();
		throw;
	}
	this->unlock();

	return files;
}

list<string> RecordingModuleDiskWriter::getOpenFiles(list<string> exclude)
{
	STACKTRACE_INSTRUMENT();
	map <string, bool> excludeFiles;
	list<string>::iterator exclIt;
	for (exclIt=exclude.begin(); exclIt!=exclude.end(); exclIt++)
		excludeFiles[(*exclIt)]=true;

	list<string> files;

	this->rlock();
	try
	{
		map<string, RecordingFile*>::iterator elIt;

		for (elIt=this->recordingFiles.begin(); elIt!=this->recordingFiles.end(); elIt++)
		{
			map<string, bool>::iterator excluded=excludeFiles.find(elIt->first);
			if (excluded==excludeFiles.end())
				files.push_back(elIt->first);
		}
	}
	catch(std::exception &stde)
	{
		cout<<" RecordingModuleDiskWriter std::exception in getOpenFiles ("<<__LINE__<< ") "<< stde.what() << endl;
		this->unlock();
		throw;
	}
	catch(...)
	{
		cout<<" RecordingModuleDiskWriter unknown exception in getOpenFiles ("<<__LINE__<< ")" << endl;
		this->unlock();
		throw;
	}
	this->unlock();

	return files;
}

RecordingModuleDiskWriterEntryGrouper*
	RecordingModuleDiskWriter::getEntriesForFile(string file)
{
	STACKTRACE_INSTRUMENT();
	RecordingModuleDiskWriterEntryGrouper *entries=NULL;

	this->rlock();
	try
	{
		map<string, RecordingModuleDiskWriterEntryGrouper*>::iterator elIt =
			this->entryLists.find(file);
		if (elIt!=this->entryLists.end())
			entries=elIt->second;
	}
	catch(std::exception &stde)
	{
		cout<<" RecordingModuleDiskWriter std::exception in getEntriesForFile("<<__LINE__<< ") "<< stde.what() << endl;
		this->unlock();
		throw;
	}
	catch(...)
	{
		cout<<" RecordingModuleDiskWriter unknown exception in getEntriesForFile("<<__LINE__<< ") "<< endl;
		this->unlock();
		throw;
	}
	this->unlock();

	return entries;
}

float RecordingModuleDiskWriter::addEntryToFile(string file,
	RecordingModuleDiskWriterEntry *entry)
{
	STACKTRACE_INSTRUMENT();
	// Si no estem gravant, passem olimpicament
	if (!this->running)
	{
		delete entry;
		return 0.0f;
	}
	
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif

	// Podem fer locks independents, perque no protegim el cas de més
	// d'un thread afegint entrades al mateix fitxer, ja que no te sentit
	// des del punt de vista de la classe
	RecordingModuleDiskWriterEntryGrouper *entries=this->getEntriesForFile(file);
	
	if (entries==NULL)
	{
		entries = new RecordingModuleDiskWriterEntryGrouper();
		this->wlock();
		try
		{
			this->entryLists[file] = entries;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
			this->unlock();
		}
		catch (...)
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
			this->unlock();
			throw;
		}
	}
	
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	return entries->addEntry(entry);
}

void RecordingModuleDiskWriter::wlock()
{
	STACKTRACE_INSTRUMENT();
	this->entryListsRW->wlock();
}

void RecordingModuleDiskWriter::rlock()
{
	STACKTRACE_INSTRUMENT();
	this->entryListsRW->rlock();
}

void RecordingModuleDiskWriter::unlock()
{
	STACKTRACE_INSTRUMENT();
	this->entryListsRW->unlock();
}

void RecordingModuleDiskWriter::startRunning()
{
	STACKTRACE_INSTRUMENT();
	if (this->runningId!=-1)
		return;

	this->running=true;
	this->flush=false;
	this->runningId=this->start();
}

void RecordingModuleDiskWriter::stopRunningAndFlush()
{
	STACKTRACE_INSTRUMENT();
	this->flush=true;
	this->join(this->runningId);
	this->runningId=-1;
}

void *RecordingModuleDiskWriter::execute(int id, void *args)
{
	STACKTRACE_INSTRUMENT();
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
	while (this->running)
	{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		Timer timer;
		timer.start();
		
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		list<string> files = this->getAvailableFiles();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		list<string>::iterator fIt;
		
		for (fIt=files.begin(); fIt!=files.end(); fIt++)
		{
			string file=*fIt;
			try
			{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
				RecordingModuleDiskWriterEntryGrouper *eList = 
					this->getEntriesForFile(file);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif

				if (eList==NULL || eList->empty())
				{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
					int counter=this->countEmpty(file);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif

					if (counter > RecordingModuleDiskWriter::maxEmptyCount)
					{
						cout << "\n\tFile: " << file << " empty. [" << counter <<"]";
						cout.flush();

						// Esta buit durant maxEmptyCount iteracions, l'eliminem
						this->cleanupFile(file);
					}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
					continue;
				}
				//			else			
				//			{
				//				cout << "\n\t[" << Thread::getId() << "] File: " << file << " eList: " << eList->size();
				//				cout.flush();
				//			}

				// Tenim grups de blocs. Mirem quins estan llestos per ser escrits
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
				list<RecordingModuleDiskWriterEntryGroup*> readyGroups =
					eList->getReadyGroups();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif

				if (readyGroups.empty())
				{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
					int counter=this->countEmpty(file);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif

					//				cout << "\n\tFile: " << file << " readyGroups empty. [" << counter << "]";
					//				cout.flush();

					if (counter > RecordingModuleDiskWriter::maxEmptyCount)
					{
						cout << "\n\tFile: " << file << " readyGroups empty. [" << counter << "]";
						cout.flush();
						// Esta buit durant maxEmptyCount iteracions, l'eliminem
						this->cleanupFile(file);
					}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
					continue;
				}

				//			cout << "\n\tFile: " << file << " readyGroups: " << readyGroups.size();
				//			cout.flush();
				//				
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
				this->resetCountEmpty(file);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
				// Escrivim
				try
				{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
					this->writeGroups(file, readyGroups);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
				}
				catch (Exception &re)
				{
					cout << "RecordingModuleDiskWriter: could not write groups "
						"for " << file << ": " << re.getClass() << ": "
						<< re.getMsg() << endl;

					// Buidatge dels readyGroups que no hem pogut gravar
					list<RecordingModuleDiskWriterEntryGroup*>::iterator
						rIt=readyGroups.begin();
					while (rIt!=readyGroups.end())
					{
						RecordingModuleDiskWriterEntryGroup *grp=*rIt;
						rIt=readyGroups.erase(rIt);
						delete grp;
					}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
					continue;
				}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
			}
			catch(Exception &e)
			{
				cout<<" RecordingModuleDiskWriter Exception in ::execute("<<__LINE__<< ") while processing file: " << file << " : "<<e.getClass()<<"::"<<e.getMsg() << endl;
			}
			catch(std::exception &stde)
			{
				cout<<" RecordingModuleDiskWriter std::exception in ::execute("<<__LINE__<< ") while processing file: " << file << " :  "<< stde.what() << endl;
			}
			catch(...)
			{
				cout<<" RecordingModuleDiskWriter unknown exception in ::execute("<<__LINE__<< ") while processing file: " << file << endl;
			}
		}

		// Comprovem els fitxers oberts, a veure si cal tancar-los, excloent
		// els que acabem de mirar...
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		list<string> openFiles = this->getOpenFiles(files);
		
		for (fIt=openFiles.begin(); fIt!=openFiles.end(); fIt++)
		{
			string file=*fIt;
			try
			{
				RecordingModuleDiskWriterEntryGrouper *eList = 
					this->getEntriesForFile(file);

				if (eList==NULL || eList->empty())
				{
					int counter=this->countEmpty(file);

					if (counter > RecordingModuleDiskWriter::maxEmptyCount)
					{
						cout << "\n\tFile: " << file << " empty. [" << counter <<"] - R";
						cout.flush();

						// Esta buit durant maxEmptyCount iteracions, l'eliminem
						this->cleanupFile(file);
					}
				}
			}
			catch(Exception &e)
			{
				cout<<" RecordingModuleDiskWriter Exception in ::execute("<<__LINE__<< ") while processing open file: " << file << " : "<<e.getClass()<<"::"<<e.getMsg() << endl;
			}
			catch(std::exception &stde)
			{
				cout<<" RecordingModuleDiskWriter std::exception in ::execute("<<__LINE__<< ") while processing open file: " << file << " :  "<< stde.what() << endl;
			}
			catch(...)
			{
				cout<<" RecordingModuleDiskWriter unknown exception in ::execute("<<__LINE__<< ") while processing open file: " << file << endl;
			}
		}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		if (this->notify!=NULL)
			this->notify->resetTimer();

		if (this->flush)
		{
			// Marquem primer perque no ens afegeixin mes.
			this->running=false;
			try
			{
				this->flushAllGroups();
			}catch(Exception &e)
			{
				cout<<" RecordingModuleDiskWriter Exception in ::execute.flushAllGroups("<<__LINE__<< ") "<<e.getClass()<<"::"<<e.getMsg() << endl;
			}
			catch(std::exception &stde)
			{
				cout<<" RecordingModuleDiskWriter std::exception in ::execute.flushAllGroups("<<__LINE__<< ") "<< stde.what() << endl;
			}
			catch(...)
			{
				cout<<" RecordingModuleDiskWriter unknown exception in ::execute.flushAllGroups("<<__LINE__<< ")" << endl;
			}
		}
		
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		// No val la pena que ens executem mes de 10 cops/s
		TimerInstant ti=timer.time();
		double waitSecs = 1.0 / ((double)RecordingModuleDiskWriter::writesPerSecond);
		double waitTime = waitSecs - ti.seconds();

//		cout << "RecordingModuleDiskWriter: waitTime: " << waitTime << "s"
//			<< endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		if (waitTime < 0.01)
			cout << "RecordingModuleDiskWriter: WARNING: waitTime<0.01 "
				<< waitTime << "s, runTime: " << ti.seconds() << "s" << endl;

		if (waitTime>0.000001)
			usleep((int)(waitTime*1000000.0));
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	}
	
	cout << "RecordingModuleDiskWriter: !running. Stopping..." << endl;
	
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	return NULL;
}

int RecordingModuleDiskWriter::countEmpty(string file)
{
	STACKTRACE_INSTRUMENT();
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	this->wlock();
	int count=1;
	try
	{
		map<string, int>::iterator eIt=this->entryListEmptyCounters.find(file);


		if (eIt!=this->entryListEmptyCounters.end())
			count=eIt->second + 1;

		this->entryListEmptyCounters[file]=count;
	}
	catch(std::exception &stde)
	{
		cout<<" RecordingModuleDiskWriter std::exception in countEmpty("<<__LINE__<< ") "<< stde.what() << endl;
		this->unlock();
		throw;
	}
	catch(...)
	{
		cout<<" RecordingModuleDiskWriter unknown exception in countEmpty("<<__LINE__<< ")" << endl;
		this->unlock();
		throw;
	}
	this->unlock();
		
	return count;
}

void RecordingModuleDiskWriter::setCountEmpty(string file, int count)
{
	STACKTRACE_INSTRUMENT();
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	this->wlock();
	try
	{
		this->entryListEmptyCounters[file]=count;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		this->unlock();
	}
	catch (...)
	{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		this->unlock();
		throw;
	}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
}

void RecordingModuleDiskWriter::resetCountEmpty(string file)
{
	STACKTRACE_INSTRUMENT();
	this->setCountEmpty(file, 0);
}

void RecordingModuleDiskWriter::cleanupFile(string file)
{
	STACKTRACE_INSTRUMENT();
	this->flushFile(file);

#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	this->wlock();
	try
	{
		// Si no hem arribat al maxim de cops que podem tenir el fitxer sense
		// escriptures, no fem res...
		if (this->unsafeGetCountEmpty(file) < 
				RecordingModuleDiskWriter::maxEmptyCount)
		{
			this->unlock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
			return;
		}

		map<string, RecordingModuleDiskWriterEntryGrouper*>::iterator eIt=
			this->entryLists.find(file);
		if (eIt!=this->entryLists.end())
		{
			RecordingModuleDiskWriterEntryGrouper *grpr = eIt->second;
			this->entryLists.erase(eIt);

			if (grpr!=NULL)
				delete grpr;
		}

		map<string, RecordingFile*>::iterator rfIt=
			this->recordingFiles.find(file);
		if (rfIt!=this->recordingFiles.end())
		{
			RecordingFile *rf = rfIt->second;
			this->recordingFiles.erase(rfIt);

			if (rf!=NULL)
				delete rf;
		}

		map<string, RecordingFileHeaderChunk*>::iterator hIt=
			this->headers.find(file);
		if (hIt!=this->headers.end())
		{
			RecordingFileHeaderChunk *header = hIt->second;
			this->headers.erase(hIt);

			if (header!=NULL)
				delete header;
		}

		map<string, int>::iterator ecIt=this->entryListEmptyCounters.find(file);
		if (ecIt!=this->entryListEmptyCounters.end())
		{
			this->entryListEmptyCounters.erase(ecIt);
		}
	}catch(Exception &e)
	{
		cout<<" RecordingModuleDiskWriter Exception in cleanupFile("<<__LINE__<< ") "<<e.getClass()<<"::"<<e.getMsg() << endl;
		this->unlock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		throw;
	}
	catch(std::exception &stde)
	{
		cout<<" RecordingModuleDiskWriter std::exception in cleanupFile("<<__LINE__<< ") "<< stde.what() << endl;
		this->unlock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		throw;
	}
	catch(...)
	{
		cout<<" RecordingModuleDiskWriter unknown exception in cleanupFile("<<__LINE__<< ") " << endl;
		this->unlock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		throw;
	}
	
	this->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	RecordingFileFrameIndices::forgetIndexForFile(file);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
}

void RecordingModuleDiskWriter::writeGroups(string file, 
	list<RecordingModuleDiskWriterEntryGroup*> &groups)
{
	STACKTRACE_INSTRUMENT();

//	FIX 20091214: En principi no cal, entre d'altres perquè el punt que
//	realment esborra el fitxer està fora de lock. Per altra banda, existeix
//	la possibilitat que un deadlock que apareix dins getRecordingFile vingui
//	per algun desordre de locking causat per estar fent en lock aqui...
//	RecordingModuleInterface::limitsThreadLock();

#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif

	RecordingFile *rf=NULL;
	try
	{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		rf=this->getRecordingFile(file);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	}
	catch (Exception &re)
	{
//		FIX 20091214: Veure mes amunt
//		RecordingModuleInterface::limitsThreadUnlock();
		//re.serialize()->materializeAndThrow(true);
		throw;
	}

//	FIX 20091214: No cal el bloc try-catch (veure més avall)
//	try
//	{
		list<RecordingModuleDiskWriterEntryGroup*>::iterator rIt=groups.begin();
		while (rIt!=groups.end())
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
			RecordingModuleDiskWriterEntryGroup *grp=*rIt;
			void *chunkData = NULL;
			try
			{
				chunkData = grp->getData();
				dword chunkDataLen = grp->getDataLength();
				fileOffset chunkOff = grp->getOffset();
				
				if (chunkData!=NULL)
				{
	//				string msg=string("\n-----------\nwriteGroups: ") +
	//					StrUtils::decToString(chunkDataLen) + string(" bytes @ ") +
	//					StrUtils::decToString(chunkOff) + string("\n-----------\n");
	//				cout << msg;
	//				cout.flush();
					
					cout << ">";
					cout.flush();
				
					// Indiquem que no afegeixi el tamany, ja ens ve del grup :P
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
					rf->writeBlock(chunkData, chunkDataLen, chunkOff, false, false);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
					delete [] (byte*)chunkData;
				}
				chunkData=NULL;
			}
			catch (Exception &e)
			{
				if (chunkData!=NULL)
					delete [] (byte*)chunkData;
				chunkData=NULL;
			}
			rIt=groups.erase(rIt);
			delete grp;
			if (this->notify!=NULL)
				this->notify->resetTimer();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		}
		
//		FIX 20091214: Veure mes amunt
//		RecordingModuleInterface::limitsThreadUnlock();

//		FIX 20091214: No cal fer el catch si era nomes per l'unlock
//		}
//		catch (Exception &e)
//		{
//	//		FIX 20091214: Veure mes amunt
//	//		RecordingModuleInterface::limitsThreadUnlock();
//			e.serialize()->materializeAndThrow(true);
//		}
//		catch (std::exception &stde)
//		{
//	//		FIX 20091214: Veure mes amunt
//	//		RecordingModuleInterface::limitsThreadUnlock();
//			throw stde;
//		}
}

void RecordingModuleDiskWriter::flushFile(string file)
{
	STACKTRACE_INSTRUMENT();
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	RecordingModuleDiskWriterEntryGrouper *eList = 
		this->getEntriesForFile(file);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	if (eList==NULL || eList->empty())
		return;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	list<RecordingModuleDiskWriterEntryGroup*> readyGroups =
		eList->getAllGroups();

	try
	{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
		this->writeGroups(file, readyGroups);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	}
	catch (Exception &re)
	{
		cout << "RecordingModuleDiskWriter: could not create "
			<< file << ": " << re.getClass() << ": "
			<< re.getMsg() << endl;
	}
	catch (...)
	{
		cout << "RecordingModuleDiskWriter: could not create "
			<< file << ": Unknown exception" << endl;
	}

	// Buidatge dels readyGroups que no hem pogut gravar
	list<RecordingModuleDiskWriterEntryGroup*>::iterator
		rIt=readyGroups.begin();
	while (rIt!=readyGroups.end())
	{
		RecordingModuleDiskWriterEntryGroup *grp=*rIt;
		rIt=readyGroups.erase(rIt);
		delete grp;
	}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
}

void RecordingModuleDiskWriter::flushAllGroups()
{
	STACKTRACE_INSTRUMENT();
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	list<string> files = this->getAvailableFiles();
	list<string>::iterator fIt;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
	for (fIt=files.begin(); fIt!=files.end(); fIt++)
	{
		string file=*fIt;

		try
		{
			if (this->notify!=NULL)
				this->notify->resetTimer();

			this->flushFile(file);		
		}catch(Exception &e)
		{
			cout<<" RecordingModuleDiskWriter Exception in flushAllGroups("<<__LINE__<< ") while processing file: " << file << " : "<<e.getClass()<<"::"<<e.getMsg() << endl;
		}
		catch(std::exception &stde)
		{
			cout<<" RecordingModuleDiskWriter std::exception in flushAllGroups("<<__LINE__<< ") while processing file: " << file << " :  "<< stde.what() << endl;
		}
		catch(...)
		{
			cout<<" RecordingModuleDiskWriter unknown exception in flushAllGroups("<<__LINE__<< ") while processing file: " << file << endl;
		}
	}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x10000000 | __LINE__;
#endif
}

float RecordingModuleDiskWriter::calcFullFactorForFile(string file)
{
	STACKTRACE_INSTRUMENT();
	// Si no estem gravant, passem olimpicament
	if (!this->running)
		return 0.0f;
	
	// Podem fer locks independents, perque no protegim el cas de més
	// d'un thread afegint entrades al mateix fitxer, ja que no te sentit
	// des del punt de vista de la classe
	RecordingModuleDiskWriterEntryGrouper *entries=this->getEntriesForFile(file);
	
	if (entries==NULL)
		return 0.0f;
	
	return entries->calcFullFactor();
}

int RecordingModuleDiskWriter::unsafeGetCountEmpty(string file)
{
	STACKTRACE_INSTRUMENT();

	map<string, int>::iterator eIt=this->entryListEmptyCounters.find(file);
	
	int count=0;
	
	if (eIt!=this->entryListEmptyCounters.end())
		count=eIt->second;

	return count;
}

