/*
 *  RecordingFilePTZMetadataChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFilePTZMetadataChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

RecordingFilePTZMetadataChunk::RecordingFilePTZMetadataChunk() : x(0.0f), y(0.0f), z(0.0f)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFilePTZMetadataChunk::newChunk;
	className=getClass();
	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// La resta autoexplicatius (o no, pero poc important si ho son o no)
	chunkSize=sizeof(dword)+sizeof(byte)+className.length()+sizeof(nSubChunks)+chunkSizeCheck;
}

RecordingFilePTZMetadataChunk::~RecordingFilePTZMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileChunk* RecordingFilePTZMetadataChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);
	
//	if (chunkClass!=getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	RecordingFilePTZMetadataChunk *resChunk=new RecordingFilePTZMetadataChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

	resChunk->chunkSize=size;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	
	chunkBytes+=chunkTypeLen;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (resChunk->nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
		
	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (dataSize!=chunkSizeCheck)
		throw RecordingFileChunkException(0, resChunk->getClass()+string(": Invalid data size (")+
											StrUtils::decToString(dataSize)+string("/")+
											StrUtils::decToString(chunkSizeCheck)+string(")"));
	
	resChunk->x=*((float*)chunkBytes);
	Endian::from(endian, &resChunk->x, sizeof(float));
	chunkBytes+=sizeof(float);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->y=*((float*)chunkBytes);
	Endian::from(endian, &resChunk->y, sizeof(float));
	chunkBytes+=sizeof(float);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	resChunk->z=*((float*)chunkBytes);
	Endian::from(endian, &resChunk->z, sizeof(float));
	chunkBytes+=sizeof(float);
		
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	return (RecordingFileChunk*)resChunk;
}

void* RecordingFilePTZMetadataChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	byte *data=new byte[chunkSize];
	byte *chunkBytes=data;
	
	if (data==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
		
	byte chunkTypeLen=getClass().length();
	*chunkBytes=chunkTypeLen;
	chunkBytes++;
	
	memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
	chunkBytes+=chunkTypeLen;
	
	if (nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
	*((dword*)chunkBytes)=nSubChunks;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	*((dword*)chunkBytes)=chunkSizeCheck;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((float*)chunkBytes)=x;
	Endian::to(dstEndian, chunkBytes, sizeof(float));
	chunkBytes+=sizeof(float);
	
	*((float*)chunkBytes)=y;
	Endian::to(dstEndian, chunkBytes, sizeof(float));
	chunkBytes+=sizeof(float);
	
	*((float*)chunkBytes)=z;
	Endian::to(dstEndian, chunkBytes, sizeof(float));

	return data;
}

string RecordingFilePTZMetadataChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFilePTZMetadataChunk";
	return c;
}

void RecordingFilePTZMetadataChunk::setPTZ(float x, float y, float z)
{
	STACKTRACE_INSTRUMENT();
	this->x=x;
	this->y=y;
	this->z=z;
}

float RecordingFilePTZMetadataChunk::getP()
{
	STACKTRACE_INSTRUMENT();
	return x;
}

float RecordingFilePTZMetadataChunk::getT()
{
	STACKTRACE_INSTRUMENT();
	return y;
}

float RecordingFilePTZMetadataChunk::getZ()
{
	STACKTRACE_INSTRUMENT();
	return z;
}

