/*
 *  RecordingFile.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/CachedFile.h>
#include <Utils/Types.h>
#include <Endian/Endian.h>
#include <Utils/debugNew.h>
#include <stdio.h>
#include <sys/types.h>

class RecordingFile 
{
public:
	// Capçalera del fitxer, en local endian
	// En Big endian sera: (aapl leet)
	// aa411ee7
	static const dword fileId=0xaa411ee7;
	
	static const dword prefetchInfoSize=5;
	
protected:
	Endian::endianType fileEndian;
	dword currentBlock;
	dword lastBlocksRead[prefetchInfoSize];
	
	// Cache obtinguda del CacheManager
	Cache *cache;
	File *cf;

//	virtual int read(void *data, dword size=1, bool needEndian=false);
//	virtual int write(void *data, dword size=1, bool needEndian=false);
//	virtual int uncachedwrite(void *data, dword size=1, bool needEndian=false);
//	// Per casos xungus de deteccio de tipus, ja que fileOffset es 64bit
//	virtual void seek(int off, int from=File::SEEK_FROMSTART);
//	virtual void seek(fileOffset off, int from=File::SEEK_FROMSTART);	
	
//	virtual void prefetch(int distance, int nBlocks=1);
	
public:
	RecordingFile(const string filename, const bool readOnly=true, const bool newFileRW=false);
	~RecordingFile();

	virtual void* readBlock(dword *size);
	virtual void* readBlock(dword *size, fileOffset off, bool stay=false);
	virtual void writeBlock(void *block, dword size, bool addLength=true);
	virtual void writeBlock(void *block, dword size, fileOffset off, bool stay=false, bool addLength=true);
//	virtual void skipBlocks(dword n=1);
//	virtual void rewindBlocks(dword n=1);
	
	virtual Endian::endianType getEndian();
	
	virtual qword getCacheHits();
	virtual qword getCacheMisses();
	virtual float getCacheHitRatio();
	virtual fileOffset tell();
	
	static fileOffset firstUsableOffset();
};

