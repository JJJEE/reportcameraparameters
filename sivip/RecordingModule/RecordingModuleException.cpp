/*
 *  RecordingModuleException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingModuleException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

RecordingModuleException::RecordingModuleException(string msg, int code): Exception(msg, code)
{

}

RecordingModuleException::RecordingModuleException(int code, string msg): Exception(msg, code)
{

}

RecordingModuleException::RecordingModuleException(SerializedException &se): Exception(se)
{

}

RecordingModuleException::~RecordingModuleException()
{
   
}

string RecordingModuleException::getClass()
{
	string c=string("RecordingModuleException");
	return c;
}

