/*
 *  RecordingFileSegmentMetadataChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <RecordingModule/RecordingFileDateMetadataChunk.h>

class RecordingFileSegmentMetadataChunk : public RecordingFileMetadataChunk
{
	static const dword chunkSizeCheck=sizeof(fileOffset)+sizeof(dword);

	fileOffset firstFrameOffset;
	dword nFrames;
	
public:
	RecordingFileSegmentMetadataChunk();
	virtual ~RecordingFileSegmentMetadataChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	virtual void setDate(qword secs, word millis);
	virtual RecordingFileDateMetadataChunk* getDate();
	virtual void setFirstFrameOffset(fileOffset off);
	virtual fileOffset getFirstFrameOffset();
	virtual void setNFrames(dword nf);
	virtual void incNFrames(int inc=1);
	virtual dword getNFrames();
};

