/*
 *  RecordingFileMetadataChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <Utils/debugStackTrace.h>

RecordingFileMetadataChunk::RecordingFileMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
	// NO caldria registrar, pq no s'haura de cridar mai newChunk, pero "per si les mosques" :)
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileMetadataChunk::newChunk;
}

RecordingFileMetadataChunk::~RecordingFileMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileChunk* RecordingFileMetadataChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	// No s'hauria de cridar mai :P
	return NULL;
}

void* RecordingFileMetadataChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	// No s'hauria de cridar mai :P
	return NULL;
}

string RecordingFileMetadataChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileMetadataChunk";
	return c;
}

