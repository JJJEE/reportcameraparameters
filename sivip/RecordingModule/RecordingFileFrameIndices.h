#ifndef __SIRIUS__RECORDINGMODULE__RecordingFileFrameIndices_H
#define __SIRIUS__RECORDINGMODULE__RecordingFileFrameIndices_H

// Encapsula els indexos dels tots els recording files

#include <Threads/RWlock.h>
#include <RecordingModule/RecordingFileFrameIndex.h>

#include <map>
#include <list>

using namespace std;

class RecordingFileFrameIndices
{
public:
	
protected:
	// Indexos per cada fitxer
	static map<string, RecordingFileFrameIndex*> frameIndexByFile;
	// Lock per concurrencia
	static RWLock frameIndexRW;
	
public:
	static RecordingFileFrameIndex *getIndexForFile(string file,
		RecordingModuleDiskWriter *writer);
	static void setIndexForFile(string file, RecordingFileFrameIndex *index);
	static void forgetIndexForFile(string file);
	static void wlock();
	static void rlock();
	static void unlock();
};

#endif
