/*
 *  RecordingFileIndexChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/05/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileIndexChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

RecordingFileIndexChunk::RecordingFileIndexChunk(dword size, dword firstFrame) : indexSize(size), 
	firstFrameNumber(firstFrame), nextIndexOffset(0)
{
	STACKTRACE_INSTRUMENT();

	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileIndexChunk::newChunk;
	className=getClass();
	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// La resta autoexplicatius (o no, pero poc important si ho son o no)
	indexEntry idxEnt;
	
	chunkSize=sizeof(byte)+className.length()+sizeof(dword)*2+sizeof(indexSize)+sizeof(firstFrameNumber)+
		sizeof(nextIndexOffset)+idxEnt.size()*indexSize;

	index=new indexEntry*[indexSize];
	
	index[0]=new indexEntry[indexSize];
	
	for (dword i=1; i<indexSize; i++)
		index[i]=index[0]+i;

}

RecordingFileIndexChunk::~RecordingFileIndexChunk()
{
	STACKTRACE_INSTRUMENT();
//	for (dword i=0; i<indexSize; i++)
//	{
//		delete index[i];
//	}
	delete [] index[0];
		
	delete [] index;
}

RecordingFileChunk* RecordingFileIndexChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);

	// cout << "RecordingFileIndexChunk::newChunk 1" << endl;
	
	RecordingFileIndexChunk *resChunk=new RecordingFileIndexChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

//	if (chunkClass!=resChunk->getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	// cout << "RecordingFileIndexChunk::newChunk 1" << endl;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	resChunk->chunkSize=size;
	
	chunkBytes+=chunkTypeLen;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (resChunk->nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
		
	// cout << "RecordingFileIndexChunk::newChunk 1" << endl;

	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

//	if (dataSize!=chunkSizeCheck)
//		throw RecordingFileChunkException(0, resChunk->getClass()+string(": Invalid data size (")+
//											StrUtils::decToString(dataSize)+string("/")+
//											StrUtils::decToString(chunkSizeCheck)+string(")"));
	
	// cout << "RecordingFileIndexChunk::newChunk 1" << endl;

	resChunk->indexSize=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->indexSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->firstFrameNumber=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->firstFrameNumber, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nextIndexOffset=*((fileOffset*)chunkBytes);
	Endian::from(endian, &resChunk->nextIndexOffset, sizeof(fileOffset));
	chunkBytes+=sizeof(fileOffset);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	// cout << "RecordingFileIndexChunk::newChunk 1" << endl;

//	if (resChunk->indexSize!=RecordingFileIndexChunk::defaultSize)
//	{
//		for (dword i=0; i<resChunk->indexSize; i++)
//			delete resChunk->index[i];
		delete [] resChunk->index[0];
		delete [] resChunk->index;
//	}
	
	resChunk->index=new indexEntry*[resChunk->indexSize];
	
	resChunk->index[0]=new indexEntry[resChunk->indexSize];
//	for (dword i=0; i<resChunk->indexSize; i++)
//		resChunk->index[i]=new indexEntry;
	for (dword i=1; i<resChunk->indexSize; i++)
		resChunk->index[i]=resChunk->index[0]+i;

	for (dword i=0; i<resChunk->indexSize; i++)
	{
		resChunk->index[i]->from(endian, chunkBytes);

		chunkBytes+=resChunk->index[i]->size();
	}
	// cout << "RecordingFileIndexChunk::newChunk 1 - return" << endl;

	return (RecordingFileChunk*)resChunk;
}

void* RecordingFileIndexChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	byte *data=new byte[chunkSize];
	byte *chunkBytes=data;
	
	if (data==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
		
	byte chunkTypeLen=getClass().length();
	*chunkBytes=chunkTypeLen;
	chunkBytes++;
	
	memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
	chunkBytes+=chunkTypeLen;
	
	if (nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
	*((dword*)chunkBytes)=nSubChunks;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	*((dword*)chunkBytes)=chunkSize-(sizeof(dword)+sizeof(byte)+className.length());
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((dword*)chunkBytes)=indexSize;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((dword*)chunkBytes)=firstFrameNumber;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((qword*)chunkBytes)=nextIndexOffset;
	Endian::to(dstEndian, chunkBytes, sizeof(qword));
	chunkBytes+=sizeof(qword);

//	cout << "RecordingFileIndexChunk::getData N1" << endl;

	for (dword i=0; i<indexSize; i++)
	{
//		cout << "RecordingFileIndexChunk::getData i: " << i << ", data " << (void*)data << ", chunkBytes: " << 
//			(void*)chunkBytes << ", size: " << chunkSize << ", sizeWritten: " << (dword)(chunkBytes-data) << endl;

		void *idxBytes=index[i]->to(dstEndian);
		memmove(chunkBytes, idxBytes, index[i]->size());
		delete [] (byte*)idxBytes;
		
		chunkBytes+=index[i]->size();
	}

//	cout << "RecordingFileIndexChunk::getData N2" << endl;

	return data;
}

string RecordingFileIndexChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileIndexChunk";
	return c;
}

void RecordingFileIndexChunk::setFirstFrame(dword absoluteFrame)
{
	STACKTRACE_INSTRUMENT();
	firstFrameNumber=absoluteFrame;
}

dword RecordingFileIndexChunk::getFirstFrame()
{
	STACKTRACE_INSTRUMENT();
	return firstFrameNumber;
}

void RecordingFileIndexChunk::setFrame(dword absoluteFrame, qword secs, word millis, fileOffset off)
{
	STACKTRACE_INSTRUMENT();
	if (absoluteFrame-firstFrameNumber >= indexSize || absoluteFrame-firstFrameNumber <0)
		throw RecordingFileChunkException(0, "Invalid frame number");
		
	indexEntry *i=index[absoluteFrame-firstFrameNumber];
	
	i->date.secs=secs;
	i->date.millis=millis;
	i->frameOffset=off;
}

RecordingFileIndexChunk::indexEntry* RecordingFileIndexChunk::getFrame(dword absoluteFrame)
{
	STACKTRACE_INSTRUMENT();
	if (absoluteFrame-firstFrameNumber >= indexSize || absoluteFrame-firstFrameNumber <0)
		throw RecordingFileChunkException(0, "Invalid frame number");
		
	return index[absoluteFrame-firstFrameNumber];
}

void RecordingFileIndexChunk::setNextIndexOffset(fileOffset off)
{
	STACKTRACE_INSTRUMENT();
	nextIndexOffset=off;
}

fileOffset RecordingFileIndexChunk::getNextIndexOffset()
{
	STACKTRACE_INSTRUMENT();
	return nextIndexOffset;
}

dword RecordingFileIndexChunk::getIndexSize()
{
	STACKTRACE_INSTRUMENT();
	return indexSize;
}

