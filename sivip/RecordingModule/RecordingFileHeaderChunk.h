/*
 *  RecordingFileHeaderChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 19/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileChunk.h>

class RecordingFileHeaderChunk : public RecordingFileChunk
{
public:
	// Reservem fins a 10ks per la capçalera.
	static const int defaultReservedBytes=10*1024;

	// tipus de gravacio
	enum recType {REC_MANUAL=0L, REC_SCHEDULED, REC_ALARM, REC_HOWMANY};

protected:
	static const dword chunkSizeCheck=sizeof(word)*4+sizeof(dword)+sizeof(recType)+sizeof(fileOffset);

	// La part fraccionaria es sobre 1000
	word verHi, verLo;
	word fpsHi, fpsLo;
	dword fourcc;
	recType recordingType;
	fileOffset firstIndexOffset;
	
public:
	RecordingFileHeaderChunk();
	virtual ~RecordingFileHeaderChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	virtual void setVersion(word whole, word frac=0);
	virtual word getVersionWhole();
	virtual word getVersionFrac();
	virtual float getVersion();
	
	virtual void setFPS(word whole, word frac=0);
	virtual word getFPSWhole();
	virtual word getFPSFrac();
	virtual float getFPS();
	
	virtual void setFourCC(dword fourcc);
	virtual dword getFourCC();
	
	virtual void setRecType(recType type);
	virtual recType getRecType();

	virtual void setFirstIndexOffset(fileOffset off);
	virtual fileOffset getFirstIndexOffset();
};

