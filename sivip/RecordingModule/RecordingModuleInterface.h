/*
 *  RecordingModuleInterface.h
 *  
 *
 *  Created by David Marí Larrosa on 27/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <ModuleInterface/ModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <Endian/Endian.h>
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Utils/Timer.h>
#include <Utils/Watchdog.h>
#include <Utils/StrUtils.h>
#include <Threads/Mutex.h>
#include <Threads/Condition.h>
#include <Threads/RWlock.h>
#include <RecordingModule/RecordingModuleTypes.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileException.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <RecordingModule/RecordingFileFrameChunk.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <RecordingModule/RecordingFileDateMetadataChunk.h>
#include <RecordingModule/RecordingFilePTZMetadataChunk.h>
#include <RecordingModule/RecordingFileSegmentMetadataChunk.h>
#include <RecordingModule/RecordingFileStringMetadataChunk.h>
#include <RecordingModule/RecordingFileIndexChunk.h>
#include <RecordingModule/RecordingFileFrameInfoMetadataChunk.h>
#include <RecordingModule/RecordingFileAlarmMetadataChunk.h>
#include <RecordingModule/RecordingModuleDiskWriter.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Exceptions/InvalidStateException.h>
#include <ObjectManager/ObjectManager.h>
//#include <ObjectManager/ObjectManager>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <string>
#include <map>
#include <iostream>
#ifndef WIN32
#ifndef WIN32
#ifndef WIN32
#include <unistd.h>
#endif
#endif
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

class RecordingModuleLimitsThread;
//class RecordingModuleInterfaceSaveStreamThread;
class RecordingModuleAlarmAndLogThread;

class RecordingModuleInterface : public ModuleInterface
{
	friend class RecordingModuleLimitsThread;
	friend class RecordingModuleAwarenessThread;
	friend class RecordingModuleAlarmAndLogThread;
//	friend class RecordingModuleInterfaceSaveStreamThread;

	string configFile;
//	static DBGateway *db;

	static const int maxCamExc = 10;
	static const int maxServExc = 10;

	static const int maxCamSecsInAlert = 5;
	static const float maxCamPercentInAlert;
	static const int reassignWaitSecs = 60;

	static RecordingModuleDiskWriter *diskWriter;

	static list<int> camsInAlert;
	static Mutex camsAlertLock;

public:
	class DWWatchdogCallback: public WatchdogCallback
	{
		RecordingModuleInterface *rmi;
	public:
		DWWatchdogCallback();
		virtual void watchdogTimeout(int seconds);
		void setRMI(RecordingModuleInterface *rmi);
	};

	static DWWatchdogCallback *dwWatchdogCallback;
	static Watchdog *dwWatchdog;
	static const int watchdogSeconds = 30;

public:
	static bool reassignPending;
	static Timer reassignRequestTime;

	// 96 fitxers per dia (8640 = 10 fitx.)
	// minim 864s (100 fitxers)
#ifdef SRF_SECS_DEBUG
	static const int SRFSeconds=10;
#else
	static const int SRFSeconds=900;
#endif

	
	// Reservem fins a 10ks per la capçalera.
	static const int headerReservedBytes=10*1024;

	static const int serviceCount=17;

	static const int startSessionServiceId=0;
	static const int endSessionServiceId=1;
	static const int saveFrameServiceId=2;
	static const int readFrameServiceId=3;
	static const int setTimeoutServiceId=4;
	static const int skipFramesServiceId=5;
	static const int startRecordingStreamServiceId=6;
	static const int stopRecordingStreamServiceId=7;
	static const int listRecordingsServiceId=8;
	static const int isRecordingServiceId=9;
	static const int getKiBLimitServiceId=10;
	static const int setKiBLimitServiceId=11;
	static const int getFrameLimitServiceId=12;
	static const int setFrameLimitServiceId=13;
	static const int getCyclicServiceId=14;
	static const int setCyclicServiceId=15;
	static const int startLocalRecordingStreamServiceId=16;



protected:
	// Directori base de gravacio
	static string baseDir;
	qword storeKiBLimit;
		
	// Informació d'indexos de frames de fitxers de gravacio. Com que te un
	// estat determinat en un moment concret en el temps, cal que sigui
	// informacio compartida entre sessions, sempre que estiguin tractant amb
	// el mateix fitxer
//	struct SharedSessionData
//	{
//		Mutex *shareMutex;
//		int refCount;
//		dword nIndices;
//		RecordingFileIndexChunk **indices; //TODO separats per lectura i escriptura --> per aixó no es veu l'ultim catxo (creiem)
//		fileOffset *indicesOffsets;
//		
//		SharedSessionData() : shareMutex(new Mutex(true)), refCount(1), nIndices(0), indices(NULL), indicesOffsets(NULL)
//		{
//			if (shareMutex==NULL)
//				throw new NotEnoughMemoryException("Not enough memory to allocate sharing Mutex"); 
//		}
//
//		// No s'hauria d'executar mai la constructora per copia, pq 
//		// SharedSessionData es fara servir nomes com a punter des de la sessio
//		
//		~SharedSessionData()
//		{
//			cout << "~SharedSessionData" << endl;
//		}
//		
//		void shareLock()
//		{
//			shareMutex->lock();
//		}
//		
//		void shareUnlock()
//		{
//			shareMutex->unlock();
//		}
//		
//		void retain()
//		{
//			shareLock();
//			if (this->refCount==0)
//			{
//				cout << "SharedSessionData::retain(): SharedSessionData already fully released" << endl;
//				throw InvalidStateException("SharedSessionData already fully released");
//			}
//			this->refCount++;
//			shareUnlock();
//		}
//		
//		void release()
//		{
//			shareLock();
//			if (this->refCount==0)
//			{
//				cout << "SharedSessionData::release(): SharedSessionData already fully released" << endl;
//				throw InvalidStateException("SharedSessionData already fully released");
//			}
//			this->refCount--;
//			if (this->refCount==0)
//			{
//				if (this->indices!=NULL)
//				{
//					for (dword i=0; i<this->nIndices; i++)
//					{
//						delete this->indices[i];
//					}
//					delete [] this->indices;
//					delete [] this->indicesOffsets;
//					this->indices=NULL;
//					this->indicesOffsets=NULL;
//					this->nIndices=0;
//				}
////				shareUnlock();
//				delete this->shareMutex;
//				this->shareMutex=NULL;
//				delete this;
//			}
//			else
//				shareUnlock();
//		}
//		
//	};
	
	// Informacio de sessio
	struct session
	{
		// Adreça de la sessio si no es local. Si es local, NULL
		Address *rmAddr;
	
		bool sessionStarted;
		bool inDB, toDB;
		RMStartSessionParams sessionParams;
		// Per anar-nos guardant els indexos q anem llegint
//		dword nIndices;
//		RecordingFileIndexChunk **indices;
//		fileOffset *indicesOffsets;
		
		// Separats perque R i W poden (i de fet es el normal) estar per
		// diferents fitxers.
		// A mes a mes, necessitariem un per cada lectura, per tal que no
		// es barregin diferents lectures dins una mateixa sessio.
//		SharedSessionData *sharedDataR;
//		SharedSessionData *sharedDataW;

		dword absFrameR; //TODO: pq no es copia al ::copy?
		dword absFrameW;
		dword currentIndexR;
		dword currentIndexW;
		
		dword indexWriteCount;
		RecordingFile *rf;
		RecordingFile *rf2;
//		int deviceId;
		RecordingFileDateMetadataChunk *currentDate;
		RecordingFileDateMetadataChunk *lastReqDate, *lastSentDate;
		RecordingFileHeaderChunk *header;
		RecordingFileSegmentMetadataChunk *segs[2];
		fileOffset headerOffset;
		string currentFilename;
		string currentFilename2;
		int cfn2Error;
		byte currentSegment;
		Timer tOutTimer;
		dword timeOut;
		qword sizeLimit;
		RecordingFileFrameChunk *lastFrame;
		fileOffset nextFrameOff;
		fileOffset nextFrameOff2;
		dword currSegFrame;
		
		fileOffset KiBLimit;
		dword frameLimit;
		
		bool startedRecording; //la sessió que realment està gravant els frames
		bool startRecStreamRecv;// ha iniciat algúna gravació d'un tipus ( no implica startedRecrd)
//		void *recordingThread;
		
		qword recIndexIdx;
		
		Mutex *sMutex;
		Mutex *endSessionMutex;
		
		Address freeSesAddr, freeSesModuleAddr;
		
		session() : rf(NULL), rf2(NULL), currentDate(NULL),
			currentFilename(string("")), currentFilename2(string("")), cfn2Error(0), sessionStarted(false), timeOut(0),
			sizeLimit(0), header(NULL), headerOffset(0), currentSegment(0), lastFrame(NULL),
			nextFrameOff(0), nextFrameOff2(0), currSegFrame(0), KiBLimit(0), frameLimit(0), startedRecording(false), startRecStreamRecv(false),
			/* sharedDataR(new SharedSessionData()), sharedDataW(new SharedSessionData()), */ absFrameR(0), absFrameW(0), currentIndexR(0), currentIndexW(0), indexWriteCount(0),
			recIndexIdx(0xffffffffffffffffll), rmAddr(NULL), sMutex(new Mutex(true)), endSessionMutex(new Mutex(true)), inDB(false), toDB(false)
		{
			STACKTRACE_INSTRUMENT();
//			ObjectManager<Mutex>::manage(
//				ObjectManager<Mutex>::id(endSessionMutex), endSessionMutex);
			segs[0]=segs[1]=NULL;
			lastReqDate = new RecordingFileDateMetadataChunk();
			lastReqDate->setDate(0,0);
			lastSentDate = new RecordingFileDateMetadataChunk();
			lastSentDate->setDate(0,0);
			
			if (sMutex==NULL)
				throw new NotEnoughMemoryException("Not enough memory to allocate session Mutex"); 

			if (endSessionMutex==NULL)
				throw new NotEnoughMemoryException("Not enough memory to allocate end session protection Mutex"); 
				
//			if (sharedDataR==NULL)
//				throw new NotEnoughMemoryException("Not enough memory to allocate Shared Session Data (r)"); 
//			if (sharedDataW==NULL)
//				throw new NotEnoughMemoryException("Not enough memory to allocate Shared Session Data (w)"); 
		};
		
		void copy(const session *s)
		{
		cout << __FILE__ << ", line " << __LINE__ << "[" << sessionParams.devId <<"]: session::copy(session &s) : "<<(rmAddr==NULL?string("NULL"):rmAddr->toString())<<" aixó no hauria de sortir"<< endl;
			STACKTRACE_INSTRUMENT();
			s->sMutex->lock();
			
			rmAddr=s->rmAddr;
			sessionStarted=s->sessionStarted;
			sessionParams=s->sessionParams;

//			// Per si ja son la mateixa, primer retain i despres release!!!
//			s->sharedDataR->retain();
//			if (sharedDataR!=NULL)
//				sharedDataR->release();
//			sharedDataR=s->sharedDataR;
//
//			// Per si ja son la mateixa, primer retain i despres release!!!
//			s->sharedDataW->retain();
//			if (sharedDataW!=NULL)
//				sharedDataW->release();
//			sharedDataW=s->sharedDataW;

			rf=s->rf;
			rf2=s->rf2;
			currentDate=s->currentDate;
			header=s->header;
			headerOffset=s->headerOffset;

			segs[0]=s->segs[0];
			segs[1]=s->segs[1];

			currentFilename=s->currentFilename;
			currentFilename2=s->currentFilename2;
			cfn2Error=0;
			currentSegment=s->currentSegment;
			tOutTimer=s->tOutTimer;
			timeOut=s->timeOut;
			sizeLimit=s->sizeLimit;
			lastFrame=s->lastFrame;
			nextFrameOff=s->nextFrameOff;
			nextFrameOff2=s->nextFrameOff2;
			currSegFrame=s->currSegFrame;

			KiBLimit=s->KiBLimit;
			frameLimit=s->frameLimit;

			startedRecording=s->startedRecording;
			startRecStreamRecv=s->startRecStreamRecv;
//			recordingThread=s->recordingThread;
			recIndexIdx=s->recIndexIdx;
			inDB = s->inDB;//controlar :P
		
			sMutex=s->sMutex;
			ObjectManager<Mutex>::retain(
				ObjectManager<Mutex>::id(s->endSessionMutex));
			endSessionMutex=s->endSessionMutex;

			s->sMutex->unlock();
		}
		
		// Constructora per copia
		session(const session &s)
		{
			STACKTRACE_INSTRUMENT();
			//this->copy(&s);
		cout << __FILE__ << ", line " << __LINE__ << "[" << sessionParams.devId <<"]: session::session(session &s) : "<<(rmAddr==NULL?string("NULL"):rmAddr->toString())<<" aixó no hauria de sortir"<< endl;
		}

		virtual ~session()
		{
//			this->sharedDataR->release();
//			this->sharedDataW->release();

			if (rf!=NULL)
			{
				cout << "stopRecording: rf cache hit ratio: " << rf->getCacheHitRatio() << ", " << rf->getCacheHits() <<
					" hits, " << rf->getCacheMisses() << " misses" << endl;
				delete rf;
				rf=NULL;
			}
			if (rf2!=NULL)
			{
				cout << "stopRecording: rf2 cache hit ratio: " << rf2->getCacheHitRatio() <<  ", " << rf2->getCacheHits() <<
					" hits, " << rf2->getCacheMisses() << " misses" << endl;
				delete rf2;
				rf2=NULL;
			}
			currentFilename=string("");
			currentFilename2=string("");
			cfn2Error=0;
			if (this->currentDate!=NULL) delete this->currentDate;
			if (this->header!=NULL) { delete this->header; this->header=NULL; }
			if (this->segs[0]!=NULL) { delete this->segs[0]; this->segs[0]=NULL; }
			if (this->segs[1]!=NULL) { delete this->segs[1]; this->segs[1]=NULL; }

			if (this->sMutex!=NULL)
			{
				delete this->sMutex;
				this->sMutex=NULL;
			}
		
			if(this->endSessionMutex!=NULL)
			{
				delete this->endSessionMutex;
				this->endSessionMutex=NULL;
			}

			// Si ens han fet el set d'una entenem que de l'altra tb :P
			if (this->freeSesAddr.getIP().toDWord()!=0 && (this->toDB || this->inDB)) //si no està, i no hauria d'estar. sudém, menys carrega a la BD
			{
				try
				{
					RecordingModuleInterface::deleteSessionFromDB(&this->freeSesAddr, this->freeSesModuleAddr);
				}
				catch (Exception &e) {};
				this->inDB=false;
			}

	//		try
	//		{
	//			if(rmAddr != NULL)//TODO:mirar
	//				deleteSessionFromDB(sesAddr, rmAddr);
	//		}
	//		catch (Exception &e)
	//		{
				// Si hi ha alguna excepcio borrant de la DB, no permetem que pugi cap
				// a dalt, ja que no es destruiria la sessio...
	//		}
	//		RecordingModuleInterface::releaseRecIndex();//del startSession(create)
														//abans el feia el freeSession
														
			if (this->lastReqDate!=NULL)
			{
				delete this->lastReqDate;
				this->lastReqDate=NULL;
			}
			if (this->lastSentDate!=NULL)
			{
				delete this->lastSentDate;
				this->lastSentDate=NULL;
			}
		}
		
		string toString()
		{
			STACKTRACE_INSTRUMENT();
			string s=string("----- session ------")+
				string("\ndevId: ")+StrUtils::decToString(this->sessionParams.devId)+
				string("\nsessionStarted: ")+(this->sessionStarted?string("true"):string("false"))+
//				string("\nrecThreadIsNull: ")+(this->recordingThread==NULL?string("true"):string("false"))+
//				string("\nrecThread: ")+StrUtils::hexToString((unsigned int)this->recordingThread)+
				string("\nstartedRecording: ")+(this->startedRecording?string("true"):string("false"))+
				string("\nstartRecStrRecv: ")+(this->startRecStreamRecv?string("true"):string("false"))+
//				string("\nisRecording: ")+(this->startedRecording && this->recordingThread!=NULL?string("true"):string("false"))+
//				string("\nnIndices (r): ")+StrUtils::decToString(this->sharedDataR->nIndices)+
//				string("\nSharedData refCount (r): ")+StrUtils::decToString(this->sharedDataR->refCount)+
//				string("\nnIndices (w): ")+StrUtils::decToString(this->sharedDataW->nIndices)+
//				string("\nSharedData refCount (w): ")+StrUtils::decToString(this->sharedDataW->refCount)+
				string("\n");
			
			return s;		
		}
	};

	class RecordingModuleInterfaceSaveStreamThread : public Thread
	{
		public:
			static int activeRecordingsCount;
			static bool warnActiveRaised;
			static bool almActiveRaised;
			static Mutex activeRecordingsCountLock;
			
			Mutex *lockMutex;
		protected:
			//	RPCPacket *decodeModuleLocation;
			//	RPC *decodeModule;
			DecodeModuleAccess *decodeModule;
//			AlarmModuleAccess *alarmModule;
//			AlarmManager *alarmManager;
			RecordingModuleInterface *intf;
			float fps;
			int devId;
			bool stop;
			Timer t;
			Mutex *sesAddrMutex;
			//per substituir el join() en el stopRecordingStream
			bool stopped;
			Condition stopCond;
			
			int alertCount;
			bool inAlert;
			bool recLost; //per logging/alarmes de grabacions perdudes/recuperades...

			Address sessionAddr;

		public:
			bool running;
			RecordingModuleInterfaceSaveStreamThread(float fps, int devId, RecordingModuleInterface *intf, Address sessionAddr);
			~RecordingModuleInterfaceSaveStreamThread();

			void stopThread();
			void clearStop();
			void setSessionAddr(Address sessAddr);
			float getFPS();
			void setFPS(float fps);

			void lockSessionAddress();
			void unlockSessionAddress();

			virtual void* execute(int id, void* params);
	};

	static map<int,RecordingModuleInterfaceSaveStreamThread*> camThreads;
	//static RWLock cThrLock;
	static Mutex cThrLock;

	static int maxActiveRecordingsCount;
	static int warnActiveRecordingsCount;
	static const int maxActiveRecordingsHardLimit=150;
	static int warnRecycleTime;

	class DBSesInfo
	{
		public:
			string ip, port;
			int devId, recType;
			bool recording;
	
			DBSesInfo():devId(-1), recType(-1), recording(false), ip(""), port("")
			{}
	};
	
	
//	static map<string,RecordingModuleInterface::session> sessions;

public:
	struct recordingSegment
	{
		RMDate startDate, endDate;
		RecordingFileHeaderChunk::recType recType;
		dword devId;
		struct fps
		{
			word whole, frac;
			
			fps() : whole(25), frac(0) {};
		} fps;
		
		recordingSegment()
		{
			STACKTRACE_INSTRUMENT();
		
		}
		
		recordingSegment(recordingSegment &s)
		{
			STACKTRACE_INSTRUMENT();
			startDate.isValid=s.startDate.isValid;
			startDate.secs=s.startDate.secs;
			startDate.millis=s.startDate.millis;

			endDate.isValid=s.endDate.isValid;
			endDate.secs=s.endDate.secs;
			endDate.millis=s.endDate.millis;

			recType=s.recType;
			devId=s.devId;
			
			fps.whole=s.fps.whole;
			fps.frac=s.fps.frac;
		}
		
		~recordingSegment() {}
	
		void from(Endian::endianType e, const void *b)
		{
			STACKTRACE_INSTRUMENT();
			byte *buf=(byte*)b;
			
			startDate.isValid=*((bool*)buf);
			buf+=sizeof(bool);
		
			startDate.secs=*((qword*)buf);
			buf+=sizeof(qword);
			Endian::from(e, &startDate.secs, sizeof(qword));

			startDate.millis=*((word*)buf);
			buf+=sizeof(word);
			Endian::from(e, &startDate.millis, sizeof(word));
			
			endDate.isValid=*((bool*)buf);
			buf+=sizeof(bool);
		
			endDate.secs=*((qword*)buf);
			buf+=sizeof(qword);
			Endian::from(e, &endDate.secs, sizeof(qword));

			endDate.millis=*((word*)buf);
			buf+=sizeof(word);
			Endian::from(e, &endDate.millis, sizeof(word));
			
			recType=*((RecordingFileHeaderChunk::recType*)buf);
			buf+=sizeof(RecordingFileHeaderChunk::recType);
			Endian::from(e, &recType, sizeof(RecordingFileHeaderChunk::recType));

			devId=*((dword*)buf);
			buf+=sizeof(dword);
			Endian::from(e, &devId, sizeof(dword));

			fps.whole=*((word*)buf);
			buf+=sizeof(word);
			Endian::from(e, &fps.whole, sizeof(word));

			fps.frac=*((word*)buf);
			buf+=sizeof(word);
			Endian::from(e, &fps.frac, sizeof(word));
		}
	
		void* to(Endian::endianType e)
		{
			STACKTRACE_INSTRUMENT();
			byte *b=new byte[size()];
			
			byte *buf=b;
			
			*((bool*)buf)=startDate.isValid;
			buf+=sizeof(bool);
			
			*((qword*)buf)=startDate.secs;
			buf+=sizeof(qword);
			Endian::to(e, buf, sizeof(startDate.secs));
			
			*((word*)buf)=startDate.millis;
			buf+=sizeof(word);
			Endian::to(e, buf, sizeof(startDate.millis));
			
			*((bool*)buf)=endDate.isValid;
			buf+=sizeof(bool);
			
			*((qword*)buf)=endDate.secs;
			buf+=sizeof(qword);
			Endian::to(e, buf, sizeof(endDate.secs));
			
			*((word*)buf)=endDate.millis;
			buf+=sizeof(word);
			Endian::to(e, buf, sizeof(endDate.millis));
			
			*((RecordingFileHeaderChunk::recType*)buf)=recType;
			buf+=sizeof(RecordingFileHeaderChunk::recType);
			Endian::to(e, buf, sizeof(RecordingFileHeaderChunk::recType));
			
			*((dword*)buf)=devId;
			buf+=sizeof(dword);
			Endian::to(e, buf, sizeof(dword));
			
			*((word*)buf)=fps.whole;
			buf+=sizeof(word);
			Endian::to(e, buf, sizeof(word));
			
			*((word*)buf)=fps.frac;
			buf+=sizeof(word);
			Endian::to(e, buf, sizeof(word));
			
			return b;
		};

		static dword size()
		{
			STACKTRACE_INSTRUMENT();
			return (sizeof(bool)+sizeof(qword)+sizeof(word))*2+sizeof(RecordingFileHeaderChunk::recType)+sizeof(dword)+
				sizeof(word)*2;
		}
	};

	struct recordingIndex
	{
		bool writeToDisk;
		
		dword id;
		qword nSegments;
		recordingSegment *segments;
		
		// Endianesa en la que esta el fitxer
		Endian::endianType e;
		qword allocatedSegments;		// Num de segments que ja tenim reservats
		
		// Sense cache, no ens permetrem tenir-lo "flotant", per si les petades...
		File *f;
				
		static const dword idInLocalEndian=0x13371de8;	// leet i(n)dex
		
		recordingIndex(string fname);
		~recordingIndex();
		
		qword addSegment(recordingSegment &seg);
		void alterSegment(qword segIdx, recordingSegment &seg);
		const recordingSegment* getSegment(qword segIdx);
		void writeNumSegments();
		void setWriteToDisk(bool wr);
		bool isWriteToDisk();
		void dumpToDisk();
	};
	
protected:
	static RWLock recIndexLock;
	static Mutex recIndexRefLock;
	static dword recIndexRef;
	static recordingIndex *recIndex;
	static DBGateway *dbGW;
//	static Mutex dbgwMutex;

	static Mutex createDirLock;
	static IP excludedSearchIP;
		
	static void *awarenessThread, *limitsThread;
	static RecordingModuleAlarmAndLogThread *almLogThread;
	
	static string getSessionKey(RecordingModuleInterface::session* s);
	static string getSessionKeyForDevice(int devId);
	static string getSessionKeyForDevice(int devId, string current);
	static string getSessionKeyForDevice(int devId, string current, RecordingFileHeaderChunk::recType);

	static RecordingModuleInterfaceSaveStreamThread* getRecThread(int devId, bool prelocked=false);
	static bool recThreadStarted(int devId, bool prelocked=false);

	static RecordingModuleInterface::session *getSessionRecordingDevice(int devId, bool local=false, bool prelocked=false);
	static RecordingModuleInterface::session *getSessionRecordingDevice(int devId, RecordingFileHeaderChunk::recType rType, bool local=false);
	static RecordingModuleInterface::session *getSessionForAddress(Address *a);
	string getAutomaticFileBasePath(int devId, RecordingFileDateMetadataChunk *date);
	string getAutomaticFileName(int devId, RecordingFileDateMetadataChunk *date);
	static void createPath(string pathStr);
	static void freeSession(Address *sesAddr, Address rmAddr);
	static void relocateSession(Address *sesAddr, Address rmAddr);

	static void sessionToDB(Address *sesAddr, Address rmAddr, session *rses = NULL);
	static void deleteSessionFromDB(Address *sesAddr, Address rmAddr);

	void logDBEvent(DBGateway *db, string logDate, string msg, string level);
	void logDBDev(DBGateway *db, string logDate, string msg, int devId, string level);
	void raiseSystemAlarm(AlarmModuleAccess *ama, string msg, int value, bool raise=true);

	static RPCPacket* RMIStartRecordingStream(RecordingModuleInterface *_this, Address *a, void *params, bool local);

		
	//void listRecordingsToDB(RMListRecordingsParams lrp, Address rmAddr);
	//void listRecordingsDiffToDB(RMListRecordingsParams lrp, Address rmAddr, RMListRecordingsParams *old);
public:
	static void setMaxActiveRecordingsCount(int newCount);
	void checkFullFactorShutdown();

	static void limitsThreadLock();
	static void limitsThreadUnlock();

	RecordingModuleInterface(Address recordingModuleAddress, short recordingModuleType, string baseDir, qword kibLimit, Canis *cn=NULL, string configFile=string(""));

//	static void retainRecIndex();
//	static void releaseRecIndex();
	
	static void wlockRecIndex();
	static void rlockRecIndex();
	static void unlockRecIndex();
	
	static recordingIndex *getRecIndex();
	string getBaseDir();
	qword getSizeLimit();

	static RPCPacket* startSession(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* endSession(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* saveFrame(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* readFrame(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* setTimeout(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* skipFrames(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* startRecordingStream(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* startLocalRecordingStream(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* stopRecordingStream(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* listRecordings(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* isRecording(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* getKiBLimit(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* setKiBLimit(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* getFrameLimit(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* setFrameLimit(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* getCyclic(RecordingModuleInterface *_this, Address *a, void *params);
	static RPCPacket* setCyclic(RecordingModuleInterface *_this, Address *a, void *params);

};

