#include <RecordingModule/RecordingModuleDiskWriterEntry.h>
#include <Utils/Log.h>
#include <Utils/StrUtils.h>
#include <Threads/Thread.h>
#include <RecordingModule/RecordingFileFrameChunk.h>

#pragma mark ***
#pragma mark *** RecordingModuleDiskWriterEntry
#pragma mark ***

#pragma mark *** Estatiques
const double RecordingModuleDiskWriterEntry::maxAge = 60.0;	// 1 minut

#pragma mark *** Constructores
RecordingModuleDiskWriterEntry::RecordingModuleDiskWriterEntry() :
	offset(0), size(0), chunk(NULL), delayedWrite(false),
	deleteAtDestructor(true)
{
	STACKTRACE_INSTRUMENT();
	this->age.start();
}

RecordingModuleDiskWriterEntry::RecordingModuleDiskWriterEntry(
	fileOffset offset, dword size, RecordingFileChunk *chunk,
	bool delayedWrite, bool deleteAtDestructor) :
	offset(offset), size(size), chunk(chunk),
	delayedWrite(delayedWrite), deleteAtDestructor(deleteAtDestructor)
{
	STACKTRACE_INSTRUMENT();
	this->age.start();

//	if (this->chunk!=NULL && this->chunk->getClass()==string("RecordingFileFrameChunk"))
//	{
//#ifndef WIN32
//		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" [RecordingModuleDiskWriterEntry] CREATING ") << (this->chunk!=NULL?this->chunk->getClass()+string(" "):" ") << (void*)this->chunk << " " << ((RecordingFileFrameChunk*)this->chunk)->getFrame() << endl;
//		STACKTRACE_DUMP();
//#endif	
//	}

}

RecordingModuleDiskWriterEntry::~RecordingModuleDiskWriterEntry()
{
	STACKTRACE_INSTRUMENT();
	if (this->chunk!=NULL && this->deleteAtDestructor)
	{
//		if (this->chunk->getClass()==string("RecordingFileFrameChunk"))
//		{
//	#ifndef WIN32
//			cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" [~RecordingModuleDiskWriterEntry] DELETING ") << (this->chunk!=NULL?this->chunk->getClass()+string(" "):" ") << (void*)this->chunk << " " << ((RecordingFileFrameChunk*)this->chunk)->getFrame() << endl;
//			STACKTRACE_DUMP();
//	#endif	
//		}
		delete this->chunk;
		this->chunk=NULL;
	}
}

#pragma mark *** Metodes
fileOffset RecordingModuleDiskWriterEntry::getOffset()
{
	STACKTRACE_INSTRUMENT();
	return this->offset;
}

dword RecordingModuleDiskWriterEntry::getSize()
{
	STACKTRACE_INSTRUMENT();
	return this->size;
}

RecordingFileChunk *RecordingModuleDiskWriterEntry::getChunk()
{
	STACKTRACE_INSTRUMENT();
	return this->chunk;
}

void RecordingModuleDiskWriterEntry::setDeleteAtDestructor(bool del)
{
	STACKTRACE_INSTRUMENT();
	this->deleteAtDestructor=del;
}

bool RecordingModuleDiskWriterEntry::isDelayed()
{
	STACKTRACE_INSTRUMENT();
	return this->delayedWrite;
}

double RecordingModuleDiskWriterEntry::getAge()
{
	STACKTRACE_INSTRUMENT();
	TimerInstant ti = this->age.time();
	
	return ti.seconds();
}

