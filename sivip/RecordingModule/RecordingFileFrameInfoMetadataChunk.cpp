/*
 *  RecordingFileFrameInfoMetadataChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 26/05/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileFrameInfoMetadataChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#ifndef WIN32
#include <sys/time.h>
#else
#include <Utils/WindowsDefs.h>
#endif
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

RecordingFileFrameInfoMetadataChunk::RecordingFileFrameInfoMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileFrameInfoMetadataChunk::newChunk;
	className=getClass();
	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// La resta autoexplicatius (o no, pero poc important si ho son o no)
	chunkSize=sizeof(dword)+sizeof(byte)+className.length()+sizeof(nSubChunks)+chunkSizeCheck;

	fourcc=0;
	codecQuality=0;
	dword codecBitrate=0;
	x=0;
	y=0;
	bpp=0;
}

RecordingFileFrameInfoMetadataChunk::~RecordingFileFrameInfoMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileChunk* RecordingFileFrameInfoMetadataChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);

	// cout << "RecordingFileFrameInfoMetadataChunk::newChunk 1" << endl;
	
	RecordingFileFrameInfoMetadataChunk *resChunk=new RecordingFileFrameInfoMetadataChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

//	if (chunkClass!=resChunk->getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	// cout << "RecordingFileFrameInfoMetadataChunk::newChunk 1" << endl;
	resChunk->chunkSize=size;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	
	chunkBytes+=chunkTypeLen;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (resChunk->nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
		
	// cout << "RecordingFileFrameInfoMetadataChunk::newChunk 1" << endl;

	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (dataSize!=chunkSizeCheck)
		throw RecordingFileChunkException(0, resChunk->getClass()+string(": Invalid data size (")+
											StrUtils::decToString(dataSize)+string("/")+
											StrUtils::decToString(chunkSizeCheck)+string(")"));
	
	// cout << "RecordingFileFrameInfoMetadataChunk::newChunk 1" << endl;

	resChunk->fourcc=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->fourcc, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	// cout << "RecordingFileFrameInfoMetadataChunk::newChunk 1" << endl;

	resChunk->codecQuality=*((byte*)chunkBytes);
	chunkBytes+=sizeof(byte);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->codecBitrate=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->codecBitrate, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	resChunk->x=*((int*)chunkBytes);
	Endian::from(endian, &resChunk->x, sizeof(int));
	chunkBytes+=sizeof(int);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	resChunk->y=*((int*)chunkBytes);
	Endian::from(endian, &resChunk->y, sizeof(int));
	chunkBytes+=sizeof(int);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	resChunk->bpp=*((byte*)chunkBytes);
	chunkBytes+=sizeof(byte);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	// cout << "RecordingFileFrameInfoMetadataChunk::newChunk 1 - return" << endl;

	return (RecordingFileChunk*)resChunk;
}

void* RecordingFileFrameInfoMetadataChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	byte *data=new byte[chunkSize];
	byte *chunkBytes=data;
	
	if (data==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
		
	byte chunkTypeLen=getClass().length();
	*chunkBytes=chunkTypeLen;
	chunkBytes++;
	
	memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
	chunkBytes+=chunkTypeLen;
	
	if (nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
	*((dword*)chunkBytes)=nSubChunks;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	*((dword*)chunkBytes)=chunkSizeCheck;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((dword*)chunkBytes)=fourcc;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	*((byte*)chunkBytes)=codecQuality;
	chunkBytes+=sizeof(byte);
	
	*((dword*)chunkBytes)=codecBitrate;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	*((int*)chunkBytes)=x;
	Endian::to(dstEndian, chunkBytes, sizeof(int));
	chunkBytes+=sizeof(int);
	
	*((int*)chunkBytes)=y;
	Endian::to(dstEndian, chunkBytes, sizeof(int));
	chunkBytes+=sizeof(int);
	
	*((byte*)chunkBytes)=bpp;
	chunkBytes+=sizeof(byte);
	
	return data;
}

string RecordingFileFrameInfoMetadataChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileFrameInfoMetadataChunk";
	return c;
}

void RecordingFileFrameInfoMetadataChunk::setFourCC(dword fcc)
{
	STACKTRACE_INSTRUMENT();
	this->fourcc=fcc;
}

dword RecordingFileFrameInfoMetadataChunk::getFourCC()
{
	STACKTRACE_INSTRUMENT();
	return this->fourcc;
}

void RecordingFileFrameInfoMetadataChunk::setCodecQuality(byte q)
{
	STACKTRACE_INSTRUMENT();
	this->codecQuality=q;
}

byte RecordingFileFrameInfoMetadataChunk::getCodecQuality()
{
	STACKTRACE_INSTRUMENT();
	return this->codecQuality;
}

void RecordingFileFrameInfoMetadataChunk::setCodecBitrate(dword br)
{
	STACKTRACE_INSTRUMENT();
	this->codecBitrate=br;
}

dword RecordingFileFrameInfoMetadataChunk::getCodecBitrate()
{
	STACKTRACE_INSTRUMENT();
	return this->codecBitrate;
}

void RecordingFileFrameInfoMetadataChunk::setX(int x)
{
	STACKTRACE_INSTRUMENT();
	this->x=x;
}

int RecordingFileFrameInfoMetadataChunk::getX()
{
	STACKTRACE_INSTRUMENT();
	return this->x;
}

void RecordingFileFrameInfoMetadataChunk::setY(int y)
{
	STACKTRACE_INSTRUMENT();
	this->y=y;
}

int RecordingFileFrameInfoMetadataChunk::getY()
{
	STACKTRACE_INSTRUMENT();
	return this->y;
}

void RecordingFileFrameInfoMetadataChunk::setBPP(byte bpp)
{
	STACKTRACE_INSTRUMENT();
	this->bpp=bpp;
}

byte RecordingFileFrameInfoMetadataChunk::getBPP()
{
	STACKTRACE_INSTRUMENT();
	return this->bpp;
}

