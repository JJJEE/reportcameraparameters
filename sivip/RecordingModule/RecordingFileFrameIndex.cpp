#include <RecordingModule/RecordingFileFrameIndex.h>

#pragma mark *** RecordingFileFrameIndex -----------------------------

#pragma mark *** Constructores
RecordingFileFrameIndex::RecordingFileFrameIndex(
	RecordingModuleDiskWriter *writer) : writer(writer), file("")
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileFrameIndex::RecordingFileFrameIndex(
	RecordingModuleDiskWriter *writer, string file) : writer(writer), file(file)
{
	STACKTRACE_INSTRUMENT();
	// Llegim l'index del fitxer
	
	RecordingFile *rf = NULL;
	
	// Deixem que l'excepcio es propagui si salta...
	rf=writer->getRecordingFile(file, true);
	
	RecordingFileHeaderChunk *header=NULL;
		
	RecordingModuleDiskWriterEntryGrouper *fileEntries =
		writer->getEntriesForFile(file);
		
	if (fileEntries!=NULL)
	{
		fileEntries->rlock();
		
		list<RecordingModuleDiskWriterEntryGroup*> *groups = 
			fileEntries->getList();
		
		list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
		for (gIt=groups->begin(); gIt!=groups->end() && header==NULL; gIt++)
		{
			RecordingModuleDiskWriterEntryGroup *g=*gIt;

			// Mirem si conte la capçalera
			header = (RecordingFileHeaderChunk*)
				g->getChunkForOffset(RecordingFile::firstUsableOffset());
		}
		
		fileEntries->unlock();
	}
	
	if (header==NULL)
	{
		// No hem trobat la capçalera...
		dword hSize=0;
		void *hData=NULL;
		hData=rf->readBlock(&hSize, RecordingFile::firstUsableOffset());
		if (hData!=NULL)
		{
			header = (RecordingFileHeaderChunk*)
				RecordingFileChunk::newChunk(hData, hSize, rf->getEndian());
			delete [] (byte*)hData;
			hData=NULL;
		}
	}
	
	// Lectura d'indexos...
	fileOffset idxOff = header->getFirstIndexOffset();
	
	while (idxOff!=0)
	{
		RecordingFileIndexChunk *idx=NULL;

		// Llegir index
		// Mirem si esta carregat...
		if (fileEntries!=NULL)
		{
			fileEntries->rlock();
			
			list<RecordingModuleDiskWriterEntryGroup*> *groups = 
				fileEntries->getList();
			
			list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
			for (gIt=groups->begin(); gIt!=groups->end() && idx==NULL; gIt++)
			{
				RecordingModuleDiskWriterEntryGroup *g=*gIt;
	
				// Mirem si conte l'index
				idx = (RecordingFileIndexChunk*)
					g->getChunkForOffset(idxOff);
			}
			
			fileEntries->unlock();
		}

		if (idx==NULL)
		{
			// Lectura de fitxer :P
			dword iSize=0;
			void *iData=NULL;
			iData=rf->readBlock(&iSize, idxOff);
			if (iData!=NULL)
			{
				idx = (RecordingFileIndexChunk*)
					RecordingFileChunk::newChunk(iData, iSize, rf->getEndian());
				delete [] (byte*)iData;
				iData=NULL;
			}
		}
		
		if (idx!=NULL)
		{
			this->indexChunks.push_back(idx);
			this->indexChunksByOffset[idxOff]=idx;
			idxOff = idx->getNextIndexOffset();
		}
		else	// Si tenim algun error raro, no llegim mes indexos
			idxOff=0;
	}
	
	// Comprovar si la capçalera encara esta pendent d'escriure i si no ho
	// esta, delete
	bool writePending=false;
	
	if (fileEntries!=NULL)
	{
		RecordingFileHeaderChunk *header=NULL;
			
		fileEntries->rlock();
		
		list<RecordingModuleDiskWriterEntryGroup*> *groups = 
			fileEntries->getList();
		
		RecordingModuleDiskWriterEntry *entry;
		list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
		for (gIt=groups->begin(); gIt!=groups->end() && header==NULL && !writePending; gIt++)
		{
			RecordingModuleDiskWriterEntryGroup *g=*gIt;

			entry=g->getEntryForOffset(RecordingFile::firstUsableOffset());
				
			if (entry!=NULL)
				header = (RecordingFileHeaderChunk*)
					entry->getChunk();
			
			writePending=(entry!=NULL);
		}

		// La capçalera es gestiona des del DiskWriter, NO la marquem com
		// borrable		
//		if (writePending)
//			entry->setDeleteAtDestructor(true);
		
		fileEntries->unlock();
	}
	
	if (!writePending)
		delete header;
}

RecordingFileFrameIndex::~RecordingFileFrameIndex()
{
	STACKTRACE_INSTRUMENT();
	
	this->wlock();
	RecordingFileIndexChunk *ic=NULL;
	list<RecordingFileIndexChunk*>::iterator icIt=this->indexChunks.begin();
	
	while (icIt!=this->indexChunks.end())
	{
		ic=*icIt;
		if (ic!=NULL)
			delete ic;

		icIt=this->indexChunks.erase(icIt);
	}
		
	this->unlock();
}

#pragma mark *** Metodes
fileOffset RecordingFileFrameIndex::getChunkOffset(
	RecordingFileIndexChunk *chunk)
{
	STACKTRACE_INSTRUMENT();
	fileOffset off=-1;
	
	this->rlock();
	map<fileOffset, RecordingFileIndexChunk*>::iterator icIt;
	
	for (icIt=this->indexChunksByOffset.begin(); 
		icIt!=this->indexChunksByOffset.end(); icIt++)
	{
		if (icIt->second==chunk)
		{
			off=icIt->first;
			break;
		}
	}
		
	this->unlock();
	return off;
}

RecordingFileIndexChunk *RecordingFileFrameIndex::getChunkByOffset(fileOffset off)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileIndexChunk *ic=NULL;
	
	this->rlock();
	map<fileOffset, RecordingFileIndexChunk*>::iterator icIt=
		this->indexChunksByOffset.find(off);
		
	if (icIt==this->indexChunksByOffset.end())
	{
		this->unlock();
		return ic;
	}	

	ic=icIt->second;
	this->unlock();

	return ic;
}

RecordingFileIndexChunk *RecordingFileFrameIndex::getChunkByIndex(dword index)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileIndexChunk *ic=NULL;

	if (index>=this->indexChunks.size())
		return ic;
	
	this->rlock();
	list<RecordingFileIndexChunk*>::iterator icIt;
	
	dword i=0;
	for (icIt=this->indexChunks.begin();
		icIt!=this->indexChunks.end() && i<index; icIt++, i++);

	if (icIt!=this->indexChunks.end() && i==index)
		ic=*icIt;
		
	this->unlock();
	return ic;
}

RecordingFileIndexChunk *RecordingFileFrameIndex::getChunkByAbsoluteFrame(dword absFrame)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileIndexChunk *ic=NULL;

	this->rlock();
	list<RecordingFileIndexChunk*>::iterator icIt;
	
	dword i=0;
	for (icIt=this->indexChunks.begin(); icIt!=this->indexChunks.end(); icIt++)
	{
		ic=*icIt;
		if (absFrame>=ic->getFirstFrame() && 
			absFrame<ic->getFirstFrame()+ic->getIndexSize())
		{
			this->unlock();
			return ic;
		}
		
		ic=NULL;
	}

	this->unlock();
	return ic;
}

RecordingFileIndexChunk *RecordingFileFrameIndex::getChunkByNearestDate(double seconds, dword *nearestFrAbsIndex)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileIndexChunk *ic=NULL;
	RecordingFileIndexChunk *nearestIC=NULL;
	qword nearestFrSecs=0;
	word nearestFrMillis=0;
	double nearestDif=99999999.0;

	this->rlock();
	list<RecordingFileIndexChunk*>::iterator icIt;
	
	dword i=0;
	for (icIt=this->indexChunks.begin(); icIt!=this->indexChunks.end(); icIt++)
	{
		ic=*icIt;
		
		dword firstFrame = ic->getFirstFrame();
		dword nFrames = ic->getIndexSize();
		dword lastFrame = firstFrame + nFrames - 1;

		RecordingFileIndexChunk::indexEntry *lfEnt=ic->getFrame(lastFrame);
		double lfSecs = ((double)lfEnt->date.secs) + 
			((double)lfEnt->date.millis)/1000.0;

		// Fem una primera comprovacio amb l'ultim frame de l'index,
		// per descartatge rapid		
		if (lfSecs!=0.0 && lfSecs<seconds)
		{
			*nearestFrAbsIndex=lastFrame;
			nearestFrSecs=lfEnt->date.secs;
			nearestFrMillis=lfEnt->date.millis;
			nearestDif = ABS(seconds - lfSecs);
			nearestIC=ic;
			continue;
		}
	
		RecordingFileIndexChunk::indexEntry *ffEnt=ic->getFrame(firstFrame);
		double ffSecs = ((double)ffEnt->date.secs) + 
			((double)ffEnt->date.millis)/1000.0;

		// Salt d'index, per no analitzar-lo sencer...
		// Ja tenim el frame mes proper.
		if (ffSecs!=0.0 && ABS(ffSecs-seconds) > nearestDif)
			continue;

		double frSecs=1.0;
		double dif=0.0;
		for (dword f=0; f<nFrames && frSecs!=0.0; f++)
		{
			dword absFr=firstFrame+f;
			RecordingFileIndexChunk::indexEntry *entry=ic->getFrame(absFr);
			
			frSecs = ((double)entry->date.secs) + 
				((double)entry->date.millis)/1000.0;
			
			dif=ABS(frSecs-seconds);
			
			if (dif<nearestDif)
			{
				*nearestFrAbsIndex=absFr;
				nearestFrSecs=entry->date.secs;
				nearestFrMillis=entry->date.millis;
				nearestDif=dif;
				nearestIC=ic;
			}
		}
	}

	this->unlock();
	return nearestIC;
}

RecordingFileIndexChunk *RecordingFileFrameIndex::linkNewIndexChunk(fileOffset offset)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileIndexChunk *ic=NULL;
	RecordingFileIndexChunk *maxIC=NULL;
	dword firstFrame=0;
	fileOffset maxICOffset=0;

	this->wlock();

	if (this->indexChunks.empty())
	{
		// Si no tenim chunks, no es responsabilitat nostra enllaçar un
		// nou index, caldra crear la capçalera o si mes no enllaçar alla
		// l'index
		this->unlock();
		return NULL;
	}
	
	map <fileOffset, RecordingFileIndexChunk*>::iterator icIt;
	
	dword i=0;
	for (icIt=this->indexChunksByOffset.begin();
		icIt!=this->indexChunksByOffset.end(); icIt++)
	{
		ic=icIt->second;
		if (ic->getFirstFrame()>=firstFrame)
		{
			maxIC=ic;
			maxICOffset=icIt->first;
			firstFrame=ic->getFirstFrame();
		}
		
		ic=NULL;
	}

	// maxIC no pot ser NULL, pq hem comprovat que tenim indexos
	ic=new RecordingFileIndexChunk();
	ic->setFirstFrame(maxIC->getFirstFrame() + maxIC->getIndexSize());
	ic->setNextIndexOffset(0);	// Assegurem :)
	maxIC->setNextIndexOffset(offset);
	
	this->indexChunks.push_back(ic);
	this->indexChunksByOffset[offset]=ic;

	this->unlock();

	// Enviem a escriure maxIC i ic
	// Els enviem com a delayed i com a no borrables
	RecordingModuleDiskWriterEntry *entry =
		new RecordingModuleDiskWriterEntry(offset, ic->size(), ic,
			true, false);
	writer->addEntryToFile(this->file, entry);
	
	entry = new RecordingModuleDiskWriterEntry(maxICOffset, maxIC->size(),
		maxIC, true, false);
	writer->addEntryToFile(this->file, entry);
	
	return ic;
}

RecordingFileIndexChunk *RecordingFileFrameIndex::linkFirstIndexChunk(
	fileOffset hdrOffset, dword reservedSize, dword fourcc,
	RecordingFileHeaderChunk::recType recType, word fpswhole, word fpsfrac)
{
	STACKTRACE_INSTRUMENT();

	//deixem que les possibles excepcions de rfhc.*Header* tirin cap a recModInt
	//i ja les tractara (reintentarà)
	RecordingFileHeaderChunk *rfhc = writer->getHeaderForFile(this->file);

	if (rfhc==NULL)
		rfhc=writer->createHeaderForFile(this->file, hdrOffset, reservedSize,
			fourcc, recType, fpswhole, fpsfrac);

	RecordingFileIndexChunk *ic=NULL;

	fileOffset indexOffset = hdrOffset + reservedSize;

	this->wlock();
	try
	{
		ic=new RecordingFileIndexChunk();
		ic->setFirstFrame(0);
		ic->setNextIndexOffset(0);	// Assegurem :)
		rfhc->setFirstIndexOffset(indexOffset);
	
		this->indexChunks.push_back(ic);
		this->indexChunksByOffset[indexOffset]=ic;
	}
	catch(...)
	{
		this->unlock();
		throw;
	}

	this->unlock();

	// Enviem a escriure com a delayed i com a no borrable
	RecordingModuleDiskWriterEntry *entry =
		new RecordingModuleDiskWriterEntry(indexOffset, ic->size(), ic,
		true, false);
	writer->addEntryToFile(this->file, entry);
	
	// Enviem a escriure la capçalera, no delayed i no borrable... ja s'esborra
	// des de DiskWriter
	entry =
		new RecordingModuleDiskWriterEntry(RecordingFile::firstUsableOffset(),
		rfhc->size(), rfhc, false, false);
	writer->addEntryToFile(this->file, entry);
	
	return ic;
}

list<RecordingFileIndexChunk*> *RecordingFileFrameIndex::getIndexChunkList()
{
	STACKTRACE_INSTRUMENT();
	return &this->indexChunks;
}

dword RecordingFileFrameIndex::getLastFrameIndex()
{
	STACKTRACE_INSTRUMENT();
	RecordingFileIndexChunk *ic=NULL;
	RecordingFileIndexChunk *maxIC=NULL;
	dword frIdx=0;
	fileOffset maxICOffset=0;
	dword lastFrAbsIndex=0;
	qword lastFrSecs=0;
	word lastFrMillis=0;

	this->rlock();

	if (this->indexChunks.empty())
	{
		// Si no tenim chunks, l'ultim frame es el "-1"
		this->unlock();
		return 0xffffffff;
	}
	
	map <fileOffset, RecordingFileIndexChunk*>::iterator icIt;
	
	for (icIt=this->indexChunksByOffset.begin();
		icIt!=this->indexChunksByOffset.end(); icIt++)
	{
		ic=icIt->second;
		if (ic->getFirstFrame()>=frIdx)
		{
			maxIC=ic;
			frIdx=ic->getFirstFrame();
		}
		
		ic=NULL;
	}

	this->unlock();
	
	dword nf=maxIC->getIndexSize();
	lastFrAbsIndex=frIdx;
	
	for (dword f=0; f<nf; f++)
	{
		RecordingFileIndexChunk::indexEntry *entry = 
			maxIC->getFrame(frIdx + f);

		if (entry->date.secs > lastFrSecs ||
			(entry->date.secs==lastFrSecs && entry->date.millis>lastFrMillis))
		{
			lastFrAbsIndex=frIdx + f;
			lastFrSecs=entry->date.secs;
			lastFrMillis=entry->date.millis;
		}
	}
	
	return lastFrAbsIndex;
}

fileOffset RecordingFileFrameIndex::getAbsoluteFrameOffset(dword absFrame)
{
	STACKTRACE_INSTRUMENT();
	fileOffset off=0;
	RecordingFileIndexChunk *ic=this->getChunkByAbsoluteFrame(absFrame);
	
	if (ic!=NULL)
	{
		RecordingFileIndexChunk::indexEntry *ent=ic->getFrame(absFrame);
		off=ent->frameOffset;
	}
	
	return off;
}

void RecordingFileFrameIndex::setFile(string file)
{
	STACKTRACE_INSTRUMENT();
	this->file=file;
}

void RecordingFileFrameIndex::wlock()
{
	STACKTRACE_INSTRUMENT();
	this->indexChunksRW.wlock();
}

void RecordingFileFrameIndex::rlock()
{
	STACKTRACE_INSTRUMENT();
	this->indexChunksRW.rlock();
}

void RecordingFileFrameIndex::unlock()
{
	STACKTRACE_INSTRUMENT();
	this->indexChunksRW.unlock();
}

