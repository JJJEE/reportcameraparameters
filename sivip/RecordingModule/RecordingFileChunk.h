/*
 *  RecordingFileChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 19/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Endian/Endian.h>
#include <Threads/Mutex.h>
#include <map>
#include <string>

using namespace std;

typedef void* (*newChunkFunction)(const void*, dword, Endian::endianType);

class RecordingFileChunk
{
protected:
    // Mutex per modificacions estructurals thread safe
    Mutex modMutex;
    
	// Tamany del chunk
	dword chunkSize;
	// Classe del chunk
	string className;
	// Numero de subchunks del chunk actual
	dword nSubChunks;
	RecordingFileChunk **subchunks;
	
	// Endian del chunk a memoria
	Endian::endianType chunkEndian;
	
	static map <string,newChunkFunction> newChunkFunctions;

public:
	RecordingFileChunk();
	virtual ~RecordingFileChunk();

	static bool initedChunks;
	static void initChunks();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass() = 0;
		
	virtual dword size();
	
	virtual dword getNSubChunks();

	virtual Endian::endianType getEndian();
	virtual void setEndian(Endian::endianType end);
	
	virtual void addSubChunk(RecordingFileChunk *c);
	virtual RecordingFileChunk* getSubChunk(dword idx);
	virtual void delSubChunk(RecordingFileChunk *c);
	virtual void delSubChunk(dword idx);
	// Canvia el subchunk numero N del mateix tipus del que ens passen pel q ens passen, o append si N es massa gran
	virtual RecordingFileChunk* replaceSubChunk(dword n, RecordingFileChunk *c, bool deleteOld=true);
};

