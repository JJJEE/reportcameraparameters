/*
 *  RecordingModuleSessionAlreadyStablishedException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <RecordingModule/RecordingModuleException.h>

using namespace std;

class RecordingModuleSessionAlreadyStablishedException : public Exception
{
 public:
	RecordingModuleSessionAlreadyStablishedException(string msg, int code=0);
	RecordingModuleSessionAlreadyStablishedException(int code, string msg);
	RecordingModuleSessionAlreadyStablishedException(SerializedException &se);

	~RecordingModuleSessionAlreadyStablishedException();
	
	virtual string getClass();
};

