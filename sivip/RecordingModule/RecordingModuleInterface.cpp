/*
 *  RecordingModuleInterface.cpp
 *  
 *
 *  Created by David Marí Larrosa on 27/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingModuleInterface.h>
#include <RecordingModule/RecordingModuleException.h>
#include <RecordingModule/RecordingModuleSessionAlreadyStablishedException.h>
#include <RecordingModule/RecordingModuleSessionNotStablishedException.h>
#include <RecordingModule/MaxActiveRecordingsReachedException.h>
#include <RecordingModule/RecordingModuleDiskWriter.h>
#include <RecordingModule/RecordingFileFrameIndices.h>
#include <RecordingModule/RecordingFileException.h>
#include <DecodeModule/DecodeModule.h>
#include <ModuleAccess/RecordingModuleSchedulerAccess.h>
#include <Plugins/GestorImageTypes.h>
#include <Plugins/GestorImageExceptions.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/SerializedException.h>
#include <Utils/RedirectCallException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <Utils/RPC.h>
#include <Utils/Timer.h>
#include <Utils/CheckPoint.h>
#include <Utils/Log.h>
#include <Plugins/GestorControlTypes.h>
#include <SessionManager/SessionManager>
#include <Exceptions/NullPointerException.h>
#include <Exceptions/IdNotFoundException.h>
#include <Exceptions/IdAlreadyInUseException.h>
#include <Exceptions/CameraNotConnectableException.h>
#ifndef WIN32
#include <unistd.h>
#include <dirent.h>
#else
#include <Utils/WindowsDefs.h>
#endif
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <iomanip>
#include <errno.h>
//#include <RecordingModule/RecordingModuleSchedulingThread.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define __STOP_SERVICE_ON_ALERT 1

using namespace std;

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

//RecordingModuleSchedulingThread *sched;

// ***
// *** RecordingModuleAwarenessThread
// ***
#pragma mark *** RecordingModuleAwarenessThread


class RecordingModuleAwarenessThread : public Thread
{
public:

protected:
	RecordingModuleInterface *intf;
	Canis *cn;
	bool run;
	Mutex stopMutex;
	
public:
	RecordingModuleAwarenessThread(RecordingModuleInterface *intf, Canis *cn);
	~RecordingModuleAwarenessThread();

	void stopRun();
	virtual void* execute(int id, void* params);
};

RecordingModuleAwarenessThread::RecordingModuleAwarenessThread(RecordingModuleInterface *intf, Canis *cn) : intf(intf), cn(cn)
{
	STACKTRACE_INSTRUMENT();
}

RecordingModuleAwarenessThread::~RecordingModuleAwarenessThread()
{
	STACKTRACE_INSTRUMENT();
}
	
void RecordingModuleAwarenessThread::stopRun()
{
	stopMutex.lock();
	this->run=false;
	stopMutex.unlock();
}

void* RecordingModuleAwarenessThread::execute(int id, void* params)
{
	STACKTRACE_INSTRUMENT();

	list<Canis::subsys> recmods;
	run=true;
	byte *oldParams=NULL;
	
	sleep(60);
	while (run)
	{ 
		RecordingModuleInterface::session *save=NULL;
		string addrs=string("");

		cout<<"RMAT::execute create iterator"<<endl;
		SessionManager<RecordingModuleInterface::session>::Iterator it=SessionManager<RecordingModuleInterface::session>::iterator();
		while (it.hasNext())
		{
			cout<<".";
			cout.flush();
			RecordingModuleInterface::session *s=it.next();

			if (s->toDB && !s->inDB )	//(&& (s->startRecStreamRecv || s->startedRecording)) // en el fons, es igual de greu tenir una sessió insertada quant no toca... :P
			{
				save = s;
				addrs = it.getLastId();

				cout<<"RMAT::execute retry "<<addrs<<endl;
				break; //while(it.hasNext)  --> tením un rlock del sessionManager, alliberém, insertém i ja tornarém a comprobar :P 
			}
			SessionManager<RecordingModuleInterface::session>::releaseFromIterator(it.getLastId());
		}
		cout<<"RMAT::execute release iterator"<<endl;
		SessionManager<RecordingModuleInterface::session>::releaseIterator();

		if(save != NULL)
		{
			int pos=addrs.find(":");
			if(pos!=string::npos)
			{
				string ip = addrs.substr(0, pos);
				int port = atoi(addrs.substr(pos+1).c_str());

				Address sesAddr(IP(ip), port);

				cout<<"RMAT :: retrying sessionToDB ses:"<<sesAddr.toString()<<" rmaddr:" << (save->rmAddr==NULL?string("NULL"):save->rmAddr->toString()) << 
					" for dev:"<< save->sessionParams.devId <<" startedRec:"<< save->startedRecording <<
					" started type:"<< save->startRecStreamRecv <<" type:"<< save->sessionParams.recType <<endl;
				try
				{
					RecordingModuleInterface::sessionToDB(&sesAddr, intf->address, save);//(getAddress()rmi->address);
				}catch(Exception &e)
				{
					cout<<" RMAT::Exception while retrying sessionToDB: "<<e.getClass()<<"::"<<e.getMsg()<<endl;
					sleep(10);	//per si es queda donant el mateix error constantment, com a minim que no toqui tant els ous
				}
				catch(std::exception &stde)
				{
					cout<<" RMAT::std::exception while retrying sessionToDB: "<< stde.what() << endl;
					sleep(10);
				}catch (...)
				{
					cout<<" RMAT::unknown exception while retrying sessionToDB: "<< endl;
					sleep(10);
				}
				cout<<"RMAT :: retry sessionToDB ses:"<<sesAddr.toString()<<" inDB:" <<save->inDB<<endl; 
			}
			else
			{
				cout<<"RMAT :: invalid session key from sessionManager : "<<addrs<<endl; 
			}
			
			try
			{
				SessionManager<RecordingModuleInterface::session>::release(save);//(addrs);
			}catch(Exception &e)
			{
				cout<<" RMAT::Exception on SessionManager::release: "<<e.getClass()<<"::"<<e.getMsg()<<endl;
			}
			catch(std::exception &stde)
			{
				cout<<" RMAT::Exception on SessionManager::release: "<<stde.what()<<endl;
			}
		}
		else
		{
			sleep(60);
#ifdef __STOP_SERVICE_ON_ALERT 
			if(intf->reassignPending)
			{
				RecordingModuleInterface::camsAlertLock.lock();
				TimerInstant ti = intf->reassignRequestTime.time();
				if(ti.seconds() > RecordingModuleInterface::reassignWaitSecs)
				{
					cout << StrUtils::getDateString() <<" RecordingModuleInterface: Recovered from too many cams in FullFactor, asking for reassign"<<endl;
	
					try
					{
						RecordingModuleSchedulerAccess rmsa(cn);
						rmsa.checkRecordingServers();
						intf->reassignPending=false;
					}catch(Exception &e)
					{
						cout << "RecordingModuleInterface: RecordingModuleScheduler Exception:" << e.getClass() << ":" << e.getMsg() << endl;

					}
				
				}
				RecordingModuleInterface::camsAlertLock.unlock();
			}
#endif
		}
	}

/*	cout<<"RMAT::execute start"<<endl;
	while (run)
	{
		stopMutex.lock();
		try
		{
			// Obtenim el llistat complet de RecordingModules, per saber
			// que hi son
	cout<<"RMAT::execute sf::GAllSubS"<<endl;
			recmods=ServiceFinder::getAllSubsystems("RecordingModule", cn, false, false);
			
			// I inserim a la BD el nostre llistat de gravacions, cada RecMod
			// fara el propi
			
			RecordingModuleInterface::listRecordingsParams lrp;
			byte *lrpData=(byte*)lrp.toNetwork();
	cout<<"RMAT::execute RMI::listRecs"<<endl;
			RPCPacket *res=RecordingModuleInterface::listRecordings(intf, NULL, lrpData);

			delete [] lrpData;
			
			lrp.toLocal(res->getData());
			
			RecordingModuleInterface::listRecordingsParams old;
			if (oldParams!=NULL)
			{
				old.toLocal(oldParams);
				delete [] oldParams;
				oldParams=NULL;
			}
			oldParams=(byte*)lrp.toNetwork();
				
			delete res;
				
			// Posem a la BD
	cout<<"RMAT::execute lrp.toDB"<<endl;
			lrp.diffToDB(intf->getAddress(), &old);
		}
		catch (Exception &e)
		{
	//		stopMutex.unlock();
			cout << "RecordingModuleAwarenessThread: " << e.getClass() << ": " << e.getMsg() << endl;
			
			if (oldParams!=NULL)
			{
				delete [] oldParams;
				oldParams=NULL;
			}
		}
		stopMutex.unlock();
			
	cout<<"RMAT::execute sleep"<<endl;
		sleep(5);
	}	

	if (oldParams!=NULL)
	{
		delete [] oldParams;
		oldParams=NULL;
	}

*/
	return NULL;
}

class RecordingModuleAlarmAndLogThread : public Thread
{
public:
	class event
	{
	public:
		int devId, cause;
		struct timeval logDate;
		string msg;
		bool raise;
		int timeout;
		Timer t;
		event(int dev, int cause, struct timeval logDate, string msg, bool raise): devId(dev), cause(cause), logDate(logDate), msg(msg), raise(raise)
		{};
		event(int dev, int cause, string msg, bool raise): devId(dev), cause(cause), msg(msg), raise(raise)
		{
			logDate.tv_sec = 0;
		};
	};

	class lowerAlarm
	{
	public:
		string almId;
		int timeout;
		Timer t;
		lowerAlarm(string almId, int timeout):almId(almId), timeout(timeout)
		{
			t.start();
		};
	};
	static const int rec_lost = 0;
//	static const int rec_recovered = 1;
	static const int rec_timeout= 1;
	static const int rec_limit_warning= 2;
	static const int rec_limit_reached= 3;
	static const int rec_time_warning = 4;

protected:
	RecordingModuleInterface *rmi;
	bool run;
	list<event> pending;
	list<lowerAlarm> lower;
	Mutex dataLock;
	
public:
	RecordingModuleAlarmAndLogThread(RecordingModuleInterface *rmi);

	void stopRun();
	void queue(event);
	void queueLower(lowerAlarm);
	virtual void* execute(int id, void* params);
};

RecordingModuleAlarmAndLogThread::RecordingModuleAlarmAndLogThread(RecordingModuleInterface *rmi):rmi(rmi), run(true)
{
}

void RecordingModuleAlarmAndLogThread::queue(event evt)
{
	dataLock.lock();
	try
	{
		pending.push_back(evt);	
	}catch(std::exception &stde)
	{
		cout<<"RMAALT::queue std::exception: "<<stde.what()<<endl;
	}
	dataLock.unlock();
}

void RecordingModuleAlarmAndLogThread::queueLower(lowerAlarm la)
{
	dataLock.lock();
	try
	{
		lower.push_back(la);	
	}catch(std::exception &stde)
	{
		cout<<"RMAALT::queue std::exception: "<<stde.what()<<endl;
	}
	dataLock.unlock();
}


void RecordingModuleAlarmAndLogThread::stopRun()
{
	dataLock.lock();
	run = false;
	dataLock.unlock();
}

void* RecordingModuleAlarmAndLogThread::execute(int, void*)
{
	this->run=true;
//	cout << "RecordingModuleAlarmAndLogThread::execute: "
//		"Creating AlarmModuleAccess, run: " << run << endl;
	AlarmModuleAccess *ama=new AlarmModuleAccess(rmi->address, rmi->type, rmi->cn);

	dataLock.lock();
	while(run)
	{
		cout << "RecordingModuleAlarmAndLogThread::execute: "
			"iteration";
		try
		{
			while(pending.size() > 0)
			{
				event evt = pending.front();
				pending.pop_front();
				dataLock.unlock();	
				string msg("");	
				string extMsg("");
				if(evt.msg.length()>1)
					extMsg = "."+evt.msg;
				//string dbmsg("");	
				string date("");
				bool raise=evt.raise;//true;
				switch(evt.cause)
				{
					//case rec_recovered:
					//	raise = false;
					case rec_lost:
						msg = string("rec_lost.")+StrUtils::decToString(evt.devId);
						extMsg = string("rec_lost.")+StrUtils::decToString(evt.devId)+string(".")+evt.msg;
					//	cout<<"RecordingModuleAlarmAndLogThread:: recording lost:"<<msg<<endl;
						break;
					//case rec_recovered:
					//	msg = string("rec_recover");
					//	cout<<"RecordingModuleAlarmAndLogThread:: recording recovered:"<<msg<<endl;
					//	break;

					case rec_timeout:
						msg = string("rec_timeout.")+StrUtils::decToString(evt.devId);
						extMsg = string("rec_timeout.")+StrUtils::decToString(evt.devId)+string(".")+evt.msg;
					//	cout<<"RecordingModuleAlarmAndLogThread:: timeout, raising:"<<msg<<endl;
						break;
					//case rec_limit_warning_lower:
					//	raise = false;
					case rec_limit_warning:
						msg = string("rec_limit_warning");
						extMsg = msg + extMsg;
					//	cout<<"RecordingModuleAlarmAndLogThread:: rec_limit_warning, raising:"<<msg<<endl;
						break;
					case rec_limit_reached:
						msg = string("rec_limit_reached");
						extMsg = msg + extMsg;
					//	cout<<"RecordingModuleAlarmAndLogThread:: rec_limit_reached, raising:"<<msg<<endl;
						break;
					//case rec_time_warning_lower:
					//	raise = false;
					case rec_time_warning:
						msg = string("rec_time_warning");
						extMsg = msg + extMsg;
					//	cout<<"RecordingModuleAlarmAndLogThread:: rec_time_warning, raising:"<<msg<<endl;
						break;
				}
				
				string almId = string("ModuleAlarm.RecordingModule.")+msg+string(":")+
						rmi->getAddress().getIP().toString()+string("|")+StrUtils::decToString(rmi->getAddress().getPort())+string(":");
				cout<<"RecordingModuleAlarmAndLogThread::raising:"<<almId+evt.msg<<endl;
				
				rmi->raiseSystemAlarm(ama, almId+extMsg, -1, raise);

				dataLock.lock();
				if(evt.cause == rec_timeout)
				{
					lower.push_back(lowerAlarm(almId, 3600)); //la baixem al cap de, per exemple, 1h 
				}
			}
		}catch(Exception &e)
		{
			cout<<"RMAALT::execute Exception: "<<e.getClass()<<":"<<e.getMsg()<<endl;
		}catch(std::exception &stde)
		{
			cout<<"RMAALT::execute std::exception: "<<stde.what()<<endl;
		}
		
		while(pending.size() == 0)
		{
//			cout<<"RecordingModuleAlarmAndLogThread:: no alarm"<<endl;
			bool lowered=false;
			if(lower.size()>0)
			{
				list<lowerAlarm>::iterator i;
				i=lower.begin();
				while( i != lower.end())
				{
					lowerAlarm la = *i;
					TimerInstant ti=la.t.time();
					if(ti.seconds()>= la.timeout)
					{
						lowered=true;
						i=lower.erase(i);
						dataLock.unlock();	
						rmi->raiseSystemAlarm(ama, la.almId, -1, false);
						dataLock.lock();
					}
					else
						i++;
				}
			}
			if(!lowered)
			{
				dataLock.unlock();
				sleep(1);
				dataLock.lock();
			}
		}
	}
	dataLock.unlock();
	return NULL;
}

// ***
// *** RecordingModuleInterfaceSaveStreamThread
// ***
#pragma mark *** RecordingModuleInterfaceSaveStreamThread

int RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount=0;
bool RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::warnActiveRaised=false;
bool RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::almActiveRaised=false;
Mutex RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock(true);

RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::RecordingModuleInterfaceSaveStreamThread(float fps, int devId,
	RecordingModuleInterface *intf, Address sessionAddr) : /*decodeModuleLocation(NULL),*/ decodeModule(NULL), stop(false), fps(fps), running(false), 
	devId(devId), intf(intf), sessionAddr(sessionAddr), lockMutex(new Mutex()), sesAddrMutex(new Mutex()), alertCount(0), inAlert(false), recLost(true)
{
	STACKTRACE_INSTRUMENT();

//	Address rmAddr=intf->getAddress();
//	short rmType=intf->getType();
	
	//alarmModule = new AlarmModuleAccess(rmAddr, rmType, intf->getCanis());
	//alarmManager = new AlarmManager(rmAddr.getIP());
	//alarmModule->setManager(alarmManager);

	// Iniciem una sessio amb el decodeModule

//	AMDeviceID AMDevId;
//	AMDevId.id=devId;

//	try
//	{
//		cout<<"alarmModule.startSession "<<endl;
		//alarmModule->startSession(AMDevId);	
//		cout<<"alarmModule.startSession started "<<endl;
//	}
//	catch(Exception &e)
//	{
//		cout<<"alarmModule.startSession exc: "<<e.getClass()<<"::"<<e.getMsg()<<endl;
//	}
//	deviceId.toNetworkEndian();
//	
//	RPCPacket *pk=new RPCPacket(rmAddr, DecodeModuleInterface::startSessionServiceId, (byte*)&deviceId,
//				sizeof(deviceId), rmType, decodeModuleLocation->origen);
//
//	RPCPacket *startSession=NULL;
//	try
//	{
//		startSession=decodeModule->call(*pk);
//	}
//	catch (Exception &e)
//	{
//		cout << e.getClass() << ": " << e.getMsg() << " (decodeModule.startSession)" << endl;
//		throw e;
//	}
//	
//	if (startSession==NULL)
//		throw RecordingModuleException(0, "Not enough memory to start session with decode module for recording");
//	
//	delete pk;
//	delete startSession;
}

RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::~RecordingModuleInterfaceSaveStreamThread()
{
	STACKTRACE_INSTRUMENT();
	this->stop=true;
	
//	cout << "--> RecordingModuleInterface delete thread join " << endl;
//	this->join();
	this->stopCond.lock();
	while(!this->stopped)
		this->stopCond.wait();
	this->stopCond.unlock();

	// Acabem la sessio amb el decodeModule

	cout << "--> RecordingModuleInterface end Decode" << endl;
	if (decodeModule!=NULL)
	{
		IPDeviceID IPDevId(this->devId);

		try
		{
			decodeModule->endSession(IPDevId);
		}
		catch(Exception &e)
		{
			cout<<"RMISaveStreamThread::~RMISaveStreamThread(): DMEndSes exc: "
				<< e.getClass() << ": " << e.getMsg() << endl;
		}

		delete decodeModule;
		decodeModule=NULL;
	}		

	delete lockMutex;
	delete sesAddrMutex;
	cout << "--> RecordingModuleInterface end " << endl;

//	cout << "--> RecordingModuleInterface end Alarm" << endl;

/*	if(alarmModule!=NULL)
	{
		try{
			AMDeviceID AMDevId;
			AMDevId.id=devId;

//			alarmModule->endSession(AMDevId);
		}
		catch(Exception &e)
		{
			cout<<"alarmModule.endSession exc: "<<e.getClass()<<"::"<<e.getMsg()<<endl;
		}

		delete alarmModule;
	}	
*/
//	if (decodeModuleLocation!=NULL)
//		delete decodeModuleLocation;
}

void RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::stopThread()
{
	STACKTRACE_INSTRUMENT();
	this->stop=true;
	
//	cout << "--> RecordingModuleInterface::RMISST::stop delete thread join " << endl;
//	this->join();
	this->stopCond.lock();
	while(!this->stopped)
		this->stopCond.wait();
	this->stopCond.unlock();
	// Acabem la sessio amb el decodeModule

	cout << "--> RecordingModuleInterface::RMISST::stop end Decode" << endl;
	if (decodeModule!=NULL)
	{
		IPDeviceID IPDevId(this->devId);

		try
		{
			decodeModule->endSession(IPDevId);
		}
		catch(Exception &e)
		{
			cout<<"RMISaveStreamThread::RMISST::stop: DMEndSes exc: "
				<< e.getClass() << ": " << e.getMsg() << endl;
		}

		delete decodeModule;
		decodeModule=NULL;
	}		
	cout << "--> RecordingModuleInterface::RMISST::stop end " << endl;
}

void RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::clearStop()
{
	this->stop=false;
}

void RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::setSessionAddr(Address sessAddr)
{
	if (this->sesAddrMutex->isHeldByCurrentThread())
	{
		cout<<"RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::setSessionAddr LOCKED id:"<<devId<<" from:"<<sessionAddr.toString()<<" to:"<<sessAddr.toString()<<" in thread:"<<(void*)this<< endl;	
		sessionAddr=sessAddr;
	}
	else
	{
		this->sesAddrMutex->lock();
		cout<<"RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::setSessionAddr id:"<<devId<<" from:"<<sessionAddr.toString()<<" to:"<<sessAddr.toString()<<" in thread:"<<(void*)this<< endl;	
		sessionAddr=sessAddr;
		this->sesAddrMutex->unlock();
	}
}

float RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::getFPS()
{
	STACKTRACE_INSTRUMENT();
	return this->fps;
}

void RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::setFPS(float fps)
{
	STACKTRACE_INSTRUMENT();
	cout << "RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::setFPS: " << fps << endl;
	this->fps=fps;
}

void RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::lockSessionAddress()
{
	this->sesAddrMutex->lock();
}

void RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::unlockSessionAddress()
{
	this->sesAddrMutex->unlock();
}

void* RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::execute(int id, void* params)
{
	STACKTRACE_INSTRUMENT();
	INIT_CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
	Timer sfdTimer;
	TimerInstant sfdInstant;
#endif
	
	// Comptem l'stream :D
RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.lock();
RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount++;
	cout << "RMISST: activeCount++: " << RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount << endl;	RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.unlock();
	
	int fullFactorWarningCount=0;
#ifdef __STOP_SERVICE_ON_ALERT 
	if(this->inAlert) 
	{
		RecordingModuleInterface::camsAlertLock.lock();
		cout << StrUtils::getDateString() << " RecordingModuleInterfaceSaveStreamThread::execute WARNING: thread started while device "<< this->devId << " inAlert==true"<<endl;
		camsInAlert.remove(this->devId);
		this->inAlert = false;
		this->intf->checkFullFactorShutdown();
		RecordingModuleInterface::camsAlertLock.unlock();
	}
#endif
	
	running=true;
	bool streamed = false;
	this->stopped=false;
	
	dword debugFrameLength=0, debugFrameChunkLength=0, debugFrameChunkSerializationSize=0;
	
	cout << "RecordingModuleInterfaceSaveStreamThread::execute: Starting recording for device " << std::dec << devId << endl;

#ifdef REC_SAVEFRAME_DEBUG
	sfdTimer.start();

	if (sfdDebug!=NULL)
		cout << "RecordingModuleInterfaceSaveStreamThread[" << std::dec << devId <<"]: Debugging, got debug pointer: " << (void*)sfdDebug << endl;
	else
		cout << "RecordingModuleInterfaceSaveStreamThread[" << std::dec << devId <<"]: NOT Debugging, did not get pointer." << endl;
#endif

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

	try
	{
//		cout << "[RMISST] ck 1 devId: " << devId << endl;
		// Establim els FPS del ImagePlugin
		float framesPerSecond=fps;

		if (framesPerSecond==0.0f)
		{
			framesPerSecond=25.0f;
			cout << "RecordingModuleInterfaceSaveStreamThread[" << std::dec << devId <<"]: Recording at 0 FPS. Forcing " << framesPerSecond << " FPS" << endl;
		}

		IPFramesPerSecond ipfps((double)framesPerSecond);

		cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] RecordingModuleInterfaceSaveStreamThread[" << std::dec << devId <<"]: setFPS: " << ipfps.whole << endl;//".";
		//	cout.width(3);
		//	cout.fill('0');
		//	cout << ipfps.frac << endl; 
		//	cout.width(0);

//		cout << "[RMISST] ck 2 devId: " << devId << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

		CHECKPOINT();
		if (decodeModule==NULL)
		{
			decodeModule=new DecodeModuleAccess(devId, intf->getCanis()->getInterfaceMask(), intf->getCanis());
			if (decodeModule==NULL)
				throw RecordingModuleException(0, "Not enough memory to start session with decode module for recording");
		}
		CHECKPOINT();
		// Iniciem una sessio amb el decodeModule
		IPDeviceID IPDevId(devId);
		try
		{
			decodeModule->startSession(IPDevId);
		}
		catch(IPSessionAlreadyStablishedException &sae)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: " << 
				sae.getClass() << ": " << sae.getMsg() << CHECKPOINT_TOSTRING() << " (ignoring)" << endl;
		}
		catch(Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: " << 
				e.getClass() << ": " << e.getMsg() << CHECKPOINT_TOSTRING() << endl;
			if (decodeModule!=NULL)
			{
				delete decodeModule;
				decodeModule=NULL;
			}
			//e.serialize()->materializeAndThrow(true);
			throw;
		}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

		CHECKPOINT();
		try
		{
			// posem el decode module en streaming mode, per comoditat :)
			decodeModule->setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));	
			decodeModule->setFPS(ipfps);
		}
		catch(Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: " << 
				e.getClass() << ": " << e.getMsg() << endl;
			try
			{
				if(decodeModule != NULL)
					delete decodeModule;
				decodeModule=NULL;

				Address rmAddr=intf->getAddress();
				short rmType=intf->getType();

				decodeModule=new DecodeModuleAccess(devId, intf->getCanis()->getInterfaceMask(), intf->getCanis());
				if (decodeModule==NULL)
					throw RecordingModuleException(0, "Not enough memory to start session with decode module for recording");

				// Iniciem una sessio amb el decodeModule
				IPDeviceID deviceId;
				deviceId.id=devId;

				decodeModule->startSession(deviceId);

				decodeModule->setStreamingMode(IPStreamingMode(IPStreamingMode::STREAM));	

				// Establim els FPS del ImagePlugin
				float framesPerSecond=fps;

				if (framesPerSecond==0.0f)
				{
					framesPerSecond=25.0f;
					cout << "RecordingModuleInterfaceSaveStreamThread[" << std::dec << devId <<"]: Recording at 0 FPS. Forcing " << framesPerSecond << " FPS" << endl;
				}

				IPFramesPerSecond ipfps((double)framesPerSecond);

				cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] RecordingModuleInterfaceSaveStreamThread[" << std::dec << devId <<"]: setFPS: " << ipfps.whole <<endl;// ".";

				decodeModule->setFPS(ipfps);
			}
			catch(Exception &e)
			{
				cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: " << 
					e.getClass() << ": " << e.getMsg() << endl;

				if (decodeModule!=NULL)
					delete decodeModule;
				decodeModule=NULL;
				//e.serialize()->materializeAndThrow(true);
				throw;
			}
			catch(...){}
		}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

		/*	AMAlarmId AlmId;
			AlmId.devId=this->devId;
			AlmId.type=AMAlarmId::Input;
			AlmId.intId=-1;
			AMAlarmConfig AlmCfg;
			AlmCfg.alarmId=AlmId;
			AlmCfg.data.baseMethod=Alarm::VALUE_ALWAYS;
			AlmCfg.addr=alarmManager->getAddr();
			try
			{
		//		cout<<"alarmModule.config "<<endl;
		//alarmModule->configureAlarm(AlmCfg);
		//		cout<<"alarmModule.monitor "<<endl;
		//alarmManager->monitorAlarms();
		//		cout<<"alarmModule.monitor  done"<<endl;
		}
		catch(Exception &e)
		{
		cout<<"alarmModule.startSession exc: "<<e.getClass()<<"::"<<e.getMsg()<<endl;
		}
		*/
//		cout << "[RMISST] ck 3 devId: " << devId << endl;
		CHECKPOINT();
//		RecordingModuleInterface::retainRecIndex();
		RecordingModuleInterface::getRecIndex();
		int camExc=0, servExc=0;
		while (!stop)
		{
			RecordingFileFrameChunk *frameChunk=NULL;
#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;
#endif

#ifdef REC_SAVEFRAME_DEBUG
	sfdInstant=sfdTimer.time();
	if (sfdInstant.seconds() >= 3600.0)
	{
		sfdTimer.start();
		if (sfdDebug!=NULL)
			cout << "RecordingModuleInterfaceSaveStreamThread[" << std::dec << devId <<"]: Debugging, got debug pointer: " << (void*)sfdDebug << endl;
		else
			cout << "RecordingModuleInterfaceSaveStreamThread[" << std::dec << devId <<"]: NOT Debugging, did not get pointer." << endl;
	}
#endif

//			cout << "[RMISST] ck 4 devId: " << devId << endl;
			try
			{
				CHECKPOINT();
				t.start();
#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;
#endif
				this->lockMutex->lock();
//				cout << "[RMISST] ck 5 devId: " << devId << endl;

#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;
#endif

				CHECKPOINT();
				decodeModule->setTimeout(1000);

#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;		// Antiga linia 939 (previ a 20110221)
#endif

				IPFrame frame=decodeModule->getCompressedNextFrame();

#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;
#endif

				decodeModule->setTimeout(5000);

#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;
#endif

				CHECKPOINT();
				frameChunk=new RecordingFileFrameChunk();
				// Ens assegurem que es el que toca
				frameChunk->setEndian(Endian::local);

				//			cout << "RMI::saveFrameThread 3.1" << endl;

				CHECKPOINT();
				frameChunk->setFrame(frame.frame, frame.frameLength);
//				cout << "saveStream, frameLength: " << frame.frameLength << endl;

				//			cout << "RMI::saveFrameThread 3.2" << endl;

				// Si es keyframe
				RecordingFileStringMetadataChunk *isKey=new RecordingFileStringMetadataChunk();
				isKey->setMetadata(string("isKeyframe"), (frame.isKey?string("true"):string("false")));
				frameChunk->addSubChunk(isKey);

				// Info de resolucio i codec
				RecordingFileFrameInfoMetadataChunk *frInfo=new RecordingFileFrameInfoMetadataChunk();
				frInfo->setFourCC(frame.codecInfo.fourcc);
				frInfo->setCodecQuality(frame.codecInfo.quality);
				frInfo->setCodecBitrate(frame.codecInfo.bitrate);
				frInfo->setX(frame.frameInfo.x);
				frInfo->setY(frame.frameInfo.y);
				frInfo->setBPP(frame.frameInfo.bpp);
				//			frInfo->setIsKey(frame.isKey);
				frameChunk->addSubChunk(frInfo);

				//			RecordingFileAlarmMetadataChunk *almInfo=new RecordingFileAlarmMetadataChunk();
				//			almInfo->activeAlarms = /*alarmManager->getActiveAlarms();*/ list<AMAlarmValue>();
				//ListIterator<AMAlarmValue> sIter=alarmManager->getActiveAlarms();//alarmModule->activeAlarms.listIterator(0); 
				//while (sIter.hasNext())  
				//{
				//	almInfo->activeAlarms.add(AMCoreAlarmSubscriber) sIter.next();
				//}
				//frameChunk->addSubChunk(almInfo);

				RMSaveFrameParams *fp=new RMSaveFrameParams();
				fp->chunk=frameChunk;

				CHECKPOINT();
				debugFrameLength=0xffffffff, debugFrameChunkLength=0xffffffff, debugFrameChunkSerializationSize=0xffffffff;
				
				dword fParamsLen=0;
				void *fParamsData=NULL;
				
				try
				{
					CHECKPOINT();
					debugFrameLength=frame.frameLength, debugFrameChunkLength=frameChunk->size(), debugFrameChunkSerializationSize=fp->serializationSize();
	
					CHECKPOINT();
					fParamsLen=fp->serializationSize();
	
					CHECKPOINT();
					fParamsData=fp->toNetwork();
	
					CHECKPOINT();
					// Ens assegurem q es deleta al moment :P
					fp->chunk=NULL;
				}
				catch (Exception &e)
				{
					if (fp!=NULL)
						delete fp;
					//e.serialize()->materializeAndThrow(true);
					throw;
				}
				catch (std::exception &stde)
				{
					if (fp!=NULL)
						delete fp;
					throw stde;
				}
				catch (...)
				{
					if (fp!=NULL)
						delete fp;
					throw Exception("RecModIntSaveStrThr: unknown en FP");
				}
				
				CHECKPOINT();
				delete fp;
				fp=NULL;
				
				CHECKPOINT();
				delete frameChunk;

				CHECKPOINT();
				frameChunk=NULL;

				//			cout << "RMI::saveFrameThread 4" << endl;


#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;
#endif

				CHECKPOINT();
				this->sesAddrMutex->lock();
				//			cout << "RMI::saveFrameThread 4.1" << endl;


#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;
#endif

				string savedFrameFile("");
				try
				{
					CHECKPOINT();
					RPCPacket *p=NULL;
					if(!stop)
					{

#ifdef REC_SAVEFRAME_DEBUG
						if (sfdDebug!=NULL)
							*sfdDebug=0x40000000 | __LINE__;
#endif

						p=intf->saveFrame(intf, &sessionAddr, fParamsData);

#ifdef REC_SAVEFRAME_DEBUG
						if (sfdDebug!=NULL)
							*sfdDebug=0x40000000 | __LINE__;
#endif

					}		
					RecordingModuleInterface::session *ses=NULL;
					try
					{

#ifdef REC_SAVEFRAME_DEBUG
						if (sfdDebug!=NULL)
							*sfdDebug=0x40000000 | __LINE__;
#endif


#ifdef REC_SAVEFRAME_DEBUG
						if (sfdDebug!=NULL)
							*sfdDebug=0x40000000 | __LINE__;
#endif

						ses=RecordingModuleInterface::getSessionForAddress(&sessionAddr);
						savedFrameFile=ses->currentFilename;
	

#ifdef REC_SAVEFRAME_DEBUG
						if (sfdDebug!=NULL)
							*sfdDebug=0x40000000 | __LINE__;
#endif

						// I no ens oblidesssssssim de fer el release
						SessionManager<session>::release(sessionAddr.toString());

#ifdef REC_SAVEFRAME_DEBUG
						if (sfdDebug!=NULL)
							*sfdDebug=0x40000000 | __LINE__;
#endif

					}
					catch(...)
					{
					}
					
					if (p!=NULL)
						delete p;
					camExc=0;
					servExc=0;

					CHECKPOINT();
					if (fParamsData!=NULL)
					{
						delete [] (byte*)fParamsData;
						fParamsData=NULL;
					}
				}
				catch(std::exception &stde)
				{
					cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ", " << stde.what() << endl;
					
					if (frameChunk!=NULL)
					{
						delete frameChunk;
						frameChunk=NULL;
					}
					if (fParamsData!=NULL)
					{
						delete [] (byte*)fParamsData;
						fParamsData=NULL;
					}

					if (this->sesAddrMutex->isHeldByCurrentThread())
						this->sesAddrMutex->unlock();
					else
						cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] sesAddrMutex NO lockat" << endl;
					
					// i cap amunt pq es processi
					throw stde;
				}
				catch (Exception &e)
				{
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << 
						e.getClass() << ": " << e.getMsg() <<" thread:"<<(void*)this<<" lockMutex:"<<(void*)this->lockMutex<< endl;

					if (frameChunk!=NULL)
					{
						delete frameChunk;
						frameChunk=NULL;
					}
					if (fParamsData!=NULL)
					{
						delete [] (byte*)fParamsData;
						fParamsData=NULL;
					}

					if (this->sesAddrMutex->isHeldByCurrentThread())
						this->sesAddrMutex->unlock();
					else
						cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] sesAddrMutex NO lockat" << endl;
					//e.serialize()->materializeAndThrow(true);
					throw;
				}

				if (this->sesAddrMutex->isHeldByCurrentThread())
					this->sesAddrMutex->unlock();
				else
					cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] sesAddrMutex NO lockat" << endl;

				//			cout << "RMI::saveFrameThread 5" << endl;

				// Deletes de les coses por dios!!!

				//			cout << "RMI::saveFrameThread 6" << endl;


#ifdef REC_SAVEFRAME_DEBUG
				if (sfdDebug!=NULL)
					*sfdDebug=0x40000000 | __LINE__;
#endif

				if (!stop && framesPerSecond!=fps)
				{
					IPFramesPerSecond ipfps((double)framesPerSecond);

					cout << "RecordingModuleInterfaceSaveStreamThread: alter FPS: " << ipfps.whole << endl;//".";
					//				cout.width(3);
					//				cout.fill('0');
					//				cout << ipfps.frac << endl; 
					//				cout.width(0);

					CHECKPOINT();
					decodeModule->setFPS(ipfps);

					framesPerSecond=fps;
				}


#ifdef REC_SAVEFRAME_DEBUG
				if (sfdDebug!=NULL)
					*sfdDebug=0x40000000 | __LINE__;
#endif

//				cout << "[RMISST] ck 7 devId: " << devId << endl;
				if (this->lockMutex->isHeldByCurrentThread())
					this->lockMutex->unlock();
				else
					cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] lockMutex NO lockat" << endl;
//				cout << "[RMISST] ck 8 devId: " << devId << endl;
				TimerInstant ti=t.time();

				if (!stop && framesPerSecond>0.0)
				{
					int maxUSecs=(1000000.0/framesPerSecond);
					int usecsRemaining=maxUSecs-ti.useconds();
					
					// 20110411: Mirem que no ens esperem més d'un frame perquè
					// de vegades la cosa s'encasquilla amb el setIndexForFile
					// durant una bona estona i llavors el temps transcorregut
					// es molt ganso, ens surt un valor negatiu i la cosa
					// es desmadra :)
					if (usecsRemaining<=maxUSecs && usecsRemaining>0)
						usleep(usecsRemaining);

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					float fullFactor=RecordingModuleInterface::diskWriter->calcFullFactorForFile(savedFrameFile);
					

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					// Si ens passem del "100%" de ple, esperem fins que baixi
					if (fullFactor>1.0)
					{
						int wait=0;
						float waitTime=0;
						while (fullFactor>1.0)
						{
							if(wait%10 == 0)
							{
								cout << "ALERT: " << __FILE__ << " line "
									<< __LINE__ << " waiting for fullFactor ("
									<< fullFactor << ") for file " <<
									savedFrameFile << ": to go down..."
									<< endl;
							}
							wait++;
							// Esperem fullFactor/10 segons
							waitTime += fullFactor*100000.0;
							usleep(fullFactor*100000.0);
							
#ifdef __STOP_SERVICE_ON_ALERT 
							if(!this->inAlert && waitTime > ((float)RecordingModuleInterface::maxCamSecsInAlert)*1000000.0)
							{
								RecordingModuleInterface::camsAlertLock.lock();
								RecordingModuleInterface::camsInAlert.push_back(this->devId);
								this->inAlert = true;
								cout << StrUtils::getDateString() << " ALERT: " << __FILE__ << " line "
									<< __LINE__ << " waited for " << waitTime/1000.0 
									<< "ms for fullFactor to go down for device:" << this->devId
									<< ", total inAlert files:" << camsInAlert.size() << endl;
								this->intf->checkFullFactorShutdown();
								RecordingModuleInterface::camsAlertLock.unlock();
							}
#endif
							

#ifdef REC_SAVEFRAME_DEBUG
							if (sfdDebug!=NULL)
								*sfdDebug=0x40000000 | __LINE__;
#endif

							fullFactor = RecordingModuleInterface::diskWriter->calcFullFactorForFile(savedFrameFile);

#ifdef REC_SAVEFRAME_DEBUG
							if (sfdDebug!=NULL)
								*sfdDebug=0x40000000 | __LINE__;
#endif
				
						}
					}
					else if (fullFactor>0.2)
					{
						if ((fullFactorWarningCount%32)==0)
						{
							cout << "WARNING: " << __FILE__ << " line "
								<< __LINE__ << " fullFactor>0.2: "
								<< fullFactor << " for file " << savedFrameFile
								<< endl;
						}
						fullFactorWarningCount++;
						// Intentem fer baixar el fullFactor esperant una mica
						// mes del compte
						// Esperem fullFactor/3 segons
						usleep(fullFactor*333333.33);

#ifdef __STOP_SERVICE_ON_ALERT 
						if(this->inAlert) 
						{
							RecordingModuleInterface::camsAlertLock.lock();
							camsInAlert.remove(this->devId);
							this->inAlert = false;
							this->intf->checkFullFactorShutdown();
							cout << StrUtils::getDateString() << " ALERT: " << __FILE__ << " line "
								<< __LINE__ << " recovered after fullFactor for device:" << this->devId
								<< ", total inAlert files:" << camsInAlert.size() << endl;
							RecordingModuleInterface::camsAlertLock.unlock();
						}
#endif
					}
					else
					{
						fullFactorWarningCount=0;
#ifdef __STOP_SERVICE_ON_ALERT 
						if(this->inAlert) 
						{
							RecordingModuleInterface::camsAlertLock.lock();
							camsInAlert.remove(this->devId);
							this->inAlert = false;
							this->intf->checkFullFactorShutdown();
							cout << StrUtils::getDateString() << " ALERT: " << __FILE__ << " line "
								<< __LINE__ << " recovered after fullFactor for device:" << this->devId
								<< ", total inAlert files:" << camsInAlert.size() << endl;
							RecordingModuleInterface::camsAlertLock.unlock();
						}
#endif
					}
					if(this->recLost)
					{
						cout<<"RMISST::recLost == true, logging"<<endl;
						almLogThread->queue(RecordingModuleAlarmAndLogThread::event(devId, RecordingModuleAlarmAndLogThread::rec_lost, "", false));
						this->recLost = false;	
					}
					streamed = true;
				}
			}
			catch (SocketTimeoutException &e)
			{

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				if (frameChunk!=NULL)
				{
					delete frameChunk;
					frameChunk=NULL;
				}

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				if (this->lockMutex->isHeldByCurrentThread())
					this->lockMutex->unlock();
				else
					cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] lockMutex NO lockat" << endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]:" << e.getClass() << ": " << e.getMsg() << CHECKPOINT_TOSTRING() << endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

			}
			catch (SocketException &e)
			{

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				if (frameChunk!=NULL)
				{
					delete frameChunk;
					frameChunk=NULL;
				}

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

//				cout << "[RMISST] ck 9 devId: " << devId << endl;
				if (this->lockMutex->isHeldByCurrentThread())
					this->lockMutex->unlock();
				else
					cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] lockMutex NO lockat" << endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				cout<<"RMISST:: id:"<<devId<<":"<<e.getClass()<<": "<<e.getMsg()<<endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

			}
			catch (RecordingModuleException &e)
			{	//excepció saveFrame

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				if (frameChunk!=NULL)
				{
					delete frameChunk;
					frameChunk=NULL;
				}

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				cout<<"RMISST:: RME id:"<<devId<<": from " << CHECKPOINT_FILE() << ", line " << CHECKPOINT_LINE() << " "<<e.getClass()<<": "<<e.getMsg()<<endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				if (this->lockMutex->isHeldByCurrentThread())
					this->lockMutex->unlock();
				else
					cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] lockMutex NO lockat" << endl;
				// Cal parar la gravacio!!!!!!
				//	cThrLock.wlock();
				RecordingModuleInterface::session *ses=NULL;
				try
				{
					this->sesAddrMutex->lock();
					cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping --> getSesForAddr"<<endl;
					ses=RecordingModuleInterface::getSessionForAddress(&sessionAddr);
					cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping <-- getSesForAddr"<<endl;

					cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping --> endSesMtx.lock"<<endl;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->lock()") << endl;
//#endif

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					ses->endSessionMutex->lock(true, "saveStreamThread 1");

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->lock() LOCKED") << endl;
//#endif
					cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping <-- endSesMtx.lock"<<endl;
					//				RecordingModuleInterfaceSaveStreamThread *th=(RecordingModuleInterfaceSaveStreamThread *)ses->recordingThread;
					//				ses->recordingThread=NULL;
					ses->startedRecording= false;
					ses->toDB=true;
					ses->inDB=false;
					cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping SessionToDB, set isRecording=false "<<endl;
					RecordingModuleInterface::sessionToDB(&sessionAddr, intf->address, ses);
					cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping --> endSesMtx.unlock"<<endl;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
//
//#endif

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					ses->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif


					// I no ens oblidesssssssim de fer el release
					SessionManager<session>::release(sessionAddr.toString());


#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					this->sesAddrMutex->unlock();
					
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
//#endif

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping <-- endSesMtx.unlock"<<endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				}
				catch(Exception &e)
				{

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					if (this->sesAddrMutex->isHeldByCurrentThread())
						this->sesAddrMutex->unlock();
					else
						cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] sesAddrMutex NO lockat" << endl;


#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << 
						e.getClass() << ": " << e.getMsg() << endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				}

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping --> SessionManager::iterator"<<endl;
				SessionManager<RecordingModuleInterface::session>::Iterator it=SessionManager<RecordingModuleInterface::session>::iterator();
				cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping <-- SessionManager::iterator"<<endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				while (it.hasNext())
				{
					RecordingModuleInterface::session *s=it.next();
					//						if(s->recordingThread==th)
					if (s->sessionParams.devId==devId)
					{
						//	s->recordingThread = NULL;
						if(s->startedRecording || s->startRecStreamRecv)
						{
							s->startedRecording = false;
							s->startRecStreamRecv = false;

							s->toDB = true;
							s->inDB = false;
							string addrs = it.getLastId();
							cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping SessionToDB, set isRecording=false :"<<addrs<<endl;
							int pos=addrs.find(":");
							if(pos!=string::npos)
							{
								string ip = addrs.substr(0, pos);
								int port = atoi(addrs.substr(pos+1).c_str());

								Address a(IP(ip), port);
								RecordingModuleInterface::sessionToDB(&a, intf->address, s);
							}
						}
					}
					SessionManager<RecordingModuleInterface::session>::releaseFromIterator(it.getLastId());
				}

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping --> SessionManager::releaseiterator"<<endl;
				SessionManager<RecordingModuleInterface::session>::releaseIterator();
				cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping <-- SessionManager::releaseiterator"<<endl;

//				cout << "[RMISST] ck 12 devId: " << devId << endl;
				//	cThrLock.unlock();
				//e.serialize()->materializeAndThrow(true);

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				throw;
			}
			catch (Exception &e)
			{

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << 
					e.getClass() << ": " << e.getMsg() << endl;
//				cout << "[RMISST] ck 13 devId: " << devId << endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				if (this->lockMutex->isHeldByCurrentThread())
					this->lockMutex->unlock();
				else
					cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] lockMutex NO lockat" << endl;


#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				cout<<"RMISST:: id:"<<devId<<": from " << CHECKPOINT_FILE() << ", line " << CHECKPOINT_LINE() << ""<<e.getClass()<<": "<<e.getMsg()<<endl;
				ControlModuleAccess *controlModule=NULL;
				cout<<"RMISST::Exception ["<<devId<<"] stopping --> ControlModule"<<endl;
				try
				{
					CHECKPOINT();
					Address rmAddr=intf->getAddress();
					short rmType=intf->getType();

					controlModule=new ControlModuleAccess(intf->getCanis());
					CHECKPOINT();
					CPDeviceID cpDev;
					cpDev.id=devId;
					controlModule->startSession(devId);
					CHECKPOINT();
					CPConfigParam cp=controlModule->getConfigParam(CPConfigParam(string("/Video/1/FPS"),string("")));
					char *ptr;
					const char *cs=cp.value.c_str();
					float fVal = strtof(cs, &ptr);

					if(ptr!=cs)//comprovem que el valor sigui valid
					{
						cout<<" Exception :  id:"<<devId<<":"<<e.getClass()<<":"<<e.getMsg()<<endl;
						cout<<"setfps:"<<fVal;
						decodeModule->setFPS(fVal);
					}
					controlModule->endSession(cpDev);
					if (controlModule!=NULL)
						delete controlModule;
					controlModule=NULL;
				}
				catch (Exception &e)
				{
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << 
						e.getClass() << ": " << e.getMsg() << endl;
					if(controlModule != NULL)
						delete controlModule;
					controlModule=NULL;
				}
				cout<<"RMISST::Exception ["<<devId<<"] stopping <-- ControlModule"<<endl;


#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

//				cout << "[RMISST] ck 14 devId: " << devId << endl;
				// Cal parar la gravacio!!!!!!
				//	cThrLock.wlock();
				RecordingModuleInterface::session *ses=NULL;
				try
				{
					this->sesAddrMutex->lock();

					cout<<"RMISST::Exception ["<<devId<<"] stopping --> getSessionForAddress"<<endl;
					ses=RecordingModuleInterface::getSessionForAddress(&sessionAddr);

				cout<<"RMISST::Exception ["<<devId<<"] stopping <-- getSessionForAddress"<<endl;

				cout<<"RMISST::Exception ["<<devId<<"] stopping --> endSesMtx.lock"<<endl;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->lock()") << endl;
//#endif
					ses->endSessionMutex->lock(true, "saveStreamThread 2");
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->lock() LOCKED") << endl;
//#endif
				cout<<"RMISST::Exception ["<<devId<<"] stopping <-- endSesMtx.lock"<<endl;
					//				RecordingModuleInterfaceSaveStreamThread *th=(RecordingModuleInterfaceSaveStreamThread *)ses->recordingThread;
					//				ses->recordingThread=NULL;
					ses->startedRecording= false;
					ses->toDB=true;
					ses->inDB=false;
					cout<<"RMISST::Exception ["<<devId<<"] stopping : SessionToDB, set isRecording=false "<<endl;
					RecordingModuleInterface::sessionToDB(&sessionAddr, intf->address, ses);
				cout<<"RMISST::Exception ["<<devId<<"] stopping --> endSesMtx.unlock"<<endl;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
//
//#endif
					ses->endSessionMutex->unlock();
	
					// I no ens oblidesssssssim de fer el release				
					SessionManager<session>::release(sessionAddr.toString());

					this->sesAddrMutex->unlock();

//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
//#endif
				cout<<"RMISST::Exception ["<<devId<<"] stopping <-- endSesMtx.unlock"<<endl;
				}
				catch(Exception &e)
				{
					if (this->sesAddrMutex->isHeldByCurrentThread())
						this->sesAddrMutex->unlock();
					else
						cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] sesAddrMutex NO lockat" << endl;
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << 
						e.getClass() << ": " << e.getMsg() << endl;
				}


#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				cout<<"RMISST::Exception ["<<devId<<"] stopping --> SessionManager::iterator"<<endl;
				SessionManager<RecordingModuleInterface::session>::Iterator it=SessionManager<RecordingModuleInterface::session>::iterator();
				cout<<"RMISST::Exception ["<<devId<<"] stopping <-- SessionManager::iterator"<<endl;

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				while (it.hasNext())
				{
					RecordingModuleInterface::session *s=it.next();
					//						if(s->recordingThread==th)
					if (s->sessionParams.devId==devId)
					{
						//	s->recordingThread = NULL;
						if(s->startedRecording || s->startRecStreamRecv)
						{
							s->startedRecording = false;
							s->startRecStreamRecv = false;

							s->toDB=true;
							s->inDB = false;
							string addrs = it.getLastId();
							cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping : SessionToDB, set isRecording=false :"<<addrs<<endl;
							int pos=addrs.find(":");
							if(pos!=string::npos)
							{
								string ip = addrs.substr(0, pos);
								int port = atoi(addrs.substr(pos+1).c_str());

								Address a(IP(ip), port);
								RecordingModuleInterface::sessionToDB(&a, intf->address, s);
							}
						}
					}

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

					SessionManager<RecordingModuleInterface::session>::releaseFromIterator(it.getLastId());
				}
				cout<<"RMISST::Exception ["<<devId<<"] stopping --> SessionManager::releaseiterator"<<endl;
				SessionManager<RecordingModuleInterface::session>::releaseIterator();
				cout<<"RMISST::Exception ["<<devId<<"] stopping <-- SessionManager::releaseiterator"<<endl;

//				cout << "[RMISST] ck 15 devId: " << devId << endl;
				//	cThrLock.unlock();
				//e.serialize()->materializeAndThrow(true);

#ifdef REC_SAVEFRAME_DEBUG
					if (sfdDebug!=NULL)
						*sfdDebug=0x40000000 | __LINE__;
#endif

				throw;
			}
			//			cout << "RMI::saveFrameThread 7" << endl;

#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x40000000 | __LINE__;
#endif

		}

		CHECKPOINT();
		ControlModuleAccess *controlModule=NULL;
		cout<<"RMISST::Stop ["<<devId<<"] stopping --> ControlModule"<<endl;
		try
		{
			Address rmAddr=intf->getAddress();
			short rmType=intf->getType();

			controlModule=new ControlModuleAccess(intf->getCanis());
			CPDeviceID cpDev;
			cpDev.id=devId;
			controlModule->startSession(devId);
			CPConfigParam cp=controlModule->getConfigParam(CPConfigParam(string("/Video/1/FPS"),string("")));
			char *ptr;
			const char *cs=cp.value.c_str();
			float fVal = strtof(cs, &ptr);

			if(ptr!=cs)//comprovem que el valor sigui valid
			{
				decodeModule->setFPS(fVal);
			}
			controlModule->endSession(cpDev);
			if (controlModule!=NULL)
				delete controlModule;
			controlModule=NULL;
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << 
				e.getClass() << ": " << e.getMsg() << endl;

			if(controlModule != NULL)
				delete controlModule;
			controlModule=NULL;
		}
		cout<<"RMISST::Stop ["<<devId<<"] stopping <-- ControlModule"<<endl;

		//		cThrLock.wlock();
//		cout << "[RMISST] ck 16 devId: " << devId << endl;
		cout<<"RMISST::Stop ["<<devId<<"] stopping --> lockMutex"<<endl;
		this->lockMutex->lock();
		cout<<"RMISST::Stop ["<<devId<<"] stopping <-- lockMutex"<<endl;
		try
		{
			//parém totes les gravacions
		cout<<"RMISST::Stop ["<<devId<<"] stopping --> SesMng::iterator"<<endl;
			SessionManager<RecordingModuleInterface::session>::Iterator it=SessionManager<RecordingModuleInterface::session>::iterator();
		cout<<"RMISST::Stop ["<<devId<<"] stopping <-- SesMng::iterator"<<endl;
			while (it.hasNext())
			{
				RecordingModuleInterface::session *s=it.next();
				//						if(s->recordingThread==th)
				if (s->sessionParams.devId==devId)
				{
					//					s->recordingThread = NULL;
					if(s->startedRecording || s->startRecStreamRecv)
					{
						s->startedRecording = false;
						s->startRecStreamRecv = false;

						s->toDB=true;
						s->inDB = false;
						string addrs = it.getLastId();
						cout<<"RMISST::RecordingModuleException ["<<devId<<"] stopping SessionToDB, set isRecording=false :"<<addrs<<endl;
						int pos=addrs.find(":");
						if(pos!=string::npos)
						{
							string ip = addrs.substr(0, pos);
							int port = atoi(addrs.substr(pos+1).c_str());

							Address a(IP(ip), port);
							RecordingModuleInterface::sessionToDB(&a, intf->address, s);
						}
					}
				}
				SessionManager<RecordingModuleInterface::session>::releaseFromIterator(it.getLastId());
			}
		cout<<"RMISST::Stop ["<<devId<<"] stopping --> SesMng::releaseiterator"<<endl;
			SessionManager<RecordingModuleInterface::session>::releaseIterator();
		cout<<"RMISST::Stop ["<<devId<<"] stopping <-- SesMng::releaseiterator"<<endl;
		}
		catch(Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << 
				e.getClass() << ": " << e.getMsg() << endl;
		}


//		cout << "[RMISST] ck 17 devId: " << devId << endl;
		//	cThrLock.unlock();
		cout<<"RMISST::Stop ["<<devId<<"] stopping --> lockMutex.unlock"<<endl;
		if (this->lockMutex->isHeldByCurrentThread())
			this->lockMutex->unlock();
		else
			cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] lockMutex NO lockat" << endl;
		cout<<"RMISST::Stop ["<<devId<<"] stopping <-- lockMutex.unlock"<<endl;
		//			cout << "RMI::saveFrameThread 8" << endl;
//		cout<<"RMISST::Stop ["<<devId<<"] stopping --> releaseRecIdx"<<endl;
//		RecordingModuleInterface::releaseRecIndex();
//		cout<<"RMISST::Stop ["<<devId<<"] stopping <-- releaseRecIdx"<<endl;
		//			cout << "RMI::saveFrameThread 9" << endl;

		//	delete pk;
	}
	catch (Exception &e)
	{
		cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << e.getClass() << ": " << e.getMsg() << ",  throwing => finishing thread abnormally" << endl;
		this->running=false;

		// Sanity check
		if (this->lockMutex->isHeldByCurrentThread())
		{
			cout << "RecordingModuleInterfaceSaveStreamThread unlock missing detected at line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << e.getClass() << ": " << e.getMsg() << endl;
			this->lockMutex->unlock();
		}
		if (this->sesAddrMutex->isHeldByCurrentThread())
		{
			cout << "RecordingModuleInterfaceSaveStreamThread unlock missing detected at line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << e.getClass() << ": " << e.getMsg() << endl;
			this->sesAddrMutex->unlock();
		}		

#ifdef __STOP_SERVICE_ON_ALERT 
		if(this->inAlert) 
		{
			cout << StrUtils::getDateString() << " RMISST::Exception[" << std::dec << devId <<"], finishing thread abnormally, inAlert == true Lock -->" << endl;
			RecordingModuleInterface::camsAlertLock.lock();
			camsInAlert.remove(this->devId);
			this->inAlert = false;
			this->intf->checkFullFactorShutdown();
			RecordingModuleInterface::camsAlertLock.unlock();
			cout << StrUtils::getDateString() << " RMISST::Exception[" << std::dec << devId <<"], finishing thread abnormally, inAlert == true Lock <--" << endl;
		}
#endif

		// Fem "l'endSession" de la sessio que grava
		cout << "RMISST::Exception[" << std::dec << devId <<"], finishing thread abnormally: sesAddr Lock -->" << endl;
    	this->sesAddrMutex->lock();
		cout << "RMISST::Exception[" << std::dec << devId <<"], finishing thread abnormally: sesAddr Lock <--" << endl;
    
		struct timeval tv;
		tv.tv_sec = 0;
    	
		RecordingModuleInterface::session *recSes=NULL;
    	try
    	{
    		recSes = RecordingModuleInterface::getSessionForAddress(&sessionAddr);	
    		recSes->freeSesAddr=sessionAddr;
    		recSes->freeSesModuleAddr=intf->getAddress();
    		// El release del get :P
    		SessionManager<RecordingModuleInterface::session>::release(sessionAddr.toString());

			// Deixem de gravar => tanquem marcatge
			RecordingModuleInterface::recordingIndex *ri=
				RecordingModuleInterface::getRecIndex();
			RecordingModuleInterface::wlockRecIndex();
			try
			{
				CHECKPOINT();
				const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(recSes->recIndexIdx);
				RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;

				r.endDate.isValid=true;
				ri->alterSegment(recSes->recIndexIdx, r);

				recSes->recIndexIdx=0xffffffffffffffffll;

				tv.tv_sec = r.endDate.secs;//fem servir la data com a data del log. Si no, fotem la data actual 
				tv.tv_usec = r.endDate.millis * 1000;

				RecordingModuleInterface::unlockRecIndex();
			}
			catch (...)
			{
				RecordingModuleInterface::unlockRecIndex();
			}

			// FI marcatge
    	}
    	catch (Exception &e)
    	{
    		cout << "RecordingModuleInterfaceSaveStreamThread line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << 
    		" finishing thread: No session for sessionAddr " << sessionAddr.toString() << ": " << e.getClass() << ": " << e.getMsg() << endl;
    	}

		//enviem al log i alarma
		if(streamed)
		{
			string msg="";
			if(e.getClass() == string("CameraNotConnectableException")) 
				msg = "not_connectable";
			else if(e.getClass() == string("CameraMalfunctionException"))
				msg = "malfunction";
			else
				msg = "unknown";//e.getClass()+string("|")+e.getMsg();
			almLogThread->queue(RecordingModuleAlarmAndLogThread::event(devId, RecordingModuleAlarmAndLogThread::rec_lost, tv, msg, true));
		}
		else
			cout<<"RecordingModuleInterfaceSaveStreamThread[" << devId << "] no frames received, not raising..."<<endl;
		this->recLost = true;	

		try
		{
			// El release que fara el delete si toca :)
			cout << "RecordingModuleInterfaceSaveStreamThread releasing recording session[" << std::dec << devId <<"] @" << sessionAddr.toString() << ", refCount: " << SessionManager<RecordingModuleInterface::session>::getReferenceCount(sessionAddr.toString()) << endl;
			SessionManager<RecordingModuleInterface::session>::release(sessionAddr.toString());
		}
		catch (Exception &e)
		{
			cout << "RecordingModuleInterfaceSaveStreamThread line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << 
				" error locu release ¿?¿?: " << e.getClass() << ": " << e.getMsg() << endl;
		}

    	cout << "RecordingModuleInterfaceSaveStreamThread released recording session @" << sessionAddr.toString() << ", refCount: " << SessionManager<RecordingModuleInterface::session>::getReferenceCount(sessionAddr.toString()) << endl;
    	
    	this->sesAddrMutex->unlock();

    	// Fem la part de les sessions que indiquen el tipus de gravacio. La que
    	// ha estat gravant aqui al thread ja no ens l'hauriem de trobar, pero
    	// ja veurem que passa :D
		// També "deixem de gravar" => tanquem el marcatge
    	for(int i=0; i<RecordingFileHeaderChunk::REC_HOWMANY; i++)
    	{
    		RecordingModuleInterface::session *updSes = 
    			RecordingModuleInterface::getSessionRecordingDevice(devId, (RecordingFileHeaderChunk::recType)i, true);
    
    		if(updSes==NULL)
    			continue;
    			
			// Deixem de gravar => tanquem marcatge
			if (updSes!=recSes)
			{
				RecordingModuleInterface::recordingIndex *ri=
					RecordingModuleInterface::getRecIndex();
				RecordingModuleInterface::wlockRecIndex();
				try
				{
					CHECKPOINT();
					const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(updSes->recIndexIdx);
					RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;
					
					r.endDate.isValid=true;
					ri->alterSegment(updSes->recIndexIdx, r);
					
					updSes->recIndexIdx=0xffffffffffffffffll;
					RecordingModuleInterface::unlockRecIndex();
				}
				catch (...)
				{
					RecordingModuleInterface::unlockRecIndex();
				}
			}
			// FI marcatge

    		string addrStr = 
    			SessionManager<RecordingModuleInterface::session>::getKey(updSes);
    
    		list<string> addrComps=StrUtils::split(addrStr, string(":"));
    		if (addrComps.size() == 2)
    		{
    			string ip=addrComps.front();
    			addrComps.pop_front();
    			int port=atoi(addrComps.front().c_str());
    			addrComps.pop_front();
    			
    			Address addr(IP(ip), port);
    			updSes->freeSesAddr = addr;
    			updSes->freeSesModuleAddr=intf->getAddress();
    		}
    		else
    		{
    			cout << "RecordingModuleInterfaceSaveStreamThread preparing release for updSes session " << i << " @" << addrStr << ", refCount: " << SessionManager<RecordingModuleInterface::session>::getReferenceCount(addrStr) <<  endl;
    		}
    
    		try
    		{
    			if (updSes!=recSes)
    			{
    				cout << "RecordingModuleInterfaceSaveStreamThread releasing updSes session " << i << " @" << sessionAddr.toString() << ", refCount: " << SessionManager<RecordingModuleInterface::session>::getReferenceCount(sessionAddr.toString()) << endl;
    				SessionManager<RecordingModuleInterface::session>::release(updSes);
    			}
    		}
    		catch (Exception &e)
    		{
    			cout << "RecordingModuleInterfaceSaveStreamThread line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << 
    			" error locu release ¿?¿?: " << e.getClass() << ": " << e.getMsg() << endl;
    		}
    	}

		cout << "RMISST::Exception[" << std::dec << devId <<"], finishing thread abnormally: stopCond lock -->" << endl;
		this->stopCond.lock();
		cout << "RMISST::Exception[" << std::dec << devId <<"], finishing thread abnormally: stopCond lock <--" << endl;
		this->stopped = true;
		this->stopCond.broadcast();
		cout << "RMISST::Exception[" << std::dec << devId <<"], finishing thread abnormally: stopCond unlock -->" << endl;
		this->stopCond.unlock();
		cout << "RMISST::Exception[" << std::dec << devId <<"], finishing thread abnormally: stopCond unlock <--" << endl;
		
		// Descomptem l'stream :D
		RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.lock();
		RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount--;
			if (RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::almActiveRaised)
			{
				cout << "RecordingModuleInterfaceSaveStreamThread line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: lower limit alarm"<< endl;
				almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_limit_reached,"", false));//(( StrUtils::decToString(RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount)));
				RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::almActiveRaised=true;
			}
			if (RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount<RecordingModuleInterface::warnActiveRecordingsCount && RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::warnActiveRaised)
			{
					almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_limit_warning,"", false));//(( StrUtils::decToString(RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount)));
					 RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::warnActiveRaised=false;
			}
		cout << "RMISST: activeCount--: " << RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount << endl;
		RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.unlock();
	
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	catch(std::exception &stde)
	{
		cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << stde.what() << endl;
		cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << " DEBUG: (" << debugFrameLength << ", " << debugFrameChunkLength << ", " << debugFrameChunkSerializationSize << ")" << endl;

		this->running=false;
		// Sanity check
		if (this->lockMutex->isHeldByCurrentThread())
		{
			cout << "RecordingModuleInterfaceSaveStreamThread unlock missing detected at line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << stde.what() << endl;
			this->lockMutex->unlock();
		}
		if (this->sesAddrMutex->isHeldByCurrentThread())
		{
			cout << "RecordingModuleInterfaceSaveStreamThread unlock missing detected at line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": " << stde.what() << endl;
			this->sesAddrMutex->unlock();
		}		
		
		//enviem al log i alarma
		//struct timeval tv;
		//tv.tv_sec = 0;
		//almLogThread->queue(RecordingModuleAlarmAndLogThread::event(devId, RecordingModuleAlarmAndLogThread::rec_lost, tv,  string("std::exception:")+stde.what()));
		if(streamed)
			almLogThread->queue(RecordingModuleAlarmAndLogThread::event(devId, RecordingModuleAlarmAndLogThread::rec_lost, "unknown", true));//string("std::exception:")+stde.what()
		else
			cout<<"RecordingModuleInterfaceSaveStreamThread[" << devId << "] no frames received, not raising..."<<endl;
		this->recLost = true;	

	}
	catch(...)
	{
		cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": unknown exception" << endl;
		this->running=false;
		// Sanity check
		if (this->lockMutex->isHeldByCurrentThread())
		{
			cout << "RecordingModuleInterfaceSaveStreamThread unlock missing detected at line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": unknown exception" << endl;
			this->lockMutex->unlock();
		}
		if (this->sesAddrMutex->isHeldByCurrentThread())
		{
			cout << "RecordingModuleInterfaceSaveStreamThread unlock missing detected at line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << ": unknown exception" << endl;
			this->sesAddrMutex->unlock();
		}		

		//enviem al log i alarma
		//struct timeval tv;
		//tv.tv_sec = 0;
		//almLogThread->queue(RecordingModuleAlarmAndLogThread::event(devId, RecordingModuleAlarmAndLogThread::rec_lost, tv,  string("desconegut")));
		if(streamed)
			almLogThread->queue(RecordingModuleAlarmAndLogThread::event(devId, RecordingModuleAlarmAndLogThread::rec_lost, string("unknown"), true));
		else
			cout<<"RecordingModuleInterfaceSaveStreamThread[" << devId << "] no frames received, not raising..."<<endl;
		this->recLost = true;	
	}

	cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"], finishing thread normally" << endl;
	this->running=false;
	// Sanity check
	if (this->lockMutex->isHeldByCurrentThread())
	{
		cout << "RecordingModuleInterfaceSaveStreamThread unlock missing detected at line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << endl;
		this->lockMutex->unlock();
	}
	if (this->sesAddrMutex->isHeldByCurrentThread())
	{
		cout << "RecordingModuleInterfaceSaveStreamThread unlock missing detected at line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << endl;
		this->sesAddrMutex->unlock();
	}		

#ifdef __STOP_SERVICE_ON_ALERT 
	if(this->inAlert) 
	{
		cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"], inAlert == true, Lock -->" << endl;
		RecordingModuleInterface::camsAlertLock.lock();
		camsInAlert.remove(this->devId);
		this->inAlert = false;
		this->intf->checkFullFactorShutdown();
		RecordingModuleInterface::camsAlertLock.unlock();
		cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"], inAlert == true, Lock <--" << endl;
	}
#endif
		// Fem "l'endSession" de la sessio que grava i de les sessions que indiquen
		// el tipus de gravacio que s'esta fent
		this->sesAddrMutex->lock();

		RecordingModuleInterface::session *recSes=NULL;
		try
		{
			recSes = RecordingModuleInterface::getSessionForAddress(&sessionAddr);	
			recSes->freeSesAddr=sessionAddr;
			recSes->freeSesModuleAddr=intf->getAddress();
			// El release del get :P
			SessionManager<RecordingModuleInterface::session>::release(sessionAddr.toString());

			// Deixem de gravar => tanquem marcatge
			RecordingModuleInterface::recordingIndex *ri=
				RecordingModuleInterface::getRecIndex();
			RecordingModuleInterface::wlockRecIndex();
			try
			{
				CHECKPOINT();
				const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(recSes->recIndexIdx);
				RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;

				r.endDate.isValid=true;
				ri->alterSegment(recSes->recIndexIdx, r);

				recSes->recIndexIdx=0xffffffffffffffffll;
				RecordingModuleInterface::unlockRecIndex();
			}
			catch (...)
			{
				RecordingModuleInterface::unlockRecIndex();
			}
			// FI marcatge
		}
		catch (Exception &e)
		{
			cout << "RecordingModuleInterfaceSaveStreamThread line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << 
			" finishing thread: No session for sessionAddr " << sessionAddr.toString() << ": " << e.getClass() << ": " << e.getMsg() << endl;
		}
		
		try
		{
			// El release que fara el delete si toca :)
			cout << "RecordingModuleInterfaceSaveStreamThread releasing recording session @" << sessionAddr.toString() << ", refCount: " << SessionManager<RecordingModuleInterface::session>::getReferenceCount(sessionAddr.toString()) << endl;
			SessionManager<RecordingModuleInterface::session>::release(sessionAddr.toString());
		}
		catch (Exception &e)
		{
			cout << "RecordingModuleInterfaceSaveStreamThread line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << 
			" error locu release ¿?¿?: " << e.getClass() << ": " << e.getMsg() << endl;
		}
	
		cout << "RecordingModuleInterfaceSaveStreamThread released recording session @" << sessionAddr.toString() << ", refCount: " << SessionManager<RecordingModuleInterface::session>::getReferenceCount(sessionAddr.toString()) << endl;
		
		this->sesAddrMutex->unlock();
	
		// Fem la part de les sessions que indiquen el tipus de gravacio. La que
		// ha estat gravant aqui al thread ja no ens l'hauriem de trobar, pero
		// ja veurem que passa :D
		for(int i=0; i<RecordingFileHeaderChunk::REC_HOWMANY; i++)
		{
			RecordingModuleInterface::session *updSes = 
				RecordingModuleInterface::getSessionRecordingDevice(devId, (RecordingFileHeaderChunk::recType)i, true);
	
			if(updSes==NULL)
				continue;
				
			// Deixem de gravar => tanquem marcatge
			if (updSes!=recSes)
			{
				RecordingModuleInterface::recordingIndex *ri=
					RecordingModuleInterface::getRecIndex();
				RecordingModuleInterface::wlockRecIndex();
				try
				{
					CHECKPOINT();
					const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(updSes->recIndexIdx);
					RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;
					
					r.endDate.isValid=true;
					ri->alterSegment(updSes->recIndexIdx, r);
					
					updSes->recIndexIdx=0xffffffffffffffffll;
					RecordingModuleInterface::unlockRecIndex();
				}
				catch (...)
				{
					RecordingModuleInterface::unlockRecIndex();
				}
			}
			// FI marcatge
			
			string addrStr = 
				SessionManager<RecordingModuleInterface::session>::getKey(updSes);
		
			list<string> addrComps=StrUtils::split(addrStr, string(":"));
			if (addrComps.size() == 2)
			{
				string ip=addrComps.front();
				addrComps.pop_front();
				int port=atoi(addrComps.front().c_str());
				addrComps.pop_front();
				
				Address addr(IP(ip), port);
				updSes->freeSesAddr = addr;
				updSes->freeSesModuleAddr=intf->getAddress();
			}
			else
			{
				cout << "RecordingModuleInterfaceSaveStreamThread preparing release for updSes session " << i << " @" << addrStr << ", refCount: " << SessionManager<RecordingModuleInterface::session>::getReferenceCount(addrStr) <<  endl;
			}
	
			try
			{
				if (updSes!=recSes)
				{
					cout << "RecordingModuleInterfaceSaveStreamThread releasing updSes session " << i << " @" << sessionAddr.toString() << ", refCount: " << SessionManager<RecordingModuleInterface::session>::getReferenceCount(sessionAddr.toString()) << endl;
					SessionManager<RecordingModuleInterface::session>::release(updSes);
				}
			}
			catch (Exception &e)
			{
				cout << "RecordingModuleInterfaceSaveStreamThread line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]" << CHECKPOINT_TOSTRING() << 
				" error locu release ¿?¿?: " << e.getClass() << ": " << e.getMsg() << endl;
			}
		}
	
	this->stopCond.lock();
	this->stopped = true;
	this->stopCond.broadcast();
	this->stopCond.unlock();
	// Descomptem l'stream :D
	RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.lock();
	RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount--;
			if (RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::almActiveRaised)
			{
				cout << "RecordingModuleInterfaceSaveStreamThread line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: lower limit alarm"<< endl;
				almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_limit_reached,"", false));//(( StrUtils::decToString(RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount)));
				RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::almActiveRaised=true;
			}
			if (RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount<RecordingModuleInterface::warnActiveRecordingsCount && RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::warnActiveRaised)
			{
					almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_limit_warning,"", false));//(( StrUtils::decToString(RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount)));
					RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::warnActiveRaised=false;
			}

	cout << "RMISST: activeCount--: " << RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount << endl;
	RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.unlock();

	return NULL;
}

// ***
// *** RecordingModuleInterfaceLimitsThread
// ***
// Thread que comprova els limits especificats, en plan lazy, per cada id de dispositiu.
#pragma mark *** RecordingModuleInterfaceLimitsThread
class RecordingModuleLimitsThread : public Thread
{
public:

protected:
	RecordingModuleInterface *intf;
	bool run;
	RWLock stopMutex;
//	Mutex stopMutex;
	qword KiBLimit;
	bool warnTimeRaised;
	
	qword calcDirSize(string dir);
	qword calcKiBSize();
	list<string> videoFilesByDate(string dir);
	list<string> videoFilesByDate();
	
	static bool videoFilesOrder(string a, string b);
	bool fileInSegment(string file, RecordingModuleInterface::recordingSegment *segment);
	
public:
	RecordingModuleLimitsThread(RecordingModuleInterface *intf, qword kLim);
	~RecordingModuleLimitsThread();
	
	qword getKiBLimit();
	void setKiBLimit(qword kLim);

	void lockRun();
	void unlockRun();

	void stopRun();
	virtual void* execute(int id, void* params);
};

bool RecordingModuleLimitsThread::videoFilesOrder(string a, string b)
{
	// yyyy/mm/dd/nnnnnnnn.srf
	if (a.length()>=23 && b.length()>=23)
	{
		string asub=a.substr(a.length()-23, 23);
		string bsub=b.substr(b.length()-23, 23);
		return (asub < bsub);
	}
	return false;
}

bool RecordingModuleLimitsThread::fileInSegment(string file, RecordingModuleInterface::recordingSegment *segment)
{
	string fileDevId, segmentDevId;
	string startFile, endFile;

	if (file.length()>=26)
	{
		//	longitud 26 = xx/2009/08/08/00000033.srf
		int currLen=26;
		fileDevId=file.substr(file.length()-currLen, currLen);
		
		while (fileDevId.substr(0, 1) != string("/"))
		{
			currLen++;
			fileDevId=file.substr(file.length()-currLen, currLen);
		}
		
		// currLen - 26 = currlen - 24 del path - 1 de la /
		fileDevId=fileDevId.substr(1, currLen-25);
	}
	else
		return false;

	segmentDevId=StrUtils::decToString(segment->devId);

//	cout << "\n\tfile in segment devId: " << segmentDevId << " vs " <<
//		fileDevId << endl;
	
	if (fileDevId != segmentDevId)
		return false;

	RecordingFileDateMetadataChunk start, end;

	start.setDate(segment->startDate.secs, segment->startDate.millis);
	end.setDate(segment->endDate.secs, segment->endDate.millis);
	startFile=this->intf->getAutomaticFileName(segment->devId, &start);
	endFile=this->intf->getAutomaticFileName(segment->devId, &end);

	if (startFile.length()>=23 && endFile.length()>=23 && file.length()>=23) 
	{
		startFile=startFile.substr(startFile.length()-23, 23);
		endFile=endFile.substr(endFile.length()-23, 23);
		file=file.substr(file.length()-23, 23);

//		cout << "\n\tfile in segment start: " << startFile << " end: " <<
//			endFile << " file: " << file << " res: " << (startFile <= file && file <= endFile) << endl;
	
		return (startFile <= file && file <= endFile);
	}
	
	return false;
}

qword RecordingModuleLimitsThread::calcDirSize(string dirPath)
{
#ifdef WIN32
	qword bytesSize=0;

	DWORD size;
	
	HANDLE DIR = CreateFile(StringToLPCWSTR(dirPath),FILE_READ_ATTRIBUTES|FILE_LIST_DIRECTORY, FILE_SHARE_READ,
		NULL,OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS,NULL);

	GetFileSize(DIR,&size);
	bytesSize=size;
	return bytesSize;
#else
	qword bytesSize=0;
	struct dirent entry, *pentry=NULL;
	struct stat st;
	
	DIR *dir=opendir(dirPath.c_str());
	if (dir==NULL)
	{
		cout << "\n" << __FILE__ << ", line " << __LINE__ << " dir null\n" << endl;
		return 0;
	}
	
	while ((readdir_r(dir, &entry, &pentry)==0) && pentry!=NULL)
	{
		if (entry.d_type&DT_DIR)
		{
			if (strcmp(entry.d_name, ".") && strcmp(entry.d_name, ".."))
				bytesSize+=calcDirSize(dirPath+string("/")+string(entry.d_name));
		}
		else
		{
			string path=dirPath+string("/")+string(entry.d_name);
			int statres=stat(path.c_str(), &st);
			if (statres==0)
			{
				bytesSize+=st.st_size;
			}
			else
			{
				cout << StrUtils::getDateString() <<
					"RecordingModuleLimitsThread::calcDirSize, line " <<
					__LINE__ << ": stat(" << path << ") error: " << errno <<
					endl;
			}
		}
	}
	
	closedir(dir);
	
	return bytesSize;
#endif
}


qword RecordingModuleLimitsThread::calcKiBSize()
{
	return calcDirSize(intf->getBaseDir())/1024;
}

list<string> RecordingModuleLimitsThread::videoFilesByDate(string dirPath)
{
#ifdef WIN32
	list<string> files;
	
/*	HANDLE DIR = CreateFile(StringToLPCWSTR(dirPath),FILE_READ_ATTRIBUTES|FILE_LIST_DIRECTORY, FILE_SHARE_READ,
		NULL,OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS,NULL);

	if (DIR==NULL)
	{
		cout << "\n" << __FILE__ << ", line " << __LINE__ << " dir null" << endl;
		return files;
	}
*/
	WIN32_FIND_DATA FindFileData;
	HANDLE hFind;

	hFind = FindFirstFile(StringToLPCWSTR(dirPath), &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) 
	{
		return files;
	} 
	else 
	{
		CW2A name(FindFileData.cFileName);
		files.push_back(string(name));
		FindClose(hFind);

		// List all the other files in the directory.
		while (FindNextFile(hFind, &FindFileData) != 0) 
		{
			CW2A name(FindFileData.cFileName);
			files.push_back(string(name));
		}

		DWORD dwError = GetLastError();
		FindClose(hFind);
		if (dwError != ERROR_NO_MORE_FILES) 
		{
			_tprintf (TEXT("FindNextFile error. Error is %u.\n"), 
				dwError);
		}
	}

	return files;
#else
	list<string> files;

	struct dirent entry, *pentry=NULL;
	
	DIR *dir=opendir(dirPath.c_str());
	if (dir==NULL)
	{
		cout << "\n" << __FILE__ << ", line " << __LINE__ << " dir null\n" << endl;
		return files;
	}
	
	while ((readdir_r(dir, &entry, &pentry)==0) && pentry!=NULL)
	{
		if (entry.d_type&DT_DIR)
		{
			if (strcmp(entry.d_name, ".") && strcmp(entry.d_name, ".."))
			{
				list <string> moreFiles=videoFilesByDate(dirPath+string("/")+string(entry.d_name));
				files.merge(moreFiles);
			}
		}
		else
		{
			string path=dirPath+string("/")+string(entry.d_name);
			
			if (path.length()>4 && path.substr(path.length()-4, 4)==string(".srf"))
			{
				files.push_back(path);
			}
		}
	}
	
	closedir(dir);
	
	return files;
#endif
}

list<string> RecordingModuleLimitsThread::videoFilesByDate()
{
	return videoFilesByDate(intf->getBaseDir());
}

RecordingModuleLimitsThread::RecordingModuleLimitsThread(RecordingModuleInterface *intf, qword kLim) : intf(intf), KiBLimit(kLim)
{
	STACKTRACE_INSTRUMENT();
	warnTimeRaised=false;
}

RecordingModuleLimitsThread::~RecordingModuleLimitsThread()
{
	STACKTRACE_INSTRUMENT();
	warnTimeRaised=false;
}
	
qword RecordingModuleLimitsThread::getKiBLimit()
{
	return KiBLimit;
}

void RecordingModuleLimitsThread::setKiBLimit(qword kLim)
{
	KiBLimit=kLim;
}

void RecordingModuleLimitsThread::lockRun()
{
	stopMutex.rlock();
	//stopMutex.lock();
}

void RecordingModuleLimitsThread::unlockRun()
{
//	if (stopMutex.isHeldByCurrentThread())
		stopMutex.unlock();
//	else
//		cout << __FILE__ << " Line " << std::dec << __LINE__ << " stopMutex NO lockat" << endl;
}

void RecordingModuleLimitsThread::stopRun()
{
	stopMutex.wlock();
//	stopMutex.lock();
	this->run=false;
	stopMutex.unlock();
}

void* RecordingModuleLimitsThread::execute(int id, void* params)
{
	STACKTRACE_INSTRUMENT();
	INIT_CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif


	list<Canis::subsys> recmods;
	run=true;
//	byte *oldParams=NULL;
	
	
	// Partim de que som l'unic thread que esborra continguts, per tant
	// no ens poden desapareixer ni fitxers ni indexos si no es perquè els
	// borrem nosaltres. stopMutex no hauria de ser necessari, si protegim
	// l'index correctament amb wlockRecIndex
	bool recLocked = false, stopLocked=false;
	while (run)
	{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
		recLocked=false;
		stopLocked=false;
		try
		{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			// Aquestes dues operacions son safe perque nomes les fem aqui i 
			// p.e. en calcular el tamany dels directoris no pot desapareixer
			// res, ja que aqui som els responsables del borrat de fitxers.
			qword KiB=calcKiBSize();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			qword KiBLimit=intf->getSizeLimit();


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			cout << "==================" << endl;
			cout << StrUtils::getDateString() << " RecMod Store Size: " << std::dec << KiB << "KiB, limit: " << std::dec << KiBLimit << "KiB, " << std::dec << (KiB*100/KiBLimit) << "%" << endl;
			cout << "==================" << endl;

			if (KiB > KiBLimit)
			{
				CHECKPOINT();
				qword needDeleteAtLeast=(KiB-KiBLimit)*1024ll;
				qword willDelete=0;
				list<string> filesToDelete;
				list<qword> invalidatedSegments;
				
				cout << "Size over limit. Cleaning up..." << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				// Identificacio de gravacions a netejar
				list <string> files=videoFilesByDate();
				files.sort(RecordingModuleLimitsThread::videoFilesOrder);
				
//				for (list<string>::iterator it=files.begin(); it!=files.end(); it++)
//				{
//					struct stat st;
//					string fname=(*it);
//
//					if (!stat(fname.c_str(), &st))
//					{
//						cout << fname << " (" << st.st_size << ")" << endl;
//					}
//					it++;
//				}

				// Alteracio de dates d'inici
				RecordingFileDateMetadataChunk *now=new RecordingFileDateMetadataChunk();
				RecordingFileDateMetadataChunk *workDate=new RecordingFileDateMetadataChunk();
				RecordingFileDateMetadataChunk *start=new RecordingFileDateMetadataChunk();

				RecordingFileDateMetadataChunk *lastDeleted=new RecordingFileDateMetadataChunk();
				lastDeleted->setDate(0,0);


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				// SOBRA, perque ja el fa getRecIndex
//				RecordingModuleInterface::retainRecIndex();
				CHECKPOINT();
				RecordingModuleInterface::recordingIndex *ri =
					RecordingModuleInterface::getRecIndex();
				
				// Mentre siguem dins del thread fent modificacions "grans",
				// no escrivim a disc
				ri->setWriteToDisk(false);
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				for (list<string>::iterator it=files.begin(); it!=files.end() && willDelete<needDeleteAtLeast; it++)
				{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					struct stat st, stDel;
					string fname=(*it);

//					cout << "\n=========== LimitsThread Checking " << fname << endl;
//	Timer t;
//	t.start();
					bool statb=stat(fname.c_str(), &st);
//	TimerInstant ti=t.time();
//	cout<<"\n=========== RecordingModuleLimitsThread::Checking size, locked, stat time:"<<ti.seconds()<<" usecs:"<<ti.useconds()<<endl;


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					if (!statb)
					{
						bool matched=false;
						bool neededByASegment=false;
					
						// Comprovem el fitxer actual respecte tots els segments
						for (qword s=0; s<ri->nSegments; s++)
						{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(s);
							RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;
		
							// Veiem si el fitxer pertany al segment.
							neededByASegment = neededByASegment ||
								this->fileInSegment(fname, &r);
		
							start->setDate(r.startDate.secs, r.startDate.millis);
							
							// Nom de fitxer d'inici del segment
							string file=intf->getAutomaticFileName(r.devId, start);
																	
							// Si el nom de fitxer d'inici del segment no coincideix amb el nom de fitxer que volem comprovar, pos a pel seguent.			
							if (file!=fname)
							{
//								cout << "\n\t=[     ]= LimitsThread SegmentStart != currCheckingFile: " << file << " != " << fname << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								continue;
							}
//							else
//								cout << "\n\t=[Match]= LimitsThread SegmentStart == currCheckingFile: " << file << " == " << fname << endl;
							

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							// Independentment de si ha matchat o no
							// anteriorment un altre fitxer, hem de comprovar
							// perque pot ser que hi hagi altres segments
							// que matxin, i s'han d'esborrar.
							matched=true;
							
							// Nom del fitxer sobre el que estem gravant actualment (si el segment esta gravant)	
							string fileNow=intf->getAutomaticFileName(r.devId, now);
							string base=intf->baseDir+string("/")+StrUtils::decToString(r.devId)+string("/");
							
							// El fitxer que estem comprovant, path "relatiu"
							string fileInt=file.substr(base.length(), file.length()-base.length());

							// El fitxer que estem gravant ara, si es q gravem, path "relatiu"
							string fileNowInt=fileNow.substr(base.length(), fileNow.length()-base.length());
							
//							cout << fileInt << " vs " << fileNowInt << endl;
							
							// ens assegurem q no borrem l'ultim segment possible, q podria estar actiu

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							if (fileInt<fileNowInt)
							{			
								cout << "\n\t=[Buidat]= LimitsThread SegmentStartFile < currentFile: " << fileInt << " < " << fileNowInt << endl;
						
								list<string> parts=StrUtils::split(fileInt, string("/"));
									
								dword year=atoi(parts.front().c_str());
								parts.pop_front();
								dword month=atoi(parts.front().c_str());
								parts.pop_front();
								dword day=atoi(parts.front().c_str());
								parts.pop_front();
								dword dayPart=atoi(parts.front().c_str());
		
								// inici del fitxer que segueix al que estem mirant
								qword nextSecs=(dayPart+1)*RecordingModuleInterface::SRFSeconds;
								
								workDate->setDate(year, month, day);
								
								qword unixTime=workDate->getSecs();
		
//								cout << "\n\t=== LimitsThread === " << year << "/" << month << "/" << day << "/" << dayPart << " - " << workDate->getSecs() << ": " << workDate->getHumanReadableYear() << "/" << (int)workDate->getHumanReadableMonth() << "/" << (int)workDate->getHumanReadableDay() << "(" << (int)workDate->getHumanReadableHour() << ":" << (int)workDate->getHumanReadableMinute() << ":" << (int)workDate->getHumanReadableSecond() << " --- Should be 00:00:00)";
								
								unixTime+=nextSecs;
								
								// unixTime es el moment en segons en que
								// comença fileInt+1
								
								workDate->setDate(unixTime, 0);
								string nextFile=intf->getAutomaticFileName(r.devId, workDate);
//								cout << "\n\t=== LimitsThread === next Time: " << unixTime << " (" << nextFile << ")";
//								cout << "\n\t=== LimitsThread === work Date: " << workDate->getSecs() << ": " << workDate->getHumanReadableYear() << "/" << (int)workDate->getHumanReadableMonth() << "/" << (int)workDate->getHumanReadableDay() << (int)workDate->getHumanReadableHour() << ":" << (int)workDate->getHumanReadableMinute() << ":" << (int)workDate->getHumanReadableSecond();
								cout.flush();
	

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								// Si hem arribat aqui, borrarem el fitxer, i
								// cal que modifiquem el segment de gravacio
								// Pot ser que estiguem tractant un segment
								// que inclogui un fitxer que ja hagi estat
								// tractat per algun altre segment i per tant
								// cal comprovar-ho. No es problema pel que fa
								// a borrar el fitxer, pero hem d'evitar
								// comptabilitzar l'espai ocupat mes d'un cop
								bool ftdFound=false;
								for (list<string>::iterator fIt=filesToDelete.begin(); fIt!=filesToDelete.end(); fIt++)
								{
									string ftd=*fIt;
									if (ftd==file)
									{
										ftdFound=true;
										break;
									}
								}
								

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								if (!ftdFound)
								{
									filesToDelete.push_back(file);
									if(workDate->getSecs() > lastDeleted->getSecs())
										lastDeleted->setDate(workDate->getSecs(),workDate->getMillis());
									willDelete+=st.st_size;
//									cout << "\n\t=== LimitsThread === QUEUE. willDelete: " << willDelete << ", needDelete: " << needDeleteAtLeast;
									cout.flush();
								}
								

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								// Casos
								// 1) es el principi, pero nomes una part
								//		modificar startDate 
								// 2) es el final, pero nomes una part
								//		No hauria de passar sense haver borrat
								//		abans el principi!!!!
								//		peroi wenu, modifiquem enddate :P
								// 3) esta totalment contingut
								// 		petar gravacio del tot
								
								qword fileStart=unixTime;
								qword fileEnd=unixTime+RecordingModuleInterface::SRFSeconds;
												
								CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								RecordingModuleInterface::wlockRecIndex();
								recLocked = true;
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								// Com que fins ara no hem tocat res, rellegim per
								// tocar
								rs=ri->getSegment(s);
							

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								r=*(RecordingModuleInterface::recordingSegment *)rs;
							
								if (r.startDate.secs<fileStart)
								{
									// els ms ens son igual.
									r.startDate.secs=fileStart;
									r.startDate.millis=0;
								}
								

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								if (r.endDate.secs<fileStart)
								{
									r.startDate.secs=fileStart-1;
									r.startDate.millis=0;
									r.endDate.secs=fileStart-1;
									r.endDate.millis=0;

									invalidatedSegments.push_back(s);
								}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								
								ri->alterSegment(s, r);
								

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								CHECKPOINT();
								RecordingModuleInterface::unlockRecIndex();
								recLocked = false;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				
							}
						}

						// Un cop comprovat el fitxer actual respecte tots els
						// segments mirem si l'hem afegit a filesToDelete

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						bool ftdFound=false;
						for (list<string>::iterator fIt=filesToDelete.begin(); fIt!=filesToDelete.end(); fIt++)
						{
							string ftd=*fIt;
							if (ftd==fname)
							{
								ftdFound=true;
								break;
							}
						}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

						if (!matched && !neededByASegment && !ftdFound)
						{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							// Nomes l'afegim si no ho hem fet ja (!matched
							// i !ftdFound) i no forma part de cap segment
							// (!needed...)
							filesToDelete.push_back(fname);
							willDelete+=st.st_size;
							cout << "\n\t=== LimitsThread === File " << fname << " NOT matched and NOT needed by any segment. Should it be deleted? [YES/NO] YES_\n\twillDelete: " << willDelete << ", needDelete: " << needDeleteAtLeast;
							cout.flush();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						}			
					}
					else
					{
						switch (errno)
						{
							case ENOTDIR:
								cout << "ENOTDIR" << endl;
								break;
							case ENAMETOOLONG:
								cout << "ENAMETOOLONG" << endl;
								break;
							case ENOENT:
								cout << "ENOENT" << endl;
								break;
							case EACCES:
								cout << "EACCES" << endl;
								break;
#ifndef WIN32
							case ELOOP:
								cout << "ELOOP" << endl;
								break;
#endif
							case EFAULT:
								cout << "EFAULT" << endl;
								break;
							case EIO:
								cout << "EIO" << endl;
								break;
								
							default:
								cout << "E_" << errno << endl;
								break;
						}
						
					}
					
				}


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				filesToDelete.sort(RecordingModuleLimitsThread::videoFilesOrder);
				filesToDelete.unique();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				invalidatedSegments.sort();
				invalidatedSegments.unique();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				cout << "\n=== LimitsThread === lastDeleted:" << (int)lastDeleted->getHumanReadableMonth() << "/" << (int)lastDeleted->getHumanReadableDay() << " " << (int)lastDeleted->getHumanReadableHour() << ":" << (int)lastDeleted->getHumanReadableMinute() << ":" << (int)lastDeleted->getHumanReadableSecond() << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				if(now->getSecs() - lastDeleted->getSecs() < RecordingModuleInterface::warnRecycleTime && !warnTimeRaised)
				{		
					intf->almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_time_warning, "", true));
					warnTimeRaised = true;
				}else if(warnTimeRaised && now->getSecs() - lastDeleted->getSecs() > RecordingModuleInterface::warnRecycleTime)
				{
					intf->almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_time_warning, "", false));
					warnTimeRaised = false;
				
				}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
//				cout << "\n\t=== LimitsThread === Invalidated segments" << endl;
				qword offset=0;
				// Nomes bloquejem si anem a fer alguna cosa
				// La modificacio de sessions es estrictament necessari que
				// quedi protegida fortament, juntament amb la invalidacio
				// de segments
				CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				stopMutex.wlock();
				stopLocked=true;
//				stopMutex.lock();

				CHECKPOINT();
				//20110314: Hem comptobat que en ocasions en bloqueja en aquest segment de coda. No sabém si per l'wlockRecIndex o en el bucle. S'han afegit els couts

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				RecordingModuleInterface::wlockRecIndex();
				recLocked = true;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				cout << "\n\t=== LimitsThread === altering " << invalidatedSegments.size() << " segments from " << ri->nSegments << " total " << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				for (list<qword>::iterator it=invalidatedSegments.begin(); it!=invalidatedSegments.end(); it++)
				{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					qword seg=(*it);
//					cout << "\n\t=== LimitsThread === " <<  seg << " (" << (seg-offset) << ")" << endl;
					seg-=offset;
					offset++;
					

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					for (qword s=seg+1; s<ri->nSegments; s++)
					{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
//						cout << "getSeg: " << s << "(numSegs: " << ri->nSegments << ")" << endl;
				CHECKPOINT();
						const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(s);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;
						
//						cout << "alterSeg: " << s-1  << "(numSegs: " << ri->nSegments << ")" << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						ri->alterSegment(s-1, r);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					}
					

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
//					cout << "nSegments--: " << ri->nSegments << " to ";
					ri->nSegments--;
//					cout << ri->nSegments << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				}
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				CHECKPOINT();
				SessionManager<RecordingModuleInterface::session>::Iterator sesIt=SessionManager<RecordingModuleInterface::session>::iterator();
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				cout << "\n\t=== deleted/modified segments, invalidating sessions, line:"<<__LINE__ << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				while (sesIt.hasNext())
				{
					RecordingModuleInterface::session *s=sesIt.next();
					
					s->recIndexIdx=0xffffffffffffffffll;
					SessionManager<RecordingModuleInterface::session>::releaseFromIterator(sesIt.getLastId());
				}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				SessionManager<RecordingModuleInterface::session>::releaseIterator();


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				RecordingModuleInterface::unlockRecIndex();
				recLocked = false;


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				stopMutex.unlock();
				stopLocked=false;
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				invalidatedSegments.clear();
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				CHECKPOINT();
				RecordingModuleInterface::wlockRecIndex();
				recLocked = true;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				ri->dumpToDisk();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
//				ri->setWriteToDisk(true);
				RecordingModuleInterface::unlockRecIndex();
				recLocked = false;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				
				// Invalidem els recIndexIdx de les sessions
				// TODO 20090507: Probablement aixo podria ser mes eficient i
				// invalidar nomes les sessions afectades. Potser cal
				// integrar-ho a dalt pq sigui aixi.			

				// Finalment, borrem els fitxers
				// TODO 20090626: Aixo es pot moure fora del lock de recIndex.
				// Es pot updatar RecIndex i despres ja hi ha temps de borrar
				// els arxius.
				cout << "\n\t=== LimitsThread === Files to Delete (" << willDelete << " bytes)" << endl;
				for (list<string>::iterator it=filesToDelete.begin(); it!=filesToDelete.end(); it++)
				{
					string fname=(*it);
					cout << "\n\t=== LimitsThread === Deleting: " << fname << endl;
					unlink(fname.c_str());
				}
				
				filesToDelete.clear();
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				// Un cop hem borrat arxius, comprovem que tots els segments tenen arxius a la punta inicial, i si no retallem.
				// Tambe comprovem que la gravacio te alguna longitud, i si no
				// la petem, tingui o no arxius...
				for (qword s=0; s<ri->nSegments; s++)
				{				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(s);
					RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					start->setDate(r.startDate.secs, r.startDate.millis);
					string file=intf->getAutomaticFileName(r.devId, start);
					CHECKPOINT();
					
					FILE *f=NULL;
					f=fopen(file.c_str(), "rb");
					while (f==NULL
						&& (r.startDate.secs < r.endDate.secs ||
							(r.startDate.secs == r.endDate.secs
							&& r.startDate.millis <= r.endDate.millis)))
					{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						// No existeix el fitxer, tirem fins el seguent interval
						dword year=start->getHumanReadableYear();
						dword month=start->getHumanReadableMonth();
						dword day=start->getHumanReadableDay();
						workDate->setDate(year, month, day);
						qword dayUnixTime=workDate->getSecs();
						qword startUnixTime=start->getSecs();
						qword daySecs = startUnixTime - dayUnixTime;
						qword daySegment = daySecs / RecordingModuleInterface::SRFSeconds;
						daySegment++;
						daySecs = daySegment * RecordingModuleInterface::SRFSeconds;
						
						cout << "\n\t=== LimitsThread === Segment " <<  s << " empty start (" << year << "/" << month << "/" << day << "/" << (daySegment-1) << ".srf), advancing 1 segment (to " << daySecs << ")" << endl;
						r.startDate.secs=dayUnixTime + daySecs;
						r.startDate.millis=0;
						
						CHECKPOINT();
						if (r.startDate.secs < r.endDate.secs ||
							(r.startDate.secs == r.endDate.secs
							&& r.startDate.millis < r.endDate.millis))
						{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							// Continuem provant...
							start->setDate(r.startDate.secs, r.startDate.millis);
							string file=intf->getAutomaticFileName(r.devId, start);
							f=fopen(file.c_str(), "rb");
						}
						else
						{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							// Gravacio buida. Comprovem que no hi hagi arxiu
							// i si n'hi ha el deixem per borrar...
							start->setDate(r.startDate.secs, r.startDate.millis);
							string file=intf->getAutomaticFileName(r.devId, start);
							f=fopen(file.c_str(), "rb");

							if (f!=NULL)
								filesToDelete.push_back(file);

							// forcem l'acabat del bucle
							r.startDate.secs=1;
							r.startDate.millis=0;
							r.endDate.secs=0;
							r.endDate.millis=0;

							invalidatedSegments.push_back(s);
						}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					}
					

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					if (f!=NULL)
						fclose(f);

//					cout << "\n\t=== LimitsThread === alter Segment " <<  s << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					ri->alterSegment(s, r);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				}

				// Finalment, borrem els fitxers de gravacions buides...
				filesToDelete.sort();
				filesToDelete.unique();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				cout << "\n\t=== LimitsThread === Deleting empty recordings' files" << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				for (list<string>::iterator it=filesToDelete.begin(); it!=filesToDelete.end(); it++)
				{
					string fname=(*it);
					cout << "\n\t=== LimitsThread === Deleting: " << fname << endl;
					unlink(fname.c_str());
				}
				

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				filesToDelete.clear();


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				CHECKPOINT();
				delete now;
				delete workDate;
				delete start;


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				// TODO 20091112: ATENCIO CODI REPETIT
				//		=> INVALIDACIO DE SEGMENTS
				invalidatedSegments.sort();
				invalidatedSegments.unique();


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				// Nomes bloquejem si anem a fer alguna cosa
				// La modificacio de sessions es estrictament necessari que
				// quedi protegida fortament, juntament amb la invalidacio
				// de segments
				CHECKPOINT();
				stopMutex.wlock();
				stopLocked=true;
//				stopMutex.lock();
				//20091116 : no arrastrém l'offset, que la lia
                //cout << "\n\t=== LimitsThread  set offset = 0 " << "( prev:" << offset << ")" << endl;
				offset = 0;


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				CHECKPOINT();
				RecordingModuleInterface::wlockRecIndex();
				recLocked = true;


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				for (list<qword>::iterator it=invalidatedSegments.begin(); it!=invalidatedSegments.end(); it++)
				{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					qword seg=(*it);
				//	cout << "\n\t=== LimitsThread === " <<  seg << " (" << (seg-offset) << ")" << endl;
					cout << "\n\t=== deleting segment: " <<  seg << " (" << (seg-offset) << ")" << endl;
					seg-=offset;
					offset++;
					

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					for (qword s=seg+1; s<ri->nSegments; s++)
					{
//						cout << "getSeg: " << s << "(numSegs: " << ri->nSegments << ")" << endl;
						CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(s);
						RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;
						

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
//						cout << "alterSeg: " << s-1  << "(numSegs: " << ri->nSegments << ")" << endl;
						ri->alterSegment(s-1, r);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					}
					

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
//					cout << "nSegments--: " << ri->nSegments << " to ";
					ri->nSegments--;
//					cout << ri->nSegments << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				}
				
				CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				sesIt=SessionManager<RecordingModuleInterface::session>::iterator();
				
				cout << "\n\t=== deleted/modified segments, invalidating sessions, line:"<<__LINE__ << endl;
				while (sesIt.hasNext())
				{
					RecordingModuleInterface::session *s=sesIt.next();
					
					s->recIndexIdx=0xffffffffffffffffll;
					SessionManager<RecordingModuleInterface::session>::releaseFromIterator(sesIt.getLastId());
				}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				SessionManager<RecordingModuleInterface::session>::releaseIterator();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				RecordingModuleInterface::unlockRecIndex();
				recLocked = false;


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				stopMutex.unlock();
				stopLocked=false;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				
				invalidatedSegments.clear();

				// TODO 20091112: FI CODI REPETIT
				//		=> INVALIDACIO DE SEGMENTS



#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			}

			CHECKPOINT();
			
			// QUANT hem d'esperar entre comprovacions?
			RecordingModuleInterface::recordingIndex *ri=
				RecordingModuleInterface::getRecIndex();
			for (int i=0; i<30; i++)
			{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				// TODO: la resta de locking :P
				RecordingModuleInterface::wlockRecIndex();
				recLocked = true;
				ri->dumpToDisk();
				RecordingModuleInterface::unlockRecIndex();
				recLocked = false;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

				sleep(1);
			}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
		}
		catch (Exception &e)
		{
			try
			{
				cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << 
					e.getClass() << ": " << e.getMsg() << endl;
			}catch(...){}
			if(recLocked)
			{
				try
				{
					cout << __FILE__ << ", line " << std::dec << __LINE__ << " Unlocking RecIndex " << endl;
					RecordingModuleInterface::unlockRecIndex();
				}
				catch(Exception &e)
				{
					cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << 
						e.getClass() << ": " << e.getMsg() << endl;
				}
			}
			
			try
			{
				// No hi ha res fora del lock que pugui llençar l'excepcio,
				// podem fer l'unlock tranquil·lament
				if (stopLocked)
					stopMutex.unlock();
			}
			catch(Exception &e)
			{
				cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << 
					e.getClass() << ": " << e.getMsg() << endl;
			}

			cout << "RecordingModuleLimitsThread: " << e.getClass() << ": " << e.getMsg() << endl;
		}
		catch (std::exception &stde)
		{
			try
			{
				cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << stde.what() << endl;
			}catch(...){}

			if(recLocked)
			{
				try
				{
					cout << __FILE__ << ", line " << std::dec << __LINE__ << " Unlocking RecIndex " << endl;
					RecordingModuleInterface::unlockRecIndex();
				}
				catch(Exception &e)
				{
					cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << 
						e.getClass() << ": " << e.getMsg() << endl;
				}
			}

			try{
				// No hi ha res fora del lock que pugui llençar l'excepcio,
				// podem fer l'unlock tranquil·lament
				if (stopLocked)
					stopMutex.unlock();
			}
			catch(Exception &e)
			{
				cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << 
					e.getClass() << ": " << e.getMsg() << endl;
			}
		}
		catch (...)
		{
			try
			{
				cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": unknown exception" << endl;
			}catch(...){}

			if(recLocked)
			{
				try
				{
					cout << __FILE__ << ", line " << std::dec << __LINE__ << " Unlocking RecIndex " << endl;
					RecordingModuleInterface::unlockRecIndex();
				}
				catch(Exception &e)
				{
					cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << 
						e.getClass() << ": " << e.getMsg() << endl;
				}
			}

			try{
				// No hi ha res fora del lock que pugui llençar l'excepcio,
				// podem fer l'unlock tranquil·lament
				if (stopLocked)
					stopMutex.unlock();
			}
			catch(Exception &e)
			{
				cout << __FILE__ << ", line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << 
					e.getClass() << ": " << e.getMsg() << endl;
			}
		}
	}	

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0;
#endif

	return NULL;
}


// ***
// *** RecordingModuleInterface::recordingIndex
// ***
#pragma mark *** RecordingModuleInterface::recordingIndex

RecordingModuleInterface::recordingIndex::recordingIndex(string fname) : f(NULL), nSegments(0),
	id(RecordingModuleInterface::recordingIndex::idInLocalEndian), segments(NULL), e(Endian::local),
	allocatedSegments(0), writeToDisk(true)
{
	STACKTRACE_INSTRUMENT();
	try
	{
		// Primer mirem si existeix, sense crear-lo
		f=new File(fname, File::readAccessMode|File::writeAccessMode);
		
		// Com que existeix, llegim
		f->read(&id, sizeof(dword));
		if (id!=RecordingModuleInterface::recordingIndex::idInLocalEndian)
		{
			if (Endian::local==Endian::BIG)
			{
				Endian::from(Endian::LITTLE, &id, sizeof(dword));
				e=Endian::LITTLE;
			}
			else
			{
				Endian::from(Endian::BIG, &id, sizeof(dword));
				e=Endian::BIG;
			}

			if (id!=RecordingModuleInterface::recordingIndex::idInLocalEndian)
				throw RecordingModuleException(0, "Invalid index file found");
		}

		f->read(&nSegments, sizeof(qword));
		Endian::from(e, &nSegments, sizeof(qword));
		
//		if (nSegments!=0)
		segments=new recordingSegment[nSegments+50];
		
		allocatedSegments=nSegments+50;
		
		byte *segBuf=new byte[recordingSegment::size()];
		recordingSegment seg;
		
		if (segments==NULL || segBuf==NULL)
		{
			if (segments!=NULL)
			{
				delete [] segments;
				segments=NULL;
			}
				
			if (segBuf!=NULL)
			{	
				delete [] segBuf;
				segBuf=NULL;
			}
			
			throw RecordingModuleException(0, "Not enough memory to initialize index");
		}
		
		for (qword i=0; i<nSegments; i++)
		{
			f->read(segBuf, recordingSegment::size());
			seg.from(e, segBuf);
			
			segments[i]=seg;
		}
		
		delete [] segBuf;
	}
	catch (Exception &e)
	{
		cout << "(" << e.getClass() << ": " << e.getMsg() << ")" << endl;
		if (f==NULL)
		{
			// Sembla que no existeix, provem de crear-lo
			f=new File(fname, File::readAccessMode|File::writeAccessMode, true);
			// Si no va be, ja anira l'excepcio cap a dalt.
		}
		else
			f->seek(0);		
		// L'hem creat, fem una primera escriptura
		f->write(&id, sizeof(dword));			// L'id
		f->write(&nSegments, sizeof(qword));	// es 0, no cal escriure cap segment

		allocatedSegments=50;
		segments=new recordingSegment[allocatedSegments];
		if (segments==NULL)
			throw RecordingModuleException(0, "Not enough memory to initialize index");
	}
}

RecordingModuleInterface::recordingIndex::~recordingIndex()
{
	STACKTRACE_INSTRUMENT();
	
//	cout << "~recordingIndex" << endl;
	if (f!=NULL)
	{
//		cout << "~recordingIndex del f" << endl;
		delete f;
		f=NULL;
	}	
	if (segments!=NULL)
	{
		delete [] segments;
		segments=NULL;
	}
}

qword RecordingModuleInterface::recordingIndex::addSegment(recordingSegment &seg)
{
	STACKTRACE_INSTRUMENT();
	if (nSegments+1>allocatedSegments)
	{
		allocatedSegments+=50;
		recordingSegment *newSegments=new recordingSegment[allocatedSegments];
		recordingSegment *oldSeg=segments;

		if (newSegments==NULL)
			throw RecordingModuleException(0, "Not enough memory to add new segment");

		if (segments!=NULL)
		{
			memmove(newSegments, segments, nSegments*sizeof(recordingSegment));
			segments=newSegments;
			delete [] oldSeg;
		}
		else	
		{
			segments=newSegments;
		}
	}
	
	if (this->isWriteToDisk())
	{
		fileOffset off=sizeof(id)+sizeof(nSegments)+nSegments*((fileOffset)recordingSegment::size());
		byte *segBuf=(byte*)seg.to(e);
		
		if (segBuf==NULL)
			throw RecordingModuleException(0, "Not enough memory to add new segment");
	
		f->seek(off, File::SEEK_FROMSTART);
		f->write(segBuf, recordingSegment::size());
		delete [] segBuf;
	}
		
	segments[nSegments]=seg;

	nSegments++;

	if (this->isWriteToDisk())
		this->writeNumSegments();

	return nSegments-1;		// Index del que hem afegit
}

void RecordingModuleInterface::recordingIndex::alterSegment(qword segIdx, recordingSegment &seg)
{
	STACKTRACE_INSTRUMENT();
	if (segIdx>=nSegments)
		throw RecordingModuleException(0, "Invalid index supplied");
		
	if (this->isWriteToDisk())
	{
		fileOffset off=sizeof(id)+sizeof(nSegments)+segIdx*((fileOffset)recordingSegment::size());
		byte *segBuf=(byte*)seg.to(e);
		
		if (segBuf==NULL)
			throw RecordingModuleException(0, "Not enough memory to add new segment");
	
		f->seek(off, File::SEEK_FROMSTART);
		f->write(segBuf, recordingSegment::size());
		delete [] segBuf;
	}
		
	segments[segIdx]=seg;
}

const RecordingModuleInterface::recordingSegment* RecordingModuleInterface::recordingIndex::getSegment(qword segIdx)
{
	STACKTRACE_INSTRUMENT();

//	cout << (void*)this << endl;
//	cout << (void*)segments << endl;
//	cout << (void*)segments[segIdx] << endl;
//	cout << segIdx << ", " << nSegments << endl;
		
	if (this==NULL || this->segments==NULL)
		throw RecordingModuleException(0, "FATAL: unexpected situation - recIndex::getSegment");
		
	if (segIdx>=nSegments)
		throw RecordingModuleException(0, "Invalid index supplied");
		
	return (const recordingSegment*)&segments[segIdx];
}

void RecordingModuleInterface::recordingIndex::writeNumSegments()
{
	qword nS=nSegments;
	Endian::to(e, &nS, sizeof(nS));
	f->seek((int)sizeof(id), File::SEEK_FROMSTART);
	f->write(&nS, sizeof(nS));
}

void RecordingModuleInterface::recordingIndex::setWriteToDisk(bool wr)
{
	this->writeToDisk=wr;
}

bool RecordingModuleInterface::recordingIndex::isWriteToDisk()
{
	return this->writeToDisk;
}

void RecordingModuleInterface::recordingIndex::dumpToDisk()
{
	for (dword s=0; s<this->nSegments; s++)
	{
		fileOffset off=sizeof(id)+sizeof(nSegments)+s*((fileOffset)recordingSegment::size());
		byte *segBuf=(byte*)this->segments[s].to(e);
		
		if (segBuf==NULL)
			throw RecordingModuleException(0, "Not enough memory to add new segment");
	
		f->seek(off, File::SEEK_FROMSTART);
		f->write(segBuf, recordingSegment::size());
		delete [] segBuf;
	}
	
	this->writeNumSegments();
}



// ***
// *** RecordingModuleInterface
// ***
#pragma mark *** Estatiques

//map<string,RecordingModuleInterface::session> RecordingModuleInterface::sessions;

RecordingModuleInterface::DWWatchdogCallback *RecordingModuleInterface::dwWatchdogCallback = 
	new RecordingModuleInterface::DWWatchdogCallback();
Watchdog *RecordingModuleInterface::dwWatchdog=
	new Watchdog(RecordingModuleInterface::watchdogSeconds, dwWatchdogCallback);
RecordingModuleDiskWriter *RecordingModuleInterface::diskWriter=
	new RecordingModuleDiskWriter(dwWatchdog);

list<int> RecordingModuleInterface::camsInAlert;
Mutex RecordingModuleInterface::camsAlertLock;
const float RecordingModuleInterface::maxCamPercentInAlert = 0.30;

bool RecordingModuleInterface::reassignPending = false;
Timer RecordingModuleInterface::reassignRequestTime;


RecordingModuleInterface::recordingIndex *RecordingModuleInterface::recIndex=NULL;
dword RecordingModuleInterface::recIndexRef=0;
RWLock RecordingModuleInterface::recIndexLock;
Mutex RecordingModuleInterface::recIndexRefLock(true);
DBGateway *RecordingModuleInterface::dbGW=NULL;
//Mutex RecordingModuleInterface::dbgwMutex;
Mutex RecordingModuleInterface::createDirLock;
IP RecordingModuleInterface::excludedSearchIP;
void *RecordingModuleInterface::awarenessThread=NULL;
void *RecordingModuleInterface::limitsThread=NULL;
RecordingModuleAlarmAndLogThread *RecordingModuleInterface::almLogThread=NULL;
map<int, RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread*> RecordingModuleInterface::camThreads;
string RecordingModuleInterface::baseDir;
//RWLock RecordingModuleInterface::cThrLock;
Mutex RecordingModuleInterface::cThrLock;
int RecordingModuleInterface::maxActiveRecordingsCount=75;
int RecordingModuleInterface::warnActiveRecordingsCount=40;
int RecordingModuleInterface::warnRecycleTime = 3600*24*4;

// OJO AQUESTA ES NOMES LOCAL: No mira BD
string RecordingModuleInterface::getSessionKeyForDevice(int devId)
{
	STACKTRACE_INSTRUMENT();
//	for (map<string,RecordingModuleInterface::session>::iterator sesIt=sessions.begin(); sesIt!=sessions.end(); sesIt++)
//	{
//		RecordingModuleInterface::session *s=&(sesIt->second);
//		
//		if (s->sessionParams.devId==devId)
//			return sesIt->first;
//	}

	SessionManager<session>::Iterator sesIt=SessionManager<session>::iterator();
	
	while (sesIt.hasNext())
	{
		RecordingModuleInterface::session *s=sesIt.next();
		
		if (s->sessionParams.devId==devId)
		{
			SessionManager<session>::releaseFromIterator(sesIt.getLastId());
			SessionManager<RecordingModuleInterface::session>::releaseIterator();
			return sesIt.getLastId();
		}
		SessionManager<session>::releaseFromIterator(sesIt.getLastId());
	}
	SessionManager<RecordingModuleInterface::session>::releaseIterator();

	// OJO AQUESTA ES NOMES LOCAL
	
	return string("");
}
string RecordingModuleInterface::getSessionKey(RecordingModuleInterface::session *s)
{
	STACKTRACE_INSTRUMENT();
	
	return SessionManager<session>::getKey(s);
}

// OJO AQUESTA ES NOMES LOCAL: No mira BD
string RecordingModuleInterface::getSessionKeyForDevice(int devId, string current)
{
	STACKTRACE_INSTRUMENT();
//	for (map<string,RecordingModuleInterface::session>::iterator sesIt=sessions.begin(); sesIt!=sessions.end(); sesIt++)
//	{
//		RecordingModuleInterface::session *s=&(sesIt->second);
//		
//		if (s->sessionParams.devId==devId)
//			return sesIt->first;
//	}

	SessionManager<session>::Iterator sesIt=SessionManager<session>::iterator();
	
	while (sesIt.hasNext())
	{
		RecordingModuleInterface::session *s=sesIt.next();
		
		if (s->sessionParams.devId==devId && sesIt.getLastId() != current)
		{
			SessionManager<session>::releaseFromIterator(sesIt.getLastId());
			SessionManager<RecordingModuleInterface::session>::releaseIterator();
			return sesIt.getLastId();
		}
		SessionManager<session>::releaseFromIterator(sesIt.getLastId());
	}
	SessionManager<RecordingModuleInterface::session>::releaseIterator();

	// OJO AQUESTA ES NOMES LOCAL
	
	return string("");
}

string RecordingModuleInterface::getSessionKeyForDevice(int devId, string current,  RecordingFileHeaderChunk::recType rType)
{
	STACKTRACE_INSTRUMENT();

	SessionManager<session>::Iterator sesIt=SessionManager<session>::iterator();
	
	while (sesIt.hasNext())
	{
		RecordingModuleInterface::session *s=sesIt.next();
		
		if (s->sessionParams.devId==devId && sesIt.getLastId() != current &&
			 s->sessionParams.recType==rType)
		{
			SessionManager<session>::releaseFromIterator(sesIt.getLastId());
			SessionManager<RecordingModuleInterface::session>::releaseIterator();
			return sesIt.getLastId();
		}
		SessionManager<session>::releaseFromIterator(sesIt.getLastId());
	}
	SessionManager<RecordingModuleInterface::session>::releaseIterator();

	// OJO AQUESTA ES NOMES LOCAL
	
	return string("");
}

RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread* RecordingModuleInterface::getRecThread(int devId, bool prelocked)
{
	if(!prelocked)
	{
		try
		{
			cThrLock.lock(true, string("getRecThread"));
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: getRecThread  cThrLock lock error, no unlock! "<<
				e.getClass() << ": " << e.getMsg() <<endl;
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
		catch(std::exception &stde)
		{
			cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"], getRecThread cThrLock lock error, no unlock! " << stde.what() << endl;
			throw(stde);
		}
	}

	RecordingModuleInterfaceSaveStreamThread *th=NULL;
	try
	{
		map<int, RecordingModuleInterfaceSaveStreamThread*>::iterator it=camThreads.find(devId);
		if(it != camThreads.end())//( && it->second->runningInstances()>0)
			th=it->second;
		if(!prelocked)
			cThrLock.unlock();
	}
	catch (Exception &e)
	{
		if(!prelocked)
		{
			try
			{
				cThrLock.unlock();
			}catch(...){}
		}
		cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] RMI::getRecThread:  cThrLock unlock! " << e.getClass() << ": " << e.getMsg() << endl;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	catch(std::exception &stde)
	{
		if(!prelocked)
		{
			try
			{
				cThrLock.unlock();
			}catch(...){}
		}
		cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"] RMI::getRecThread:  cThrLock unlock! " << stde.what() << endl;
		throw(stde);
	}
	return th;
} 

bool RecordingModuleInterface::recThreadStarted(int devId, bool prelocked)
{
	if(!prelocked)
	{
		try
		{
			cThrLock.lock(true, string("recThreadStarted"));
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: recThreadStarted  cThrLock lock error, no unlock! "<<
				e.getClass() << ": " << e.getMsg() <<endl;
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
		catch(std::exception &stde)
		{
			cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"], recThreadSctarted  cThrLock lock error, no unlock! " << stde.what() << endl;
			throw(stde);
		}
	}

	try
	{
		map<int, RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread*>::iterator it=camThreads.find(devId);
		if(it != camThreads.end() && (it->second->runningInstances()>0 && it->second->running))
		{
			if(!prelocked)
				cThrLock.unlock();
			return true;
		}
		if(!prelocked)
			cThrLock.unlock();
		return false;
	}
	catch (Exception &e)
	{
		if(!prelocked)
		{
			try
			{
				cThrLock.unlock();
			}catch(...){}
		}
		cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: recThreadStarted  cThrLock unlock! "<<
			e.getClass() << ": " << e.getMsg() <<endl;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	catch(std::exception &stde)
	{
		if(!prelocked)
		{
			try
			{
				cThrLock.unlock();
			}catch(...){}
		}
		cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << std::dec << devId <<"], recThreadSctarted  cThrLock unlock! " << stde.what() << endl;
		throw(stde);
	}
}

RecordingModuleInterface::session *RecordingModuleInterface::getSessionRecordingDevice(int devId, bool local, bool prelocked)
{
	STACKTRACE_INSTRUMENT();
//	cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice "<<prelocked<<endl;
//	for (map<string,RecordingModuleInterface::session>::iterator sesIt=sessions.begin(); sesIt!=sessions.end(); sesIt++)
//	{
//		RecordingModuleInterface::session *s=&(sesIt->second);
//		
//		if (s->sessionParams.devId==devId && s->recordingThread!=NULL
//			&& s->startedRecording)
//			return s;
//	}

	try
	{
		cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice recThreadStarted "<<prelocked<<endl;
		if(recThreadStarted(devId, prelocked))
		{
			cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice recThreadStarted true "<<endl;
	
//			cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice iterator"<<endl;
			SessionManager<session>::Iterator sesIt=SessionManager<session>::iterator();
//			cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice iterator"<<endl;
	
			while (sesIt.hasNext())
			{
//				cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice iterator next"<<endl;
				RecordingModuleInterface::session *s=sesIt.next();
//				cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice iterator next"<<endl;

				if (s->sessionParams.devId==devId && s->startedRecording)
				{
//					cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice release iterator return"<<endl;
					SessionManager<RecordingModuleInterface::session>::releaseIterator();
					cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice release iterator return"<<endl;
					return s;
				}

//				cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice releaseFromIterator"<<endl;
				SessionManager<session>::releaseFromIterator(sesIt.getLastId());
//				cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice releaseFromIterator"<<endl;
			}
//			cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice release iterator not found"<<endl;
			SessionManager<RecordingModuleInterface::session>::releaseIterator();
			cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice release iterator not found, throw Exception"<<endl;
			throw RecordingModuleException(0, "Recording thread started but no session found");
		}
		else
			cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice recThreadStarted false "<<endl;
	}
	catch (Exception &e)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: getSessionRecordingDevice local:"<<local <<
			e.getClass() << ": " << e.getMsg() <<endl;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}



	// No s'ha trobat, hauriem de mirar a la resta de moduls --- BD
	
	if(!local)
	{
		cout<<"["<<Thread::getId()<<"] RecordingModuleInterface::getSessionRecordingDevice no s'ha trobat la sessió, busquem a la resta de moduls el dev:"<<devId<<endl;
//TODO:_ mirar de infobalanceo la ultima actualitzaci´-o, que poit ser no esborrada
		string query=string("SELECT * FROM getRecModRecordingDevice('") + excludedSearchIP.toString() + string("' ,'")+StrUtils::decToString(devId)+string("');");

		cout << query << endl;

		XML *res=NULL;
		try
		{
//			dbgwMutex.lock();
			cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice query"<<endl;
			res=dbGW->query(query);
			cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice query"<<endl;
			//		cout<<" subsistemes:"<<res->toString()<<endl;
			if(res == NULL || res->getNode("/result") == NULL)
				throw NullPointerException("NULL Pointer at getSessionRecordingDevice(devId)");
			int nRows=atoi(res->getNode("/result")->getParam("numRows").c_str());

			if (nRows==0)
			{
				if (res!=NULL)
				{
					delete res;
					res=NULL;
				}
				cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice NO ROWS "<<nRows<<endl;
				return NULL;
			}

			string serv=res->getNode("/result/[0]/[0]")->getCdata();
			if(serv != string(""))
			{
				cout<<"["<<Thread::getId()<<"] getSessionRecordingDevice recording at "<<serv<<endl;
				int pos=serv.find(":");
				if(pos!=string::npos)
				{
					string ip = serv.substr(0, pos);
					int port = atoi(serv.substr(pos+1).c_str());
					Address serverAddr(IP(ip), port);
					if (res!=NULL)
					{
						delete res;
						res=NULL;
					}
					cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice RedirectCallException "<<endl;
					throw RedirectCallException(string("Redirect: ")+serv, serverAddr);
				}
			}
			cout<<"["<<Thread::getId()<<"] getSessionRecordingDevice no redirection"<<endl;
			
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}
			
//			try{
//			dbgwMutex.unlock();
//			}catch(Exception &e){}
		}catch(RedirectCallException &e)
		{
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}

//			try{
//			dbgwMutex.unlock();
//			}catch(Exception &e){}
			//e.serialize()->materializeAndThrow(true);
			throw;
		}catch(Exception &e)
		{
			cout<<"["<<Thread::getId()<<"] getSessionRecordingDevice : exception while checking DB:"<<e.getClass()<<" : "<<e.getMsg()<<endl;
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}
//			try{
//			dbgwMutex.unlock();
//			}catch(Exception &e){}
		}
	}
	cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice "<<prelocked<<endl;
	return NULL;
}

RecordingModuleInterface::session *RecordingModuleInterface::getSessionRecordingDevice(int devId, RecordingFileHeaderChunk::recType rType, bool local)
{
	STACKTRACE_INSTRUMENT();
//	for (map<string,RecordingModuleInterface::session>::iterator sesIt=sessions.begin(); sesIt!=sessions.end(); sesIt++)
//	{
//		RecordingModuleInterface::session *s=&(sesIt->second);
//		
//		if (s->sessionParams.devId==devId && s->recordingThread!=NULL
//			&& s->startedRecording && s->sessionParams.recType==rType)
//			return s;
//	}
	try
	{
		if(recThreadStarted(devId))
		{

			SessionManager<session>::Iterator sesIt=SessionManager<session>::iterator();

			while (sesIt.hasNext())
			{
				RecordingModuleInterface::session *s=sesIt.next();

				if (s->sessionParams.devId==devId && s->startRecStreamRecv && s->sessionParams.recType==rType)
				{
					SessionManager<RecordingModuleInterface::session>::releaseIterator();
					return s;
				}

				SessionManager<session>::releaseFromIterator(sesIt.getLastId());
			}
			SessionManager<RecordingModuleInterface::session>::releaseIterator();
		}
	}
	catch (Exception &e)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << devId <<"]: getSessionRecordingDevice type:" <<rType<<" local:"<<local<< 
			e.getClass() << ": " << e.getMsg() <<endl;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}



	// No s'ha trobat, hauriem de mirar a la resta de moduls --- BD
	
/*	string otherModRecsQuery=string("SELECT nc4.id, nc4.padre, (SELECT '/'||type||'/'||host(ip)||'/'||port FROM getSubsystemForConfigNode(nc4.id))||'/recordings/'||(SELECT nombre FROM nodoconfig WHERE id=nc4.padre)||'/'||nc4.nombre AS path, nc4.valor FROM nodoconfig nc4 WHERE nc4.padre IN (SELECT nc3.padre FROM nodoconfig nc3 WHERE (nc3.nombre='devId' AND nc3.valor='")+StrUtils::decToString(devId)+string("') AND nc3.padre IN (SELECT nc2.id FROM nodoconfig nc2 WHERE nc2.padre IN (SELECT nc1.id FROM nodoconfig nc1 WHERE nc1.nombre='recordings'))) ORDER BY nc4.padre, nc4.nombre");
	
	XML *res=dbGW->query(otherModRecsQuery);
	
	int nRows=atoi(res->getNode("/result")->getParam("numRows").c_str());
	
	if (nRows>0)
	{
		// Ho convertim en un CPConfigParamSeq
		CPConfigParamSeq conf;
		
		for (int r=0; r<nRows; r++)
		{
			CPConfigParam cp(res->getNode(string("/result/[")+StrUtils::decToString(r)+string("]/path"))->getCdata(), res->getNode(string("/result/[")+StrUtils::decToString(r)+string("]/valor"))->getCdata());
			
			conf.push_back(cp);
		}
	}
*/	


	if(!local)
	{
		cout<<"["<<Thread::getId()<<"] RecordingModuleInterface::getSessionRecordingDevice no s'ha trobat la sessió, busquem a la resta de moduls el dev:"<<devId<<endl;
//TODO:_ mirar de infobalanceo la ultima actualitzaci´-o, que poit ser no esborrada
		string query=string("SELECT * FROM getRecModRecordingDevice('") + excludedSearchIP.toString() + string("' ,'")+StrUtils::decToString(devId)+string("');");

		cout << query << endl;

		XML *res=NULL;
		try
		{
//			dbgwMutex.lock();
			cout<<"["<<Thread::getId()<<"] --> getSessionRecordingDevice query"<<endl;
			res=dbGW->query(query);
			cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice query"<<endl;
			//		cout<<" subsistemes:"<<res->toString()<<endl;
			if(res == NULL || res->getNode("/result") == NULL)
				throw NullPointerException("NULL Pointer at getSessionRecordingDevice(devId)");
			int nRows=atoi(res->getNode("/result")->getParam("numRows").c_str());

			if (nRows==0)
			{
				if (res!=NULL)
				{
					delete res;
					res=NULL;
				}
				cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice NO ROWS "<<nRows<<endl;
				return NULL;
			}

			string serv=res->getNode("/result/[0]/[0]")->getCdata();
			if(serv != string(""))
			{
				cout<<"["<<Thread::getId()<<"] getSessionRecordingDevice recording at "<<serv<<endl;
				int pos=serv.find(":");
				if(pos!=string::npos)
				{
					string ip = serv.substr(0, pos);
					int port = atoi(serv.substr(pos+1).c_str());
					Address serverAddr(IP(ip), port);
					if (res!=NULL)
					{
						delete res;
						res=NULL;
					}
					cout<<"["<<Thread::getId()<<"] <-- getSessionRecordingDevice RedirectCallException "<<endl;
					throw RedirectCallException(string("Redirect: ")+serv, serverAddr);
				}
			}
			cout<<"["<<Thread::getId()<<"] getSessionRecordingDevice no redirection"<<endl;
			
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}
			
//			try{
//			dbgwMutex.unlock();
//			}catch(Exception &e){}
		}catch(RedirectCallException &e)
		{
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}

//			try{
//			dbgwMutex.unlock();
//			}catch(Exception &e){}
			//e.serialize()->materializeAndThrow(true);
			throw;
		}catch(Exception &e)
		{
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}
//			try{
//			dbgwMutex.unlock();
//			}catch(Exception &e){}
		}
	}


/*	if(!local)
	{
		cout<<"RecordingModuleInterface::getSessionRecordingDevice no s'ha trobat la sessió, busquem a la resta de moduls el dev:"<<devId<<endl;

//		string query("SELECT s.ip, s.puerto, s.raizconfig FROM subsistema s, tiposubsistema ts WHERE ts.nombre='RecordingModule' AND ts.id=s.tipo AND s.raizconfig IS NOT NULL");

		string query=string("SELECT s.ip, s.puerto, s.raizconfig, ib.subsistema FROM subsistema s, tiposubsistema ts, infobalanceo ib WHERE ts.nombre='RecordingModule' AND ts.id=s.tipo AND host(s.ip)<>'") + excludedSearchIP.toString() + string("' AND ib.subsistema = s.id AND s.raizconfig IS NOT NULL");

		XML *res=NULL;
		try
		{
//			dbgwMutex.lock();
			res=dbGW->query(query);
			//cout<<" subsistemes:"<<res->toString()<<endl;
			if(res == NULL || res->getNode("/result") == NULL)
				throw NullPointerException("NULL Pointer at getSessionRecordingDevice(devId, rType)");
			int nRows=atoi(res->getNode("/result")->getParam("numRows").c_str());
			for(int i=0;i<nRows;i++)
			{
				string ip=res->getNode("/result/["+StrUtils::decToString(i)+"]/ip")->getCdata();
				string port=res->getNode("/result/["+StrUtils::decToString(i)+"]/puerto")->getCdata();
				string config=res->getNode("/result/["+StrUtils::decToString(i)+"]/raizconfig")->getCdata();
				if(config!=string(""))// raizConfig!=NULL
				{
					int raizconfig=atoi(config.c_str());	
					query= string("SELECT * FROM getConfigParamRecursive(")+config+string(",'') WHERE ") +
						string("(name ILIKE '%/devId' AND value = '")+StrUtils::decToString(devId)+string("') OR ")+
						string("(name ILIKE '%/isRecording' AND value = 'true') "
								" ORDER BY name");
					XML *servConfig=dbGW->query(query);

					//		cout <<" config for "<<ip<<":"<<port<<" "<<raizconfig<<endl;//<<servConfig->toString()<<endl;
					int nServRows=atoi(servConfig->getNode("/result")->getParam("numRows").c_str());
					//				int dev=-1;
					//				bool recording=false, recfound=false, devfound=false;
					map<string, DBSesInfo> dbses;		
					for(int j=0; j<nServRows  ;j++)
					{
						string fullParm=servConfig->getNode("/result/["+StrUtils::decToString(j)+"]/name")->getCdata();
						//								cout<<" parm:"<<j<<" fullParam:"<<fullParm<<endl;
						if(fullParm.find(string("/sessions/"))!=0)
						{
							//					cout<<"error a la info de gravacions de la BD : "<<fullParm<<endl;
							continue;
						}
						fullParm=fullParm.substr(10);//string("/sessions/").size()
						int pos=fullParm.find("/");
						if(pos==string::npos)
							continue;
						pos=fullParm.find("/", pos+1);	// /sessions/IP/port/param
						if(pos==string::npos)
							continue;

						string sesId=fullParm.substr(0, pos);

						//					pos=fullParm.rfind("/");
						//					if(pos==string::npos)
						//						continue;
						string parName=fullParm.substr(pos+1);
						//					cout<<" ses:"<<sesId<<" p:"<<parName<<endl;
						if(dbses.find(sesId)==dbses.end())
							dbses.insert(make_pair(sesId, DBSesInfo()));
						if(parName == string("devId"))
						{
							dbses[sesId].devId=atoi(servConfig->getNode("/result/["+StrUtils::decToString(j)+"]/value")->getCdata().c_str());
							//				cout<<"found session for device:"<<dev<<endl;	
							//devfound=true;
						} else if(parName == string("isRecording"))
						{
							dbses[sesId].recording = servConfig->getNode("/result/["+StrUtils::decToString(j)+"]/value")->getCdata() == string("true");
							//				cout<<"found IsRecording "<<servConfig->getNode("/result/["+StrUtils::decToString(j)+"]/value")->getCdata()<<" "<<recording<<", dev:"<<dev<<endl;	
							//recfound=true;
						}
					}

					if (servConfig!=NULL)
					{
						delete servConfig;
						servConfig=NULL;
					}

					for(map<string, DBSesInfo>::iterator it = dbses.begin(); it!=dbses.end();it++)
					{
						//					cout<<" session "<<it->first<<" for dev:"<<it->second.devId<<" rec:"<<it->second.recording<<endl;

						//cout<<" foud session for device:"<<dev<<" recording:"<<recording<<" at server:"<<ip<<":"<<port<<endl;
						if(it->second.devId == devId && it->second.recording)
						{ //no mirém el recType --> redirigím sempre la crida caqp al modul que s'encarrega de la camera :P
							int pos=ip.find("/");
							if(pos!=string::npos)
							{
								ip=ip.substr(0, pos);
							}
							Address serverAddr(IP(ip), atoi(port.c_str()));
							cout<<"throw RedirectCallException  "<<serverAddr.toString()<<endl;
							throw RedirectCallException(string("Redirect: ")+serverAddr.toString(), serverAddr);
						}
					}
				}
			}
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}
//			try{
//				dbgwMutex.unlock();
//			}catch(Exception &e){}
		}catch(RedirectCallException &e)
		{
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}
//			try{
//			dbgwMutex.unlock();
//			}catch(Exception &e){}
			//e.serialize()->materializeAndThrow(true);
			throw;
		}catch(Exception &e)
		{
			if (res!=NULL)
			{
				delete res;
				res=NULL;
			}
//			try{
//			dbgwMutex.unlock();
//			}catch(Exception &e){}
		}
	}*/

	return NULL;
}

RecordingModuleInterface::session* RecordingModuleInterface::getSessionForAddress(Address *a)
{
	STACKTRACE_INSTRUMENT();
//	map<string,RecordingModuleInterface::session>::iterator itSes=sessions.find(a->toString());
//	
//	if (itSes==sessions.end())
//	{

	try
	{
		return SessionManager<session>::retain(a->toString());
	}
	catch (IdNotFoundException &idnfe)
	{
/*		// Mirem si no es local, i si no ho es enviem un paquet de redireccio.
		string query=string("SELECT nc3.id FROM nodoconfig nc3 WHERE nombre='")+StrUtils::decToString(a->getPort())+string("' AND padre IN (SELECT nc2.id FROM nodoconfig nc2 WHERE nombre <> '")+a->getIP().toString()+string("' AND  padre IN (SELECT nc1.id FROM nodoconfig nc1 WHERE nombre='sessions' AND padre IN (SELECT s.raizconfig FROM subsistema s, tiposubsistema ts WHERE ts.nombre='RecordingModule' AND ts.id=s.tipo AND s.raizconfig IS NOT NULL)))");
	
			cout<<"RMI::getSesForAddr: query1"<<endl;
		XML *res=dbGW->query(query);
		
		int nRows=atoi(res->getNode("/result")->getParam("numRows").c_str());
		
		if (nRows==1)
		{
			// Obtenim el servidor que es
			string serverQuery=string("SELECT host(ip) AS ip, port, type FROM getSubsystemForConfigNode('")+res->getNode("/result/[0]/id")->getCdata()+string("')");
			cout<<"RMI::getSesForAddr: query2"<<endl;
	
			XML *server=dbGW->query(serverQuery);

			string ip=server->getNode("/result/[0]/ip")->getCdata();
			string port=server->getNode("/result/[0]/port")->getCdata();
			
			cout<<"RMI::getSesForAddr: addr:"<<endl;
			Address serverAddr(IP(ip), atoi(port.c_str()));

			// Enviem un paquet de redireccio
			//RPC::redirect(a, &serverAddr);
			
			// Tirem una excepcio de redireccio	
			cout<<"RMI::getSesForAddr: throw redirectCE:"<<serverAddr.toString()<<endl;
			throw RedirectCallException(string("Redirect: ")+serverAddr.toString(), serverAddr);
		}
//		else if (nRows>1)		// Cal escollir o de fet es una condicio d'error
//		{
//			// Obtenim el servidor que es
//			
//			// Enviem un paquet de redireccio
//		
//			// Tirem una excepcio de redireccio	
//		}
		else
*/
		{
			// Si no existex, ni local ni remota, excepcio	
			throw (RecordingModuleSessionNotStablishedException(2, string("Session not stablished with any device ")+
											   string("from client ")+a->toString()));
		}

	}
	//}	
	// No hauriem d'arribar aqui
	return NULL;
}

string RecordingModuleInterface::getAutomaticFileBasePath(int devId, RecordingFileDateMetadataChunk *date)
{
	STACKTRACE_INSTRUMENT();
//	cout << "RecordingModuleInterface::getAutomaticFileBasePath ch 1" << endl;

	string ret=baseDir+string("/");

//	cout << "RecordingModuleInterface::getAutomaticFileBasePath ch 2" << endl;

	ret+=StrUtils::decToString(devId)+string("/");

//	cout << "RecordingModuleInterface::getAutomaticFileBasePath ch 2.1" << endl;

//	cout << "RecordingModuleInterface::getAutomaticFileBasePath date: " << date << endl;
//	cout << "RecordingModuleInterface::getAutomaticFileBasePath date Year: " << date->getHumanReadableYear() << endl;
	
	ret+=StrUtils::decToString(date->getHumanReadableYear())+string("/");

//	cout << "RecordingModuleInterface::getAutomaticFileBasePath ch 3" << endl;

	string month=StrUtils::decToString(date->getHumanReadableMonth());
	while (month.length()<2)
		month=string("0")+month;
	ret+=month+string("/");

//	cout << "RecordingModuleInterface::getAutomaticFileBasePath ch 4" << endl;

	string day=StrUtils::decToString(date->getHumanReadableDay());
	while (day.length()<2)
		day=string("0")+day;
	ret+=day;

//	cout << "RecordingModuleInterface::getAutomaticFileBasePath ch 5" << endl;
//	cout << "RecordingModuleInterface::getAutomaticFileBasePath " << ret << endl;
	
	return ret;
}

string RecordingModuleInterface::getAutomaticFileName(int devId, RecordingFileDateMetadataChunk *date)
{
	STACKTRACE_INSTRUMENT();
//	cout << "RecordingModuleInterface::getAutomaticFileName ch 1" << endl;

	string path=getAutomaticFileBasePath(devId, date)+string("/");

	// BUGFIX 20101111 LNOU-37 No es pot fer aixi per gestionar be els
	// canvis horaris!	
//	dword daySeconds=date->getHumanReadableHour()*3600+date->getHumanReadableMinute()*60+date->getHumanReadableSecond();
	
	RecordingFileDateMetadataChunk today;
	today.setDate(date->getHumanReadableYear(),
		date->getHumanReadableMonth(),
		date->getHumanReadableDay());

	dword daySeconds = date->getSecs() - today.getSecs();
	

//	cout << "RecordingModuleInterface::getAutomaticFileName ch 3" << endl;
//	cout << "RecordingModuleInterface::getAutomaticFileName daySeconds " << daySeconds << endl;
	
//	// A 10 fitxers per dia :) (de 2.4h)
//	dword fileIdx=daySeconds/8640;

	// A entre 96 i 100 fitxers per dia, segons DST, amb el valor SRFSeconds
	// per defecte de 900 segons :)
	dword fileIdx=daySeconds/RecordingModuleInterface::SRFSeconds;
	
	string fname=StrUtils::decToString(fileIdx);
	
	while (fname.length()<8)
		fname=string("0")+fname;

	fname+=string(".srf");

//	cout << "RecordingModuleInterface::getAutomaticFileName ch 4" << endl;

//	cout << "RecordingModuleInterface::getAutomaticFileName " << fname << endl;
	
	return path+fname;
}

void RecordingModuleInterface::createPath(string pathStr)
{
	RecordingModuleInterface::createDirLock.lock();

	list<string> path=StrUtils::split(pathStr, string("/\\"));
	
	if (pathStr.length()>0 && pathStr[0]=='/')
		chdir("/");

	for (list<string>::iterator itPath=path.begin(); itPath!=path.end(); itPath++)
	{
		string pathComponent=(*itPath);
		
		int res=chdir(pathComponent.c_str());
		if (res<0)
		{
			int mkres=mkdir(pathComponent.c_str(), 0750);
			if (mkres<0)
			{
				RecordingModuleInterface::createDirLock.unlock();
				throw RecordingModuleException(0, "Cannot create directory for recording");
			}
							
			res=chdir(pathComponent.c_str());
			if (res<0)
			{
				RecordingModuleInterface::createDirLock.unlock();
				throw RecordingModuleException(0, "Cannot chdir to directory for recording");
			}
		}
	}
	
	RecordingModuleInterface::createDirLock.unlock();
}


void RecordingModuleInterface::relocateSession(Address *sesAddr, Address rmAddr)
{
	STACKTRACE_INSTRUMENT();

	// *****************************************************************
	// IMPORTANT: Per tal que la funcio sigui de fiar, lockMutex del 
	// recordingThread corresponent a la sessio ha d'esta lockat()
	// *****************************************************************

	// Sense try ni res pq es tracta de tobar-la i si no hi es pos ja es propaga l'excepcio
	RecordingModuleInterface::session *ses;
	ses=RecordingModuleInterface::getSessionForAddress(sesAddr);
	cout << "reloc getSesForAddr:"<<sesAddr->toString()<<" rc:"<< SessionManager<session>::getReferenceCount(sesAddr->toString())<<endl;//" th:"<<(void*)ses->recordingThread<<endl;

	RecordingModuleInterfaceSaveStreamThread *th=getRecThread(ses->sessionParams.devId);

	if ((!ses->startRecStreamRecv && !ses->startedRecording))// || th==NULL)
	{
		SessionManager<session>::release(sesAddr->toString());
		throw RecordingModuleException(0, "Cannot relocate session - did not start recording");
	}

	cout << "Relocating session: " << sesAddr->toString() << "\n" << ses->toString() << endl;

	//	RecordingModuleInterfaceSaveStreamThread *th=(RecordingModuleInterfaceSaveStreamThread *)ses->recordingThread;
	//	cout << "th:"<<(void*)th << endl;
	if(th==NULL) //ens l'han parat per algún motiu, i.e., excepció ->no cal relocar
	{
		return;
	}

	//	RecordingModuleInterface::session *s=getSessionRecordingDevice(ses->sessionParams.devId);

	Address *newSesAddr=NULL;

	//		if(s!=NULL)
	//			SessionManager<session>::release(s);

	cout << "reloc: no more sessions for device...  ses:"<< sesAddr->toString() << endl;
	IP newSesIP(string("255.255.255.255"));
	word port=0;

	newSesAddr=new Address(newSesIP, port);

	th->lockSessionAddress();
	bool addressIsEmpty=false;
	while (!addressIsEmpty)
	{
		string s=sesAddr->toString();
		try
		{
			//cout << "reloc: no more sessions for device... rename ses:"<< sesAddr->toString()<<" to:"<<newSesAddr->toString() << endl;
			//cout << "reloc: no more sessions for device... rc:"<< SessionManager<session>::getReferenceCount(s)<<endl;

			SessionManager<session>::rename(s, newSesAddr->toString());
			th->setSessionAddr(*newSesAddr);

			//pel sessionToDB
			ses->toDB=true;
			ses->inDB=false;
			
			// Fem el release del nostre get
			SessionManager<session>::release(newSesAddr->toString());//(s);
			addressIsEmpty=true;
		}
		catch (IdAlreadyInUseException &ide)
		{
			port++;

			if (port>=50000)
			{
				//				th->lockMutex.unlock();
				SessionManager<session>::release(sesAddr->toString());
				delete newSesAddr;
				throw RecordingModuleException(0, "Cannot relocate session - not enough space");
			}
			delete newSesAddr;
			newSesAddr=new Address(newSesIP, port);
		}
		catch (IdNotFoundException &idnfe)
		{
			// NO hauria de passar!
			cout << "reloc: no more sessions for device... rc:"<< SessionManager<session>::getReferenceCount(s)<<endl;
						cout << "FATAL: relocateSession: Session no longer exists :"<<idnfe.getClass()<<":"<<idnfe.getMsg() << endl;
			abort();
		}
		catch (Exception &e)
		{
			// Considerem que qualsevol altra excepcio es inocua :P
		}
	}

	th->unlockSessionAddress();

	int sesRefCount=SessionManager<session>::getReferenceCount(newSesAddr->toString());
	cout << "Relocated at: " << newSesAddr->toString() << ", refCount: " << sesRefCount << "\n" << ses->toString() << endl;

	sessionToDB(newSesAddr, rmAddr);

	try
	{
		deleteSessionFromDB(sesAddr, rmAddr);
	}
	catch (Exception &e)
	{
		cout << "RMI::relocateSession: deleteSessionFromDB ses:"<<sesAddr->toString()<<" rmAddr:"<<rmAddr.toString()<<" Exception:"<< e.getClass() << ": " << e.getMsg() << endl;
	}

	delete newSesAddr;

	return;
}

void RecordingModuleInterface::sessionToDB(Address *sesAddr, Address rmAddr, session* rses)
{
	STACKTRACE_INSTRUMENT();
	
	cout << "--> sessionToDB (" << sesAddr->toString() << ")" << endl;
	
	// Sense try ni res pq es tracta de tobar-la i si no hi es pos ja es propaga l'excepcio
	RecordingModuleInterface::session *ses;
	if(rses != NULL)
		ses=rses;
	else
		ses=RecordingModuleInterface::getSessionForAddress(sesAddr);
	
	// Hem de posar a la BD:
	// session.sessionStarted
	// session.sessionParams.devId
	// session.sessionParams.FPS (whole+frac)
	// session.sessionParams.isRecoded
	// session.sessionParams.isCyclic
	// session.sessionParams.recType
	// session.sessionParams.fourcc
	
	STACKTRACE_EXTRATRACE();
	string ip=sesAddr->getIP().toString();
	string port=StrUtils::decToString(sesAddr->getPort());
	
	string basePath=string("/sessions/")+ip+string("/")+port;

	map<string, string> info;
	
	info["sessionStarted"]=(ses->sessionStarted?"true":"false");
	info["devId"]=StrUtils::decToString(ses->sessionParams.devId);
	float fps=(float)ses->sessionParams.fpswhole+(float)ses->sessionParams.fpsfrac/1000.0f;
	info["FPS"]=StrUtils::floatToString(fps);
	info["isRecoded"]=(ses->sessionParams.isRecoded?"true":"false");
	info["isCyclic"]=(ses->sessionParams.isCyclic?"true":"false");
	info["recType"]=StrUtils::floatToString(ses->sessionParams.recType);
//	info["isRecording"]=(ses->recordingThread!=NULL && ses->startedRecording?"true":"false");
	info["isRecording"]=(ses->startRecStreamRecv?"true":"false");
	info["startedRecording"]=(ses->startedRecording?"true":"false");
//	info["recThreadIsNull"]=(ses->recordingThread==NULL?"true":"false");
		
	char fourcc[4];
	memmove(fourcc, &ses->sessionParams.fourcc, sizeof(dword));
	Endian::to(Endian::BIG, fourcc, sizeof(dword));

	string fourCCtmp("");
	
	STACKTRACE_EXTRATRACE();
	for (int fcci=0; fcci<4; fcci++)
	{
		if (fourcc[fcci]==0)
			fourCCtmp += string("\\\\000");
		else
			fourCCtmp += string(&fourcc[fcci], 1);
	}
	
	info["FourCC"]=fourCCtmp;
	


	STACKTRACE_EXTRATRACE();
	RPC *rpc=NULL;
	try
	{
//		dbgwMutex.lock();
		rpc=dbGW->getRPC();
		
		if (rpc==NULL)
		{
//			cout << "sessionToDB: Error accessing database" << endl;
			throw RecordingModuleException("Error accessing database");
		}
		
		dbGW->disableTransactionMode(rpc);
		dbGW->begin(rpc);
		
		map<string, string>::iterator infoIt;
	
		for (infoIt=info.begin(); infoIt!=info.end(); infoIt++)
		{
			string path=basePath+string("/")+infoIt->first;
			string val=infoIt->second;

//			cout << "sessionToDB: " << path << ": " << val << endl;
			
			dbGW->setSubsystemConfigParam("RecordingModule", rmAddr, path, val, rpc);
		}
		
		dbGW->commit(rpc);
		
		dbGW->enableTransactionMode(rpc);
		dbGW->freeRPC(rpc);
		ses->inDB=true;
//		try{
//		dbgwMutex.unlock();
//		}catch(Exception &e){}
	}
	catch(Exception &e)
	{
		STACKTRACE_EXTRATRACE();
		//ses->inDB=false;// no ho posem, es controla des de fora per si els relocates...
		cout << "sessionToDB: " << e.getClass() << ": " << e.getMsg() << endl;
		if (rpc!=NULL) //  && (e.getMsg().find("commands ignored until end of transaction block")!=string::npos || e.getMsg().find("Already inside a transaction")!=string::npos))
		{
			try
			{
		cout << "sessionToDB: -> rollback" << endl;
				dbGW->rollback(rpc);
		cout << "sessionToDB: -> rollback ok" << endl;
			}catch(Exception ex)
			{
				cout << "sessionToDB rollback exception : " << ex.getClass() << ": " << ex.getMsg() << endl;
			}

			dbGW->enableTransactionMode(rpc);
			dbGW->freeRPC(rpc);
		}

//		try{
//		dbgwMutex.unlock();
//		}catch(Exception &e){}
//			
		cout << "<-- sessionToDB (" << sesAddr->toString() << ") [exception]" << endl;
//		if(e.getMsg().find("commands ignored until end of transaction block")==string::npos)

		// 20090708: Comentem temporalment :P
	//	if(rses == NULL)
	//		SessionManager<session>::release(sesAddr->toString());
	//		e.serialize()->materializeAndThrow(true);
	}
	STACKTRACE_EXTRATRACE();

	if(rses == NULL)
		SessionManager<session>::release(sesAddr->toString());
	
	cout << "<-- sessionToDB (" << sesAddr->toString() << ")" << endl;
}

void RecordingModuleInterface::deleteSessionFromDB(Address *sesAddr, Address rmAddr)
{
	STACKTRACE_INSTRUMENT();
	
	cout << "--> deleteSessionFromDB (" << sesAddr->toString() << ")" << endl;

	// Sense try ni res pq es tracta de tobar-la i si no hi es pos ja es propaga l'excepcio
	// **** ES IGUAL **** si existeix o no, es tracta de netejar la BD.
//	RecordingModuleInterface::session *ses;
//	ses=RecordingModuleInterface::getSessionForAddress(sesAddr);
	


	// Hem de posar a la BD:
	// session.sessionStarted
	// session.sessionParams.devId
	// session.sessionParams.FPS (whole+frac)
	// session.sessionParams.isRecoded
	// session.sessionParams.isCyclic
	// session.sessionParams.recType
	// session.sessionParams.fourcc


	
	string ip=sesAddr->getIP().toString();
	string port=StrUtils::decToString(sesAddr->getPort());
	
	string basePath=string("/sessions/")+ip+string("/")+port;

	RPC *rpc=NULL;
	try
	{
//		dbgwMutex.lock();
		rpc=dbGW->getRPC();
		
		if (rpc==NULL)
		{
//			cout << "sessionToDB: Error accessing database" << endl;
			throw RecordingModuleException("Error accessing database");
		}
		
		dbGW->disableTransactionMode(rpc);
		dbGW->begin(rpc);
		
		dbGW->deleteSubsystemConfigParam("RecordingModule", rmAddr, basePath, rpc);
		
		dbGW->commit(rpc);
		
		dbGW->enableTransactionMode(rpc);
		dbGW->freeRPC(rpc);
//		try{
//		dbgwMutex.unlock();
//		}catch(Exception &e){}
	}
	catch(Exception &e)
	{
		if (rpc!=NULL)
		{
			try
			{
				dbGW->rollback(rpc);
			}catch(Exception ex)
			{
				cout << "deleteSessionFromDB rollback exception : " << ex.getClass() << ": " << ex.getMsg() << endl;
			}
		}
		dbGW->enableTransactionMode(rpc);
		if(rpc!=NULL)
			dbGW->freeRPC(rpc);
		
		cout << "deleteSessionFromDB: " << e.getClass() << ": " << e.getMsg() << endl;
			
		cout << "<-- deleteSessionFromDB [exception] (" << sesAddr->toString() << ")" << endl;

//		try{
//		dbgwMutex.unlock();
//		}catch(Exception &e){}
		
		// 20090710: Comentem temporalment, també
//		e.serialize()->materializeAndThrow(true);
	}
	
	cout << "<-- deleteSessionFromDB (" << sesAddr->toString() << ")" << endl;
}

/*
void RecordingModuleInterface::listRecordingsToDB(Address rmAddr)
{
	STACKTRACE_INSTRUMENT();
	
	cout << "--> ListRecParams::ToDB (" << rmAddr.toString() << ")" << endl;
	
	// Hem de posar a la BD:
	// recordings.nrecs
	// recordings[i].startDate
	// recordings[i].endDate
	// recordings[i].isRecording
	// recordings[i].id
	// recordings[i].devId
	// recordings[i].type
	// recordings[i].fps
	
	string basePath=string("/recordings");
	map<string, string> info;
	
	for (dword r=0; r<this->nRecordings; r++)
	{
		string id=StrUtils::decToString(this->recordings[r].id);
		
		info[id+string("/devId")]=StrUtils::decToString(this->recordings[r].devId);
		info[id+string("/type")]=StrUtils::decToString(this->recordings[r].recType);
		float fps=(float)this->recordings[r].fps.whole+(float)this->recordings[r].fps.frac/1000.0f;
		info[id+string("/FPS")]=StrUtils::floatToString(fps);
		info[id+string("/startDate/seconds")]=StrUtils::decToString(this->recordings[r].startDate.secs);
		info[id+string("/startDate/milliseconds")]=StrUtils::decToString(this->recordings[r].startDate.millis);
		info[id+string("/endDate/seconds")]=StrUtils::decToString(this->recordings[r].endDate.secs);
		info[id+string("/endDate/milliseconds")]=StrUtils::decToString(this->recordings[r].endDate.millis);

		info[id+string("/isRecording")]=(this->recordings[r].endDate.isValid?"false":"true");
	}
				
	info["nRecordings"]=StrUtils::decToString(this->nRecordings);

	RPC *rpc=NULL;
	try
	{
//		dbgwMutex.lock();
		rpc=dbGW->getRPC();
		
		if (rpc==NULL)
		{
//			cout << "sessionToDB: Error accessing database" << endl;
			throw RecordingModuleException("Error accessing database");
		}
		
		dbGW->disableTransactionMode(rpc);
		dbGW->begin(rpc);

		map<string, string>::iterator infoIt;
	
		for (infoIt=info.begin(); infoIt!=info.end(); infoIt++)
		{
			string path=basePath+string("/")+infoIt->first;
			string val=infoIt->second;

//			cout << "sessionToDB: setSubSysCP " << path << ": " << val << endl;
			
			dbGW->setSubsystemConfigParam("RecordingModule", rmAddr, path, val, rpc);
		}
		
		string deleteQ=string("DELETE FROM nodoconfig WHERE padre=(SELECT id FROM nodoconfig WHERE nombre='recordings' AND padre=(SELECT s.raizconfig FROM subsistema s, infobalanceo ib WHERE host(ip)='")+rmAddr.getIP().toString()+string("' and puerto='")+StrUtils::decToString(rmAddr.getPort())+string("' AND ib.subsistema = s.id)) AND nombre::integer>='")+info["nRecordings"]+string("'");
		cout<<"RMListRecordingsParams :: deleteQ:"<<deleteQ<<endl;
		XML *xml=dbGW->update(deleteQ, rpc);
		delete xml;
		
		dbGW->commit(rpc);
		dbGW->enableTransactionMode(rpc);
		
		dbGW->freeRPC(rpc);
//		dbgwMutex.unlock();
	}
	catch(Exception &e)
	{
		if (rpc!=NULL)
		{
			dbGW->rollback(rpc);
			dbGW->freeRPC(rpc);
		}
		
		dbGW->enableTransactionMode(rpc);
		
		cout << "ListRecParams::ToDB: " << e.getClass() << ": " << e.getMsg() << endl;
			
		cout << "<-- ListRecParams::ToDB (" << rmAddr.toString() << ") [exception]" << endl;
		
//		try{
//		dbgwMutex.unlock();
//		}catch(Exception &e){}
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	
	cout << "<-- ListRecParams::ToDB (" << rmAddr.toString() << ")" << endl;
	
}

void RecordingModuleInterface::listRecordingsDiffToDB(Address rmAddr, listRecordingsParams *old)
{
	STACKTRACE_INSTRUMENT();

	if (old==NULL || old->nRecordings==0)
	{
		this->toDB(rmAddr);
		return;
	}
	
	cout << "--> ListRecParams::diffToDB (" << rmAddr.toString() << ")" << endl;
	
	// Hem de posar a la BD:
	// recordings.nrecs
	// recordings[i].startDate
	// recordings[i].endDate
	// recordings[i].isRecording
	// recordings[i].id
	// recordings[i].devId
	// recordings[i].type
	// recordings[i].fps
	
	string basePath=string("/recordings");
	map<string, string> info;
	
	for (dword r=0; r<this->nRecordings; r++)
	{
		bool insert=true;
		for (dword ro=0; ro<old->nRecordings; ro++)
		{
			if (this->recordings[r].devId==old->recordings[ro].devId  &&
				this->recordings[r].recType==old->recordings[ro].recType &&
				this->recordings[r].startDate.secs==old->recordings[ro].startDate.secs &&
				this->recordings[r].startDate.millis==old->recordings[ro].startDate.millis &&
				this->recordings[r].endDate.secs==old->recordings[ro].endDate.secs &&
				this->recordings[r].endDate.millis==old->recordings[ro].endDate.millis)
			{
				insert=false;
				break;
			}
		}

		if (insert)
		{
			cout << "Insert: " << this->recordings[r].id << endl;
		
			string id=StrUtils::decToString(this->recordings[r].id);
			
			info[id+string("/devId")]=StrUtils::decToString(this->recordings[r].devId);
			info[id+string("/type")]=StrUtils::decToString(this->recordings[r].recType);
			float fps=(float)this->recordings[r].fps.whole+(float)this->recordings[r].fps.frac/1000.0f;
			info[id+string("/FPS")]=StrUtils::floatToString(fps);
			info[id+string("/startDate/seconds")]=StrUtils::decToString(this->recordings[r].startDate.secs);
			info[id+string("/startDate/milliseconds")]=StrUtils::decToString(this->recordings[r].startDate.millis);
			info[id+string("/endDate/seconds")]=StrUtils::decToString(this->recordings[r].endDate.secs);
			info[id+string("/endDate/milliseconds")]=StrUtils::decToString(this->recordings[r].endDate.millis);
	
			info[id+string("/isRecording")]=(this->recordings[r].endDate.isValid?"false":"true");
		}
	}
				
	info["nRecordings"]=StrUtils::decToString(this->nRecordings);

	RPC *rpc=NULL;
	try
	{
//		dbgwMutex.lock();
		rpc=dbGW->getRPC();
		
		if (rpc==NULL)
		{
//			cout << "sessionToDB: Error accessing database" << endl;
			throw RecordingModuleException("Error accessing database");
		}
		
		dbGW->disableTransactionMode(rpc);
		dbGW->begin(rpc);

		map<string, string>::iterator infoIt;
	
		for (infoIt=info.begin(); infoIt!=info.end(); infoIt++)
		{
			string path=basePath+string("/")+infoIt->first;
			string val=infoIt->second;

//			cout << "diffToDB: setSSysCP  " << path << ": " << val << endl;
			
			dbGW->setSubsystemConfigParam("RecordingModule", rmAddr, path, val, rpc);
		}
		
		string deleteQ=string("DELETE FROM nodoconfig WHERE padre=(SELECT id FROM nodoconfig WHERE nombre='recordings' AND padre=(SELECT s.raizconfig FROM subsistema s, infobalanceo ib WHERE host(ip)='")+rmAddr.getIP().toString()+string("' and puerto='")+StrUtils::decToString(rmAddr.getPort())+string("' AND ib.subsistema = s.id)) AND nombre::integer>='")+info["nRecordings"]+string("'");
		cout<<"RMListRecordingsParams :: deleteQ:"<<deleteQ<<endl;
		XML *xml=dbGW->update(deleteQ, rpc);
		delete xml;
		
		dbGW->commit(rpc);
		dbGW->enableTransactionMode(rpc);
		
		dbGW->freeRPC(rpc);
//		try{
//		dbgwMutex.unlock();
//		}catch(Exception &e){}
	}
	catch(Exception &e)
	{
		if (rpc!=NULL)
		{
			dbGW->rollback(rpc);
			dbGW->freeRPC(rpc);
		}

		dbGW->enableTransactionMode(rpc);
		
		cout << "ListRecParams::diffToDB: " << e.getClass() << ": " << e.getMsg() << endl;
			
		cout << "<-- ListRecParams::diffToDB (" << rmAddr.toString() << ") [exception]" << endl;
		
//		try{
//		dbgwMutex.unlock();
//		}catch(Exception &e){}
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	
	cout << "<-- ListRecParams::diffToDB (" << rmAddr.toString() << ")" << endl;
}
*/
void RecordingModuleInterface::setMaxActiveRecordingsCount(int newCount)
{
	if (newCount>0 &&
		newCount<RecordingModuleInterface::maxActiveRecordingsHardLimit)
	{
		RecordingModuleInterface::maxActiveRecordingsCount=newCount;
	}
}

void RecordingModuleInterface::checkFullFactorShutdown()
{
	if(RecordingModuleInterface::camsInAlert.size() > RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount * RecordingModuleInterface::maxCamPercentInAlert)
	{
		if (this->cn->isSending())
		{
			cout << " RecordingModuleInterface::checkFullFactorShutdown(): >"<<(int)(RecordingModuleInterface::maxCamPercentInAlert*100)<<"% cams with FullFactor>1"
			<< "(" << RecordingModuleInterface::camsInAlert.size() << "/" << RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount 
			<< "), stopping Canis announce thread." << endl;
			this->reassignPending = false;
			this->cn->stopSend();
		}
	}
	else
	{
		if (!this->cn->isSending())
		{
			cout << " RecordingModuleInterface::checkFullFactorShutdown(): cams with FullFactor>1 dropped <"<<(int)(RecordingModuleInterface::maxCamPercentInAlert*100)<<"% "
			<< "(" << RecordingModuleInterface::camsInAlert.size() << "/" << RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount 
			<< "), restarting Canis announce thread." << endl;
			this->cn->startSend();
			this->reassignRequestTime.start();
			this->reassignPending = true;

		}
	}

}

RecordingModuleInterface::DWWatchdogCallback::DWWatchdogCallback():rmi(NULL)
{
}

void RecordingModuleInterface::DWWatchdogCallback::setRMI(RecordingModuleInterface *rmi)
{
	this->rmi = rmi;
}

void RecordingModuleInterface::DWWatchdogCallback::watchdogTimeout(int seconds)
{
	cout<<endl<<"*********"<<endl<<"*** RecordingModuleInterface::watchdogTimeout: "<<seconds<<" seconds without writing"<<endl<<"*********"<<endl;


//	DBGateway *db = new DBGateway(rmi->address, rmi->type, rmi->cn);
//	rmi->logDBEvent(db, "", string("Disc Writer Watchdog timeout: ")+rmi->address.toString(), string("Critial_errors"));
	
	AlarmModuleAccess *ama=new AlarmModuleAccess(rmi->address, rmi->type, rmi->cn);
	rmi->raiseSystemAlarm(ama, string("ModuleAlarm.RecordingModule.watchdog_timeout:")+rmi->getAddress().getIP().toString()+string("|")+StrUtils::decToString(rmi->getAddress().getPort())+string(":watchdog_timeout"), -1);
	
	cout<<"RecordingModuleInterface::watchdogTimeout: restarting"<<endl;
	exit(-1);
}

void RecordingModuleInterface::logDBEvent(DBGateway *db, string logDate, string msg, string level)
{
	if(logDate == string(""))
		logDate = "(select now())::timestamp";
	else
		logDate = string("'")+logDate+string("'");

	string query=string("SELECT * FROM loglineevent(")+logDate+string(", '")+msg+("', (select id from loglevel where nombre = '")+level+string("')); ") ;
	cout<<"loggingDB event:"<<query<<endl;
	try
	{
		db->query(query);
	}catch(Exception &e)
	{
		cout<<"RecordingModuleInterface::logDBEvent: exception while logging to DB:"<<e.getClass()<<" : "<<e.getMsg()<<endl;
		cout<<"RecordingModuleInterface::logDBEvent: from query:"<<query<<endl;
	}catch(std::exception &stde)
	{
		cout<<"RecordingModuleInterface::logDBDev: exception while logging to DB:"<<stde.what()<<endl;
		cout<<"RecordingModuleInterface::logDBDev: from query:"<<query<<endl;
	}
}

void RecordingModuleInterface::logDBDev(DBGateway *db, string logDate, string msg, int devId, string level) 
{
	if(logDate == string(""))
		logDate = "(select now())::timestamp";
	else
		logDate = string("'")+logDate+string("'");

	string query=string("SELECT * FROM loglinedisp(")+logDate+string(", '")+msg+("', '")+StrUtils::decToString(devId)+("', (select id from loglevel where nombre = '")+level+string("')); ") ;
	cout<<"loggingDB device:"<<query<<endl;
	try
	{
		db->query(query);
	}catch(Exception &e)
	{
		cout<<"RecordingModuleInterface::logDBDev: exception while logging to DB:"<<e.getClass()<<" : "<<e.getMsg()<<endl;
		cout<<"RecordingModuleInterface::logDBDev: from query:"<<query<<endl;
	}catch(std::exception &stde)
	{
		cout<<"RecordingModuleInterface::logDBDev: exception while logging to DB:"<<stde.what()<<endl;
		cout<<"RecordingModuleInterface::logDBDev: from query:"<<query<<endl;
	}
}


void RecordingModuleInterface::raiseSystemAlarm(AlarmModuleAccess *ama, string msg, int value, bool raise)
{
	try
	{
		AMAlarmId alm;
		alm.devId=-1;
		alm.type=AMAlarmId::Filter;
		alm.strId=msg;
		alm.intId=-1;

		AMAlarmValue av;
		av.value = value;
		av.alarmId = alm;
		av.raised = raise;

		cout<<"RecordingModuleInterface::raising system alarm: "<<alm.strId<<":"<<raise<<endl;
		ama->startSession(-1);
		ama->setAlarm(av);
	}catch(Exception &e)
	{
		cout<<"RecordingModuleInterface::raiseSystemAlarm: exception while raising alarm:"<<e.getClass()<<" : "<<e.getMsg()<<endl;
	}catch(std::exception &stde)
	{
		cout<<"RecordingModuleInterface::raiseSystemAlarm: exception while raising alarm:"<<stde.what()<<endl;
	}
}

void RecordingModuleInterface::limitsThreadLock()
{
	((RecordingModuleLimitsThread*)RecordingModuleInterface::limitsThread)->lockRun();
}

void RecordingModuleInterface::limitsThreadUnlock()
{
	((RecordingModuleLimitsThread*)RecordingModuleInterface::limitsThread)->unlockRun();
}

RecordingModuleInterface::RecordingModuleInterface(Address recordingModuleAddress, short recordingModuleType, string baseDir,
	qword kibLimit, Canis *cn, string configFile) : ModuleInterface(recordingModuleAddress, recordingModuleType, cn), storeKiBLimit(kibLimit), configFile(configFile)
{
	STACKTRACE_INSTRUMENT();

	RecordingModuleInterface::diskWriter->startRunning();

	RecordingModuleInterface::baseDir=baseDir;
	// Fem una primera inicialitzacio de tots els chunks, pq no hi hagi problemes de registre de
	// les diverses *::newChunk
	RecordingFileChunk::initChunks();

	RecordingModuleInterface::excludedSearchIP=recordingModuleAddress.getIP();
	
	numServices=RecordingModuleInterface::serviceCount;
	services=new serviceDef[RecordingModuleInterface::serviceCount];
	
	memset(services, 0, sizeof(serviceDef)*RecordingModuleInterface::serviceCount);
	services[RecordingModuleInterface::startSessionServiceId].call=(DefaultModuleCall)RecordingModuleInterface::startSession;
	services[RecordingModuleInterface::endSessionServiceId].call=(DefaultModuleCall)RecordingModuleInterface::endSession;
	services[RecordingModuleInterface::saveFrameServiceId].call=(DefaultModuleCall)RecordingModuleInterface::saveFrame;
	services[RecordingModuleInterface::readFrameServiceId].call=(DefaultModuleCall)RecordingModuleInterface::readFrame;
	services[RecordingModuleInterface::setTimeoutServiceId].call=(DefaultModuleCall)RecordingModuleInterface::setTimeout;

	services[RecordingModuleInterface::skipFramesServiceId].call=(DefaultModuleCall)RecordingModuleInterface::skipFrames;
	services[RecordingModuleInterface::startRecordingStreamServiceId].call=(DefaultModuleCall)RecordingModuleInterface::startRecordingStream;
	services[RecordingModuleInterface::startLocalRecordingStreamServiceId].call=(DefaultModuleCall)RecordingModuleInterface::startLocalRecordingStream;
	services[RecordingModuleInterface::stopRecordingStreamServiceId].call=(DefaultModuleCall)RecordingModuleInterface::stopRecordingStream;
	services[RecordingModuleInterface::listRecordingsServiceId].call=(DefaultModuleCall)RecordingModuleInterface::listRecordings;
	services[RecordingModuleInterface::isRecordingServiceId].call=(DefaultModuleCall)RecordingModuleInterface::isRecording;
	services[RecordingModuleInterface::getKiBLimitServiceId].call=(DefaultModuleCall)RecordingModuleInterface::getKiBLimit;
	services[RecordingModuleInterface::setKiBLimitServiceId].call=(DefaultModuleCall)RecordingModuleInterface::setKiBLimit;
	services[RecordingModuleInterface::getFrameLimitServiceId].call=(DefaultModuleCall)RecordingModuleInterface::getFrameLimit;
	services[RecordingModuleInterface::setFrameLimitServiceId].call=(DefaultModuleCall)RecordingModuleInterface::setFrameLimit;
	services[RecordingModuleInterface::getCyclicServiceId].call=(DefaultModuleCall)RecordingModuleInterface::getCyclic;
	services[RecordingModuleInterface::setCyclicServiceId].call=(DefaultModuleCall)RecordingModuleInterface::setCyclic;

	dbGW=new DBGateway(this->address, this->type, this->cn);

	cout<<"creating recIndex"<<endl;
	recIndex=new RecordingModuleInterface::recordingIndex(RecordingModuleInterface::baseDir+string("/recIndex.sri"));
	recIndex->setWriteToDisk(false);
	
	awarenessThread=new RecordingModuleAwarenessThread(this, this->cn);
	((RecordingModuleAwarenessThread*)awarenessThread)->start();

	limitsThread=new RecordingModuleLimitsThread(this, storeKiBLimit);
	((RecordingModuleLimitsThread*)limitsThread)->start();

	almLogThread=new RecordingModuleAlarmAndLogThread(this);
	almLogThread->start();

	dwWatchdogCallback->setRMI(this);
	dwWatchdog->startCheck();
	
	string alarmSource=this->getAddress().getIP().toString()+string("|")+StrUtils::decToString(this->getAddress().getPort());

	almLogThread->queueLower(RecordingModuleAlarmAndLogThread::lowerAlarm(string("ModuleAlarm.RecordingModule.watchdog_timeout:")+alarmSource+string(":"), 3600*24));

	// Baixem les alarmes perque si no estan settades no surtin com indefinides
	almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_limit_reached, "", false));

	almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_limit_warning, "", false));

	almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_time_warning, "", false));

//	sched = new RecordingModuleSchedulingThread(this);//(configFile);

//	sched->loadAllSchedules();
//	sched->start(new RecordingModuleSchedulingThread::threadCall(0, NULL), Thread::RESTART);
	try
	{
		RecordingModuleSchedulerAccess rmsa(cn);
		RMSServerId id(recordingModuleAddress);
		cout<<" rmsa.checkSchedulesForServer(id); "<<endl;
		rmsa.checkSchedulesForServer(id);
	}
	catch(Exception &e)
	{
		cout<<"Error while notifying Scheduler: "<<e.getClass()<<"::"<<e.getMsg()<<endl;
	}
}

/*
void RecordingModuleInterface::retainRecIndex()
{
	STACKTRACE_INSTRUMENT();
//		cout << "retainRecIndex" << endl;
//		recIndexRefLock.lock();
//		recIndexRef++;
//		recIndexRefLock.unlock();
}

void RecordingModuleInterface::releaseRecIndex()
{
	STACKTRACE_INSTRUMENT();
//		recIndexRefLock.lock();
//		recIndexRef--;
//	//	cout << "releaseRecIndex" << endl;
//		if (recIndexRef==0)
//		{
//	//		cout << "releaseRecIndex --- ref 0" << endl;
//			delete recIndex;
//			recIndex=NULL;
//			recIndexRefLock.unlock();
//		}
//		else
//			recIndexRefLock.unlock();
}
*/

void RecordingModuleInterface::wlockRecIndex()
{
	STACKTRACE_INSTRUMENT();
	recIndexLock.wlock();
}

void RecordingModuleInterface::rlockRecIndex()
{
	STACKTRACE_INSTRUMENT();
	recIndexLock.rlock();
}

void RecordingModuleInterface::unlockRecIndex()
{
	STACKTRACE_INSTRUMENT();
	recIndexLock.unlock();
}

RecordingModuleInterface::recordingIndex *RecordingModuleInterface::getRecIndex()
{
	if (recIndex==NULL)
	{
		cout<<"RecordingModuleInterface::getRecIndex FATAL recIndex==NULL"<<endl;
	}

	return recIndex;
}

string RecordingModuleInterface::getBaseDir()
{
	return baseDir;
}

qword RecordingModuleInterface::getSizeLimit()
{
	return storeKiBLimit;
}

//RPCPacket *RecordingModuleInterface::service(RPCPacket *inPkt, Address *a)
//{
//	STACKTRACE_INSTRUMENT();
//	cout << "RecordingModuleInterface::service()" << endl;
//	if (((short)inPkt->id)<0 || inPkt->id>=NUM_SERVICES)
//	{
//		return NULL;
//	}
//	
//	RPCPacket *result;
//	
//	try
//	{
//	cout<<" Call:"<<inPkt->id<<" : "<<a->toString()<<","<<(void*)inPkt<<":"<<(void*)inPkt->getData()<<":"<< inPkt->getSize()<<endl;
//		result=services[inPkt->id].call(this, a, inPkt->getData());
//	}
//	catch (Exception &e)
//	{
//		// cout << "RecordingModuleInterface: " << e.getClass() << ": " << e.getMsg() << endl;
//		// Muntem un RPCPacket d'Excepcio
//		SerializedException *se=e.serialize();
//		
//		// TODO: Atencio, que passa amb el tipus desti? (ara 0)
//		RPCPacket *p=new RPCPacket(this->rmAddr, (word)-2, se->bytes(), se->size(), this->rmType,
//				inPkt->origen, true);
//
//		delete se;
//		
//		return p;
//	}
//
//	return result;
//}

RPCPacket* RecordingModuleInterface::startSession(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	//RecordingModuleInterface::startSessionParams p;
	cout << "--> RecordingModuleInterface::startSession: @" << a->toString() << ", refCount: " << SessionManager<session>::getReferenceCount(a->toString()) << endl;
//	cout << "RecordingModuleInterface::startSession 1" << endl;

//	p.toLocal(params);

	RecordingModuleInterface::session *ses=NULL;

//	cout << "startSession: " << StrUtils::hexDump(string((char*)params, ses.sessionParams.size())) << endl;
	
//	void *tonet=ses.sessionParams.toNetwork();
//	cout << "startSession toNet: " << StrUtils::hexDump(string((char*)tonet, ses.sessionParams.size())) << endl;
//	delete [] tonet;
	
//	cout << "RecordingModuleInterface::startSession 2" << endl;
	
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
		
		// Si la sessio ja existeix la matxaquem perque el mes probable es que
		// sigui una sessio vella que no s'ha tancat previament. En el fons
		// es un problema de seguretat i integritat, pero com que tenim 
		// altres problemes ara no vindra d'aqui :P
		
//		SessionManager<session>::release(a->toString());
//		throw RecordingModuleSessionAlreadyStablishedException(("Session already started (RMI::startSession) for ") + string(a->toString()));
	}
	catch (RedirectCallException &rce)
	{
		cout << __FILE__ << " line " << std::dec << __LINE__ << " (startSession) " << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	catch (RecordingModuleSessionNotStablishedException &rmsnse)
	{
		// Es precisament el que ha de ser, aixi que no fem res :D
//		cout << "\tRecordingModuleInterface::startSession: @" << a->toString() << ", session not stablished (OK)" << endl;
	}
	
	if (ses==NULL)
		ses=SessionManager<session>::create(a->toString());

//	cout << "RecordingModuleInterface::startSession 3" << endl;

	ses->sessionParams.toLocal(params);
	if (ses->sessionParams.date!=NULL)
	{
		delete ses->sessionParams.date;
		ses->sessionParams.date=NULL;
	}
//	ses.deviceId=p.devId;
	if (ses->currentDate!=NULL)
	{
		ses->currentDate->setCurrentDate();
	}
	else
		ses->currentDate=new RecordingFileDateMetadataChunk();
//	ses.currentDate->setDate(p.date->getSecs(), p.date->getMillis());
//	ses.currentDate->setDate(ses.sessionParams.date->getSecs(), ses.sessionParams.date->getMillis());
	

//	cout << "RecordingModuleInterface::startSession date: " << ses.currentDate << endl;

	ses->tOutTimer.start();
//	cout << "RecordingModuleInterface::startSession 4" << endl;
	
//	ses.sessionParams=p;

	string pathOnly=_this->getAutomaticFileBasePath(ses->sessionParams.devId, ses->currentDate);
	RecordingModuleInterface::createPath(pathOnly);

//	cout << "RecordingModuleInterface::startSession 4.5" << endl;

	recIndex=RecordingModuleInterface::getRecIndex();
	
	ses->sessionStarted=true;

	cout << "Started session: " << a->toString() << ", devId: " << ses->sessionParams.devId << endl;
	
	ses->toDB=false;
	ses->inDB=false;
//	no l'afegim si no fa falta
//	RecordingModuleInterface::sessionToDB(a, _this->address);
	
	cout << "<-- RecordingModuleInterface::startSession: @" << a->toString() << ", refCount: " << SessionManager<session>::getReferenceCount(a->toString()) << endl;

	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}

RPCPacket* RecordingModuleInterface::endSession(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	RMEndSessionParams p(params);
	
	cout << "--> RecordingModuleInterface::endSession" << endl;

	// Sense try ni res pq es tracta de tobar-la i si no hi es pos ja es propaga l'excepcio
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << __FILE__ << " line " << std::dec << __LINE__ << " (endSession) " << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}

	int sesRefCount=SessionManager<session>::getReferenceCount(a->toString());
	cout << "RecordingModuleInterface::endSession: @" << a->toString() << ", id:"<<ses->sessionParams.devId<<" started: " << ses->sessionStarted << ", refCount: " << sesRefCount << endl;

	if (!ses->sessionStarted)
	{
		cout<<"RecordingModuleInterface::endSession !sesStarted"<<endl;
/*		while (sesRefCount > 0)//¿?
		{
			SessionManager<session>::release(a->toString());
			sesRefCount=SessionManager<session>::getReferenceCount(a->toString());
		}
*/		
		RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
		return pk;
	}

	// Per quan es faci el deleteSessionFromDB
	ses->freeSesAddr=*a;
	ses->freeSesModuleAddr=_this->address;

//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->lock()") << endl;
//#endif
	ses->endSessionMutex->lock(true,"endSession");
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->lock() LOCKED") << endl;
//#endif
	cout<<"endSes::endSesMtx->locked"<<endl;

	if (ses->sessionParams.devId!=p.devId && ses->sessionParams.devId>0)
	{
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
//
//#endif
		ses->endSessionMutex->unlock();
		cout<< __FILE__ << " line " << std::dec << __LINE__ << " endSes::endSesMtx->unlocked"<<endl;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
//#endif
		SessionManager<session>::release(a->toString());
		throw RecordingModuleException(0, string("Invalid Parameters (") +
			StrUtils::decToString(ses->sessionParams.devId) + string(" vs ") +
			StrUtils::decToString(p.devId) + string(") from ") +
			a->toString());
	}

	bool unlocked=false;
	try
	{
		cout << "Ending session: " << a->toString()<<", id:"<<ses->sessionParams.devId<<" startStr:"<<ses->startRecStreamRecv << endl;

		// Nomes si no es la que ha iniciat la gravacio fem el free...
	//	if (!ses->startRecStreamRecv)//(!ses->startedRecording)
//		if (!ses->startedRecording)

	
		//cThrLock.rlock();
		if(recThreadStarted(ses->sessionParams.devId) && (ses->startedRecording || ses->startRecStreamRecv))
		{//propietaria thread/gravant
			ses->sessionStarted=false;
			RecordingModuleInterfaceSaveStreamThread *th=getRecThread(ses->sessionParams.devId);
			//cThrLock.unlock();
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
//
//#endif
			ses->endSessionMutex->unlock();
			unlocked=true;
			cout<< __FILE__ << " line " << std::dec << __LINE__ << " endSes::endSesMtx->unlocked"<<endl;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
//#endif

			cout << "RMI::endSession[" << a->toString() << "]: lockMutex.lock() thread:"<<(void*)th << endl;
			th->lockMutex->lock();
			cout << "RMI::endSession[" << a->toString() << "]: lockMutex.lock() LOCKED thr:"<<(void*)th<<" lock:"<<(void*)th->lockMutex<< endl;

			try
			{
				ses->toDB=true;
				relocateSession(a, _this->address);
			}catch(Exception e){}
			cout << "RMI::endSession[" << a->toString() << "]: lockMutex.unlock()" << endl;
			if (th->lockMutex->isHeldByCurrentThread())
				th->lockMutex->unlock();
			else
				cout << __FILE__ << " Line " << std::dec << __LINE__ << " lockMutex NO lockat" << endl;
			cout << "RMI::endSession[" << a->toString() << "]: lockMutex.lock() UNLOCKED" << endl;

			// Ja que el de dalt es un relocate, fem el release despres de
			// relocar, per si les mosques --- Ens assegurem que tenim una
			// referencia valida, suposadament ("salvo bugs")
			
			// El release del get s'ha de fer sobre la nova sessio perque
			// l'acabem de moure de lloc :D
			string addrStr;
			try
			{
				cout<<"SesMng.release reloc"<<endl;
//				addrStr = 
//					SessionManager<RecordingModuleInterface::session>::getKey(ses);
//			
//				list<string> addrComps=StrUtils::split(addrStr, string(":"));
//				if (addrComps.size()==2)
//				{
//					string ip=addrComps.front();
//					addrComps.pop_front();
//					int port=atoi(addrComps.front().c_str());
//					addrComps.pop_front();
//					
//					Address addr(IP(ip), port);
//					SessionManager<session>::release(addr.toString());
//				}
				SessionManager<session>::release(ses);
			}
			catch(Exception &e)
			{
				int sesRefCount=SessionManager<session>::getReferenceCount(addrStr);
				cout << "RecordingModuleInterface::endSession: SesMng.release reloc: " << addrStr << ", id:"<<ses->sessionParams.devId<<" started: " << ses->sessionStarted << ", refCount: " << sesRefCount << " - " << e.getClass() << ": " << e.getMsg() << endl;
			}
			
			// NO fem release del startSession, perque el thread de
			// gravacio no te cap retain fet.
			int sesRefCount=SessionManager<session>::getReferenceCount(addrStr);
			cout << "RecordingModuleInterface::endSession: @" << addrStr << ", id:"<<ses->sessionParams.devId<<" started: " << ses->sessionStarted << ", refCount: " << sesRefCount << endl;
		}
		else
		{
			//cThrLock.unlock();
			ses->sessionStarted=false;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
//
//#endif
			ses->endSessionMutex->unlock();
			unlocked=true;
			cout<< __FILE__ << " line " << std::dec << __LINE__ << " endSes::endSesMtx->unlocked"<<endl;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
//#endif

//			try
//			{
//				deleteSessionFromDB(a, _this->address); //ja ho farà  relocate/freesession
//				ses->inDB=false;
//			}catch(Exception e){}  //sinó es tiren les excepcions de BD cap al client...

		//	ses->sMutex->unlock();

			// El release del get
			SessionManager<session>::release(a->toString());

			//freeSession(a, _this->address);
			
			// El release de l'startsession
			SessionManager<session>::release(a->toString());

		}
	}
	catch (Exception &e)
	{
		// No pot estar locked pq fem l'unlock abans q es pugui llençar cap
		// excepcio
		if(!unlocked && ses->endSessionMutex->isHeldByCurrentThread())
		{
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
//
//#endif
			ses->endSessionMutex->unlock();
			cout<< __FILE__ << " line " << std::dec << __LINE__ << " endSes::endSesMtx->unlocked"<<endl;
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
//#endif
		}
		//e.serialize()->materializeAndThrow(true);
		throw;
	}

	cout << "<-- RecordingModuleInterface::endSession: @" << a->toString() << ", refCount: " << SessionManager<session>::getReferenceCount(a->toString()) << endl;
	
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}

RPCPacket* RecordingModuleInterface::saveFrame(RecordingModuleInterface *_this, Address *a, void *params)
{
//	cout << "--> RecordingModuleInterface::saveFrame" << endl;
	STACKTRACE_INSTRUMENT();
	INIT_CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif

	try
	{
		RMSaveFrameParams p(params);
//		p.toLocal(params);

		RecordingModuleInterface::session *ses;
		string startAddr("");
		try
		{
			startAddr=a->toString();
			ses=RecordingModuleInterface::getSessionForAddress(a);
		}
		catch (RedirectCallException &rce)
		{
			cout << rce.getClass() << ": " << rce.getMsg() << endl;
			return NULL;
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << "ses->sessionParams.devId (ojo que aqui ses sera null :P)" <<"]: saveFrame (getSesForAddr) " << 
				e.getClass() << ": " << e.getMsg() <<" Address:"<<a->toString()<<" starting addr:"<<startAddr<< endl;
			throw e;
		}

		CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

		ses->endSessionMutex->lock(true, "saveFrame ses");

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

		qword prevUnixTime=ses->currentDate->getSecs();
		byte humanPrevSec=ses->currentDate->getHumanReadableSecond();
		byte humanPrevDay=ses->currentDate->getHumanReadableDay();
		ses->currentDate->setCurrentDate();
		qword currUnixTime=ses->currentDate->getSecs();
		byte humanCurrSec=ses->currentDate->getHumanReadableSecond();
		byte humanCurrDay=ses->currentDate->getHumanReadableDay();

		RecordingModuleInterface::session *updSes = NULL;
		bool sesFound=false;
		try
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			CHECKPOINT();
			for(int i=0; i<RecordingFileHeaderChunk::REC_HOWMANY; i++)
			{
				updSes=RecordingModuleInterface::getSessionRecordingDevice(ses->sessionParams.devId, (RecordingFileHeaderChunk::recType)i, true);

				if(updSes==NULL)
					continue;

				// Comprovar si cal tancar un bloc de marcatge de gravacio
				// (si porta més de 30s sense gravar)
				// Mirem també que tinguem un index...
				// Asegurem mirant que current sigui > previous
				if (currUnixTime>prevUnixTime &&
					currUnixTime-prevUnixTime>30 && 
					updSes->recIndexIdx==0xffffffffffffffffll)
				{
					// Pot haver-se invalidat. Busquem un segment que tingui
					// endDate.isValid a false i que sigui de la cam i data q toca,
					// mes o menys.
					bool found=false;

					qword s=0;
					cout << "30+ seconds without recording... (devId:"<<updSes->sessionParams.devId<<"), invalidated" << endl;

					struct timeval tv;
					tv.tv_sec = 0;
					int lostSecs = currUnixTime-prevUnixTime;

					for (s=0; s<recIndex->nSegments; s++)
					{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						const RecordingModuleInterface::recordingSegment *rs=recIndex->getSegment(s);
						RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;

						if (!r.endDate.isValid && r.devId==updSes->sessionParams.devId
								&& r.recType==updSes->sessionParams.recType
								&& r.fps.whole==updSes->sessionParams.fpswhole
								&& r.fps.frac==updSes->sessionParams.fpsfrac
								&& (r.startDate.secs<updSes->currentDate->getSecs() ||
									(r.startDate.secs==updSes->currentDate->getSecs() &&
									 r.startDate.millis<=updSes->currentDate->getMillis())))
						{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							RecordingModuleInterface::wlockRecIndex();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							try
							{
								// Nomes tanquem, sense variar la data de fi.
								r.endDate.isValid=true;
	
								recIndex->alterSegment(s, r);
								updSes->recIndexIdx=0xffffffffffffffffll;
	
								cout<<"recIndex closed after 30+ seconds, "<<__LINE__<<", found dev:"<<r.devId<<" start:"<<StrUtils::toDateString(r.startDate.secs)<<"."<<setfill('0')<<setw(3)<<r.startDate.millis<<" end:"<<StrUtils::toDateString(r.endDate.secs)<<"."<<setfill('0')<<setw(3)<<r.endDate.millis<<endl;

								tv.tv_sec = r.endDate.secs;//fem servir la data com a data del log. Si no, fotem la data actual 
								tv.tv_usec = r.endDate.millis * 1000;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								RecordingModuleInterface::unlockRecIndex();
							}
							catch (...)
							{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								RecordingModuleInterface::unlockRecIndex();
							}
							
							// Si hem tancat ja hem tallat i per tant
							// ni tall de mitjanit ni hosties...
							ses->currentDate->setCurrentDate();
							prevUnixTime=ses->currentDate->getSecs();
							humanPrevSec=ses->currentDate->getHumanReadableSecond();
							humanPrevDay=ses->currentDate->getHumanReadableDay();
							currUnixTime=ses->currentDate->getSecs();
							humanCurrSec=ses->currentDate->getHumanReadableSecond();
							humanCurrDay=ses->currentDate->getHumanReadableDay();							
							break;
						}
					}

					//enviem al log i alarma
					almLogThread->queue(RecordingModuleAlarmAndLogThread::event(updSes->sessionParams.devId, RecordingModuleAlarmAndLogThread::rec_timeout, tv, StrUtils::decToString(lostSecs), true));
				}
				else if (currUnixTime>prevUnixTime &&
					currUnixTime-prevUnixTime>30 && 
					updSes->recIndexIdx!=0xffffffffffffffffll)
				{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					cout << "30+ seconds without recording... (devId:"<<updSes->sessionParams.devId<<"), closing..." << endl;
					// Deixem de gravar => tanquem marcatge
					
					struct timeval tv;
					tv.tv_sec = 0;
					int lostSecs = currUnixTime-prevUnixTime;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					RecordingModuleInterface::wlockRecIndex();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					try
					{
						CHECKPOINT();
						const RecordingModuleInterface::recordingSegment *rs=recIndex->getSegment(updSes->recIndexIdx);
						RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;
						
						r.endDate.isValid=true;
						recIndex->alterSegment(updSes->recIndexIdx, r);
						
						updSes->recIndexIdx=0xffffffffffffffffll;
						
						tv.tv_sec = r.endDate.secs;//fem servir la data com a data del log. Si no, fotem la data actual 
						tv.tv_usec = r.endDate.millis * 1000;
						
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						RecordingModuleInterface::unlockRecIndex();
					}
					catch (...)
					{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						RecordingModuleInterface::unlockRecIndex();
					}
					// FI marcatge
					
					//enviem al log i alarma
					almLogThread->queue(RecordingModuleAlarmAndLogThread::event(updSes->sessionParams.devId, RecordingModuleAlarmAndLogThread::rec_timeout, tv, StrUtils::decToString(lostSecs), true));

							
					// Si hem tancat ja hem tallat i per tant
					// ni tall de mitjanit ni hosties...
					ses->currentDate->setCurrentDate();
					prevUnixTime=ses->currentDate->getSecs();
					humanPrevSec=ses->currentDate->getHumanReadableSecond();
					humanPrevDay=ses->currentDate->getHumanReadableDay();
					currUnixTime=ses->currentDate->getSecs();
					humanCurrSec=ses->currentDate->getHumanReadableSecond();
					humanCurrDay=ses->currentDate->getHumanReadableDay();

					// Un cop aquí hem tancat i llestos :)
				}

				sesFound=true;
				
				// Si canviem de segon traiem l'ID de tipus de gravacio,
				// per veure que esta gravant :D
				// Pero no ho traiem la hostia de cops per segon com abans :P
				if (humanPrevSec != humanCurrSec)
				{
					cout << std::dec << i;
					cout.flush();
				}
				
			CHECKPOINT();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

				updSes->endSessionMutex->lock(true, "saveFrame updSes" );

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

			CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				RecordingModuleInterface::wlockRecIndex();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			CHECKPOINT();
				updSes->sMutex->lock();

				try
				{
					// Muntem les hores de tall, pq siguin 23:59:59.999 i
					// 00:00:00
					// Nota: now es just despres de mitjanit
					qword nowHRHour=
						(qword)ses->currentDate->getHumanReadableHour();
					qword nowHRMin=
						(qword)ses->currentDate->getHumanReadableMinute();
					qword nowHRSec=
						(qword)ses->currentDate->getHumanReadableSecond();

					qword midnightSecs=ses->currentDate->getSecs() - 
						nowHRHour*3600 -
						nowHRMin*60 - nowHRSec;

					RecordingFileDateMetadataChunk preMidnight, postMidnight;
					// 23:59:59.999, per deixar la probabilitat de que hi hagi
					// un frame al 0,1%
					preMidnight.setDate(midnightSecs-1, 999);
					// 00:00:00.000
					postMidnight.setDate(midnightSecs, 0);

//					if(now.getHumanReadableDay() != updSes->currentDate->getHumanReadableDay())
					if (humanPrevDay != humanCurrDay)
					{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						//tanquém l'index anterior...
						if (updSes->recIndexIdx==0xffffffffffffffffll)
						{
							cout<<" ------- cam "<<updSes->sessionParams.devId<<" tall mitjanit : recIndex invalidat ------- "<<endl;
							// Pot haver-se invalidat. Busquem un segment que tingui
							// endDate.isValid a false i que sigui de la cam i data q toca,
							// mes o menys.
							bool found=false;
							int nfound=0;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							for (qword s=0; s<recIndex->nSegments; s++)
							{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								const RecordingModuleInterface::recordingSegment *ri=recIndex->getSegment(s);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)ri;
								if (!r.endDate.isValid && r.devId==updSes->sessionParams.devId
										&& r.recType==updSes->sessionParams.recType
										&& r.fps.whole==updSes->sessionParams.fpswhole
										&& r.fps.frac==updSes->sessionParams.fpsfrac
										&& (r.startDate.secs<updSes->currentDate->getSecs() ||
											(r.startDate.secs==updSes->currentDate->getSecs() &&
											 r.startDate.millis<=updSes->currentDate->getMillis())))
								{
									nfound++;
									found = true;
							cout<<" recIndex invalidated, "<<__LINE__<<",  found dev:"<<r.devId<<" (prev, closing "<<nfound<<") start:"<<StrUtils::toDateString(r.startDate.secs)<<"."<<setfill('0')<<setw(3)<<r.startDate.millis<<" current:"<<StrUtils::toDateString(updSes->currentDate->getSecs())<<"."<<setfill('0')<<setw(3)<<updSes->currentDate->getMillis()<<endl;

									r.endDate.isValid=true;//false;
									r.endDate.secs=preMidnight.getSecs();
									r.endDate.millis=preMidnight.getMillis();
									updSes->recIndexIdx=s;
									recIndex->alterSegment(updSes->recIndexIdx, r);
								}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
							}

							if(nfound == 0)
							{
								cout<<" Warning: updSes invalidated and no recIndex found to close, "<<__LINE__<<",  dev:"<<updSes->sessionParams.devId<<"date::"<<StrUtils::toDateString(updSes->currentDate->getSecs())<<endl;
								bool found=false;
								for (qword s=0; s<recIndex->nSegments; s++)
								{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
									const RecordingModuleInterface::recordingSegment *ri=recIndex->getSegment(s);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
									RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)ri;
									if (r.devId==updSes->sessionParams.devId && !r.endDate.isValid)
									{
										cout<<" line "<<__LINE__<<" recindex, endDate invalida:"<<r.devId<<" (prev, not closing ) start:"<<StrUtils::toDateString(r.startDate.secs)<<"."<<setfill('0')<<setw(3)<<r.startDate.millis<<" current:"<<StrUtils::toDateString(updSes->currentDate->getSecs())<<"."<<setfill('0')<<setw(3)<<updSes->currentDate->getMillis()<<endl;
										found=true;
									}
									else if (r.devId==updSes->sessionParams.devId && 
										ABS(((long long)r.endDate.secs)-((long long)updSes->currentDate->getSecs())) < 30)
									{
										cout<<" line "<<__LINE__<<" recindex endDate propera:"<<r.devId<<" (prev, not closing ) start:"<<StrUtils::toDateString(r.startDate.secs)<<"."<<setfill('0')<<setw(3)<<r.startDate.millis<<" current:"<<StrUtils::toDateString(updSes->currentDate->getSecs())<<"."<<setfill('0')<<setw(3)<<updSes->currentDate->getMillis()<<endl;
										found=true;
									}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
								}
								if(!found)
								{
									cout<<"/Warning, dev:"<<updSes->sessionParams.devId<<", no suitable sessions found"<<endl;
								}
							}
						}
						else
						{
							cout<<" ------- cam "<<updSes->sessionParams.devId<<" tall mitjanit: tanquem recIndex------- "<<endl;
							const RecordingModuleInterface::recordingSegment *ri=recIndex->getSegment(updSes->recIndexIdx);
							RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)ri;
							r.endDate.isValid=true;
							r.endDate.secs=preMidnight.getSecs();
							r.endDate.millis=preMidnight.getMillis();
							recIndex->alterSegment(updSes->recIndexIdx, r);
						}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						// ... i obrim el seguent

						RecordingModuleInterface::recordingSegment seg;
						seg.devId=updSes->sessionParams.devId;
						seg.recType=updSes->sessionParams.recType;
						seg.fps.whole=updSes->sessionParams.fpswhole;
						seg.fps.frac=updSes->sessionParams.fpsfrac;
						seg.startDate.isValid=true;
						seg.startDate.secs=postMidnight.getSecs();
						seg.startDate.millis=postMidnight.getMillis();
						seg.endDate.isValid=false;
						seg.endDate.secs=ses->currentDate->getSecs();
						seg.endDate.millis=ses->currentDate->getMillis();
						updSes->recIndexIdx=recIndex->addSegment(seg);
					}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					updSes->currentDate->setDate(ses->currentDate->getSecs(), ses->currentDate->getMillis());

					if (updSes->recIndexIdx==0xffffffffffffffffll)
					{
						// Pot haver-se invalidat. Busquem un segment que tingui
						// endDate.isValid a false i que sigui de la cam i data q toca,
						// mes o menys.
						bool found=false;

						qword s=0;
						cout << "updSes recIndex invalidated ("<<updSes->sessionParams.devId<<") getSegment (total " << std::dec << recIndex->nSegments << ") current:"<<StrUtils::toDateString(updSes->currentDate->getSecs())<<"."<<setfill('0')<<setw(3)<<updSes->currentDate->getMillis()<<endl;
						cout.flush();

						for (s=0; s<recIndex->nSegments; s++)
						{
							const RecordingModuleInterface::recordingSegment *ri=recIndex->getSegment(s);
							RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)ri;

							if (!r.endDate.isValid && r.devId==updSes->sessionParams.devId
									&& r.recType==updSes->sessionParams.recType
									&& r.fps.whole==updSes->sessionParams.fpswhole
									&& r.fps.frac==updSes->sessionParams.fpsfrac
									&& (r.startDate.secs<updSes->currentDate->getSecs() ||
										(r.startDate.secs==updSes->currentDate->getSecs() &&
										 r.startDate.millis<=updSes->currentDate->getMillis())))
							{
								found=true;

								r.endDate.isValid=false;
								r.endDate.secs=updSes->currentDate->getSecs();
								r.endDate.millis=updSes->currentDate->getMillis();

								updSes->recIndexIdx=s;
								recIndex->alterSegment(updSes->recIndexIdx, r);
								cout<<" recIndex invalidated, "<<__LINE__<<",  found dev:"<<r.devId<<" start:"<<StrUtils::toDateString(r.startDate.secs)<<"."<<setfill('0')<<setw(3)<<r.startDate.millis<<" current:"<<StrUtils::toDateString(updSes->currentDate->getSecs())<<"."<<setfill('0')<<setw(3)<<updSes->currentDate->getMillis()<<endl;
								break;
							}
						}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

						for (s++;s<recIndex->nSegments; s++)
						{
							const RecordingModuleInterface::recordingSegment *ri=recIndex->getSegment(s);
							RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)ri;

							if (!r.endDate.isValid && r.devId==updSes->sessionParams.devId
									&& r.recType==updSes->sessionParams.recType
									&& r.fps.whole==updSes->sessionParams.fpswhole
									&& r.fps.frac==updSes->sessionParams.fpsfrac
									&& (r.startDate.secs<updSes->currentDate->getSecs() ||
										(r.startDate.secs==updSes->currentDate->getSecs() &&
										 r.startDate.millis<=updSes->currentDate->getMillis())))
							{
								cout<<"Warning: recIndex invalidated, "<<__LINE__<<", (post) found dev:"<<r.devId<<" start:"<<StrUtils::toDateString(r.startDate.secs)<<" current:"<<StrUtils::toDateString(updSes->currentDate->getSecs())<<endl;
							}
						}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						
						// Si no hem trobat cap invalidat, es que estem començant
						if (!found)
						{
							cout<<" recIndex invalidated, "<<__LINE__<<",  not found dev:"<<updSes->sessionParams.devId<<" opening new segment:"<<StrUtils::toDateString(updSes->currentDate->getSecs())<<"."<<setfill('0')<<setw(3)<<updSes->currentDate->getMillis()<<endl;
							RecordingModuleInterface::recordingSegment seg;
							seg.devId=updSes->sessionParams.devId;
							seg.recType=updSes->sessionParams.recType;
							seg.fps.whole=updSes->sessionParams.fpswhole;
							seg.fps.frac=updSes->sessionParams.fpsfrac;
							seg.startDate.isValid=true;
							seg.startDate.secs=updSes->currentDate->getSecs();
							seg.startDate.millis=updSes->currentDate->getMillis();
							seg.endDate.isValid=false;
							seg.endDate.secs=updSes->currentDate->getSecs();
							seg.endDate.millis=updSes->currentDate->getMillis();

							updSes->recIndexIdx=recIndex->addSegment(seg);
						}

					}
					else
					{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						const RecordingModuleInterface::recordingSegment *ri=recIndex->getSegment(updSes->recIndexIdx);
						RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)ri;
						r.endDate.isValid=false;
						r.endDate.secs=updSes->currentDate->getSecs();
						r.endDate.millis=updSes->currentDate->getMillis();

						recIndex->alterSegment(updSes->recIndexIdx, r);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					}
				}
				catch (Exception &e)
				{
					try
					{
						cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: saveFrame (updateSes) " << 
							e.getClass() << ": " << e.getMsg() <<" Address:"<<a->toString()<<" starting addr:"<<startAddr<< endl;
					}catch(...){}
					updSes->sMutex->unlock();
					RecordingModuleInterface::unlockRecIndex();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

					updSes->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

					ses->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					SessionManager<session>::release(a->toString());
					SessionManager<session>::release(updSes);
					throw(e);
				}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				updSes->sMutex->unlock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

				RecordingModuleInterface::unlockRecIndex();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

				updSes->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				SessionManager<session>::release(updSes);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			CHECKPOINT();
			}
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: saveFrame (updateSes loop) " << 
				e.getClass() << ": " << e.getMsg() <<" Address:"<<a->toString()<<" starting addr:"<<startAddr<<" updSes:"<<((updSes==NULL)?string("NULL"):updSes->toString())<< endl;
			//e.serialize()->materializeAndThrow(true);
			throw;
		}

		CHECKPOINT();

		if(!sesFound)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: saveFrame (updateSes loop) Address:  no recording session found"<<endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			ses->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			SessionManager<session>::release(a->toString());
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			throw RecordingModuleException(0, string("No recording session found"));
		}

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
//		ses->currentDate->setDate(now.getSecs(), now.getMillis());	
		string fname=_this->getAutomaticFileName(ses->sessionParams.devId, ses->currentDate);
		string pathOnly=_this->getAutomaticFileBasePath(ses->sessionParams.devId, ses->currentDate);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

		fileOffset frameOff=0;
		RecordingFileFrameIndex *frIdx = NULL;
		RecordingFileIndexChunk *currIdx = NULL;
		dword absoluteFrameNumber = 0;
		RecordingFile *rf=NULL;
		
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
		ses->sMutex->lock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
		CHECKPOINT();
		try
		{
			if (ses->currentFilename!=fname)
			{
				string prev = ses->currentFilename;
				CHECKPOINT();
				RecordingModuleInterface::createPath(pathOnly);

				bool newFile=false;
				
				ses->currentFilename=fname;		// Aixo es una cosa que cal guardar "immediatament"

				// "assegurem" que no se'ns tanqui (si ja ho ha fet
				// no passa res)
				CHECKPOINT();
				RecordingModuleInterface::diskWriter->resetCountEmpty(ses->currentFilename);

				CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				RecordingFileHeaderChunk *rfhc =
					RecordingModuleInterface::diskWriter->getHeaderForFile(
						ses->currentFilename);
						
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				if (rfhc==NULL)
				{
					CHECKPOINT();
					// Si no hi ha capçalera no hi haura index, ho creem tot
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 0 first"<<endl; 
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					frIdx = new RecordingFileFrameIndex(
						RecordingModuleInterface::diskWriter);
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 1 setFile"<<endl; 
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					frIdx->setFile(ses->currentFilename);
					
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 2 link"<<endl; 
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					// Crea la capçalera i enllaça. A mes envia l'index a
					// escriure
					CHECKPOINT();
					frIdx->linkFirstIndexChunk(
						RecordingFile::firstUsableOffset(), 
						RecordingModuleInterface::headerReservedBytes,
						ses->sessionParams.fourcc,
						ses->sessionParams.recType,
						ses->sessionParams.fpswhole,
						ses->sessionParams.fpsfrac);
					// reconeixem l'index.
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 3 setIndex"<<endl; 
#endif
					CHECKPOINT();
					RecordingFileFrameIndices::setIndexForFile(
						ses->currentFilename, frIdx);
	
					CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 4 getHeader"<<endl; 
#endif
					rfhc=RecordingModuleInterface::diskWriter->getHeaderForFile(
						ses->currentFilename);
					
					CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 5"<<endl; 
#endif
					if (rfhc==NULL)
					{
						cout << "WARNING: Header is NULL @ " << __FILE__
							<< " line " << std::dec << __LINE__ << ": Not saving frame for device: " << ses->sessionParams.devId 
							<< " in file: " << ses->currentFilename << " initial file: " << fname << " previous current: " << prev << endl;
						
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						ses->sMutex->unlock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

						ses->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

						SessionManager<session>::release(a->toString());

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						// Retornem un packet d'OK com si no hagues passat
						// res... Ja veurem si s'executa aquest codi que
						// caldra fer :P
						RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
						return pk;
					}
					
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 7"<<endl; 
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					currIdx = frIdx->getChunkByAbsoluteFrame(0);
					
					CHECKPOINT();
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 8"<<endl; 
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					frameOff = RecordingFile::firstUsableOffset() +
						RecordingModuleInterface::headerReservedBytes + 
						sizeof(dword) + currIdx->size();

					// Enviem la capçalera a escriure's. La marquem com
					// no borrable, ja que es gestiona des de DiskWriter
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 9"<<endl; 
#endif
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					RecordingModuleDiskWriterEntry *entry =
						new RecordingModuleDiskWriterEntry(
							RecordingFile::firstUsableOffset(),
							rfhc->size(), rfhc, false, false);
//					float fullFactor = 
					CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 10"<<endl; 
#endif
						RecordingModuleInterface::diskWriter->addEntryToFile(
						ses->currentFilename, entry);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
#ifdef REC_FILE_DEBUG
					cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << std::dec << ses->sessionParams.devId <<"]: saveFrame rfhc==NULL 11 last"<<endl; 
#endif

				}		
				else	// Ja tenim capçalera i per tant index
				{
					CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					frIdx = RecordingFileFrameIndices::getIndexForFile(
						ses->currentFilename,
						RecordingModuleInterface::diskWriter);
					
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					// Ens cal saber quin es el proper frame que toca
					absoluteFrameNumber=frIdx->getLastFrameIndex() + 1;

					// Ens guardem l'offset de l'ultim frame guardat
					frameOff =
						frIdx->getAbsoluteFrameOffset(absoluteFrameNumber-1);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					CHECKPOINT();
					// Llegim el tamany del frame (i de moment no tenim mes
					// remei que llegir tb el frame)... Sort que aixo no
					// hauria de passar gaire (no te logica canvi de fitxer +
					// fitxer ja existeix!)
					dword frSize=0;
					void *frame=NULL;
					try
					{
						CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						rf = RecordingModuleInterface::diskWriter->
								getRecordingFile(ses->currentFilename);
				
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						CHECKPOINT();
						frame=rf->readBlock(&frSize, frameOff);
						delete [] (byte*)frame;
						frame=NULL;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					}
					catch (Exception &e)
					{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						if (frame!=NULL)
							delete [] (byte*)frame;
						frame=NULL;
						//e.serialize()->materializeAndThrow(true);
						throw;
					}
					if (frSize>0)
						frameOff += sizeof(dword) + frSize;

					// Mirem si hi ha index pel seguent frame
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					currIdx = 
						frIdx->getChunkByAbsoluteFrame(absoluteFrameNumber);
						
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					if (currIdx==NULL)
					{
						// Cal afegir un nou index!
						CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						frIdx->linkNewIndexChunk(frameOff);
						
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						currIdx =
							frIdx->getChunkByAbsoluteFrame(absoluteFrameNumber);
						
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
						// saltem l'index
						frameOff += sizeof(dword) + currIdx->size();
					}
					CHECKPOINT();
				}

				// En arribar aqui, frameOff es l'offset ON TOCA ESCRIURE
//				ses->header=rfhc;
			}
			else
			{
				// "assegurem" que no se'ns tanqui (si ja ho ha fet
				// no passa res)
				CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				RecordingModuleInterface::diskWriter->resetCountEmpty(ses->currentFilename);

				// Ens situem sobre l'offset ON TOCA ESCRIURE
				frameOff=ses->nextFrameOff;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				frIdx = RecordingFileFrameIndices::getIndexForFile(
					ses->currentFilename,
					RecordingModuleInterface::diskWriter);

				absoluteFrameNumber=ses->absFrameW+1;
				
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				currIdx = 
					frIdx->getChunkByAbsoluteFrame(absoluteFrameNumber);
					
				if (currIdx==NULL)
				{
					// Cal afegir un nou index!
					CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					frIdx->linkNewIndexChunk(frameOff);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					
					currIdx =
						frIdx->getChunkByAbsoluteFrame(absoluteFrameNumber);
					
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
					// saltem l'index
					frameOff += sizeof(dword) + currIdx->size();
				}
				CHECKPOINT();
			}
		}
		catch(std::exception &stde)
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: saveFrame (open/create File) " << 
				stde.what() <<" Address:"<<a->toString()<<" starting addr:"<<startAddr<< endl;
			ses->sMutex->unlock();
			ses->endSessionMutex->unlock();
			SessionManager<session>::release(a->toString());
			throw;
		}
		catch (Exception &e)
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: saveFrame (open/create File) " << 
				e.getClass() << ": " << e.getMsg() <<" Address:"<<a->toString()<<" starting addr:"<<startAddr<< endl;
			ses->sMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			ses->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			SessionManager<session>::release(a->toString());
			throw(e);
		}

		CHECKPOINT();
		STACKTRACE_EXTRATRACE();
	
		try
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			rf = RecordingModuleInterface::diskWriter->
					getRecordingFile(ses->currentFilename);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
		}
		catch (std::exception stde)
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: saveFrame (open/create File) " << 
				stde.what() <<" Address:"<<a->toString()<<" starting addr:"<<startAddr<< endl;
			ses->sMutex->unlock();
			ses->endSessionMutex->unlock();
			SessionManager<session>::release(a->toString());
			throw;
		}
		catch (Exception &e)
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: saveFrame (open/create File) " << 
				e.getClass() << ": " << e.getMsg() <<" Address:"<<a->toString()<<" starting addr:"<<startAddr<< endl;
			ses->sMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			ses->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

			SessionManager<session>::release(a->toString());
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			throw(e);
		}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
		ses->sMutex->unlock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

		// Gravem el frame
		dword frameSize=p.chunk->size();

		// En aquest moment frameOff ja conte un valor valid.

		fileOffset nextFrameOffset=frameOff+((fileOffset)frameSize+sizeof(dword));


#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
		ses->sMutex->lock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
		try
		{
			// I finalment, podem enviar el frame a gravar-se i
			// updatem l'index que toca

			ses->absFrameW=absoluteFrameNumber;
			
			// currIdx no hauria de ser NULL; pero per si les mosques...
			if (currIdx==NULL)
				currIdx = frIdx->getChunkByAbsoluteFrame(ses->absFrameW);
			
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

			// Enviem el frame a escriure's
			p.chunk->setEndian(rf->getEndian());

			STACKTRACE_EXTRATRACE();
	
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			RecordingModuleDiskWriterEntry *entry =
				new RecordingModuleDiskWriterEntry(frameOff,
					p.chunk->size(), p.chunk);
			
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			RecordingModuleInterface::diskWriter->addEntryToFile(
				ses->currentFilename, entry);

			// ja el borrara el writer.		
			p.chunk=NULL;
			
			STACKTRACE_EXTRATRACE();
	
			// Ja hem garantit que cap i afegit indexos si cal, i a mes hem
			// enviat el frame a escriure's
			currIdx->setEndian(rf->getEndian());

			currIdx->setFrame(ses->absFrameW, ses->currentDate->getSecs(),
				ses->currentDate->getMillis(), frameOff);

			// Enviem l'index a escriure's - i marquem que no s'esborri!
			entry = new RecordingModuleDiskWriterEntry(
				frIdx->getChunkOffset(currIdx),
				currIdx->size(), currIdx, true, false);
					
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

			RecordingModuleInterface::diskWriter->addEntryToFile(
				ses->currentFilename, entry);

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

			STACKTRACE_EXTRATRACE();
	
//			cout << ">";
//			cout.flush();

			// Movem el "punter" d'on toca escriure el proxim frame.
			// Es suma el tamany del frame mes el dword que guarda el tamany
			ses->nextFrameOff=nextFrameOffset;
			ses->tOutTimer.start();

			if (ses->sMutex->isHeldByCurrentThread())
				ses->sMutex->unlock();
			else
				cout << __FILE__ << " Line " << std::dec << __LINE__ << " sMutex NO lockat" << endl;
			if (ses->endSessionMutex->isHeldByCurrentThread())
			{

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
				ses->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			}
			else
				cout << __FILE__ << " Line " << std::dec << __LINE__ << " endSessionMutex NO lockat" << endl;

			SessionManager<session>::release(a->toString()); //TODO: fer el release de la sessió directament
			//independentment del nom

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

			CHECKPOINT();

		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: saveFrame " << 
				e.getClass() << ": " << e.getMsg() <<" Address:"<<a->toString()<<" starting addr:"<<startAddr<< endl;
			ses->sMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			ses->endSessionMutex->unlock();

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
			SessionManager<session>::release(a->toString());
			throw(e);
		}

		STACKTRACE_EXTRATRACE();
		RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);

		CHECKPOINT();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif

		return pk;
	}
	catch (Exception &e)
	{
		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + CHECKPOINT_TOSTRING() + string(": ") + e.getClass() + string(": ") + e.getMsg() << endl;		
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	catch(std::exception &stde)
	{
		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + CHECKPOINT_TOSTRING() + string(": ") + string(stde.what()) << endl;
		throw(stde);
	}
	
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x40000000 | __LINE__;
#endif
	return NULL;
}

RPCPacket* RecordingModuleInterface::readFrame(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	INIT_CHECKPOINT();	
	Timer t;
	t.start();

	RecordingModuleInterface::session *ses=NULL;

	try
	{
	// 	cout << "--> RecordingModuleInterface::readFrame" << endl;
		RMReadFrameParams p;
		p.toLocal(params);
		
		try
		{
			ses=RecordingModuleInterface::getSessionForAddress(a);
		}
		catch (RedirectCallException &rce)
		{
			cout << rce.getClass() << ": " << rce.getMsg() << endl;
			return NULL;
		}
	
		CHECKPOINT();
		while (!ses->endSessionMutex->isHeldByCurrentThread())
		{
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->lock()") << endl;
	//#endif
			ses->endSessionMutex->lock(true, "readFrame");
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->lock() LOCKED") << endl;
	//#endif
	
			CHECKPOINT();
			if (ses->sessionStarted==false)
			{
				// Pot haver canviat ses de lloc!!!
				CHECKPOINT();
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
	//
	//#endif
				ses->endSessionMutex->unlock();
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
	//#endif
				
				ses=NULL;
				try
				{
					ses=RecordingModuleInterface::getSessionForAddress(a);
				}
				catch (Exception &e)
				{
					try{
						SessionManager<session>::release(a->toString());
					}catch(...){}
					throw (RecordingModuleSessionNotStablishedException(2, string("Session no longer stablished with any device from client ")+a->toString()+string(" (readFrame)")));
				}
				
				if (ses!=NULL)
					ses->sessionStarted=true;
			}
		}
	
		RecordingFileDateMetadataChunk *reqDate=NULL;
		RecordingFileDateMetadataChunk *prevFrameDate=NULL;
		double reqSecs=0;
	
	 //	cout << "--> RecordingModuleInterface::readFrame 1" << endl;
			
		try
		{
			CHECKPOINT();
			reqDate=(RecordingFileDateMetadataChunk*)p.chunk->getSubChunk(0);
			reqSecs=reqDate->microtime();
		}
		catch(Exception &e)
		{
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
	//
	//#endif
			ses->endSessionMutex->unlock();
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
	//#endif
			SessionManager<session>::release(a->toString());
			throw e;
		}	
	
	//	cout.fill('0');
	//	cout << "RecordingModuleInterface::readFrame 1, reqDate " << reqDate->getSecs() << ".";
	//	cout.width(3);
	//	cout << reqDate->getMillis() << endl;
	//	cout.width(0);
		
		CHECKPOINT();
	//	ses->currentDate->setCurrentDate();
		string fname=_this->getAutomaticFileName(ses->sessionParams.devId, reqDate);
		string pathOnly=_this->getAutomaticFileBasePath(ses->sessionParams.devId, reqDate);
		
	//	cout << "RecordingModuleInterface::readFrame fname: " << fname <<  endl;
	
		fileOffset frameOff=0;
		dword absoluteFrameNumber=0;
		RecordingFile *rf=NULL;
		RecordingFileFrameIndex *frIdx = NULL;
		RecordingFileIndexChunk *currIdx = NULL;
		
	//	cout << "--> RecordingModuleInterface::readFrame 2" << endl;
		CHECKPOINT();
		ses->sMutex->lock();
	
		try
		{
			CHECKPOINT();
			if (ses->currentFilename2!=fname)
			{
				ses->currentFilename2=fname;		// Aixo es una cosa que cal guardar "immediatament"
				ses->cfn2Error=0;
			}
			
			CHECKPOINT();
			rf = RecordingModuleInterface::diskWriter->
					getRecordingFile(ses->currentFilename2, true);
	
			CHECKPOINT();
			// Si no esta carregat, ja el carrega del fitxer
			frIdx = RecordingFileFrameIndices::getIndexForFile(
				ses->currentFilename2,
				RecordingModuleInterface::diskWriter);
	
			CHECKPOINT();
			if (frIdx==NULL)
			{
				// Excepcio, no trobem el fitxer o el que sigui...
				// En principi no hauria de passar mai...
				throw Exception(string(__FILE__) + string(" line ")
					+ StrUtils::decToString(__LINE__) + string(": Unexpected "
						"situation: frIdx==NULL"));
			}
			
			CHECKPOINT();
			// Busquem l'index que conté la data més propera a la data solicitada...
			// Assumim q sera anterior a la data actual ;)
			currIdx = frIdx->getChunkByNearestDate(reqSecs, &absoluteFrameNumber);
			
			CHECKPOINT();
			// Ens situem sobre el frame q toca,  *** SOBRE, NO DESPRES ***
			RecordingFileIndexChunk::indexEntry *idxEntry=currIdx->getFrame(absoluteFrameNumber);
				
			frameOff=idxEntry->frameOffset;
		}
		catch (std::exception stde)
		{
			cout << "RecordingModuleInterface::readFrame Exception: " << stde.what()<< endl;
			ses->sMutex->unlock();
			ses->endSessionMutex->unlock();
			SessionManager<session>::release(a->toString());
			throw ;
		}	
		catch (Exception &e)
		{
			if(e.getClass() == string("FileException"))
			{
				ses->cfn2Error++;
				if(ses->cfn2Error == 1)
					cout << "RecordingModuleInterface::readFrame Exception: " << e.getClass() << ": " << e.getMsg() << endl;
				else if(ses->cfn2Error==250) //10s @25fps
					ses->cfn2Error=0;
			}
			else
			{
				cout << "RecordingModuleInterface::readFrame Exception: " << e.getClass() << ": " << e.getMsg() << endl;
			}
			ses->sMutex->unlock();
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
	//
	//#endif
			ses->endSessionMutex->unlock();
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
	//#endif
			SessionManager<session>::release(a->toString());
			throw;
		}	
		ses->sMutex->unlock();
	//	cout << "--> RecordingModuleInterface::readFrame 7" << endl;
		
		// Llegim un frame... no cal comrpovar q es el mes proper a la data q ens passen, pq teoricament ja ho hem fet
		// abans :P
	
		// Mirem primer si esta pendent d'escriure...
		CHECKPOINT();
		RecordingModuleDiskWriterEntryGrouper *fileEntries =
			RecordingModuleInterface::diskWriter->getEntriesForFile(
				ses->currentFilename2);
		
		CHECKPOINT();
		RecordingFileFrameChunk *frameChunk=NULL;
		if (fileEntries!=NULL)
		{
			fileEntries->rlock();
			
			list<RecordingModuleDiskWriterEntryGroup*> *groups = 
				fileEntries->getList();
			
			list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
			for (gIt=groups->begin(); gIt!=groups->end() && frameChunk==NULL; gIt++)
			{
				RecordingModuleDiskWriterEntryGroup *g=*gIt;
	
				// Mirem si conte la capçalera
				frameChunk = (RecordingFileFrameChunk*)
					g->getChunkForOffset(frameOff);
			}
			
			fileEntries->unlock();
		}
		
		CHECKPOINT();
		// Si no esta pendent d'escriure, cal llegir-lo...
		dword frameSize=0;
		try
		{
			if (frameChunk==NULL)
			{
				void *frameData=rf->readBlock(&frameSize, frameOff);
		
				if (frameData!=NULL)
				{
					frameChunk=(RecordingFileFrameChunk *)
						RecordingFileChunk::newChunk(frameData, frameSize, 
							rf->getEndian());
		
					delete [] (byte*)frameData;
				}
			}
		}
		catch (Exception &e)
		{
			ses->endSessionMutex->unlock();
			cout << __FILE__ << " line " << std::dec << __LINE__ << ": frameSize: " << 
				frameSize << ", frameOff: " << frameOff << endl;

			SessionManager<session>::release(a->toString());

			//e.serialize()->materializeAndThrow(true);
			throw;
		}
				
		CHECKPOINT();
		dword nch = frameChunk->getNSubChunks();
	
		RecordingFileFrameInfoMetadataChunk *frameMetaData=NULL;
		RecordingFileStringMetadataChunk *keyChunk=NULL;
		RecordingFileDateMetadataChunk *dateChunk=NULL;
		int keyChunkIdx=-1;
		int dateChunkIdx=-1;
	
		CHECKPOINT();
		for (dword i=0; i<nch; i++)
		{
			RecordingFileChunk *ch=frameChunk->getSubChunk(i);
	
			if (ch->getClass()==string("RecordingFileFrameInfoMetadataChunk"))
			{
				frameMetaData=(RecordingFileFrameInfoMetadataChunk*)ch;
			}
			else if (ch->getClass()==string("RecordingFileStringMetadataChunk"))
			{
				map <string,string> md=((RecordingFileStringMetadataChunk*)ch)->getMetadata();
				if (md[string("name")]==string("isKeyframe"))
				{
					keyChunk=(RecordingFileStringMetadataChunk*)ch;
					keyChunkIdx=i;
				}
			}
			else if (ch->getClass()==string("RecordingFileDateMetadataChunk"))
			{
				dateChunk=(RecordingFileDateMetadataChunk*)ch;
				dateChunkIdx=i;
			}
		}
	
		CHECKPOINT();
		byte *frData=(byte*)frameChunk->getFrame();
	
		map <string,string> md=((RecordingFileStringMetadataChunk*)keyChunk)->getMetadata();
	
		string value=md["value"];
	
		int nFrames = 0;//*((int*)frData);
	
		string sizeHdr = string("");
		string frames = string("");
		bool key=false;
	
		int firstFrIdx=absoluteFrameNumber;
		int lastFrIdx=absoluteFrameNumber;
	
		CHECKPOINT();
	//	TimerInstant ti=t.time();
	//	if(ti.seconds() > 1)
	//	{
	//		cout<<"RMI::readFrame WARNING: readFrame time:"<<ti.seconds()<<" usecs:"<<ti.useconds()<<endl;
	//		cout << " reading:" << ses->lastSentDate->getHumanReadableYear() << "/" << (int)ses->lastSentDate->getHumanReadableMonth() << "/" << (int)ses->lastSentDate->getHumanReadableDay() << "(" << (int)ses->lastSentDate->getHumanReadableHour() << ":" << (int)ses->lastSentDate->getHumanReadableMinute() << ":" << (int)ses->lastSentDate->getHumanReadableSecond() << " )"  << endl;
	//	}
		if(value==string("false"))
		{
			CHECKPOINT();
			if(ses->lastReqDate->getSecs() > reqDate->getSecs() ||(ses->lastReqDate->getSecs() == reqDate->getSecs() && ses->lastReqDate->getMillis() > reqDate->getMillis()))
			{
				ses->lastSentDate->setDate(0,0);
			}
	
			int i;
	
			CHECKPOINT();
	
			
			for(i=absoluteFrameNumber; i>0; i--)
			{
				if(i<currIdx->getFirstFrame()) //Canvi d'index (idx -> 4096 frames)
				{
					currIdx=frIdx->getChunkByAbsoluteFrame(i);
					if(currIdx == NULL)
					{
						cout<<" RecordingModuleInterface::readFrame : WARNING frameIndexChunk == NULL while changing to index:"<<i<<endl;
						break;
					}
	//				cout<<" RecordingModuleInterface::readFrame canvi d'index i:"<<i<<" nou: current fisrt:"<<currIdx->getFirstFrame()<<" size:"<<currIdx->getIndexSize()<<endl;
				}
	
				RecordingFileIndexChunk::indexEntry *prevEnt=currIdx->getFrame(i);
				fileOffset frOff=prevEnt->frameOffset;
				if(prevEnt->date.secs==0)
				{
					// data no inicialitzada: no hi ha frame - no hauria de passar
					continue;
				}
	
				CHECKPOINT();
				// Llegim el frame anterior
				RecordingFileFrameChunk *prevFrChunk=NULL;
				if (fileEntries!=NULL)
				{
					fileEntries->rlock();
					
					list<RecordingModuleDiskWriterEntryGroup*> *groups = 
						fileEntries->getList();
					
					list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
					for (gIt=groups->begin(); gIt!=groups->end() && prevFrChunk==NULL; gIt++)
					{
						RecordingModuleDiskWriterEntryGroup *g=*gIt;
			
						// Mirem si conte la capçalera
						prevFrChunk = (RecordingFileFrameChunk*)
							g->getChunkForOffset(frOff);
					}
					
					fileEntries->unlock();
				}
				
				CHECKPOINT();
				// Si no esta pendent d'escriure, cal llegir-lo...
				dword prevFrSize=0;
				try
				{
					if (prevFrChunk==NULL)
					{
						void *frameData=rf->readBlock(&prevFrSize, frOff);
				
						if (frameData!=NULL)
						{
							prevFrChunk=(RecordingFileFrameChunk *)
								RecordingFileChunk::newChunk(frameData, prevFrSize, 
									rf->getEndian());
				
							delete [] (byte*)frameData;
						}
					}
				}
				catch (Exception &e)
				{
					ses->endSessionMutex->unlock();
					cout << __FILE__ << " line " << std::dec << __LINE__ << ": prevFrSize: " << 
						prevFrSize << ", frOff: " << frOff << endl;
					SessionManager<session>::release(a->toString());
					//e.serialize()->materializeAndThrow(true);
					throw;
				}
	
				CHECKPOINT();
	
				byte *frData=(byte*)prevFrChunk->getFrame();//Data();
				int f = *((int*)frData);
				CHECKPOINT();
				if(f==0)
				{
					// Si estava pendent d'escriure, hem de comprovar si s'ha escrit i 
					// si ja ho ha fet, com que haura quedat marcat com no borrable, borrar-lo.
					if (prevFrChunk!=NULL)
					{
						bool writePending=false;
						if (fileEntries!=NULL)
						{
							RecordingFileFrameChunk *prevFrChunk=NULL;
							fileEntries->rlock();
							
							list<RecordingModuleDiskWriterEntryGroup*> *groups = 
								fileEntries->getList();
							
							RecordingModuleDiskWriterEntry *entry;
							list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
							for (gIt=groups->begin(); gIt!=groups->end() && prevFrChunk==NULL && !writePending; gIt++)
							{
								RecordingModuleDiskWriterEntryGroup *g=*gIt;
					
								entry=g->getEntryForOffset(frOff);
									
								if (entry!=NULL)
									prevFrChunk = (RecordingFileFrameChunk*)
										entry->getChunk();
								
								writePending=(entry!=NULL);
							}
							
							if (writePending)
								entry->setDeleteAtDestructor(true);
							
							fileEntries->unlock();
						}
						
						if (!writePending)
							delete prevFrChunk;
					}
					
					continue;
				}
		
				CHECKPOINT();
	
				dword nch = prevFrChunk->getNSubChunks();
	
				RecordingFileStringMetadataChunk *keyChunk=NULL;
				RecordingFileDateMetadataChunk *dateChunk=NULL;
				for (dword j=0; j<nch; j++)
				{
					RecordingFileChunk *ch=prevFrChunk->getSubChunk(j);
					if (ch->getClass()==string("RecordingFileStringMetadataChunk"))
					{
						map <string,string> md=((RecordingFileStringMetadataChunk*)ch)->getMetadata();
						if (md[string("name")]==string("isKeyframe"))
							keyChunk=(RecordingFileStringMetadataChunk*)ch;
					}
					else if (ch->getClass()==string("RecordingFileDateMetadataChunk"))
					{
						dateChunk=(RecordingFileDateMetadataChunk*)ch;
					}
				}
	
				CHECKPOINT();
				if(ses->lastSentDate->getSecs() > dateChunk->getSecs() ||(ses->lastSentDate->getSecs() == dateChunk->getSecs() && ses->lastSentDate->getMillis() >= dateChunk->getMillis())) //lastSent > data actual -> guardem prev i break;
				{
					lastFrIdx=i+1;
	
					if (prevFrChunk!=NULL)
					{
						bool writePending=false;
						if (fileEntries!=NULL)
						{
							RecordingFileFrameChunk *prevFrChunk=NULL;
							fileEntries->rlock();
							
							list<RecordingModuleDiskWriterEntryGroup*> *groups = 
								fileEntries->getList();
							
							RecordingModuleDiskWriterEntry *entry;
							list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
							for (gIt=groups->begin(); gIt!=groups->end() && prevFrChunk==NULL && !writePending; gIt++)
							{
								RecordingModuleDiskWriterEntryGroup *g=*gIt;
					
								entry=g->getEntryForOffset(frOff);
									
								if (entry!=NULL)
									prevFrChunk = (RecordingFileFrameChunk*)
										entry->getChunk();
								
								writePending=(entry!=NULL);
							}
							
							if (writePending)
								entry->setDeleteAtDestructor(true);
							
							fileEntries->unlock();
						}
						
						if (!writePending)
							delete prevFrChunk;
					}
					break;
				}
	
	
				CHECKPOINT();
				nFrames += f;
	
				sizeHdr = string(((char*)frData)+sizeof(int), f*sizeof(int)) + sizeHdr;
	
				byte *nextFrame=(byte*)(frData+((f+1)*sizeof(int)));
	
				CHECKPOINT();
				frames = string((char*)nextFrame, prevFrChunk->getFrameSize() - ((f+1)*sizeof(int))) + frames;
	
				CHECKPOINT();
				map <string,string> md=((RecordingFileStringMetadataChunk*)keyChunk)->getMetadata();
	
				string value=md["value"];
				if(value==string("true"))
				{
					lastFrIdx=i;
					key=true;
					// BUGFIX? 20100629: En teoria s'hauria de fer normal
					// igual que si no fos key...
//					if (prevFrChunk!=NULL)
//						delete (RecordingFileFrameChunk *)prevFrChunk;
//					break;
				}
	
				CHECKPOINT();
				if (prevFrChunk!=NULL)
				{
					bool writePending=false;
					if (fileEntries!=NULL)
					{
						RecordingFileFrameChunk *prevFrChunk=NULL;
						fileEntries->rlock();
						
						list<RecordingModuleDiskWriterEntryGroup*> *groups = 
							fileEntries->getList();
	
						RecordingModuleDiskWriterEntry *entry;
						list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
						for (gIt=groups->begin(); gIt!=groups->end() && prevFrChunk==NULL && !writePending; gIt++)
						{
							RecordingModuleDiskWriterEntryGroup *g=*gIt;
				
							entry=g->getEntryForOffset(frOff);
								
							if (entry!=NULL)
								prevFrChunk = (RecordingFileFrameChunk*)
									entry->getChunk();
							
							writePending=(entry!=NULL);
						}
						
						if (writePending)
							entry->setDeleteAtDestructor(true);
						
						fileEntries->unlock();
					}
					
					if (!writePending)
						delete prevFrChunk;
				}
				
				// Veure BUGFIX 20100629 a dalt...
				if (key)
					break;
			}
		}
		else
		{
			CHECKPOINT();
	//		if(dateChunk != NULL && (ses->lastSentDate->getSecs() > dateChunk->getSecs() ||(ses->lastSentDate->getSecs() == dateChunk->getSecs() && ses->lastSentDate->getMillis() >= dateChunk->getMillis()))) 
			if(dateChunk != NULL && (ses->lastSentDate->getSecs() == dateChunk->getSecs() && ses->lastSentDate->getMillis() == dateChunk->getMillis()))
			{
	//			cout<<" RecordingModuleInterface::readFrame : returning empty key frame:"<<ses->lastSentDate<<endl;
	//			cout << " last sent:" << ses->lastSentDate->getHumanReadableYear() << "/" << (int)ses->lastSentDate->getHumanReadableMonth() << "/" << (int)ses->lastSentDate->getHumanReadableDay() << "(" << (int)ses->lastSentDate->getHumanReadableHour() << ":" << (int)ses->lastSentDate->getHumanReadableMinute() << ":" << (int)ses->lastSentDate->getHumanReadableSecond() << " )"  << endl;
	//			cout << " found :" <<dateChunk->getHumanReadableYear() << "/" << (int)dateChunk->getHumanReadableMonth() << "/" << (int)dateChunk->getHumanReadableDay() << "(" << (int)dateChunk->getHumanReadableHour() << ":" << (int)dateChunk->getHumanReadableMinute() << ":" << (int)dateChunk->getHumanReadableSecond() << " )"  << endl;
				nFrames=0;
				sizeHdr="";
				frames ="";
				key=false;
			}
			else
			{
				nFrames = *((int*)frData);
				sizeHdr = string((char*)frData+sizeof(int), nFrames*sizeof(int));
				byte *fr=(byte*)(frData+((nFrames+1)*sizeof(int)));
				frames = string((char*)fr, frameChunk->getFrameSize() - ((nFrames+1)*sizeof(int)));
				key=true;
			}
		}
	//	TimerInstant ti=t.time();
	//	if(ti.seconds() > 1)
	//	{
	//		cout<<"RMI::readFrame WARNING: readFrame(2) time:"<<ti.seconds()<<" usecs:"<<ti.useconds()<<endl;
	//		cout << " reading:" << ses->lastSentDate->getHumanReadableYear() << "/" << (int)ses->lastSentDate->getHumanReadableMonth() << "/" << (int)ses->lastSentDate->getHumanReadableDay() << "(" << (int)ses->lastSentDate->getHumanReadableHour() << ":" << (int)ses->lastSentDate->getHumanReadableMinute() << ":" << (int)ses->lastSentDate->getHumanReadableSecond() << " )"  << endl;
	//	}
	
		CHECKPOINT();
		ses->lastSentDate->setDate(dateChunk->getSecs(), dateChunk->getMillis());
		ses->lastReqDate->setDate(reqDate->getSecs(), reqDate->getMillis());
	
		CHECKPOINT();
	
		{
			//Ajuntem tots els catxos
			int len=sizeHdr.length() + frames.length() + sizeof(int);
			byte frame[len];
	
			*((unsigned int*)frame)=nFrames;
			memmove(frame+sizeof(int), sizeHdr.c_str(), sizeHdr.length());
			memmove(frame+sizeof(int)+sizeHdr.length(), frames.c_str(), frames.length());
			frameChunk->setFrame(frame, len);
			RecordingFileStringMetadataChunk *isKey=new RecordingFileStringMetadataChunk();
			if(key)
				isKey->setMetadata(string("isKeyframe"), string("true"));
			else
				isKey->setMetadata(string("isKeyframe"), string("false"));
			if(keyChunkIdx>=0)
			{
				frameChunk->replaceSubChunk(/*keyChunkIdx*/0, isKey, true);
			}
			else
			{
				frameChunk->addSubChunk(isKey);
			}
		}
	
		CHECKPOINT();
	
		// Hauriem d'haver rebut un frame buit per parametre, aixi que el borrem :)
		// I si no es buit, doncs tambe el borrem perque no el volem per res
		if (p.chunk!=NULL)
			delete p.chunk;
			
		p.chunk=frameChunk;
		ses->tOutTimer.start();
	
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
	//
	//#endif
		ses->endSessionMutex->unlock();
	//#ifndef WIN32
	//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
	//#endif
		CHECKPOINT();
		SessionManager<session>::release(a->toString());
	
		CHECKPOINT();
		dword len=p.serializationSize();
		void *chunkResponse=p.toNetwork();
	//	cout<<"ReadFrame return:"<<len<<" bytes, chunk size:"<<frameChunk->size()<<endl;
		RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, (byte*)chunkResponse, len, _this->type, 0, true);
	
		delete [] (byte*)chunkResponse;
	
		// Perque no es borri dos cops el frame, el posem a NULL a ***p***.
		// setFrame fa que s'esborri, i aixi no s'esborrara de p.chunk i frameChunk
		// evitant un double free.
		// Ho fem a p pq frameChunk te el tractament especial de DiskWriter
		p.chunk=NULL;
	//	frameChunk->setFrame(NULL,0);
	
		CHECKPOINT();
		// Si estava pendent d'escriure, hem de comprovar si s'ha escrit i 
		// si ja ho ha fet, com que haura quedat marcat com no borrable, borrar-lo.
		bool writePending=false;
		if (fileEntries!=NULL)
		{
			RecordingFileFrameChunk *frameChunk=NULL;
			fileEntries->rlock();
			
			list<RecordingModuleDiskWriterEntryGroup*> *groups = 
				fileEntries->getList();
			
			RecordingModuleDiskWriterEntry *entry;
			list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;
			for (gIt=groups->begin(); gIt!=groups->end() && frameChunk==NULL && !writePending; gIt++)
			{
				RecordingModuleDiskWriterEntryGroup *g=*gIt;
	
				entry=g->getEntryForOffset(frameOff);
					
				if (entry!=NULL)
					frameChunk = (RecordingFileFrameChunk*)
						entry->getChunk();
				
				writePending=(entry!=NULL);
			}
			
			if (writePending)
				entry->setDeleteAtDestructor(true);
			
			fileEntries->unlock();
		}
		
		if (!writePending)
			delete frameChunk;
	
	//	ti=t.time();
	//	if(ti.seconds() > 1)
	//	{
	//		cout<<"RMI::readFrame WARNING: readFrame(3) time:"<<ti.seconds()<<" usecs:"<<ti.useconds()<<endl;
	//		cout << " reading:" << ses->lastSentDate->getHumanReadableYear() << "/" << (int)ses->lastSentDate->getHumanReadableMonth() << "/" << (int)ses->lastSentDate->getHumanReadableDay() << "(" << (int)ses->lastSentDate->getHumanReadableHour() << ":" << (int)ses->lastSentDate->getHumanReadableMinute() << ":" << (int)ses->lastSentDate->getHumanReadableSecond() << " )"  << endl;
	//	}
		return pk;
		
	}
	catch (Exception &e)
	{
		if(e.getClass() != string("FileException") || ses->cfn2Error == 1)
		{
			cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + CHECKPOINT_TOSTRING() + string(": ") + e.getClass() + string(": ") + e.getMsg() << endl;
		}

		if (ses!=NULL && ses->endSessionMutex->isHeldByCurrentThread())
		{
			ses->endSessionMutex->unlock();
			try{
				SessionManager<session>::release(a->toString());
			}catch(...){}
		}		
		
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	catch (std::exception &stde)
	{
		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + CHECKPOINT_TOSTRING() + string(": ") + string(stde.what()) << endl;

		if (ses!=NULL && ses->endSessionMutex->isHeldByCurrentThread())
		{
			ses->endSessionMutex->unlock();
			try{
				SessionManager<session>::release(a->toString());
			}catch(...){}
		}		
	}
	catch (...)
	{
		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + CHECKPOINT_TOSTRING() + string(": Unknown Exception") << endl;

		if (ses!=NULL && ses->endSessionMutex->isHeldByCurrentThread())
		{
			ses->endSessionMutex->unlock();
			try{
				SessionManager<session>::release(a->toString());
			}catch(...){}
		}		
	}
	
	return NULL;
}

RPCPacket* RecordingModuleInterface::setTimeout(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	cout << "--> RecordingModuleInterface::setTiemout" << endl;
	RMSetTimeoutParams p(params);
	
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	
	ses->timeOut=p.ms;

	SessionManager<session>::release(a->toString());

	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}

RPCPacket* RecordingModuleInterface::skipFrames(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	cout << "--> RecordingModuleInterface::skipFrames" << endl;
	RMSkipFramesParams p(params);
	
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}

//TODO: diria que tota la crida es innecessaria, si fem les coses per data :P
/*	try
	{
		if (p->nFrames>0)
			ses->rf2->skipBlocks(p->nFrames);
		else if (p->nFrames<0)
			ses->rf2->rewindBlocks(-p->nFrames);
	}
	catch (Exception &e)
	{
		SessionManager<session>::release(a->toString());
		throw e;	
	}
*/	
	SessionManager<session>::release(a->toString());
	
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}

RPCPacket* RecordingModuleInterface::startRecordingStream(RecordingModuleInterface *_this, Address *a, void *params)
{
	return RMIStartRecordingStream(_this, a, params, false);
}

RPCPacket* RecordingModuleInterface::startLocalRecordingStream(RecordingModuleInterface *_this, Address *a, void *params)
{
	cout << "["<<Thread::getId()<<"] ******************************* RecordingModuleInterface::startLocalRecStream **************************"<< endl;
	return RMIStartRecordingStream(_this, a, params, true);
}

RPCPacket* RecordingModuleInterface::RMIStartRecordingStream(RecordingModuleInterface *_this, Address *a, void *params, bool local)
{
	STACKTRACE_INSTRUMENT();
	cout << "["<<Thread::getId()<<"] --> RecordingModuleInterface::startRecStream"<< endl;
	
//	cout << "RecordingModuleInterface::startRecordingStream 1" << endl;

	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}

 	cout << "["<<Thread::getId()<<"] RecordingModuleInterface::startRecordingStream 2, device:"<<ses->sessionParams.devId << endl;

	RecordingModuleInterface::session *s;
	try
	{
		s=getSessionRecordingDevice(ses->sessionParams.devId,
			ses->sessionParams.recType, local);

		if(s != NULL)
		{
			cout<<"["<<Thread::getId()<<"] --> RecordingModuleInterface::startRecordingStream alreadyRec ["<<ses->sessionParams.devId<<"]"<<endl;
			SessionManager<session>::release(a->toString());
			cout<<"["<<Thread::getId()<<"] --> RecordingModuleInterface::startRecordingStream alreadyRec throw"<<endl;
			throw RecordingModuleException(
				string("Stream already being recorded with type ") + 
				StrUtils::decToString(ses->sessionParams.recType));
		}
		cout<<"["<<Thread::getId()<<"] RecordingModuleInterface::startRecordingStream("<<ses->sessionParams.devId<<") noRec"<<endl;
	}
	catch (RedirectCallException &rce)
	{
		SessionManager<session>::release(a->toString());
		
		// Rebem el redirect perque un altre servidor ja esta gravant la camera
		// encara que sigui amb un altre tipus i hem d'iniciar alla!!
		//rce.serialize()->materializeAndThrow(true);
		throw;

		// AIXO NO
//		throw RecordingModuleException(string("Stream already being recorded "
//			"with type ") + StrUtils::decToString(ses->sessionParams.recType) +
//			string(", ") + rce.getMsg());
	}
	
	try
	{
		cThrLock.lock(true, string("startRecStream"));
		cout<<"["<<Thread::getId()<<"] startRecStream("<<ses->sessionParams.devId<<")  cThrLock locked"<<endl;
	}
	catch (Exception &e)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: startRecStream("<<ses->sessionParams.devId<<")  cThrLock lock error, no unlock! "<<
			e.getClass() << ": " << e.getMsg() <<endl;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	catch(std::exception &stde)
	{
		cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"], startRecStream("<<ses->sessionParams.devId<<")  cThrLock lock error, no unlock! " << stde.what() << endl;
		throw(stde);
	}

	try
	{
		try
		{
			cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  getSessionRecDev"<<endl;
			s=getSessionRecordingDevice(ses->sessionParams.devId, local, true);
			cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  getSessionRecDev"<<endl;
		}
		catch (RecordingModuleException &e)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId <<"]: startRecStream getSessionRecordingDevice  "<<
				e.getClass() << ": " << e.getMsg() <<endl;
		//	e.serialize()->materializeAndThrow(true);
			//TODO: s'ha de mirar perqué passa i actuar en consequencia
			throw RecordingModuleException(string("Stream already being recorded with type ") + 
				StrUtils::decToString(ses->sessionParams.recType));
			
		}

		float desiredFPS=((float)ses->sessionParams.fpswhole)+((float)ses->sessionParams.fpsfrac)/1000.0f;
		if (s!=NULL)
		{
			ses->startRecStreamRecv=true;
			// Ja s'esta gravant, adaptem els FPS si cal
			cout << "["<<Thread::getId()<<"] RecordingModuleInterface::startRecordingStream already recording["<<ses->sessionParams.devId<<"], setFrames type:"<<ses->sessionParams.recType << " s:"<<(void*)s<<"  s type:"<<s->sessionParams.recType<< endl;
		cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  getRecThread"<<endl;
			RecordingModuleInterfaceSaveStreamThread *recTh = getRecThread(ses->sessionParams.devId, true);
		cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  getRecThread"<<endl;

			float currRecFPS=recTh->getFPS();

			if (desiredFPS>currRecFPS)
				recTh->setFPS(desiredFPS);

		cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  SessionMng release"<<endl;
			SessionManager<session>::release(s);
		cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  SessionMng release"<<endl;
		}
		else
		{
			//	cout << "RecordingModuleInterface::startRecordingStream 3" << endl;

			cout << "["<<Thread::getId()<<"] RecordingModuleInterface::startRecordingStream not recording, create Thread["<<ses->sessionParams.devId<<"], type:"<<ses->sessionParams.recType << endl;

		cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  camThreads.find"<<endl;
			map<int, RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread*>::iterator it=camThreads.find(ses->sessionParams.devId);
		cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  camThreads.find"<<endl;
			
				RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.lock();
			if (RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount>=RecordingModuleInterface::maxActiveRecordingsCount)
			{
				cout << "["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<"): TUMENI activeRecordingsCount " << RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount << endl;
				
				string excMsg=
					string("Maximum active recordings count reached: ") +
					StrUtils::decToString(RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount);
				
RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.unlock();

				SessionManager<session>::release(a->toString());

				if (!RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::almActiveRaised)
				{
					cout << "["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<"): raise limit alarm"<< endl;
					almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_limit_reached,"", true));//(( StrUtils::decToString(RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount)));
					RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::almActiveRaised=true;
				}
				throw MaxActiveRecordingsReachedException(excMsg);
			}
			else if (RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount>=RecordingModuleInterface::warnActiveRecordingsCount && !RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::warnActiveRaised)
			{
				cout << "["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<"): NOTUMENI activeRecordingsCount " << RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount <<": raise Warning"<< endl;
				almLogThread->queue(RecordingModuleAlarmAndLogThread::event(-1, RecordingModuleAlarmAndLogThread::rec_limit_warning,"", true));//(( StrUtils::decToString(RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount)));
				RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::warnActiveRaised=true;
			}
			else
			{
				cout << "["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<"): NOTUMENI activeRecordingsCount " << RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCount << endl;
			}
			ses->startRecStreamRecv=true;
			RecordingModuleInterface::RecordingModuleInterfaceSaveStreamThread::activeRecordingsCountLock.unlock();

			if(it == camThreads.end())
			{
		cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  new RMISST"<<endl;
				RecordingModuleInterfaceSaveStreamThread *th=
					new RecordingModuleInterfaceSaveStreamThread(desiredFPS, 
							ses->sessionParams.devId, _this, *a);
		cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  new RMISST"<<endl;
		cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  RMISST.start"<<endl;
				int thrId = th->start(NULL, Thread::Thread::OnExceptionExitThread, true); //start, valors per defecte, en debug
				th->detach(thrId);
		cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  RMISST.start"<<endl;
				camThreads[ses->sessionParams.devId]=th;
			}
			else
			{	//no hi ha ningú gravant, ens apropiem del thread :P
				camThreads[ses->sessionParams.devId]->setFPS(desiredFPS);
				cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  RMISST.setSesAddr"<<endl;
				camThreads[ses->sessionParams.devId]->setSessionAddr(*a);
				cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  RMISST.setSesAddr"<<endl;
				camThreads[ses->sessionParams.devId]->clearStop();
				cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  RMISST.runningInst"<<endl;
				if(camThreads[ses->sessionParams.devId]->runningInstances() == 0)
				{
					cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  RMISST.start"<<endl;
					int thrId = camThreads[ses->sessionParams.devId]->start(NULL, Thread::Thread::OnExceptionExitThread, true);
					camThreads[ses->sessionParams.devId]->detach(thrId);
					cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  RMISST.start"<<endl;
				}
				else
				{
					cout<<"["<<Thread::getId()<<"] -- startRecStream("<<ses->sessionParams.devId<<")  runningInstances:"<<camThreads[ses->sessionParams.devId]->runningInstances()<<endl;
					if(!camThreads[ses->sessionParams.devId]->running)
					{
						cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  RMISST.start(!running)"<<endl;
						int thrId = camThreads[ses->sessionParams.devId]->start(NULL, Thread::Thread::OnExceptionExitThread, true);
						camThreads[ses->sessionParams.devId]->detach(thrId);
						cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  RMISST.start(!running)"<<endl;
					}
				}
				cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  RMISST.runningInst"<<endl;
			}
			cout << "["<<Thread::getId()<<"] RecordingModuleInterface::startRecordingStream("<<ses->sessionParams.devId<<")  not recording, created thread Thread unlock"<< endl;
			ses->startedRecording=true;
		}
		// 	cout << "RecordingModuleInterface::startRecordingStream 5" << endl;
		cout<<"["<<Thread::getId()<<"] startRecStream("<<ses->sessionParams.devId<<")  end try"<<endl;

	}
	catch (Exception &e)
	{
		try
		{
			cThrLock.unlock();
		}
		catch (Exception &le)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId<<"]: cThread unlock invalid "<<
				le.getClass() << ": " << le.getMsg() <<endl<<"     in exception:" << 
				e.getClass() << ": " << e.getMsg() << endl;
		}
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	catch(std::exception &stde)
	{
		cout << __FILE__ << " Line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId<<"], cThrLock unlock! startRecordingStream std::exception " << stde.what() << endl;
		try
		{
			cThrLock.unlock();
		}
		catch (Exception &le)
		{
			cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId<<"]: cThread unlock invalid "<<
				le.getClass() << ": " << le.getMsg() <<endl<<"     in exception:" << stde.what() << endl;
		}
		throw(stde);
	}

	try
	{
		cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  cThrLock unlock"<<endl;
		cThrLock.unlock();
		cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  cThrLock unlock"<<endl;
	}
	catch (Exception &le)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ << "[" << ses->sessionParams.devId<<"]: cThread unlock invalid" << 
			le.getClass() << ": " << le.getMsg() << endl;
	}

	ses->toDB=true;
	ses->inDB=false;
	try{
		cout<<"["<<Thread::getId()<<"] --> startRecStream("<<ses->sessionParams.devId<<")  sesToDB"<<endl;
	RecordingModuleInterface::sessionToDB(a, _this->address);
		cout<<"["<<Thread::getId()<<"] <-- startRecStream("<<ses->sessionParams.devId<<")  sesToDB"<<endl;
	}catch(...){}
	SessionManager<session>::release(a->toString());

	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	cout << "["<<Thread::getId()<<"] <-- RecordingModuleInterface::startRecStream("<<ses->sessionParams.devId<<") " << endl;
	return pk;
}


RPCPacket* RecordingModuleInterface::stopRecordingStream(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	cout << "--> RecordingModuleInterface::stopRecStream" << endl;
	
	RecordingModuleInterface::session *ses;
	try
	{
//	cout << "--> RecordingModuleInterface::getSesForAddr" << endl;
		ses=RecordingModuleInterface::getSessionForAddress(a);
//	cout << "--> //RecordingModuleInterface::getSesForAddr" << endl;
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	catch (Exception &e)
	{
		cout << "<-- RecordingModuleInterface::stopRecStream: " << 
			e.getClass() << ": " << e.getMsg() << endl;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
//	RecordingModuleInterfaceSaveStreamThread *th=NULL;
	
	
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->lock()") << endl;
//#endif
	ses->endSessionMutex->lock(true, "stopRecStream");
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->lock() LOCKED") << endl;
//#endif
	ses->sMutex->lock();
	try
	{
		cout << "--> RecordingModuleInterface::stopRecStream search" << endl;
		// Busquem si esta en alguna altra sessio del mateix tipus...
		RecordingModuleInterface::session *stopSes = 
			getSessionRecordingDevice(ses->sessionParams.devId,
				ses->sessionParams.recType, false);
		cout << "--> RecordingModuleInterface::stopRecStream getSesRecording stopSes:"<<(void*)stopSes<<" type s:"<<ses->sessionParams.recType<<" sSes:"<<(stopSes==NULL?-1:stopSes->sessionParams.recType)<< endl;
		
		if(stopSes!=NULL)
		{
			cout << "RecordingModuleInterface::stopRecStream stopSes "<<(void*)stopSes<<endl;
			cout<<" dat:"<<(void*)stopSes->currentDate << endl;
			RecordingModuleInterface::wlockRecIndex();
			try
			{
				stopSes->currentDate->setCurrentDate();
				if (stopSes->recIndexIdx==0xffffffffffffffffll)
				{
					// Pot haver-se invalidat. Busquem un segment que tingui
					// endDate.isValid a false i que sigui de la cam i data q toca,
					// mes o menys.
					bool found=false;

					for (qword s=0; s<recIndex->nSegments; s++)
					{
						const RecordingModuleInterface::recordingSegment *ri=recIndex->getSegment(s);
						RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)ri;

						if (!r.endDate.isValid && r.devId==stopSes->sessionParams.devId
								&& r.recType==stopSes->sessionParams.recType
								&& r.fps.whole==stopSes->sessionParams.fpswhole
								&& r.fps.frac==stopSes->sessionParams.fpsfrac
								&& (r.startDate.secs<stopSes->currentDate->getSecs() ||
									(r.startDate.secs==stopSes->currentDate->getSecs() &&
									 r.startDate.millis<=stopSes->currentDate->getMillis())))
						{
							r.endDate.isValid=true;//false;
							r.endDate.secs=stopSes->currentDate->getSecs();
							r.endDate.millis=stopSes->currentDate->getMillis()+1;
							if (r.endDate.millis>=1000)
							{
								r.endDate.secs+=r.endDate.millis/1000;
								r.endDate.millis%=1000;
							}

							stopSes->recIndexIdx=s;
							recIndex->alterSegment(stopSes->recIndexIdx, r);
						}
					}
				}
				else // if (stopSes->recIndexIdx!=0xffffffffffffffffll)
				{
					const RecordingModuleInterface::recordingSegment *ri=recIndex->getSegment(stopSes->recIndexIdx);
					cout<<" set endDate 2 sec:"<<ri->endDate.secs<<":"<<ri->endDate.millis<<" index:"<<stopSes->recIndexIdx<< endl;
					RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)ri;
					cout<<" set endDate 2 r sec:"<<r.endDate.secs<<":"<<r.endDate.millis<<endl;

					r.endDate.isValid=true;
					r.endDate.secs=stopSes->currentDate->getSecs();
					r.endDate.millis=stopSes->currentDate->getMillis();

					recIndex->alterSegment(stopSes->recIndexIdx, r);
				}
			}
			catch (Exception &e)
			{
				RecordingModuleInterface::unlockRecIndex();
				SessionManager<session>::release(a->toString());
				//e.serialize()->materializeAndThrow(true);
				throw;
			}
			cout << "--> RecordingModuleInterface::stopRecStream stopSes 2" << endl;
			RecordingModuleInterface::unlockRecIndex();

			SessionManager<session>::Iterator sesIt=SessionManager<session>::iterator();

			while (sesIt.hasNext())
			{
				RecordingModuleInterface::session *s=sesIt.next();

				if (s->sessionParams.devId==ses->sessionParams.devId 
						&& s->sessionParams.recType==ses->sessionParams.recType)
				{
					cout << "--> RecordingModuleInterface::stopRecStream stopping ses:"<<(void*)s << endl;
//					s->recordingThread=NULL;
					//				s->startedRecording=false;
					if(s->startRecStreamRecv)
					{
						s->startRecStreamRecv =false;
						s->toDB = true;
						s->inDB = false;
					}
				}

				SessionManager<session>::releaseFromIterator(s);
			}
			SessionManager<RecordingModuleInterface::session>::releaseIterator();


			// Del getSessionRecordingDevice de mes amunt
			if(stopSes!=NULL)
				SessionManager<session>::release(stopSes);

			// i comprovem si cal petar el thread o no...
			// Si esta gravant, sera un altra sessio, pq ja hem posat a NULL
			// la que tocava...
			RecordingModuleInterface::session *otherSes =NULL, *recSes=NULL;// getSessionRecordingDevice(ses->sessionParams.devId, true);//en teoria no hauria d'estar gravant cap altre modul, peró aixó es pel thread local...
			SessionManager<session>::Iterator it=SessionManager<session>::iterator();

			bool fnd=false;
			while (it.hasNext() && (otherSes==NULL || recSes==NULL))
			{
				RecordingModuleInterface::session *s=it.next();
				fnd=false;
				if (s->sessionParams.devId==ses->sessionParams.devId
						&& s->startRecStreamRecv )
				{
					otherSes = s;
					fnd=true;
				}
				if (s->sessionParams.devId==ses->sessionParams.devId
						&& s->startedRecording)
				{
					recSes = s;
					fnd=true;
				}
				if(!fnd)	
					SessionManager<session>::releaseFromIterator(it.getLastId());
			}
			SessionManager<RecordingModuleInterface::session>::releaseIterator();

			if (otherSes==NULL)
			{
				cout << "RecordingModuleInterface::stopRecStream delete thread id:"<< ses->sessionParams.devId << endl;
				//	s->startedRecording=false;

				//cThrLock.wlock();
				//cThrLock.rlock(); //TODO wlock > tal i com està, no fa res
				RecordingModuleInterfaceSaveStreamThread *th = getRecThread(ses->sessionParams.devId);
				cout << "RecordingModuleInterface::stopRecStream delete thread:"<<(void*)th << endl;
				if(th!=NULL)
				{
					th->stopThread();
				}
				//cThrLock.unlock();

				SessionManager<session>::Iterator it=SessionManager<session>::iterator();
				while (it.hasNext())
				{//TODO: aixó es massa a sac
					RecordingModuleInterface::session *s=it.next();
					if (s->sessionParams.devId==ses->sessionParams.devId)
					{
						s->startedRecording = false;
						s->startRecStreamRecv = false;
					}
					SessionManager<session>::releaseFromIterator(it.getLastId());
				}
				SessionManager<RecordingModuleInterface::session>::releaseIterator();

				// 20090910 No fem el release perque ara ja el fa el thread
//				if(recSes!=NULL)
//				{
//					SessionManager<session>::release(recSes);
//				}
				//			SessionManager<session>::release(recSes);
				cout<<"RecordingModuleInterface::stopRecStream deleted"<<endl;
			}
			else
			{
				// Busquem el màxim d'FPS de les sessions que [creuen] graven la camera...
				float maxFPS=1.0f;

				SessionManager<session>::Iterator sesIt=SessionManager<session>::iterator();

				while (sesIt.hasNext())
				{
					RecordingModuleInterface::session *s=sesIt.next();

					if (s->sessionParams.devId==ses->sessionParams.devId && s->startRecStreamRecv)
					{
						float sesFPS=((float)s->sessionParams.fpswhole)+((float)s->sessionParams.fpsfrac)/1000.0f;
						cout<<" search FPS recType:"<<s->sessionParams.recType<<" fps:"<<sesFPS<<endl;


						if (sesFPS>maxFPS)
							maxFPS=sesFPS;
					}
					SessionManager<session>::releaseFromIterator(s);
				}
				SessionManager<RecordingModuleInterface::session>::releaseIterator();

				cout << "RecordingModuleInterface::stopRecStream Setting Maximum FPS to " << maxFPS << endl;
				//cThrLock.wlock();
				//cThrLock.rlock(); //TODO wlock > tal i com està, no fa res
				RecordingModuleInterfaceSaveStreamThread *th = getRecThread(ses->sessionParams.devId);
				if(th!=NULL)
				{
					th->setFPS(maxFPS);
					if(th->runningInstances()==0 || !th->running)
					{
						cout << "RecordingModuleInterface::stopRecStream WARNING:recording thread ot startes, and recording sessions remaining, starting... (running:"<<th->running<<", runningInstances:"<<th->runningInstances()<<")" << endl;
						int id = th->start();
						th->detach(id);
					}
				}
				//cThrLock.unlock();

				// 20090910 No fem el release perque ara ja el fa el thread
//				SessionManager<session>::release(otherSes);

				if(recSes!=NULL && recSes!=otherSes)
					SessionManager<session>::release(recSes);
			}
		}
		else
		{
			throw RecordingModuleException(0, "Stream not being recorded");
		}
	}
	catch (Exception &e)
	{
		// Pot ser una Redirect
		cout << "--> RecordingModuleInterface::stopRecStream "<<e.getClass()<<": "<<e.getMsg() << endl;
		ses->sMutex->unlock();
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
//
//#endif
		ses->endSessionMutex->unlock();
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
//#endif
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	ses->sMutex->unlock();
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" --> endSessionMutex->UNlock()") << endl;
//
//#endif
	ses->endSessionMutex->unlock();
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" <-- endSessionMutex->UNlock()") << endl;
//#endif

/*
	if (stopSes->rf!=NULL)
	{
		cout << "stopRecording: rf cache hit ratio: " << stopSes->rf->getCacheHitRatio() << ", " << stopSes->rf->getCacheHits() <<
		" hits, " << stopSes->rf->getCacheMisses() << " misses" << endl;
		delete stopSes->rf;
		stopSes->rf=NULL;
	}
	if (stopSes->rf2!=NULL)
	{
		cout << "stopRecording: rf2 cache hit ratio: " << stopSes->rf2->getCacheHitRatio() <<  ", " << stopSes->rf2->getCacheHits() <<
		" hits, " << stopSes->rf2->getCacheMisses() << " misses" << endl;
		delete stopSes->rf2;
		stopSes->rf2=NULL;
	}
	stopSes->currentFilename=string("");
	stopSes->currentFilename2=string("");
*/
	// Per si stopSes es ses :P
//	sessions[a->toString()]=ses;

	try
	{
		ses->toDB=true;
		ses->inDB=false;
		RecordingModuleInterface::sessionToDB(a, _this->address);
	}
	catch (...)
	{
	}
	
	SessionManager<session>::release(a->toString());

	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);

//	cout << "--> RecordingModuleInterface 4 end" << endl;
	return pk;
}

RPCPacket* RecordingModuleInterface::listRecordings(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	INIT_CHECKPOINT();
	
	cout << "--> RecordingModuleInterface::listRecordings" << endl;
//	cout << "--> RecordingModuleInterface::listRecordings():" <<(void*)params<< endl;
	
	string pathOnly=_this->baseDir;
	RecordingModuleInterface::createPath(pathOnly);

	RMListRecordingsParams p(params);
	
//	cout << "--> RecordingModuleInterface::listRecordings()" << endl;
//	cout << StrUtils::hexDump(string((char*)params, p.size())) << endl;

//	NO CAL SESSIO -> DEMANEM NOMES UN LLISTAT!!
	
//	RecordingModuleInterface::session ses=RecordingModuleInterface::getSessionForAddress(a);

	CHECKPOINT();
	
	qword nRecsFilter=p.nRecsFilter;
	double start=0.0, end=((double)0xffffffffffffffffll);
	double minDate=end, maxDate=start;
	
	if (p.absStartDate.isValid)
		start=((double)p.absStartDate.secs)+((double)p.absStartDate.millis)/1000.0;

	if (p.absEndDate.isValid)
		end=((double)p.absEndDate.secs)+((double)p.absEndDate.millis)/1000.0;

	qword *recIdsFilter=NULL;
	dword *devIdsFilter=NULL;
	RecordingFileHeaderChunk::recType *recTypeFilter=NULL;

//	cout << "\t1 RecordingModuleInterface::listRecordings()" << endl;

	if (nRecsFilter>0)
	{
		recIdsFilter=new qword[nRecsFilter];
		devIdsFilter=new dword[nRecsFilter];
		recTypeFilter=new RecordingFileHeaderChunk::recType[nRecsFilter];
		
		if (devIdsFilter==NULL || recTypeFilter==NULL)
		{
			if (recIdsFilter!=NULL)
				delete [] recIdsFilter;

			if (devIdsFilter!=NULL)
				delete [] devIdsFilter;
				
			if (recTypeFilter!=NULL)
				delete [] recTypeFilter;
				
			throw RecordingModuleException(0, "Not enough memory to filter by devices/recording types");
		}
		
		for (dword i=0; i<nRecsFilter; i++)
		{
			recIdsFilter[i]=p.recordings[i].id;
			devIdsFilter[i]=p.recordings[i].devId;
			recTypeFilter[i]=p.recordings[i].recType;
		}
	}

//	cout << "\t2 RecordingModuleInterface::listRecordings()" << endl;

	list <RMListRecordingsParams::recording> lrec;
	
	double absStart=0.0, absEnd=((double)0xffffffffffffffffll);

	CHECKPOINT();
	
	try
	{	
		recIndex=RecordingModuleInterface::getRecIndex();
	}
	catch (Exception &e)
	{
		if (recIdsFilter!=NULL)
			delete [] recIdsFilter;

		if (devIdsFilter!=NULL)
			delete [] devIdsFilter;
			
		if (recTypeFilter!=NULL)
			delete [] recTypeFilter;

		//e.serialize()->materializeAndThrow(true);
		throw;
	}

	CHECKPOINT();
	
	RecordingModuleInterface::rlockRecIndex();

//	cout << "\t3 RecordingModuleInterface::listRecordings():"<<recIndex->nSegments <<" nRecsFlt:"<<nRecsFilter<< endl;
	for (qword i=0; i<recIndex->nSegments; i++)
	{
		const recordingSegment* seg=NULL;
		CHECKPOINT();
	
		try
		{
			seg=recIndex->getSegment(i);
		}
		catch (Exception &e)
		{
			try
			{
				cout << __FILE__ << " line " << std::dec << __LINE__ << "" << CHECKPOINT_TOSTRING() << ", " << e.getClass() << ": " << e.getMsg() << endl;
			}catch(...){}
			RecordingModuleInterface::unlockRecIndex();
//			RecordingModuleInterface::releaseRecIndex();
			
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
		catch (std::exception &stde)
		{
			try
			{
				cout << __FILE__ << " line " << std::dec << __LINE__ << "" << CHECKPOINT_TOSTRING() << ", " << stde.what() << endl;
			}catch(...){}
			
			RecordingModuleInterface::unlockRecIndex();
//			RecordingModuleInterface::releaseRecIndex();
			
			throw RecordingModuleException(string(stde.what()));
		}
		catch (...)
		{
			try
			{
				cout << __FILE__ << " line " << std::dec << __LINE__ << ", from " << CHECKPOINT_FILE() << ",  recIndex unlock! line " << CHECKPOINT_LINE() << ", unknown exception" << endl;
			}catch(...){}
			
			RecordingModuleInterface::unlockRecIndex();
//			RecordingModuleInterface::releaseRecIndex();
			
			throw RecordingModuleException("Unknown exception");
		}

		try
		{	

			if (nRecsFilter>0)
			{
				bool found=false;
				for (dword idf=0; idf<nRecsFilter; idf++)
				{
					if (((devIdsFilter[idf]!=0xffffffff && seg->devId==devIdsFilter[idf]) || devIdsFilter[idf]==0xffffffff) &&
							((recTypeFilter[idf]<RecordingFileHeaderChunk::REC_HOWMANY && seg->recType==recTypeFilter[idf]) || recTypeFilter[idf]>=RecordingFileHeaderChunk::REC_HOWMANY) &&
							((recIdsFilter[idf]!=0xffffffffffffffffll && i==recIdsFilter[idf]) || recIdsFilter[idf]==0xffffffffffffffffll))
					{
						found=true;
						break;
					}
				}

				if (!found) continue;
			}

			double segStart=((double)seg->startDate.secs)+((double)seg->startDate.millis)/1000.0;
			double segEnd=((double)seg->endDate.secs)+((double)seg->endDate.millis)/1000.0;

			if (segStart>end || segEnd<start) continue;

			if (segStart<minDate) minDate=segStart;
			if (segEnd>maxDate) maxDate=segEnd;

			RMListRecordingsParams::recording recSeg;

			recSeg.rmAddress=_this->address;
			recSeg.id=i;
			recSeg.devId=seg->devId;
			recSeg.recType=seg->recType;
			recSeg.fps.whole=seg->fps.whole;
			recSeg.fps.frac=seg->fps.frac;
			recSeg.startDate=seg->startDate;
			recSeg.endDate=seg->endDate;

			CHECKPOINT();

			lrec.push_back(recSeg);
		}
		catch (std::exception &stde)
		{
			try
			{
				cout << __FILE__ << " line " << std::dec << __LINE__ << "" << CHECKPOINT_TOSTRING() << ", " << stde.what() << endl;
			}catch(...){}
			
			RecordingModuleInterface::unlockRecIndex();
//			RecordingModuleInterface::releaseRecIndex();
			
			throw RecordingModuleException(string(stde.what()));
		}
		catch (...)
		{
			try
			{
				cout << __FILE__ << " line " << std::dec << __LINE__ << "" << CHECKPOINT_TOSTRING() << ", unknown exception" << endl;
			}catch(...){}
			
			RecordingModuleInterface::unlockRecIndex();
//			RecordingModuleInterface::releaseRecIndex();
			
			throw RecordingModuleException("Unknown exception");
		}
	}

	RecordingModuleInterface::unlockRecIndex();
//	RecordingModuleInterface::releaseRecIndex();

	CHECKPOINT();
	
	p.nRecsFilter=0;
	p.nRecordings=lrec.size();

	p.absStartDate.isValid=true;
	p.absStartDate.secs=(qword)floor(minDate);
	p.absStartDate.millis=(word)((minDate-floor(minDate))*1000.0);

	p.absEndDate.isValid=true;
	p.absEndDate.secs=(qword)floor(maxDate);
	p.absEndDate.millis=(word)((maxDate-floor(maxDate))*1000.0);

	if (p.recordings!=NULL)
		delete [] p.recordings;
	
	p.recordings=new RMListRecordingsParams::recording[p.nRecordings];
	qword nR=0;
	
//	cout << "\t5 RecordingModuleInterface::listRecordings()" << endl;
	for (list<RMListRecordingsParams::recording>::iterator it=lrec.begin(); it!=lrec.end(); it++, nR++)
	{
		p.recordings[nR]=(*it);
//		cout << "\tRMRList recording: " << endl << "\t" << p.recordings[nR].devId << ", " << p.recordings[nR].recType << ", " << p.recordings[nR].fps.whole << "." << p.recordings[nR].fps.frac << ", " << p.recordings[nR].startDate.secs << " - " << p.recordings[nR].endDate.secs << endl;
		
	}
//	cout << "\t6 RecordingModuleInterface::listRecordings()" << endl;
	
	lrec.empty();
	
//	sessions[a->toString()]=ses;

//	cout << "\t7 RecordingModuleInterface::listRecordings() response: " << endl;
	void *chunkResponse=p.toNetwork();

//	cout << "\tRMRList: " << StrUtils::hexDump(string((char*)chunkResponse, p.size())) << endl;

	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, (byte*)chunkResponse, p.serializationSize(), _this->type, 0, true);
	
	delete [] (byte*)chunkResponse;


	if (recIdsFilter!=NULL)
		delete [] recIdsFilter;

	if (devIdsFilter!=NULL)
		delete [] devIdsFilter;

	if (recTypeFilter!=NULL)
		delete [] recTypeFilter;


	cout << "<-- RecordingModuleInterface::listRecordings" << endl;
	return pk;
}

RPCPacket* RecordingModuleInterface::isRecording(RecordingModuleInterface *_this, Address *a, void *params)
{
	cout << "--> RecordingModuleInterface::isRecording" << endl;
	STACKTRACE_INSTRUMENT();
	RMIsRecordingParams p(params);
	
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}

	p.isRecording=false;
	p.isOwnSession=false;

	RecordingModuleInterface::session *s=NULL;
	try
	{
		s=getSessionRecordingDevice(ses->sessionParams.devId);

		if (s!=NULL)
		{
			if (ses->startedRecording)
			{
				p.isOwnSession=true;
			}
			p.isRecording=true;
			p.fpswhole=s->sessionParams.fpswhole;
			p.fpsfrac=s->sessionParams.fpsfrac;

			SessionManager<session>::release(s);
		}
	}
	catch (RecordingModuleException &e)
	{
		//TODO: s'ha de mirar perqué passa i actuar en consequencia
		//de moment, noRec
	}
	/*catch(RedirectCallException &e) // que li demani un altre cop al que toca, per si els updates a la BD no estàn be...
	{
		p.isRecording=true;
		p.fpswhole=0;
		p.fpsfrac=0;
	}
	*/

	SessionManager<session>::release(a->toString());

	byte *res=(byte*)p.toNetwork();
	
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, res, p.serializationSize(), _this->type, 0, true);

	delete [] res;
	
	return pk;
}

RPCPacket* RecordingModuleInterface::getKiBLimit(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	RMGetKiBLimitParams p;
	
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	
	p.KiB=ses->KiBLimit;

	SessionManager<session>::release(a->toString());

	byte *res=(byte*)p.toNetwork();
	
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, res, p.serializationSize(), _this->type, 0, true);

	delete [] res;
	
	return pk;
}

RPCPacket* RecordingModuleInterface::setKiBLimit(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	RMSetKiBLimitParams p(params);
	
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	
	ses->KiBLimit=p.KiB;
	
	SessionManager<session>::release(a->toString());

	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}

RPCPacket* RecordingModuleInterface::getFrameLimit(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	RMGetFrameLimitParams p;
	
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	
	p.nFrames=ses->frameLimit;

	SessionManager<session>::release(a->toString());

	byte *res=(byte*)p.toNetwork();
	
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, res, p.serializationSize(), _this->type, 0, true);

	delete [] res;

	return pk;
}

RPCPacket* RecordingModuleInterface::setFrameLimit(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	RMSetFrameLimitParams p(params);
	
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	ses->frameLimit=p.nFrames;

	SessionManager<session>::release(a->toString());

	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}

RPCPacket* RecordingModuleInterface::getCyclic(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	RMGetCyclicParams p;	

	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	
	p.isCyclic=ses->sessionParams.isCyclic;

	SessionManager<session>::release(a->toString());

	byte *res=(byte*)p.toNetwork();
	
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, res, p.serializationSize(), _this->type, 0, true);

	delete [] res;

	return pk;
}

RPCPacket* RecordingModuleInterface::setCyclic(RecordingModuleInterface *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	RMSetCyclicParams p(params);
	
	RecordingModuleInterface::session *ses;
	try
	{
		ses=RecordingModuleInterface::getSessionForAddress(a);
	}
	catch (RedirectCallException &rce)
	{
		cout << rce.getClass() << ": " << rce.getMsg() << endl;
		return NULL;
	}
	
	ses->sessionParams.isCyclic=p.isCyclic;

	SessionManager<session>::release(a->toString());

	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}
