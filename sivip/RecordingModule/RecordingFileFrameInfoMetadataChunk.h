/*
 *  RecordingFileFrameInfoMetadataChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 26/05/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */


#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>

class RecordingFileFrameInfoMetadataChunk : public RecordingFileMetadataChunk
{
	static const dword chunkSizeCheck=sizeof(dword)*2+sizeof(byte)*2+sizeof(int)*2;
	
	dword fourcc;			// El fourcc del codec :P
	byte codecQuality;		// 0-100
	dword codecBitrate;		// Bits/s
	int x, y;
	byte bpp;
	
public:
	RecordingFileFrameInfoMetadataChunk();
	virtual ~RecordingFileFrameInfoMetadataChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	void setFourCC(dword fcc);
	dword getFourCC();
	
	void setCodecQuality(byte q);
	byte getCodecQuality();
	
	void setCodecBitrate(dword br);
	dword getCodecBitrate();
	
	void setX(int x);
	int getX();
	
	void setY(int y);
	int getY();

	void setBPP(byte bpp);
	byte getBPP();
};

