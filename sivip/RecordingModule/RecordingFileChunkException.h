/*
 *  RecordingFileChunkException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class RecordingFileChunkException : public Exception
{
 public:
	RecordingFileChunkException(string msg, int code=0);
	RecordingFileChunkException(int code, string msg);
	RecordingFileChunkException(SerializedException &se);

	~RecordingFileChunkException();
	
	virtual string getClass();
};

