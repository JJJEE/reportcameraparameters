/*
 *  RecordingFileFrameChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 19/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileDateMetadataChunk.h>

class RecordingFileFrameChunk : public RecordingFileChunk
{
protected:
	dword frameSize;
	void *frameData;
	
public:
	RecordingFileFrameChunk();
	virtual ~RecordingFileFrameChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	virtual void setFrame(void *frame, dword len);
	virtual const void* getFrame();
	virtual dword getFrameSize();
};

