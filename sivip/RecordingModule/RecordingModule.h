/*
 *  RecordingModule.h
 *  
 *
 *  Created by David Marí Larrosa on 27/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_BASE_RECORDINGMODULE_RECORDINGMODULE_H_
#define SIRIUS_BASE_RECORDINGMODULE_RECORDINGMODULE_H_

#include <Utils/debugNew.h>
#include <Module/Module.h>

class RecordingModule : public Module
{
	string baseDir;
	qword storeSizeLimit;
	string configFile;

public:
	RecordingModule(Address rmAddr, short rmType, string baseDir, qword kibLimit=0, Canis *cn=NULL);
	RecordingModule(string xmlFile);

	void fixateIndex();
	void clearDatabase();
	void startService();
	
	virtual void statsOutput();
};

#endif
