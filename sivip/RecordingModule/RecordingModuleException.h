/*
 *  RecordingModuleException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class RecordingModuleException : public Exception
{
 public:
	RecordingModuleException(string msg, int code=0);
	RecordingModuleException(int code, string msg);
	RecordingModuleException(SerializedException &se);

	~RecordingModuleException();
	
	virtual string getClass();
};

