/*
 *  RecordingFileDateMetadataChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileDateMetadataChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#ifndef WIN32
#include <sys/time.h>
#else
#include <Utils/WindowsDefs.h>
#endif
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/errno.h>

using namespace std;

RecordingFileDateMetadataChunk::RecordingFileDateMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileDateMetadataChunk::newChunk;
	className=getClass();
	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// La resta autoexplicatius (o no, pero poc important si ho son o no)
	chunkSize=sizeof(dword)+sizeof(byte)+className.length()+sizeof(nSubChunks)+chunkSizeCheck;

	setCurrentDate();
}

RecordingFileDateMetadataChunk::~RecordingFileDateMetadataChunk()
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileChunk* RecordingFileDateMetadataChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);

	// cout << "RecordingFileDateMetadataChunk::newChunk 1" << endl;
	
	RecordingFileDateMetadataChunk *resChunk=new RecordingFileDateMetadataChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

//	if (chunkClass!=resChunk->getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	// cout << "RecordingFileDateMetadataChunk::newChunk 1" << endl;
	resChunk->chunkSize=size;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	chunkClass.clear();
	
	chunkBytes+=chunkTypeLen;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (resChunk->nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
		
	// cout << "RecordingFileDateMetadataChunk::newChunk 1" << endl;

	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (dataSize!=chunkSizeCheck)
		throw RecordingFileChunkException(0, resChunk->getClass()+string(": Invalid data size (")+
											StrUtils::decToString(dataSize)+string("/")+
											StrUtils::decToString(chunkSizeCheck)+string(")"));
	
	// cout << "RecordingFileDateMetadataChunk::newChunk 1" << endl;

	resChunk->secs=*((qword*)chunkBytes);
	Endian::from(endian, &resChunk->secs, sizeof(qword));
	chunkBytes+=sizeof(qword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	// cout << "RecordingFileDateMetadataChunk::newChunk 1" << endl;

	resChunk->millis=*((word*)chunkBytes);
	Endian::from(endian, &resChunk->millis, sizeof(word));
	chunkBytes+=sizeof(word);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	// cout << "RecordingFileDateMetadataChunk::newChunk 1 - return" << endl;

	localtime_r((const time_t*)&resChunk->secs, &resChunk->t);

	return (RecordingFileChunk*)resChunk;
}

void* RecordingFileDateMetadataChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	byte *data=new byte[chunkSize];
	byte *chunkBytes=data;
	
	if (data==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
		
	byte chunkTypeLen=getClass().length();
	*chunkBytes=chunkTypeLen;
	chunkBytes++;
	
	memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
	chunkBytes+=chunkTypeLen;
	
	if (nSubChunks!=0)
		throw RecordingFileChunkException(0, "Basic chunk should have 0 subchunks");
	*((dword*)chunkBytes)=nSubChunks;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	*((dword*)chunkBytes)=chunkSizeCheck;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((qword*)chunkBytes)=secs;
	Endian::to(dstEndian, chunkBytes, sizeof(qword));
	chunkBytes+=sizeof(qword);
	
	*((word*)chunkBytes)=millis;
	Endian::to(dstEndian, chunkBytes, sizeof(word));

	return data;
}

string RecordingFileDateMetadataChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileDateMetadataChunk";
	return c;
}

void RecordingFileDateMetadataChunk::setDate(qword secs, word millis)
{
	STACKTRACE_INSTRUMENT();
	this->secs=secs;
	this->millis=millis;
	localtime_r((const time_t*)&this->secs, &t);
}

void RecordingFileDateMetadataChunk::setDate(dword year, dword month, dword day, dword hour, dword minute, dword second, word millis)
{
	STACKTRACE_INSTRUMENT();
	
	struct tm tm;
	
	tm.tm_sec=second;
	tm.tm_min=minute;
	tm.tm_hour=hour;
	tm.tm_mday=day;
	tm.tm_mon=month-1; 
	tm.tm_year=year-1900;
	tm.tm_isdst=-1;
	
	this->secs=(qword)mktime(&tm);	
	this->millis=(word)millis;

	localtime_r((const time_t*)&this->secs, &t);
}

void RecordingFileDateMetadataChunk::setCurrentDate()
{
	STACKTRACE_INSTRUMENT();
	struct timeval date;
	int res=gettimeofday(&date, NULL);
	
	if (res<0)
	{
		cout << "WARNING: RecordingFileDateMetadataChunk::setCurrentDate(): "
			"gettimeofday failed with " << errno << endl;
	}
	
	secs=date.tv_sec;
	millis=date.tv_usec/1000;
	localtime_r((const time_t*)&this->secs, &t);
}

qword RecordingFileDateMetadataChunk::getSecs()
{
	STACKTRACE_INSTRUMENT();
	return secs;
}

word RecordingFileDateMetadataChunk::getMillis()
{
	STACKTRACE_INSTRUMENT();
	return millis;
}

double RecordingFileDateMetadataChunk::microtime()
{
	STACKTRACE_INSTRUMENT();
	return ((double)secs)+((double)millis)/1000.0;
}

dword RecordingFileDateMetadataChunk::getHumanReadableYear()
{
	STACKTRACE_INSTRUMENT();
	return t.tm_year+1900;
}

byte RecordingFileDateMetadataChunk::getHumanReadableMonth()
{
	STACKTRACE_INSTRUMENT();
	return t.tm_mon+1;
}

byte RecordingFileDateMetadataChunk::getHumanReadableDay()
{
	STACKTRACE_INSTRUMENT();
	return t.tm_mday;
}

byte RecordingFileDateMetadataChunk::getHumanReadableHour()
{
	STACKTRACE_INSTRUMENT();
	return t.tm_hour;
}

byte RecordingFileDateMetadataChunk::getHumanReadableMinute()
{
	STACKTRACE_INSTRUMENT();
	return t.tm_min;
}

byte RecordingFileDateMetadataChunk::getHumanReadableSecond()
{
	STACKTRACE_INSTRUMENT();
	return t.tm_sec;
}

word RecordingFileDateMetadataChunk::getHumanReadableMillisecond()
{
	STACKTRACE_INSTRUMENT();
	return millis;
}

bool RecordingFileDateMetadataChunk::isDST()
{
	STACKTRACE_INSTRUMENT();
	return (this->t.tm_isdst!=0);
}

bool operator < (RecordingFileDateMetadataChunk &a,
	RecordingFileDateMetadataChunk &b)
{
	if(a.getSecs()<b.getSecs()
		|| (a.getSecs()==b.getSecs() && a.getMillis()<b.getMillis()))
		return true;
	return false;
}

bool operator > (RecordingFileDateMetadataChunk &a,
	RecordingFileDateMetadataChunk &b)
{
	if(a.getSecs()>b.getSecs()
		|| (a.getSecs()==b.getSecs() && a.getMillis()>b.getMillis()))
		return true;
	return false;
}
