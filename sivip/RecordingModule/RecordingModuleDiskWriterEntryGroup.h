#ifndef __SIRIUS__RECORDINGMODULE__RECORDINGMODULEDISKWRITERENTRYGROUP_H
#define __SIRIUS__RECORDINGMODULE__RECORDINGMODULEDISKWRITERENTRYGROUP_H

#include <RecordingModule/RecordingModuleDiskWriterEntry.h>

#include <map>
#include <list>

using namespace std;

class RecordingModuleDiskWriterEntryGroup
{
public:
	static const int entryReadyLimitCount = 25;
	static const int maxGroupLength = 2*1024*1024;	// 2 MBytes
	
protected:
	// Offset dins el de l'inici del grup
	fileOffset offset;
	// Tamany de la informacio del grup (suma de les diferents Entries)
	dword size;
	// llistat d'entries
	list<RecordingModuleDiskWriterEntry*> entries;

public:
	RecordingModuleDiskWriterEntryGroup();
	virtual ~RecordingModuleDiskWriterEntryGroup();

	bool addEntry(RecordingModuleDiskWriterEntry *entry);
	bool joinGroup(RecordingModuleDiskWriterEntryGroup *group);

	fileOffset getOffset();
	void *getData();
	dword getDataLength();
	RecordingFileChunk *getChunkForOffset(fileOffset off);
	RecordingModuleDiskWriterEntry *getEntryForOffset(fileOffset off);
	dword getEntryCount();
	
	bool isDelayed();
	bool isReady();
};

#endif
