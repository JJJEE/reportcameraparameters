#include <RecordingModule/RecordingModuleDiskWriterEntryGrouper.h>

#pragma mark ***
#pragma mark *** RecordingModuleDiskWriterEntryGrouper
#pragma mark ***

#pragma mark *** Constructores
RecordingModuleDiskWriterEntryGrouper::RecordingModuleDiskWriterEntryGrouper()
{
	STACKTRACE_INSTRUMENT();
}

RecordingModuleDiskWriterEntryGrouper::~RecordingModuleDiskWriterEntryGrouper()
{
	STACKTRACE_INSTRUMENT();
	this->wlock();
	list<RecordingModuleDiskWriterEntryGroup*>::iterator
		gIt=this->groups.begin();
	while (gIt!=this->groups.end())
	{
		RecordingModuleDiskWriterEntryGroup *grp=*gIt;
		if (grp!=NULL)
			delete grp;
		gIt=this->groups.erase(gIt);
	}
	this->unlock();
}

float RecordingModuleDiskWriterEntryGrouper::addEntry(
	RecordingModuleDiskWriterEntry *entry)
{
	STACKTRACE_INSTRUMENT();
	this->wlock();
	list<RecordingModuleDiskWriterEntryGroup*>::iterator gIt;

	RecordingModuleDiskWriterEntryGroup *grp=NULL;
	for (gIt=this->groups.begin(); gIt!=this->groups.end(); gIt++)
	{
		grp=*gIt;
		
		if (grp->addEntry(entry))
			break;

		grp=NULL;
	}
	
	if (grp!=NULL)
	{
		for (gIt=this->groups.begin(); gIt!=this->groups.end(); gIt++)
		{
			RecordingModuleDiskWriterEntryGroup *gr=*gIt;
			if (grp!=gr && grp->joinGroup(gr))
			{
				// Ens es igual l'iterador resultat, perque acabem aqui.
				this->groups.erase(gIt);
				delete gr;
				this->unlock();
				return this->calcFullFactor();
			}
		}
		this->unlock();
		return this->calcFullFactor();
	}
			
	// Si arribem aqui es que no s'ha afegit :P
	grp = new RecordingModuleDiskWriterEntryGroup();
	grp->addEntry(entry);
	
	this->groups.push_back(grp);

	this->unlock();
	
	return this->calcFullFactor();
}

list<RecordingModuleDiskWriterEntryGroup*>*
	RecordingModuleDiskWriterEntryGrouper::getList()
{
	STACKTRACE_INSTRUMENT();
	return &this->groups;
}

list<RecordingModuleDiskWriterEntryGroup*>
	RecordingModuleDiskWriterEntryGrouper::getReadyGroups()
{
	STACKTRACE_INSTRUMENT();
	list<RecordingModuleDiskWriterEntryGroup*> readyGroups;
	
	this->wlock();
	list<RecordingModuleDiskWriterEntryGroup*>::iterator
		gIt=this->groups.begin();
	while (gIt!=this->groups.end())
	{
		RecordingModuleDiskWriterEntryGroup *grp=*gIt;
		
		if (grp->isReady())
		{
			readyGroups.push_back(grp);
			gIt=this->groups.erase(gIt);
		}
		else
			gIt++;
	}	
	this->unlock();
	
	return readyGroups;
}

list<RecordingModuleDiskWriterEntryGroup*>
	RecordingModuleDiskWriterEntryGrouper::getAllGroups()
{
	STACKTRACE_INSTRUMENT();
	list<RecordingModuleDiskWriterEntryGroup*> allGroups;
	
	this->wlock();
	list<RecordingModuleDiskWriterEntryGroup*>::iterator
		gIt=this->groups.begin();
	while (gIt!=this->groups.end())
	{
		RecordingModuleDiskWriterEntryGroup *grp=*gIt;
		
		allGroups.push_back(grp);
		gIt=this->groups.erase(gIt);
	}	
	this->unlock();
	
	return allGroups;
}

void RecordingModuleDiskWriterEntryGrouper::wlock()
{
	STACKTRACE_INSTRUMENT();
	this->groupsRW.wlock();
}

void RecordingModuleDiskWriterEntryGrouper::rlock()
{
	STACKTRACE_INSTRUMENT();
	this->groupsRW.rlock();
}

void RecordingModuleDiskWriterEntryGrouper::unlock()
{
	STACKTRACE_INSTRUMENT();
	this->groupsRW.unlock();
}

bool RecordingModuleDiskWriterEntryGrouper::empty()
{
	STACKTRACE_INSTRUMENT();
	this->rlock();
	bool empty=this->groups.empty();
	this->unlock();
	return empty;
}

dword RecordingModuleDiskWriterEntryGrouper::size()
{
	STACKTRACE_INSTRUMENT();
	this->rlock();
	dword size=this->groups.size();
	this->unlock();
	return size;
}

float RecordingModuleDiskWriterEntryGrouper::calcFullFactor()
{
	dword totalBytes=0;
	
	this->rlock();
	list<RecordingModuleDiskWriterEntryGroup*>::iterator
		gIt=this->groups.begin();
	while (gIt!=this->groups.end())
	{
		RecordingModuleDiskWriterEntryGroup *grp=*gIt;
		
		totalBytes+=grp->getDataLength();
		
		gIt++;
	}	
	this->unlock();
	
	return (float)totalBytes/
		(float)RecordingModuleDiskWriterEntryGrouper::maxTotalLength;
}
