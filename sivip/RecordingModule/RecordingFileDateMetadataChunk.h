/*
 *  RecordingFileDateMetadataChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>

class RecordingFileDateMetadataChunk : public RecordingFileMetadataChunk
{
	static const dword chunkSizeCheck=sizeof(qword)+sizeof(word);
	
	qword secs;
	word millis;
	struct tm t;
	
public:
	RecordingFileDateMetadataChunk();
	virtual ~RecordingFileDateMetadataChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	virtual void setDate(qword secs, word millis);
	virtual void setDate(dword year, dword month, dword day, dword hour=0, dword minute=0, dword second=0, word millis=0);

	virtual void setCurrentDate();
	virtual qword getSecs();
	virtual word getMillis();
	virtual double microtime();

	// Gets en plan human-readable
	virtual dword getHumanReadableYear();
	virtual byte getHumanReadableMonth();
	virtual byte getHumanReadableDay();
	virtual byte getHumanReadableHour();
	virtual byte getHumanReadableMinute();
	virtual byte getHumanReadableSecond();
	virtual word getHumanReadableMillisecond();
	
	// Informació de DST
	virtual bool isDST();
};

extern bool operator < (RecordingFileDateMetadataChunk &a,
	RecordingFileDateMetadataChunk &b);
extern bool operator > (RecordingFileDateMetadataChunk &a,
	RecordingFileDateMetadataChunk &b);
