/*
 *  MaxActiveRecordingsReachedException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <RecordingModule/RecordingModuleException.h>

using namespace std;

class MaxActiveRecordingsReachedException : public Exception
{
 public:
	MaxActiveRecordingsReachedException(string msg, int code=0);
	MaxActiveRecordingsReachedException(int code, string msg);
	MaxActiveRecordingsReachedException(SerializedException &se);

	~MaxActiveRecordingsReachedException();
	
	virtual string getClass();
};

