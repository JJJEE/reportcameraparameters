/*
 *  RecordingModule.cpp
 *  
 *
 *  Created by David Marí Larrosa on 27/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingModule.h>
#include <RecordingModule/RecordingModuleException.h>
#include <Utils/Exception.h>
#include <XML/xmlNode.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Canis.h>
#include <RecordingModule/RecordingModuleInterface.h>
#include <string>
#include <cctype>
#include <algorithm>
#include <iostream>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

// Servei de modul de gravacio que fa servir l'interface
RecordingModule::RecordingModule(Address rmAddr, short rmType, string baseDir, qword kibLimit, Canis *cn) : Module(rmAddr, rmType, cn), baseDir(baseDir), storeSizeLimit(kibLimit), configFile("")
{
	STACKTRACE_INSTRUMENT();
//	cout << "serviceSocket: " << this->centralDirectoryAddr.toString() << endl;

	this->moduleInterface=(ModuleInterface*)new RecordingModuleInterface(this->address, this->type, baseDir, storeSizeLimit, this->cn, configFile);
	
}

// Servei de modul de gravacio que fa servir l'interface, inicialitzat mitjançant un XML
RecordingModule::RecordingModule(string xmlFile) : Module(xmlFile), baseDir(""), storeSizeLimit(0xffffffffffffffffll), configFile(xmlFile)
{
	STACKTRACE_INSTRUMENT();

//	FILE *f=fopen(xmlFile.c_str(),"rb");
//
//	fseek(f,0,SEEK_END);
//	int len=ftell(f);
//	fseek(f,0,SEEK_SET);
//	
//	char *buf=new char[len];
//	
//	if (buf==NULL)
//	{
//		throw (Exception(0, "Not enough memory to read configuration file"));
//	}
//		
//	fread(buf, len, 1, f);
//	fclose(f);
//	
//	string xmlConts(buf, len);
//	delete [] buf;
//	XML *config=xmlParser::parse(xmlConts);
	
	xmlNode *n;
	
	n = config->getNode("/[0]/baseDir");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file"));
	}
	
	baseDir=n->getCdata();

	storeSizeLimit=0;
	n = config->getNode("/[0]/Limits/Size");
	if(n!=NULL)
	{
		string strSize=n->getCdata();
		transform(strSize.begin(), strSize.end(), strSize.begin(), ::tolower);
		double storeLimit=atof(strSize.c_str());
		cout << "StoreLimit read: " << storeLimit << endl;
		string suffix=strSize.substr(strSize.length()-1, 1);
		double mult=1.0;
		
		if (suffix==string("k"))
			mult=1024.0;
		else if (suffix==string("m"))
			mult=1024.0*1024.0;
		else if (suffix==string("g"))
			mult=1024.0*1024.0*1024.0;
		else if (suffix==string("t"))
			mult=1024.0*1024.0*1024.0*1024.0;
		else if (suffix==string("p"))
			mult=1024.0*1024.0*1024.0*1024.0*1024.0;
	
		storeLimit*=mult;
		cout << "StoreLimit read: " << storeLimit << endl;
		storeSizeLimit=(qword)storeLimit;
		cout << "StoreLimit read: " << storeSizeLimit << endl;
	}

	// En KiB q es el que agafa l'interface
	storeSizeLimit/=1024;

	// 20100310: es la de this que ens ve de Module
//	delete config;
}

void RecordingModule::fixateIndex()
{
	STACKTRACE_INSTRUMENT();
	RecordingModuleInterface::recordingIndex *ri=NULL;
	try
	{
		ri=new RecordingModuleInterface::recordingIndex(baseDir+string("/recIndex.sri"));

		if (ri==NULL)
			throw RecordingModuleException(0, "Not enough memory to fixate recording index");

		for (dword s=0; s<ri->nSegments; s++)
		{
			const RecordingModuleInterface::recordingSegment *rs=ri->getSegment(s);
			RecordingModuleInterface::recordingSegment r=*(RecordingModuleInterface::recordingSegment *)rs;
			r.endDate.isValid=true;

			ri->alterSegment(s, r);	
		}
	}catch(Exception e)
	{
		if(ri!=NULL)
			delete ri;
		//e.serialize()->materializeAndThrow();
		throw;
	}

	delete ri;
}

void RecordingModule::clearDatabase()
{
	STACKTRACE_INSTRUMENT();

	DBGateway *dbGW=new DBGateway(this->address, this->type, this->cn);
	
	RPC *rpc=NULL;
	try
	{
		rpc=dbGW->getRPC();
		
		if (rpc==NULL)
		{
			cout << "clearDatabase: Error accessing database" << endl;
			throw RecordingModuleException("Error accessing database");
		}
		
		dbGW->disableTransactionMode(rpc);
//		cout<<" begin"<<endl;
		dbGW->begin(rpc);

		string deleteQ=string("DELETE FROM nodoconfig nc WHERE nc.nombre='recordings' AND nc.padre IN (SELECT s.raizconfig FROM subsistema s WHERE host(s.ip)='")+this->address.getIP().toString()+string("' and s.puerto='")+StrUtils::decToString(this->address.getPort())+string("' AND s.tipo = '")+StrUtils::decToString(this->type)+string("')");

//		cout<<" -- clear call 1 "<<deleteQ<<endl;
//		cout << deleteQ << endl;
		XML *xml=dbGW->call(deleteQ, rpc, 1);
//		cout << xml->toString() << endl;
		delete xml;
		
		deleteQ=string("DELETE FROM nodoconfig nc WHERE nc.nombre='sessions' AND nc.padre IN (SELECT s.raizconfig FROM subsistema s WHERE host(s.ip)='")+this->address.getIP().toString()+string("' and s.puerto='")+StrUtils::decToString(this->address.getPort())+string("' AND s.tipo = '")+StrUtils::decToString(this->type)+string("')");
		
//		cout << deleteQ << endl;
//		cout<<" -- clear call 2"<<endl;
		xml=dbGW->call(deleteQ, rpc, 1);
//		cout << xml->toString() << endl;
		delete xml;
		
		dbGW->setSubsystemConfigParam("RecordingModule", this->address, string("/sessions"), string(""), rpc);
		dbGW->setSubsystemConfigParam("RecordingModule", this->address, string("/recordings"), string(""), rpc);
		dbGW->setSubsystemConfigParam("RecordingModule", this->address, string("/test_pa_ver_que_va"), string(""), rpc);
		
		
//		cout << "commit" << endl;
		dbGW->commit(rpc);
		dbGW->enableTransactionMode(rpc);
		
//		cout << "delRPC" << endl;
		dbGW->delRPC(rpc);
//		cout << "delete dbgw" << endl;
		delete dbGW;
	}
	catch(Exception &e)
	{
		cout << "RecordingModule::clearDatabase: " << e.getClass() << ": " << e.getMsg() << endl;
		dbGW->rollback(rpc);
		dbGW->enableTransactionMode(rpc);
		delete dbGW;
	}
	
}

void RecordingModule::startService()
{

	moduleInterface=(ModuleInterface*)new RecordingModuleInterface(this->address, this->type, baseDir, storeSizeLimit, this->cn, configFile);

	// Mirem si tenim setting per MaxActiveRecordingsCount
	xmlNode *n = config->getNode("/[0]/MaxActiveRecordingsCount");
	if(n!=NULL)
	{
		int count=atoi(n->getCdata().c_str());
		((RecordingModuleInterface*)moduleInterface)->setMaxActiveRecordingsCount(count);
	}

	this->cn->startSend();
}

void RecordingModule::statsOutput()
{
#ifdef REC_SAVEFRAME_DEBUG
	Thread::ttdDump();
#endif
}
