/*
 *  RecordingFileChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 19/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileChunkException.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <RecordingModule/RecordingFileFrameChunk.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <RecordingModule/RecordingFileDateMetadataChunk.h>
#include <RecordingModule/RecordingFilePTZMetadataChunk.h>
#include <RecordingModule/RecordingFileSegmentMetadataChunk.h>
#include <RecordingModule/RecordingFileStringMetadataChunk.h>
#include <RecordingModule/RecordingFileIndexChunk.h>
#include <RecordingModule/RecordingFileFrameInfoMetadataChunk.h>
#include <RecordingModule/RecordingFileAlarmMetadataChunk.h>
#include <RecordingModule/RecordingModuleDiskWriter.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Log.h>
#include <Utils/StrUtils.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

#pragma mark *** RecordingModuleInterface

#define INIT_RMCHUNK(_NAME,_VAR) _NAME *_VAR=new _NAME(); if (_VAR!=NULL) delete _VAR;

map <string,newChunkFunction> RecordingFileChunk::newChunkFunctions;
bool RecordingFileChunk::initedChunks=false;

void RecordingFileChunk::initChunks()
{
	STACKTRACE_INSTRUMENT();
	if (!initedChunks)
	{
		INIT_RMCHUNK(RecordingFileHeaderChunk, b);
		INIT_RMCHUNK(RecordingFileFrameChunk, c);
		INIT_RMCHUNK(RecordingFileMetadataChunk, d);
		INIT_RMCHUNK(RecordingFileDateMetadataChunk, e);
		INIT_RMCHUNK(RecordingFilePTZMetadataChunk, f);
		INIT_RMCHUNK(RecordingFileSegmentMetadataChunk, g);
		INIT_RMCHUNK(RecordingFileStringMetadataChunk, h);
		INIT_RMCHUNK(RecordingFileIndexChunk, i);
		INIT_RMCHUNK(RecordingFileFrameInfoMetadataChunk, j);
		initedChunks=true;
	}
}


RecordingFileChunk::RecordingFileChunk() : chunkSize(0), className(string("")), nSubChunks(0), subchunks(NULL), 
	chunkEndian(Endian::local)
{
	STACKTRACE_INSTRUMENT();
}

RecordingFileChunk::~RecordingFileChunk()
{
	STACKTRACE_INSTRUMENT();

	for (int i=nSubChunks-1; i>=0; i--)
	{
//		cout << "RecordingFileChunk - delSubChunk " << i << endl;
		this->delSubChunk(i); 
	}
	
	if (this->subchunks!=NULL)
		delete [] this->subchunks;
}

RecordingFileChunk* RecordingFileChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);
	
//	cout << "RecordingFileChunk::newChunk, typeLen: " << (int)chunkTypeLen << ", class: " << chunkClass << endl;

	map <string,newChunkFunction>::iterator nCFIt=RecordingFileChunk::newChunkFunctions.find(chunkClass);
	
	if (nCFIt==RecordingFileChunk::newChunkFunctions.end())
		throw RecordingFileChunkException(0, chunkClass+string("::newChunk not registered yet"));

	// cout << "RecordingFileChunk::newChunk " << chunkClass << endl;

	return (RecordingFileChunk*)RecordingFileChunk::newChunkFunctions[chunkClass](chunk, size, endian);
}

void* RecordingFileChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	// No s'hauria de cridar mai :P
	return NULL;
}

string RecordingFileChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileChunk";
	return c;
}

dword RecordingFileChunk::size()
{
	STACKTRACE_INSTRUMENT();
	return chunkSize;
}

dword RecordingFileChunk::getNSubChunks()
{
	STACKTRACE_INSTRUMENT();
	return nSubChunks;
}
	
Endian::endianType RecordingFileChunk::getEndian()
{
	STACKTRACE_INSTRUMENT();
	return chunkEndian;
}
	
void RecordingFileChunk::setEndian(Endian::endianType end)
{
	STACKTRACE_INSTRUMENT();
	chunkEndian=end;
}
	
void RecordingFileChunk::addSubChunk(RecordingFileChunk *c)
{
	STACKTRACE_INSTRUMENT();
	if (c==NULL)
		throw RecordingFileChunkException(0, "Invalid parameter");

//	cout << getClass() << ": addSubChunk, chunkSize start: " << chunkSize << endl;

    modMutex.lock();
    
	RecordingFileChunk **subchunksPrev=subchunks;
	
	nSubChunks++;
	subchunks=new RecordingFileChunk*[nSubChunks];
	
	if (subchunks==NULL)
	{
        modMutex.unlock();
		throw RecordingFileChunkException(0, "Not enough memory to reallocate subchunks array");
    }
		
	for (dword ch=0; ch<nSubChunks-1; ch++)
	{
		subchunks[ch]=subchunksPrev[ch];
	}
	
	subchunks[nSubChunks-1]=c;
	
	chunkSize+=sizeof(dword)+c->size();
	
	modMutex.unlock();
	
//	cout << getClass() << ": addSubChunk, chunkSize end: " << chunkSize << ", added: " << sizeof(dword) << "+"
//		<< c->size() << endl;
	
	delete [] subchunksPrev;
}

RecordingFileChunk* RecordingFileChunk::getSubChunk(dword idx)
{
	STACKTRACE_INSTRUMENT();
	if (idx>=nSubChunks)
		throw RecordingFileChunkException(0, "Supplied index out of range");

	return subchunks[idx];
}

void RecordingFileChunk::delSubChunk(RecordingFileChunk *c)
{
	STACKTRACE_INSTRUMENT();
	for (dword ch=0; ch<nSubChunks; ch++)
	{
        modMutex.lock();
        // doble check, per si canvia abans de fer el lock
		if (ch<nSubChunks && subchunks[ch]==c)
		{
			chunkSize-=sizeof(dword)+c->size();
			delete subchunks[ch];
			memmove(&subchunks[ch], &subchunks[ch+1], sizeof(RecordingFileChunk*)*(nSubChunks-(ch+1)));
			nSubChunks--;
            modMutex.unlock();
			return;
		}
        modMutex.unlock();
	}
	
	throw RecordingFileChunkException(0, "Supplied chunk not found");
}

void RecordingFileChunk::delSubChunk(dword idx)
{
	STACKTRACE_INSTRUMENT();
    modMutex.lock();
	if (idx>=nSubChunks)
	{
        modMutex.unlock();
		throw RecordingFileChunkException(0, "Supplied index out of range");
    }
    
	chunkSize-=sizeof(dword)+subchunks[idx]->size();
	delete subchunks[idx];
	memmove(&subchunks[idx], &subchunks[idx+1], sizeof(RecordingFileChunk*)*(nSubChunks-(idx+1)));
	nSubChunks--;
    modMutex.unlock();
}

RecordingFileChunk* RecordingFileChunk::replaceSubChunk(dword n, RecordingFileChunk *c, bool deleteOld)
{
	STACKTRACE_INSTRUMENT();
	if (n>=nSubChunks)
	{
		addSubChunk(c);
		return NULL;
	}
	
	RecordingFileChunk *old=NULL;
	dword curr=0;
	bool replaced=false;
	for (dword ch=0; ch<nSubChunks; ch++)
	{
        modMutex.lock();
		if (ch<nSubChunks && subchunks[ch]->getClass()==c->getClass() && curr==n)
		{
			chunkSize -= sizeof(dword) + subchunks[ch]->size();
			chunkSize += sizeof(dword) + c->size();

			if (deleteOld)
				delete subchunks[ch];
			else
				old=subchunks[ch];
			
			subchunks[ch]=c;
			replaced=true;
            modMutex.unlock();
			break;
		}
		else if (subchunks[ch]->getClass()==c->getClass())
			curr++;
        
        modMutex.unlock();
	}
	
	if (!replaced)
		addSubChunk(c);
	
	return old;
}

