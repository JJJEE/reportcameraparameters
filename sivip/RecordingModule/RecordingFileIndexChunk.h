/*
 *  RecordingFileIndexChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 18/05/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */


#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileMetadataChunk.h>
#include <map>

using namespace std;

class RecordingFileIndexChunk : public RecordingFileMetadataChunk
{
public:
	struct indexEntry
	{
		struct
		{
			qword secs;
			word millis;
		} date;
		fileOffset frameOffset;
		
		indexEntry()
		{
			date.secs=0;
			date.millis=0;
			frameOffset=0;
		};
		
		void from(Endian::endianType e, const void *b)
		{
			byte *buf=(byte*)b;
			
			date.secs=*((qword*)buf);
			buf+=sizeof(qword);
		
			date.millis=*((word*)buf);
			buf+=sizeof(word);
		
			frameOffset=*((fileOffset*)buf);
			buf+=sizeof(fileOffset);
		
			Endian::from(e, &date.secs, sizeof(date.secs));
			Endian::from(e, &date.millis, sizeof(date.millis));
			Endian::from(e, &frameOffset, sizeof(frameOffset));
		};

		void* to(Endian::endianType e)
		{
			byte *b=new byte[size()];
			
			byte *buf=b;
			
			*((qword*)buf)=date.secs;
			buf+=sizeof(qword);
			Endian::to(e, buf, sizeof(date.secs));
			
			*((word*)buf)=date.millis;
			buf+=sizeof(word);
			Endian::to(e, buf, sizeof(date.millis));
			
			*((fileOffset*)buf)=frameOffset;
			buf+=sizeof(fileOffset);
			Endian::to(e, buf, sizeof(frameOffset));
			
			return b;
		};
		
		dword size()
		{
			return sizeof(qword)+sizeof(word)+sizeof(fileOffset);
		}
	};

protected:

	dword indexSize;
	dword firstFrameNumber;
	fileOffset nextIndexOffset;

	indexEntry **index;
	map<fileOffset,dword> frameAtOffset;
	
	static const dword defaultSize=4096;
	
public:
	RecordingFileIndexChunk(dword size=RecordingFileIndexChunk::defaultSize, dword firstFrame=1);
	virtual ~RecordingFileIndexChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
	
	virtual void setFirstFrame(dword absoluteFrame);
	virtual dword getFirstFrame();
	virtual void setFrame(dword absoluteFrame, qword secs, word millis, fileOffset off);
	virtual indexEntry* getFrame(dword absoluteFrame);
	virtual void setNextIndexOffset(fileOffset off);
	virtual fileOffset getNextIndexOffset();
	
	dword getIndexSize();
};

