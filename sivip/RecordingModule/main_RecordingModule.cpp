#include <RecordingModule/RecordingModule.h>
#include <RecordingModule/RecordingModuleInterface.h>
#include <Utils/Types.h>
#include <Utils/Canis.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <stdio.h>
#include <signal.h>

#include <signal.h>

#include <version.h>

#ifndef WIN32
void sigattention(int sig)
{
	cout << "Signal: " << sig << endl;

	if (sig==SIGSEGV || sig==SIGBUS || sig==SIGINT)
	{
		NEW_DUMPALLOCATEDMEMORYMAP();
		STACKTRACE_DUMPALL();
	}
	abort();
}
#endif

int main()
{
#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGINT, sigattention);
	signal(SIGSEGV, sigattention);
	signal(SIGBUS, sigattention);
#endif
#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigattention);
	signal(SIGSEGV, sigattention);
	signal(SIGBUS, sigattention);
#endif

	STACKTRACE_INSTRUMENT();

	cout << siriusRelease << endl;
	RecordingModule *rm = NULL;
	try
	{
		rm = new RecordingModule("RecordingModuleConfig.xml");
		
		// Primer de tot, ara que ens estem iniciant, tanquem les gravacions actives
		cout << "Initializating service..." << endl;
		cout << "Fixating indexes..." << endl;
		rm->fixateIndex();
		cout << "Clearing database..." << endl;
		rm->clearDatabase();
		cout << "Starting service..." << endl;
		rm->startService();

		cout << "Service started. Serving..." << endl;
		rm->serve();
	}
	catch (Exception e)
	{
		cout << "Exception ocurred during initialization: " << e.getMsg() << endl;
		delete rm;
	}
	catch(std::exception &stde)
	{
		cout << "std::exception ocurred during initialization: " << stde.what()<< endl;
		delete rm;
	}

	return 0;
}

