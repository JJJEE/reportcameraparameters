/*
 *  RecordingFileSegmentMetadataChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileSegmentMetadataChunk.h>
#include <RecordingModule/RecordingFileDateMetadataChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

RecordingFileSegmentMetadataChunk::RecordingFileSegmentMetadataChunk() : firstFrameOffset(0)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileSegmentMetadataChunk::newChunk;
	className=getClass();
	
	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// La resta autoexplicatius (o no, pero poc important si ho son o no)
	chunkSize=sizeof(dword)+sizeof(byte)+className.length()+sizeof(nSubChunks)+chunkSizeCheck;

	RecordingFileDateMetadataChunk *date=new RecordingFileDateMetadataChunk();
	addSubChunk(date);
}

RecordingFileSegmentMetadataChunk::~RecordingFileSegmentMetadataChunk()
{
	STACKTRACE_INSTRUMENT();

}

RecordingFileChunk* RecordingFileSegmentMetadataChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();

	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);
	
//	if (chunkClass!=getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	RecordingFileSegmentMetadataChunk *resChunk=new RecordingFileSegmentMetadataChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

	// Li traiem els subchunks q pugui portar per defecte
	for (int i=resChunk->nSubChunks-1; i>=0; i--)
	{
		resChunk->delSubChunk(i); 
	}

	resChunk->chunkSize=size;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	
	chunkBytes+=chunkTypeLen;
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (resChunk->nSubChunks!=1)
		throw RecordingFileChunkException(0, "Segment chunk should have 1 and only 1 subchunk");
		
	if (resChunk->subchunks!=NULL)
		delete [] resChunk->subchunks;
	resChunk->subchunks=new RecordingFileChunk*[resChunk->nSubChunks];
	if (resChunk->subchunks==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate subchunks");
	
	// cout << "RecordingFileSegmentMetadataChunk::newChunk 1 - " << resChunk->nSubChunks << " subchunks"<< endl;

	for (dword c=0; c<resChunk->nSubChunks; c++)
	{
		dword subChunkSize=*((dword*)chunkBytes);
		Endian::from(endian, &subChunkSize, sizeof(dword));
		chunkBytes+=sizeof(dword);
		
		// cout << "subchunksize: " << subChunkSize << endl;

		resChunk->subchunks[c]=RecordingFileChunk::newChunk(chunkBytes, subChunkSize, endian);
		chunkBytes+=subChunkSize;
	}

	// cout << "RecordingFileSegmentMetadataChunk::newChunk 1" << endl;
	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	if (dataSize!=chunkSizeCheck)
		throw RecordingFileChunkException(0, resChunk->getClass()+string(": Invalid data size (")+
											StrUtils::decToString(dataSize)+string("/")+
											StrUtils::decToString(chunkSizeCheck)+string(")"));
	
	resChunk->firstFrameOffset=*((fileOffset*)chunkBytes);
	Endian::from(endian, &resChunk->firstFrameOffset, sizeof(fileOffset));
	chunkBytes+=sizeof(fileOffset);
	
	resChunk->nFrames=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nFrames, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

	return (RecordingFileChunk*)resChunk;
}

void* RecordingFileSegmentMetadataChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();

	byte *data=new byte[chunkSize];
	byte *chunkBytes=data;
	
	if (data==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
		
	byte chunkTypeLen=getClass().length();
	*chunkBytes=chunkTypeLen;
	chunkBytes++;
	
	memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
	chunkBytes+=chunkTypeLen;
	
	if (nSubChunks!=1)
		throw RecordingFileChunkException(0, "SegmentMetadata chunk should have 1 and only 1 subchunk");
	*((dword*)chunkBytes)=nSubChunks;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	for (dword c=0; c<nSubChunks; c++)
	{
		dword subchunkSize=subchunks[c]->size();
		*((dword*)chunkBytes)=subchunkSize;
		Endian::to(dstEndian, chunkBytes, sizeof(dword));
		chunkBytes+=sizeof(dword);

		byte *subchunkData=(byte*)subchunks[c]->getData(dstEndian);
		memmove(chunkBytes, subchunkData, subchunkSize);
		delete [] subchunkData;
		chunkBytes+=subchunkSize;
	}
	
	*((dword*)chunkBytes)=chunkSizeCheck;
	Endian::to(dstEndian, chunkBytes, sizeof(dword));
	chunkBytes+=sizeof(dword);

	*((fileOffset*)chunkBytes)=firstFrameOffset;
	Endian::to(dstEndian, chunkBytes, sizeof(firstFrameOffset));
	chunkBytes+=sizeof(fileOffset);

	*((dword*)chunkBytes)=nFrames;
	Endian::to(dstEndian, chunkBytes, sizeof(nFrames));

	return data;
}

string RecordingFileSegmentMetadataChunk::getClass()
{
	STACKTRACE_INSTRUMENT();

	string c="RecordingFileSegmentMetadataChunk";
	return c;
}

void RecordingFileSegmentMetadataChunk::setDate(qword secs, word millis)
{
	STACKTRACE_INSTRUMENT();

	((RecordingFileDateMetadataChunk*)subchunks[0])->setDate(secs, millis);
}

RecordingFileDateMetadataChunk* RecordingFileSegmentMetadataChunk::getDate()
{
	STACKTRACE_INSTRUMENT();

	return (RecordingFileDateMetadataChunk*)subchunks[0];
}

void RecordingFileSegmentMetadataChunk::setFirstFrameOffset(fileOffset off)
{
	STACKTRACE_INSTRUMENT();

	firstFrameOffset=off;
}

fileOffset RecordingFileSegmentMetadataChunk::getFirstFrameOffset()
{
	STACKTRACE_INSTRUMENT();

	return firstFrameOffset;
}

void RecordingFileSegmentMetadataChunk::setNFrames(dword nf)
{
	STACKTRACE_INSTRUMENT();

	nFrames=nf;
}

void RecordingFileSegmentMetadataChunk::incNFrames(int inc)
{
	STACKTRACE_INSTRUMENT();
	nFrames+=inc;
}

dword RecordingFileSegmentMetadataChunk::getNFrames()
{
	STACKTRACE_INSTRUMENT();
	return nFrames;
}

