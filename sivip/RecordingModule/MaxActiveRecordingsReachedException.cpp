/*
 *  MaxActiveRecordingsReachedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/MaxActiveRecordingsReachedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

MaxActiveRecordingsReachedException::MaxActiveRecordingsReachedException(string msg, int code): Exception(msg, code)
{

}

MaxActiveRecordingsReachedException::MaxActiveRecordingsReachedException(int code, string msg): Exception(msg, code)
{

}

MaxActiveRecordingsReachedException::MaxActiveRecordingsReachedException(SerializedException &se): Exception(se)
{

}

MaxActiveRecordingsReachedException::~MaxActiveRecordingsReachedException()
{
   
}

string MaxActiveRecordingsReachedException::getClass()
{
	string c=string("MaxActiveRecordingsReachedException");
	return c;
}

