/*
 *  RecordingFileException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class RecordingFileException : public Exception
{
 public:
	RecordingFileException(string msg, int code=0);
	RecordingFileException(int code, string msg);
	RecordingFileException(SerializedException &se);

	~RecordingFileException();
	
	virtual string getClass();
};

