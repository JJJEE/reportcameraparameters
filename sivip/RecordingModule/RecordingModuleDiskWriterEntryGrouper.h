#ifndef __SIRIUS__RECORDINGMODULE__RECORDINGMODULEDISKWRITERENTRYGROUPER_H
#define __SIRIUS__RECORDINGMODULE__RECORDINGMODULEDISKWRITERENTRYGROUPER_H

#include <Threads/RWlock.h>
#include <RecordingModule/RecordingModuleDiskWriterEntry.h>
#include <RecordingModule/RecordingModuleDiskWriterEntryGroup.h>

#include <map>
#include <list>

using namespace std;

class RecordingModuleDiskWriterEntryGrouper
{
public:
	// Tamany maxim que guardem per cada camera
	static const dword maxTotalLength=8*1024*1024;

protected:
	list<RecordingModuleDiskWriterEntryGroup*> groups;
	RWLock groupsRW;
	
public:
	RecordingModuleDiskWriterEntryGrouper();
	virtual ~RecordingModuleDiskWriterEntryGrouper();
	
	float addEntry(RecordingModuleDiskWriterEntry *entry);
	
	list<RecordingModuleDiskWriterEntryGroup*>* getList();
	list<RecordingModuleDiskWriterEntryGroup*> getReadyGroups();
	list<RecordingModuleDiskWriterEntryGroup*> getAllGroups();
	void wlock();
	void rlock();
	void unlock();

	bool empty();
	dword size();
	
	float calcFullFactor();
};

#endif
