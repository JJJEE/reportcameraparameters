/*
 *  RecordingModuleSessionNotStablishedException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <RecordingModule/RecordingModuleException.h>

using namespace std;

class RecordingModuleSessionNotStablishedException : public Exception
{
 public:
	RecordingModuleSessionNotStablishedException(string msg, int code=0);
	RecordingModuleSessionNotStablishedException(int code, string msg);
	RecordingModuleSessionNotStablishedException(SerializedException &se);

	~RecordingModuleSessionNotStablishedException();
	
	virtual string getClass();
};

