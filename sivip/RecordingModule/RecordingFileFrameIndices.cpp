#include <RecordingModule/RecordingFileFrameIndices.h>

#pragma mark *** RecordingFileFrameIndices ---------------------------

#pragma mark *** Estatiques
map<string, RecordingFileFrameIndex*>
	RecordingFileFrameIndices::frameIndexByFile;
RWLock RecordingFileFrameIndices::frameIndexRW;

#pragma mark *** Metodes
RecordingFileFrameIndex *RecordingFileFrameIndices::getIndexForFile(string file,
	RecordingModuleDiskWriter *writer)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileFrameIndex *fi=NULL;
	
	RecordingFileFrameIndices::rlock();

	map<string, RecordingFileFrameIndex*>::iterator iIt =RecordingFileFrameIndices::frameIndexByFile.end();
	try
	{
		iIt =
			RecordingFileFrameIndices::frameIndexByFile.find(file);
	
		if (iIt!=RecordingFileFrameIndices::frameIndexByFile.end())
			fi=iIt->second;
	}catch(...)
	{
		RecordingFileFrameIndices::unlock();
		throw;
	}

	RecordingFileFrameIndices::unlock();

	if (fi!=NULL)
		return fi;

	// wlock, double check i posar.
	RecordingFileFrameIndices::wlock();

	try
	{
		iIt = RecordingFileFrameIndices::frameIndexByFile.find(file);

		if (iIt!=RecordingFileFrameIndices::frameIndexByFile.end())
			fi=iIt->second;
		else
		{
			// Si no l'hem trobat i no ens l'han creat entre locks
			// intentem crear-lo a partir del fitxer
			fi = new RecordingFileFrameIndex(writer, file);
			RecordingFileFrameIndices::frameIndexByFile[file] = fi;
		}
	}
	catch (Exception &e)
	{
		RecordingFileFrameIndices::unlock();
		// Ens assegurem q si hi ha cap error, fi es NULL
		fi=NULL;

		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	catch (...)
	{
		RecordingFileFrameIndices::unlock();
		fi=NULL;
		throw;
	}

	RecordingFileFrameIndices::unlock();
	
	return fi;
}

void RecordingFileFrameIndices::setIndexForFile(string file,
	RecordingFileFrameIndex *index)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileFrameIndices::wlock();
	try
	{
		RecordingFileFrameIndices::frameIndexByFile[file] = index;
		RecordingFileFrameIndices::unlock();
	}
	catch (...)
	{
		RecordingFileFrameIndices::unlock();
		throw;
	}
}

void RecordingFileFrameIndices::forgetIndexForFile(string file)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileFrameIndex *fi=NULL;
	
	RecordingFileFrameIndices::wlock();
	try
	{
		map<string, RecordingFileFrameIndex*>::iterator iIt =
			RecordingFileFrameIndices::frameIndexByFile.find(file);
	
		if (iIt!=RecordingFileFrameIndices::frameIndexByFile.end())
		{
			fi=iIt->second;
			RecordingFileFrameIndices::frameIndexByFile.erase(iIt);
		}

		RecordingFileFrameIndices::unlock();
		if (fi!=NULL)
			delete fi;
	}
	catch (...)
	{		
		RecordingFileFrameIndices::unlock();
		if (fi!=NULL)
			delete fi;

		throw;
	}	
}

void RecordingFileFrameIndices::wlock()
{
	STACKTRACE_INSTRUMENT();
	RecordingFileFrameIndices::frameIndexRW.wlock();
}

void RecordingFileFrameIndices::rlock()
{
	STACKTRACE_INSTRUMENT();
	RecordingFileFrameIndices::frameIndexRW.rlock();
}

void RecordingFileFrameIndices::unlock()
{
	STACKTRACE_INSTRUMENT();
	RecordingFileFrameIndices::frameIndexRW.unlock();
}

