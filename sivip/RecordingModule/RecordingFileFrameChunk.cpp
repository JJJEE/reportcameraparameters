/*
 *  RecordingFileFrameChunk.cpp
 *  
 *
 *  Created by David Marí Larrosa on 19/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <RecordingModule/RecordingFileFrameChunk.h>
#include <RecordingModule/RecordingFileChunkException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/CheckPoint.h>
#include <Utils/Log.h>
#include <Utils/StrUtils.h>
// #include <Threads/Thread.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

RecordingFileFrameChunk::RecordingFileFrameChunk() : frameSize(0), frameData(NULL)
{
	STACKTRACE_INSTRUMENT();
	RecordingFileChunk::newChunkFunctions[getClass()]=(newChunkFunction)RecordingFileFrameChunk::newChunk;
	className=getClass();

	// sizeof(dword) per l'indicador de tamany de les dades (el del mig del chunk)
	// sizeof(byte) per la longitud del nom de classe
	// La resta autoexplicatius (o no, pero poc important si ho son o no)
	chunkSize=sizeof(dword)+sizeof(byte)+className.length()+sizeof(nSubChunks)+sizeof(frameSize);

	// Afegim la unica metadada obligatoria q es la data del frame. La resta de metadades, com que
	// son opcionals, s'afegiran des de fora :P
	RecordingFileDateMetadataChunk *date=new RecordingFileDateMetadataChunk();
	addSubChunk(date);
}

RecordingFileFrameChunk::~RecordingFileFrameChunk()
{
	STACKTRACE_INSTRUMENT();

	if (this->frameData!=NULL)
	{
//#ifndef WIN32
//		cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" [delete] ") << this->frameData << endl;
//		STACKTRACE_DUMP();
//#endif	
		delete [] (byte*)this->frameData;
		this->frameData=NULL; //paranoia
	}
}

RecordingFileChunk* RecordingFileFrameChunk::newChunk(const void *chunk, dword size, Endian::endianType endian)
{
	STACKTRACE_INSTRUMENT();
	const byte *chunkBytesBase=(byte*)chunk;
	const byte *chunkBytes=(byte*)chunk;
	
	byte chunkTypeLen=*chunkBytes;
	chunkBytes+=sizeof(byte);
	
	if (chunkBytes-chunkBytesBase>size)
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	
	string chunkClass((char*)chunkBytes, chunkTypeLen);
	
//	if (chunkClass!=getClass())
//		throw RecordingFileChunkException(0, "Invalid chunk");
	
	RecordingFileFrameChunk *resChunk=new RecordingFileFrameChunk();
	if (resChunk==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");

	// Li traiem els subchunks q pugui portar per defecte
	for (int i=resChunk->nSubChunks-1; i>=0; i--)
	{
		resChunk->delSubChunk(i); 
	}

	resChunk->chunkSize=size;
	resChunk->chunkEndian=endian;
	resChunk->className=chunkClass;
	
	chunkBytes+=chunkTypeLen;
	
	if (chunkBytes-chunkBytesBase>size)
	{
		delete resChunk;
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	}
	
	resChunk->nSubChunks=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->nSubChunks, sizeof(dword));
	chunkBytes+=sizeof(dword);

	if (chunkBytes-chunkBytesBase>size)
	{
		delete resChunk;
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	}
	
	if (resChunk->nSubChunks!=0)
	{
		if (resChunk->subchunks!=NULL)
			delete [] resChunk->subchunks;

		resChunk->subchunks=new RecordingFileChunk*[resChunk->nSubChunks];
		if (resChunk->subchunks==NULL)
		{
			delete resChunk;
			throw RecordingFileChunkException(0, "Not enough memory to allocate subchunks");
		}
		
		for (dword c=0; c<resChunk->nSubChunks; c++)
		{
			dword subChunkSize=*((dword*)chunkBytes);
			Endian::from(endian, &subChunkSize, sizeof(dword));
			chunkBytes+=sizeof(dword);
			
			resChunk->subchunks[c]=RecordingFileChunk::newChunk(chunkBytes, subChunkSize, endian);
			chunkBytes+=subChunkSize;
		}
	}
	
	dword dataSize=*((dword*)chunkBytes);
	Endian::from(endian, &dataSize, sizeof(dword));
	chunkBytes+=sizeof(dword);
	
	if (chunkBytes-chunkBytesBase+dataSize>size)
	{
		delete resChunk;
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	}
	
//	if (chunkBytes-chunkBytesBase>size)
//		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");

//	if (dataSize!=chunkSizeCheck)
//		throw RecordingFileChunkException(0, resChunk->getClass()+string(": Invalid data size (")+
//											StrUtils::decToString(dataSize)+string("/")+
//											StrUtils::decToString(chunkSizeCheck)+string(")"));

	resChunk->frameSize=*((dword*)chunkBytes);
	Endian::from(endian, &resChunk->frameSize, sizeof(dword));
	chunkBytes+=sizeof(dword);

	STACKTRACE_EXTRATRACE();
	resChunk->frameData=new byte[resChunk->frameSize];
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" [new] ") << resChunk->frameData << endl;
//	STACKTRACE_DUMP();
//#endif	
	STACKTRACE_EXTRATRACE();
	if (resChunk->frameData==NULL)
	{
		delete resChunk;
		throw RecordingFileChunkException(0, "Not enough memory to allocate frame");
	}
	
	memmove(resChunk->frameData, chunkBytes, resChunk->frameSize);
	chunkBytes+=resChunk->frameSize;
	
	if (chunkBytes-chunkBytesBase>size)
	{
		delete resChunk;
		throw RecordingFileChunkException(0, "Invalid chunk - Out of bounds");
	}
	
	return (RecordingFileChunk*)resChunk;
}

void* RecordingFileFrameChunk::getData(Endian::endianType dstEndian)
{
	STACKTRACE_INSTRUMENT();
	INIT_CHECKPOINT();

	try
	{
		CHECKPOINT();
		byte *data=new byte[this->chunkSize];
		//cout<<" RecFileFrChk data:"<<(void*)data<<endl;
		CHECKPOINT();
		byte *chunkBytes=data;
		
		CHECKPOINT();
		if (data==NULL)
			throw RecordingFileChunkException(0, "Not enough memory to allocate chunk");
			
		CHECKPOINT();
		byte chunkTypeLen=getClass().length();
		*chunkBytes=chunkTypeLen;
		chunkBytes++;
		
		CHECKPOINT();
		memmove(chunkBytes, getClass().c_str(), chunkTypeLen);
		chunkBytes+=chunkTypeLen;
		
		CHECKPOINT();
		*((dword*)chunkBytes)=nSubChunks;
		Endian::to(dstEndian, chunkBytes, sizeof(dword));
		chunkBytes+=sizeof(dword);
	
		for (dword c=0; c<nSubChunks; c++)
		{
			CHECKPOINT();
			dword subchunkSize=subchunks[c]->size();
			*((dword*)chunkBytes)=subchunkSize;
			Endian::to(dstEndian, chunkBytes, sizeof(dword));
			chunkBytes+=sizeof(dword);
	
			CHECKPOINT();
			byte *subchunkData=(byte*)subchunks[c]->getData(dstEndian);
			memmove(chunkBytes, subchunkData, subchunkSize);
			delete [] subchunkData;
			chunkBytes+=subchunkSize;
		}
		
		CHECKPOINT();
		*((dword*)chunkBytes)=sizeof(dword)+frameSize;
		Endian::to(dstEndian, chunkBytes, sizeof(dword));
		chunkBytes+=sizeof(dword);
	
		CHECKPOINT();
		*((dword*)chunkBytes)=frameSize;
		Endian::to(dstEndian, chunkBytes, sizeof(dword));
		chunkBytes+=sizeof(dword);
	
		CHECKPOINT();
		memmove(chunkBytes, frameData, frameSize);
	
	//	cout << getClass() << "::getData() chunkSize: " << chunkSize << endl;
	
		return data;
	}
	catch (std::exception &stde)
	{
		cout << Log::getDateLogTag() << __FILE__ << " Line " << std::dec << __LINE__ << CHECKPOINT_TOSTRING() << ": " << stde.what() << endl;

#ifdef WITH_DEBUG_GDB
		abort();
#endif
		
		throw stde;
	}

	return NULL;	
}

string RecordingFileFrameChunk::getClass()
{
	STACKTRACE_INSTRUMENT();
	string c="RecordingFileFrameChunk";
	return c;
}

void RecordingFileFrameChunk::setFrame(void *frame, dword len)
{
	STACKTRACE_INSTRUMENT();
	if (len==frameSize && frameData!=NULL)
	{
		memmove(frameData, frame, frameSize);
		return;
	}

	STACKTRACE_EXTRATRACE();
	byte *newFrame=new byte[len];
//#ifndef WIN32
//	cout << string("[") + Log::getDateLogTag() + string(" - ") + StrUtils::hexToString((dword)Thread::getId()) + string("] ") + string(__FILE__) + string(" line ") + StrUtils::decToString(__LINE__) + string(" [new] ") << newFrame << endl;
//	STACKTRACE_DUMP();
//#endif	
	
	if (newFrame==NULL)
		throw RecordingFileChunkException(0, "Not enough memory to allocate frame");
	
	if (frameData!=NULL)
	{
		delete [] (byte*)frameData;
		frameData=NULL;
	}

	chunkSize-=frameSize;
	chunkSize+=len;
	frameData=newFrame;
	frameSize=len;
	memmove(frameData, frame, frameSize);
}

const void* RecordingFileFrameChunk::getFrame()
{
	STACKTRACE_INSTRUMENT();
	return (const void*)frameData;
}

dword RecordingFileFrameChunk::getFrameSize()
{
	STACKTRACE_INSTRUMENT();
	return frameSize;
}

