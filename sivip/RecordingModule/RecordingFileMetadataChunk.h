/*
 *  RecordingFileMetadataChunk.h
 *  
 *
 *  Created by David Marí Larrosa on 21/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <RecordingModule/RecordingFileChunk.h>

class RecordingFileMetadataChunk : public RecordingFileChunk
{

public:
	RecordingFileMetadataChunk();
	virtual ~RecordingFileMetadataChunk();

	static RecordingFileChunk* newChunk(const void *chunk, dword size, Endian::endianType endian);

	virtual void* getData(Endian::endianType dstEndian);
	virtual string getClass();
};

