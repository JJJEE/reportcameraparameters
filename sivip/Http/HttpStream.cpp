#include <Http/HttpStream.h>
#include <Utils/StrUtils.h>
#include <Sockets/SocketException.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <errno.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif

using namespace std;

void HttpStream::addStatusDesc(string status, string msg, bool replace)
{
	if (requestStatusDescription.find(string("200")) == 
			requestStatusDescription.end() || replace)
		requestStatusDescription[status]=msg;
}

HttpStream::HttpStream(bool debugMode) : socket(NULL), requestStatus(200),
estat(RCV_STATE_INITIAL), version(string("1.1")), debug(debugMode),
Log("HttpStream.log"), conts(string(""))
{
	recvBuf=new char[HttpStream::recvBufMax];
}

HttpStream::~HttpStream(void)
{
	if (socket!=NULL)
		delete socket;

	if (recvBuf!=NULL)
		delete [] recvBuf;
}

// retorna un string amb la descripcio d'un codi de resposta HTTP
string HttpStream::getRequestStatusDesc(int status)
{
	if (requestStatusDescription.size()<6)
	{
		// Si es < 6 es que no hem inicialitzat. Igual hem posat alguna
		// "personalitzada" pel processat de la resposta, pero no
		// hem posat aquestes
		requestStatusDescription[string("200")]=string("OK");
		requestStatusDescription[string("204")]=string("No Content");
		requestStatusDescription[string("401")]=string("Authorization required");
		requestStatusDescription[string("403")]=string("Forbidden");
		requestStatusDescription[string("404")]=string("Not Found");
		requestStatusDescription[string("500")]=string("Internal Server Error");
	}
	
	if (requestStatusDescription.find(StrUtils::decToString(status))!=
		requestStatusDescription.end())
	{
		return requestStatusDescription[StrUtils::decToString(status)];
	}
	
	return string("--");
}

// estableix codi de resposta HTTP
void HttpStream::setRequestStatus(int status)
{
	requestStatus=status;
}

// obte codi de resposta HTTP
int HttpStream::getRequestStatus()
{
	return requestStatus;
}

// codifica un text en format per parametre http
string HttpStream::urlEncode(string str)
{
	string s=string("");
	for (string::size_type i=0; i<str.length(); i++)
	{
		if (str[i]<=32 || (unsigned char)str[i]>127)
			s+=string("%")+StrUtils::decToHex(str[i]);
		else
			s+=str[i];
	}
	return s;
}

// descodifica una string codificada en format url
string HttpStream::urlDecode(string str)
{
	string s=string("");

	for (string::size_type i=0; i<str.length(); i++)
	{
		if (str[i]=='%')
		{
			string num=str.substr(i+1, 2);
			string s2(" ");
			s2[0]=StrUtils::hexToDec(num);
			s+=s2;
			i+=2;
		}
		else
			s+=str[i];
	}
	return s;
}

// retorna les parts d'una url. ('proto', 'host', 'port', 'path', 'params')
map<string,string> HttpStream::parseURL(string url, string defaultPort)
{
	map <string, string> empty;
	map <string, string> parsed;

	if (url.length()==0)
	{
		return empty;
	}

	string parsingUrl=url;

	string::size_type endProto=parsingUrl.find(string(":"));

	if (endProto==string::npos)
	{
		return empty;
	}

	parsed["proto"]=parsingUrl.substr(0, endProto);

	parsingUrl=parsingUrl.substr(endProto+1);

	if (parsingUrl.length()==0)
	{
		return empty;
	}
	
	// ens hauria de quedar //resta_url
	//
	if (parsingUrl.substr(0, 2).compare(string("//"))!=0)
	{
		return empty;
	}

	parsingUrl=parsingUrl.substr(2);

	if (parsingUrl.length()==0)
	{
		return empty;
	}

	string::size_type endHost=parsingUrl.find(string("/"));

	if (endHost==string::npos)
		endHost=parsingUrl.length();

	string hostAndPort=parsingUrl.substr(0, endHost);

	string::size_type portSep=hostAndPort.find(":");

	if (portSep==string::npos)
	{
		portSep=hostAndPort.length();
		parsed["port"]=defaultPort;
	}
	else
		parsed["port"]=hostAndPort.substr(portSep+1);

	parsed["host"]=hostAndPort.substr(0, portSep);

	// parsingUrl ha de ser /xxxx si es que es algo
	parsingUrl=parsingUrl.substr(hostAndPort.length());

	if (parsingUrl.length()==0)
	{
		parsed["path"]=string("/");
		return parsed;
	}

	string::size_type iniParams=parsingUrl.find("?");

	if (iniParams==string::npos)
	{
		parsed["path"]=parsingUrl;
		return parsed;
	}

	parsed["path"]=parsingUrl.substr(0, iniParams);
	parsed["params"]=parsingUrl.substr(iniParams);

	return parsed;
}

// Estableix el socket de recepcio de dades
void HttpStream::setReceiveSocket(SocketTCP *stcp)
{
	// Si ens estan canviant el socket, hauriem de borrar el vell...
	if (this->socket!=NULL)
		delete this->socket;
		
	this->socket=stcp;
}

// munta una peticio http
string HttpStream::createRequestHeader(string url,
									   map<string,string> params,
									   string httpVersion,
									   map<string,string> extraHeaders)
{
	string req=string("");

	return req;
}

// munta una capcalera de resposta HTTP
string HttpStream::createResponseHeader(map<string,string> extraHeaders)
{
	string response=string("HTTP/")+version+string(" ")+
		StrUtils::decToString(requestStatus)+string(" ")+
		getRequestStatusDesc(requestStatus)+
		string("\r\n");
	
	map<string,string>::const_iterator itCT=extraHeaders.find("Content-Type");

	map<string,string>::const_iterator it;
	if (itCT!=extraHeaders.end() && itCT->second.substr(0,9)!=string("multipart"))
	{
		it=extraHeaders.find("Content-Length");
	
		if (it==extraHeaders.end())
			response+=string("Content-Length: 0\r\n");
	}
	
	for (it=extraHeaders.begin(); it!=extraHeaders.end(); it++)
	{
		if ((*it).first!=string("") && (*it).second!=string(""))
			response+=(*it).first+string(": ")+(*it).second+string("\r\n");
	}
	
	response+=string("\r\n");
						 
	return response;
}


// retorna un tros d'informacio:
// - Si hi ha boundaries en la resposta, el tros corresponent a un
//   boundary
// - Si no hi ha boundaries, tot el reply
string HttpStream::getNextInformationChunk(string **partHdr)
{
	if (debug)
		logLine(string(">>> getNextInformationChunk"));
	
	switch (estat&0xffff)
	{
	case RCV_STATE_INITIAL:
		{
			if (debug)
				logLine(string("   RCV_STATE_INITIAL"));

			string hdrStr("");

//			char tmp;

				int loop=0;
			while (hdrStr.length()<4 ||
				   (hdrStr.substr(hdrStr.length()-4)!=string("\r\n\r\n") && hdrStr.substr(hdrStr.length()-2)!=string("\n\n")))
			{
				int hLen=socket->peek(recvBuf, HttpStream::recvBufMax);
				
				if (hLen>0)
				{
					hdrStr=string(recvBuf, hLen);
				//cout<<" peek:"<<hLen<<" -->"<<hdrStr<<endl;
					
					// Trobem el primer final de capçaleres
					string::size_type hdrEnd=hdrStr.find("\r\n\r\n");
					
					if (hdrEnd!=string::npos)
					{
						hLen=hdrEnd+4;
						hdrStr=hdrStr.substr(0, hLen);
						// I traiem la capçalera del buffer :P
						socket->read(recvBuf, hLen);
					}
					else
					{
						hdrEnd=hdrStr.find("\n\n");
						
						if (hdrEnd!=string::npos)
						{
							hLen=hdrEnd+2;
							hdrStr=hdrStr.substr(0, hLen);
							// I traiem la capçalera del buffer :P
							socket->read(recvBuf, hLen);
						}
					}
		//			cout<<"hdr:"<<StrUtils::hexDump(hdrStr)<<endl;
		//			cout<<"srch:"<<StrUtils::hexDump(string("\r\n\r\n"))<<endl;
				}
				if(++loop > maxRetries)
				{
					this->setRequestStatus(404);//no es un servidor Http? diguem que "Not Found"
					cout<<"HttpStream Error: could not read Http Header after "<<maxRetries<<" retries. Server answer:"<<hdrStr<<endl;
					throw SocketException("Http Error: not an Http Server?:"+hdrStr);
				}

//				socket->read(&tmp, 1);
//				hdrStr+=string(" ");
//				hdrStr[hdrStr.length()-1]=tmp;
				
//				if (debug)
//					logLine(string("   Header read: ")+hdrStr);
			}

//			cout<<"   GetNextInfChk::"<<hdrStr<<endl;
			// Fem un parsat de la capcalera
			list <string> linies=StrUtils::split(hdrStr, "\n");
			string lastHeader("");
			list<string>::const_iterator itStr=linies.begin();

			string linia=(*itStr);
	//cout<<"linia1:"<<linia<<endl;
			// Primera linia, status...
			if (debug)
				logLine(string("   ")+linia);
			list <string> status=StrUtils::split(linia, " ");

			list<string>::const_iterator statusIt=status.begin();
			statusIt++;
			string statusCodeStr=(*statusIt);
			requestStatus=atoi((*statusIt).c_str());
			statusIt++;
			
			string statusMsg;
			while (statusIt!=status.end())
			{
				//cout<<" status:"<<*statusIt<<endl;
				statusMsg=(*statusIt)+string(" ");
				statusIt++;
			}
			if (statusMsg.length()>0)
			{
				statusMsg=statusMsg.substr(0, statusMsg.length()-1);
				this->addStatusDesc(statusCodeStr, statusMsg);
			}
			
			itStr++;

			for (;itStr!=linies.end(); itStr++)
			{

				string linia=(*itStr);
	//cout<<"linia2-l:"<<linia<<endl;

				if (linia[0]==' ' || linia[0]=='\t')
				{
					// Es continuacio de l'anterior
					header[lastHeader]+=string(" ")+StrUtils::trim(linia);
				}
				else if (linia!=string("\r\n") &&
						 linia.find(string(":"))!=string::npos)
				{
					list <string> components=StrUtils::split(linia, ":", 1);

					list<string>::const_iterator itHdrName=components.begin();
					itHdrName++;
					string contents=(*itHdrName);
					itHdrName--;

					lastHeader=(*itHdrName);

					if (debug)
						logLine(string("   ")+lastHeader+string(": ")+contents);
					header[lastHeader]=StrUtils::trim(contents);
				}
			}

			estat=RCV_STATE_HEADER_RECVD;

			return hdrStr;
		}
		break;

	case RCV_STATE_HEADER_RECVD:
		{
			if (debug)
				logLine(string("   RCV_STATE_RECVD"));


			// Pot ser:
			// - Normal: llegit Content-length i a cagar a la via
			// - Chunked: Content-Length no te cap valor per la lectura
			// - si hi ha boundary, llegir un catxo i retornar
			string readData("");

			map<string,string>::const_iterator te=header.find("Transfer-Encoding");
			map<string,string>::const_iterator cl=header.find("Content-Length");
			map<string,string>::const_iterator ct=header.find("Content-Type");
			if (te!=header.end() && (*te).second==string("chunked"))
			{

				if (debug)
					logLine(string("   chunked"));
				//char *buf=new char [1024];

				// A llegir chunks
				string linia=StrUtils::readLine(socket);

				// ha de ser un numero :P
				unsigned int len=StrUtils::stringToHex(linia);

				while (len!=0)
				{
					unsigned int readLen=0;
					while (readLen<len)
					{
						//cout << "(" << __FILE__ << ":" << __LINE__ << "reading... len:"<<len<<" readLen:"<<readLen << endl;
						int n=socket->read(recvBuf, (len-readLen>=HttpStream::recvBufMax?HttpStream::recvBufMax:len-readLen));

						//cout << "(" << __FILE__ << ":" << __LINE__ << " readData.append(buf, " << n << "); len:"<<len<<" readLen:"<<readLen << endl;
						if (n>0)
							readData.append(recvBuf, n); //+=string(buf, (len-readLen>=1024?1024:len-readLen));

						readLen+=n;
					}

					// linia en blanc de despres del chunk
					linia=StrUtils::readLine(socket);
	 //cout <<"linia3:"<<linia<<endl;

					// seguent chunk
					linia=StrUtils::readLine(socket);
	 //cout <<"linia4:"<<linia<<endl;

					// ha de ser un numero :P
					len=StrUtils::stringToHex(linia);
	 //cout <<"linia5:"<<linia<<endl;
				}

//				delete [] buf;
				return readData;
			}
			else if (ct!=header.end() && (*ct).second.find(string("boundary"))!=string::npos)
			{
				if (debug)
					logLine(string("   boundary"));

				int n=0;
				
				// amb boundary
				string::size_type bndpos=(*ct).second.find(string("boundary="));
				bndpos+=string("boundary=").length();

				string boundary=(*ct).second.substr(bndpos);
				
				int bLen=boundary.length();
				int bLast=bLen-1;

				// eliminem \r\n del final (que en principi no hi sera)...
				while (boundary[bLast]=='\n' ||
					   boundary[bLast]=='\r')
				{
					boundary=boundary.substr(0, bLast);
					bLast--;
				}

				if (boundary[bLast]==';')  // Nomes 1 ;)
				{
					boundary=boundary.substr(0, bLast);
					bLast--;
				}
				
				if (debug)
					logLine(string("      > ")+boundary);

				// anar llegint i trobant boundaries...
//				char *buf=new char[1024];
		
				n=socket->read(recvBuf, HttpStream::smallArbitraryReadLength);
				if (n>0)
					conts+=string(recvBuf, n);

				string::size_type bndPosEnd=conts.find(string("--")+boundary+string("--\r\n"));
				string::size_type bndPos=conts.find(string("\r\n")+boundary+string("\r\n"));
				// El primer boundary ve sense \r\n davant
				string::size_type firstBndPos=conts.find(boundary+string("\r\n"));

//				cout << "bndPos " << bndPos << ", bndPosEnd " << bndPosEnd << endl;

				if (bndPosEnd!=string::npos && bndPos==string::npos && firstBndPos==string::npos)
				{
					return string("");
				}
				
				// TODO: 20090513 Caldria comprovar a mes que realment es el
				// primer boundary que rebem. Com que es altament improbable
				// que no ho sigui, doncs eso, TODO :)
				if (bndPos==string::npos)
					bndPos=firstBndPos;
					
				if (bndPos!=string::npos)
				{
//					cout << "Found " << boundary << " at " << bndPos << endl;
								
					string::size_type iniHdr=bndPos+(string("\r\n")+boundary+string("\r\n")).length();
					while (conts.substr(bndPos, 4)!=string("\r\n\r\n"))
					{
						bndPos++;
						
						while (bndPos>=conts.length()-4) // >= per curar-nos en salut
						{
							// bucle petat per longitud
							// Es un while per si algun read retornes 0 
							int len=conts.length()-4;
							n=socket->read(recvBuf, HttpStream::smallArbitraryReadLength);
							if (n>0)
								conts+=string(recvBuf, n);

							conts=conts.substr(len);
							bndPos=0;
						}
					}

					if (partHdr!=NULL)
						*(*partHdr)=conts.substr(iniHdr, bndPos+4-iniHdr);
					
//					cout << "Part Header " << conts.substr(iniHdr, bndPos+4-iniHdr) << endl;
					
					conts=conts.substr(bndPos+4);
				}
				
				bndPos=conts.find(string("\r\n")+boundary+string("\r\n"));
				bndPosEnd=conts.find(string("--")+boundary+string("--\r\n"));
				
				while (bndPos==string::npos && bndPosEnd==string::npos)
				{
					n=socket->read(recvBuf, HttpStream::smallArbitraryReadLength);
					if (n>0)
						conts+=string(recvBuf, n);
					bndPos=conts.find(string("\r\n")+boundary+string("\r\n"));
					bndPosEnd=conts.find(string("--")+boundary+string("--\r\n"));
				}
				
				if (bndPosEnd!=string::npos)
				{
					if (debug)
						logLine(string("   bndPosEnd: ")+
								StrUtils::decToString(bndPosEnd));
					
					readData=conts.substr(0, bndPosEnd);

					// com que hem llegit entre boundaries pot ser que les dades acabin amb un \r\n
					// eliminem \r\n del final (que en principi no hi sera)...

					// NO PODEM ELIMINAR DEL FINAL UNS BYTES QUE SON DE LES
					// DADES PER MOLT QUE SIGUIN \r, \n O EL QUE SIGUIN!!!
//					if (readData[readData.length()-1]=='\n' &&
//						   readData[readData.length()-2]=='\r')
//						readData=readData.substr(0, readData.length()-2);
//					else if (readData[readData.length()-1]=='\n' ||  
//						   readData[readData.length()-1]=='\r')
//						readData=readData.substr(0, readData.length()-1);
					
					conts=conts.substr(bndPosEnd);
				}
				else
				{
					if (debug)
						logLine(string("   bndPos: ")+
								StrUtils::decToString(bndPos));

					readData=conts.substr(0, bndPos);

					// com que hem llegit entre boundaries pot ser que les dades acabin amb un \r\n
					// eliminem \r\n del final (que en principi no hi sera)...
					
					// NO PODEM ELIMINAR DEL FINAL UNS BYTES QUE SON DE LES
					// DADES PER MOLT QUE SIGUIN \r, \n O EL QUE SIGUIN!!!
//					if (readData[readData.length()-1]=='\n' &&
//						   readData[readData.length()-2]=='\r')
//						readData=readData.substr(0, readData.length()-2);
//					else if (readData[readData.length()-1]=='\n' ||  
//						   readData[readData.length()-1]=='\r')
//						readData=readData.substr(0, readData.length()-1);

					conts=conts.substr(bndPos);
				}
			
//				delete [] buf;

//				cout << "HttpStream: Ret: "
//					<< StrUtils::hexDump(string(readData.c_str(), 32))
//					<< "..."
//					<< StrUtils::hexDump(string(readData.c_str() + 
//						readData.length()-32, 32))
//					<< endl;
				return readData;
			}
			else if (cl!=header.end())
			{
				if (debug)
					logLine(string("   length"));
				// Normal amb content-length
//				char *buf=new char [1024];

				// ha de ser un numero :P
				unsigned int len=atoi((*cl).second.c_str());

				unsigned int readLen=0;

				while (readLen<len)
				{
					int n=socket->read(recvBuf, (len-readLen>HttpStream::recvBufMax?HttpStream::recvBufMax:len-readLen));

//					cout << "len: " << len << ", readLen: " << readLen << " errno: " << errno << endl;
//					cout << "(" << __FILE__ << ":" << __LINE__ << " readData.append(buf, " << n << ");" << endl;
					if (n>0)
						readData.append(recvBuf, n);//(len-readLen>1024?1024:len-readLen));

					readLen+=n;
					if(n==0)
					{
					//	exit(0);
						return readData;
					}
				}
//	cout<<"linia7:"<<len<<":"<<readLen<<":"<<buf<<endl;
				
//				delete [] buf;
				return readData;
			}
			else
			{
				if (debug)
					logLine(string("   unknown"));

//				cout << "arbitrary read" << endl;
				// sense info de length ni res de res...
				// Fem un best effort, fent una lectura "arbitraria"

//				char *buf=new char[HttpStream::arbitraryReadLength];
				int n=socket->read(recvBuf, HttpStream::bigArbitraryReadLength);

//				cout << "(" << __FILE__ << ":" << __LINE__ << " readData.append(buf, " << n << ");" << endl;
	//			cout << "(" << __FILE__ << ":" << __LINE__ << " readData:" << readData << " + "<<string(recvBuf,n)<<" n:"<<n << endl;
				if (n>0)
					readData.append(recvBuf, n);

//				delete [] buf;

				return readData;
			}
		}
		break;
	}

	return string("");
}

// retorna info sobre la request HTTP (un hash)
// - "requestType" -> "GET"/"POST"
// - variables -> "var_XXXXX"
// (capcaleres a header)
map<string,string> HttpStream::getRequestData()
{
	map<string,string> info;

	string hdrStr("");
	int i=0;

	try{
		while (hdrStr.length()<4 ||
			   hdrStr.substr(hdrStr.length()-4)!=string("\r\n\r\n"))
		{
			int hLen=socket->peek(recvBuf, HttpStream::recvBufMax);
			
			if (hLen>0)
			{
				hdrStr=string(recvBuf, hLen);
				
				// Trobem el primer final de capçaleres
				string::size_type hdrEnd=hdrStr.find("\r\n\r\n");
				
				if (hdrEnd!=string::npos)
				{
					hLen=hdrEnd+4;
					hdrStr=hdrStr.substr(0, hLen);
					// I traiem la capçalera del buffer :P
					socket->read(recvBuf, hLen);
				}
			}
		}
	}
	catch(SocketException &e)
	{
		cout<<"SocketException getRequestData 1: "<<socket->getAddr().toString()<<" : "<<i<<" - "<<e.getMsg()<<endl;
		throw e;
	}
	// Fem un parsat de la capcalera
//	cout<<" Stream  get hdr:"<<hdrStr<<endl;
	list <string> linies=StrUtils::split(hdrStr, "\n");
	string lastHeader=string("");
	list<string>::const_iterator itStr=linies.begin();

	string linia=(*itStr);
	// Primera linia, status...
	list <string> request=StrUtils::split(linia, " ");

	list<string>::const_iterator requestIt=request.begin();
	info[string("requestType")]=(*requestIt);
	requestIt++;
	info[string("requestString")]=(*requestIt);

	itStr++;

	for (;itStr!=linies.end(); itStr++)
	{
		string linia=(*itStr);
		
		if (linia[0]==' ' || linia[0]=='\t')
		{
			// Es continuacio de l'anterior
			header[lastHeader]+=string(" ")+StrUtils::trim(linia);
		}
		else if (linia!=string("\r\n") &&
				 linia.find(string(":"))!=string::npos)
		{
			list <string> components=StrUtils::split(linia, ":", 1);

			list<string>::const_iterator itHdrName=components.begin();
			itHdrName++;
			string contents=(*itHdrName);
			itHdrName--;

			lastHeader=(*itHdrName);

			header[lastHeader]=StrUtils::trim(contents);
		}
	}

	string readData("");

	map<string,string>::const_iterator te=header.find("Transfer-Encoding");
	map<string,string>::const_iterator cl=header.find("Content-Length");
	map<string,string>::const_iterator ct=header.find("Content-Type");
	if (te!=header.end() && (*te).second==string("chunked"))
	{
		// A llegir chunks
		string linia=StrUtils::readLine(socket);

		// ha de ser un numero :P
		unsigned int len=StrUtils::stringToHex(linia);

		while (len!=0)
		{
			unsigned int readLen=0;

			while (readLen<len)
			{
				int n=socket->read(recvBuf, (len-readLen>=HttpStream::recvBufMax?HttpStream::recvBufMax:len-readLen));
				if (n>0)
					readData.append(recvBuf, n);
				readLen+=n;
			}

			// linia en blanc de despres del chunk
			linia=StrUtils::readLine(socket);
			// seguent chunk
			linia=StrUtils::readLine(socket);
			// ha de ser un numero :P
			len=StrUtils::stringToHex(linia);
		}
	}
	else if (ct!=header.end() && (*ct).second.find(string("boundary"))!=string::npos)
	{
		int n=0;
		
		// amb boundary
		string::size_type bndpos=(*ct).second.find(string("boundary="));
		bndpos+=string("boundary=").length();

		string boundary=(*ct).second.substr(bndpos);
		
		int bLen=boundary.length();
		int bLast=bLen-1;

		// eliminem \r\n del final (que en principi no hi sera)...
		while (boundary[bLast]=='\n' ||
			   boundary[bLast]=='\r')
		{
			boundary=boundary.substr(0, bLast);
			bLast--;
		}

		if (boundary[bLast]==';')  // Nomes 1 ;)
		{
			boundary=boundary.substr(0, bLast);
			bLast--;
		}
		
		if (debug)
			logLine(string("      > ")+boundary);

		// anar llegint i trobant boundaries...

		int maxLen=HttpStream::recvBufMax>>3;
		if (maxLen<1024) maxLen=1024;

		n=socket->read(recvBuf, maxLen);
		if (n>0)
			conts+=string(recvBuf, n);

		string::size_type bndPosEnd=conts.find(string("--")+boundary+string("--\r\n"));
		string::size_type bndPos=conts.find(string("\r\n")+boundary+string("\r\n"));

//				cout << "bndPos " << bndPos << ", bndPosEnd " << bndPosEnd << endl;

		if (bndPosEnd!=string::npos && bndPos==string::npos)
		{
			info[string("data")]=readData;
			return info;
		}
		
		if (bndPos!=string::npos)
		{
//					cout << "Found " << boundary << " at " << bndPos << endl;
						
			string::size_type iniHdr=bndPos+(string("\r\n")+boundary+string("\r\n")).length();
			while (conts.substr(bndPos, 4)!=string("\r\n\r\n"))
			{
				bndPos++;
				
				while (bndPos>=conts.length()-4) // >= per curar-nos en salut
				{
					// bucle petat per longitud
					// Es un while per si algun read retornes 0 
					int len=conts.length()-4;
					n=socket->read(recvBuf, maxLen);
					if (n>0)
						conts+=string(recvBuf, n);

					conts=conts.substr(len);
					bndPos=0;
				}
			}

			conts=conts.substr(bndPos+4);
		}
		
		bndPos=conts.find(string("\r\n")+boundary+string("\r\n"));
		bndPosEnd=conts.find(string("--")+boundary+string("--\r\n"));
		
		while (bndPos==string::npos && bndPosEnd==string::npos)
		{
			n=socket->read(recvBuf, maxLen);
			if (n>0)
				conts+=string(recvBuf, n);
			bndPos=conts.find(string("\r\n")+boundary+string("\r\n"));
			bndPosEnd=conts.find(string("--")+boundary+string("--\r\n"));
		}
		
		if (bndPosEnd!=string::npos)
		{
			if (debug)
				logLine(string("   bndPosEnd: ")+
						StrUtils::decToString(bndPosEnd));
			
			readData=conts.substr(0, bndPosEnd);

			// com que hem llegit entre boundaries pot ser que les dades acabin amb un \r\n
			// eliminem \r\n del final (que en principi no hi sera)...
			if (readData[readData.length()-1]=='\n' &&
				   readData[readData.length()-2]=='\r')
				readData=readData.substr(0, readData.length()-2);
			else if (readData[readData.length()-1]=='\n' ||  
				   readData[readData.length()-1]=='\r')
				readData=readData.substr(0, readData.length()-1);
			
			conts=conts.substr(bndPosEnd);
		}
		else
		{
			if (debug)
				logLine(string("   bndPos: ")+
						StrUtils::decToString(bndPos));

			readData=conts.substr(0, bndPos);

			// com que hem llegit entre boundaries pot ser que les dades acabin amb un \r\n
			// eliminem \r\n del final (que en principi no hi sera)...
			if (readData[readData.length()-1]=='\n' &&
				   readData[readData.length()-2]=='\r')
				readData=readData.substr(0, readData.length()-2);
			else if (readData[readData.length()-1]=='\n' ||  
				   readData[readData.length()-1]=='\r')
				readData=readData.substr(0, readData.length()-1);

			conts=conts.substr(bndPos);
		}
	}
	else if (cl!=header.end())
	{
		// ha de ser un numero :P
		unsigned int len=atoi((*cl).second.c_str());

		unsigned int readLen=0;

		while (readLen<len)
		{
			int n=socket->read(recvBuf, (len-readLen>HttpStream::recvBufMax?HttpStream::recvBufMax:len-readLen));
			if (n>0)
				readData.append(recvBuf, n);
			readLen+=n;
		}
	}
	else
	{
		// sense info de length ni res de res...
		
	}

//	cout<<" Stream  get data:"<<readData<<endl;
	info[string("data")]=readData;
	
	return info;
}

// envia una resposta pel socket
void HttpStream::sendResponse(string response)
{
	try
	{
		int n=response.length();
		int w=0;

		while (w<n)
		{
			int wr=socket->write((void *)(response.c_str()+w), response.length()-w);
			w+=wr;
		}
	}
	catch(SocketException &se)
	{
		socket->closeSocket();
	}
}

// activa o desactiva el mode debug
void HttpStream::setDebug(bool debug)
{
	this->debug=debug;
}
	

