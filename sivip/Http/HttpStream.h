#ifndef SIRIUS_BASE_HTTP_HTTPSTREAM_H_
#define SIRIUS_BASE_HTTP_HTTPSTREAM_H_


#include <Utils/debugNew.h>
#include <string>
#include <list>
#include <map>
#include <Sockets/SocketTCP.h>
#include <Utils/Log.h>

using namespace std;

class HttpStream : public Log
{
public:
	// 16ks de buffer de recepcio... sera suficient oi? ;)
	const static int recvBufMax=16384;
	const static int maxRetries=3;

	SocketTCP *socket;
protected:
	enum receiveState {RCV_STATE_INITIAL=0x0,
			RCV_STATE_HEADER_RECVD=0x1, 
			RCV_STATE_RECV_W_BOUNDARY=0x10000,
			RCV_STATE_RECV_CHUNKED=0x20000,
	};
	
	string version;
	receiveState estat;
	
	int requestStatus;
	map <string,string> requestStatusDescription;
	
	bool debug;
	
	// Variables per getNextInformationChunk i getRequestData que necessiten
	// conservar l'estat
	// 
	// conts: Es posa a string("") a la construccio de l'objecte
	string conts;
	
	char *recvBuf; //[HttpStream::recvBufMax];
	
	void addStatusDesc(string status, string msg, bool replace=false);
		
public:
	map <string, string> header;
	static const int bigArbitraryReadLength=16384;
	static const int midArbitraryReadLength=4096;
	static const int smallArbitraryReadLength=1024;
	static const int minReadLength=1024;

	HttpStream(bool debugMode=false);
	virtual ~HttpStream(void);
	
	// retorna un string amb la descripcio d'un codi de resposta HTTP
	string getRequestStatusDesc(int status);

	// estableix codi de resposta HTTP
	void setRequestStatus(int status);
	
	// obte codi de resposta HTTP
	int getRequestStatus();
	
	// codifica un text en format per parametre http
	static string urlEncode(string str);
	// descodifica una string codificada en format url
	static string urlDecode(string str);
	// retorna les parts d'una url. ('proto', 'host', 'port', 'path', 'params')
	static map<string,string> parseURL(string url, string defaultPort=string("80"));

	// Estableix el socket de recepcio de dades
	virtual void setReceiveSocket(SocketTCP *stcp);

	// munta una capcalera de peticio http
	virtual string createRequestHeader(string url, 
									   map<string,string> params,
									   string httpVersion,
									   map<string,string> extraHeaders);
	
	// munta una capcalera de resposta HTTP
	virtual string createResponseHeader(map<string,string> extraHeaders);
	
	// retorna un tros d'informacio:
	// - Si hi ha boundaries en la resposta, el tros corresponent a un 
	//   boundary
	// - Si no hi ha boundaries, tot el reply
	string getNextInformationChunk(string **partHdr=NULL);
	
	// retorna info sobre la request HTTP (un hash)
	// - "requestType" -> "GET"/"POST"
	// - variables -> "var_XXXXX"
	// (capcaleres a header)
	map<string,string> getRequestData();

	// envia una resposta pel socket
	virtual void sendResponse(string response);
	
	// activa o desactiva el mode debug
	void setDebug(bool debug);
};
#endif
