#include <Http/HttpStreamGET.h>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>

using namespace std;

HttpStreamGET::HttpStreamGET(void)
{
}

HttpStreamGET::~HttpStreamGET(void)
{
}

// munta una peticio http
string HttpStreamGET::createRequestHeader(string url, 
										  map<string,string> params,
										  string httpVersion,
										  map<string,string> extraHeaders)
{
	string req=string("GET ");
	
	map <string, string> parsedUrl=parseURL(url);
	
	if (parsedUrl.find("proto")==parsedUrl.end())
	{
		return string("");
	}
	
	req+=parsedUrl["path"];
	
	if (parsedUrl.find("params")!=parsedUrl.end())
	{
		// tenim params a la URL
		req+=parsedUrl["params"];
	}
	else if (params.size()>0)
	{
		// No tenim params a la URL pero si que en tenim a params
		req+=string("?");
	}
	
	// Falta patejada de params...
	map<string, string>::const_iterator parIt;
	
	for (parIt=params.begin(); parIt!=params.end(); parIt++)
	{
		req+=string("&")+(*parIt).first+string("=")+urlEncode((*parIt).second);
	}
		
	req+=string(" HTTP/")+httpVersion+("\r\n");
	
	// Aqui van les capcalleres addicionals que rebem...
	map<string, string>::const_iterator hdrIt;
	
	for (hdrIt=extraHeaders.begin(); hdrIt!=extraHeaders.end(); hdrIt++)
	{
		req+=(*hdrIt).first+string(": ")+(*hdrIt).second+string("\r\n");
	}
	
	if (httpVersion.compare(string("1.1"))==0)
	{
		req+=string("Host: ")+parsedUrl["host"];
		req+=string("\r\n\r\n");
	}
	else
		req+=string("\r\n");
	
	return req;
}


