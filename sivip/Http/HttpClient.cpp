#include <Http/HttpClient.h>
#include <Utils/StrUtils.h>
#include <stdlib.h>
#include <Utils/debugStackTrace.h>
#include <Sockets/SocketException.h>
#include <string.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <iostream>
using namespace std;

HttpClient::HttpClient(string url, string httpVer) : URL(url), 
	httpVersion(httpVer), socket(NULL),
	socketTimeout(HttpClient::defaultSocketTimeout)
{
	
	
}

HttpClient::~HttpClient()
{
}

// Sets, Gets i Dels

// URL
string HttpClient::getURL()
{
	return URL;
}

void HttpClient::setURL(string url)
{
	URL=url;
}

// Http version
string HttpClient::getHttpVersion()
{
	return httpVersion;
}

void HttpClient::setHttpVersion(string httpVer)
{
	httpVersion=httpVer;
}

// params
void HttpClient::setParam(string param, string value)
{
	params[param]=value;
}

string HttpClient::getParam(string param)
{
	map <string, string>::iterator p=params.find(param);
	

	if (p!=params.end())
		return (*p).second;
	else
		return string("");
}

void HttpClient::delParam(string param)
{
	map <string, string>::iterator p=params.find(param);
	
	if (p!=params.end())
		params[param].erase();
}

// headers
void HttpClient::setHeader(string header, string value)
{
	extraHeaders[header]=value;
}

string HttpClient::getHeader(string header)
{
	map <string, string>::iterator p=extraHeaders.find(header);
	
	if (p!=extraHeaders.end())
		return (*p).second;
	else
		return string("");
}

void HttpClient::delHeader(string header)
{
	map <string, string>::iterator p=extraHeaders.find(header);
	
	if (p!=extraHeaders.end())
		extraHeaders[header].erase();
}

void HttpClient::setConnectionTimeout(dword ms)
{
	this->socketTimeout=ms;
	if (this->socket!=NULL)
		this->socket->setTimeout(ms);
}

dword HttpClient::getConnectionTimeout()
{
	if (this->socket!=NULL)
		this->socketTimeout=this->socket->getTimeout();
	
	return this->socketTimeout;
}

HttpStream *HttpClient::sendRequest(string type)
{
	HttpStream *stream;
	string hdr;
	if (type==string("GET"))
	{
		HttpStreamGET *httpGet=new HttpStreamGET();
		hdr=httpGet->createRequestHeader(URL, params, httpVersion, extraHeaders);
		stream=(HttpStream*)httpGet;
	}
	else if (type==string("POST"))
	{
		HttpStreamPOST *httpPost=new HttpStreamPOST();
		hdr=httpPost->createRequestHeader(URL, params, httpVersion, extraHeaders);
		stream=(HttpStream*)httpPost;
	}
	else
	{
		return NULL;
	}
	
	if (hdr==string(""))
	{
		return NULL;
	}

	map <string, string> parsed=	HttpStream::parseURL(URL);

	IP ip(parsed["host"]);
	int port=atoi(parsed["port"].c_str());

	Address addr(ip, port);

	socket=new SocketTCP(addr);
	
	char *b=NULL;
	try
	{
		this->setConnectionTimeout(this->socketTimeout);
		this->socket->establish();
		b=new char[hdr.length()];
		memmove(b, hdr.c_str(), hdr.length());
		this->socket->write(b, hdr.length());
		delete [] b;
	}
	catch (SocketException &se)
	{
		delete socket;
		socket=NULL;
		if(b != NULL)
			delete[] b;
		throw se;
	}
	
	stream->setReceiveSocket(this->socket);
	//stream->setReceiveSocket(socket);
	//delete socket;

	return stream;
}


void HttpClient::CloseConnection()
{
	this->socket->closeSocket();
}


