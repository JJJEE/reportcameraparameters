#include <Http/HttpStreamPOST.h>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
//#include <iostream>
using namespace std;

HttpStreamPOST::HttpStreamPOST(void)
{
}

HttpStreamPOST::~HttpStreamPOST(void)
{
}

// munta una peticio http
string HttpStreamPOST::createRequestHeader(string url, 
										   map<string,string> params,
										   string httpVersion,
										   map<string,string> extraHeaders)
{
	string req=string("POST ");
	
	map <string, string> parsedUrl=parseURL(url);
	
	if (parsedUrl.find("proto")==parsedUrl.end())
	{
		return string("");
	}
	
	req+=parsedUrl["path"];
	
	bool paramsPresent=false;
	if (parsedUrl.find("params")!=parsedUrl.end())
	{
		// tenim params a la URL
		paramsPresent=true;
	}
	else if (params.size()>0)
	{
		// No tenim params a la URL pero si que en tenim a params
		paramsPresent=true;
	}
	
	string boundary=string("--HttpStreamPOST_09823665290540567827894705626");
	
	req+=string(" HTTP/")+httpVersion+("\r\n");

	// Aqui van les capcalleres addicionals que rebem...
	map<string, string>::const_iterator hdrIt;
	
	for (hdrIt=extraHeaders.begin(); hdrIt!=extraHeaders.end(); hdrIt++)
	{
		req+=(*hdrIt).first+string(": ")+(*hdrIt).second+string("\r\n");
	}
	
	if (httpVersion.compare(string("1.1"))==0)
	{
		req+=string("Host: ")+parsedUrl["host"];
		req+=string("\r\n");
	}
	
	if (paramsPresent)
		req+=string("Content-Type: multipart/form-data;\r\n\tboundary=")+
		boundary+string("\r\n");

	string body=string("");
	// Falta patejada de params...
	if (paramsPresent)
	{
		map<string, string>::const_iterator parIt;
		
		for (parIt=params.begin(); parIt!=params.end(); parIt++)
		{
			body+=string("--")+boundary+string("\r\n")+
				string("Content-Type: text/plain\r\n")+
				string("Content-Length: ")+
				StrUtils::decToString((int)(*parIt).second.length())+string("\r\n")+
				string("Content-Disposition: form-data; name=\"")+(*parIt).first+string("\"\r\n\r\n")+
				(*parIt).second+string("\r\n");
		}
		
		body+=string("--")+boundary+string("--\r\n\r\n");
	}
	
	req+=string("Content-Length: ")+StrUtils::decToString((int)body.length())+
		string("\r\n");

	req+=string("\r\n");

	req+=body;
	
	return req;
}

