#include <Http/HttpServer.h>
#include <Utils/StrUtils.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <Sockets/SocketException.h>

using namespace std;

void* HttpServer::execute(int i, void *arg)
{
	if(arg==NULL)
		serveThread();
	else
	{
		servInfo* a=(servInfo*)arg ;
		a->func(a->stream, a->params);
		//exitThread(0);
		
		// **** TODO **** DELETES!!!!
		// delete a->stream;
		// delete a;
	}
	return 0;
}

void HttpServer::serveThread()
{
	SocketTCP *s;
	
	try
	{
		while ((s=socket->establish())!=NULL && !getStopServing())
		{
			HttpStream *stream=new HttpStream();
			servInfo *inf=new servInfo(connThreadRoutine, stream, params);

			
			stream->setReceiveSocket(s);
			
			
			
			dword thId;
			int i=start((void*)inf);
			join(i);
			
		
			detach(i);

		
			if (inf != 0) {
				cout << "ServInfo borrado" << endl;
				delete inf;
			}
		//	if (stream != 0)
		//		delete stream;

			
			
		}
	}
	catch(Exception &e)
	{
		cout << "[Serving Thread] " << e.getClass() << ": " << e.getMsg() << endl;
	}

	cout << "salimos del while" <<endl;
	
//	exitThread(0);
}

void  HttpServer::defaultConnThreadRoutine(HttpStream *stream, void* params)
{
	//HttpStream *stream=(HttpStream*)httpStream;
	
	map <string, string> req=stream->getRequestData();
	
	map<string,string>::const_iterator it=req.begin();
	
	for (; it!=req.end(); it++)
		cout << (*it).first << ": " << (*it).second << endl;

	string content=string("<html><body>Default Connection Thread response</body></html>");
	
	map <string, string> hdrs;
	
	hdrs[string("Content-Type")]=string("text/html;");
	hdrs[string("Content-Length")]=StrUtils::decToString(content.length());

	string response=stream->createResponseHeader(hdrs)+
		content;
	
	stream->sendResponse(response);
	
//	exitThread(0);
}

HttpServer::HttpServer(Address addr) : stop(false)
{
	socket=new SocketTCP(addr, SOCK_SERVE);
}

HttpServer::HttpServer() : stop(false), socket(NULL)
{}

void HttpServer::setAddress(Address addr)
{
	if(socket!=NULL)
		delete socket;
	socket=new SocketTCP(addr, SOCK_SERVE);
}

Address HttpServer::getAddress()
{
	if(socket==NULL)
	{
		return Address();
	}
	
	return socket->getAddr();
}


HttpServer::~HttpServer()
{
	joinAll();
	if(socket!=NULL)
		delete socket;
}

void HttpServer::serve( void(*connectionAttentionThread)(HttpStream*, void*), void* params)
{ 
	connThreadRoutine=connectionAttentionThread;
	this->params=params;

	start(NULL);
}

bool HttpServer::getStopServing()
{
	return stop;
}

void HttpServer::stopServing()
{
	stop=true;
}

