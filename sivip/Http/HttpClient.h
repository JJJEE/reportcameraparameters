#ifndef SIRIUS_BASE_HTTP_HTTPCLIENT_H_
#define SIRIUS_BASE_HTTP_HTTPCLIENT_H_

#include <Utils/debugNew.h>
#include <string>
#include <Http/HttpStream.h>
#include <Http/HttpStreamGET.h>
#include <Http/HttpStreamPOST.h>
#include <Sockets/IP.h>
#include <Sockets/Address.h>
#include <Sockets/SocketTCP.h>
#include <Utils/Exception.h>

class HttpClient
{
public:
	const static dword defaultSocketTimeout=10000;

protected:
	dword socketTimeout;
	string URL;
	map <string, string> params;
	map <string, string> extraHeaders;
	string httpVersion;

	SocketTCP *socket;
	
public:
	HttpClient(string url=string(""), string httpVer=string("1.1"));
	~HttpClient();
	
	// Sets, Gets i Dels
	// URL
	string getURL();
	void setURL(string url);
	// http version
	string getHttpVersion();
	void setHttpVersion(string httpVer);
	// params
	void setParam(string param, string value);
	string getParam(string param);
	void delParam(string param);
	// headers
	void setHeader(string header, string value);
	string getHeader(string header);
	void delHeader(string header);
	
	// Actuacions sobre el socket
	void setConnectionTimeout(dword ms);
	dword getConnectionTimeout();
	void CloseConnection();
	
	// enviament de la request preparada. Retorna un HttpStream
	HttpStream *sendRequest(string type=string("GET"));
};
#endif
