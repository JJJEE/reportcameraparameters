#ifndef SIRIUS_BASE_HTTP_HTTPSTREAMPOST_H_
#define SIRIUS_BASE_HTTP_HTTPSTREAMPOST_H_

#include <Utils/debugNew.h>
#include <Http/HttpStream.h>

class HttpStreamPOST :
	public HttpStream
{
public:
	HttpStreamPOST(void);
	virtual ~HttpStreamPOST(void);

	// munta una capcalera de peticio http
	virtual string createRequestHeader(string url, 
									   map<string,string> params,
									   string httpVersion,
									   map<string,string> extraHeaders);
};
#endif
