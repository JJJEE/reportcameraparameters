#ifndef SIRIUS_BASE_HTTP_HTTPSERVER_H_
#define SIRIUS_BASE_HTTP_HTTPSERVER_H_

#include <Utils/debugNew.h>
#include <string>
#include <Http/HttpStream.h>
#include <Http/HttpStreamGET.h>
#include <Http/HttpStreamPOST.h>
#include <Sockets/IP.h>
#include <Sockets/Address.h>
#include <Sockets/SocketTCP.h>
#include <Utils/Exception.h>
#include <Utils/Types.h>
#include <Threads/Thread.h>

class HttpServer: public Thread
{
	private:
		
		class servInfo{
			public:
				void(*func)(HttpStream *, void*);
				HttpStream *stream;
				void *params;
			
				servInfo( void(*f)(HttpStream*, void*), HttpStream *s, void* p):func(f),stream(s), params(p){}
		};

protected:
	SocketTCP *socket;
	HttpStream *lastRequest;
	bool stop;
	void *params;

	void(*connThreadRoutine)(HttpStream*, void*);

	void serveThread();
	void* execute(int i, void *args);
	HttpServer();
	void setAddress(Address a);
	
public:
	void defaultConnThreadRoutine(HttpStream *httpStream, void* params);

	HttpServer(Address addr);
	~HttpServer();

	Address getAddress();
	void* execute(int);
	void serve(void(*connThreadRoutine)(HttpStream*, void*), void* params=NULL);
	bool getStopServing();
	void stopServing();
};

#endif
