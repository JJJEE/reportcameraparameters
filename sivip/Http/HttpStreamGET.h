#ifndef SIRIUS_BASE_HTTP_HTTPSTREAMGET_H_
#define SIRIUS_BASE_HTTP_HTTPSTREAMGET_H_

#include <Utils/debugNew.h>
#include <Http/HttpStream.h>

class HttpStreamGET :
	public HttpStream
{
public:
	HttpStreamGET(void);
	virtual ~HttpStreamGET(void);

	// munta una capcalera de peticio http
	virtual string createRequestHeader(string url, 
									   map<string,string> params,
									   string httpVersion,
									   map<string,string> extraHeaders);
};
#endif
