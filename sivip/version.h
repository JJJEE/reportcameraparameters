#ifndef __VERSION_H__
#define __VERSION_H__

#include <string>

using namespace std;

static string siriusVersion("1.0-rc1");
static string siriusBuild("20140925");
static string siriusRelease=string("SIRIUS v")+siriusVersion+string(" build ")+siriusBuild;

#endif
