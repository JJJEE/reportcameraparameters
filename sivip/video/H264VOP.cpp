#include <video/H264VOP.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

// #define H264VOP_VERBOSE

H264VOP::H264VOP() : vopFull(false), 
							type(0)
{
}


H264VOP::H264VOP( dword type ) : 	vopFull(false), 
												type(type)
{
}

H264VOP::~H264VOP()
{
	list<H264VOPSlice*>::iterator lIt = voplist.begin();
	for ( ; lIt != voplist.end(); ++lIt )
	{
		delete (*lIt);
	}
	
	voplist.clear();
}

dword H264VOP::getVOPStart(	void *data, 
							dword len, 
							dword off,
							dword *codeType )
{
	dword vopStartCode = 0;
	byte *b = (byte*)data;
	
#ifdef H264VOP_VERBOSE
	RTSP::lerr 	<< "H264VOP::getVOPStart(" << data << ", " << len 
					<< ", " << off << ", " << (void*)codeType << ");" << endl;
#endif

	// Com a minim hem d'haver llegit 4 bytes!!
	for ( dword l = off; l < off + 3  && l < len; ++l )
	{
		vopStartCode = (vopStartCode << 8) | (dword)b[l];
	}
	
	for ( dword l = off + 3; l < len; ++l)
	{
		// El 4t el llegim aqui :)
		vopStartCode = (vopStartCode << 8) | (dword)b[l];
		
		switch (vopStartCode)
		{
			case VOP_06:
			case VOP_21:
			case VOP_25:
			case VOP_27:
			case VOP_28:
				if ( codeType != NULL )
					*codeType = vopStartCode;
				return l-3;
		}
	}
	
	return 0xffffffff;
}

void H264VOP::setVOPFull( bool full )
{
	this->vopFull = full;
}

bool H264VOP::isVOPFull()
{
	return this->vopFull;
}

void *H264VOP::getVOPData()
{
	dword vopSize = this->getVOPSize();

	byte *vop = new byte[vopSize];
	
	if (vop==NULL)
		throw NotEnoughMemoryException("Not enough memory to allocate VOP buffer");

	dword offset=0;
	list<H264VOPSlice*>::iterator lIt=voplist.begin();
	for (; lIt!=voplist.end(); ++lIt )
	{
		memmove(&vop[offset], (*lIt)->getSliceData(), (*lIt)->getSliceSize());
		offset+=(*lIt)->getSliceSize();
	}

	return vop;
}

dword H264VOP::getVOPSize()
{
	dword vopSize = 0;
	
	list<H264VOPSlice*>::iterator lIt = voplist.begin();
	for (; lIt!=voplist.end(); ++lIt )
		vopSize+=(*lIt)->getSliceSize();
		
	return vopSize;
}

dword H264VOP::getType()
{
	return this->type;
}

bool H264VOP::isKeyFrame()
{
	if ( this->getType() == H264VOP::VOP_25 )
	{
		byte key 	 	= 0;
		dword vopOff 	= 0;
		dword vopRead 	= 0;
		list<H264VOPSlice*>::iterator lIt = voplist.begin();
		for (; lIt!=voplist.end(); ++lIt )
		{
			vopRead+=(*lIt)->getSliceSize();

			if (vopRead < 5)
				vopOff=vopRead;
			else
			{
				byte *d=(byte*)(*lIt)->getSliceData();
				key=d[5-vopOff];
				break;
			}
		}
	
		return ((key&0xc0)==0);
	}
	
	return false;
}

void H264VOP::addSlice( H264VOPSlice *slice, bool last )
{
	voplist.push_back(slice);
	this->vopFull=last;
}
