#ifndef __MARINA_EYE_CAM__BASE__VIDEO__H264CHUNKPROCESSOR_H
#define __MARINA_EYE_CAM__BASE__VIDEO__H264CHUNKPROCESSOR_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <video/H264VOP.h>
#include <string>

using namespace std;

class H264ChunkProcessor
{
protected:
	list<H264VOP*>availableVOPs;
	H264VOP *currentVOP;

public:
	H264ChunkProcessor();
	~H264ChunkProcessor();

	void processChunk(  void *data, dword len   );
	bool isFrameAvailable();
    H264VOP *getFrame();
};

#endif
