#include <video/MPEG4ChunkProcessor.h>
#include <Utils/StrUtils.h>
#include <Exceptions/InvalidStateException.h>

#define MPEG4CHUNKPROCESSOR_VERBOSE
// #define MPEG4CHUNKPROCESSOR_EXTRAVERBOSE

MPEG4ChunkProcessor::MPEG4ChunkProcessor() : currentVOP(NULL)
{
}

MPEG4ChunkProcessor::~MPEG4ChunkProcessor()
{
	if (currentVOP!=NULL)
		delete currentVOP;
		
	list<MPEG4VOP*>::iterator lIt=availableVOPs.begin();
	for (; lIt!=availableVOPs.end(); lIt++)
		delete (*lIt);

	availableVOPs.clear();
}

void MPEG4ChunkProcessor::processChunk(void *data, dword len)
{
	dword codeType=0;		// Tipus de VOP
	dword vs=0;				// VOP Start, index del primer VOP des de d
	byte *d=(byte*)data;	// Inici de les dades que es tracten en cada moment
//	dword done=0;			// Compta el que portem fet des de data
	int remaining=(int)len;		// Compta quant queda fins al final, des de d
	while (remaining>0)
	{
		// Trobem el primer VOP del chunk
		vs=MPEG4VOP::getVOPStart(d, remaining-vs, vs, &codeType);
				
		switch (vs)
		{
			case 0:
				// Inici, tancar el vop en curs i iniciar-ne un de nou
				if (this->currentVOP!=NULL)
				{
					this->currentVOP->setVOPFull(true);
					this->availableVOPs.push_back(this->currentVOP);
				}

				this->currentVOP=new MPEG4VOP(codeType);
				// Tenim el done a 0 i busquem el seguent
				// No incrementem d
				vs=4;	// Saltem la cap�alera del VOP a la propera cerca
				
				// NO GESTIONEM que nomes quedessin 4 bytes al buffer,
				// i per tant es perdria la cap�alera del VOP. Ho fem perque
				// no te sentit. Si passes, en principi es saltaria el VOP.
				break;

			case 0xffffffff:
				// No s'ha trobat cap inici de chunk, hauriem d'afegir
				// al VOP en curs
				if (this->currentVOP!=NULL)
				{
					MPEG4VOPSlice *slice=new MPEG4VOPSlice(d, remaining, true);						this->currentVOP->addSlice(slice);
				}
				// Si no hi ha VOP, saltem les dades!!

				// Final de dades
				remaining=0;
				break;

			default:
				// Un offset pel mig, copiar al vop en curs
				// A la seguent iteracio ja s'obrira un VOP nou per "case 0"
				if (this->currentVOP!=NULL)
				{
					MPEG4VOPSlice *slice=new MPEG4VOPSlice(d, vs, true);
					this->currentVOP->addSlice(slice, true);
					this->availableVOPs.push_back(this->currentVOP);
					this->currentVOP=NULL;
				}

				// Si no tenim VOP, ens saltem les dades!!
//				done += vs;
				remaining -= vs;
				d += vs;
				vs=0;
				break;
		}
	}
}

bool MPEG4ChunkProcessor::isFrameAvailable()
{
	return !availableVOPs.empty();
}

MPEG4VOP *MPEG4ChunkProcessor::getFrame()
{
	MPEG4VOP *avail=availableVOPs.front();
	availableVOPs.pop_front();
	
	return avail;
}
