#ifndef __MARINA_EYE_CAM__BASE__VIDEO__MPEG4VOPSLICE_H
#define __MARINA_EYE_CAM__BASE__VIDEO__MPEG4VOPSLICE_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <list>

using namespace std;

class MPEG4VOPSlice
{
protected:
	void *data;
	dword len;
	bool ownCopy;
	
public:
    MPEG4VOPSlice(void *data, dword len, bool copy=false);
    ~MPEG4VOPSlice();

	void *getSliceData();
	dword getSliceSize();
};

#endif
