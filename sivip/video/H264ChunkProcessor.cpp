#include <video/H264ChunkProcessor.h>
#include <Utils/StrUtils.h>
#include <Exceptions/InvalidStateException.h>
#include <cstdio>
#define H264CHUNKPROCESSOR_VERBOSE
// #define H264CHUNKPROCESSOR_EXTRAVERBOSE

H264ChunkProcessor::H264ChunkProcessor() : currentVOP(NULL)
{
}

H264ChunkProcessor::~H264ChunkProcessor()
{
	if (currentVOP!=NULL)
		delete currentVOP;
		
	list<H264VOP*>::iterator lIt=availableVOPs.begin();
	for (; lIt!=availableVOPs.end(); ++lIt )
		delete (*lIt);

	availableVOPs.clear();
}

void H264ChunkProcessor::processChunk( void *data, dword len )
{
	dword codeType=0;					// Tipos de VOP
	dword vs = 0;						// VOP Start, index del primer VOP des de d
	byte *d = (byte*)data;			// Inici de les dades que es tracten en 
											// cada moment

	int remaining = (int)len;		// Compta quant queda fins al final, des de d
	while (remaining>0)
	{
		// Buscamos el primer VOP del chunk
		vs = H264VOP::getVOPStart(d, remaining-vs, vs, &codeType);
				
		switch (vs)
		{
			case 0:
				// Inicio, se cierra el vop en curso y se inciia uno nuevo
				if (this->currentVOP!=NULL)
				{
					this->currentVOP->setVOPFull(true);
					this->availableVOPs.push_back(this->currentVOP);
				}

				this->currentVOP = new H264VOP(codeType);
				// Tenim el done a 0 i busquem el seguent
				// No incrementem d
				vs = 4;	// Saltem la cap�alera del VOP a la propera cerca
				
				// NO GESTIONEM que nomes quedessin 4 bytes al buffer,
				// i per tant es perdria la cap�alera del VOP. Ho fem perque
				// no te sentit. Si passes, en principi es saltaria el VOP.
				break;

			case 0xffffffff:
				// No s'ha trobat cap inici de chunk, hauriem d'afegir
				// al VOP en curs
				if (this->currentVOP!=NULL)
				{
					H264VOPSlice *slice = new H264VOPSlice( d, remaining, true );
					this->currentVOP->addSlice( slice );
				}
				// Si no hi ha VOP, saltem les dades!!

				// Final de los datos
				remaining = 0;
				break;

			default:
				// Un offset pel mig, copiar al vop en curs
				// A la seguent iteracio ja s'obrira un VOP nou per "case 0"
				if ( this->currentVOP != NULL )
				{
					H264VOPSlice *slice=new H264VOPSlice(d, vs, true);
					this->currentVOP->addSlice(slice, true);
					this->availableVOPs.push_back(this->currentVOP);
					this->currentVOP=NULL;
				}

				// Si no tenim VOP, ens saltem les dades!!
//				done += vs;
				remaining -= vs;
				d += vs;
				vs=0;
				break;
		}
	}
}

bool H264ChunkProcessor::isFrameAvailable()
{
	return !availableVOPs.empty();
}

H264VOP *H264ChunkProcessor::getFrame()
{
	H264VOP *avail=availableVOPs.front();
	availableVOPs.pop_front();
	
	return avail;
}
