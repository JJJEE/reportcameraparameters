#ifndef __MARINA_EYE_CAM__BASE__VIDEO__MPEG4FRAMEQUEUE_H
#define __MARINA_EYE_CAM__BASE__VIDEO__MPEG4FRAMEQUEUE_H

#include <Utils/Types.h>
#include <video/Frame.h>
#include <video/MPEG4ChunkProcessor.h>
#include <video/FrameQueue.h>
#include <stdio.h>
#include <string.h>         
#include <queue>      
    

class MPEG4FrameQueue: public FrameQueue
{
private:
	MPEG4ChunkProcessor*	m_VOPChunk;
	uint8_t*				m_currentFrame;
	int  					m_currentFrameSize;
	dword					m_lastVOPType;
	uint32_t				m_numProcessFrames;
	bool					m_init;		// Flag de inicialización
	bool					m_isKey;	// Flag de comienzo de keyFrame

	void add2CurrentFrame(void *data, dword len);

public:
	MPEG4FrameQueue();
	MPEG4FrameQueue(uint32_t size);
	~MPEG4FrameQueue();

	virtual void addVOPChunk(void *data, dword len);

};

#endif
