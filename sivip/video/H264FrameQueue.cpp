#include <Utils/Types.h>
#include <video/H264VOP.h>
#include <stdio.h>
#include <string.h>         
#include <queue>  
#include <video/H264FrameQueue.h>


H264FrameQueue::H264FrameQueue()
{
	printf("H264FrameQueue::H264FrameQueue()\n");
	m_currentFrame 		= 0;
	m_currentFrameSize 	= 0;
	m_init				= false;
	m_numProcessFrames	= 0;
	m_VOPChunk			= new H264ChunkProcessor();
}

H264FrameQueue::H264FrameQueue(uint32_t size): FrameQueue(size)
{
	m_currentFrame 		= 0;
	m_currentFrameSize 	= 0;
	m_init				= false;
	m_numProcessFrames	= 0;
	m_VOPChunk			= new H264ChunkProcessor();
}

H264FrameQueue::~H264FrameQueue()
{
	printf("H264FrameQueue::~H264FrameQueue()\n");
	if( m_currentFrame != NULL)
		delete[] m_currentFrame;

	if( m_VOPChunk != NULL)
		delete m_VOPChunk;
}

void H264FrameQueue::addVOPChunk(void *data, dword len)
{	
	//printf("H264FrameQueue::addVOPChunk()\n");
	H264VOP* vop;
	uint8_t* vopData;
	int 	 vopLen;
					
	m_VOPChunk->processChunk( data, len );
	
	while( m_VOPChunk->isFrameAvailable() )
	{
		vop 		= m_VOPChunk->getFrame();
		dword type	= vop->getType();
		vopData 	= (uint8_t *) vop->getVOPData();
		vopLen 		= (int) vop->getVOPSize();
		
		if( ! m_init )
		{
			// Esperamos que llegue frame principal y comenzamos a añadir bytes
			if( type == H264VOP::VOP_27 )
			{
				//printf("Primer Frame\n");
				m_init 			= true;
				m_isKey 		= true;
				m_lastVOPType 	= type;
				add2CurrentFrame(vopData, vopLen);
			}
		}
		else
		{
			if(type == H264VOP::VOP_27)
			{
				//printf("----------------------->KEY FRAME\n");
				// Comienzo de siguiente frame
				m_isKey 		= true;
				add2CurrentFrame(vopData, vopLen);	
			}
			if(type == H264VOP::VOP_06 )
			{
				if( m_lastVOPType == H264VOP::VOP_25 || 
					m_lastVOPType == H264VOP::VOP_21 )
				{
					//printf("-----------------------> NUEVO FRAME : %d \n", m_currentFrameSize);				
					++m_numProcessFrames;

					// Frame Completo
					Frame *thisFrame = new Frame( (void *) m_currentFrame, m_currentFrameSize, m_numProcessFrames, m_isKey );
					m_isKey 		= false;
					setFrame(thisFrame);


					m_currentFrameSize = 0;

					// Comienzo de siguiente frame
					add2CurrentFrame(vopData, vopLen);
				}
			}
			else
			{
				// Mismo Frame, añadimos bytes
				add2CurrentFrame(vopData, vopLen);
			}

			m_lastVOPType 	= type;

			delete[] vopData;
			vopData = NULL;
			delete vop;
			vop = NULL;
		}
	}
}

void H264FrameQueue::add2CurrentFrame(void *data, dword len)
{
	//printf("frameSize %d\n", m_currentFrameSize);
	if( m_currentFrameSize == 0)
	{
		m_currentFrame = new uint8_t[len];
		memcpy( m_currentFrame, data, len);
	}
	else
	{
		byte *copy = new uint8_t[ m_currentFrameSize ];
		memcpy( copy, m_currentFrame, m_currentFrameSize);
		delete[] m_currentFrame;

		m_currentFrame = new uint8_t[ m_currentFrameSize + len ];

		memcpy( m_currentFrame, copy, m_currentFrameSize);
		delete[] copy;
		memcpy( &m_currentFrame[m_currentFrameSize] , data, len);
	}
		m_currentFrameSize += len;
}
