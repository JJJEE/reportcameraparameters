#include <video/Frame.h> 
#include <time.h>
#include <sys/time.h>

Frame::Frame(): m_data(0), m_len(0), m_isKey(false), m_timestamp(0)
{

}

Frame::Frame(void* data, uint32_t len, uint64_t timestamp, bool iskey)
{
	m_data  	= (uint8_t *)data;	
	m_len 		= len;
	m_isKey 	= iskey;

	if(timestamp == 0)
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		m_timestamp 	= now.tv_sec*1000 + now.tv_usec/1000;
	}
	else
	{
		m_timestamp 	= timestamp;
	}
}

Frame::Frame(const Frame &copyFrame): m_data(copyFrame.m_data), m_len(copyFrame.m_len), m_isKey(copyFrame.m_isKey), m_timestamp(copyFrame.m_timestamp)
{

}

Frame::~Frame()
{
	if( m_data != 0)
		delete[] m_data;

	m_data 	= 0;
	m_len 	= 0;
}

void Frame::clearFrame()
{
	if( m_data != 0)
		delete[] m_data;
	
	m_data 	= 0;
	m_len 	= 0;
	m_timestamp 	= 0;
}

void Frame::setFrame(void* data, uint32_t len, uint64_t timestamp, bool iskey)
{
	clearFrame();

	m_data  	= (uint8_t *)data;
	m_len 		= len;
	m_isKey 	= iskey;

	if(timestamp == 0)
	{
		struct timeval now;
		gettimeofday(&now, NULL);
		m_timestamp 	= now.tv_sec*1000 + now.tv_usec/1000;
	}
	else
	{
		m_timestamp 	= timestamp;
	}
}

const int Frame::getSize() const 
{
	return m_len;
}

const void* Frame::getData() const
{
	return m_data;
}

const uint64_t Frame::getId() const
{
	return m_timestamp;
}

const bool Frame::isKeyFrame() const
{
	return m_isKey;
}
