#ifndef __MARINA_EYE_CAM__BASE__VIDEO__FRAME_H
#define __MARINA_EYE_CAM__BASE__VIDEO__FRAME_H

#include <Utils/Types.h> 
#include <stdio.h>
class Frame 
{

private:
	uint64_t 	m_timestamp;		// Milisegundo en el que fue creado
	uint32_t	m_len; 				// Numero de bytes
	uint8_t* 	m_data; 			// Datos del frame
	bool 		m_isKey;			// Marca el key frame


public:
	Frame();
	Frame(const Frame &copyFrame);
	Frame(void* data, uint32_t len, uint64_t timestamp=0, bool iskey=false);	
	~Frame();
	
	void clearFrame();				
	void setFrame(void* data, uint32_t len, uint64_t timestamp=0, bool iskey=false);
	const int getSize() const;		// Numero de bytes que contiene el frame
	const void* getData() const;	// Bytes del frame
	const uint64_t getId() const; 		// Timestamp del frame
	const bool isKeyFrame() const; 		// keyFrame?
};

#endif




