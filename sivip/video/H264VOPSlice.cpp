#include <video/H264VOPSlice.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

// #define H264VOPSLICE_VERBOSE

H264VOPSlice::H264VOPSlice( void *data, dword len, bool copy ) :data(data), len(len), ownCopy(copy) 
{
	if ( ownCopy )
	{
		this->data = (void*) new byte[len];
		if (this->data==NULL)
		{
			throw NotEnoughMemoryException(
								"Not enough memory to allocate VOP Slice data buffer"
													);
		}
		
		memmove( this->data, data, len );
	}
}

H264VOPSlice::~H264VOPSlice()
{
	if (  ( ownCopy ) && ( this->data!=NULL )	)
	{
		delete[] (byte*)this->data;
	}
		
}

void* H264VOPSlice::getSliceData()
{
	return this->data;
}

dword H264VOPSlice::getSliceSize()
{
	return this->len;
}
