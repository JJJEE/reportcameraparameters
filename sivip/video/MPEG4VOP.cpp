#include <video/MPEG4VOP.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

// #define MPEG4VOP_VERBOSE

MPEG4VOP::MPEG4VOP() : vopFull(false), type(0)
{
}


MPEG4VOP::MPEG4VOP(dword type) : vopFull(false), type(type)
{
}

MPEG4VOP::~MPEG4VOP()
{
	list<MPEG4VOPSlice*>::iterator lIt=voplist.begin();
	for (; lIt!=voplist.end(); lIt++)
		delete (*lIt);
	
	voplist.clear();
}

dword MPEG4VOP::getVOPStart(void *data, dword len, dword off,
	dword *codeType)
{
	dword vopStartCode=0;
	byte *b=(byte*)data;
	
#ifdef MPEG4VOP_VERBOSE
	RTSP::lerr << "MPEG4VOP::getVOPStart(" << data << ", " << len << ", " << off << ", " << (void*)codeType << ");" << endl;
#endif

	// Com a minim hem d'haver llegit 4 bytes!!
	for (dword l=off; l<off+3 && l<len; l++)
		vopStartCode = (vopStartCode << 8) | (dword)b[l];
	
	for (dword l=off+3; l<len; l++)
	{
		// El 4t el llegim aqui :)
		vopStartCode = (vopStartCode << 8) | (dword)b[l];
		
		switch (vopStartCode)
		{
			case VOP_B0:
			case VOP_B1:
			case VOP_B2:
			case VOP_B3:
			case VOP_B4:
			case VOP_B5:
			case VOP_B6:
				if (codeType!=NULL)
					*codeType=vopStartCode;
				return l-3;
		}
	}
	
	return 0xffffffff;
}

void MPEG4VOP::setVOPFull(bool full)
{
	this->vopFull=full;
}

bool MPEG4VOP::isVOPFull()
{
	return this->vopFull;
}

void *MPEG4VOP::getVOPData()
{
	dword vopSize = this->getVOPSize();

	byte *vop=new byte[vopSize];
	
	if (vop==NULL)
		throw NotEnoughMemoryException("Not enough memory to allocate VOP buffer");

	dword offset=0;
	list<MPEG4VOPSlice*>::iterator lIt=voplist.begin();
	for (; lIt!=voplist.end(); lIt++)
	{
		memmove(&vop[offset], (*lIt)->getSliceData(), (*lIt)->getSliceSize());
		offset+=(*lIt)->getSliceSize();
	}
	
	return vop;
}

dword MPEG4VOP::getVOPSize()
{
	dword vopSize=0;
	
	list<MPEG4VOPSlice*>::iterator lIt=voplist.begin();
	for (; lIt!=voplist.end(); lIt++)
		vopSize+=(*lIt)->getSliceSize();
		
	return vopSize;
}

dword MPEG4VOP::getType()
{
	return this->type;
}

bool MPEG4VOP::isKeyFrame()
{
	if (this->getType() == MPEG4VOP::VOP_B6)
	{
		byte key=0;
		dword vopOff=0;
		dword vopRead=0;
		list<MPEG4VOPSlice*>::iterator lIt=voplist.begin();
		for (; lIt!=voplist.end(); lIt++)
		{
			vopRead+=(*lIt)->getSliceSize();
			if (vopRead<5)
				vopOff=vopRead;
			else
			{
				byte *d=(byte*)(*lIt)->getSliceData();
				key=d[5-vopOff];
				break;
			}
		}
	
		return ((key&0xc0)==0);
	}
	
	return false;
}

void MPEG4VOP::addSlice(MPEG4VOPSlice *slice, bool last)
{
	voplist.push_back(slice);
	this->vopFull=last;
}
