#ifndef __MARINA_EYE_CAM__BASE__VIDEO__MPEG4CHUNKPROCESSOR_H
#define __MARINA_EYE_CAM__BASE__VIDEO__MPEG4CHUNKPROCESSOR_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <video/MPEG4VOP.h>
#include <string>

using namespace std;

class MPEG4ChunkProcessor
{
protected:
	list<MPEG4VOP*>availableVOPs;
	MPEG4VOP *currentVOP;

public:
	bool alarmStatus;
	MPEG4ChunkProcessor();
	~MPEG4ChunkProcessor();

	void processChunk(void *data, dword len);
	bool isFrameAvailable();
    MPEG4VOP *getFrame();
};

#endif
