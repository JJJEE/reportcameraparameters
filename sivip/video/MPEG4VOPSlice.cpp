#include <video/MPEG4VOPSlice.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

// #define MPEG4VOPSLICE_VERBOSE

MPEG4VOPSlice::MPEG4VOPSlice(void *data, dword len, bool copy) :
	data(data), len(len), ownCopy(copy) 
{
	if (ownCopy)
	{
		this->data=(void*)new byte[len];
		if (this->data==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate VOP Slice data buffer");
		
		memmove(this->data, data, len);
	}
}

MPEG4VOPSlice::~MPEG4VOPSlice()
{
	if (ownCopy && this->data!=NULL)
		delete[] (byte*)this->data;
}

void *MPEG4VOPSlice::getSliceData()
{
	return this->data;
}

dword MPEG4VOPSlice::getSliceSize()
{
	return this->len;
}
