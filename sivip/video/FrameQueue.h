#ifndef __MARINA_EYE_CAM__BASE__VIDEO__FRAMEQUEUE_H
#define __MARINA_EYE_CAM__BASE__VIDEO__FRAMEQUEUE_H

#include <Utils/Types.h>
#include <video/Frame.h>
#include <stdio.h>
#include <string.h>         
#include <queue>      
    
class FrameQueue
{
private:
	uint32_t				m_maxSize;	
	std::queue< Frame * > 	m_frameQueue;
	

public:
	FrameQueue();
	FrameQueue(uint32_t size);
	virtual ~FrameQueue();

	const uint64_t getNextFrameId() const; 
	const int getSize() const;
	const Frame* getNextFrame();
	Frame* readNextFrame(); 
    const Frame* getNextKeyFrame();
	const uint32_t getMaxSize() const;
	const uint32_t getFbk();	// Frames entre un key frame y el siguiente

	void setMaxSize(uint32_t size);
	void setFrame(Frame* newFrame);
	void setFrame(void* newFrame, uint32_t size, uint32_t id=0, bool isKey=false);
	
	int resetQueue();
	virtual void addVOPChunk(void *data, dword len) = 0;
	
};

#endif
