#include <iostream>
#include <Utils/Types.h>
#include <video/H264VOP.h>
#include <stdio.h>
#include <string.h>         
#include <queue>  
#include <video/MPEG4FrameQueue.h>


MPEG4FrameQueue::MPEG4FrameQueue()
{
	//printf("MPEG4FrameQueue::MPEG4FrameQueue()\n");
	m_currentFrame 		= 0;
	m_currentFrameSize 	= 0;
	m_init				= false;
	m_isKey				= false;
	m_numProcessFrames	= 0;
	m_VOPChunk			= new MPEG4ChunkProcessor();
}

MPEG4FrameQueue::MPEG4FrameQueue(uint32_t size): FrameQueue(size)
{
	//printf("MPEG4FrameQueue::MPEG4FrameQueue()\n");
	m_currentFrame 		= 0;
	m_currentFrameSize 	= 0;
	m_init				= false;
	m_isKey				= false;
	m_numProcessFrames	= 0;
	m_VOPChunk			= new MPEG4ChunkProcessor();
}

MPEG4FrameQueue::~MPEG4FrameQueue()
{
	//printf("MPEG4FrameQueue::~MPEG4FrameQueue()\n");
	if( m_currentFrame != NULL)
		delete[] m_currentFrame;

	if( m_VOPChunk != NULL)
		delete m_VOPChunk;
}

void MPEG4FrameQueue::addVOPChunk(void *data, dword len)
{	
	//printf("MPEG4FrameQueue::addVOPChunk()\n");
	MPEG4VOP* vop;
	uint8_t* vopData;
	int 	 vopLen;
					
	m_VOPChunk->processChunk( data, len );

	while( m_VOPChunk->isFrameAvailable() )
	{
		vop 		= m_VOPChunk->getFrame();
		dword type	= vop->getType();
		vopData 	= (uint8_t *) vop->getVOPData();
		vopLen 		= (int) vop->getVOPSize();

		if( ! m_init )
		{
			// Esperamos que llegue frame principal y comenzamos a añadir bytes
			if( type == MPEG4VOP::VOP_B0 )
			{
				//printf("Primer Frame\n");
				m_init 			= true;
				m_isKey 		= true;
				m_lastVOPType 	= type;
				add2CurrentFrame(vopData, vopLen);
			}
		}
		else
		{
			if(type == MPEG4VOP::VOP_B0)
			{
				//printf("----------------------->KEY FRAME\n");
				// Comienzo de siguiente frame
				m_isKey 		= true;
				add2CurrentFrame(vopData, vopLen);	
			}
			if(type == MPEG4VOP::VOP_B6 )
			{
				//printf("-----------------------> NUEVO FRAME : %d \n", m_currentFrameSize);				
				++m_numProcessFrames;
				// Frame Completo
				Frame *thisFrame = new Frame( (void *) m_currentFrame, m_currentFrameSize, m_numProcessFrames, m_isKey );

				m_isKey 		= false;
				setFrame(thisFrame);
				m_currentFrameSize = 0;

				// Comienzo de siguiente frame
				add2CurrentFrame(vopData, vopLen);
			}
			else
			{
				//printf("-----------------------> Añadiendo datos \n");
				// Mismo Frame, añadimos bytes
				add2CurrentFrame(vopData, vopLen);
			}

			m_lastVOPType 	= type;

			delete[] vopData;
			vopData = NULL;
			delete vop;
			vop = NULL;
		}
	}
}

void MPEG4FrameQueue::add2CurrentFrame(void *data, dword len)
{
	//printf("frameSize %d\n", m_currentFrameSize);

	if( m_currentFrameSize == 0)
	{
		m_currentFrame = new uint8_t[len];
		memcpy( m_currentFrame, data, len);
	}
	else
	{
		byte *copy = new uint8_t[ m_currentFrameSize ];
		memcpy( copy, m_currentFrame, m_currentFrameSize);
		delete[] m_currentFrame;

		m_currentFrame = new uint8_t[ m_currentFrameSize + len ];

		memcpy( m_currentFrame, copy, m_currentFrameSize);
		delete[] copy;
		memcpy( &m_currentFrame[m_currentFrameSize] , data, len);
	}
		m_currentFrameSize += len;
}
