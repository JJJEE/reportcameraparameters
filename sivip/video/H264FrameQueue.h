#ifndef __MARINA_EYE_CAM__BASE__VIDEO__H264FRAMEQUEUE_H
#define __MARINA_EYE_CAM__BASE__VIDEO__H264FRAMEQUEUE_H

#include <Utils/Types.h>
#include <video/Frame.h>
#include <video/H264ChunkProcessor.h>
#include <video/FrameQueue.h>
#include <stdio.h>
#include <string.h>         
#include <queue>      
    

class H264FrameQueue: public FrameQueue
{
private:
	H264ChunkProcessor*	m_VOPChunk;
	uint8_t*			m_currentFrame;
	int  				m_currentFrameSize;
	dword				m_lastVOPType;
	uint32_t			m_numProcessFrames;
	bool				m_init;		// Flag de inicialización
	bool				m_isKey;	// Flag de comienzo de keyFrame

	void add2CurrentFrame(void *data, dword len);

public:
	H264FrameQueue();
	H264FrameQueue(uint32_t size);
	~H264FrameQueue();

	virtual void addVOPChunk(void *data, dword len);

};

#endif
