#include <Utils/Types.h>
#include <video/Frame.h>
#include <stdio.h>
#include <string.h>         
#include <queue>  
#include <video/FrameQueue.h>

FrameQueue::FrameQueue()
{
	printf("FrameQueue::FrameQueue()\n");
	m_maxSize 	= 1; 
}

FrameQueue::FrameQueue(uint32_t size)
{
	m_maxSize 	= size; 
}

FrameQueue::~FrameQueue()
{
	printf("FrameQueue::~FrameQueue()\n");
	while( !m_frameQueue.empty() )
	{
		Frame* dropFrame 	= m_frameQueue.front();
		m_frameQueue.pop();
		if( dropFrame != 0) { delete dropFrame; }
	}
	printf("FrameQueue::~FrameQueue() bye\n");
}

const int FrameQueue::getSize() const
{
	return m_frameQueue.size();
}

const uint32_t FrameQueue::getFbk()
{
	std::queue< Frame * > 	tempQueue;
	Frame* tempFrame;
	bool firstKeyFrame 		= false;
	bool sencondKeyFrame 	= false;
	int fps 				= 0;

	// Contamos frames entre un key frame y el siguiente
	while( !m_frameQueue.empty() )
	{
		tempFrame 	= m_frameQueue.front();
		m_frameQueue.pop();
		tempQueue.push(tempFrame);

		if( tempFrame->isKeyFrame() && firstKeyFrame == false)
		{
			firstKeyFrame 		= true;
		}
		else if( tempFrame->isKeyFrame() && sencondKeyFrame == false)
		{
			sencondKeyFrame 	= true;
		}
		if( firstKeyFrame == true && sencondKeyFrame == false )
		{
			++fps;
		}
	}

	// Reestablecemos la cola
	while( !tempQueue.empty() )
	{
		tempFrame 	= tempQueue.front();
		tempQueue.pop();
		m_frameQueue.push(tempFrame);
	}

	return fps;
}

const Frame* FrameQueue::getNextFrame() 
{
	//printf("FrameQueue::getNextFrame()\n");
	if( !m_frameQueue.empty() )
	{
		Frame* dropFrame 	= m_frameQueue.front();
		//printf("FrameQueue::getNextFrame() - size = %d\n", dropFrame->getSize());
		m_frameQueue.pop();
		return dropFrame;
	}
	else
	{
		return 0;
	}
}

Frame* FrameQueue::readNextFrame() 
{
	//printf("FrameQueue::getNextFrame()\n");
	if( !m_frameQueue.empty() )
	{
		Frame* dropFrame 	= m_frameQueue.front();
		return dropFrame;
	}
	else
	{
		return 0;
	}
}


const Frame* FrameQueue::getNextKeyFrame()
{
	printf("FrameQueue::getNextKeyFrame()\n");
	fflush(stdout);
	bool isKeyFrame	= false;
	
	Frame* dropFrame = 0;
	Frame* keyFrame;

	while( !m_frameQueue.empty() )
	{
		dropFrame 	= m_frameQueue.front();
		m_frameQueue.pop();

		if( dropFrame->isKeyFrame() )
		{
			//printf("FrameQueue::getNextKeyFrame()- KeyFrame found in pos: %d !\n", i);
			fflush(stdout);
			keyFrame = new Frame( *dropFrame );

			printf("FrameQueue::getNextKeyFrame() keyFrame->getSize(): %u \n", keyFrame->getSize());
			isKeyFrame 	= true;
			break;
		}
		//else
		//{
			delete dropFrame;
			dropFrame 	= 0;	
		//}
	}

	// Eliminamos hasta el siguiente keyFrame
	while( !m_frameQueue.empty() )
	{
		dropFrame 	= m_frameQueue.front();

		if( dropFrame->isKeyFrame() )
		{
			break;
		}
		else
		{
			m_frameQueue.pop();
			delete dropFrame;
			dropFrame 	= 0;	
		}
	}

	if(isKeyFrame == true)
	{
		printf("FrameQueue::getNextKeyFrame() keyFrame->getSize(): %u \n", keyFrame->getSize());
		return keyFrame;
	}
	else
	{
		printf("FrameQueue::getNextKeyFrame() No Frame\n");
		return 0;
	}
	
}

const uint64_t FrameQueue::getNextFrameId()  const
{
	if( !m_frameQueue.empty() )
	{
		Frame* dropFrame 	= m_frameQueue.front();

		return dropFrame->getId();
	}
	else
	{
		return 0;
	}
}

void FrameQueue::setMaxSize(uint32_t size)
{
	m_maxSize 	= size; 
}

const uint32_t FrameQueue::getMaxSize() const
{
	return m_maxSize;
}

void FrameQueue::setFrame(Frame* newFrame)
{
	if(m_frameQueue.size() >= m_maxSize)
	{
		Frame* dropFrame 	= m_frameQueue.front();
		printf("FrameQueue::setFrame(): Max size of queue reached. Dropping oldest frame (%llu).\n", dropFrame->getId());
		m_frameQueue.pop();

		if( dropFrame != 0) { delete dropFrame; }
	}

	//printf("FrameQueue::setFrame(): Frame size = %d\n", newFrame->getSize());
	m_frameQueue.push(newFrame);
}

void FrameQueue::setFrame(void* newFrame, uint32_t size, uint32_t id, bool isKey)
{
	Frame* fr = new Frame(newFrame, size, id, isKey);	

	setFrame(fr);
}

int FrameQueue::resetQueue()
{
	Frame* dropFrame;
	int i = 0;
	while( !m_frameQueue.empty() )
	{
		dropFrame 	= m_frameQueue.front();
		m_frameQueue.pop();
		delete dropFrame;
		dropFrame 	= 0;
		++i;
	}

	return i;
}



