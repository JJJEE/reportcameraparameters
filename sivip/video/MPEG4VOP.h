#ifndef __MARINA_EYE_CAM__BASE__VIDEO__MPEG4VOP_H
#define __MARINA_EYE_CAM__BASE__VIDEO__MPEG4VOP_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <video/MPEG4VOPSlice.h>
#include <list>

using namespace std;

class MPEG4VOP
{
public:
	static const dword VOP_B0 = 0x000001b0;	// Visual Object Sequence Start
	static const dword VOP_B1 = 0x000001b1;	// Visual Object Sequence End
	static const dword VOP_B2 = 0x000001b2;	// User Data
	static const dword VOP_B3 = 0x000001b3;	// Group of VOP Start
	static const dword VOP_B4 = 0x000001b4;	// Video Session Error
	static const dword VOP_B5 = 0x000001b5;	// Visual Object Start
	static const dword VOP_B6 = 0x000001b6;	// Visual Object Plane Start
	
//	static const dword KeyFrame = MPEG4VOP::VOP_B0;  // 20090304 - NO ES CERT
	static const dword UserData = MPEG4VOP::VOP_B2;
	static const dword FrameData = MPEG4VOP::VOP_B6;
	static const dword AlarmData = MPEG4VOP::VOP_B2;  // 20090304 - NO ES CERT

protected:
	bool vopFull;
	dword type;
	list<MPEG4VOPSlice*> voplist;

public:
    MPEG4VOP();
    MPEG4VOP(dword type);
    ~MPEG4VOP();
    
    static dword getVOPStart(void *data, dword len, dword off=0, dword *codeType=0);
    
	void setVOPFull(bool full);
    bool isVOPFull();
	void *getVOPData();
	dword getVOPSize();
	dword getType();
	bool isKeyFrame();
	
	void addSlice(MPEG4VOPSlice *slice, bool last=false);
};

#endif
