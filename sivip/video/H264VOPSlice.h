#ifndef __MARINA_EYE_CAM__BASE__VIDEO__H264VOPSLICE_H
#define __MARINA_EYE_CAM__BASE__VIDEO__H264VOPSLICE_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <list>

using namespace std;

class H264VOPSlice
{
protected:
	void *data;
	dword len;
	bool ownCopy;
	
public:
    H264VOPSlice( void *data, dword len, bool copy = false );
    ~H264VOPSlice();

	void *getSliceData();
	dword getSliceSize();
};

#endif
