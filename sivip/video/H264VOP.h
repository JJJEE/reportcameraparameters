#ifndef __MARINA_EYE_CAM__BASE__VIDEO__H264VOP_H
#define __MARINA_EYE_CAM__BASE__VIDEO__H264VOP_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <video/H264VOPSlice.h>
#include <list>
#include <cstddef> // NULL

using namespace std;

class H264VOP
{
public:
	static const dword VOP_06 = 0x00000106;	// Cabecera del frame //dec: 262
	static const dword VOP_21 = 0x00000121;	// Single Frame	//dec: 289
	static const dword VOP_25 = 0x00000125;	// Key Frame   //dec: 293
	static const dword VOP_27 = 0x00000127;	// NAL  //dec: 295
	static const dword VOP_28 = 0x00000128;	// NAL  //dec: 296

	//static const dword FrameData 		= H264VOP::VOP_06;
	//static const dword KeyFrame 		= H264VOP::VOP_25;  
	//static const dword SingleFrame 	= H264VOP::VOP_21;

protected:
	bool vopFull;
	dword type;
	list<H264VOPSlice*> voplist;

public:
    H264VOP();
    H264VOP(dword type);
    ~H264VOP();
    
    static dword getVOPStart(	void *data, 
								dword len, 
    							dword off = 0, 
    							dword *codeType = NULL );
    
	void 	setVOPFull(bool full);
   	bool 	isVOPFull();
	void 	*getVOPData();
	dword 	getVOPSize();
	dword 	getType();
	bool 	isKeyFrame();
	
	void 	addSlice(	H264VOPSlice *slice, bool last = false	);
};

#endif
