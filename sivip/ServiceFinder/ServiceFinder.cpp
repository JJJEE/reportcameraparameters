/*
 *  ServiceFinder.cpp
 *  
 *
 *  Created by David Marí Larrosa on 21/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <ServiceFinder/ServiceFinder.h>
#include <Utils/Exception.h>
#include <CentralDirectory/CentralDirectoryInterface.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <XML/xmlNode.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <Sockets/SocketTimeoutException.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>

#ifdef WIN32
#include <Utils/WindowsDefs.h>
#endif

#pragma mark *** Estatiques
PtrPool ServiceFinder::ptrPool(string("serviceFinder Pool"));
map<string, word> ServiceFinder::subsystemResolveCache;
map<word, string> ServiceFinder::subsystemInverseResolveCache;
RWLock ServiceFinder::cacheRWLock;
RWLock ServiceFinder::invCacheRWLock;
Mutex  ServiceFinder::creationLock;
int ServiceFinder::dbSocketProblemCounter=0;
ModuleIsAliveThread *ServiceFinder::moduleIsAliveThread=NULL;
	
#pragma mark *** Metodes

void ServiceFinder::setupModuleIsAliveThread(Canis *cn)
{
//	cout<<"ServiceFinder::setupModuleIsAliveThread "<<(void*)cn<<endl;
	if(moduleIsAliveThread == NULL)
	{
		creationLock.lock();
		if(moduleIsAliveThread == NULL && cn != NULL)
		{
			int *filter = new int[2];
			filter[0] = centralDirectoryTypeId;
			filter[1] = dbGatewayTypeId;
//			cout<<"ServiceFinder::setupModuleIsAliveThread new  ModuleIsAliveThread "<<(void*)cn<<endl;
			moduleIsAliveThread = new ModuleIsAliveThread(cn, filter, 2);
			moduleIsAliveThread->start();
		}
		creationLock.unlock();
	}
}


RPC* ServiceFinder::getRPC(Address *a)
{
	STACKTRACE_INSTRUMENT();
	
	RPC *rpc=(RPC*)ptrPool.get();
	
	if (rpc==NULL && a!=NULL)
	{
		rpc=new RPC(*a);
		if (rpc==NULL)
		{
			throw (Exception("FATAL: Not enough memory"));
		}
		ptrPool.add(rpc, true);
	}
	
	return rpc;
}

short ServiceFinder::registerSubsystemTypeName(string type, Canis *cn, bool debug)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
	bool cache=false;
	short typeId=0;
	
	ServiceFinder::cacheRWLock.rlock();
	map<string,word>::iterator mIt=
		ServiceFinder::subsystemResolveCache.find(type);
	if (mIt!=ServiceFinder::subsystemResolveCache.end())
	{
		cache=true;
		typeId=(short)mIt->second;
	}
	ServiceFinder::cacheRWLock.unlock();
	if (cache)
	{
		if(debug)
			cout << "-> registerSubsystemTypeName(" << type << ") in cache:"<<typeId << endl;
		return typeId;
	}
	
	if(debug)
		cout << "-> registerSubsystemTypeName(" << type << ")" << endl;

	// Trobem la BD per queryar-la
	RPC *rpc=ServiceFinder::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn);
		
		if(debug)
			cout << "-> registerSubsystemTypeName(" << type << ") bd:"<<db->a->toString() << endl;
		rpc=ServiceFinder::getRPC(db->a);
		
		delete db;
	}
	
	string q=string("SELECT id FROM tiposubsistema WHERE nombre='")+
			type+string("';");

	RPCPacket pk(Address(IP("0.0.0.0"), 0), 0, (byte*)q.c_str(), q.length(), 0, ServiceFinder::dbGatewayTypeId);
	

//	cout << "-> getBestSubsys call select tiposubsist." << endl;
	bool registered=false;

resolveRegisteredSubsys:
	RPCPacket *result=NULL;
	int retryCount=2;
	while (retryCount>0)
	{
		try
		{
			result=rpc->call(pk);
			// No fem el release fins que no tinguem clar que retornem...
			// P.e. el necessitem per fer el register si toca
//			ptrPool.release(rpc);
			retryCount=0;
		}
		catch (SocketTimeoutException &ste)
		{
			try
			{
				RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn, true);
				rpc->setDefaultDestination(*db->a);
				delete db;
			}
			catch (Exception &e)
			{
			}
			
			ServiceFinder::dbSocketProblemCounter++;
			if (ServiceFinder::dbSocketProblemCounter >
				ServiceFinder::maxSocketProblemCount)
			{
				ServiceFinder::dbSocketProblemCounter=0;
				retryCount--;
			}
			else
			{
				retryCount=0;
				ptrPool.release(rpc);
				throw ste;
			}
		}
		catch (Exception &e)
		{	
			retryCount=0;
			ptrPool.release(rpc);
			throw e;
		}
	}
	
	if (result==NULL)
	{
		ptrPool.release(rpc);
		throw (Exception(string("ServiceFinder::registerSubsystemTypeName: No response for type ")+type+string(" after 3 retries")));
	}
	
	ptrPool.release(rpc);
	
	string *subsysXML=new string((char*)result->getData(), (size_t)result->getSize());

	if(debug)
		cout << "-> registerSubsystemTypeName(" << type << ") query:"<< q << endl << "res:"<< *subsysXML << endl;
	
	delete result;

	XML *xml=xmlParser::parse(*subsysXML);
	
	delete subsysXML;
	
	xmlNode *n=xml->getNode("/result/[0]/id");
	
	if (n==NULL && !registered)
	{
		// No hem trobat, registrem...
		string q=string("INSERT INTO tiposubsistema (nombre) VALUES ('")+
				type+string("');");
	
		RPCPacket pk(Address(IP("0.0.0.0"), 0), 1, (byte*)q.c_str(), q.length(), 0, ServiceFinder::dbGatewayTypeId);
		
	//	cout << "-> getBestSubsys call select tiposubsist." << endl;
		result=NULL;
		retryCount=2;
		while (retryCount>0)
		{
			try
			{
				result=rpc->call(pk);
//				ptrPool.release(rpc);
				retryCount=0;
			}
			catch (SocketTimeoutException &ste)
			{
				try
				{
					RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn, true);
					rpc->setDefaultDestination(*db->a);
					delete db;
				}
				catch (Exception &e)
				{
				}
				
				ServiceFinder::dbSocketProblemCounter++;
				if (ServiceFinder::dbSocketProblemCounter >
					ServiceFinder::maxSocketProblemCount)
				{
					ServiceFinder::dbSocketProblemCounter=0;
					retryCount--;
				}
				else
				{
					retryCount=0;
					ptrPool.release(rpc);
					throw ste;
				}
			}
			catch (Exception &e)
			{	
				retryCount=0;
				ptrPool.release(rpc);
				throw e;
			}
		}
		
		if (result!=NULL)
		{
			delete result;
			result=NULL;
		}
		registered=true;
		
		goto resolveRegisteredSubsys;
	}
	else if (n==NULL)
	{
//		cout << "i getBestSS: Exc unreg. type." << endl;
		throw (Exception(string("Unregistered type ")+type+string(": ")+xml->toString()));
	}
	
	typeId=atoi(n->getCdata().c_str());
	
	delete xml;
	
	ServiceFinder::cacheRWLock.wlock();
	ServiceFinder::subsystemResolveCache[type]=typeId;
	ServiceFinder::cacheRWLock.unlock();
	
	if(debug)
		cout << "-> registerSubsystemTypeName(" << type << ") found:"<< typeId << endl;
	return typeId;	
}


short ServiceFinder::resolveSubsystemTypeName(string type, Canis *cn, bool debug)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
	bool cache=false;
	short typeId=0;
	
	ServiceFinder::cacheRWLock.rlock();
	map<string,word>::iterator mIt=
		ServiceFinder::subsystemResolveCache.find(type);
	if (mIt!=ServiceFinder::subsystemResolveCache.end())
	{
		cache=true;
		typeId=(short)mIt->second;
	}
	ServiceFinder::cacheRWLock.unlock();
	if (cache)
	{
		if(debug)
			cout << "-> resolveSubsysTypeName(" << type << ") in cache:"<<typeId << endl;
		return typeId;
	}
	if(debug)
		cout << "-> resolveSubsysTypeName(" << type << ")" << endl;
	// Trobem la BD per queryar-la
	RPC *rpc=ServiceFinder::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn);
		if(debug)
			cout << "-> resolveSubsysTypeName(" << type << ") bd:"<<db->a->toString() << endl;
		rpc=ServiceFinder::getRPC(db->a);
		delete db;
	}
	
	string q=string("SELECT id FROM tiposubsistema WHERE nombre='")+
			type+string("';");

	RPCPacket pk(Address(IP("0.0.0.0"), 0), 0, (byte*)q.c_str(), q.length(), 0, ServiceFinder::dbGatewayTypeId);
	
//	cout << "-> getBestSubsys call select tiposubsist." << endl;
	RPCPacket *result=NULL;
	int retryCount=2;
	while (retryCount>0)
	{
		try
		{
			result=rpc->call(pk);
			ptrPool.release(rpc);
			retryCount=0;
		}
		catch (SocketTimeoutException &ste)
		{
			try
			{
				RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn, true);
				rpc->setDefaultDestination(*db->a);
				delete db;
			}
			catch (Exception &e)
			{
			}
			
			ServiceFinder::dbSocketProblemCounter++;
			if (ServiceFinder::dbSocketProblemCounter >
				ServiceFinder::maxSocketProblemCount)
			{
				ServiceFinder::dbSocketProblemCounter=0;
				retryCount--;
			}
			else
			{
				retryCount=0;
				ptrPool.release(rpc);
				throw ste;
			}
		}
		catch (Exception &e)
		{	
			retryCount=0;
			ptrPool.release(rpc);
			throw e;
		}
	}
	
	if (result==NULL)
		throw (Exception(string("ServiceFinder::resolveSubsystemTypeName: No response for type ")+type+string(" after 3 retries")));
	
	string *subsysXML=new string((char*)result->getData(), (size_t)result->getSize());

	if(debug)
		cout << "-> resolveSubsysTypeName(" << type << ") query:"<< q << endl << "res:"<< *subsysXML << endl;
	
	delete result;

	XML *xml=xmlParser::parse(*subsysXML);
	
	delete subsysXML;
	
	xmlNode *n=xml->getNode("/result/[0]/id");
	
	if (n==NULL)
	{
//		cout << "i getBestSS: Exc unreg. type." << endl;
		throw (Exception(string("Unregistered type ")+type+string(":")+xml->toString()));
	}
	
	typeId=atoi(n->getCdata().c_str());
	
	delete xml;
	
	ServiceFinder::cacheRWLock.wlock();
	ServiceFinder::subsystemResolveCache[type]=typeId;
	ServiceFinder::cacheRWLock.unlock();
	
	if(debug)
		cout << "-> resolveSubsysTypeName(" << type << ") found:"<< typeId << endl;
	return typeId;	
}

string ServiceFinder::inverseResolveSubsystemTypeId(short type, Canis *cn)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
	bool cache=false;
	string typeName("");
	
	ServiceFinder::invCacheRWLock.rlock();
	map<word,string>::iterator mIt=
		ServiceFinder::subsystemInverseResolveCache.find((word)type);
	if (mIt!=ServiceFinder::subsystemInverseResolveCache.end())
	{
		cache=true;
		typeName=mIt->second;
	}
	ServiceFinder::invCacheRWLock.unlock();
	if (cache)
		return typeName;

	// Trobem la BD per queryar-la
	RPC *rpc=ServiceFinder::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn);
		rpc=ServiceFinder::getRPC(db->a);
		delete db;
	}
	
	string q=string("SELECT nombre FROM tiposubsistema WHERE id='")+
			StrUtils::decToString(type)+string("';");

	RPCPacket pk(Address(IP("0.0.0.0"), 0), 0, (byte*)q.c_str(), q.length(), 0, ServiceFinder::dbGatewayTypeId);
	
//	cout << "-> getBestSubsys call select tiposubsist." << endl;
	RPCPacket *result=NULL;
	int retryCount=2;
	while (retryCount>0)
	{
		try
		{
			result=rpc->call(pk);
			ptrPool.release(rpc);
			retryCount=0;
		}
		catch (SocketTimeoutException &ste)
		{
			try
			{
				RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn, true);
				rpc->setDefaultDestination(*db->a);
				delete db;
			}
			catch (Exception &e)
			{
			}
			
			ServiceFinder::dbSocketProblemCounter++;
			if (ServiceFinder::dbSocketProblemCounter >
				ServiceFinder::maxSocketProblemCount)
			{
				ServiceFinder::dbSocketProblemCounter=0;
				retryCount--;
			}
			else
			{
				retryCount=0;
				ptrPool.release(rpc);
				throw ste;
			}
		}
		catch (Exception &e)
		{	
			retryCount=0;
			ptrPool.release(rpc);
			throw e;
		}
	}
	
	if (result==NULL)
		throw (Exception(string("ServiceFinder::inverseresolveSubsystemTypeId: No response for type ")+StrUtils::decToString(type)+string(" after 3 retries")));
	
	string *subsysXML=new string((char*)result->getData(), (size_t)result->getSize());
	
	delete result;

	XML *xml=xmlParser::parse(*subsysXML);
	
	delete subsysXML;
	
	xmlNode *n=xml->getNode("/result/[0]/nombre");
	
	if (n==NULL)
	{
//		cout << "i getBestSS: Exc unreg. type." << endl;
		throw (Exception(string("Unregistered type ")+StrUtils::decToString(type)+string(":")+xml->toString()));
	}
	
	typeName=n->getCdata();
	
	delete xml;

	ServiceFinder::invCacheRWLock.wlock();
	ServiceFinder::subsystemInverseResolveCache[(word)type]=typeName;
	ServiceFinder::invCacheRWLock.unlock();
	
	return typeName;	
}

IP ServiceFinder::getIPForDevice(dword devId, Canis *cn)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
//	cout << "-> resolveSubsysTypeName(" << type << ")" << endl;
	// Trobem la BD per queryar-la
	RPC *rpc=ServiceFinder::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn);
		cout<<"-> getRPC"<<endl;
		rpc=ServiceFinder::getRPC(db->a);
		delete db;
	}
	
	string q=string("SELECT host(ip) AS ip FROM dispositivo WHERE id='")+
			StrUtils::decToString(devId)+string("';");

	RPCPacket pk(Address(IP("0.0.0.0"), 0), 0, (byte*)q.c_str(), q.length(), 0, ServiceFinder::dbGatewayTypeId);
	
//	cout << "-> getBestSubsys call select tiposubsist." << endl;
	RPCPacket *result=NULL;
	int retryCount=2;
	while (retryCount>0)
	{
		try
		{
			result=rpc->call(pk);
			ptrPool.release(rpc);
			retryCount=0;
		}
		catch (SocketTimeoutException &ste)
		{
			try
			{
				RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn, true);
				rpc->setDefaultDestination(*db->a);
				delete db;
			}
			catch (Exception &e)
			{
			}
			
			ServiceFinder::dbSocketProblemCounter++;
			if (ServiceFinder::dbSocketProblemCounter >
				ServiceFinder::maxSocketProblemCount)
			{
				ServiceFinder::dbSocketProblemCounter=0;
				retryCount--;
			}
			else
			{
				retryCount=0;
				ptrPool.release(rpc);
				throw ste;
			}
		}
		catch (Exception &e)
		{	
			retryCount=0;
			ptrPool.release(rpc);
			throw e;
		}
	}

	if (result==NULL)
		throw (Exception(string("ServiceFinder::getIPForDevice: No response for device ")+StrUtils::decToString(devId)+string(" after 3 retries")));
	
	string *subsysXML=new string((char*)result->getData(), (size_t)result->getSize());
	
	delete result;

	XML *xml=xmlParser::parse(*subsysXML);
	
	delete subsysXML;
	
	xmlNode *n=xml->getNode("/result/[0]/ip");
	
	if (n==NULL)
	{
//		cout << "i getBestSS: Exc unreg. type." << endl;
		throw (Exception(string("Unregistered device ")+StrUtils::decToString(devId)+string(":")+xml->toString()));
	}
	
	IP ip(n->getCdata().c_str());
	
	delete xml;
	
	return ip;	
}

IP ServiceFinder::getIPForSubsys(dword subsysId, Canis *cn)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
//	cout << "-> resolveSubsysTypeName(" << type << ")" << endl;
	// Trobem la BD per queryar-la
	RPC *rpc=ServiceFinder::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn);
		cout<<"-> getRPC"<<endl;
		rpc=ServiceFinder::getRPC(db->a);
		delete db;
	}
	
	string q=string("SELECT host(ip) AS ip FROM subsistema WHERE id='")+
			StrUtils::decToString(subsysId)+string("';");

	RPCPacket pk(Address(IP("0.0.0.0"), 0), 0, (byte*)q.c_str(), q.length(), 0, ServiceFinder::dbGatewayTypeId);
	
//	cout << "-> getBestSubsys call select tiposubsist." << endl;
	RPCPacket *result=NULL;
	int retryCount=2;
	while (retryCount>0)
	{
		try
		{
			result=rpc->call(pk);
			ptrPool.release(rpc);
			retryCount=0;
		}
		catch (SocketTimeoutException &ste)
		{
			try
			{
				RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn, true);
				rpc->setDefaultDestination(*db->a);
				delete db;
			}
			catch (Exception &e)
			{
			}
			
			ServiceFinder::dbSocketProblemCounter++;
			if (ServiceFinder::dbSocketProblemCounter >
				ServiceFinder::maxSocketProblemCount)
			{
				ServiceFinder::dbSocketProblemCounter=0;
				retryCount--;
			}
			else
			{
				retryCount=0;
				ptrPool.release(rpc);
				throw ste;
			}
		}
		catch (Exception &e)
		{	
			retryCount=0;
			ptrPool.release(rpc);
			throw e;
		}
	}

	if (result==NULL)
		throw (Exception(string("ServiceFinder::getIPForSubsys: No response for subsys ")+StrUtils::decToString(subsysId)+string(" after 3 retries")));
	
	string *subsysXML=new string((char*)result->getData(), (size_t)result->getSize());
	
	delete result;

	XML *xml=xmlParser::parse(*subsysXML);
	
	delete subsysXML;
	
	xmlNode *n=xml->getNode("/result/[0]/ip");
	
	if (n==NULL)
	{
//		cout << "i getBestSS: Exc unreg. type." << endl;
		throw (Exception(string("Unregistered subsystem ")+StrUtils::decToString(subsysId)+string(":")+xml->toString()));
	}
	
	IP ip(n->getCdata().c_str());
	
	delete xml;
	
	return ip;	
}

dword ServiceFinder::getSubsysIdForAddressAndType(Address addr, word typeId, Canis *cn)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
//	cout << "-> resolveSubsysTypeName(" << type << ")" << endl;
	// Trobem la BD per queryar-la
	RPC *rpc=ServiceFinder::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn);
		cout<<"-> getRPC"<<endl;
		rpc=ServiceFinder::getRPC(db->a);
		delete db;
	}
	
	string q=string("SELECT id FROM subsistema WHERE ip='")+
		addr.getIP().toString() + string("' AND puerto='") +
		StrUtils::decToString(addr.getPort())+string("' AND tipo='") +
		StrUtils::decToString(typeId)+string("';");

	RPCPacket pk(Address(IP("0.0.0.0"), 0), 0, (byte*)q.c_str(), q.length(), 0, ServiceFinder::dbGatewayTypeId);
	
//	cout << "-> getBestSubsys call select tiposubsist." << endl;
	RPCPacket *result=NULL;
	int retryCount=2;
	while (retryCount>0)
	{
		try
		{
			result=rpc->call(pk);
			ptrPool.release(rpc);
			retryCount=0;
		}
		catch (SocketTimeoutException &ste)
		{
			try
			{
				RPCPacket *db=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, cn, true);
				rpc->setDefaultDestination(*db->a);
				delete db;
			}
			catch (Exception &e)
			{
			}
			
			ServiceFinder::dbSocketProblemCounter++;
			if (ServiceFinder::dbSocketProblemCounter >
				ServiceFinder::maxSocketProblemCount)
			{
				ServiceFinder::dbSocketProblemCounter=0;
				retryCount--;
			}
			else
			{
				retryCount=0;
				ptrPool.release(rpc);
				throw ste;
			}
		}
		catch (Exception &e)
		{	
			retryCount=0;
			ptrPool.release(rpc);
			throw e;
		}
	}

	if (result==NULL)
		throw (Exception(string("ServiceFinder::getSubsysIdForAddressAndType: No response for subsys @ ")+addr.toString() + string(" type ") + StrUtils::decToString(typeId)+string(" after 3 retries")));

	string *subsysXML=new string((char*)result->getData(), (size_t)result->getSize());
	
	delete result;

	XML *xml=xmlParser::parse(*subsysXML);
	
	delete subsysXML;
	
	xmlNode *n=xml->getNode("/result/[0]/id");
	
	if (n==NULL)
	{
//		cout << "i getBestSS: Exc unreg. type." << endl;
		throw (Exception(string("Unregistered subsystem at ")+addr.toString()+string(":")+xml->toString()));
	}
	
	dword subsysId=atoi(n->getCdata().c_str());
	
	delete xml;
	
	return subsysId;	
}

RPCPacket* ServiceFinder::getBestCentralDirectory(Canis *cn, Address oldCDAddr)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
	// cout << "-> getBestCentralDirectory()" << endl;
	if (cn==NULL)
	{
		throw (Exception("Invalid parameter"));
	}	
	Address oldAddr=oldCDAddr;
	Address bestAddr=oldAddr;
	bool oldOnline=false;
	
	list<Canis::subsys> systems=cn->getTypeList(ServiceFinder::centralDirectoryTypeId);

	float load=99999999.0f;

	for (list<Canis::subsys>::iterator it=systems.begin(); it!=systems.end(); it++)
	{
		IP ipCanis((*it).ip);
		Address newAddr(ipCanis, (*it).port);

		if(moduleIsAliveThread->isAlive(newAddr))
		{
			if (newAddr.toString()==oldAddr.toString())
				oldOnline=true;

			if ((*it).load<load)
			{
				load=(*it).load;
				bestAddr=newAddr;
			}
		}
	}
	
	if (bestAddr.toString()!=oldAddr.toString() || oldOnline)
	{
		RPCPacket *pk=new RPCPacket(bestAddr, 0, (byte*)&load, sizeof(load), ServiceFinder::centralDirectoryTypeId, 0, true);
		
		if (pk==NULL)
		{
			throw (Exception("Not enough memory to allocate result"));
		}
		
		// cout << "<- getBestCentralDirectory()" << endl;
		return pk;
	}
	
	// Si arribem aqui, !oldOnline
	// Comprovem !oldOnline perquè si old TORNA a estar online, coincideixen les adreces i es la millor, ja ens va be
	while (bestAddr.toString()==oldAddr.toString() && !oldOnline)
	{
		sleep(1);
		
		list<Canis::subsys> systems=cn->getTypeList(centralDirectoryTypeId);

		float load=99999999.0f;

		for (list<Canis::subsys>::iterator it=systems.begin(); 
			 it!=systems.end(); it++)
		{
			IP ipCanis((*it).ip);
			Address newAddr(ipCanis, (*it).port);
			if(moduleIsAliveThread->isAlive(newAddr))
			{
				if (newAddr.toString()==oldAddr.toString())
					oldOnline=true;

				if ((*it).load<load)
				{
					load=(*it).load;
					bestAddr=newAddr;
				}
			}
		}
	}

	RPCPacket *pk=new RPCPacket(bestAddr, 0, (byte*)&load, sizeof(load), ServiceFinder::centralDirectoryTypeId, 0, true);
	
	if (pk==NULL)
	{
		throw (Exception("Not enough memory to allocate result"));
	}
	
	// cout << "<- getBestCentralDirectory()" << endl;
	return pk;
}

RPCPacket* ServiceFinder::getBestSubsys(string type, Canis *cn, bool canisOnly)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	cout<<"----- SF::GetBestSS str:"<<type<<" resolve"<<endl;
	int nType=ServiceFinder::resolveSubsystemTypeName(type, cn);
	cout<<"----- SF::GetBestSS str:"<<nType<<" getBSS"<<endl;
	RPCPacket *res=ServiceFinder::getBestSubsys(nType, cn, canisOnly);
	cout<<"----- SF::GetBestSS res:"<<(void*)res<<endl;
	return res;
}

RPCPacket* ServiceFinder::getBestSubsys(short type, Canis *cn, bool canisOnly)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
//	 cout << "-> getBestSubsys(" << type << ")" << endl;
	Address addr(IP(string("0.0.0.0")), 0);
	RPCPacket *result=NULL;
	
	bool checkCanis=(cn!=NULL);
	
//	if (canisOnly && !checkCanis)
//		throw (Exception("Inconsistent parameters. Need Canis if canisOnly set to true"));
	
	if (!canisOnly)
	{
		if (cn==NULL)
		{
			cn=new Canis(addr, 0, true);
			if (cn==NULL)
			{
	//		cout << " getBestSS: Exc mem" << endl;
				throw (Exception("Not enough memory to allocate temporal Canis"));
			}
		}
		
	//	cout << "getBestSubsys get Best CDir" << endl;
		RPCPacket *dir=ServiceFinder::getBestCentralDirectory(cn, addr);
		
	//	cout << "getBestSubsys get bcd = " << dir->a->toString() << endl;
		if (!checkCanis)
			delete cn;
		
		// Preguntem al central directory en asincron, i mentrestant demanem resposta tb al canis
		CentralDirectoryInterface::findBestSubsystemByTypeIdParams params;
		
		params.algorithm=CentralDirectoryInterface::CENTRAL_DIRECTORY;
		params.type=type;
		
		void *paramsSer=params.toNetwork();
	
		RPCPacket pk(Address(IP("0.0.0.0"), 0), CentralDirectoryInterface::findBestSubsystemByTypeIdServiceId, (byte*)paramsSer, 
			params.serializationSize(), 0, ServiceFinder::centralDirectoryTypeId,
			true);
	
		delete [] (byte*)paramsSer;

		RPC rpc(*dir->a);
		
		// No ens cal dir mes...
		delete dir;
		
		// Enviem comanda per trobar el millor, asincronament, per directori central
//		cout << "	async call CentralDir..." << type <<endl;
		rpc.callAsync(pk);
	
		// Trobem el millor per canis
		list<Canis::subsys> systems;
		Canis::subsys bestSys;
		float load=99999999.0f;
	
		if (checkCanis)
		{
	//		 cout << "	canis search..." << endl;
			systems=cn->getTypeList(type);
			for (list<Canis::subsys>::iterator it=systems.begin(); 
				 it!=systems.end(); it++)
			{
				IP ipCanis((*it).ip);
				Address newAddr(ipCanis, (*it).port);

				if(moduleIsAliveThread->isAlive(newAddr))
				{
					if ((*it).load<load)
					{
						load=(*it).load;
						bestSys=(*it);
					}
				}
			}
		}
		
		// Aagfem el resultat de l'RPC
		int nEx=0;
		while (nEx<5)
		{
			// De moment assumim que si hi ha excepcio es per timeout del socket => cal esperar mes
			try 
			{
//				 cout << "	async call result..." << endl;
				result=rpc.getAsyncResult();
//				 cout << "	async call result obtained..." << endl;
				break;
			}
			catch (RPCException rpce)
			{
	//			 cout << "	async call result EXCEPTION... " << rpce.getMsg() << endl;
				nEx++;
			}
			catch (SocketTimeoutException rpce)
			{
	//			 cout << "	async call result timeout exception... " << rpce.getMsg() << endl;
				nEx+=5;
			}
		}
		
		if (nEx>=5 && (!checkCanis || systems.size()==0))
		{
	//		cout << " getBestSS: Exc  \"Could not find best subsystem either by means of Canis nor Central Directory\"" << endl;
			throw(Exception(0, "Could not find best subsystem either by means of Canis nor Central Directory"));
		}
		
		if(result!=NULL)
		{
			load=*((float *)result->getData());
			Endian::from(Endian::xarxa, &load, sizeof(load));
		}	
		// Prioritzem la resposta del CDir, per tant nomes fem canis per
		// sobre cdir si no tenim ja cdir
		else if (checkCanis)
		{
			if (nEx<5 && systems.size()!=0)
			{
//				cout << "getBestSubsys CheckCanis 1"<< endl;
				if (load>bestSys.load)
				{
					if (result!=NULL)
					{
						delete result;
						result=NULL;
					}
					
					Endian::to(Endian::xarxa, &bestSys.load, sizeof(bestSys.load));
	//				cout << "getBestSubsys CheckCanis 1 call"<< endl;
					result=new RPCPacket(Address(IP(bestSys.ip), bestSys.port), (word)-1, (byte*)&bestSys.load, sizeof(bestSys.load), type, 0, true);
					
					if (result==NULL)
					{
	//				cout << " getBestSS: Exc mem resp" << endl;
						throw (Exception(0, "Not enough memory to allocate response"));
					}
				}
			}
			else if (nEx>=5)
			{
				// El millor esta a bestAddr, nEx>=5 => systems.size()!=0, si no ja hauriem throwat excepcio
//				cout << "getBestSubsys CheckCanis 2 call"<< endl;
				if (result!=NULL)
					delete result;
				Endian::to(Endian::xarxa, &load, sizeof(load));
				result=new RPCPacket(Address(IP(bestSys.ip), bestSys.port), (word)-1, (byte*)&load, sizeof(load), type, 0, true);
	//			cout << "getBestSubsys CheckCanis 2.3"<< endl;
				
				if (result==NULL)
				{
	//				cout << " getBestSS: Exc mem resp 2" << endl;
					throw (Exception(0, "Not enough memory to allocate response"));
				}
			}
		}
//		 cout << "<- getBestSubsys(" << type << ")" << endl;
	}
	else
	{
		// Trobem el millor per canis
		list<Canis::subsys> systems;
		Canis::subsys bestSys;
		bool canisFoundBest=false;
		float load=99999999.0f;
	
		if (checkCanis)
		{
	//		 cout << "	canis search..." << endl;
			systems=cn->getTypeList(type);
			for (list<Canis::subsys>::iterator it=systems.begin(); 
				 it!=systems.end(); it++)
			{
				IP ipCanis((*it).ip);
				Address newAddr(ipCanis, (*it).port);

				if(moduleIsAliveThread->isAlive(newAddr))
				{
					if ((*it).load<load)
					{
						load=(*it).load;
						bestSys=(*it);
						canisFoundBest=true;
					}
				}
			}
		}
	
		if (!canisFoundBest)
			throw(Exception(0, "Could not find best subsystem by means of Canis"));		

		result=new RPCPacket(Address(IP(bestSys.ip), bestSys.port), (word)-1, (byte*)&load, sizeof(load), type, 0, true);
	}
	
		
//	cout<<"result :"<<type<<endl;
	result->setOrType(type);
//	cout<<"result"<<endl;
	return result;
}


RPCPacket* ServiceFinder::getNearestSubsys(string type, IP nearest, byte mask, Canis *cn)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
//	cout<<"----- SF::GetBestSS str:"<<type<<" resolve"<<endl;
	int nType=ServiceFinder::resolveSubsystemTypeName(type, cn);
//	cout<<"----- SF::GetBestSS str:"<<nType<<" getBSS"<<endl;
	RPCPacket *res=ServiceFinder::getNearestSubsys(nType, nearest, mask, cn);
//	cout<<"----- SF::GetBestSS res:"<<(void*)res<<endl;
	return res;
}

RPCPacket* ServiceFinder::getNearestSubsys(short type, IP nearest, byte mask, Canis *cn)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
//	 cout << "-> getBestSubsys(" << type << ")" << endl;
	Address addr(IP(string("0.0.0.0")), 0);
	RPCPacket *result=NULL;
	
	bool checkCanis=(cn!=NULL);
	
//	if (canisOnly && !checkCanis)
//		throw (Exception("Inconsistent parameters. Need Canis if canisOnly set to true"));
	
	if (cn==NULL)
	{
		cn=new Canis(addr, 0, true);
		if (cn==NULL)
		{
//		cout << " getBestSS: Exc mem" << endl;
			throw (Exception("Not enough memory to allocate temporal Canis"));
		}
	}
		
//	cout << "getBestSubsys get Best CDir" << endl;
	RPCPacket *dir=ServiceFinder::getBestCentralDirectory(cn, addr);
	
//	cout << "getBestSubsys get bcd = " << dir->a->toString() << endl;
	if (!checkCanis)
		delete cn;
		
	// Preguntem al central directory en asincron, i mentrestant demanem resposta tb al canis
	CentralDirectoryInterface::findBestSubsystemByTypeIdParams params;
	
	params.algorithm=CentralDirectoryInterface::NEAREST_BY_IP_GROUP;
	params.nearestByIpUntieAlgorithm=
		CentralDirectoryInterface::CENTRAL_DIRECTORY;
	params.nearestByIPIP=nearest.toDWord();
//	cout << "----> getNearestSubsys set Mask: " << (int)mask << endl;
	params.nearestByIPMask=mask;
//	cout << "----> getNearestSubsys set Mask: " << (int)params.nearestByIPMask << endl;
	params.type=type;

	void *paramsSer=params.toNetwork();

	RPCPacket pk(Address(IP("0.0.0.0"), 0), CentralDirectoryInterface::findBestSubsystemByTypeIdServiceId, (byte*)paramsSer, 
		params.serializationSize(), 0, ServiceFinder::centralDirectoryTypeId,
		true);

	delete [] (byte*)paramsSer;
	
	RPC rpc(*dir->a);
	
	// No ens cal dir mes...
	delete dir;
	
	// Aagfem el resultat de l'RPC
	int nEx=0;
	while (nEx<5)
	{
		try 
		{
			result=rpc.call(pk);
			break;
		}
		catch (SocketTimeoutException &ste)
		{
			nEx+=5;
		}
		catch (Exception &e)
		{
			cout << "getNearestSubsys: " << e.getClass() << ": " <<
				e.getMsg() << endl;
			nEx++;
		}
	}
		
	if (nEx>=5 || result==NULL)
	{
		cout << "getNearestSubsys throw!" << endl;
		throw(Exception(0, "Could not find nearest subsystem"));
	}	
//	cout<<"result :"<<type<<endl;
	result->setOrType(type);
//	cout<<"result"<<endl;
	return result;
}

RPCPacket* ServiceFinder::getNearestSubsysForDevice(string type, dword devId, byte mask, Canis *cn)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
	int nType=ServiceFinder::resolveSubsystemTypeName(type, cn);
	RPCPacket *res=ServiceFinder::getNearestSubsysForDevice(nType, devId, mask, cn);
	return res;
}

RPCPacket* ServiceFinder::getNearestSubsysForDevice(short type, dword devId, byte mask, Canis *cn)
{
	setupModuleIsAliveThread(cn);
	IP ip=ServiceFinder::getIPForDevice(devId, cn);
	return ServiceFinder::getNearestSubsys(type, ip, mask, cn);
}

RPCPacket* ServiceFinder::getNearestSubsysForSubsys(string type, dword subsysId, byte mask, Canis *cn)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
	int nType=ServiceFinder::resolveSubsystemTypeName(type, cn);
	RPCPacket *res=ServiceFinder::getNearestSubsysForSubsys(nType, subsysId, mask, cn);
	return res;
}

RPCPacket* ServiceFinder::getNearestSubsysForSubsys(short type, dword subsysId, byte mask, Canis *cn)
{
	setupModuleIsAliveThread(cn);
	IP ip=ServiceFinder::getIPForSubsys(subsysId, cn);
	return ServiceFinder::getNearestSubsys(type, ip, mask, cn);
}

list<Canis::subsys> ServiceFinder::getAllSubsystems(string type, Canis *cn, bool canisOnly, bool checkCanis)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
	return ServiceFinder::getAllSubsystems(ServiceFinder::resolveSubsystemTypeName(type, cn), cn, canisOnly, checkCanis);
}
	
list<Canis::subsys> ServiceFinder::getAllSubsystems(short type, Canis *cn, bool canisOnly, bool checkCanis)
{
	STACKTRACE_INSTRUMENT();
	setupModuleIsAliveThread(cn);
	
//	 cout << "-> getBestSubsys(" << type << ")" << endl;
	Address addr(IP(string("0.0.0.0")), 0);
	RPCPacket *result=NULL;
	
	bool newCanis=(cn==NULL);
	
	if (canisOnly && !checkCanis)
		throw (Exception("Inconsistent parameters. Need Canis if canisOnly set to true"));
	
	if (!canisOnly)
	{
		if (newCanis)
		{
			cn=new Canis(addr, 0, true);
			if (cn==NULL)
			{
	//		cout << " getBestSS: Exc mem" << endl;
				throw (Exception("Not enough memory to allocate temporal Canis"));
			}
		}
		
	//	cout << "getBestSubsys get Best CDir" << endl;
		RPCPacket *dir=ServiceFinder::getBestCentralDirectory(cn, addr);
		
	//	cout << "getBestSubsys get bcd = " << dir->a->toString() << endl;
		if (!checkCanis && newCanis)
		{
			delete cn;
			cn=NULL;
		}
		// Preguntem al central directory en asincron, i mentrestant demanem resposta tb al canis
		// 20091228: SUDEM del canis
		CentralDirectoryInterface::findAllSubsystemsByTypeIdParams params;
		params.type=type;
		params.nSubsys=0;
		
		byte *paramsData=(byte*)params.toNetwork();
	
		RPCPacket pk(Address(IP("0.0.0.0"), 0), CentralDirectoryInterface::findAllSubsystemsByTypeIdServiceId, paramsData, 
					params.sizeOf(), 0, ServiceFinder::centralDirectoryTypeId, true);
		RPC rpc(*dir->a);
		
		// No ens cal dir mes...
		delete [] paramsData;
		delete dir;
		
		// Enviem comanda per trobar el millor, asincronament, per directori central
	//	cout << "	async call CentralDir..." << type <<endl;
		rpc.callAsync(pk);
	
		list<Canis::subsys> systems;

		// Aagfem el resultat de l'RPC
		int nEx=0;
		while (nEx<5)
		{
			// De moment assumim que si hi ha excepcio es per timeout del socket => cal esperar mes
			try 
			{
	//			 cout << "	async call result..." << endl;
				result=rpc.getAsyncResult();
	//			 cout << "	async call result obtained..." << endl;
				break;
			}
			catch (RPCException rpce)
			{
	//			 cout << "	async call result EXCEPTION... " << rpce.getMsg() << endl;
				nEx++;
			}
			catch (SocketTimeoutException rpce)
			{
	//			 cout << "	async call result timeout exception... " << rpce.getMsg() << endl;
				nEx+=5;
			}
		}
		
		if (nEx>=5 && (!checkCanis || systems.size()==0))
		{
	//		cout << " getBestSS: Exc  \"Could not find best subsystem either by means of Canis nor Central Directory\"" << endl;
			throw(Exception(0, "Could not retrieve subsystem list either by means of Canis nor Central Directory"));
		}
		
		if(result!=NULL)
		{
			params.toLocal(result->getData());
		}	

		time_t now=time(NULL);

		for (dword s=0; s<params.nSubsys; s++)
		{
			Canis::subsys sys;
			
			sys.ip=params.subsystems[s].addr->getIP().toDWord();
			sys.port=params.subsystems[s].addr->getPort();
			sys.type=type;
			sys.load=params.subsystems[s].load;
			sys.last=now;
			bool found=false;
			// ja no comprovem si ja el teniem al canis, perque tenim en compte
			// nomes el que ens ve del CentralDirectory, que te en compte els
			// moduls que no responen :P
/*			list<Canis::subsys>::const_iterator i;
			for (i=systems.begin(); i!=systems.end(); i++)
			{
				Canis::subsys s=*i;
				if(s.ip == sys.ip && s.port == sys.port && s.type == sys.type)
					found=true;
			}
			if(!found)
*/
			systems.push_back(sys);
		}

		if (newCanis && cn!=NULL)
		{
			delete cn;
			cn=NULL;
		}

		//	 cout << "<- getBestSubsys(" << type << ")" << endl;
		return systems;
	}
	else
	{
		// Trobem el millor per canis
		list<Canis::subsys> systems;
		bool canisFoundBest=false;
	
		if (checkCanis)
		{
	//		 cout << "	canis search..." << endl;
			return cn->getTypeList(type);
		}
	
		if (!canisFoundBest)
			throw(Exception(0, "Could not retrieve subsystem list by means of Canis"));		
	}
	
	list<Canis::subsys> subsys;
	return subsys;
}

