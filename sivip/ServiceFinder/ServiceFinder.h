/*
 *  ServiceFinder.h
 *  
 *
 *  Created by David Marí Larrosa on 21/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_BASE_SERVICEFINDER_SERVICEFINDER_H_
#define SIRIUS_BASE_SERVICEFINDER_SERVICEFINDER_H_

#include <Utils/debugNew.h>
#include <Utils/Canis.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>
#include <Utils/PtrPool.h>
#include <Threads/RWlock.h>
#include <Module/ModuleIsAliveThread.h>

#include <list>
#include <map>
#include <string>

using namespace std;

class ServiceFinder
{
protected:
	static PtrPool ptrPool;

	static map<string, word> subsystemResolveCache;
	static map<word, string> subsystemInverseResolveCache;
	static RWLock cacheRWLock, invCacheRWLock;
	static Mutex creationLock;
	static ModuleIsAliveThread *moduleIsAliveThread;

	static int dbSocketProblemCounter;

	static RPC* getRPC(Address *a);
	
	static void setupModuleIsAliveThread(Canis *cn);
public:
	static const short centralDirectoryTypeId=-1;
	static const short dbGatewayTypeId=-2;

	static const short maxSocketProblemCount=2;

	static short registerSubsystemTypeName(string type, Canis *cn, bool debug=false);
	static short resolveSubsystemTypeName(string type, Canis *cn, bool debug=false);
	static string inverseResolveSubsystemTypeId(short type, Canis *cn);

	static IP getIPForDevice(dword devId, Canis *cn);
	static IP getIPForSubsys(dword subsysId, Canis *cn);
	static dword getSubsysIdForAddressAndType(Address addr, word typeId, Canis *cn);

	static RPCPacket* getBestCentralDirectory(Canis *cn, Address oldAddr);
	static RPCPacket* getBestSubsys(string type, Canis *cn, bool canisOnly=false);
	static RPCPacket* getBestSubsys(short type, Canis *cn, bool canisOnly=false);
	static RPCPacket* getNearestSubsys(string type, IP nearest, byte mask, Canis *cn);
	static RPCPacket* getNearestSubsys(short type, IP nearest, byte mask, Canis *cn);
	static RPCPacket* getNearestSubsysForDevice(string type, dword devId, byte mask, Canis *cn);
	static RPCPacket* getNearestSubsysForDevice(short type, dword devId, byte mask, Canis *cn);
	static RPCPacket* getNearestSubsysForSubsys(string type, dword subsysId, byte mask, Canis *cn);
	static RPCPacket* getNearestSubsysForSubsys(short type, dword subsysId, byte mask, Canis *cn);

	static list<Canis::subsys> getAllSubsystems(string type, Canis *cn, bool canisOnly=false, bool checkCanis=true);
	static list<Canis::subsys> getAllSubsystems(short type, Canis *cn, bool canisOnly=false, bool checkCanis=true);
};
#endif

