#ifndef __SIRIUS__MODULEACCESS__MODULEACCESS_H
#define __SIRIUS__MODULEACCESS__MODULEACCESS_H

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Utils/Exception.h>
#include <Utils/Canis.h>
#include <Sockets/Address.h>
#include <Threads/Mutex.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>

class ModuleAccess
{
public:
	static const int defaultTimeout = 10000;
	static const dword socketTimeoutExceptionThreshold = 2;
	
protected:
	Canis *cn;
	bool ownCanis;
	bool useFixedAddress;
	RPCPacket *mod;
	Address sockAddr;
	string typeIdStr;
	RPC *rpc;
	dword socketTimeoutExceptionCounter; 
	dword recvSocketBuffer;
	dword sendSocketBuffer;
	int currentTimeout;
	
	Mutex callMutex;
	
	void init(string type, dword devId, byte mask, Canis *cn=NULL, bool useFixedAddress=false);
	void init(word type, dword devId, byte mask, Canis *cn=NULL, bool useFixedAddress=false);
	
public:
	virtual string getClass();
	
	ModuleAccess(string type, Canis *cn=NULL);
	ModuleAccess(word type, Canis *cn=NULL);
	ModuleAccess(string type, Address modAddr, Canis *cn=NULL);
	ModuleAccess(word type, Address modAddr, Canis *cn=NULL);
	ModuleAccess(string type, dword devId, byte mask, Canis *cn=NULL);
	ModuleAccess(word type, dword devId, byte mask, Canis *cn=NULL);
	virtual ~ModuleAccess();
	
	virtual RPC* getRPC();
	virtual void closeRPC();
	
	virtual bool processExceptionFromRPC(Exception &e);
	
	virtual void processNullRPCResult(RPCPacket *rpcRes, string func,
		string file=string(""), int fileLine=-1);

	virtual void processNullRPCDataResult(RPCPacket *rpcRes, string func,
		string file=string(""), int fileLine=-1);

	virtual RPCPacket* createCallPacket(word service, void *params,
		dword paramsLen);
	
	virtual dword getRecvBuffer();
	virtual void setRecvBuffer(dword bytes);
	virtual dword getSendBuffer();
	virtual void setSendBuffer(dword bytes);

	virtual void setTimeout(dword ms);
	static bool isAlive(Address module, int type, int timeout = defaultTimeout);
	string getVersion();
	static string getVersion(Address module, int type, int timeout = defaultTimeout);
};

#endif

