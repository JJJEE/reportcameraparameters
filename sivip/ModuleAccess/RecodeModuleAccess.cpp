#include <ModuleAccess/RecodeModuleAccess.h>
#include <RecodeModule/RecodeModuleInterface.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <Sockets/SocketException.h>
#include <iostream>
#include <Utils/WindowsDefs.h>


using namespace std;

RecodeModuleAccess::RecodeModuleAccess(Canis *cn) : 
	ModuleAccess("RecodeModule", cn), sesId(), sesIdRecover()
{
}

RecodeModuleAccess::RecodeModuleAccess(word type, Canis *cn) : 
	ModuleAccess(type, cn), sesId(), sesIdRecover()
{
}

RecodeModuleAccess::RecodeModuleAccess(Address modAddr, Canis *cn) :
	ModuleAccess("RecodeModule", modAddr, cn), sesId(), sesIdRecover()
{
}

RecodeModuleAccess::RecodeModuleAccess(word type, Address modAddr,
	Canis *cn) :
	ModuleAccess(type, modAddr, cn), sesId(), sesIdRecover()
{
}

RecodeModuleAccess::~RecodeModuleAccess()
{
}

void RecodeModuleAccess::startSession(RPSession ses)
{
	callMutex.lock();

	if (!this->useFixedAddress)
	{
		RPCPacket *mod=this->mod;
		try
		{
			this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
			
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)ses.toNetwork();
		pk = 
			this->createCallPacket(RecodeModuleInterface::startSessionServiceId,
				params, ses.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		this->sesId=ses;
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
	}
	catch(RPSessionAlreadyStablishedException &e)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

void RecodeModuleAccess::endSession(RPSession ses)
{
	// Si no hi ha sessio iniciada no fem res :D
	if (this->sesId.in.name==string("") || this->sesId.out.name==string(""))
		return;
		
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)ses.toNetwork();
		pk = 
			this->createCallPacket(RecodeModuleInterface::endSessionServiceId,
				params, ses.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		this->sesId=RPSession();
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
	}
	catch(RPSessionNotStablishedException &e)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

void RecodeModuleAccess::processFrame(RPFrame frame)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)frame.toNetwork();
		pk = 
			this->createCallPacket(RecodeModuleInterface::processFrameServiceId,
				params, frame.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
	}
	catch(Exception &e)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

RPFrame RecodeModuleAccess::getFrame()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(RecodeModuleInterface::getFrameServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		RPFrame res(rpcRes->data);
		delete rpcRes;
		
		return res;
	}
	catch(Exception &e)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return RPFrame();
}

bool RecodeModuleAccess::processExceptionFromRPC(Exception &e)
{
	if (e.getClass()==string("SocketTimeoutException"))
		this->socketTimeoutExceptionCounter++;
	else if (e.getClass()==string("SocketException") || 
		e.getClass()==string("RPCException"))
		this->closeRPC();
		
	if (e.getClass()==string("SocketException") ||
		(e.getClass()==string("SocketTimeoutException") && 
		this->socketTimeoutExceptionCounter >= 
			ModuleAccess::socketTimeoutExceptionThreshold))
	{
		// Probablement necessitem buscar un nou modul, ja que el nostre
		// deu haver caigut
		if (!this->useFixedAddress)
		{
			RPCPacket *mod=this->mod;
			try
			{
				this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
				if (mod!=NULL)
					delete mod;

				this->socketTimeoutExceptionCounter=0;
			}
			catch (...)
			{
				this->mod=mod;
			}
		}
//		e.serialize()->materializeAndThrow(true);
		return true;
	}
	else if (e.getClass()==string("ServiceException") || e.getClass()==string("RPSessionNotStablishedException") || e.getClass()==string("RPCException"))
	{
		this->socketTimeoutExceptionCounter=0;
		if (sesId.in.name!=string("") && sesId.out.name!=string(""))
		{
			cout << "Recovering from " << e.getClass() << " - searching new RecodeModule and starting session" << endl;
			
			sesIdRecover=sesId;
			
			sesId=RPSession();
			
			try
			{
				this->startSession(sesIdRecover);
			}
			catch (Exception &sse)
			{
				if (sesId.in.name==string("") || sesId.out.name==string(""))
					sesId=sesIdRecover;
//				e.serialize()->materializeAndThrow(true);
				return true;
			}
		}
		else
//			e.serialize()->materializeAndThrow(true);
			return true;
	}
	else
	{
		this->socketTimeoutExceptionCounter=0;
//		e.serialize()->materializeAndThrow(true);
		return true;
	}
	return false;
}
