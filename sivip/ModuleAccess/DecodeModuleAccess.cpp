#include <ModuleAccess/DecodeModuleAccess.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <Utils/debugStackTrace.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <Sockets/SocketException.h>
#include <Plugins/PluginImage.h>
#include <Plugins/GestorImageExceptions.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Utils/WindowsDefs.h>

using namespace std;

DecodeModuleAccess::DecodeModuleAccess(Canis *cn) : 
	ModuleAccess("DecodeModule", cn), devId(-1), devIdRecover(-1),
	mode(IPStreamingMode::STREAM)
{
	this->setRecvBuffer(2*1024*1024);	// 2MB per un sol stream, de sobres
	this->setSendBuffer(512*1024);
}

DecodeModuleAccess::DecodeModuleAccess(word type, Canis *cn) : 
	ModuleAccess(type, cn), devId(-1), devIdRecover(-1),
	mode(IPStreamingMode::STREAM)
{
	this->setRecvBuffer(2*1024*1024);	// 2MB per un sol stream, de sobres
	this->setSendBuffer(512*1024);
}

DecodeModuleAccess::DecodeModuleAccess(Address modAddr, Canis *cn) :
	ModuleAccess("DecodeModule", modAddr, cn), devId(-1), devIdRecover(-1),
	mode(IPStreamingMode::STREAM)
{
	this->setRecvBuffer(2*1024*1024);	// 2MB per un sol stream, de sobres
	this->setSendBuffer(512*1024);
}

DecodeModuleAccess::DecodeModuleAccess(word type, Address modAddr,
	Canis *cn) :
	ModuleAccess(type, modAddr, cn), devId(-1), devIdRecover(-1),
	mode(IPStreamingMode::STREAM)
{
	this->setRecvBuffer(2*1024*1024);	// 2MB per un sol stream, de sobres
	this->setSendBuffer(512*1024);
}

DecodeModuleAccess::DecodeModuleAccess(dword devId, byte mask, Canis *cn) :
	ModuleAccess("DecodeModule", devId, mask, cn), devId(devId), devIdRecover(-1),
	mode(IPStreamingMode::STREAM)
{
	this->setRecvBuffer(2*1024*1024);	// 2MB per un sol stream, de sobres
	this->setSendBuffer(512*1024);
}

DecodeModuleAccess::DecodeModuleAccess(word type, dword devId, byte mask,
	Canis *cn) :
	ModuleAccess(type, devId, mask, cn), devId(devId), devIdRecover(-1),
	mode(IPStreamingMode::STREAM)
{
	this->setRecvBuffer(2*1024*1024);	// 2MB per un sol stream, de sobres
	this->setSendBuffer(512*1024);
}

DecodeModuleAccess::~DecodeModuleAccess()
{
}

void DecodeModuleAccess::startSession(IPDeviceID dev)
{
	callMutex.lock();

	if (	!this->useFixedAddress	)
	{
		RPCPacket *mod=this->mod;
		try
		{
			cout	<<	"DecodeModuleAccess::startSession ["	<<	dev.id	<<	"]: searching for best Subsys"	<<endl;

			this->mod = ServiceFinder::getNearestSubsysForDevice(	this->typeIdStr, 
																					dev.id, 
																					this->cn->getInterfaceMask(), 
																					this->cn	);
			
			cout<<"DecodeModuleAccess::startSession ["<<dev.id<<"] mod:"<<this->mod->a->toString()<<endl;

			if (mod!=NULL)
				delete mod;
		}
		catch (Exception &e)
		{
			if(mod == NULL)
				cout<<"DecodeModuleAccess::startSession Exception in getNearestSubSysForDevice ["<<dev.id<<"] , "<<e.getClass()<<":"<<e.getMsg()<<" WARNING:mod==NULL"<<endl;
			else
				cout<<"DecodeModuleAccess::startSession Exception in getNearestSubSysForDevice ["<<dev.id<<"] , "<<e.getClass()<<":"<<e.getMsg()<<" using old Module:"<<mod->a->toString()<<endl;

			this->mod=mod;
		}
		catch (...)
		{
			if(mod == NULL)
				cout<<"DecodeModuleAccess::startSession unknown Exception in getNearestSubSysForDevice ["<<dev.id<<"] , WARNING:mod==NULL"<<endl;
			else
				cout<<"DecodeModuleAccess::startSession unknown Exception in getNearestSubSysForDevice ["<<dev.id<<"] , using old Module:"<<mod->a->toString()<<endl;
			this->mod=mod;
		}
	}

	RPC *rpc=this->getRPC();

	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		this->devId=dev;
		byte *params=(byte*)dev.toNetwork();
		pk = 
			this->createCallPacket(	PluginImage::startSessionServiceId,
											params, 
											dev.serializationSize()	);
		
		delete [] params;
		
		cout<<"DecodeModuleAccess::startSession rpc::call ["<<dev.id<<"] @"<<this->mod->a->toString()<<endl;
		rpcRes=rpc->call(*pk);			
		cout<<"DecodeModuleAccess::startSession /rpc::call ["<<dev.id<<"] @"<<this->mod->a->toString()<<endl;
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(IPSessionAlreadyStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		this->devIdRecover = dev;
		cout<<"void DecodeModuleAccess::startSession(IPDeviceID dev) processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

void DecodeModuleAccess::endSession(IPDeviceID dev)
{
	// Si no tenim sessió iniciada no fem res :)
	if(devId.id<=0)
		return;

	callMutex.lock();
	
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)dev.toNetwork();
		pk = 
			this->createCallPacket(PluginImage::endSessionServiceId,
				params, dev.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		this->devId=IPDeviceID(-1);
		this->devIdRecover=IPDeviceID(-1);
		this->mode=IPStreamingMode(IPStreamingMode::STREAM);
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		
		// Forcem que per la propera sessio es faci servir un socket diferent.
		this->closeRPC();
		this->socketTimeoutExceptionCounter=0;
	}
	catch(IPSessionNotStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::endSession(IPDeviceID dev) processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

IPDeviceInfo DecodeModuleAccess::getDeviceInformation(IPDeviceInfo dev)
{
	callMutex.lock();

	if (!this->useFixedAddress)
	{
		RPCPacket *mod=this->mod;
		try
		{
//			this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
			this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, dev.id.id, this->cn->getInterfaceMask(), this->cn);
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
			
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)dev.toNetwork();
		pk = 
			this->createCallPacket(PluginImage::getDeviceInformationServiceId,
				params, dev.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		IPDeviceInfo res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::getDeviceInformation() processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return IPDeviceInfo();
}

IPCodecInfo DecodeModuleAccess::getCodecInUse()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginImage::getCodecInUseServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		IPCodecInfo res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::getCodecInUse() processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return IPCodecInfo();
}

void DecodeModuleAccess::setCodecInUse(IPCodecInfo codec)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)codec.toNetwork();
		pk = 
			this->createCallPacket(PluginImage::setCodecInUseServiceId,
				params, codec.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::setCodecInUse() processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

IPFrameBufferSize DecodeModuleAccess::getFrameBufferSize()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginImage::getFrameBufferSizeServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		IPFrameBufferSize res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return IPFrameBufferSize();
}

void DecodeModuleAccess::setFrameBufferSize(IPFrameBufferSize size)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)size.toNetwork();
		pk = 
			this->createCallPacket(PluginImage::setFrameBufferSizeServiceId,
				params, size.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

float DecodeModuleAccess::getFrameBufferPercentInUse()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(
				PluginImage::getFrameBufferPercentInUseServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		float res=*(float*)rpcRes->data;
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return 0.0f;
}

int DecodeModuleAccess::getFrameBufferFramesInUse()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(
				PluginImage::getFrameBufferFramesInUseServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		int res=*(float*)rpcRes->data;
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return 0;
}


IPStreamingMode DecodeModuleAccess::getStreamingMode()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginImage::getStreamingModeServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		IPStreamingMode res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::getStreamingMode() processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return IPStreamingMode(IPStreamingMode::STREAM);
}

void DecodeModuleAccess::setStreamingMode(IPStreamingMode mode)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)mode.toNetwork();
		pk = 
			this->createCallPacket(PluginImage::setStreamingModeServiceId,
				params, mode.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::setStreamingMode() processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

IPFrame DecodeModuleAccess::getCompressedNextFrame()
{
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
	callMutex.lock();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

	RPC *rpc=this->getRPC();
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		pk = 
			this->createCallPacket(PluginImage::getCompressedNextFrameServiceId,
				NULL, 0);
		
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		rpcRes =rpc->call(*pk);			
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		if (callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		else
			cout << __FILE__ << " Line " << __LINE__ << " callMutex NO lockat" << endl;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		delete pk;
		pk=NULL;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		IPFrame res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

		return res;
	}
	catch(Exception &e)
	{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		if (callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		else
			cout << __FILE__ << " Line " << __LINE__ << " callMutex NO lockat" << endl;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::getCompressedNextFrame() dev:"<<devId.id<<" processExceptionFromRPC:"<<e.getClass()<<endl;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		if (this->processExceptionFromRPC(e))
		{
#ifdef REC_SAVEFRAME_DEBUG
			if (sfdDebug!=NULL)
				*sfdDebug=0x20000000 | __LINE__;
#endif
			throw;
		}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
	}
	catch (...)
	{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
		if (callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		else
			cout << __FILE__ << " Line " << __LINE__ << " callMutex NO lockat" << endl;
		if (pk!=NULL)
			delete pk;
	}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

	return IPFrame();
}

IPFrame DecodeModuleAccess::getDecompressedNextFrame()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(
				PluginImage::getDecompressedNextFrameServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		IPFrame res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return IPFrame();
}

IPFramesPerSecond DecodeModuleAccess::getFPS()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginImage::getFPSServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		IPFramesPerSecond res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::getFPS() processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return IPFramesPerSecond();
}

void DecodeModuleAccess::setFPS(IPFramesPerSecond fps)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)fps.toNetwork();
		pk = 
			this->createCallPacket(PluginImage::setFPSServiceId,
				params, fps.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"void DecodeModuleAccess::setFPS() processExceptionFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

bool DecodeModuleAccess::processExceptionFromRPC(Exception &e)
{
#ifdef REC_SAVEFRAME_DEBUG
	int *sfdDebug = Thread::ttdGetPointer();
#endif

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

	if (e.getClass()==string("SocketTimeoutException"))
		this->socketTimeoutExceptionCounter++;
	else if (e.getClass()==string("SocketException") || 
		e.getClass()==string("RPCException"))
		this->closeRPC();
		
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

	if (e.getClass()==string("SocketException") ||
		(e.getClass()==string("SocketTimeoutException") && 
		this->socketTimeoutExceptionCounter >= 
			ModuleAccess::socketTimeoutExceptionThreshold))
	{
		// Probablement necessitem buscar un nou modul, ja que el nostre
		// deu haver caigut
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

		if (!this->useFixedAddress)
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

			RPCPacket *mod=this->mod;
			try
			{
				if (devId.id!=-1 && devIdRecover.id!=-1)
				{
					int id; 
					if (devId.id!=-1)
						id=devId.id;
					else
						id=devIdRecover.id;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

					this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, id, this->cn->getInterfaceMask(), this->cn);
				}
				else
				{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

					this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
				}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

				if (mod!=NULL)
					delete mod;

#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

				this->socketTimeoutExceptionCounter=0;
			}
			catch (...)
			{
				this->mod=mod;
			}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

		}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

//		e.serialize()->materializeAndThrow(true);
		return true;
	}
	else if (e.getClass()==string("ServiceException") || e.getClass()==string("IPSessionNotStablishedException") || e.getClass()==string("RPCException"))
	{
		this->socketTimeoutExceptionCounter=0;
		if (devId.id>0)
		{
			cout << "Recovering from " << e.getClass() << "(" << e.getMsg() << ") ["<<devId.id<<"]- searching new DecodeModule and starting session" << endl;
			
			devIdRecover=devId;
			
			devId=IPDeviceID(-1);
			
			try
			{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

				cout << "Recovering from " << e.getClass() << " ["<<devId.id<<"] - startSession" << endl;
				this->startSession(devIdRecover);
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

				cout << "Recovering from " << e.getClass() << " ["<<devId.id<<"] - setStreamingMode" << endl;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

				this->setStreamingMode(this->mode);
			}
			catch (Exception &sse)
			{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

				cout << "Exception while recovering from:" << e.getClass() << " ["<<devId.id<<"] : "<<sse.getClass()<<"::"<<e.getMsg() << endl;
				if (devId.id==-1)
					devId=devIdRecover;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

//				e.serialize()->materializeAndThrow(true);
				return true;
			}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

		}
		else
		{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

			cout << "WARNING: Not Recovering from " << e.getClass() << " devID:"<< devId <<" recover:"<< devIdRecover <<" searching new DecodeModule and starting session" << endl;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

//			e.serialize()->materializeAndThrow(true);
			return true;
		}
	}
	else
	{
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

		if(e.getClass()!=string("SocketTimeoutException"))
				this->socketTimeoutExceptionCounter=0;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

// 		e.serialize()->materializeAndThrow(true);
		return true;
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif

	}
#ifdef REC_SAVEFRAME_DEBUG
	if (sfdDebug!=NULL)
		*sfdDebug=0x20000000 | __LINE__;
#endif
	return false;
}
