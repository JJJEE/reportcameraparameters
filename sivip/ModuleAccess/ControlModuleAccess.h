#pragma once
#include <Utils/debugNew.h>
#include <Sockets/Address.h>
#include <Utils/Exception.h>
#include <ModuleAccess/ModuleAccess.h>
#include <Plugins/GestorControlTypes.h>

class ControlModuleAccess : public ModuleAccess
{
private:
	CPDeviceID devId, devIdRecover;

public:
	ControlModuleAccess(Canis *cn=NULL);
	ControlModuleAccess(word type, Canis *cn=NULL);
	ControlModuleAccess(string type, Canis *cn=NULL);
	ControlModuleAccess(Address modAddr, Canis *cn=NULL);
	ControlModuleAccess(word type, Address modAddr, Canis *cn=NULL);
	ControlModuleAccess(dword devId, byte mask, Canis *cn=NULL);
	ControlModuleAccess(word type, dword devId, byte mask, Canis *cn=NULL);
	virtual ~ControlModuleAccess();

	bool sessionStarted();

	void startSession(CPDeviceID idservei);
	void endSession(CPDeviceID id);

	CPPTZ getPTZ();
	void setPTZ(CPPTZ ptz);

	void setInput(int in, int value);
	void setInput(CPInput in);
	int getInput(int in);
	int getInput(int in, int value);
	CPInput getInput(CPInput in);

	void setOutput(int in, int value);
	void setOutput(int in, int value, int pulse);
	void setOutput(CPOutput out);
	int getOutput(int out);
	CPOutput getOutput(CPOutput out);

	void setCommandBufferSize(CPCommandBufferSize size);
	CPCommandBufferSize getCommandBufferSize();

	float getCommandBufferPercentInUse();
	int getCommandBufferCommandsInUse();
	
	string getMetadataValue(string name);
	CPMetadata getMetadataValue(CPMetadata data);
	void setMetadataValue(string name, string value);
	void setMetadataValue(CPMetadata data);

	void setMetadataValueAsync(CPMetadata data);

	CPConfigParam getConfigParam(CPConfigParam p);
	CPConfigParamSeq getConfigParamRecursive(CPConfigParam p);
	string getConfigParam(string path);
	void setConfigParam(CPConfigParam p);
	void setConfigParam(string path, string value);
	CPConfigParamSeq getAllConfigParams();

	void move(CPMovement move);

	virtual bool processExceptionFromRPC(Exception &e);
	
	// Aberracio. TODO 20060808: Desaberrar
	RPCPacket *getAsyncResult();
	void ignoreAsyncResult();
	// Fi aberracio
};

