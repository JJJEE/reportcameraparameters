#include <Utils/debugStackTrace.h>
#include <Http/HttpClient.h>
#include <ModuleAccess/ConcurrencyArbiterModuleAccess.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Sockets/SocketException.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <XML/xmlParser.h>
#include <iostream>
using namespace std;

ConcurrencyArbiterModuleAccess::ConcurrencyArbiterModuleAccess(Canis *cn) : 
	ModuleAccess("ConcurrencyArbiterModule", cn)
{
}

ConcurrencyArbiterModuleAccess::ConcurrencyArbiterModuleAccess(word type, Canis *cn) : 
	ModuleAccess(type, cn)
{
}

ConcurrencyArbiterModuleAccess::ConcurrencyArbiterModuleAccess(Address modAddr, Canis *cn) :
	ModuleAccess("ConcurrencyArbiterModule", modAddr, cn)
{
}

ConcurrencyArbiterModuleAccess::ConcurrencyArbiterModuleAccess(word type, Address modAddr,
	Canis *cn) :
	ModuleAccess(type, modAddr, cn)
{
}

ConcurrencyArbiterModuleAccess::~ConcurrencyArbiterModuleAccess()
{
}



string ConcurrencyArbiterModuleAccess::lock(string name)
{
	STACKTRACE_INSTRUMENT();
	
	if (!this->useFixedAddress)
	{
		RPCPacket *mod=this->mod;
		try
		{
			this->mod=ServiceFinder::getBestSubsys("ConcurrencyArbiterModule", this->cn);
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
			
	ConcurrencyArbiterModuleInterface::namedMutexLockServiceParams nmlParams;
	nmlParams.inName=name;
	
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL;
	RPCPacket *ret=NULL;
	string code;
	
	try
	{
		dword nmlParamsLen=nmlParams.size();
		void *nmlParamsData=nmlParams.toNetwork();
	
		pk = 
		this->createCallPacket(
			ConcurrencyArbiterModuleInterface::namedMutexLockServiceId,
			nmlParamsData, nmlParamsLen);

		delete [] (byte*)nmlParamsData;
		
		ret=rpc->call(*pk);
		delete pk;
		
		nmlParams.toLocal(ret->getData());

		delete ret;
	}
	catch(Exception &e)
	{
		if (ret!=NULL)
			delete ret;
		if (rpc!=NULL)
			delete rpc;
		//e.serialize()->materializeAndThrow();
		throw;
	}
	
	return nmlParams.outCode;
}

void ConcurrencyArbiterModuleAccess::unlock(string name, string code)
{
	STACKTRACE_INSTRUMENT();

	if (!this->useFixedAddress)
	{
		RPCPacket *mod=this->mod;
		try
		{
			this->mod=ServiceFinder::getBestSubsys("ConcurrencyArbiterModule", this->cn);
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
	
	ConcurrencyArbiterModuleInterface::namedMutexUnlockServiceParams nmuParams;
	nmuParams.inName=name;
	nmuParams.inCode=code;
	
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL;
	RPCPacket *ret=NULL;
	
	try
	{
		dword nmuParamsLen=nmuParams.size();
		void *nmuParamsData=nmuParams.toNetwork();
	
		pk = 
		this->createCallPacket(
			ConcurrencyArbiterModuleInterface::namedMutexUnlockServiceId,
			nmuParamsData, nmuParamsLen);
		
		delete [] (byte*)nmuParamsData;
		
		RPCPacket *ret=rpc->call(*pk);
		delete pk;
		
		delete ret;
	}
	catch(Exception &e)
	{
		if (ret!=NULL)
			delete ret;
		if (rpc!=NULL)
			delete rpc;
		//e.serialize()->materializeAndThrow();
		throw;
	}
}
