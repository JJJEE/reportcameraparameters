#include <Http/HttpClient.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <AlarmModule/AlarmModuleException.h>
#include <base/Utils/ServiceException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <XML/xmlParser.h>
#include <iostream>
#include <algorithm>
#include <math.h>
using namespace std;



AlarmManager::AlarmManager(IP local): recv(local,0), stayRunning(true), running(false), socket(recv, SOCK_SERVE)
{
	cout<<"AM::AM socket:"<< socket.getAddr().toString()<<endl;
}

void* AlarmManager::execute(int thrId, void* param)
{
	Address recv;
	int retries=0;
	try
	{

		socket.setTimeout(1000);
		while(stayRunning)
		{
			RPCPacket *r=NULL;
			try
			{
				Address addr;
				r=RPC::receivePacket(&socket, &addr);
			}
			catch(SocketTimeoutException se)
			{
				if(se.getMsg() != string("No bytes to read"))
				{
					cout<<"AlarmModuleAccess::getAsyncResult SocketTimeoutException :"<<se.getMsg()<<endl;
				}
				r=NULL;
			}

			if(r!=NULL && r->data!=NULL)
			{
				cout<<"AlarmModuleAccess::waitForAlarm received:"<<(void*) r->data<<" : "<<r->size<<endl;

				AMAlarmValue av;
				av.toLocal((char*)r->data, r->size);

				this->activeAlarmsLock.wlock();
				activeAlarms.erase(av.alarmId);
				if (av.raised)
					activeAlarms[av.alarmId]=av;
				this->activeAlarmsLock.unlock();

				this->inactiveAlarmsLock.wlock();
				inactiveAlarms.erase(av.alarmId);
				if (!av.raised)
					inactiveAlarms[av.alarmId]=av;
				this->inactiveAlarmsLock.unlock();
			}
/*			else
			{
				cout<<" waitForAlm recv  res:"<<(void*)r<<" data:";
				if(r!=NULL)
					cout<<(void*)r->data;
				cout<<endl;
				{
					retries++;
					if(retries==5)
					{
						cout<<"clear"<<endl;
						dataMutex.lock(); 
						valors.clear();
						dataMutex.unlock();
						retries=0;
					}
				}
			}
*/
			if(r!=NULL)
				delete r;
		}
	}
	catch(...){}

//	dataMutex.lock();
	running=false;
//	dataMutex.unlock();
	return NULL;
}

Address  AlarmManager::getAddr()
{
	cout<<"AM::AM getAddr:"<< socket.getLocalAddr().toString()<<endl;
	return socket.getLocalAddr();
}

bool AlarmManager::getAlarmStatus(AMAlarmId id)
{
	this->activeAlarmsLock.rlock();
	bool ret = (this->activeAlarms.find(id)==this->activeAlarms.end());
	this->activeAlarmsLock.unlock();
	return ret;
}

list<AMAlarmValue> AlarmManager::getActiveAlarms()
{
	this->activeAlarmsLock.rlock();
	list<AMAlarmValue> ret;
	map<AMAlarmId, AMAlarmValue>::iterator aIt = this->activeAlarms.begin();

	for (; aIt!=this->activeAlarms.end(); aIt++)
		ret.push_back(aIt->second);
	
	this->activeAlarmsLock.unlock();
	return ret;
}

void AlarmManager::monitorAlarms()
{
//	dataMutex.lock();
	try{
		stayRunning=true;
		if(!running)
		{
			running=true;
			this->start();
		}
	}catch(Exception e){}
//	dataMutex.unlock();
}

void AlarmManager::endMonitoring()
{
//	dataMutex.lock();
	try{
		stayRunning=false;
	}catch(Exception e){}
//	dataMutex.unlock();
}


void AlarmModuleAccess::findBestAlarmModule(AMDeviceID dev)
{

	try
	{
		while(a.getIP().toString()==string("0.0.0.0"))
		{
			//	cout<<"AlarmModuleAcces::findBestAM:"<<servei.id<<" "<<a.toString()<<endl;
//			RPCPacket *plugin=ServiceFinder::getBestSubsys("AlarmModule", cn);
			RPCPacket *plugin=NULL;
			if(dev.id >= 0)
				plugin = ServiceFinder::getNearestSubsysForDevice("AlarmModule", dev.id, this->cn->getInterfaceMask(), this->cn);
			else //-1 alms de sistema
				plugin = ServiceFinder::getBestSubsys("AlarmModule", cn);
			this->socketTimeoutExceptionCounter=0;
			if(plugin != NULL)
			{
				a=*plugin->a;
				destType=plugin->origen;
				delete plugin;
			//			break;

				cout<<"AlarmModuleAcces::foundBestAM dev:"<<dev.id<<" :"<<servei.id<<" "<<a.toString()<<endl;
			}
		}
	}catch(Exception &e)
	{
//		cout<<"AlarmModuleAcces::findBestAM Exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
//		a=Address(IP("0.0.0.0"),0);
//		destType=0;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
}

CallData AlarmModuleAccess::sesCall(CallData cdp)
{	
	try
	{
		if(a.getIP().toString()==string("0.0.0.0"))
			findBestAlarmModule(servei);
		return call(cdp);
	}
	catch(AMSessionNotStablishedException &e) // s'ha perdut la sessió, reestablim 
	{
		this->socketTimeoutExceptionCounter=0;
		cout<<"AMA::sesCall::AMSessionNotStablishedException:"<<e.getMsg()<<endl;
		startSession(servei);
		sessions--; //"no compta"
		return call(cdp);
	}
	catch(Exception &e)
	{
		cout<<"AMA::sesCall::Exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		if (e.getClass()==string("SocketTimeoutException"))
		{
			this->socketTimeoutExceptionCounter++;

			if(this->socketTimeoutExceptionCounter >= 
					ModuleAccess::socketTimeoutExceptionThreshold)
			{
				a=Address(IP("0.0.0.0"),0);
				this->findBestAlarmModule(servei);
			}
		}
		else
			this->socketTimeoutExceptionCounter=0;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
}

AlarmModuleAccess::AlarmModuleAccess(Address orig, string type, Canis *cn): Gestor(orig, type, cn), sessions(0), callMutex(true), running(false), stayRunning(true), dataMutex(true), sessionStarted(false), thId0(-1), manager(NULL)
{
	a=Address(IP("0.0.0.0"),0);
	servei.id = -99;
//	thId0=start((void*)0);
//	thId2=start((void*)2);
}

AlarmModuleAccess::AlarmModuleAccess(Address orig, int type, Canis *cn): Gestor(orig, type, cn), sessions(0), callMutex(true), running(false), stayRunning(true), sessionStarted(false), dataMutex(true), thId0(-1), manager(NULL)
{
	a=Address(IP("0.0.0.0"),0);
	servei.id = -99;
//	thId0=start((void*)0);
//	thId2=start((void*)2);
}

AlarmModuleAccess::AlarmModuleAccess(string file, Canis *cn): Gestor(file, cn), sessions(0), callMutex(true), running(false), stayRunning(true), sessionStarted(false), dataMutex(true), thId0(-1)
{
	a=Address(IP("0.0.0.0"),0);
	servei.id = -99;
//	thId0=start((void*)0);
//	thId2=start((void*)2);
}

AlarmModuleAccess::~AlarmModuleAccess()
{
	stayRunning=false;
	cout << "~ama() -> stayRunning=false" << endl;
	if (thId0!=-1)
		this->join(thId0);
	cout << "~ama() -> joined thId0: " << thId0 << endl;
//	if (thId2!=-1)
//		this->join(thId2);
//	cout << "~ama() -> joined thId2: " << thId2 << endl;
//	if (thId1!=-1)
//		this->endMonitoring();
//	cout << "~ama() -> joined thId1: " << thId1 << endl;
//	this->killAll(); //threads
}

void AlarmModuleAccess::setManager(AlarmManager *manager)
{
	this->manager=manager;
}

bool AlarmModuleAccess::sesStarted()
{
	return servei.id != -99;
	//	return devId.id!=-1;
}

void AlarmModuleAccess::startSession(AMDeviceID idservei)
{
	dataMutex.lock();
	servei=idservei;
	servei.toNetworkEndian();
	dataMutex.unlock();

	callMutex.lock();
	try
	{
		findBestAlarmModule(idservei);

		CallData cdp(a, AMServ::startSession, &servei, sizeof(servei),destType);
		cout<<"  											call startSes "<<servei.id<<endl;
		call(cdp);
		sessions++;
		cout<<"session started"<<endl;
		sessionStarted=true;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(SocketException &e)
	{
		cout<<" AlarmModuleAccess::startSession SocketException:"<<e.getClass()<<" : "<<e.getMsg();
		a=Address(IP("0.0.0.0"),0);
		sessions=0;
		//callMutex.unlock();
		//e.serialize()->materializeAndThrow();
	}
	catch(AMSessionAlreadyStablishedException &e) //ja establerta, ignorém
	{
		sessions++;
		cout<<" :session started, stay:"<<stayRunning<<endl;
		sessionStarted=true;
	}
	catch(Exception &e)
	{
		//callMutex.unlock();
		//e.serialize()->materializeAndThrow();
		sessionStarted=false;
		cout<<": AlarmModuleAccess::startSession exception:"<<e.getClass()<<" : "<<e.getMsg();
		//			string s("Unregistered type");
		//			if(e.getMsg().compare(0,s.size(), s) == 0)
		//				sleep(5);
	}
	//		catch(ServiceException &se)
	//		{
	//	cout<<" AlarmModuleAccess::startSession ServiceException:"<<se.getClass()<<" : "<<se.getMsg();
	//			sessionStarted=false;
	//				cout<<"thr1: "<<thrId<<" :AlarmModuleAccess::execute startSession ServiceExc"<<endl;
	//				sleep(1); //no està iniciat el modul, podia ser que no estés instalat al sistema
	//		}
	servei.toLocalEndian();

	callMutex.unlock();
}


void AlarmModuleAccess::endSession(AMDeviceID idservei)
{
//	cout << "-------> AlarmModuleAccess endSession" << endl;
	dataMutex.lock();
	stayRunning=false;
//	if(!sessionStarted)
//	{
//	cout << "-------> AlarmModuleAccess endSession kill" << endl;
//		try{
//			this->stayRunning=false;//kill()
//		}catch(Exception e){}
//		dataMutex.unlock();
//		return;	
//	}
//	cout << "-------> AlarmModuleAccess endSession no kill" << endl;
	dataMutex.unlock();

	callMutex.lock();
	try
	{
		if(idservei.id!=servei.id)
		{
			throw (Exception(0, string(" Session not stablished with this device for this AlarmModuleAccess:")+StrUtils::decToString(idservei.id)+string("!=")+StrUtils::decToString(servei.id)));
		}
		sessions--;
		if(sessions==0)
		{
			idservei.toNetworkEndian();
			CallData cd(a, AMServ::endSession, &idservei, sizeof(idservei),destType);
			call(cd);
			this->socketTimeoutExceptionCounter=0;

		}
	}
	catch(AMSessionNotStablishedException &e)
	{}
	catch(Exception &e)
	{
		a=Address(IP("0.0.0.0"),0);
		callMutex.unlock();
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	a=Address(IP("0.0.0.0"),0);
	callMutex.unlock();
}


void AlarmModuleAccess::configureAlarm(AMAlarmConfig data)
{

	confCond.lock();
	if(thId0 == -1)
	{
		stayRunning=true;
		thId0=start((void*)0);
	}
	try{
		configuredAlarms.push_back(data);
		confCond.signal();
	}catch(Exception e){}
	confCond.unlock();
}
/*
void AlarmModuleAccess::removeAlarm(AMAlarmConfig data)
{

	confCond.lock();
	try{
		list<AMAlarmConfig>::iterator it=std::find(configuredAlarms.begin(), configuredAlarms.end(), data);
		if(it != configuredAlarms.end())
			configuredAlarms.erase(it);
//		confCond.signal();
	}catch(Exception e){}
	confCond.unlock();
}
*/
void AlarmModuleAccess::callConfigAlarm(AMAlarmConfig data)
{
	int sz=data.size();
	char *v=data.toNetwork();

	CallData cd(a, AMServ::configureAlarm, v, sz,destType);
	delete [] v;
	callMutex.lock();
	try
	{
	//	sesCall(cd);
//					cout<<"callConfigAlm "<<endl;
		sesCall(cd);
//					cout<<"/callConfigAlm "<<endl;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
					cout<<"callConfigAlm exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		callMutex.unlock();
//		cout<<"materializeAndThrow"<<endl;
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	callMutex.unlock();
}

void AlarmModuleAccess::setAlarm(AMAlarmValue data)
{
	//sentCond.lock();
//	try{
//		cout<<"SentAlarms 1 :"<<sentAlarms.size()<<"   :"<<data.alarmId.devId<<": type:"<<data.alarmId.type<<" str:"<<data.alarmId.strId<<" "<<data.value<<":"<<data.raised<<endl;
	//	sentAlarms.remove(data);
//		cout<<"SentAlarms 2 :"<<sentAlarms.size()<<endl;
	//	sentAlarms.push_back(data);
//		cout<<"SentAlarms 3 :"<<sentAlarms.size()<<endl;
	//	sentCond.signal();
//	}catch(Exception e){}
	//sentCond.unlock();
//	cout<<"AMA.setAlarm callSetAlm"<<endl;
	callSetAlarm(data);
}

void AlarmModuleAccess::callSetAlarm(AMAlarmValue data)
{
	int sz=data.size();
	char *v=data.toNetwork();

	CallData cd(a, AMServ::setAlarm, v, sz,destType);
	delete [] v;
	callMutex.lock();
	try
	{
		//sesCall(cd);
//		cout<<"setAlm :"<<endl;//data.alarmId.devId<<": type:"<<data.alarmId.type<<" str:"<<data.alarmId.strId<<" "<<data.value<<":"<<data.raised<<endl;
		sesCall(cd);
		this->socketTimeoutExceptionCounter=0;
//		cout<<"/setAlm "<<endl;
	}
	catch(Exception &e)
	{
		cout<<"setAlarm::call exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		callMutex.unlock();
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	callMutex.unlock();
}
/*
RPCPacket *AlarmModuleAccess::getAsyncResult(int msTimeout)
{
	if(rpcCrida==NULL)
		throw Exception("AlarmModuleAccess::getAsyncResult : no RPC available yet");
	callMutex.lock();
	int	tout=rpcCrida->getTimeout(); 
	try
	{
		rpcCrida->setTimeout(msTimeout); //TODO-->separar l'RPC del de les crides
		RPCPacket *pk=rpcCrida->getAsyncResult();
		rpcCrida->setTimeout(tout);
//		cout<<"AlarmModuleAccess::getAsyncResult pk data:"<<(void*)pk->data<<" sz:"<<pk->size<<" id:"<<(signed short)pk->id<<" org:"<<pk->origen<<" desti:"<<pk->desti<<endl;
		callMutex.unlock();
		return pk;
	}catch(SocketTimeoutException se)
	{
		rpcCrida->setTimeout(tout);
		if(se.getMsg() != string("No bytes to read"))
		{
			cout<<"AlarmModuleAccess::getAsyncResult SocketTimeoutException :"<<se.getMsg()<<endl;
			cout<<"AlarmModuleAccess::getAsyncResult SocketTimeoutException "<<rpcCrida->socket->getAddr().toString()<<endl;
		}
		callMutex.unlock();
		return NULL;	
	}
}

void AlarmModuleAccess::ignoreAsyncResult()
{
	rpcCrida->ignoreAsyncResult();
	//callMutex.unlock();
}

AMAlarmValue AlarmModuleAccess::waitForAlarm()
{
	RPCPacket *res=NULL;
	int retries=0;
	while((res==NULL || res->data==NULL))
	{
		try
		{
			res=this->getAsyncResult(1000);
//			cout<<"AlarmModuleAccess::waitForAlarm res:"<<(void*) res<<endl;
//			if(res!=NULL)
//				cout<<"AlarmModuleAccess::waitForAlarm data:"<<(void*) res->data<<" == "<<(void*)NULL<<endl;

			if(res!=NULL && res->data!=NULL)
			{
				cout<<"AlarmModuleAccess::waitForAlarm received:"<<(void*) res->data<<" : "<<res->size<<endl;
			}
			else
			{
				cout<<" waitForAlm recv  res:"<<(void*)res<<" data:";
				if(res!=NULL)
					cout<<(void*)res->data;
				cout<<endl;
			//	if(!stayRunning || !sessionStarted)
			//	{
			//		cout<<" waitForAlm : throw Exception(\"Receiving thread stopped or session lost\");"<<endl;
			//		throw Exception("Receiving thread stopped or session lost");
			//	}
				//	if(res==NULL)
				{
					retries++;
					if(retries==5)
					{
						cout<<"clear"<<endl;
						dataMutex.lock(); 
						valors.clear();
						dataMutex.unlock();
						retries=0;
					}
				}
			}
		}catch(SocketTimeoutException e)
		{
			if(!stayRunning)//|| !sessionStarted)
				e.serialize()->materializeAndThrow();
			usleep(100000); // 0.1 secs - per deixar lliure el CallMutex
		}catch(Exception e)
		{
			if(!stayRunning)// || !sessionStarted)
				e.serialize()->materializeAndThrow();
			usleep(100000); // 0.1 secs - per deixar lliure el CallMutex
		}
	}
	AMAlarmValue av;
	av.toLocal((char*)res->data);
	return av;
}

void AlarmModuleAccess::monitorAlarms()
{
	dataMutex.lock();
	try{
		stayRunning=true;
		if(thId1==-1)
		{
			running=true;
			thId1=this->start((void*) 1);
		}
	}catch(Exception e){}
	dataMutex.unlock();
}

void AlarmModuleAccess::endMonitoring()
{
	dataMutex.lock();
	stayRunning=false;
	dataMutex.unlock();
	if (thId1!=-1)
		this->join(thId1);
	if (thId0!=-1)
		this->join(thId0);
}

bool AlarmModuleAccess::getAlarmStatus(AMAlarmId id)
{
	if(valors.size()==0)
		return false;
	list<AMAlarmValue>::iterator pos=std::find(valors.begin(), valors.end(), id);
	return pos != valors.end();
}
*/


/*
bool AlarmModuleAccess::getAlarmStatus(int input)
{
	AMAlarmId id(servei.id, AMAlarmId::Input, input);
	return valors.find(id) != valors.end();	
}


double AlarmModuleAccess::getAlarmValue(int input)
{
	AMAlarmId id(servei.id, AMAlarmId::Input, input);
	map<AMAlarmId, double>::iterator it=valors.find(id);
	if(it==valors.end())
		return NAN;	
	else
		return it->second;
}

double AlarmModuleAccess::getAlarmValue(AMAlarmId id)
{
	map<AMAlarmId, double>::iterator it=valors.find(id);
	if(it==valors.end())
		return NAN;	
	else
		return it->second;
}
*/
void* AlarmModuleAccess::execute(int thrId, void* param)
{
	int thread = (int)param;
	if(thread==0)
	{
		confCond.lock();
		while(stayRunning) //renova les subscripcions a les alarmes
		{ 
			for (list<AMAlarmConfig>::iterator it=configuredAlarms.begin(); it!=configuredAlarms.end(); it++)
			{
				cout<<"thr0: AlarmModuleAccess::execute configuredAlms"<<endl;
				AMAlarmConfig cfg = *it;
				try
				{
//					cout<<"AlarmModuleAccess::execute callconfigAlm 1"<<endl;
					confCond.unlock();
//					cout<<"thr0: AlarmModuleAccess::execute callconfigAlm2"<<endl;
					callConfigAlarm(cfg);
//					cout<<"AlarmModuleAccess::execute callconfigAlm 4"<<endl;
				}
				catch(Exception &e)
				{
					cout<<"thr0: callConfigAlm exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
					//			e.serialize()->materializeAndThrow();
					if(e.getClass() ==string("AMSessionNotStablishedException"))
					{
					cout<<"thr0: callConfigAlm exc started->false "<<endl;
						this->sessionStarted=false;
					//	this->startSession(this->servei);
					}
				}
//				cout<<"thr0: AlarmModuleAccess::execute callconfigAlm 3"<<endl;
				confCond.lock();
			}
			try
			{
				struct timespec t;
				t.tv_sec=time(NULL)+3;
				t.tv_nsec=0;
//				cout<<"thr0: AlarmModuleAccess::execute --> wait"<<endl;
				confCond.wait(t);//unlocka el mutex automaticament
//				cout<<"thr0: AlarmModuleAccess::execute --> waited"<<endl;
			}catch(...){}
			cout<<"thr0: AlarmModuleAccess::execute configuring :"<<configuredAlarms.size()<<" ses:"<<sessionStarted<<" run:"<<stayRunning<<endl;
		}
		confCond.unlock();
		running=false;
	}
	return NULL;
/*	else if(thread==1)
	{
		dataMutex.lock();
		while(stayRunning)
		{

			try
			{
				try{
				dataMutex.unlock();
				}catch(Exception e){}
				cout<<"thr1: "<<thrId<<" :session started waitForAlm start:"<<sessionStarted<<" stay:"<<stayRunning<<endl;
				AMAlarmValue av = this->waitForAlarm();


				cout<<"thr1: "<<thrId<<" : Alarm Received "<<av.value<<":"<<av.raised<<"   lock()"<<endl;
				dataMutex.lock();
				//					cout<<"+"<<av.raised<<":"<<valors.size();cout.flush();
				valors.remove(av); //evitem duplicats, av.operator== no te en compte el valor
				//					cout<<" Alarm Received locked()"<<endl;
				//valors.push_back(av);
				if(av.raised)
					valors.push_back(av);
			}
			catch(Exception e)
			{
				cout<<"thr1: "<<thrId<<" :waitForAlm exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
				cout<<"thr1: "<<thrId<<" :waitForAlm exc:"<<e.getClass()<<" started:"<<this->sessionStarted<<" stay:"<<this->stayRunning<<endl;
				if(e.getClass() ==string("AMSessionNotStablishedException"))
				{
					this->sessionStarted=false;
				}

			}
		} //while(stayRunning)
	
		running=false;
		thId1=-1;
		try{
			dataMutex.unlock();
		}catch(...){}
	}	
	*/
}


