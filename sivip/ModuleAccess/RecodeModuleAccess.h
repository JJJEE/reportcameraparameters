#pragma once
#include <Utils/debugNew.h>
#include <ModuleAccess/ModuleAccess.h>
#include <Plugins/GestorRecodeExceptions.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/Address.h>

class RecodeModuleAccess : public ModuleAccess
{
protected:
	RPSession sesId, sesIdRecover;	

public:
	RecodeModuleAccess(Canis *cn=NULL);
	RecodeModuleAccess(word type, Canis *cn=NULL);
	RecodeModuleAccess(Address modAddr, Canis *cn=NULL);
	RecodeModuleAccess(word type, Address modAddr, Canis *cn=NULL);
	virtual ~RecodeModuleAccess();

	void startSession(RPSession id);
	void endSession(RPSession id);

	void processFrame(RPFrame frame);
	RPFrame getFrame();

	virtual bool processExceptionFromRPC(Exception &e);
};

