#pragma once
#include <Utils/debugNew.h>
#include <ModuleAccess/ModuleAccess.h>
#include <Plugins/Gestor.h>
#include <AlarmModule/AlarmModuleTypes.h>
#include <Sockets/Address.h>
#include <Threads/Mutex.h>
#include <Threads/RWlock.h>
#include <Threads/Condition.h>

#include <map>

class AlarmManager: private Thread
{
	protected:
		Address recv;
		SocketUDP socket;
//		list<AMAlarmValue> valors;
		bool stayRunning;
		bool running;
//		Mutex dataMutex;

		map<AMAlarmId, AMAlarmValue> activeAlarms;
		RWLock activeAlarmsLock;
		map<AMAlarmId, AMAlarmValue> inactiveAlarms;
		RWLock inactiveAlarmsLock;
		
	public:
		AlarmManager(IP local);
		bool getAlarmStatus(AMAlarmId id);
		list<AMAlarmValue> getActiveAlarms();
		void* execute(int id, void* param);
		Address getAddr();
	
		void monitorAlarms();
		void endMonitoring();
};

class AlarmModuleAccess: private Thread, private Gestor 
{
	private:
		AMDeviceID servei;	
		Address a;
		int destType;
		
		int  sessions;
		Mutex callMutex, dataMutex;
//		map<AMAlarmId, double> valors;
		list<AMAlarmConfig> configuredAlarms;
		AlarmManager *manager;
		dword socketTimeoutExceptionCounter; //TODO: treure quan es passi a ModuleAccess
//		list<AMAlarmValue> valors;

	
		bool running, stayRunning, sessionStarted;
		int thId0;//, thId1;//, thId2;
	
		void findBestAlarmModule(AMDeviceID dev);
		void callConfigAlarm(AMAlarmConfig data);
		void callSetAlarm(AMAlarmValue data);

	public:
		list<AMAlarmValue> sentAlarms;
		Condition confCond;//, sentCond;

		AlarmModuleAccess(Address orig, string type, Canis *cn=NULL);
		AlarmModuleAccess(Address orig, int type, Canis *cn=NULL);
		AlarmModuleAccess(string file, Canis *cn=NULL);
		~AlarmModuleAccess();
		
		void setManager(AlarmManager *manager);
		bool sesStarted();

		void startSession(AMDeviceID idservei);
		void endSession(AMDeviceID id);

		CallData sesCall(CallData cd);

		void configureAlarm(AMAlarmConfig data);
		void removeAlarm(AMAlarmConfig data);
		void setAlarm(AMAlarmValue data);
		
//		AMAlarmValue waitForAlarm();
//		void monitorAlarms();
//		void endMonitoring();
//		bool getAlarmStatus(AMAlarmId id);
/*		bool getAlarmStatus(int input);
		double getAlarmValue(int input);
		double getAlarmValue(AMAlarmId id);
*/
		// cutipaste de ControlMA 20080919// Aberracio. TODO 20060808: Desaberrar
		RPCPacket *getAsyncResult(int msTimeout = 1000);
		void ignoreAsyncResult();
		void* execute(int id, void* param);
};

