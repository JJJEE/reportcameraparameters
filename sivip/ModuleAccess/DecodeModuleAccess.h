#pragma once
#include <Utils/debugNew.h>
#include <Sockets/Address.h>
#include <Utils/Exception.h>
#include <ModuleAccess/ModuleAccess.h>
#include <Plugins/GestorImageTypes.h>

class DecodeModuleAccess : public ModuleAccess
{
protected:
	IPDeviceID devId, devIdRecover;	
	IPStreamingMode mode;
	
public:
	DecodeModuleAccess(Canis *cn=NULL);
	DecodeModuleAccess(word type, Canis *cn=NULL);
	DecodeModuleAccess(Address modAddr, Canis *cn=NULL);
	DecodeModuleAccess(word type, Address modAddr, Canis *cn=NULL);
	DecodeModuleAccess(dword devId, byte mask, Canis *cn=NULL);
	DecodeModuleAccess(word type, dword devId, byte mask, Canis *cn=NULL);
	virtual ~DecodeModuleAccess();

	IPDeviceInfo getDeviceInformation(IPDeviceInfo dev);
	void startSession(IPDeviceID dev);
	void endSession(IPDeviceID dev);

	IPCodecInfo getCodecInUse();
	void setCodecInUse(IPCodecInfo codec);

	IPFrameBufferSize getFrameBufferSize();
	void setFrameBufferSize(IPFrameBufferSize size);
	float getFrameBufferPercentInUse();
	int getFrameBufferFramesInUse();

	IPStreamingMode getStreamingMode();
	void setStreamingMode(IPStreamingMode mode);

	IPFrame getCompressedNextFrame();
	IPFrame getDecompressedNextFrame();
	
	IPFramesPerSecond getFPS();
	void setFPS(IPFramesPerSecond fps);
	
	virtual bool processExceptionFromRPC(Exception &e);
};
