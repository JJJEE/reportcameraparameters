#include <ModuleAccess/ControlModuleAccess.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Sockets/SocketException.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Plugins/PluginControl.h>
#include <Plugins/GestorControlExceptions.h>
#include <iostream>
#include <Utils/WindowsDefs.h>

using namespace std;

ControlModuleAccess::ControlModuleAccess(Canis *cn) : 
	ModuleAccess("ControlModule", cn), devId(-1), devIdRecover(-1)
{
}

ControlModuleAccess::ControlModuleAccess(word type, Canis *cn) : 
	ModuleAccess(type, cn), devId(-1), devIdRecover(-1)
{
}

ControlModuleAccess::ControlModuleAccess(string type, Canis *cn) : 
	ModuleAccess(type, cn), devId(-1), devIdRecover(-1)
{
}


ControlModuleAccess::ControlModuleAccess(Address modAddr, Canis *cn) :
	ModuleAccess("ControlModule", modAddr, cn), devId(-1), devIdRecover(-1)
{
}

ControlModuleAccess::ControlModuleAccess(word type, Address modAddr,
	Canis *cn) :
	ModuleAccess(type, modAddr, cn), devId(-1), devIdRecover(-1)
{
}

ControlModuleAccess::ControlModuleAccess(dword devId, byte mask, Canis *cn) :
	ModuleAccess("ControlModule", devId, mask, cn), devId(-1), devIdRecover(-1)
{
}

ControlModuleAccess::ControlModuleAccess(word type, dword devId, byte mask,
	Canis *cn) :
	ModuleAccess(type, devId, mask, cn), devId(-1), devIdRecover(-1)
{
}

ControlModuleAccess::~ControlModuleAccess()
{
}

bool ControlModuleAccess::sessionStarted()
{
	return devId.id!=-1;
}

void ControlModuleAccess::startSession(CPDeviceID dev)
{
	callMutex.lock();

	if (!this->useFixedAddress)
	{
		RPCPacket *mod=this->mod;
		try
		{
			this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, dev.id, this->cn->getInterfaceMask(), this->cn);
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
			
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)dev.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::startSessionServiceId,
				params, dev.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		this->devId=dev;
		this->socketTimeoutExceptionCounter=0;
		
		delete rpcRes;
	}
	catch(CPSessionAlreadyStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		this->devIdRecover = dev;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

void ControlModuleAccess::endSession(CPDeviceID dev)
{
	// Si no tenim sessió iniciada no fem res :)
	if(devId.id<=0)
		return;

	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)dev.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::endSessionServiceId,
				params, dev.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		this->devId=CPDeviceID(-1);
		this->devIdRecover=CPDeviceID(-1);
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		
		// Forcem que per la propera sessio es faci servir un socket diferent.
		this->closeRPC();
		this->socketTimeoutExceptionCounter=0;
	}
	catch(CPSessionNotStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

CPPTZ ControlModuleAccess::getPTZ()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginControl::getPTZServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		CPPTZ res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return CPPTZ();
}

void ControlModuleAccess::setPTZ(CPPTZ ptz)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)ptz.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::setPTZServiceId,
				params, ptz.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

void ControlModuleAccess::setInput(int in, int value)
{
	setInput(CPInput(in,value));
}

void ControlModuleAccess::setInput(CPInput in)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)in.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::setInputServiceId,
				params, in.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

int ControlModuleAccess::getInput(int in)
{
	CPInput res=getInput(CPInput(in,0));
	return res.value;
}

int ControlModuleAccess::getInput(int in, int value)
{
	CPInput res=getInput(CPInput(in,value));
	return res.value;
}

CPInput ControlModuleAccess::getInput(CPInput in)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginControl::getInputServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		CPInput res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return CPInput();
}

void ControlModuleAccess::setOutput(int in, int value)
{
	setOutput(CPOutput(in,value));
}

void ControlModuleAccess::setOutput(int in, int value, int pulse)
{
	setOutput(CPOutput(in, value, pulse));
}

void ControlModuleAccess::setOutput(CPOutput out)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)out.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::setOutputServiceId,
				params, out.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

int ControlModuleAccess::getOutput(int in)
{
	CPOutput res=getOutput(CPOutput(in,0));
	return res.value;
}

CPOutput ControlModuleAccess::getOutput(CPOutput out)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginControl::getOutputServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		CPOutput res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return CPOutput(); 
}

void ControlModuleAccess::setCommandBufferSize(CPCommandBufferSize size)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)size.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::setOutputServiceId,
				params, size.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

CPCommandBufferSize ControlModuleAccess::getCommandBufferSize()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginControl::getCommandBufferSizeServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		CPCommandBufferSize res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return CPCommandBufferSize();
}

float ControlModuleAccess::getCommandBufferPercentInUse()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(
				PluginControl::getCommandBufferPercentInUseServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		float res=*(float*)rpcRes->data;
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return 0.0f;
}

int ControlModuleAccess::getCommandBufferCommandsInUse()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(
				PluginControl::getCommandBufferCommandsInUseServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		int res=*(int*)rpcRes->data;
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return 0;
}

string ControlModuleAccess::getMetadataValue(string name)
{
	CPMetadata p(name,"");
	CPMetadata res=getMetadataValue(p);
	return res.value;
}

CPMetadata ControlModuleAccess::getMetadataValue(CPMetadata data)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginControl::getMetadataValueServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		CPMetadata res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return CPMetadata(); 
}

void ControlModuleAccess::setMetadataValue(string name, string value)
{
	setMetadataValue(CPMetadata(name, value));
}

void ControlModuleAccess::setMetadataValue(CPMetadata data)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)data.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::setMetadataValueServiceId,
				params, data.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

void ControlModuleAccess::setMetadataValueAsync(CPMetadata data)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)data.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::setMetadataValueServiceId,
				params, data.serializationSize());
		
		delete [] params;
		
		rpc->callAsync(*pk);			

		delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (pk!=NULL)
			delete pk;
	}
}

string ControlModuleAccess::getConfigParam(string path)
{
	CPConfigParam p(path,"");
	CPConfigParam res=getConfigParam(p);
	return res.value;
}

CPConfigParam ControlModuleAccess::getConfigParam(CPConfigParam p)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)p.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::getConfigParamServiceId,
				params, p.serializationSize());
		
		delete [] params;
		
//		cout  << "\tControlModulAccess::getConfigParam(" << p.path << "): "
//			"pre call to " << this->mod->a->toString() << endl;
		rpcRes=rpc->call(*pk);			
//		cout  << "\tControlModulAccess::getConfigParam(" << p.path << "): "
//			"called" << endl;
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		CPConfigParam res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		cout  << "\tControlModulAccess::getConfigParam(" << p.path << "): "
			"exc" << endl;
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return CPConfigParam();
}

void ControlModuleAccess::setConfigParam(string path, string value)
{
	CPConfigParam p(path, value);
	setConfigParam(p);
}

void ControlModuleAccess::setConfigParam(CPConfigParam p)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)p.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::setConfigParamServiceId,
				params, p.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

CPConfigParamSeq ControlModuleAccess::getAllConfigParams()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(PluginControl::getAllConfigParamsServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		CPConfigParamSeq res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return CPConfigParamSeq(); 
}

CPConfigParamSeq ControlModuleAccess::getConfigParamRecursive(CPConfigParam p)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)p.toNetwork();
		pk = 
			this->createCallPacket(
				PluginControl::getConfigParamRecursiveServiceId,
				params, p.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		CPConfigParamSeq res(rpcRes->data);
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return CPConfigParamSeq(); 
}


void ControlModuleAccess::move(CPMovement move)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)move.toNetwork();
		pk = 
			this->createCallPacket(PluginControl::moveServiceId,
				params, move.serializationSize());
		
		delete [] params;
		
		rpc->callAsync(*pk);			

		delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (pk!=NULL)
			delete pk;
	}
}

bool ControlModuleAccess::processExceptionFromRPC(Exception &e)
{
	if (e.getClass()==string("SocketTimeoutException"))
		this->socketTimeoutExceptionCounter++;
	else if (e.getClass()==string("SocketException") || 
		e.getClass()==string("RPCException"))
		this->closeRPC();
		
	if (e.getClass()==string("SocketException") ||
		(e.getClass()==string("SocketTimeoutException") && 
		this->socketTimeoutExceptionCounter >= 
			ModuleAccess::socketTimeoutExceptionThreshold))
	{
		// Probablement necessitem buscar un nou modul, ja que el nostre
		// deu haver caigut
		if (!this->useFixedAddress)
		{
			RPCPacket *mod=this->mod;
			try
			{
				if (devId.id!=-1 && devIdRecover.id!=-1)
				{
					int id; 
					if (devId.id!=-1)
						id=devId.id;
					else
						id=devIdRecover.id;
					this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, id, this->cn->getInterfaceMask(), this->cn);
				}
				else
					this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);

				if (mod!=NULL)
					delete mod;

				this->socketTimeoutExceptionCounter=0;
			}
			catch (...)
			{
				this->mod=mod;
			}
		}
		//e.serialize()->materializeAndThrow(true);
		return true;
	}
	else if (e.getClass()==string("ServiceException") || e.getClass()==string("CPSessionNotStablishedException") || e.getClass()==string("RPCException"))
	{
		this->socketTimeoutExceptionCounter=0;
		if (devId.id>0)
		{
			cout << "Recovering from " << e.getClass() << " - searching new ControlModule and starting session" << endl;
			
			devIdRecover=devId;
			
			devId=CPDeviceID(-1);
			
			try
			{
				this->startSession(devIdRecover);
			}
			catch (Exception &sse)
			{
				if (devId.id==-1)
					devId=devIdRecover;
				//e.serialize()->materializeAndThrow(true);
				return true;
			}
		}
		else
			//e.serialize()->materializeAndThrow(true);
			return true;
	}
	else
	{
		if(e.getClass()!=string("SocketTimeoutException"))
			this->socketTimeoutExceptionCounter=0;
		// e.serialize()->materializeAndThrow(true);
		return true;
	}
	
	return false;
}


RPCPacket *ControlModuleAccess::getAsyncResult()
{
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk=rpc->getAsyncResult();
		callMutex.unlock();
		this->socketTimeoutExceptionCounter=0;
		return pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return NULL;	
}

void ControlModuleAccess::ignoreAsyncResult()
{
	RPC *rpc=this->getRPC();
	rpc->ignoreAsyncResult();
	callMutex.unlock();
}

