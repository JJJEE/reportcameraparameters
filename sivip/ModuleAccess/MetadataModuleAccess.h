#pragma once
#include <Utils/debugNew.h>
#include <ModuleAccess/ModuleAccess.h>
#include <MetadataModule/MetadataModuleTypes.h>
#include <Sockets/Address.h>
#include <Utils/Exception.h>

class MetadataModuleAccess : public ModuleAccess
{
private:

public:
	dword devId, devIdRecover;	

public:
	MetadataModuleAccess(Canis *cn=NULL);
	MetadataModuleAccess(word type, Canis *cn=NULL);
	MetadataModuleAccess(Address modAddr, Canis *cn=NULL);
	MetadataModuleAccess(word type, Address modAddr, Canis *cn=NULL);
	MetadataModuleAccess(dword devId, byte mask, Canis *cn=NULL);
	MetadataModuleAccess(word type, dword devId, byte mask, Canis *cn=NULL);
	virtual ~MetadataModuleAccess();

	void startSession(dword id);
	void endSession(dword id);
	string getAttribute(string name);
	void setAttribute(string name, string value);
	MetadataModuleTypes::getObjectMetadataParams *getObjectMetadata();
	void addFilter(MetadataModuleFilter *f);
	MetadataModuleTypes::getFiltersParams* getFilters();
	void delFilter(MetadataModuleFilter *f);
	
	virtual bool processExceptionFromRPC(Exception &e);
};
