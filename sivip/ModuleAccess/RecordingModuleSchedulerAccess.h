#pragma once
#include <Utils/debugNew.h>
#include <Sockets/Address.h>
#include <Utils/Exception.h>
#include <ModuleAccess/ModuleAccess.h>
#include <RecordingModule/RecordingModuleSchedulingThread.h>

class RecordingModuleSchedulerAccess : public ModuleAccess
{

public:
	RecordingModuleSchedulerAccess(Canis *cn=NULL);
	RecordingModuleSchedulerAccess(word type, Canis *cn=NULL);
	RecordingModuleSchedulerAccess(Address modAddr, Canis *cn=NULL);
	RecordingModuleSchedulerAccess(word type, Address modAddr, Canis *cn=NULL);
	RecordingModuleSchedulerAccess(dword devId, byte mask, Canis *cn=NULL);
	RecordingModuleSchedulerAccess(word type, dword devId, byte mask, Canis *cn=NULL);
	virtual ~RecordingModuleSchedulerAccess();

	void checkRecordingServers();
	void checkSchedulesForServer(RMSServerId);

	virtual bool processExceptionFromRPC(Exception &e);
	
	// Aberracio. TODO 20060808: Desaberrar
	RPCPacket *getAsyncResult();
	void ignoreAsyncResult();
	// Fi aberracio
};

