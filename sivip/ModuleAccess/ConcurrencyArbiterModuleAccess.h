#pragma once

#include <Utils/debugNew.h>
#include <ConcurrencyArbiterModule/ConcurrencyArbiterModuleInterface.h>
#include <ModuleAccess/ModuleAccess.h>
#include <Utils/Canis.h>
#include <Sockets/Address.h>
#include <Threads/Mutex.h>

class ConcurrencyArbiterModuleAccess : public ModuleAccess
{
public:
	ConcurrencyArbiterModuleAccess(Canis *cn=NULL);
	ConcurrencyArbiterModuleAccess(word type, Canis *cn=NULL);
	ConcurrencyArbiterModuleAccess(Address modAddr, Canis *cn=NULL);
	ConcurrencyArbiterModuleAccess(word type, Address modAddr, Canis *cn=NULL);
	virtual ~ConcurrencyArbiterModuleAccess();

	string lock(string name);
	void unlock(string name, string code);
};
