#pragma once
#include <Utils/debugNew.h>
#include <ModuleAccess/ModuleAccess.h>
#include <RecordingModule/RecordingModuleInterface.h>
#include <RecordingModule/RecordingFile.h>
#include <RecordingModule/RecordingFileChunk.h>
#include <RecordingModule/RecordingFileHeaderChunk.h>
#include <RecordingModule/RecordingFileFrameChunk.h>
#include <RecordingModule/RecordingFileStringMetadataChunk.h>
#include <Utils/ServiceException.h>

#include <Sockets/Address.h>
#include <Threads/Mutex.h>

class RecordingModuleAccess : public ModuleAccess
{
private:
	RMStartSessionParams sesId, sesIdRecover;
	bool successfulRedirect;

	RPCPacket* startSessionCall(
		RMStartSessionParams ssParams);
public:
	class subsystemRecordingList
	{
		public:
		Address server;
		RMListRecordingsParams *listRecs;
		subsystemRecordingList(Address server, RMListRecordingsParams *listRecs):server(server), listRecs(listRecs)
		{}
	};
	
	RecordingModuleAccess(Canis *cn=NULL);
	RecordingModuleAccess(word type, Canis *cn=NULL);
	RecordingModuleAccess(Address modAddr, Canis *cn=NULL);
	RecordingModuleAccess(word type, Address modAddr, Canis *cn=NULL);
	RecordingModuleAccess(dword devId, byte mask, Canis *cn=NULL);
	RecordingModuleAccess(word type, dword devId, byte mask, Canis *cn=NULL);
	virtual ~RecordingModuleAccess();

	void startSession(RMStartSessionParams ssParams,
		bool searchBest=true);
	void endSession(RMEndSessionParams ep);
	void startRecordingStream();
	void startLocalRecordingStream();
	void stopRecordingStream();
	RMListRecordingsParams* listRecordings(RMListRecordingsParams lrp);
	list<subsystemRecordingList>* listRecordingsBySubsystem(RMListRecordingsParams lrp);
	RMIsRecordingParams isRecording(RMIsRecordingParams);
	void saveFrame(RMSaveFrameParams sfp);
	RecordingFileFrameChunk* readFrame(RMReadFrameParams rfp);
	RMGetFrameLimitParams getFrameLimit(RMGetFrameLimitParams flp);

	virtual bool processExceptionFromRPC(Exception &e);
};
