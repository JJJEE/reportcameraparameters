#include <ModuleAccess/RecordingModuleSchedulerAccess.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Sockets/SocketException.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Plugins/PluginControl.h>
#include <Plugins/GestorControlExceptions.h>
#include <iostream>
#include <Utils/WindowsDefs.h>

using namespace std;

RecordingModuleSchedulerAccess::RecordingModuleSchedulerAccess(Canis *cn) : 
	ModuleAccess("RecordingModuleScheduler", cn)
{
}

RecordingModuleSchedulerAccess::RecordingModuleSchedulerAccess(word type, Canis *cn) : 
	ModuleAccess(type, cn)
{
}

RecordingModuleSchedulerAccess::RecordingModuleSchedulerAccess(Address modAddr, Canis *cn) :
	ModuleAccess("RecordingModuleScheduler", modAddr, cn)
{
}

RecordingModuleSchedulerAccess::RecordingModuleSchedulerAccess(word type, Address modAddr,
	Canis *cn) :
	ModuleAccess(type, modAddr, cn)
{
}

RecordingModuleSchedulerAccess::RecordingModuleSchedulerAccess(dword devId, byte mask, Canis *cn) :
	ModuleAccess("RecordingModuleScheduler", devId, mask, cn)
{
}

RecordingModuleSchedulerAccess::RecordingModuleSchedulerAccess(word type, dword devId, byte mask,
	Canis *cn) :
	ModuleAccess(type, devId, mask, cn)
{
}

RecordingModuleSchedulerAccess::~RecordingModuleSchedulerAccess()
{
}

void RecordingModuleSchedulerAccess::checkRecordingServers()
{
	callMutex.lock();

	if (!this->useFixedAddress)
	{
		RPCPacket *mod=this->mod;
		try
		{
			this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
//			this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, dev.id, this->cn->getInterfaceMask(), this->cn);
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
			
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = this->createCallPacket(RecordingModuleSchedulingThreadInterface::checkRecordingServersServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}



void RecordingModuleSchedulerAccess::checkSchedulesForServer(RMSServerId id)
{
	callMutex.lock();

	if (!this->useFixedAddress)
	{
		RPCPacket *mod=this->mod;
		try
		{
			this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
//			this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, dev.id, this->cn->getInterfaceMask(), this->cn);
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
			
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)id.toNetwork();
		pk = 
			this->createCallPacket( RecordingModuleSchedulingThreadInterface::checkSchedulesForServerServiceId,
				params, id.size());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

		

bool RecordingModuleSchedulerAccess::processExceptionFromRPC(Exception &e)
{
	if (e.getClass()==string("SocketTimeoutException"))
		this->socketTimeoutExceptionCounter++;
	else if (e.getClass()==string("SocketException") || 
		e.getClass()==string("RPCException"))
		this->closeRPC();
		
	if (e.getClass()==string("SocketException") ||
		(e.getClass()==string("SocketTimeoutException") && 
		this->socketTimeoutExceptionCounter >= 
			ModuleAccess::socketTimeoutExceptionThreshold)
		||e.getClass()==string("ServiceException") || e.getClass()==string("RPCException"))
	{
		// Probablement necessitem buscar un nou modul, ja que el nostre
		// deu haver caigut
		if (!this->useFixedAddress)
		{
			RPCPacket *mod=this->mod;
			try
			{
//				if (devId.id!=-1)
//					this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, devId.id, this->cn->getInterfaceMask(), this->cn);
//				else
					this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);

				if (mod!=NULL)
					delete mod;

				this->socketTimeoutExceptionCounter=0;
			}
			catch (...)
			{
				this->mod=mod;
			}
		}
//		e.serialize()->materializeAndThrow(true);
		return true;
	}
/*	//Sense sessions, de moment. ServiceException/RPCExc les tract閙 com les altres...
	else if (e.getClass()==string("ServiceException") || e.getClass()==string("CPSessionNotStablishedException") || e.getClass()==string("RPCException"))
	{
		this->socketTimeoutExceptionCounter=0;
  if (devId.id>0)
		{
			cout << "Recovering from " << e.getClass() << " - searching new RecordingModuleScheduler and starting session" << endl;
			
			devIdRecover=devId;
			
			devId=CPDeviceID(-1);
			
			try
			{
				this->startSession(devIdRecover);
			}
			catch (Exception &sse)
			{
				if (devId.id==-1)
					devId=devIdRecover;
//				e.serialize()->materializeAndThrow(true);
				return true;
			}
		}
		else
//			e.serialize()->materializeAndThrow(true);
			return true;
	}
*/
	else
	{
		if(e.getClass()!=string("SocketTimeoutException"))
			this->socketTimeoutExceptionCounter=0;
//		e.serialize()->materializeAndThrow(true);
		return true;
	}
	return false;
}

