#include <ModuleAccess/ModuleAccess.h>
#include <ModuleInterface/ModuleInterface.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Utils/ServiceException.h>
#include <exceptions/DeprecatedOperationException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Utils/StrUtils.h>

#pragma mark *** Metodes protected
void ModuleAccess::init(string type, dword devId, byte mask, Canis *cn, bool useFixedAddress)
{
	this->recvSocketBuffer=64*1024;
	this->sendSocketBuffer=64*1024;
	
	if (cn!=NULL)
	{
		this->cn=cn;
		this->ownCanis=false;
	}
	else
	{
		throw DeprecatedOperationException("ModuleAccess::ModuleAccess()"
			" no longer accepts NULL Canis. Please initialize"
			" Canis with the correct settings for type and multicast group"
			" before initializing ModuleAccess");
	}
	
	this->typeIdStr=type;
	if (this->typeIdStr.length()==0)
		this->mod=NULL;
	else if (devId==0xffffffff)
		this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
	else
		this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, devId, mask, this->cn);
	
	this->rpc=NULL;
	this->useFixedAddress=useFixedAddress;
}

void ModuleAccess::init(word type, dword devId, byte mask, Canis *cn, bool useFixedAddress)
{
	this->recvSocketBuffer=64*1024;
	this->sendSocketBuffer=64*1024;
	
	if (cn!=NULL)
	{
		this->cn=cn;
		this->ownCanis=false;
	}
	else
	{
		throw DeprecatedOperationException("ModuleAccess::ModuleAccess()"
			" no longer accepts NULL Canis. Please initialize"
			" Canis with the correct settings for type and multicast group"
			" before initializing ModuleAccess");
	}
	
	this->typeIdStr=ServiceFinder::inverseResolveSubsystemTypeId(type,
		this->cn);

	this->rpc=NULL;
	this->useFixedAddress=useFixedAddress;

	if (devId==0xffffffff)
		this->mod=ServiceFinder::getBestSubsys(type, this->cn);
	else
		this->mod=ServiceFinder::getNearestSubsysForDevice(type, devId, mask, this->cn);
}

#pragma mark *** Misc
string ModuleAccess::getClass()
{
	return string("ModuleAccess");
}

#pragma mark *** Constructores
ModuleAccess::ModuleAccess(string type, Canis *cn) : 
	sockAddr(IP("0.0.0.0"), 0), callMutex(true),
	socketTimeoutExceptionCounter(0),
	currentTimeout(ModuleAccess::defaultTimeout)
{
	init(type, 0xffffffff, 0, cn, false);
}

ModuleAccess::ModuleAccess(word type, Canis *cn) : 
	sockAddr(IP("0.0.0.0"), 0), callMutex(true),
	socketTimeoutExceptionCounter(0),
	currentTimeout(ModuleAccess::defaultTimeout)
{
	init(type, 0xffffffff, 0, cn, false);
}

ModuleAccess::ModuleAccess(string type, Address modAddr, Canis *cn) : 
	sockAddr(IP("0.0.0.0"), 0), callMutex(true),
	socketTimeoutExceptionCounter(0),
	currentTimeout(ModuleAccess::defaultTimeout)
{
	init(type, 0xffffffff, 0, cn, true);
	if (this->mod->a!=NULL)
		delete this->mod->a;
	this->mod->a=new Address(modAddr.getIP(), modAddr.getPort());
}

ModuleAccess::ModuleAccess(word type, Address modAddr, Canis *cn) : 
	sockAddr(IP("0.0.0.0"), 0), callMutex(true),
	socketTimeoutExceptionCounter(0),
	currentTimeout(ModuleAccess::defaultTimeout)
{
	init(type, 0xffffffff, 0, cn, true);
	if (this->mod->a!=NULL)
		delete this->mod->a;
	this->mod->a=new Address(modAddr.getIP(), modAddr.getPort());
}

ModuleAccess::ModuleAccess(string type, dword devId, byte mask, Canis *cn) : 
	sockAddr(IP("0.0.0.0"), 0), callMutex(true),
	socketTimeoutExceptionCounter(0),
	currentTimeout(ModuleAccess::defaultTimeout)
{
	init(type, devId, mask, cn, false);
}

ModuleAccess::ModuleAccess(word type, dword devId, byte mask, Canis *cn) : 
	sockAddr(IP("0.0.0.0"), 0), callMutex(true),
	socketTimeoutExceptionCounter(0),
	currentTimeout(ModuleAccess::defaultTimeout)
{
	init(type, devId, mask, cn, false);
}

ModuleAccess::~ModuleAccess()
{
	// No destruim mentre siguem dins una crida, tot i que no es una gran
	// garantia, ja que ens poden intentar fer una crida mentre tenim
	// el mutex, i igualment s'intentara executar una crida amb l'objecte
	// en un estat invalid. De totes maneres, aix� deixem que les crides
	// "en curs" s'executin be.
	this->callMutex.lock();

	if (ownCanis && this->cn!=NULL)
	{
		this->cn->stopRecv();
		delete this->cn;		
		this->cn=NULL;
	}
	
	if (this->rpc!=NULL)
	{
		delete this->rpc;
		this->rpc=NULL;
	}

	if (this->mod!=NULL)
	{
		delete this->mod;
		this->mod=NULL;
	}

	this->callMutex.unlock();
}

#pragma mark *** Metodes
RPC* ModuleAccess::getRPC()
{
//	cout  << endl << " --> ModuleAccess::getRPC --> " << endl;
	this->callMutex.lock();
	
	if (this->rpc==NULL)
	{
		try
		{
			this->rpc=new RPC(*this->mod->a);
			if (this->rpc==NULL)
			{	
				// L'unlock es fa al catch
				throw NotEnoughMemoryException("Not enough memory to allocate "
					" RPC for ModuleClient");
			}
			
			dword set=this->rpc->setRecvBuffer(this->recvSocketBuffer);
			if (set!=this->recvSocketBuffer)
			{
				cout << "WARNING: " << __FILE__ << ", line " << __LINE__
				<< ": set receive buffer to " << set << " bytes, "
				<< this->recvSocketBuffer << " requested." << endl;
			}
			set=this->rpc->setSendBuffer(this->sendSocketBuffer);
			if (set!=this->sendSocketBuffer)
			{
				cout << "WARNING: " << __FILE__ << ", line " << __LINE__
				<< ": set send buffer to " << set << " bytes, "
				<< this->sendSocketBuffer << " requested." << endl;
			}
			
			if(currentTimeout <= 0)
			{
				cout<<"WARNING: ModuleAcces timeout:"<<currentTimeout<<"ms, forcing default"<<endl;
				currentTimeout = ModuleAccess::defaultTimeout;
			}

			this->rpc->setTimeout(currentTimeout);
		}
		catch (Exception &e)
		{
			if (this->callMutex.isHeldByCurrentThread())
				this->callMutex.unlock();
			else
				cout << __FILE__ << " Line " << __LINE__ << " callMutex NO lockat" << endl;
			throw e;
		}
		catch (std::exception &stde)
		{
			if (this->callMutex.isHeldByCurrentThread())
				this->callMutex.unlock();
			else
				cout << __FILE__ << " Line " << __LINE__ << " callMutex NO lockat" << endl;
			throw stde;
		}
	}
	else
	{
		if(currentTimeout <= 0)
		{
			cout<<"WARNING: ModuleAcces timeout:"<<currentTimeout<<"ms, forcing default"<<endl;
			currentTimeout = ModuleAccess::defaultTimeout;
		}
		this->rpc->setTimeout(currentTimeout);
		this->rpc->setDefaultDestination(*(this->mod->a));
	}
	
	//cout  << endl << " Default Destination (*this->mod->a) = " <<  this->mod->a->getIP().toString() << endl;
	
	if (this->callMutex.isHeldByCurrentThread())
		this->callMutex.unlock();
	else
		cout << __FILE__ << " Line " << __LINE__ << " callMutex NO lockat" << endl;
	
	//cout  << endl << " <-- ModuleAccess::getRPC <-- " << endl;
	return this->rpc;
}

void ModuleAccess::closeRPC()
{
	this->callMutex.lock();
	
	delete this->rpc;
	this->rpc=NULL;
	
	this->callMutex.unlock();
}

bool ModuleAccess::processExceptionFromRPC(Exception &e)
{
	return true;
}

void ModuleAccess::processNullRPCResult(RPCPacket *rpcRes, string func,
	string file, int fileLine)
{
	if (rpcRes==NULL)
	{
		cout << "ModuleAccess::processNullRPCResult: " << 
			this->getClass() + string("::") + func + 
			string("(): No RPC result received") +
			(file.length()>0?string(" at ") + file +
				(fileLine>0?string(", line ") + StrUtils::decToString(fileLine):
				string(""))
			:string("")) << endl;
	
		throw ServiceException(this->getClass() + string("::") + func + 
			string("(): No RPC result received") +
			(file.length()>0?string(" at ") + file +
				(fileLine>0?string(", line ") + StrUtils::decToString(fileLine):
				string(""))
			:string("")));
	}
}

void ModuleAccess::processNullRPCDataResult(RPCPacket *rpcRes, string func,
	string file, int fileLine)
{
	if (rpcRes==NULL)
	{
		cout << "ModuleAccess::processNullRPCDataResult: " << 
			this->getClass() + string("::") + func + 
			string("(): No RPC result received") +
			(file.length()>0?string(" at ") + file +
				(fileLine>0?string(", line ") + StrUtils::decToString(fileLine):
				string(""))
			:string("")) << endl;
	
		throw ServiceException(this->getClass() + string("::") + func + 
			string("(): No RPC result received") +
			(file.length()>0?string(" at ") + file +
				(fileLine>0?string(", line ") + StrUtils::decToString(fileLine):
				string(""))
			:string("")));
	}
	else if (rpcRes->data == NULL)
	{
		cout << "ModuleAccess::processNullRPCDataResult: " << 
			this->getClass() + string("::") + func + 
			string("(): RPC result with no data received") +
			(file.length()>0?string(" at ") + file +
				(fileLine>0?string(", line ") + StrUtils::decToString(fileLine):
				string(""))
			:string("")) << endl;
	
		throw ServiceException(this->getClass() + string("::") + func + 
			string("(): RPC result with no data received") +
			(file.length()>0?string(" at ") + file +
				(fileLine>0?string(", line ") + StrUtils::decToString(fileLine):
				string(""))
			:string("")));
	}
}

RPCPacket* ModuleAccess::createCallPacket(word service, void *params,
	dword paramsLen)
{
	// Podria passar que no haguem fet cap crida i que no haguem fet cap 
	// getRPC(). P.e. Quan fem servir un RecordingModuleAccess nomes per
	// llistar gravacions, que es una funcionalitat especial de funcionament
	// diferent.
	if (this->rpc==NULL)
		this->getRPC();

	return new RPCPacket(this->rpc->socket->getLocalAddr(), service, 
		(byte*)params, paramsLen, 0, this->mod->origen, true);
}

dword ModuleAccess::getRecvBuffer()
{
	return this->recvSocketBuffer;
}

void ModuleAccess::setRecvBuffer(dword bytes)
{
	this->recvSocketBuffer=bytes;
	// Si ja tenim rpc, ho apliquem, si no ha es fara quan es crei
	if (this->rpc!=NULL)
	{
		dword set=this->rpc->setRecvBuffer(bytes);
		if (set!=bytes)
		{
			cout << "WARNING: " << __FILE__ << ", line " << __LINE__
			<< ": set receive buffer to " << set << " bytes, "
			<< bytes << " requested." << endl;
		}
	}
}

dword ModuleAccess::getSendBuffer()
{
	return this->sendSocketBuffer;
}

void ModuleAccess::setSendBuffer(dword bytes)
{
	this->sendSocketBuffer=bytes;
	// Si ja tenim rpc, ho apliquem, si no ha es fara quan es crei
	if (this->rpc!=NULL)
	{
		dword set=this->rpc->setSendBuffer(bytes);
		if (set!=bytes)
		{
			cout << "WARNING: " << __FILE__ << ", line " << __LINE__
			<< ": set send buffer to " << set << " bytes, "
			<< bytes << " requested." << endl;
		}
	}
}

void ModuleAccess::setTimeout(dword ms)
{
	this->currentTimeout=ms;
}

bool ModuleAccess::isAlive(Address module, int type, int timeout)
{
	RPC *rpc= NULL;
	RPCPacket *pk = NULL;
	try
	{
		rpc = new RPC(module);
		pk = new RPCPacket(rpc->socket->getLocalAddr(), ModuleInterface::isAliveServiceId,
			NULL, 0, 0, type, true);
		rpc->setTimeout(timeout);
		RPCPacket *rpcRes=rpc->call(*pk);			
		if(rpcRes == NULL)
		{
			cout<<"ModuleAccess::isAlive Exception while checking:"<<module.toString()<<" type:"<<type<<": no RPC result received"<<endl;
			if (rpc!=NULL)
				delete rpc;
			if (pk!=NULL)
				delete pk;
			return false;
		}else
		{
			if (rpc!=NULL)
				delete rpc;
			if (pk!=NULL)
				delete pk;
			delete rpcRes;
			return true;
		}
	}
	catch(Exception &e)
	{
		if (rpc!=NULL)
			delete rpc;
		if (pk!=NULL)
			delete pk;
		cout<<"ModuleAccess::isAlive Exception while checking:"<<module.toString()<<" type:"<<type<<" -> "<<e.getClass()<<":"<<e.getMsg()<<endl;
		return false;
	}
	
}

string ModuleAccess::getVersion()
{
	return ModuleAccess::getVersion(this->mod->a, this->mod->desti);
}

string ModuleAccess::getVersion(Address module, int type, int timeout)
{
	RPC *rpc= NULL;
	RPCPacket *pk = NULL;
	try
	{
		rpc = new RPC(module);
		pk = new RPCPacket(rpc->socket->getLocalAddr(), ModuleInterface::getVersionServiceId,
			NULL, 0, 0, type);
		rpc->setTimeout(timeout);
		RPCPacket *rpcRes=rpc->call(*pk);			
		if(rpcRes == NULL)
		{
			cout<<"ModuleAccess::isAlive Exception while checking:"<<module.toString()<<" type:"<<type<<": no RPC result received"<<endl;
			if (rpc!=NULL)
				delete rpc;
			if (pk!=NULL)
				delete pk;
			return "";
		}else
		{
			if (rpc!=NULL)
				delete rpc;
			if (pk!=NULL)
				delete pk;
			string res((char*)rpcRes->data, rpcRes->size);
			delete rpcRes;
			return res;
		}
	}
	catch(Exception &e)
	{
		if (rpc!=NULL)
			delete rpc;
		if (pk!=NULL)
			delete pk;
		cout<<"ModuleAccess::isAlive Exception while checking:"<<module.toString()<<" type:"<<type<<" -> "<<e.getClass()<<":"<<e.getMsg()<<endl;
		return "";
	}
	
}
