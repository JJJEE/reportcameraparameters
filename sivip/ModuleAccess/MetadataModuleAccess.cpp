#include <ModuleAccess/MetadataModuleAccess.h>
#include <MetadataModule/MetadataModuleServices.h>
#include <Exceptions/MetadataModuleSessionAlreadyStablishedException.h>
#include <Exceptions/MetadataModuleSessionNotStablishedException.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <Utils/debugStackTrace.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <Sockets/SocketException.h>
#include <iostream>
#include <Utils/WindowsDefs.h>

using namespace std;


MetadataModuleAccess::MetadataModuleAccess(Canis *cn) : 
	ModuleAccess("MetadataModule", cn), devId(0xffffffff),
	devIdRecover(0xffffffff)
{
}

MetadataModuleAccess::MetadataModuleAccess(word type, Canis *cn) : 
	ModuleAccess(type, cn), devId(0xffffffff), devIdRecover(0xffffffff)
{
}

MetadataModuleAccess::MetadataModuleAccess(Address modAddr, Canis *cn) :
	ModuleAccess("MetadataModule", modAddr, cn), devId(0xffffffff),
	devIdRecover(0xffffffff)
{
}

MetadataModuleAccess::MetadataModuleAccess(word type, Address modAddr,
	Canis *cn) :
	ModuleAccess(type, modAddr, cn), devId(0xffffffff), devIdRecover(0xffffffff)
{
}

MetadataModuleAccess::MetadataModuleAccess(dword devId, byte mask, Canis *cn) :
	ModuleAccess("MetadataModule", devId, mask, cn), devId(devId),
	devIdRecover(0xffffffff)
{
}

MetadataModuleAccess::MetadataModuleAccess(word type, dword devId, byte mask,
	Canis *cn) :
	ModuleAccess(type, devId, mask, cn), devId(devId), devIdRecover(0xffffffff)
{
}

MetadataModuleAccess::~MetadataModuleAccess()
{
}

void MetadataModuleAccess::startSession(dword devId)
{
	callMutex.lock();

	if (!this->useFixedAddress)
	{
		RPCPacket *mod=this->mod;
		try
		{
//			this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
			this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, devId, this->cn->getInterfaceMask(), this->cn);
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
			
	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		dword params=devId;
		Endian::to(Endian::xarxa, &params, sizeof(dword));
		pk = 
			this->createCallPacket(
				MetadataModuleServices::startSessionServiceId,
				&params, sizeof(dword));
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		this->devId=devId;
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(MetadataModuleSessionAlreadyStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		this->devId=devId;
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		this->devIdRecover = devId;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

void MetadataModuleAccess::endSession(dword devId)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		dword params=devId;
		Endian::to(Endian::xarxa, &params, sizeof(dword));
		pk = 
			this->createCallPacket(
				MetadataModuleServices::endSessionServiceId,
				&params, sizeof(dword));
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		this->devId=0xffffffff;
		this->devIdRecover=0xffffffff;
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(MetadataModuleSessionNotStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

string MetadataModuleAccess::getAttribute(string name)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		MetadataModuleTypes::getAttributeParams getAtt;
		getAtt.setName(name);
		
		byte *params=(byte*)getAtt.toNetwork();
		pk = 
			this->createCallPacket(
				MetadataModuleServices::getAttributeServiceId,
				params, getAtt.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		getAtt.toLocal(rpcRes->data);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;

		return getAtt.getValue();
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return string("");
}

void MetadataModuleAccess::setAttribute(string name, string value)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		MetadataModuleTypes::setAttributeParams setAtt;
		setAtt.setName(name);
		
		byte *params=(byte*)setAtt.toNetwork();
		pk = 
			this->createCallPacket(
				MetadataModuleServices::setAttributeServiceId,
				params, setAtt.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

MetadataModuleTypes::getObjectMetadataParams* MetadataModuleAccess::getObjectMetadata()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(
				MetadataModuleServices::getObjectMetadataServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		MetadataModuleTypes::getObjectMetadataParams *res = 
			new MetadataModuleTypes::getObjectMetadataParams(rpcRes->data);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;

		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
//		cout<<"MeteModAccs::getObjMetaData process:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return new MetadataModuleTypes::getObjectMetadataParams();
}

void MetadataModuleAccess::addFilter(MetadataModuleFilter *f)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)f->toNetwork();
		pk = 
			this->createCallPacket(
				MetadataModuleServices::addFilterServiceId,
				params, f->size());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

MetadataModuleTypes::getFiltersParams* MetadataModuleAccess::getFilters()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(
				MetadataModuleServices::getFiltersServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);

		MetadataModuleTypes::getFiltersParams *res = 
			new MetadataModuleTypes::getFiltersParams(rpcRes->data);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
		
		return res;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return new MetadataModuleTypes::getFiltersParams();
}

void MetadataModuleAccess::delFilter(MetadataModuleFilter *f)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)f->toNetwork();
		pk = 
			this->createCallPacket(
				MetadataModuleServices::delFilterServiceId,
				params, f->size());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

bool MetadataModuleAccess::processExceptionFromRPC(Exception &e)
{
	if (e.getClass()==string("SocketTimeoutException"))
		this->socketTimeoutExceptionCounter++;
	else if (e.getClass()==string("SocketException") || 
		e.getClass()==string("RPCException"))
		this->closeRPC();
		
	if (e.getClass()==string("SocketException") ||
		(e.getClass()==string("SocketTimeoutException") && 
		this->socketTimeoutExceptionCounter >= 
			ModuleAccess::socketTimeoutExceptionThreshold))
	{
		// Probablement necessitem buscar un nou modul, ja que el nostre
		// deu haver caigut
		if (!this->useFixedAddress)
		{
			RPCPacket *mod=this->mod;
			try
			{
				if (devId!=0xffffffff || devIdRecover!=0xffffffff)
				{
					dword id;
					if (devId!=0xffffffff)
						id = devId;
					else
						id=devIdRecover;

					cout<<"MetadataModuleAccess::Recover getBestSubSysForDev:"<<id<<endl;
					this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, id, this->cn->getInterfaceMask(), this->cn);
				}else
				{
					cout<<"MetadataModuleAccess::Recover getBestSubSys"<<endl;
					this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
				}
				if (mod!=NULL)
					delete mod;

				this->socketTimeoutExceptionCounter=0;
			}
			catch (...)
			{
				this->mod=mod;
			}
		}
		//e.serialize()->materializeAndThrow(true);
		return true;
	}
	else if (e.getClass()==string("ServiceException") || e.getClass()==string("MetadataModuleSessionNotStablishedException") || e.getClass()==string("RPCException"))
	{
		this->socketTimeoutExceptionCounter=0;
		if (devId!=0xffffffff)
		{
			cout << "Recovering from " << e.getClass() << " - searching new MetadataModule and starting session dev:"<<devId << endl;
			
			devIdRecover=devId;
			
			devId=0xffffffff;
			
			try
			{
				this->startSession(devIdRecover);
			}
			catch (Exception &sse)
			{
				if (devId==0xffffffff)
					devId=devIdRecover;
//				e.serialize()->materializeAndThrow(true);
				return true;
			}
		}
		else
			//e.serialize()->materializeAndThrow(true);
			return true;
	}
	else
	{
		if(e.getClass()!=string("SocketTimeoutException"))
			this->socketTimeoutExceptionCounter=0;
//		e.serialize()->materializeAndThrow(true);
		return true;
	}
	return false;
}
