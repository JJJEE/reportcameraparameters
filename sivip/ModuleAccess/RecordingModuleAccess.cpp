#include <Http/HttpClient.h>
#include <ModuleAccess/RecordingModuleAccess.h>
#include <RecordingModule/RecordingModuleException.h>
#include <RecordingModule/RecordingModuleSessionAlreadyStablishedException.h>
#include <RecordingModule/RecordingModuleSessionNotStablishedException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Sockets/SocketException.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/RedirectCallException.h>
#include <XML/xmlParser.h>
#include <iostream>
#include <Utils/WindowsDefs.h>

using namespace std;

//		CallData RecordingModuleAccess::sesCall(CallData cdp)
//		{
//			return sesCall(cdp, 0);
//		}
//		
//		CallData RecordingModuleAccess::sesCall(CallData cdp, int recurse)
//		{	
//			if(recurse==3)
//			{
//				throw ServiceException("Call Redirection limit exceeded received");
//			}
//		
//		
//			try
//			{
//				return call(cdp);
//			}catch(RecordingModuleException rme)
//			{
//				cout<<"RMA::sesCall::RMSessionNotStablishedException:"<<rme.getMsg()<<endl;
//				if(rme.getMsg().find("Session not stablished")==string::npos)
//					throw rme;
//				startSession(servei);
//				sessions--; //"no compta"
//				return call(cdp);
//			}
//			catch(RedirectCallException &e)
//			{
//				string msg=e.getMsg();
//				string red("Redirect: ");
//				int pos=msg.find(red);
//				cout<<"RMA::sesCall RedirectCall : "<<msg<<endl;
//				if(pos!=string::npos)
//				{
//					string serv=msg.substr(red.length());
//		
//					pos=serv.find(":");
//					if(pos!=string::npos)
//					{
//						string ip=serv.substr(0, pos);
//						int port = atoi(serv.substr(pos+1).c_str());
//		//		cout<<"RMA::sesCall  RedirectCall 2 : "<<ip<<":"<<port<<endl;
//						a = Address(IP(ip), port);
//						delete this->rpcCrida;
//						this->rpcCrida=NULL;
//						cdp.dest=a;
//		
//						try
//						{
//		//		cout<<"RMA::sesCall  RedirectCall 3 startSes"<<endl;
//							startSession(servei);
//						}catch(RecordingModuleException rme)
//						{
//							if(rme.getMsg().find("Session already started")==string::npos)
//								throw rme;
//						}
//		//		cout<<"RMA::sesCall  RedirectCall 3 call"<<endl;
//						return sesCall(cdp, recurse+1);
//					}
//				}
//				throw ServiceException("Invalid RedirectCallException received");
//			}
//			catch(Exception &e)
//			{
//				cout<<"CMA::sesCall::Exc:"<<e.getClass()<<":"<<e.getMsg()<<endl;
//				e.serialize()->materializeAndThrow();
//			}
//		}



RecordingModuleAccess::RecordingModuleAccess(Canis *cn) : 
	ModuleAccess("RecordingModule", cn), sesId(), sesIdRecover()
{
}

RecordingModuleAccess::RecordingModuleAccess(word type, Canis *cn) : 
	ModuleAccess(type, cn), sesId(), sesIdRecover()
{
}

RecordingModuleAccess::RecordingModuleAccess(Address modAddr, Canis *cn) :
	ModuleAccess("RecordingModule", modAddr, cn), sesId(), sesIdRecover()
{
}

RecordingModuleAccess::RecordingModuleAccess(word type, Address modAddr,
	Canis *cn) :
	ModuleAccess(type, modAddr, cn), sesId(), sesIdRecover()
{
}

RecordingModuleAccess::RecordingModuleAccess(dword devId, byte mask,
	Canis *cn) :
	ModuleAccess("RecordingModule", devId, mask, cn), sesId(), sesIdRecover()
{
}

RecordingModuleAccess::RecordingModuleAccess(word type, dword devId, byte mask,
	Canis *cn) :
	ModuleAccess(type, devId, mask, cn), sesId(), sesIdRecover()
{
}

RecordingModuleAccess::~RecordingModuleAccess()
{
}

void RecordingModuleAccess::startSession(
	RMStartSessionParams ssParams, bool searchBest)
{
	callMutex.lock();
	string debugStr("");

	if (!this->useFixedAddress && searchBest)
	{
		RPCPacket *mod=this->mod;
		try
		{
//			this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
			debugStr = string("RecordingModuleAccess::startSession::ServiceFinder:getNearestSubsystemForDevice [") + StrUtils::decToString(ssParams.devId) + string("]");
			cout << debugStr << endl;
			this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, ssParams.devId, this->cn->getInterfaceMask(), this->cn);
			debugStr = string("RecordingModuleAccess::startSession::ServiceFinder: found RecordingModule at ") + mod->a->toString() + string(" fixed:") + (useFixedAddress?string("true"):string("false"));
			cout << debugStr << endl;
			if (mod!=NULL)
				delete mod;
		}
		catch (...)
		{
			this->mod=mod;
		}
	}
	else
	{
//			debugStr = string("RecordingModuleAccess::startSession: not using serviceFinder, module:") + string(mod->a->toString()) + string(" fixed:") + string(useFixedAddress) + string(" search:") + string(searchBest);
//			cout << debugStr << endl;
	}

//	debugStr = string(" ##################### startSession at ") + string(mod->a->toString()) + string(" fixed:") + string(useFixedAddress) + string(" search:") + string(searchBest);
//	cout << debugStr << endl;
			
//	debugStr = string(" ##################### startSession rpc Addr:") + string(rpc->socket->getAddr().toString());
//	cout << debugStr << endl;
	RPCPacket *rpcRes=NULL;
	try
	{
		rpcRes=this->startSessionCall(ssParams);			

		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(RecordingModuleSessionAlreadyStablishedException &e)
	{
		debugStr = string("startSession:RMSessionAlreadyStartedParamsdev start:") + StrUtils::decToString(ssParams.devId) + string(" saved:") + StrUtils::decToString(this->sesId.devId);
		cout << debugStr << endl;
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();

		if(e.getClass()==string("RedirectCallException"))
		{
			if(this->sesId.date!=NULL)
				delete this->sesId.date;
			this->sesId=ssParams;
			this->sesId.date=new RecordingFileDateMetadataChunk();
			if(ssParams.date!=NULL)
				*this->sesId.date=*ssParams.date;
		}
		debugStr = string("startSession:processExcFromRPC dev start:") + StrUtils::decToString(ssParams.devId) + string(" saved:") + StrUtils::decToString(this->sesId.devId) + string(" ex:") + e.getClass();
		cout << debugStr << endl;
		if (this->processExceptionFromRPC(e))
			throw;
		if(e.getClass()==string("RedirectCallException") && (!this->successfulRedirect && !this->useFixedAddress))
		{
			this->sesId.devId=0;
		}
//		if (this->successfulRedirect)
//			this->startSession(ssParams, searchBest);
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
	}
}

RPCPacket* RecordingModuleAccess::startSessionCall(
	RMStartSessionParams ssParams)
{
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		RPC *rpc=this->getRPC();
		
		byte *params=(byte*)ssParams.toNetwork();
		pk = 
			this->createCallPacket(
				RecordingModuleInterface::startSessionServiceId,
				params, ssParams.serializationSize());
		
		delete [] params;

		if(this->sesId.date!=NULL)
			delete this->sesId.date;
		this->sesId=ssParams;
		this->sesId.date=new RecordingFileDateMetadataChunk();
		if(ssParams.date!=NULL)
			*this->sesId.date=*ssParams.date;
		
		string debugStr = string("RecordingModuleAccess::startSession call [") + StrUtils::decToString(ssParams.devId) + string("]");
		cout << debugStr << endl;
		rpcRes=rpc->call(*pk);			
		debugStr = string("RecordingModuleAccess::startSession called [") + StrUtils::decToString(ssParams.devId) + string("]");
		cout << debugStr << endl;
		callMutex.unlock();

		delete pk;
		return rpcRes;
	}
	catch(...)
	{
		if (pk!=NULL)
			delete pk;
		throw;
	}
}

void RecordingModuleAccess::endSession(RMEndSessionParams ep)
{
//	if(ep.devId!=this->sesId.devId)
//	{
//		throw (Exception(0, string(" Session not stablished with this device for this RecordingModuleAccess:")+StrUtils::decToString(ep.devId)+string("!=")+StrUtils::decToString(this->sesId.devId)));
//	}

	callMutex.lock();

	RPC *rpc=this->getRPC();
//	cout<<" ////////////////////// endSession at "<<mod->a->toString()<<endl;
//	cout<<" ////////////////////// endSession rpc Addr:"<<rpc->socket->getAddr().toString()<<endl;
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)ep.toNetwork();
		pk = 
			this->createCallPacket(
				RecordingModuleInterface::endSessionServiceId,
				params, ep.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		this->sesId.devId=0;
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(RecordingModuleSessionNotStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		cout<<"endSession:processExcFromRPC:"<<e.getClass()<<endl;
		if (this->processExceptionFromRPC(e))
			throw;
		if (this->successfulRedirect)
			this->endSession(ep);
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

void RecordingModuleAccess::startRecordingStream()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	string debugStr("");
	try
	{
		debugStr = string(" RMA::startRecStream:") + StrUtils::decToString(sesId.devId) + string(" mod:") + mod->a->toString() + string(" rpc addr:") + rpc->socket->getAddr().toString();
		cout << debugStr << endl;
		pk = 
			this->createCallPacket(
				RecordingModuleInterface::startRecordingStreamServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();
		debugStr = string(" RMA::startRecStream:") + StrUtils::decToString(sesId.devId) + string(" ok");
		cout << debugStr << endl;

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(RecordingModuleSessionNotStablishedException &e)
	{
		debugStr = string("startRecStream:") + StrUtils::decToString(sesId.devId) + string(" RecordingModuleSessionNotStablishedException ") ;
		cout << debugStr << endl;
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
//		debugStr = string("startRecStream:") + StrUtils::decToString(sesId.devId) + string(" processExcFromRPC:") + string(e.getClass());
//		cout << debugStr << endl;
		if (this->processExceptionFromRPC(e))
			throw;
		if (this->successfulRedirect)
			this->startLocalRecordingStream();
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}

 void RecordingModuleAccess::startLocalRecordingStream()
 {
 	callMutex.lock();
 
 	RPC *rpc=this->getRPC();
 	RPCPacket *pk=NULL, *rpcRes=NULL;
 	try
 	{
 		pk = 
 			this->createCallPacket(
 				RecordingModuleInterface::startLocalRecordingStreamServiceId,
 				NULL, 0);
 		
 		rpcRes=rpc->call(*pk);			
 		callMutex.unlock();
 
 		delete pk;
 		pk=NULL;
 		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
 		
 		// Si rpcRes es null, l'anterior haura llençat una excepció.
 		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
 	}
 	catch(RecordingModuleSessionNotStablishedException &e)
 	{
 		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
 		if (pk!=NULL)
 			delete pk;
 	}
 	catch(Exception &e)
 	{
 		if (e.getClass() == string("RedirectCallException"))
 		{
 			cout<<"RecordingModuleAccess : WARNING RedirectCallException on startLocalRecording"<<endl;	
 		}
 		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
 		if (pk!=NULL)
 			delete pk;
		cout<<"startLocalRecStream:processExcFromRPC:"<<e.getClass()<<endl;
 		if (this->processExceptionFromRPC(e))
			throw;
 		if (this->successfulRedirect)
 			this->startRecordingStream();
 	}
 	catch (...)
 	{
 		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
 		if (pk!=NULL)
 			delete pk;
 	}
 }

void RecordingModuleAccess::stopRecordingStream()
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		pk = 
			this->createCallPacket(
				RecordingModuleInterface::stopRecordingStreamServiceId,
				NULL, 0);
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(RecordingModuleSessionNotStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
		if (this->successfulRedirect)
			this->stopRecordingStream();
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}


RecordingFileFrameChunk* RecordingModuleAccess::readFrame(RMReadFrameParams rfp)
{
	//std::cout<< "--> RecordingModuleAccess::readFrame -->"<<std::endl;
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{

		byte *params=(byte*)rfp.toNetwork();
		pk = 
			this->createCallPacket(
				RecordingModuleInterface::readFrameServiceId,
				params, rfp.serializationSize());
		delete [] params;


				if(pk==NULL) std::cout<< "--> readFrame: NULL POINTER !!-->"<<std::endl;

		rpcRes=rpc->call(*pk);		


		callMutex.unlock();

		delete pk;
		pk=NULL;


		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);


		dword chunkSize=*(dword*)rpcRes->data;
		Endian::from(Endian::xarxa, &chunkSize, sizeof(dword));
		RecordingFileFrameChunk *recvFrame = 
			(RecordingFileFrameChunk *)RecordingFileChunk::newChunk(
				((byte*)rpcRes->data) + sizeof(dword),
				chunkSize, Endian::xarxa);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;

		return recvFrame;
	}
	catch(RecordingModuleSessionNotStablishedException &e)
	{
		//std::cout<< "--> readFrame: catch1 -->"<<std::endl;
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		//std::cout<< "--> readFrame: catch2 -->"<<std::endl;
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
		if (this->successfulRedirect)
			return this->readFrame(rfp);
	}
	catch (...)
	{
		//std::cout<< "--> readFrame: catch3 -->"<<std::endl;
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return new RecordingFileFrameChunk();
}



void RecordingModuleAccess::saveFrame(RMSaveFrameParams sfp)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)sfp.toNetwork();
		pk = 
			this->createCallPacket(
				RecordingModuleInterface::saveFrameServiceId,
				params, sfp.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;
	}
	catch(RecordingModuleSessionNotStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
		if (this->successfulRedirect)
			this->saveFrame(sfp);
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
}


RMListRecordingsParams * RecordingModuleAccess::listRecordings(RMListRecordingsParams lrp)
{

	list<Canis::subsys> recmods =
		ServiceFinder::getAllSubsystems("RecordingModule", cn, false, false);

	byte *params=(byte*)lrp.toNetwork();
	RPCPacket *pk = 
		this->createCallPacket(
			RecordingModuleInterface::listRecordingsServiceId,
			params, lrp.serializationSize());
	
	delete [] params;

	RMListRecordingsParams *res =
		new RMListRecordingsParams();

	res->absStartDate=lrp.absStartDate;
	res->absEndDate=lrp.absEndDate;

	list<RMListRecordingsParams*> lres; 

	//cout<<"RMA::listRecordings :"<<recmods.size()<<endl;
	for (list<Canis::subsys>::iterator it=recmods.begin(); it!=recmods.end(); it++)
	{
		Address a(it->ip, it->port); 		

		RPC *rpcListRecs=new RPC(a);
		RPCPacket *pkRes=NULL;
		try
		{
			//cout<<"RMA::listRecordings calling "<<a.toString()<<endl;
			pkRes=rpcListRecs->call(*pk);

			RMListRecordingsParams* lrpServ = 
				new RMListRecordingsParams(pkRes->data);
			
			if (pkRes!=NULL)
				delete pkRes;

			lres.push_back(lrpServ);
		}
		catch(Exception e)
		{
			//cout<<"RecordingModuleAccess::listRecordings - error while checking server :"<<a.toString()<<"  ex:"<<e.getClass()<<"::"<<e.getMsg()<<endl;	
		}

		delete rpcListRecs;
	}

	int totalRecs=0;
	//cout<<"RMA::listRecordings counting for "<<lres.size()<<" modules"<<endl;
	
	for (list<RMListRecordingsParams*>::iterator it=lres.begin(); it!=lres.end(); it++)
	{
		RMListRecordingsParams*p=*it;	
		totalRecs+=p->nRecordings;
	}

	res->nRecordings=totalRecs;
	res->recordings=new RMListRecordingsParams::recording[totalRecs];

	int idx=0;
	//cout<<"RMA::listRecordings merging "<<totalRecs<<" recordings"<<endl;
	for (list<RMListRecordingsParams*>::iterator it=lres.begin(); it!=lres.end(); it++)
	{
		RMListRecordingsParams*p=*it;	
	//	cout<<"RMA::listRecordings add "<<p->nRecordings<<" to pos:"<<idx<<endl;
//		cout<<"RMA::listRecordings add from "<<(void*)p->recordings<<" to "<<(void*)res->recordings<<endl;
		for(int i=0; i<p->nRecordings; i++)
		{
//			cout<<"1:"<<endl;
			RMListRecordingsParams::recording r = p->recordings[i];
//			cout<<"2:"<<endl;
			res->recordings[idx+i] = r;
//			cout<<"3:"<<endl;
		}
		idx+=p->nRecordings;
		
		delete p;
	}

	return res;
}

RMIsRecordingParams RecordingModuleAccess::isRecording(RMIsRecordingParams isRec)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)isRec.toNetwork();

		pk = this->createCallPacket(
				RecordingModuleInterface::isRecordingServiceId,
				params, isRec.serializationSize());
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCResult(rpcRes, __func__, __FILE__, __LINE__);
		
		RMIsRecordingParams res(rpcRes->data);
		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;

		return res;
	}
	catch(RecordingModuleSessionNotStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
		if (this->successfulRedirect)
			this->stopRecordingStream();
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return RMIsRecordingParams();
}


list<RecordingModuleAccess::subsystemRecordingList> *RecordingModuleAccess::listRecordingsBySubsystem(RMListRecordingsParams lrp)
{
	list<Canis::subsys> recmods =
		ServiceFinder::getAllSubsystems("RecordingModule", cn, false, false);

	byte *params=(byte*)lrp.toNetwork();
	RPCPacket *pk = 
		this->createCallPacket(
			RecordingModuleInterface::listRecordingsServiceId,
			params, lrp.serializationSize());
	
	delete [] params;

	list<RecordingModuleAccess::subsystemRecordingList> *lres = new list<RecordingModuleAccess::subsystemRecordingList>(); 

	//cout<<"RMA::listRecordingsBySubsystem :"<<recmods.size()<<endl;
	for (list<Canis::subsys>::iterator it=recmods.begin(); it!=recmods.end(); it++)
	{
		Address a(it->ip, it->port); 		

		RPC *rpcListRecs=new RPC(a);
		RPCPacket *pkRes=NULL;
		try
		{
	//		cout<<"RMA::listRecordingsBySubsystem calling "<<a.toString()<<endl;
			pkRes=rpcListRecs->call(*pk);

			RMListRecordingsParams*lrpServ = 
				new RMListRecordingsParams(pkRes->data);
			
			if (pkRes!=NULL)
				delete pkRes;
			subsystemRecordingList srl(Address(it->ip, it->port), lrpServ);
			lres->push_back(srl);
		}
		catch(Exception e)
		{
		cout<<"RecordingModuleAccess::listRecordings - error while checking server :"<<a.toString()<<"  ex:"<<e.getClass()<<"::"<<e.getMsg()<<endl;	
		}

		delete rpcListRecs;
	}

	return lres;
}

RMGetFrameLimitParams RecordingModuleAccess::getFrameLimit(RMGetFrameLimitParams flp)
{
	callMutex.lock();

	RPC *rpc=this->getRPC();
	RPCPacket *pk=NULL, *rpcRes=NULL;
	try
	{
		byte *params=(byte*)flp.toNetwork();
		pk = 
			this->createCallPacket(
				RecordingModuleInterface::getFrameLimitServiceId,
				params, flp.serializationSize());
		
		delete [] params;
		
		rpcRes=rpc->call(*pk);			
		callMutex.unlock();

		delete pk;
		pk=NULL;
		this->processNullRPCDataResult(rpcRes, __func__, __FILE__, __LINE__);
		
		RMGetFrameLimitParams res(rpcRes->data);

		// Si rpcRes es null, l'anterior haura llençat una excepció.
		delete rpcRes;
		this->socketTimeoutExceptionCounter=0;

		return res;
	}
	catch(RecordingModuleSessionNotStablishedException &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	catch(Exception &e)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
		if (this->processExceptionFromRPC(e))
			throw;
		if (this->successfulRedirect)
			return this->getFrameLimit(flp);
	}
	catch (...)
	{
		if (this->callMutex.isHeldByCurrentThread())
			callMutex.unlock();
		if (pk!=NULL)
			delete pk;
	}
	return RMGetFrameLimitParams();
}

bool RecordingModuleAccess::processExceptionFromRPC(Exception &e)
{
	this->successfulRedirect=false;

	if (e.getClass()==string("SocketTimeoutException"))
		this->socketTimeoutExceptionCounter++;
	else if (e.getClass()==string("SocketException") || 
		e.getClass()==string("RPCException") )
		this->closeRPC();

	if (e.getClass()==string("RedirectCallException"))
	{
		this->socketTimeoutExceptionCounter=0;

		string redirMsg=e.getMsg();
      
	  cout << e.getClass() << ": " << redirMsg << " ending session "
		            " and starting a new session for device:"<<sesId.devId<<" with new RecordingModule at:"<<this->mod->a->toString()<<endl;

		string redir("Redirect: ");
		int pos=redirMsg.find(redir);

		if(pos!=string::npos)
		{
			string serv=redirMsg.substr(redir.length());

			pos=serv.find(":");
			if(pos!=string::npos)
			{
				string ip = serv.substr(0, pos);
				int port = atoi(serv.substr(pos+1).c_str());
				
				Address *newRMAddr=new Address(IP(ip), port);

				int devId = this->sesId.devId;
				try
				{
					RMEndSessionParams esp;
					esp.devId=this->sesId.devId;
					//cout<<"													endSession RMA.redirect mod:"<<this->mod->a->toString()<<" rpc:"<<(void*)this->rpc;
					//if(this->rpc != NULL)
					//	cout<<this->rpc->socket->getAddr().toString();
					//cout<<endl;
					this->endSession(esp);
				}
				catch(Exception &ex)
				{
					cout<<"processExcFromRPC -- exception while ending session in a redirect:"<<ex.getClass()<<":"<<ex.getMsg()<<endl;
				}
				this->sesId.devId = devId ; // l'endSession s'el carrega


				try
				{
					Address *dell = this->mod->a;
					this->mod->a = newRMAddr;
					if (dell != NULL)
						delete dell;
					
		//	cout<<"													startSession RMA.redirect"<<endl;
					//this->startSession(this->sesId, false);
					callMutex.lock();
					RPCPacket *rpcRes=NULL;
					rpcRes=this->startSessionCall(this->sesId);			
					if(rpcRes != NULL)
						delete rpcRes;
				}
				catch(Exception &ex)
				{
					if (this->callMutex.isHeldByCurrentThread())
						callMutex.unlock();
					cout<<"processExcFromRPC -- exception while redirecting:"<<ex.getClass()<<":"<<ex.getMsg()<<endl;
					// Llencem cap amunt qualsevol excepcio excepte
					// un already stablished
					if(ex.getClass() !=
				string("RecordingModuleSessionAlreadyStablishedException"))
					{
						this->successfulRedirect=false;
						throw;
					}
				}catch(...)
				{
					if (this->callMutex.isHeldByCurrentThread())
						callMutex.unlock();
						throw;
				}

				this->successfulRedirect=true;
			}
		}
		else
			throw ServiceException("Invalid RedirectCallException received");
	}
	else if (e.getClass()==string("SocketException") ||
		(e.getClass()==string("SocketTimeoutException") && 
		this->socketTimeoutExceptionCounter >= 
			ModuleAccess::socketTimeoutExceptionThreshold))
	{
		// Probablement necessitem buscar un nou modul, ja que el nostre
		// deu haver caigut
		if (!this->useFixedAddress)
		{
			RPCPacket *mod=this->mod;
			try
			{
				if (sesId.devId>0 || sesIdRecover.devId>0)
				{
					int devId;
					if (sesId.devId>0)
						devId=sesId.devId;
					else
						devId=sesIdRecover.devId;
//					cout <<"RecordingModuleAcces::processExceptionFromRPC:" << e.getClass() << " searching for best RecordingModule for device:"<< devId<< endl;
					this->mod=ServiceFinder::getNearestSubsysForDevice(this->typeIdStr, devId, this->cn->getInterfaceMask(), this->cn);
//					cout <<"RecordingModuleAcces::" << e.getClass() << " dev:"<< devId<<" "<< endl;
				}
				else
				{
//					cout <<"RecordingModuleAcces::processExceptionFromRPC:" << e.getClass() << " searching for best RecordingModule , no session started:"<< sesId.devId<<" recover:"<<sesIdRecover.devId<< endl;
					this->mod=ServiceFinder::getBestSubsys(this->typeIdStr, this->cn);
				}

				if (mod!=NULL)
					delete mod;

				this->socketTimeoutExceptionCounter=0;
			}
			catch (...)
			{
				this->mod=mod;
			}
		}
//		e.serialize()->materializeAndThrow(true);
		return true;
	}
	else if (e.getClass()==string("ServiceException") || e.getClass()==string("RecordingModuleSessionNotStablishedException") || e.getClass()==string("RPCException"))
	{
		this->socketTimeoutExceptionCounter=0;
		if (sesId.devId<=0)
		{
			cout << "Recovering from " << e.getClass() << " - searching new RecordingModule and starting session" << endl;
			
			sesIdRecover=sesId;
			
			sesId=RMStartSessionParams();
			
			try
			{
				this->startSession(sesIdRecover);
			}
			catch (Exception &sse)
			{
				if (sse.getClass() == string("RedirectCallException"))
				{
					if (this->processExceptionFromRPC(sse))
						throw;
					if (this->successfulRedirect)
						return false;
				}
					
				if (sesId.devId<=0)
					sesId=sesIdRecover;
//				e.serialize()->materializeAndThrow(true);
				return true;
			}
		}
		else
//			e.serialize()->materializeAndThrow(true);
			return true;
	}
	else
	{
		if(e.getClass()!=string("SocketTimeoutException"))
			this->socketTimeoutExceptionCounter=0;
//		e.serialize()->materializeAndThrow(true);
		return true;
	}
	return false;
}

