#ifndef SIRIUS_BASE_ENDIAN_ENDIAN_H_
#define SIRIUS_BASE_ENDIAN_ENDIAN_H_

#include <Utils/debugNew.h>
#include <stddef.h>
#include <string>

using namespace std;

class Endian
{
	public:
		enum endianType {LITTLE, BIG};
		static endianType local;
		static const endianType xarxa=BIG;		
		static endianType detectEndian(); 
		static string endianToString(endianType end); 

	private:
		static void change(void* data, size_t nbytes); 
		static void init(); 
		
	public:
		Endian();
		void* endiana(void* data, size_t nbytes);
		void* desendiana(void* data, size_t nbytes);
		void* endianTo(endianType end, void* data, size_t nbytes);
		void* endianFrom(endianType end, void* data, size_t nbytes);
		
		static void* to(endianType endian, void* data, size_t nbytes);
		static void* from(endianType endian, void* data, size_t nbytes);

};
#endif
