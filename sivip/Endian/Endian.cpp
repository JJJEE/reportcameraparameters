#include <Endian/Endian.h>

Endian::endianType Endian::local=Endian::detectEndian();//(*((char*)EndianInitVar)==1?Endian::BIG:Endian::LITTLE);

string endianNames[2]={string("Little Endian"), string("Big Endian")};

Endian::endianType Endian::detectEndian()
{
	int i=1;

	if(*((char *)&i)==1)
		return LITTLE;

	return BIG;
}

string Endian::endianToString(Endian::endianType end)
{
	if (end>=Endian::LITTLE && end<=Endian::BIG) 
		return endianNames[end];

	return string("Unkown Endian");
}

void Endian::change(void* data, size_t nbytes)
{
	register char *d=(char*)data, a,b;
	register int j=nbytes-1;
	nbytes>>=1;
	for(register int i=0;i<nbytes;i++)
	{
		d[j]^=d[i];
		d[i]^=d[j];
		d[j]^=d[i];
		j--;
	}
}

void Endian::init()
{
/*	int i=1;

	if(*((char *)&i)==1)
		Endian::local=LITTLE;
	else
		Endian::local=BIG;
*/
}
	
Endian::Endian()
{
	init();
}

void* Endian::endiana(void* data, size_t nbytes)
{
	if(Endian::local!=Endian::xarxa)
	{
		change(data, nbytes);
	}
	return data;
}

void* Endian::desendiana(void* data, size_t nbytes)
{
	if(Endian::local!=Endian::xarxa)
	{
		change(data, nbytes);
	}
	return data;
}


void* Endian::endianTo(endianType end, void* data, size_t nbytes)
{
	if(Endian::local!=end)
	{
		change(data, nbytes);
	}
	return data;
}

void* Endian::endianFrom(endianType end, void* data, size_t nbytes)
{
	if(Endian::local!=end)
	{
		change(data, nbytes);
	}
	return data;
}

void* Endian::to(endianType end, void* data, size_t nbytes)
{
	init();
	if(Endian::local!=end)
	{
		change(data, nbytes);
	}
	return data;
}

void* Endian::from(endianType end, void* data, size_t nbytes)
{
	init();
	if(Endian::local!=end)
	{
		change(data, nbytes);
	}
	return data;
}

