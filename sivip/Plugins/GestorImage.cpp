#include <Plugins/GestorImage.h>
#include <XML/xmlParser.h>
#include <XML/XML.h>
#include <XML/xmlNode.h>
#include <Utils/StrUtils.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <Plugins/GetBestPluginThread.h>

#include <WorkerThreads/WorkerThreadPool.h>
#include <Module/Module.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

#define __PLUGIN_NAMESPACE "ImagePlugin"

GestorImage::GestorImage(Address orig, string type, Canis *cn): Gestor(orig, type, cn)
{
	STACKTRACE_INSTRUMENT();
	
	// 512k, es d'imatge.
	this->rpcBuf=2*1024*1024;
}

GestorImage::GestorImage(Address orig, int type, Canis *cn): Gestor(orig, type, cn)
{
	STACKTRACE_INSTRUMENT();
	
	// 512k, es d'imatge.
	this->rpcBuf=2*1024*1024;
}

GestorImage::GestorImage(string file, Canis *cn): Gestor(file, cn)
{
	STACKTRACE_INSTRUMENT();
	
	// 512k, es d'imatge.
	this->rpcBuf=2*1024*1024;
}

GestorImage::~GestorImage()
{
	STACKTRACE_INSTRUMENT();

}

void GestorImage::startSession(IPDeviceID devId)
{
	STACKTRACE_INSTRUMENT();

	dword idDev=devId.id;
//	cout << "--> GestorImage::startSession fBPFD" << endl;
	plgInfo=findBestPluginForDevice(idDev, string(__PLUGIN_NAMESPACE));
//	cout << "\tGestorImage::startSession /fBPFD" << endl;

	byte *devIdNet=(byte*)devId.toNetwork();

//	cout << "GI::startSession: to " << plgInfo->a.toString() << endl;
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, IPServ::start_session, devIdNet, devId.serializationSize(), plgTypeId);
	
	delete [] devIdNet;
	
//	cout<<"  GestorImage::StartSession  call: "<<plgInfo->a.toString()<<endl;
//	cout << "\tGestorImage::startSession call" << endl;
	CallData cdret=call(cdp);
//	cout << "<-- GestorImage::startSession /call" << endl;
//	cout<<"  GestorImage::StartSession  call returned "<<endl;
	
	sessionDev=idDev;
}

void GestorImage::endSession(IPDeviceID devId)
{
	STACKTRACE_INSTRUMENT();

	dword idDev=devId.id;
	if (sessionDev==0xffffffff)
		throw(IPSessionNotStablishedException(0, "Sessi�� no establerta"));

	if (sessionDev!=idDev)
		throw(IPSessionNotStablishedException(0, "Sessi�� no establerta"));

	plgInfo=findBestPluginForDevice(idDev, string(__PLUGIN_NAMESPACE));
	
	byte *devIdNet=(byte*)devId.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, IPServ::end_session, devIdNet, devId.serializationSize(), plgTypeId);
	
	delete [] devIdNet;

	CallData cdret=call(cdp);

	sessionDev=0xffffffff;
}

IPDeviceInfo GestorImage::getDeviceInformation(IPDeviceInfo dev)
{
	STACKTRACE_INSTRUMENT();

	dword idDev=dev.id.id;
	plgInfo=findBestPluginForDevice(idDev, string(__PLUGIN_NAMESPACE));

	int sz=dev.serializationSize();
	byte *v=(byte*)dev.toNetwork();
	
	// cout  << "\t--> calldata" << endl;
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::get_device_information, v, sz, plgTypeId);

	delete [] v;
	
	CallData cdret=call(cd);

	IPDeviceInfo res(cdret.data);

	return res;
}

IPCodecInfo GestorImage::getCodecInUse()
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::get_codec_in_use, NULL, 0, plgTypeId);
	CallData cdret=call(cd);

	IPCodecInfo res(cdret.data);

	return res;
}

void GestorImage::setCodecInUse(IPCodecInfo codec)
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	int sz=codec.serializationSize();
	byte *codecNet=(byte*)codec.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::set_codec_in_use, codecNet, sz, plgTypeId);
	delete [] codecNet;
	CallData cdret=call(cd);

//	if (cd.data!=NULL)
//		delete [] cd.data;
}

IPFrameBufferSize GestorImage::getFrameBufferSize()
{
	STACKTRACE_INSTRUMENT();

//	cout << "IP " << a.toString() << endl;

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::get_frame_buffer_size, NULL, 0, plgTypeId);
	CallData cdret=call(cd);
	IPFrameBufferSize res(cdret.data);

	return res;
}

void GestorImage::setFrameBufferSize(IPFrameBufferSize size)
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	int sz=size.serializationSize();
	byte *sizeNet=(byte*)size.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::set_frame_buffer_size, sizeNet, sz, plgTypeId);
	delete [] sizeNet;
	CallData cdret=call(cd);
}

float GestorImage::getFrameBufferPercentInUse()
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::get_frame_buffer_percent_in_use, NULL, 0, plgTypeId);
	CallData cdret=call(cd);
	float res=*(float*)cdret.data;

	Endian::from(Endian::xarxa, &res, sizeof(float));	

	return res;
}

int GestorImage::getFrameBufferFramesInUse()
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::get_frame_buffer_frames_in_use, NULL, 0, plgTypeId);
	CallData cdret=call(cd);
	int res=*(int*)cdret.data;

	Endian::from(Endian::xarxa, &res, sizeof(int));	

	return res;
}


IPStreamingMode GestorImage::getStreamingMode()
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::get_streaming_mode, NULL, 0, plgTypeId);
	CallData cdret=call(cd);
	IPStreamingMode res(cdret.data);
	
	return res;
}

void GestorImage::setStreamingMode(IPStreamingMode mode)
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	int sz=mode.serializationSize();
	byte *modeNet=(byte*)mode.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::set_streaming_mode, modeNet, sz, plgTypeId);
	delete [] modeNet;
	CallData cdret=call(cd);

//	if (cd.data!=NULL)
//		delete [] cd.data;
}

IPFrame GestorImage::getCompressedNextFrame()
{
	STACKTRACE_INSTRUMENT();

	bool doDebug = false;
	int *debug = WorkerThreadPool<ModuleServiceThread>::getMyDebugPtr();
	doDebug = (debug != NULL);

	if(doDebug)
		*debug = 100;

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	if(doDebug)
		*debug = 200;

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	if(doDebug)
		*debug = 300;
	CallData cd(plgAddr, IPServ::get_compressed_next_frame, NULL, 0, plgTypeId);
	CallData cdret=call(cd);

	
	if(doDebug)
		*debug = 400;
	IPFrame res(cdret.data);
	return res;
}

IPFrame GestorImage::getDecompressedNextFrame()
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::get_decompressed_next_frame, NULL, 0, plgTypeId);
	CallData cdret=call(cd);

	IPFrame res(cdret.data);
	return res;
}

IPFramesPerSecond GestorImage::getFPS()
{
	STACKTRACE_INSTRUMENT();

//	cout << "IP " << a.toString() << endl;

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::get_fps, NULL, 0, plgTypeId);
	CallData cdret=call(cd);

	IPFramesPerSecond res(cdret.data);
	return res;
}

void GestorImage::setFPS(IPFramesPerSecond fps)
{
	STACKTRACE_INSTRUMENT();

	if(plgInfo==NULL)
		throw IPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	int sz=fps.serializationSize();
	byte *fpsNet=(byte*)fps.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, IPServ::set_fps, fpsNet, sz, plgTypeId);
	delete [] fpsNet;
	CallData cdret=call(cd);

//	if (cd.data!=NULL)
//		delete [] cd.data;

}

