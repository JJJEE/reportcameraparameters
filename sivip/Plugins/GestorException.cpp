#include <Plugins/GestorException.h>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

GestorException::GestorException(int code, string msg):Exception(code,msg)
{}

GestorException::GestorException(SerializedException &se):Exception(se)
{}

string GestorException::getClass()
{
	string c=string("GestorException");
	return c;
}

