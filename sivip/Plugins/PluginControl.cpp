#include <Plugins/PluginControl.h>
#include <Plugins/GestorControlTypes.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;

void PluginControl::init()
{
	this->numServices=PluginControl::serviceCount;
	this->services=new serviceDef[this->numServices];
	memset(this->services, 0, sizeof(serviceDef)*this->numServices);
	
	this->services[PluginControl::startSessionServiceId].call = (DefaultPluginCall)startSessionService;
	this->services[PluginControl::endSessionServiceId].call = (DefaultPluginCall)endSessionService;

	this->services[PluginControl::getPTZServiceId].call = (DefaultPluginCall)getPTZService;
	this->services[PluginControl::setPTZServiceId].call = (DefaultPluginCall)setPTZService;

	this->services[PluginControl::setInputServiceId].call = (DefaultPluginCall)setInputService;
	this->services[PluginControl::getInputServiceId].call = (DefaultPluginCall)getInputService;

	this->services[PluginControl::setOutputServiceId].call = (DefaultPluginCall)setOutputService;
	this->services[PluginControl::getOutputServiceId].call = (DefaultPluginCall)getOutputService;

	this->services[PluginControl::setCommandBufferSizeServiceId].call = (DefaultPluginCall)setCommandBufferSizeService;
	this->services[PluginControl::getCommandBufferSizeServiceId].call = (DefaultPluginCall)getCommandBufferSizeService;

	this->services[PluginControl::getCommandBufferPercentInUseServiceId].call = (DefaultPluginCall)getCommandBufferPercentInUseService;
	this->services[PluginControl::getCommandBufferCommandsInUseServiceId].call = (DefaultPluginCall)getCommandBufferCommandsInUseService;

	this->services[PluginControl::getMetadataValueServiceId].call = (DefaultPluginCall)getMetadataValueService;
	this->services[PluginControl::setMetadataValueServiceId].call = (DefaultPluginCall)setMetadataValueService;

	this->services[PluginControl::getConfigParamServiceId].call = (DefaultPluginCall)getConfigParamService;
	this->services[PluginControl::getConfigParamRecursiveServiceId].call = (DefaultPluginCall)getConfigParamRecursiveService;
	this->services[PluginControl::setConfigParamServiceId].call = (DefaultPluginCall)setConfigParamService;
	this->services[PluginControl::getAllConfigParamsServiceId].call = (DefaultPluginCall)getAllConfigParamsService;

	this->services[PluginControl::moveServiceId].call = (DefaultPluginCall)moveService;

}

RPCPacket* PluginControl::startSessionService(PluginControl *_this, Address *a, void *params)
{
	CPDeviceID id(params);

	_this->startSession(id, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginControl::endSessionService(PluginControl *_this, Address *a, void *params)
{
	CPDeviceID id(params);

	_this->endSession(id, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}


RPCPacket* PluginControl::getPTZService(PluginControl *_this, Address *a, void *params)
{
	CPPTZ res=_this->getPTZ(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginControl::setPTZService(PluginControl *_this, Address *a, void *params)
{
	CPPTZ ptz(params);
	
	_this->setPTZ(ptz, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}


RPCPacket* PluginControl::setInputService(PluginControl *_this, Address *a, void *params)
{
	CPInput in(params);
	
	_this->setInput(in, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	
	return pk;
}

RPCPacket* PluginControl::getInputService(PluginControl *_this, Address *a, void *params)
{
	CPInput in(params);
	
	CPInput res=_this->getInput(in, a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


RPCPacket* PluginControl::setOutputService(PluginControl *_this, Address *a, void *params)
{
	CPOutput out(params);
	
	_this->setOutput(out, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginControl::getOutputService(PluginControl *_this, Address *a, void *params)
{
	CPOutput out(params);
	
	CPOutput res=_this->getOutput(out, a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


RPCPacket* PluginControl::setCommandBufferSizeService(PluginControl *_this, Address *a, void *params)
{
	CPCommandBufferSize size(params);
	
	_this->setCommandBufferSize(size, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginControl::getCommandBufferSizeService(PluginControl *_this, Address *a, void *params)
{
	CPCommandBufferSize res=_this->getCommandBufferSize(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


RPCPacket* PluginControl::getCommandBufferPercentInUseService(PluginControl *_this, Address *a, void *params)
{
	float res=_this->getCommandBufferPercentInUse(a);

	Endian::to(Endian::xarxa, &res, sizeof(float));

	byte *pkData=(byte*)&res;
	dword pkDataSize=sizeof(float);
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginControl::getCommandBufferCommandsInUseService(PluginControl *_this, Address *a, void *params)
{
	int res=_this->getCommandBufferCommandsInUse(a);

	Endian::to(Endian::xarxa, &res, sizeof(int));
	
	byte *pkData=(byte*)&res;
	dword pkDataSize=sizeof(int);
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

		
RPCPacket* PluginControl::getMetadataValueService(PluginControl *_this, Address *a, void *params)
{
	CPMetadata m(params);

	CPMetadata res=_this->getMetadataValue(m, a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginControl::setMetadataValueService(PluginControl *_this, Address *a, void *params)
{
	CPMetadata m(params);

	_this->setMetadataValue(m, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}


RPCPacket* PluginControl::getConfigParamService(PluginControl *_this, Address *a, void *params)
{
	CPConfigParam cp(params);

	CPConfigParam res=_this->getConfigParam(cp, a);
	
	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginControl::getConfigParamRecursiveService(PluginControl *_this, Address *a, void *params)
{
	CPConfigParam cp(params);

	CPConfigParamSeq res=_this->getConfigParamRecursive(cp, a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginControl::setConfigParamService(PluginControl *_this, Address *a, void *params)
{
	CPConfigParam cp(params);

	_this->setConfigParam(cp, a);
	
	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginControl::getAllConfigParamsService(PluginControl *_this, Address *a, void *params)
{
	CPConfigParamSeq res=_this->getAllConfigParams(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginControl::moveService(PluginControl *_this, Address *a, void *params)
{
	CPMovement move(params);
	
	_this->move(move, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

PluginControl::PluginControl(Address addr, string type) : Plugin(addr, type)
{
	init();
}

PluginControl::PluginControl(Address addr) : Plugin(addr, "ControlPlugin")
{
	init();
}

PluginControl::PluginControl(string file) : Plugin(file)
{
	init();	
}
