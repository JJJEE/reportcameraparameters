#pragma once
#include <Utils/debugNew.h>
#include <Sockets/SocketUDP.h>
#include <Sockets/Address.h>
#include <Threads/Thread.h>
#include <Threads/RWlock.h>
#include <Utils/Canis.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>

class CallData
{
public:
	Address dest;
	word id;
	void* data;
	dword dsize;
	short destType;
	string destName;
	
	CallData(Address dest, word id, void* data, dword dsize, short destType);
	CallData(Address dest, word id, void* data, dword dsize, string destName);
	CallData(word id, void* data, dword dsize, short destType);
	CallData(word id, void* data, dword dsize, string destName);
	CallData(const CallData &cd);
	CallData operator=(const CallData &cd);
	CallData();
	
	virtual ~CallData();

	void setData(void *data, dword size);
}; 

class Gestor //: public Thread
{
protected:
	struct pluginInfo
	{
		Address a;
		string type;
		word typeId;
		
		pluginInfo(Address a, string type, word typeId);
		virtual ~pluginInfo();
	};
	
	pluginInfo *findBestPluginForDevice(dword devId, string pluginTypeNameSpace);

	RWLock plgInfoLock;
	bool renewPlgInfo;
	pluginInfo *plgInfo;
	dword sessionDev;		// Device amb el q tenim establerta sessio

	protected:
		Address db;
		Address orig;
		string type;
		short idType;
		bool newCanis;
				
		Address *keepAlive;
		Mutex rpcLock;
		RPC *rpcCrida;
		dword rpcBuf;
		
	public:
		Canis *cn;
		Gestor(Address orig, string type, Canis *cn=NULL);
		Gestor(Address orig, int type, Canis *cn=NULL);
		Gestor(string file, Canis *cn=NULL);
		virtual ~Gestor();
		CallData call(CallData &cd);
		
		virtual void setKeepAliveAddress(Address *a=NULL);
		virtual void clearKeepAliveAddress();
		
		virtual void clearPlgInfo();
		
		Address getCurrentPluginAddress();
		Address getLocalAddr();
		//int sendCall(CallData cd);
		//CallData getResult(int idCall);
};

