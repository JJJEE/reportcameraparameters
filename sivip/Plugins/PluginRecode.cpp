#include <Plugins/PluginRecode.h>
#include <Plugins/GestorRecodeTypes.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;

void PluginRecode::init()
{
	this->numServices=PluginRecode::serviceCount;
	this->services=new serviceDef[this->numServices];
	memset(this->services, 0, sizeof(serviceDef)*this->numServices);

	this->services[PluginRecode::startSessionServiceId].call = (DefaultPluginCall)startSessionService;
	this->services[PluginRecode::endSessionServiceId].call = (DefaultPluginCall)endSessionService;

	this->services[PluginRecode::decodeFrameServiceId].call = (DefaultPluginCall)decodeFrameService;
	this->services[PluginRecode::encodeFrameServiceId].call = (DefaultPluginCall)encodeFrameService;
	this->services[PluginRecode::getFrameServiceId].call = (DefaultPluginCall)getFrameService;

}

RPCPacket* PluginRecode::startSessionService(PluginRecode *_this, Address *a, void *params)
{
	RPCodecInfo id(params);
	
	cout<<"PluginRecode::startSession:"<<(void*)a<<endl;
	_this->startSession(id, a);
	cout<<"/ PluginRecode::startSession:"<<endl;

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginRecode::endSessionService(PluginRecode *_this, Address *a, void *params)
{
	RPCodecInfo id(params);
	
	cout<<"PluginRecode::endSession:"<<(void*)a<<endl;
	_this->endSession(id, a);
	cout<<"/ PluginRecode::endSession"<<endl;

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginRecode::decodeFrameService(PluginRecode *_this, Address *a, void *params)
{
	RPFrame cp(params);

	cout<<"PluginRecode::DecodeFrame :"<<(void*)a<<endl;
	RPFrame *res=_this->decodeFrame(&cp, a);
	
	byte *pkData=(byte*)res->toNetwork();
	dword pkDataSize=res->serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	
	delete [] pkData;
	delete res;

	return pk;
}

RPCPacket* PluginRecode::encodeFrameService(PluginRecode *_this, Address *a, void *params)
{
	RPFrame cp(params);

	cout<<"PluginRecode::EncodeFrame :"<<(void*)a<<endl;
	_this->encodeFrame(&cp, a);
	
	cout<<" / PluginRecode::EncodeFrame"<<endl;

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginRecode::getFrameService(PluginRecode *_this, Address *a, void *params)
{
	cout<<"PluginRecode::GetFrame :"<<(void*)a<<endl;
	RPFrame *res=_this->getFrame(a);
	
	byte *pkData=(byte*)res->toNetwork();
	dword pkDataSize=res->serializationSize();;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	
	delete [] pkData;
	delete res;

	return pk;
}

PluginRecode::PluginRecode(Address addr) : Plugin(addr, "RecodePlugin")
{
	init();
}

PluginRecode::PluginRecode(string file) : Plugin(file)
{
	init();	
}

