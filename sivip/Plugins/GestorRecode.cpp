#include <Plugins/GestorRecode.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

void GestorRecode::findBestPluginsForSession(RPSession ses)
{
	if (inaddr.getIP().toDWord()!=0 && outaddr.getIP().toDWord()!=0)
	{
		return;
	}

	GestorRecodeGetBestPluginThread th;
	GestorRecodeGetBestPluginThread::args th1a, th2a, th3a;
	
	if (ses.in.name==string("") || ses.out.name==string(""))
	{
		throw (Exception(0, string("Invalid codec id:")+ses.in.name+string(" , ")+ses.out.name));
	}
	
/*	if (pluginType!=string("") && servei.id!=-1) // ???
	{
		return;
	}	
*/	
	th1a.cn=this->cn;
	th1a.pluginType=string("RecodePlugin");
	int th1id=th.start(&th1a, Thread::OnExceptionExitThread);


	
	
//	RPCPacket *db=ServiceFinder::getBestSubsys(string("DBGateway"), cn);
//	if (dbrpc==NULL)
//		dbrpc=new RPC(*db->a);
	
	th2a.cn=this->cn;
	th2a.pluginType=string("RecodePlugin::")+ses.in.name;
//	cout<<"RecodePlugin findBestPlugin : "<<th2a.pluginType<<endl;
	int th2id=th.start(&th2a, Thread::OnExceptionExitThread);

	th3a.cn=this->cn;
	th3a.pluginType=string("RecodePlugin::")+ses.out.name;
//	cout<<"RecodePlugin findBestPlugin : "<<th3a.pluginType<<endl;
	int th3id=th.start(&th3a, Thread::OnExceptionExitThread);


//cout<<" GestCont::getBPh join 1"<<endl;
	RPCPacket *plugin1=(RPCPacket *)th.join(th1id);
//cout<<" GestCont::getBPh join 2"<<endl;
	RPCPacket *plugin2=(RPCPacket *)th.join(th2id);
//cout<<" GestCont::getBPh join 3"<<endl;
	RPCPacket *plugin3=(RPCPacket *)th.join(th3id);
//cout<<" GestCont::getBPh joined"<<endl;

	RPCPacket *plugin=NULL;
	string pt;

	if (plugin2!=NULL)
	{
	//	plugin=plugin1;
	//	pt=th1a.pluginType;
		inaddr=*(plugin2->a);
		inType=th2a.pluginType;
		inTypeId=th2a.pluginTypeId;
	}
	else if (plugin1!=NULL)
	{
		inaddr=*(plugin1->a);
		inType=th1a.pluginType;
		inTypeId=th1a.pluginTypeId;
	}
	else
	{
		throw (Exception(0, "Failed to find decode plugin"));
	}
	
	if (plugin3!=NULL)
	{
		outaddr=*(plugin3->a);
		outType=th3a.pluginType;
		outTypeId=th3a.pluginTypeId;
	}
	else if (plugin1!=NULL)
	{
		outaddr=*(plugin1->a);
		outType=th1a.pluginType;
		outTypeId=th1a.pluginTypeId;
	}
	else
	{
		throw (Exception(0, "Failed to find encode plugin (GestorRecode)"));
	}

//cout<<"GestorRecode::FindBestPlForDevice: plugin*:"<<((void*)plugin)<<endl;
//cout<<"GestorRecode::FindBestPlForDevice: plugin->a:"<<((void*)plugin->a)<<endl;
//cout<<"GestorRecode::FindBestPlForDevice: plugin->a:"<<plugin->a->toString()<<endl;
//cout<<"GestorRecode::FindBestPlForDevice:"<<a.toString()<<endl;
//cout<<"GestorRecode::FindBestPlForDevice typ:"<<pt<<endl;
	
	if (plugin1!=NULL)
		delete plugin1;

	if (plugin2!=NULL)
		delete plugin2;

	if (plugin3!=NULL)
		delete plugin3;

}


GestorRecode::GestorRecode(Address orig, string type, Canis *cn): Gestor(orig, type, cn), out(orig, type, cn),/* Gestor(orig, type),*/ inType(string("")), outType(string("")), dbrpc(NULL)
{}

GestorRecode::GestorRecode(Address orig, int type, Canis *cn):Gestor(orig, type, cn), out(orig, type, cn),/*Gestor(orig, type),*/ inType(string("")), outType(string("")), dbrpc(NULL)
{}

GestorRecode::GestorRecode(string file, Canis *cn):Gestor(file, cn), out(file, cn),/* Gestor(file),*/ inType(string("")), outType(string("")), dbrpc(NULL)
{}

GestorRecode::~GestorRecode()
{
	if (dbrpc!=NULL)
		delete dbrpc;
}

void GestorRecode::startSession(RPSession idservei)
{
	findBestPluginsForSession(idservei);

	servei=idservei; 
	byte* buf=(byte*)idservei.in.toNetwork();
	
	CallData cdi(inaddr, RPServ::startSession, buf, idservei.in.serializationSize(),inTypeId);
	RPC::keepAlive(&orig, 0);
	cout<<"GestRec:startSes:Call: decode :"<<idservei.in.name<<" mode:"<<idservei.in.mode<<" : "<<inaddr.toString()<<endl;
	
	delete [] buf;

	call(cdi); //TODO: async :)

	buf=(byte*)idservei.out.toNetwork();

	CallData cdo(outaddr, RPServ::startSession, buf, idservei.out.serializationSize(),outTypeId);
	cout<<"GestRec:startSes:Call: encode :"<<idservei.out.name<<" mode:"<<idservei.out.mode<<" : "<<outaddr.toString()<<endl;
	RPC::keepAlive(&orig, 0);
	
	delete [] buf;

	out.call(cdo);
	cout<<"GestRec:startSes:Call:End"<<endl;
}




void GestorRecode::endSession(RPSession idservei)
{
	if(servei.in.name==string("") || servei.out.name==string("")) //??? tb estava al GestCtrl
	{
		throw(RPSessionNotStablishedException(0, "SessiĂł no establerta"));
	}
	findBestPluginsForSession(idservei); //??? tb estava al GestCtrl
	
	byte *buf=(byte*)idservei.in.toNetwork();
	
	CallData cdi(inaddr, RPServ::endSession, buf, idservei.in.serializationSize(),inTypeId);
	
	delete [] buf;
	
	call(cdi); 

	buf=(byte*)idservei.out.toNetwork();
	CallData cdo(outaddr, RPServ::endSession, buf, idservei.out.serializationSize(),outTypeId);
	delete [] buf;
	out.call(cdo);

	servei=RPSession();
}


void GestorRecode::processFrame(RPFrame p)
{
	int sz=p.serializationSize();
	byte *buf=(byte*)p.toNetwork();

	CallData cd(inaddr, RPServ::decodeFrame, buf, sz, inTypeId);
	
	delete [] buf;
	
	cout<<"GestorRecode::ProcessFrame call decode"<<endl;
	CallData cdraw=call(cd);	//desencodem...
	cout<<"GestorRecode::ProcessFrame /call decode"<<endl;
	RPC::keepAlive(&orig, 0);

	cdraw.dest=outaddr;
	cdraw.destType=outTypeId;
	cdraw.id=RPServ::encodeFrame;

	cout<<"GestorRecode::ProcessFrame call encode"<<endl;
	out.call(cdraw);	//... i reencodem
	cout<<"GestorRecode::ProcessFrame /call encode"<<endl;
	
}

RPFrame GestorRecode::getFrame()
{
	CallData cd(outaddr, RPServ::getFrame, NULL, 0, outTypeId);
	cout<<"GestorRecode::ProcessFrame call getFrame"<<endl;
	CallData cdres=out.call(cd);	//desencodem...
	cout<<"GestorRecode::ProcessFrame /call getFrame"<<endl;
	
	RPFrame res(cdres.data);
	return res;
}


GestorRecodeGetBestPluginThread::GestorRecodeGetBestPluginThread()
{
}

GestorRecodeGetBestPluginThread::~GestorRecodeGetBestPluginThread()
{
}
	
void* GestorRecodeGetBestPluginThread::execute(int id, void *args)
{
	GestorRecodeGetBestPluginThread::args *a=(GestorRecodeGetBestPluginThread::args *)args;

	try
	{
		if (a->pluginType==string(""))
		{
			RPCPacket *p=ServiceFinder::getBestSubsys(a->pluginTypeId, a->cn);
			return p;
		}
		else
		{
			RPCPacket *p=ServiceFinder::getBestSubsys(a->pluginType, a->cn);
			a->pluginTypeId=p->getOrType();
			return p;
		}
	}
	catch (Exception &e)
	{
		cout << "\tGestorRecodeGetBestPluginThread Exception, " << e.getClass() << ": " << e.getMsg() << endl;
	}

	return NULL;
}


