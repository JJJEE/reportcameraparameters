#pragma once
#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <Plugins/GestorControlExceptions.h>
#include <Plugins/GestorControlTypes.h>
#include <Sockets/Address.h>

class GestorControl: public Gestor
{
	private:
	
//		RPC *dbrpc;
//		string pluginType;
//	
//		void findBestPluginForDevice(CPDeviceID id);

	public:
//		CPDeviceID servei;	
//		Address a;
		GestorControl(Address orig, string type, Canis *cn=NULL);
		GestorControl(Address orig, int type, Canis *cn=NULL);
		GestorControl(string file, Canis *cn=NULL);
		virtual ~GestorControl();

		void startSession(CPDeviceID idservei);
		void endSession(CPDeviceID id);

		CPPTZ getPTZ();
		void setPTZ(CPPTZ ptz);

		void setInput(CPInput in);
		CPInput getInput(CPInput in);

		void setOutput(CPOutput out);
		CPOutput getOutput(CPOutput out);

		void setCommandBufferSize(CPCommandBufferSize size);
		CPCommandBufferSize getCommandBufferSize();

		float getCommandBufferPercentInUse();
		int getCommandBufferCommandsInUse();
		
		CPMetadata getMetadataValue(CPMetadata data);
		void setMetadataValue(CPMetadata data);

		CPConfigParam getConfigParam(CPConfigParam p);
		CPConfigParamSeq getConfigParamRecursive(CPConfigParam p);
		void setConfigParam(CPConfigParam p);
		CPConfigParamSeq getAllConfigParams();

		void move(CPMovement move);
};
