#pragma once
#include <Utils/debugNew.h>
#include <Endian/Endian.h>
#include <Utils/Types.h>
#include <string>
#include <list>
using namespace std;

#pragma mark *** IPCodecInfo
class IPCodecInfo
{
public:
	dword fourcc;
	dword quality;
	dword bitrate;
	
	IPCodecInfo();
	IPCodecInfo(void *buf);
	IPCodecInfo(dword fourcc, dword quality, dword bitrate);
	~IPCodecInfo();
	
	void toLocal(void *buf);
	void *toNetwork(void *buf=NULL);
	
	dword serializationSize();
};

#pragma mark *** IPCodecInfoSeq
class IPCodecInfoSeq : public list<IPCodecInfo>
{
public:
	IPCodecInfoSeq();
	IPCodecInfoSeq(void* buf);
	~IPCodecInfoSeq();
	
	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

#pragma mark *** IPFrameInfo
class IPFrameInfo 
{
public:
	dword x, y, bpp;

	IPFrameInfo();
	IPFrameInfo(void *buf);
	IPFrameInfo(dword x, dword y, dword bpp);
	~IPFrameInfo();
	
	void toLocal(void *buf);
	void* toNetwork(void *buf=NULL);
	
	dword serializationSize();
};

#pragma mark *** IPFrameInfoSeq
class IPFrameInfoSeq : public list<IPFrameInfo>
{
public:
	IPFrameInfoSeq();
	IPFrameInfoSeq(void* buf);
	~IPFrameInfoSeq();
	
	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

#pragma mark *** IPDeviceID
class IPDeviceID
{
public:
	int id;
	
	IPDeviceID();
	IPDeviceID(void *buf);
	IPDeviceID(int id);
	~IPDeviceID();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
	
	bool operator < (const IPDeviceID) const;
	friend ostream& operator << (ostream& stream, IPDeviceID id);
};

#pragma mark *** IPDeviceInfo
class IPDeviceInfo
{
public:
	IPDeviceID id;
	string make;
	string model;
	IPFrameInfoSeq res;
	IPCodecInfoSeq codecs;
	
	IPDeviceInfo();
	IPDeviceInfo(void *buf);
	IPDeviceInfo(int id, string make, string model, IPFrameInfoSeq res,
		IPCodecInfoSeq codecs);
	IPDeviceInfo(IPDeviceID id, string make, string model,
		IPFrameInfoSeq res, IPCodecInfoSeq codecs);
	~IPDeviceInfo();
	
	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);
	
	dword serializationSize();
};

#pragma mark *** IPFrameBufferSize
class IPFrameBufferSize
{
public:
	dword nFrames;
	
	IPFrameBufferSize();
	IPFrameBufferSize(void *buf);
	IPFrameBufferSize(dword nFrames);
	~IPFrameBufferSize();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);
	
	dword serializationSize();
};

#pragma mark *** IPFrame
class IPFrame
{
public:
	IPFrameInfo frameInfo;
	IPCodecInfo codecInfo;
	bool isKey;
	dword frameLength;
	void* frame;
	
	IPFrame();
	IPFrame(void *buf);
	IPFrame(IPFrameInfo frameInfo, IPCodecInfo codecInfo, bool isKey,
		dword frameLength, void *frame);
	~IPFrame();
	IPFrame(const IPFrame& f);
//	IPFrame & operator=(const IPFrame& f);

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);
	
	dword serializationSize();
};

#pragma mark *** IPStreamingMode
class IPStreamingMode
{
public:
	static const dword NOSTREAM=0;
	static const dword STREAM=1;

	dword mode;
	
	IPStreamingMode();
	IPStreamingMode(void *buf);
	IPStreamingMode(dword nFrames);
	~IPStreamingMode();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);
	
	dword serializationSize();
};

#pragma mark *** IPFramesPerSecond
struct IPFramesPerSecond
{
public:
	word whole;
	word frac;
	
	IPFramesPerSecond();
	IPFramesPerSecond(void *buf);
	IPFramesPerSecond(word fpswhole, word fpsfrac);
	IPFramesPerSecond(double fps);
	~IPFramesPerSecond();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);
	
	dword serializationSize();
};

static const int IP_NUM_SERV = 15; 

class IPServ
{
public:
	static const int get_device_information=0;
	static const int start_session=1;
	static const int end_session=2;
	static const int get_codec_in_use=3;
	static const int set_codec_in_use=4;
	static const int get_frame_buffer_size=5;
	static const int set_frame_buffer_size=6;
	static const int get_frame_buffer_percent_in_use=7;
	static const int get_frame_buffer_frames_in_use=8;
	static const int get_streaming_mode=9;
	static const int set_streaming_mode=10;
	static const int get_compressed_next_frame=11;
	static const int get_decompressed_next_frame=12;
	static const int get_fps=13;
	static const int set_fps=14;
	
	static const int num_serv = 15; 
};

