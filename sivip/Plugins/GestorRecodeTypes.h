#pragma once
#include <Utils/debugNew.h>
#include <Endian/Endian.h>
#include <Utils/Types.h>
#include <string>
#include <list>
using namespace std;

#pragma mark *** RPCodecInfo
class RPCodecInfo
{
public:
	static const int encode=0; 
	static const int decode=1; 

	string name;
	int mode;
	int width, height;

	RPCodecInfo();
	RPCodecInfo(void *buf);
	RPCodecInfo(string name, int mode, int width, int height);
	~RPCodecInfo();

	void toLocal(void *buf);
	void *toNetwork(void *buf=NULL);
	
	dword serializationSize();
};

#pragma mark *** RPSession
class RPSession
{
public:
	RPCodecInfo in, out;
	RPSession();
	RPSession(void *buf);
	RPSession(RPCodecInfo in, RPCodecInfo out);
	~RPSession();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);
	dword serializationSize();
};

#pragma mark *** RPFrame
class RPFrame
{
public:
	// Per defecte 256 bytes de padding, veurem que passa amb
	// avcodec_decode_video, si continua sortint-se de les dades
	// d'entrada
	static const int defaultPadding=256;
	
	static const int maxWidth=2048;
	static const int maxHeight=1536;
			
	int dsize;
	int width, height;
	int bitDepth;
	void* data;
	bool isKey, ownData;
	int padding;
			
	RPFrame();
	RPFrame(void *buf);
	RPFrame(int size, int width, int height, void* data,
		bool isKey=true, bool dontCopy=false,
		int padding=RPFrame::defaultPadding);
	RPFrame(const RPFrame& f);
	~RPFrame();

	void setOwnData(bool own);
	RPFrame & operator=(const RPFrame& f);

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);
	dword serializationSize();
};


class RPServ {
public:
	static const int startSession=0; 
	static const int endSession=1; 
	static const int decodeFrame=2; 
	static const int encodeFrame=3; 
	static const int getFrame=4; 

	static const int RPNumServ = 5; 
};

