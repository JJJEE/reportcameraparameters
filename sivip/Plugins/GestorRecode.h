#pragma once
#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <Plugins/GestorRecodeExceptions.h>
#include <Plugins/GestorRecodeTypes.h>
#include <Sockets/Address.h>

class GestorRecode: public Gestor
{
	private:
		RPSession servei;	
		Address inaddr, outaddr;

		Gestor out;
	
		RPC *dbrpc;
		string inType, outType;
		int inTypeId, outTypeId;
	
		void findBestPluginsForSession(RPSession ses);

	public:
		GestorRecode(Address orig, string type, Canis *cn=NULL);
		GestorRecode(Address orig, int type, Canis *cn=NULL);
		GestorRecode(string file, Canis *cn=NULL);
		~GestorRecode();

		void startSession(RPSession idservei);
		void endSession(RPSession idservei);

		void processFrame(RPFrame p);
		RPFrame getFrame(void);
};

class GestorRecodeGetBestPluginThread : public Thread
{
	public:
		struct args
		{
			Canis *cn;
			string pluginType;
			short pluginTypeId;
		};

		GestorRecodeGetBestPluginThread();
		~GestorRecodeGetBestPluginThread();
		
		virtual void* execute(int id, void *args);
};

