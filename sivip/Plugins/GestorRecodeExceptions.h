#pragma once
#include <Utils/debugNew.h>
#include <Plugins/GestorException.h>

class RecodePluginException: public GestorException
{
	public:
		RecodePluginException(int code, string msg);
		RecodePluginException(SerializedException &se);
		virtual string getClass();
};

class RPInvalidParamException: public RecodePluginException
{
	public:
		RPInvalidParamException(int code, string msg);
		RPInvalidParamException(SerializedException &se);
		virtual string getClass();
};

class RPSessionAlreadyStablishedException: public RecodePluginException
{
	public:
		RPSessionAlreadyStablishedException(int code, string msg);
		RPSessionAlreadyStablishedException(SerializedException &se);
		virtual string getClass();
};

class RPSessionNotStablishedException: public RecodePluginException
{
	public:
		RPSessionNotStablishedException(int code, string msg);
		RPSessionNotStablishedException(SerializedException &se);
		virtual string getClass();
};

