#ifndef SIRIUS_BASE_PLUGIN_GESTOREXCEPTION_H_
#define SIRIUS_BASE_PLUGIN_GESTOREXCEPTION_H_

#include <Utils/debugNew.h>
#include <Utils/Exception.h>

class GestorException: public Exception
{
	public:
		GestorException(int code, string msg);
		GestorException(SerializedException &se);
		virtual string getClass();
};

#endif
