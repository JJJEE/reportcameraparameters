#pragma once
#include <Utils/debugNew.h>
#include <Endian/Endian.h>
#include <Utils/Types.h>
#include <string>
#include <list>
using namespace std;

#pragma mark *** IPDeviceID
class CPDeviceID
{
public:
	int id;
	
	CPDeviceID();
	CPDeviceID(void *buf);
	CPDeviceID(int id);
	~CPDeviceID();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

#pragma mark *** CPInput
class CPInput
{
public:
	int id, value;

	CPInput();
	CPInput(void *buf);
	CPInput(int id, int value);
	~CPInput();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

#pragma mark *** CPOutput
class CPOutput
{
public:
	int id, value, pulse;

	CPOutput();
	CPOutput(void *buf);
	CPOutput(int id, int value, int pulse=0);
	~CPOutput();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};


#pragma mark *** CPPTZ
class CPPTZ
{
public:
	float x, y, z;
	bool bx, by, bz;

	CPPTZ();
	CPPTZ(void *buf);
	CPPTZ(float x, float y, float z, bool bx=true, bool by=true, bool bz=true);
	~CPPTZ();

	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

#pragma mark *** CPMovement
typedef CPPTZ CPMovement;

#pragma mark *** CPCommandBufferSize
class CPCommandBufferSize
{
public:
	dword nCommands;

	CPCommandBufferSize();
	CPCommandBufferSize(void *buf);
	CPCommandBufferSize(dword nCommands);
	~CPCommandBufferSize();
	
	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

#pragma mark *** CPMetadata
struct CPMetadata
{
public:
	string name;
	string value;
	
	CPMetadata();
	CPMetadata(void *buf);
	CPMetadata(string name, string value);
	~CPMetadata();
	
	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

#pragma mark *** CPConfigParam
struct CPConfigParam
{
public:
	string path;
	string value;
	
	CPConfigParam();
	CPConfigParam(void *buf);
	CPConfigParam(string path, string value);
	~CPConfigParam();
	
	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

#pragma mark *** CPConfigParamSeq
class CPConfigParamSeq : public list<CPConfigParam>
{
public:
	CPConfigParamSeq();
	CPConfigParamSeq(void* buf);
	~CPConfigParamSeq();
	
	void toLocal(void* buf);
	void* toNetwork(void* buf=NULL);

	dword serializationSize();
};

class CPServ
{
public:
	static const int startSession=0; 
	static const int endSession=1; 
	static const int getPTZ=2; 
	static const int setPTZ=3; 
	static const int setInput=4; 
	static const int getInput=5; 
	static const int setOutput=6; 
	static const int getOutput=7; 
	static const int setCommandBufferSize=8; 
	static const int getCommandBufferSize=9; 
	static const int getCommandBufferPercentInUse=10; 
	static const int getCommandBufferCommandsInUse=11; 
	static const int getMetadataValue=12; 
	static const int setMetadataValue=13; 
	static const int getConfigParam=14; 
	static const int setConfigParam=15; 
	static const int getAllConfigParams=16;
	static const int move=17;
	static const int getConfigParamRecursive=18; 

	static const int CPNumServ = 19; 
};

