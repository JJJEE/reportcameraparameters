#ifndef __SIRIUS__BASE__PLUGINS__PLUGINMETADATA_H
#define __SIRIUS__BASE__PLUGINS__PLUGINMETADATA_H

#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Plugins/Plugin.h>
#include <Threads/Mutex.h>
#include <MetadataModule/MetadataModuleTypes.h>
#include <MetadataModule/MetadataModuleServices.h>

class PluginMetadata : public Plugin
{
protected:
	void init();

public:
	static RPCPacket* startSessionService(PluginMetadata *_this, Address *a, void *params);
	static RPCPacket* endSessionService(PluginMetadata *_this, Address *a, void *params);
	static RPCPacket* getAttributeService(PluginMetadata *_this, Address *a, void *params);
	static RPCPacket* setAttributeService(PluginMetadata *_this, Address *a, void *params);
	static RPCPacket* getObjectMetadataService(PluginMetadata *_this, Address *a, void *params);
	static RPCPacket* addFilterService(PluginMetadata *_this, Address *a, void *params);
	static RPCPacket* getFiltersService(PluginMetadata *_this, Address *a, void *params);
	static RPCPacket* delFilterService(PluginMetadata *_this, Address *a, void *params);


protected:
	static Mutex filterListToogleMutex;
	PluginMetadata(Address addr, string type);

public:
	PluginMetadata(Address addr);
	PluginMetadata(string file);

	virtual void startSession(dword id, Address *a) = 0;
	virtual void endSession(dword id, Address *a) = 0;
	virtual string getAttribute(string name, Address *a) = 0;
	virtual void setAttribute(string name, string value, Address *a) = 0;
	virtual MetadataModuleTypes::getObjectMetadataParams *getObjectMetadata(Address *a) = 0;
	virtual void addFilter(MetadataModuleFilter *f, Address *a) = 0;
	virtual MetadataModuleTypes::getFiltersParams* getFilters(Address *a) = 0;
	virtual void delFilter(MetadataModuleFilter *f, Address *a) = 0;
};

#endif
