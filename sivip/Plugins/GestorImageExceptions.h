#pragma once
#include <Utils/debugNew.h>
#include <Plugins/GestorException.h>

class ImagePluginException: public GestorException
{
	public:
		ImagePluginException(string msg);
		ImagePluginException(int code, string msg);
		ImagePluginException(SerializedException &se);
		virtual string getClass();
};

class IPInvalidParamException: public ImagePluginException
{
	public:
		IPInvalidParamException(string msg);
		IPInvalidParamException(int code, string msg);
		IPInvalidParamException(SerializedException &se);
		virtual string getClass();
};

class IPSessionAlreadyStablishedException: public ImagePluginException
{
	public:
		IPSessionAlreadyStablishedException(string msg);
		IPSessionAlreadyStablishedException(int code, string msg);
		IPSessionAlreadyStablishedException(SerializedException &se);
		virtual string getClass();
};

class IPSessionNotStablishedException: public ImagePluginException
{
	public:
		IPSessionNotStablishedException(string msg);
		IPSessionNotStablishedException(int code, string msg);
		IPSessionNotStablishedException(SerializedException &se);
		virtual string getClass();
};

