#ifndef __SIRIUS_PLUGINS_CONTROLPLUGINDEVICE_H
#define __SIRIUS_PLUGINS_CONTROLPLUGINDEVICE_H

#include <vector>
#include <map>
#include <Plugins/GestorControlTypes.h>
#include <Sockets/Address.h>
#include <XML/XML.h>

using namespace std;

class ControlPluginDevice
{
public:
	// Temps per defecte de cache de I/O
	static const int defaultCacheTime=500000;

	class IO
	{
	public:
		static const int DigitalInput=0;
		static const int AnalogInput=1;
		static const int TemperatureInput=2;
		static const int DigitalOutput=3;
		static const int AnalogOutput=4;
		static const int RelayOutput=5;

		static const int InvalidValue=-1;
		
	protected:
		int digitalValue;
		int analogValue;
		int temperatureValue;
		
	public:
		IO();
		IO(int d, int a, int t);
			
		int getValue(int type);
		void setValue(int type, int value);
		void setValueFromXML(XML *xml, string path, int type);
		bool hasValidValue(int type);
	};
	
protected:

	CPDeviceID id;
	Address address;
	
	int nInputs, nOutputs, firstRelay;
	vector<IO> inputs, outputs;

	int cacheTime;	// Temps en microsegons de cache d'I/O 
	
	map <string, string> metadataValues;
	
	bool inited;
	
	XML *modelConfig;
	
	ControlPluginDevice();
	ControlPluginDevice(CPDeviceID id, Address addr, string modelConfigFile);
	~ControlPluginDevice();

	virtual void init(CPDeviceID id, Address addr, string modelConfigFile) = 0;
	
	virtual int getInput(int input, int valueType) = 0;
	virtual void setInput(int input, int valueType, int value) = 0;

	virtual int getOutput(int output, int valueType) = 0;
	virtual void setOutput(int input, int valueType, int value) = 0;

	virtual void updateState() = 0;
};


#endif
