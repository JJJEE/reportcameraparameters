#include <Plugins/GestorControl.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Utils/debugStackTrace.h>
#include <Plugins/GetBestPluginThread.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

#define __PLUGIN_NAMESPACE "ControlPlugin"

GestorControl::GestorControl(Address orig, string type, Canis *cn): Gestor(orig, type, cn)
{
}

GestorControl::GestorControl(Address orig, int type, Canis *cn): Gestor(orig, type, cn)
{
}

GestorControl::GestorControl(string file, Canis *cn): Gestor(file, cn)
{
}

GestorControl::~GestorControl()
{

}

void GestorControl::startSession(CPDeviceID devId)
{
	dword idDev=devId.id;
	plgInfo=findBestPluginForDevice(idDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)devId.toNetwork();
	
	cout << "GestorControl::startSession ----> CallData("<< plgInfo->a.toString() << ")" << endl;
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, CPServ::startSession, params, devId.serializationSize(), plgTypeId);
	
	delete [] params;
	
	CallData cdpres=call(cdp);
		
	cout << "GestorControl::startSession <---- CallData()" << endl;
	sessionDev=idDev;
}

void GestorControl::endSession(CPDeviceID devId)
{
	dword idDev=devId.id;
	if (sessionDev==0xffffffff)
		throw(CPSessionNotStablishedException(0, "Sessió no establerta"));

	if (sessionDev!=idDev)
		throw(CPSessionNotStablishedException(0, "Sessió no establerta"));

	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(idDev, string(__PLUGIN_NAMESPACE));
	
	byte *params=(byte*)devId.toNetwork();
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::endSession, params, devId.serializationSize(), plgTypeId);
	CallData cdres=call(cd);

	sessionDev=0xffffffff;
}

CPPTZ GestorControl::getPTZ()
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getPTZ, NULL, 0, plgTypeId);
	CallData cdres=call(cd);

	CPPTZ res(cdres.data);
	
	return res;
}

void GestorControl::setPTZ(CPPTZ ptz)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)ptz.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::setPTZ, params, ptz.serializationSize(), plgTypeId);
	
	delete [] params;

	call(cd);
}

void GestorControl::setInput(CPInput in)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)in.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::setInput, params, in.serializationSize(), plgTypeId);
	
	delete [] params;
	
	call(cd);
}

CPInput GestorControl::getInput(CPInput in)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)in.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getInput, params, in.serializationSize(), plgTypeId);
	
	delete [] params;

	CallData cdres=call(cd);

	CPInput res(cdres.data);

	return res;
}

void GestorControl::setOutput(CPOutput out)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)out.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::setOutput, params, out.serializationSize(), plgTypeId);

	delete [] params;
	
	call(cd);
}

CPOutput GestorControl::getOutput(CPOutput out)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)out.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getOutput, params, out.serializationSize(),  plgInfo->typeId);
	
	delete [] params;
	
	CallData cdres=call(cd);
	CPOutput res(cdres.data);
	
	return res;
}

void GestorControl::setCommandBufferSize(CPCommandBufferSize size)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)size.toNetwork();

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::setCommandBufferSize, params, size.serializationSize(), plgTypeId);

	delete [] params;	
	
	call(cd);
}

CPCommandBufferSize GestorControl::getCommandBufferSize()
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getCommandBufferSize, NULL, 0, plgTypeId);
	CallData cdres=call(cd);
	CPCommandBufferSize res(cdres.data);

	return res;
}

float GestorControl::getCommandBufferPercentInUse()
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getCommandBufferPercentInUse, NULL, 0, plgTypeId);
	CallData cdres=call(cd);
	float res=*((float*)cdres.data);
	
	Endian::from(Endian::xarxa, &res, sizeof(float));

	return res;
}

int GestorControl::getCommandBufferCommandsInUse()
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getCommandBufferCommandsInUse, NULL, 0, plgTypeId);
	CallData cdres=call(cd);
	int res=*((int*)cd.data);

	Endian::from(Endian::xarxa, &res, sizeof(int));

	return res;
}

CPMetadata GestorControl::getMetadataValue(CPMetadata data)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)data.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getMetadataValue, params, data.serializationSize(), plgTypeId);

	delete [] params;
	
	CallData cdres=call(cd);
	
	CPMetadata res(cdres.data);

	return res;
}

void GestorControl::setMetadataValue(CPMetadata data)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)data.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::setMetadataValue, params, data.serializationSize(), plgTypeId);
	
	delete [] params;

	call(cd);
}

CPConfigParam GestorControl::getConfigParam(CPConfigParam p)
{
//	cout  << "\t--> GestorControl::getConfigParam(" << p.path << ")" << endl;
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

//	cout  << "\t\tGestorControl::getConfigParam(" << p.path << ") ck 1" << endl;

	byte *params=(byte*)p.toNetwork();

//	cout  << "\t\tGestorControl::getConfigParam(" << p.path << ") ck 2" << endl;
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getConfigParam, params, p.serializationSize(), plgTypeId);
//	cout  << "\t\tGestorControl::getConfigParam(" << p.path << ") ck 3" << endl;

	delete [] params;

//	cout  << "\t\tGestorControl::getConfigParam(" << p.path << ") ck 4" << endl;
	CallData cdres=call(cd);

//	cout  << "\t\tGestorControl::getConfigParam(" << p.path << ") ck 5" << endl;
	CPConfigParam res(cdres.data);

//	cout  << "\t<-- GestorControl::getConfigParam(" << p.path << ")" << endl;

	return res;
}

void GestorControl::setConfigParam(CPConfigParam p)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)p.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::setConfigParam, params, p.serializationSize(), plgTypeId);

	delete [] params;
	
	call(cd);
}

CPConfigParamSeq GestorControl::getAllConfigParams()
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getAllConfigParams, NULL, 0, plgTypeId);
	CallData cdres=call(cd);

	CPConfigParamSeq res(cdres.data);
	return res;
}


CPConfigParamSeq GestorControl::getConfigParamRecursive(CPConfigParam p)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)p.toNetwork();

	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::getConfigParamRecursive, params, p.serializationSize(), plgTypeId);

	delete [] params;

	CallData cdres=call(cd);

	CPConfigParamSeq res(cdres.data);
	return res;
}


void GestorControl::move(CPMovement move)
{
	if(plgInfo==NULL)
		throw CPSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));

	byte *params=(byte*)move.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cd(plgAddr, CPServ::move, params, move.serializationSize(), plgTypeId);
	call(cd);
}

