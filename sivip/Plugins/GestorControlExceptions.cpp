#include <Plugins/GestorControlExceptions.h>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

ControlPluginException::ControlPluginException(string msg): GestorException(0, msg)
{}

ControlPluginException::ControlPluginException(int code, string msg): GestorException(code, msg)
{}

ControlPluginException::ControlPluginException(SerializedException &se): GestorException(se)
{}

string ControlPluginException::getClass()
{
	string c=string("ControlPluginException");
	return c;
}


CPInvalidParamException::CPInvalidParamException(string msg):ControlPluginException(0,msg)
{}	

CPInvalidParamException::CPInvalidParamException(int code, string msg):ControlPluginException(code,msg)
{}	

CPInvalidParamException::CPInvalidParamException(SerializedException &se):ControlPluginException(se)
{}	

string CPInvalidParamException::getClass()
{
	string c=string("CPInvalidParamException");
	return c;
}


CPSessionAlreadyStablishedException::CPSessionAlreadyStablishedException(string msg):ControlPluginException(0,msg)
{}

CPSessionAlreadyStablishedException::CPSessionAlreadyStablishedException(int code, string msg):ControlPluginException(code,msg)
{}

CPSessionAlreadyStablishedException::CPSessionAlreadyStablishedException(SerializedException &se):ControlPluginException(se)
{}

string CPSessionAlreadyStablishedException::getClass()
{
	string c=string("CPSessionAlreadyStablishedException");
	return c;
}


CPSessionNotStablishedException::CPSessionNotStablishedException(string msg):ControlPluginException(0,msg)
{}

CPSessionNotStablishedException::CPSessionNotStablishedException(int code, string msg):ControlPluginException(code,msg)
{}

CPSessionNotStablishedException::CPSessionNotStablishedException(SerializedException &se):ControlPluginException(se)
{}

string CPSessionNotStablishedException::getClass()
{
	string c=string("CPSessionNotStablishedException");
	return c;
}

