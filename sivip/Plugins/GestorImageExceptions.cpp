#include <Plugins/GestorImageExceptions.h>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

ImagePluginException::ImagePluginException(string msg):GestorException(0, msg)
{}

ImagePluginException::ImagePluginException(int code, string msg):GestorException(code, msg)
{}

ImagePluginException::ImagePluginException(SerializedException &se):GestorException(se)
{}

string ImagePluginException::getClass()
{
	string c=string("ImagePluginException");
	return c;
}


IPInvalidParamException::IPInvalidParamException(string msg):ImagePluginException(0,msg)
{}	

IPInvalidParamException::IPInvalidParamException(int code, string msg):ImagePluginException(code,msg)
{}	

IPInvalidParamException::IPInvalidParamException(SerializedException &se):ImagePluginException(se)
{}	

string IPInvalidParamException::getClass()
{
	string c=string("IPInvalidParamException");
	return c;
}


IPSessionAlreadyStablishedException::IPSessionAlreadyStablishedException(string msg):ImagePluginException(0,msg)
{}

IPSessionAlreadyStablishedException::IPSessionAlreadyStablishedException(int code, string msg):ImagePluginException(code,msg)
{}

IPSessionAlreadyStablishedException::IPSessionAlreadyStablishedException(SerializedException &se):ImagePluginException(se)
{}

string IPSessionAlreadyStablishedException::getClass()
{
	return string("IPSessionAlreadyStablishedException");
}


IPSessionNotStablishedException::IPSessionNotStablishedException(string msg):ImagePluginException(0,msg)
{}

IPSessionNotStablishedException::IPSessionNotStablishedException(int code, string msg):ImagePluginException(code,msg)
{}

IPSessionNotStablishedException::IPSessionNotStablishedException(SerializedException &se):ImagePluginException(se)
{}

string IPSessionNotStablishedException::getClass()
{
	string c=string("IPSessionNotStablishedException");
	return c;
}

