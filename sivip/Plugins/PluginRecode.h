#pragma once

#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Plugins/Plugin.h>
#include <Plugins/GestorRecodeTypes.h>

class PluginRecode : public Plugin
{
public:
	static const word startSessionServiceId=0; 
	static const word endSessionServiceId=1; 
	static const word decodeFrameServiceId=2; 
	static const word encodeFrameServiceId=3; 
	static const word getFrameServiceId=4; 

	static const word serviceCount = 5; 

private:
	void init();

private:		
	static RPCPacket* startSessionService(PluginRecode *_this, Address *a, void *params);
	static RPCPacket* endSessionService(PluginRecode *_this, Address *a, void *params);
	static RPCPacket* decodeFrameService(PluginRecode *_this, Address *a, void *params);
	static RPCPacket* encodeFrameService(PluginRecode *_this, Address *a, void *params);
	static RPCPacket* getFrameService(PluginRecode *_this, Address *a, void *params);

public:
	PluginRecode(Address addr);
	PluginRecode(string file);

	virtual void startSession(RPCodecInfo id, Address *a) = 0;
	virtual void endSession(RPCodecInfo id, Address *a) = 0;

	virtual RPFrame *decodeFrame(RPFrame *f, Address *a) = 0;
	virtual void encodeFrame(RPFrame *f, Address *a) = 0;

	virtual RPFrame *getFrame(Address *a) = 0;
};

