#ifndef __SIRIUS__BASE__PLUGIN_H
#define __SIRIUS__BASE__PLUGIN_H

#include <util/Queue/Queue.h>
#include <WorkerThreads/WorkerThreadPool>
#include <Utils/Exception.h>
#include <Utils/FileException.h>
#include <XML/xmlNode.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/debugNew.h>
#include <Sockets/SocketUDP.h>
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
//#include <Utils/RPC.h>
//#include <Utils/Canis.h>
#include <Utils/DeviceAuthentication.h>
#include <WorkerThreads/WorkUnitQueue.h>
#include <WorkerThreads/WorkerThread.h>
#include <WorkerThreads/WorkerThreadPool.h>
#include <Module/ModulePriorityServices.h>

#include <string>

using namespace std;

class Plugin;
typedef RPCPacket* (*DefaultPluginCall)(Plugin*, Address*, void*);

class PluginServiceThread : public WorkerThread
{
	friend class Plugin;

	// TODO: Fer servir per evitar unable to create socket :P
	static SocketUDP *responseSocket;		// static?

public:
	class WorkUnitData
	{
	public:
		bool requestAlive;
		RPCPacket *p;
		Plugin *plugin;

		WorkUnitData() : p(NULL), plugin(NULL),
			requestAlive(true) {};

		WorkUnitData(RPCPacket *p, Plugin *plugin) :
			p(p), plugin(plugin),
			requestAlive(true) {};

		virtual ~WorkUnitData()
		{
			if (p!=NULL)
				delete p;
		};
	};

	PluginServiceThread();
	PluginServiceThread(WorkUnitQueue *wuQueue);
	
	virtual void* execute(int id, void* params);
};

class Plugin: public ModulePriorityServices
{
protected:
	Canis *cn;
	bool freeCanis;
	SocketUDP *serviceSocket;
	Address address;
	short type;

	int nWorkers;
	WorkerThreadPool<PluginServiceThread> *workers;

	static Mutex lastWULock;
	static map<string, WorkUnit*> lastWorkUnitReceived;

	DeviceAuthentication *devAuth;

	class serviceDef
	{
		public:
			dword inSize, outSize;	// de moment no es fan servir...
			DefaultPluginCall call;
	};

	dword numServices;
	serviceDef *services;

public:
	// 20090530: Deprecated cn=NULL...
	Plugin(Address addr, string type, Canis *cn=NULL);
	Plugin(Address addr, short type, Canis *cn=NULL);
	Plugin(string xmlFile);
	
	~Plugin();
	virtual void startPriorityService();
	virtual void stopPriorityService();
	
	virtual void serve();
	virtual void serveStartUp();
	virtual void serveShutDown();

	static WorkUnit *getLastWorkUnitForOrigin(string origin);
	static WorkUnit *safelyRemoveWorkUnitForOrigin(string origin,
		WorkUnit *removableWorkUnit);
	
	Address getAddress();
	short getType();
	
	Canis *getCanis();

	RPCPacket* service(RPCPacket *inPkt, Address *a);
	static RPCPacket* isAlive(Plugin *_this, Address *a, void *params);
	static RPCPacket* getVersion(Plugin *_this, Address *a, void *params);
};

#endif
