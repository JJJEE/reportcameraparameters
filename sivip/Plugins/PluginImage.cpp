#include <Plugins/PluginImage.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;


void PluginImage::init()
{
	STACKTRACE_INSTRUMENT();
	dword set=this->serviceSocket->setRecvBuffer(2*1024*1024);
	if (set!=2*1024*1024)
	{
		cout << "WARNING: " << __FILE__ << ", line " << __LINE__
		<< ": set send buffer to " << set << " bytes, "
		<< 2*1024*1024 << " requested." << endl;
	}

	set=this->serviceSocket->setSendBuffer(1024*1024);
	if (set!=1024*1024)
	{
		cout << "WARNING: " << __FILE__ << ", line " << __LINE__
		<< ": set send buffer to " << set << " bytes, "
		<< 1024*1024 << " requested." << endl;
	}

	this->numServices=PluginImage::serviceCount;
	this->services=new serviceDef[this->numServices];
	memset(this->services, 0, sizeof(serviceDef)*this->numServices);
	
	this->services[PluginImage::startSessionServiceId].call = (DefaultPluginCall)startSessionService;
	this->services[PluginImage::endSessionServiceId].call = (DefaultPluginCall)endSessionService;
	this->services[PluginImage::getDeviceInformationServiceId].call = (DefaultPluginCall)getDeviceInformationService;
	this->services[PluginImage::getCodecInUseServiceId].call = (DefaultPluginCall)getCodecInUseService;
	this->services[PluginImage::setCodecInUseServiceId].call = (DefaultPluginCall)setCodecInUseService;
	this->services[PluginImage::getFrameBufferSizeServiceId].call = (DefaultPluginCall)getFrameBufferSizeService;
	this->services[PluginImage::setFrameBufferSizeServiceId].call = (DefaultPluginCall)setFrameBufferSizeService;
	this->services[PluginImage::getFrameBufferPercentInUseServiceId].call = (DefaultPluginCall)getFrameBufferPercentInUseService;
	this->services[PluginImage::getFrameBufferFramesInUseServiceId].call = (DefaultPluginCall)getFrameBufferFramesInUseService;
	this->services[PluginImage::getStreamingModeServiceId].call = (DefaultPluginCall)getStreamingModeService;
	this->services[PluginImage::setStreamingModeServiceId].call = (DefaultPluginCall)setStreamingModeService;
	this->services[PluginImage::getCompressedNextFrameServiceId].call = (DefaultPluginCall)getCompressedNextFrameService;
	this->services[PluginImage::getDecompressedNextFrameServiceId].call = (DefaultPluginCall)getDecompressedNextFrameService;
	this->services[PluginImage::getFPSServiceId].call = (DefaultPluginCall)getFPSService;
	this->services[PluginImage::setFPSServiceId].call = (DefaultPluginCall)setFPSService;
}

RPCPacket* PluginImage::startSessionService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPDeviceID id(params);
	
	_this->startSession(id, a);
		
	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginImage::endSessionService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPDeviceID id(params);
	
	_this->endSession(id, a);
		
	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginImage::getDeviceInformationService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPDeviceInfo dev(params);

	IPDeviceInfo res=_this->getDeviceInformation(dev, a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


RPCPacket* PluginImage::getCodecInUseService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPCodecInfo res=_this->getCodecInUse(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


RPCPacket* PluginImage::setCodecInUseService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPCodecInfo codecInfo(params);	
	_this->setCodecInUse(codecInfo, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	return pk;
}


RPCPacket* PluginImage::getFrameBufferSizeService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPFrameBufferSize res=_this->getFrameBufferSize(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


RPCPacket* PluginImage::setFrameBufferSizeService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPFrameBufferSize size(params);
	_this->setFrameBufferSize(size, a);

	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	return pk;
}


RPCPacket* PluginImage::getFrameBufferPercentInUseService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	float res = _this->getFrameBufferPercentInUse(a);

	Endian::to(Endian::xarxa, &res, sizeof(float));

	byte *pkData=(byte*)&res;
	dword pkDataSize=sizeof(float);
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	return pk;
}

RPCPacket* PluginImage::getFrameBufferFramesInUseService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	int res = _this->getFrameBufferFramesInUse(a);

	Endian::to(Endian::xarxa, &res, sizeof(int));

	byte *pkData=(byte*)&res;
	dword pkDataSize=sizeof(int);
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	return pk;
}



RPCPacket* PluginImage::getStreamingModeService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPStreamingMode res = _this->getStreamingMode(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


RPCPacket* PluginImage::setStreamingModeService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPStreamingMode mode(params);

	_this->setStreamingMode(mode, a);

	byte *pkData=(byte*)NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginImage::getCompressedNextFrameService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPFrame res = _this->getCompressedNextFrame(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginImage::getDecompressedNextFrameService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPFrame res = _this->getDecompressedNextFrame(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginImage::getFPSService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPFramesPerSecond res=_this->getFPS(a);

	byte *pkData=(byte*)res.toNetwork();
	dword pkDataSize=res.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


RPCPacket* PluginImage::setFPSService(PluginImage *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	IPFramesPerSecond fps(params);

	_this->setFPS(fps, a);

	byte *pkData=(byte*)NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}


PluginImage::PluginImage(Address addr, string type):Plugin(addr, type)
{
	STACKTRACE_INSTRUMENT();
	init();
}

PluginImage::PluginImage(Address addr) : Plugin(addr, "ImagePlugin")
{
	STACKTRACE_INSTRUMENT();
	init();
}

PluginImage::PluginImage(string file):Plugin(file)
{
	STACKTRACE_INSTRUMENT();
	init();	
}

