#include <Plugins/GestorMetadata.h>
#include <XML/xmlParser.h>
#include <XML/XML.h>
#include <XML/xmlNode.h>
#include <Utils/StrUtils.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <MetadataModule/MetadataModuleServices.h>
#include <Exceptions/MetadataModuleSessionNotStablishedException.h>
#include <Utils/RPC.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;

#define __PLUGIN_NAMESPACE "MetadataPlugin"

#pragma mark *** Constructores
GestorMetadata::GestorMetadata(Address orig, string type, Canis *cn): Gestor(orig, type, cn)
{
	STACKTRACE_INSTRUMENT();
	
}

GestorMetadata::GestorMetadata(Address orig, int type, Canis *cn): Gestor(orig, type, cn)
{
	STACKTRACE_INSTRUMENT();
	
}

GestorMetadata::GestorMetadata(string file, Canis *cn): Gestor(file, cn)
{
	STACKTRACE_INSTRUMENT();
	
}

GestorMetadata::~GestorMetadata()
{
	STACKTRACE_INSTRUMENT();
	
}

#pragma mark *** Metodes
void GestorMetadata::startSession(dword devId)
{
	STACKTRACE_INSTRUMENT();
	
	plgInfo=findBestPluginForDevice(devId, string(__PLUGIN_NAMESPACE));
	
	MetadataModuleTypes::startSessionParams p(devId);
	byte *params=(byte*)p.toNetwork();
	
//	cout<<"GestorMetadata::startSession"<<endl;
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, MetadataModuleServices::startSessionServiceId, params, p.serializationSize(), plgTypeId);
//	cout<<"GestorMetadata::/startSession"<<endl;
	delete [] params;
	CallData cdret=call(cdp);
	sessionDev=devId;
}

void GestorMetadata::endSession(dword devId)
{
	STACKTRACE_INSTRUMENT();
	
	if (sessionDev==0xffffffff)
		throw(MetadataModuleSessionNotStablishedException(0, "Sessió no establerta"));

	if (sessionDev!=devId)
		throw(MetadataModuleSessionNotStablishedException(0, "Sessió no establerta"));
	
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(devId, string(__PLUGIN_NAMESPACE));
	
	MetadataModuleTypes::endSessionParams p(devId);
	byte *params=(byte*)p.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, MetadataModuleServices::endSessionServiceId, params, p.serializationSize(), plgTypeId);

	delete [] params;

	CallData cdret=call(cdp);

	sessionDev=0xffffffff;
}

string GestorMetadata::getAttribute(string name)
{
	STACKTRACE_INSTRUMENT();
	if(plgInfo==NULL)
		throw MetadataModuleSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));
	
	MetadataModuleTypes::getAttributeParams p;
	p.setName(name);
	byte *params=(byte*)p.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, MetadataModuleServices::getAttributeServiceId, params, p.serializationSize(), plgTypeId);
	
	delete [] params;
	
	CallData cdret=call(cdp);

	p.toLocal((byte*)cdret.data);

	return p.getValue();
}

void GestorMetadata::setAttribute(string name, string value)
{
	STACKTRACE_INSTRUMENT();
	if(plgInfo==NULL)
		throw MetadataModuleSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));
	
	MetadataModuleTypes::setAttributeParams p;
	p.setName(name);
	p.setValue(value);
	byte *params=(byte*)p.toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, MetadataModuleServices::setAttributeServiceId, params, p.serializationSize(), plgTypeId);
	
	delete [] params;
	
	CallData cdret=call(cdp);
}

MetadataModuleTypes::getObjectMetadataParams* GestorMetadata::getObjectMetadata()
{
//	cout << "--> GestorMetadata::getObjectMetadata()" << endl;
	STACKTRACE_INSTRUMENT();
	if(plgInfo==NULL)
		throw MetadataModuleSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));
//	cout << "--> GestorMetadata::getObjectMetadata() STCK_INST()" << endl;
	
	MetadataModuleTypes::getObjectMetadataParams p;
	byte *params=(byte*)p.toNetwork();

//	cout << "GM: p: size: " << p.size() << ", nObj: " << p.nObj << endl;
	
//	cout<<"callData:"<<(void*)plgInfo<<" "<<(void*)params<<endl;
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, MetadataModuleServices::getObjectMetadataServiceId, params, p.serializationSize(), plgTypeId);
//	cout<<"call"<<endl;

	delete [] params;

	CallData cdret=call(cdp);

//	cout << "GestorMetadata getObjectMetadata ret..." << endl;
//
//	cout << "GM: size: " << cdret.dsize << endl;
//	cout << StrUtils::hexDump(string((char*)cdret.data, cdret.dsize));

	MetadataModuleTypes::getObjectMetadataParams *res=new MetadataModuleTypes::getObjectMetadataParams(cdret.data);

//	cout << "GestorMetadata getObjectMetadata res..." << endl;
	
	return res;
}

void GestorMetadata::addFilter(MetadataModuleFilter *f)
{
	STACKTRACE_INSTRUMENT();
	if(plgInfo==NULL)
		throw MetadataModuleSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));
	
	byte *params=(byte*)f->toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, MetadataModuleServices::addFilterServiceId, params, f->size(), plgTypeId);
	
	delete [] params;
	
	CallData cdret=call(cdp);
}

MetadataModuleTypes::getFiltersParams* GestorMetadata::getFilters()
{
	STACKTRACE_INSTRUMENT();
	if(plgInfo==NULL)
		throw MetadataModuleSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, MetadataModuleServices::getFiltersServiceId, NULL, 0, plgTypeId);

	CallData cdret=call(cdp);
	
	MetadataModuleTypes::getFiltersParams *res=new MetadataModuleTypes::getFiltersParams(cdret.data);

	return res;
}

void GestorMetadata::delFilter(MetadataModuleFilter *f)
{
	STACKTRACE_INSTRUMENT();
	if(plgInfo==NULL)
		throw MetadataModuleSessionNotStablishedException(string("Session not stablished with any device"));
	// Obtenim l'actual (pq plgInfo no es NULL)
	plgInfo=findBestPluginForDevice(sessionDev, string(__PLUGIN_NAMESPACE));
	
	byte *params=(byte*)f->toNetwork();
	
	this->plgInfoLock.rlock();
	Address plgAddr=plgInfo->a;
	word plgTypeId=plgInfo->typeId;
	this->plgInfoLock.unlock();

	CallData cdp(plgAddr, MetadataModuleServices::delFilterServiceId, params, f->size(), plgTypeId);

	delete [] params;
	
	CallData cdret=call(cdp);
}

