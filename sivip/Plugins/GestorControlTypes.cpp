#include <Plugins/GestorControlTypes.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Utils/StrUtils.h>
#include <Exceptions/InvalidParameterException.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;
#pragma mark *** Globals
void GestorControlInvalidParameterByLength(string file, int line, dword len, string extraInfo)
{
	dword endianedLen=len;
	Endian::from(Endian::xarxa, &endianedLen, sizeof(dword));
	string excMsg=file + string(", line ") +
		StrUtils::decToString(line) + string(" invalid length: ") +
		StrUtils::decToString(len) + string(" (endianed: ") +
		StrUtils::decToString(endianedLen) + string(")") + 
		(extraInfo.length()>0?string(" - ")+extraInfo:string(""));

	cout << excMsg << endl;
	throw InvalidParameterException(excMsg);
}

#pragma mark *** CPDeviceID
CPDeviceID::CPDeviceID() : id(-1)
{
}

CPDeviceID::CPDeviceID(void *buf)
{
	this->toLocal(buf);
}

CPDeviceID::CPDeviceID(int id) : id(id)
{
}

CPDeviceID::~CPDeviceID()
{
}

void CPDeviceID::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->id=*(int*)b;
	Endian::from(Endian::xarxa, &this->id, sizeof(int));
	b+=sizeof(int);
}

void* CPDeviceID::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" CPDeviceID serialization buffer");
	}
	
	byte *b=bbuf;

	*((int*)b)=this->id;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	return bbuf;
}

dword CPDeviceID::serializationSize()
{
	return sizeof(int);
}

#pragma mark *** CPInput
CPInput::CPInput() : id(0), value(0)
{
}

CPInput::CPInput(void *buf)
{
	this->toLocal(buf);
}

CPInput::CPInput(int id, int value): id(id), value(value)
{
}

CPInput::~CPInput()
{
}

void CPInput::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->id=*(int*)b;
	Endian::from(Endian::xarxa, &this->id, sizeof(int));
	b+=sizeof(int);
	
	this->value=*(int*)b;
	Endian::from(Endian::xarxa, &this->value, sizeof(int));
	b+=sizeof(int);
}

void* CPInput::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" CPInput serialization buffer");
	}
	
	byte *b=bbuf;

	*((int*)b)=this->id;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	*((int*)b)=this->value;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	return bbuf;
}

dword CPInput::serializationSize()
{
	return sizeof(int)*2;
}


#pragma mark *** CPOutput
CPOutput::CPOutput() : id(0), value(0), pulse(0)
{
}

CPOutput::CPOutput(void *buf)
{
	this->toLocal(buf);
}

CPOutput::CPOutput(int id, int value, int pulse) : id(id), value(value),
	pulse(pulse)
{
}

CPOutput::~CPOutput()
{
}

void CPOutput::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->id=*(int*)b;
	Endian::from(Endian::xarxa, &this->id, sizeof(int));
	b+=sizeof(int);
	
	this->value=*(int*)b;
	Endian::from(Endian::xarxa, &this->value, sizeof(int));
	b+=sizeof(int);
	
	this->pulse=*(int*)b;
	Endian::from(Endian::xarxa, &this->pulse, sizeof(int));
	b+=sizeof(int);
}

void* CPOutput::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" CPOutput serialization buffer");
	}
	
	byte *b=bbuf;

	*((int*)b)=this->id;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	*((int*)b)=this->value;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	*((int*)b)=this->pulse;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	return bbuf;
}

dword CPOutput::serializationSize()
{
	return sizeof(int)*3;
}


#pragma mark *** CPPTZ
CPPTZ::CPPTZ() : x(0.0f), y(0.0f), z(1.0f), bx(true), by(true), bz(true)
{
}

CPPTZ::CPPTZ(void *buf)
{
	this->toLocal(buf);
}

CPPTZ::CPPTZ(float x, float y, float z, bool bx, bool by, bool bz) :
	x(x), y(y), z(z), bx(bx), by(by), bz(bz)
{
}

CPPTZ::~CPPTZ()
{
}

void CPPTZ::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->x=*(float*)b;
	Endian::from(Endian::xarxa, &this->x, sizeof(float));
	b+=sizeof(float);
	
	this->y=*(float*)b;
	Endian::from(Endian::xarxa, &this->y, sizeof(float));
	b+=sizeof(float);
	
	this->z=*(float*)b;
	Endian::from(Endian::xarxa, &this->z, sizeof(float));
	b+=sizeof(float);

	this->bx=*(bool*)b;
	b+=sizeof(bool);
	
	this->by=*(bool*)b;
	b+=sizeof(bool);
	
	this->bz=*(bool*)b;
	b+=sizeof(bool);
}

void* CPPTZ::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" CPPTZ serialization buffer");
	}
	
	byte *b=bbuf;

	*((float*)b)=this->x;
	Endian::to(Endian::xarxa, b, sizeof(float));
	b+=sizeof(float);
	
	*((float*)b)=this->y;
	Endian::to(Endian::xarxa, b, sizeof(float));
	b+=sizeof(float);
	
	*((float*)b)=this->z;
	Endian::to(Endian::xarxa, b, sizeof(float));
	b+=sizeof(float);
	
	*((bool*)b)=this->bx;
	b+=sizeof(bool);
	
	*((bool*)b)=this->by;
	b+=sizeof(bool);
	
	*((bool*)b)=this->bz;
	b+=sizeof(bool);
	
	return bbuf;
}

dword CPPTZ::serializationSize()
{
	return sizeof(float)*3+sizeof(bool)*3;
}

#pragma mark *** CPCommandBufferSize
CPCommandBufferSize::CPCommandBufferSize() : nCommands(0)
{
}

CPCommandBufferSize::CPCommandBufferSize(void *buf)
{
	this->toLocal(buf);
}

CPCommandBufferSize::CPCommandBufferSize(dword nCommands): nCommands(nCommands)
{
}

CPCommandBufferSize::~CPCommandBufferSize()
{
}

void CPCommandBufferSize::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->nCommands=*(dword*)b;
	Endian::from(Endian::xarxa, &this->nCommands, sizeof(dword));
	b+=sizeof(dword);
}

void* CPCommandBufferSize::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" CPCommandBufferSize serialization buffer");
	}
	
	byte *b=bbuf;

	*((dword*)b)=this->nCommands;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	return bbuf;
}

dword CPCommandBufferSize::serializationSize()
{
	return sizeof(dword);
}


#pragma mark *** CPMetadata
CPMetadata::CPMetadata() : name(""), value("")
{
}

CPMetadata::CPMetadata(void *buf)
{
	this->toLocal(buf);
}

CPMetadata::CPMetadata(string name, string value): name(name), value(value)
{
}

CPMetadata::~CPMetadata()
{
}

void CPMetadata::toLocal(void* buf)
{
	this->name=string("");
	this->value=string("");

	byte *b=(byte*)buf;
	
	dword nameLen=*(dword*)b;
	Endian::from(Endian::xarxa, &nameLen, sizeof(dword));
	b+=sizeof(dword);

	if (nameLen>=16777216) // Llencem una excepcio
		GestorControlInvalidParameterByLength(__FILE__, __LINE__, nameLen, string((char*)b, 4));

	this->name=string((char*)b, nameLen);
	b+=nameLen;

	dword valueLen=*(dword*)b;
	Endian::from(Endian::xarxa, &valueLen, sizeof(dword));
	b+=sizeof(dword);

	if (valueLen>=16777216) // Llencem una excepcio
		GestorControlInvalidParameterByLength(__FILE__, __LINE__, valueLen, (nameLen<80?this->name:this->name.substr(0, 80)));
		

	this->value=string((char*)b, valueLen);
	b+=valueLen;
}

void* CPMetadata::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" CPMetadata serialization buffer");
	}
	
	byte *b=bbuf;

	dword nameLen=this->name.length();
	dword valueLen=this->value.length();
	
	*((dword*)b)=nameLen;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);

	memmove(b, this->name.c_str(), nameLen);
	b+=nameLen;
	
	*((dword*)b)=valueLen;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);

	memmove(b, this->value.c_str(), valueLen);
	b+=valueLen;
	
	return bbuf;
}

dword CPMetadata::serializationSize()
{
	return sizeof(dword)*2 + this->name.length() + this->value.length();
}

#pragma mark *** CPConfigParam
CPConfigParam::CPConfigParam() : path(""), value("")
{
}

CPConfigParam::CPConfigParam(void *buf)
{
	this->toLocal(buf);
}

CPConfigParam::CPConfigParam(string path, string value): path(path), value(value)
{
}

CPConfigParam::~CPConfigParam()
{
}

void CPConfigParam::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	dword pathLen=*(dword*)b;
	Endian::from(Endian::xarxa, &pathLen, sizeof(dword));
	b+=sizeof(dword);

	if (pathLen>=16777216) // Llencem una excepcio
		GestorControlInvalidParameterByLength(__FILE__, __LINE__, pathLen, string((char*)b, 4) + StrUtils::hexDump(string((char*)buf, 64)));

	this->path=string((char*)b, pathLen);
	b+=pathLen;

	dword valueLen=*(dword*)b;
	Endian::from(Endian::xarxa, &valueLen, sizeof(dword));
	b+=sizeof(dword);

	if (valueLen>=16777216) // Llencem una excepcio
		GestorControlInvalidParameterByLength(__FILE__, __LINE__, valueLen, (pathLen<80?this->path:this->path.substr(0, 80)));

	this->value=string((char*)b, valueLen);
	b+=valueLen;
}

void* CPConfigParam::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" CPConfigParam serialization buffer");
	}
	
	byte *b=bbuf;

	dword pathLen=this->path.length();
	dword valueLen=this->value.length();
	
	*((dword*)b)=pathLen;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);

	memmove(b, this->path.c_str(), pathLen);
	b+=pathLen;
	
	*((dword*)b)=valueLen;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);

	memmove(b, this->value.c_str(), valueLen);
	b+=valueLen;
	
	return bbuf;
}

dword CPConfigParam::serializationSize()
{
	return sizeof(dword)*2 + this->path.length() + this->value.length();
}

#pragma mark *** CPConfigParamSeq
CPConfigParamSeq::CPConfigParamSeq()
{
}

CPConfigParamSeq::CPConfigParamSeq(void *buf)
{
	this->toLocal(buf);
}

CPConfigParamSeq::~CPConfigParamSeq()
{
}

void CPConfigParamSeq::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	dword nItems=*(dword*)b;
	Endian::from(Endian::xarxa, &nItems, sizeof(dword));
	b+=sizeof(dword);

	if (nItems>=16777216) // Llencem una excepcio
		GestorControlInvalidParameterByLength(__FILE__, __LINE__, nItems, string(""));

	for(dword i=0; i<nItems; i++)
	{
		CPConfigParam item(b);
		push_back(item);

		b+=item.serializationSize();
	}
}

void* CPConfigParamSeq::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" CPConfigParamSeq serialization buffer");
	}
		
	byte *b=bbuf;

	dword nItems=this->size();

	*((dword*)b)=nItems;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);

	list<CPConfigParam>::iterator lIt;
	for(lIt=begin(); lIt!=end(); lIt++)
	{
		(*lIt).toNetwork(b);
		b += (*lIt).serializationSize();
	}
	return bbuf;
}

dword CPConfigParamSeq::serializationSize()
{
	dword s=sizeof(dword);

	list<CPConfigParam>::iterator lIt;
	for(lIt=begin(); lIt!=end(); lIt++)
		s+=(*lIt).serializationSize();

	return s;
}

