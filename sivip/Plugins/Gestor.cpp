#include <Plugins/Gestor.h>
#include <Plugins/GestorException.h>
#include <Plugins/GestorImageExceptions.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/Types.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>
#include <ServiceFinder/ServiceFinder.h>
#include <XML/XML.h>
#include <iostream>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
#include <Utils/DBGateway.h>
#include <Plugins/GetBestPluginThread.h>
#include <Exceptions/IdNotFoundException.h>

#include <WorkerThreads/WorkerThreadPool.h>
#include <WorkerThreads/WorkerThreadPool>
#include <Module/Module.h>
#include <Module/CancelledCallException.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;

#pragma mark *** CallData (ui ui ui)

CallData::CallData(Address dest, word id, void* data, dword dsize, short destType):
	dest(dest), id(id), dsize(dsize), destType(destType)
{
	STACKTRACE_INSTRUMENT();
	
	if(dsize==0)
		this->data=new byte[1];
	else
		this->data=new byte[dsize];
	if (this->data==NULL)
	{
		this->data=data;
		throw(GestorException(0, "Data referenced, not enough memory to allocate for new data"));
	}
//	cout<<"CallData:: memmove 1"<<endl;
	memmove(this->data, data, dsize);
	
	destName=string("");
}

CallData::CallData(Address dest, word id, void* data, dword dsize, string destName):
	dest(dest), id(id), dsize(dsize), destName(destName)
{
	STACKTRACE_INSTRUMENT();
	
	if(dsize==0)
		this->data=new byte[1];
	else
	this->data=new byte[dsize];
	if (this->data==NULL)
	{
		this->data=data;
		throw(GestorException(0, "Data referenced, not enough memory to allocate for new data"));
	}
	memmove(this->data, data, dsize);
	
	destType=0;
}
CallData::CallData(word id, void* data, dword dsize, short destType):
id(id), dsize(dsize), destType(destType)
{
	STACKTRACE_INSTRUMENT();
	
	if(dsize==0)
		this->data=new byte[1];
	else
		this->data=new byte[dsize];
	if (this->data==NULL)
	{
		this->data=data;
		throw(GestorException(0, "Data referenced, not enough memory to allocate for new data"));
	}
	memmove(this->data, data, dsize);
	
	Address tmpDst(IP("0.0.0.0"),0);
	dest=tmpDst;
	destName=string("");
}

CallData::CallData(word id, void* data, dword dsize, string destName):
		id(id), dsize(dsize), destName(destName)
{
	STACKTRACE_INSTRUMENT();
	
	if(dsize==0)
		this->data=new byte[1];
	else
		this->data=new byte[dsize];
	if (this->data==NULL)
	{
		this->data=data;
		throw(GestorException(0, "Data referenced, not enough memory to allocate for new data"));
	}
	memmove(this->data, data, dsize);
	
	Address tmpDst(IP("0.0.0.0"),0);
	dest=tmpDst;
	destType=0;
}

CallData::CallData(const CallData &cd):
		id(cd.id), dsize(cd.dsize), dest(cd.dest), destType(cd.destType), destName(cd.destName), data(NULL)
{
	STACKTRACE_INSTRUMENT();

//	if(data!=NULL)
//		delete[] (byte*)data;

	if(dsize==0)
		this->data=new byte[1];
	else
		data=new byte[dsize];
	if (data==NULL)
	{
		throw(GestorException(0, "Not enough memory to allocate CallData's data"));
	}
	
	memmove(data, cd.data, dsize);
	

//	cout << "CallData cd" << endl << StrUtils::hexDump(string((char*)cd.data, cd.dsize)) << endl;
//	cout << "CallData copy" << endl << StrUtils::hexDump(string((char*)data, dsize)) << endl;
}

CallData CallData::operator=(const CallData &cd)
{
	STACKTRACE_INSTRUMENT();
	
//	cout<<" CallData:operator= "<<endl;
	if(data!=NULL)
		delete[] (byte*)data;
	id=cd.id;
	dsize=cd.dsize;
	dest=cd.dest;
	destType=cd.destType;
	destName=cd.destName;

	if(dsize==0)
		this->data=new byte[1];
	else
		data=new byte[dsize];
	if (data==NULL)
	{
		throw(GestorException(0, "Not enough memory to allocate CallData's data"));
	}
	
	memmove(data, cd.data, dsize);
	
//	cout<<" /CallData:operator= "<<endl;
	return *this;
}

CallData::CallData(): id(0), dsize(0), dest(), destType(-1), data(NULL)
{
	STACKTRACE_INSTRUMENT();
	
//	cout<<" CallData() "<<endl;
}

CallData::~CallData()
{
	STACKTRACE_INSTRUMENT();
	
//	cout<<" ~CallData() "<<endl;
	if (data!=NULL)
	{
		delete [] (byte*)data;
		data=NULL;
	}
//	cout<<" / ~CallData() "<<endl;
}

void CallData::setData(void *data, dword size)
{
	STACKTRACE_INSTRUMENT();
	
	if (dsize!=size)
	{
		void *oldData=this->data;
		dword oldSize=this->dsize;

		if(size==0)
			this->data=new byte[1];
		else
			this->data=new byte[size];
		this->dsize=size;

		if (this->data==NULL)
		{
			this->data=oldData;
			this->dsize=oldSize;
			
			throw(GestorException(0, "Left data unchanged, not enough memory to allocate for new data"));
		}

		if(oldData!=NULL)
			delete [] (byte*)oldData;
	}
	
	memmove(this->data, data, dsize);
}

#pragma mark *** Privades (pluginInfo, findBestPluginForDevice)

Gestor::pluginInfo::pluginInfo(Address a, string type, word typeId) : a(a), type(type), typeId(typeId)
{
	STACKTRACE_INSTRUMENT();
	
}

Gestor::pluginInfo::~pluginInfo()
{
	STACKTRACE_INSTRUMENT();
	
}


Gestor::pluginInfo* Gestor::findBestPluginForDevice(dword devId, string pluginTypeNameSpace)
{
	STACKTRACE_INSTRUMENT();
	
	this->plgInfoLock.rlock();
	if (!this->renewPlgInfo && this->plgInfo!=NULL && this->plgInfo->a.getIP().toDWord()!=0)
	{
		this->plgInfoLock.unlock();
//		cout << "Gestor::findBestPluginForDevice(): returning pre-known Plugin"
//			" Info: " << this->plgInfo->a.toString() << endl;
		return plgInfo;
	}
	this->plgInfoLock.unlock();

	if (devId==0 || devId==0xffffffff)
	{
		this->plgInfoLock.wlock();
		pluginInfo *pi=this->plgInfo;
		try
		{
			this->plgInfo=NULL;
			
			if (pi!=NULL)
				delete pi;
	
			this->plgInfoLock.unlock();
		}
		catch (...)
		{
//			this->plgInfoLock.unlock();
		}
		
		throw IdNotFoundException(string("Invalid device id: ") + 
			StrUtils::decToString(devId));
	}
		
	this->plgInfoLock.wlock();
	this->renewPlgInfo=false;
	this->plgInfoLock.unlock();

	Address plgAddr;
	string plgType;
	short plgTypeId;

	try
	{
		DBGateway *db=new DBGateway(this->orig, this->idType, this->cn);
		
		string q=string("SELECT f.nombre, m.codigo FROM dispositivo d, ")+
			string("modelo m, fabricante f WHERE d.id='")+
			StrUtils::decToString(devId)+string("' AND d.modelo=m.id ")+
			string("AND d.fabricante=m.fabricante AND m.fabricante=f.id;");
			
		XML *xml=db->query(q);
		
		delete db;
		
		xmlNode *fab=xml->getNode("/result/[0]/nombre");
		xmlNode *mod=xml->getNode("/result/[0]/codigo");

		if (fab==NULL || mod==NULL)
		{
			delete xml;
			throw (Exception(0, string("Invalid device id:")+StrUtils::decToString(devId)));
		}	

		string fabricant=fab->getCdata();
		string model=mod->getCdata();

		delete xml;

		GetBestPluginThread th;
		GetBestPluginThread::args th1a, th2a;
		
		// Busquem el plugin mes proper a nosaltres...
		th1a.cn=this->cn;
		th1a.pluginType=pluginTypeNameSpace+string("::")+fabricant+string("::")+model;
		th1a.devId=0xffffffff;
		th1a.moduleAddr=orig;
		th1a.moduleTypeId=idType;
		int th1id=th.start(&th1a, Thread::OnExceptionExitThread);
		
		th2a.cn=this->cn;
		th2a.pluginType=pluginTypeNameSpace+string("::")+fabricant;
		th2a.devId=0xffffffff;
		th2a.moduleAddr=orig;
		th2a.moduleTypeId=idType;
		int th2id=th.start(&th2a, Thread::OnExceptionExitThread);

		GetBestPluginThread::result *res1=(GetBestPluginThread::result*)th.join(th1id);
		GetBestPluginThread::result *res2=(GetBestPluginThread::result*)th.join(th2id);

		SerializedException *e1=NULL;
		RPCPacket *plugin1=NULL;
		SerializedException *e2=NULL;
		RPCPacket *plugin2=NULL;

		if(res1!=NULL)
		{
			e1=res1->e;
			plugin1=res1->plugin;
		}
		
		if(res2!=NULL)
		{
			e2=res2->e;
			plugin2=res2->plugin;
		}

		// 20090602: Lo seguent es una xapussa que ja no hauria de caler
//		if(e1!=NULL && e1->getClass()==string("SocketException") && e1->getCode()==2) // donem prioritat a l'"unable to create Socket" XD
//		{
//			cout<<" Gestor::findBestPluginForDevice - 1 - throw SExc:no sockets "<<endl;
//			
//			if (plugin1!=NULL) delete plugin1;
//			if (plugin2!=NULL) delete plugin2;
//			if (e2!=NULL) delete e2;
//			
//			e1->materializeAndThrow(true);
//		}
//		else if(e2!=NULL && e2->getClass()==string("SocketException") && e2->getCode()==2)
//		{
//			cout<<" GestorImage::findBestPluginForDevice - 2 - throw SExc:no sockets "<<endl;
//
//			if (plugin1!=NULL) delete plugin1;
//			if (plugin2!=NULL) delete plugin2;
//			if (e1!=NULL) delete e1;
//			
//			e2->materializeAndThrow(true);
//		}		

		RPCPacket *plugin=NULL;
		string pt;

		if (plugin1!=NULL)
		{
			plugin=plugin1;
			pt=th1a.pluginType;
		}
		else if (plugin2!=NULL)
		{
			plugin=plugin2;
			pt=th2a.pluginType;
		}

		if (plugin==NULL)
		{
			if (plugin1!=NULL) delete plugin1;
			if (plugin2!=NULL) delete plugin2;
			if (e1!=NULL) delete e1;
			if (e2!=NULL) delete e2;
			if (res1!=NULL) delete res1;
			if (res2!=NULL) delete res2;
			throw (Exception(0, string("Failed to find ") + 
				pluginTypeNameSpace + string("::") + 
					fabricant + string("( & ::") + model + string(")")));
		}

		plgAddr=*plugin->a;
		plgTypeId=plugin->origen;
		plgType=pt;

		if (plugin1!=NULL) delete plugin1;
		if (plugin2!=NULL) delete plugin2;
		if (e1!=NULL) delete e1;
		if (e2!=NULL) delete e2;
		if (res1!=NULL) delete res1;
		if (res2!=NULL) delete res2;

	}
	catch(Exception &e)
	{
 		cout<<"findBestPluginForDevice("+StrUtils::decToString(devId)+") exception :"<<e.getClass()<<":"<<e.getMsg()<<endl;
		this->plgInfoLock.wlock();
		pluginInfo *pi=this->plgInfo;
		try
		{
			this->plgInfo=NULL;
			
			if (pi!=NULL)
				delete pi;
	
			this->plgInfoLock.unlock();
		}
		catch (...)
		{
//			this->plgInfoLock.unlock();
		}

		//e.serialize()->materializeAndThrow(true);
		throw;
	}
		
	this->plgInfoLock.wlock();
	pluginInfo *pi=this->plgInfo;
	try
	{
//		cout << "Gestor::findBestPluginForDevice(): Found " << plgType << 
//			" at " << plgAddr.toString() << endl;
			
		this->plgInfo=new pluginInfo(plgAddr, plgType, plgTypeId);
		
		if (pi!=NULL)
			delete pi;

		this->plgInfoLock.unlock();
	}
	catch (...)
	{
		try
		{
			this->plgInfoLock.unlock();
		}
		catch (...)
		{}
	}
	
	return this->plgInfo;
}

#pragma mark *** Gestor

Gestor::Gestor(Address orig, string type, Canis *c):orig(orig), type(type), rpcCrida(NULL), rpcBuf(0), keepAlive(NULL), renewPlgInfo(true), plgInfo(NULL), sessionDev(0xffffffff)
{
	STACKTRACE_INSTRUMENT();
	
	if(c!=NULL)
	{
		cn=c;
		newCanis=false;
	}	
	else
	{
		cn=new Canis(orig, type, true);
		newCanis=true;
	}

	idType=cn->getType();
//	cn->setLoad(0.5);
}

Gestor::Gestor(Address orig, int type, Canis *c):orig(orig), rpcCrida(NULL), rpcBuf(0), keepAlive(NULL), renewPlgInfo(true), plgInfo(NULL), sessionDev(0xffffffff)
{
	STACKTRACE_INSTRUMENT();
	
	if(c!=NULL)
	{
		cn=c;
		newCanis=false;
	}	
	else
	{
		cn=new Canis(orig, type, true);
		newCanis=true;
	}

	
	idType=type;
//	cn->setLoad(0.5);
}

Gestor::Gestor(string file, Canis *c) : rpcCrida(NULL), rpcBuf(0), keepAlive(NULL), renewPlgInfo(true), plgInfo(NULL), sessionDev(0xffffffff)
{
	STACKTRACE_INSTRUMENT();
	
	if(c!=NULL)
	{
		cn=c;
		newCanis=false;
	}	
	else
	{
		cn=new Canis(file, true);
		newCanis=true;
	}


	orig=cn->getAddress();
	
	idType=cn->getType();
//	cn->setLoad(0.5);
}

Gestor::~Gestor()
{
	STACKTRACE_INSTRUMENT();
	
	if (rpcCrida!=NULL)
		delete rpcCrida;
	if (newCanis==true && cn!=NULL)
		delete cn;
		
	rpcCrida=NULL;
	cn=NULL;
}


CallData Gestor::call(CallData &cd)
{
	STACKTRACE_INSTRUMENT();

	bool doDebug = false;
	int *debug = WorkerThreadPool<ModuleServiceThread>::getMyDebugPtr();
	doDebug = (debug != NULL);

	if(doDebug)
		*debug = 1000;
	
//	Address dest=cd.dest;
	
	if(cd.dest.getIP().toDWord()==0)
	{ 
		// TODO: Arreglar el tipus de destí
		RPCPacket* cdres=NULL;
		if(cd.destName==string(""))
		{
			if(doDebug)
				*debug = 2000;
			cdres=ServiceFinder::getBestSubsys(cd.destType, cn);
		}
		else
		{
			if(doDebug)
				*debug = 3000;
			cdres=ServiceFinder::getBestSubsys(cd.destName, cn);
		}

		if(cdres!=NULL)
		{
			cd.destType=cdres->getOrType();
			cd.dest=*(cdres->a);
			delete cdres;
		}
	}

//	RPCPacket r(orig, cd.id, (byte*)cd.data, cd.dsize, idType, cd.destType, true);
	RPCPacket *callPacket = NULL;
	callPacket = new RPCPacket(orig, cd.id, (byte*)cd.data, cd.dsize, idType, cd.destType, true);
	
	if (callPacket==NULL)
		throw NotEnoughMemoryException("Cannot allocate memory for call packet");
	
	RPCPacket *res=NULL;

	int retryCounter=3;

retryCall:

	if(doDebug)
		*debug = 4000;
//	this->rpcLock.lock();
	if(!this->rpcLock.tryLock())
	{
		usleep(10000); // per donar temps a que arribi el signal i cancel·li l'anterior
		if(!this->rpcLock.tryLock())
		{
			if (callPacket!=NULL)
				delete callPacket;
			cout << __FILE__ << " line " << __LINE__ 
				<< "Gestor::call() Warning: could not lock rpc, cancelling" << endl;
			throw CancelledCallException("Gestor::call");
		}
	}

	try
	{
		if (rpcCrida==NULL)
		{
			if(doDebug)
				*debug = 5000;
			rpcCrida=new RPC(cd.dest);
			if (rpcCrida==NULL)
			{
				this->rpcLock.unlock();
				throw(GestorException(0, "Could not allocate RPC"));
			}
			
			if (rpcBuf > 1024*1024)
				rpcBuf=1024*1024;
			
			if (rpcBuf!=0 && rpcBuf <= 1024*1024) // Limitem a 1MB per sentit comu
			{
				dword set=rpcCrida->setRecvBuffer(rpcBuf);
				if (set!=rpcBuf)
				{
					cout << "WARNING: " << __FILE__ << ", line " << __LINE__
					<< ": set receive buffer to " << set << " bytes, "
					<< rpcBuf << " requested." << endl;
				}
				set=rpcCrida->setSendBuffer(rpcBuf);
				if (set!=rpcBuf)
				{
					cout << "WARNING: " << __FILE__ << ", line " << __LINE__
					<< ": set send buffer to " << set << " bytes, "
					<< rpcBuf << " requested." << endl;
				}
			}
		}

		// Apliquem l'adreça de destinacio cada cop per si ha canviat
		rpcCrida->setDefaultDestination(cd.dest);
		// TODO 20090621: En alguns casos dona error gestionant el tema del 
		// keepAlive, des d'RPC::call. Veure si esta ben gestionat
		if(doDebug)
			*debug = 6000;
		if(WorkerThreadPool<WorkerThread>::isCancelled())
		{
			//cout << __FILE__ << " line " << __LINE__ 
			//	<< "Gestor::call() cancelled request" << endl;
			this->rpcLock.unlock();
			throw CancelledCallException("Gestor::call");
		}
//		res=rpcCrida->call(r);
		if (callPacket!=NULL)
		{
			res=rpcCrida->call(*callPacket);
			delete callPacket;
			callPacket=NULL;
		}
		this->rpcLock.unlock();
	}
	catch (RPCException &e)
	{
		if(doDebug)
			*debug = 7000;

		if (callPacket!=NULL)
			delete callPacket;

		try
		{
			if (rpcCrida!=NULL)
				delete rpcCrida;
		}
		catch (...)
		{
		}

		rpcCrida=NULL;

		if (this->rpcLock.isHeldByCurrentThread())
			this->rpcLock.unlock();

		cout << "Capturada Excepcio: " << e.getClass() << ": " <<  e.getMsg() << ", retryCounter: " << retryCounter << endl;
			
		if (retryCounter-- > 0)
			goto retryCall;

		throw;
	}
	catch (...)
	{
		if (callPacket!=NULL)
			delete callPacket;

		if(doDebug)
			*debug = 8000;
		if (this->rpcLock.isHeldByCurrentThread())
			this->rpcLock.unlock();
		throw;
	}

	if(res->id==RPCPacket::exceptionPacketId)
	{
		if(doDebug)
			*debug = 9000;
		try {delete rpcCrida;} catch (...) {}
		rpcCrida=NULL;

		SerializedException se(res->getSize(), (byte*)res->getData(), false);	
		se.materializeAndThrow();
	}


	if(doDebug)
		*debug = 9500;
	CallData cdret(res->getCall(), res->getData(), res->getSize(), cd.destType);
	delete res;

	return cdret;
}

void Gestor::setKeepAliveAddress(Address *a)
{
	STACKTRACE_INSTRUMENT();
	
	keepAlive=a;
}

void Gestor::clearKeepAliveAddress()
{
	STACKTRACE_INSTRUMENT();
	
	setKeepAliveAddress();
}

void Gestor::clearPlgInfo()
{
	this->renewPlgInfo=true;
}

Address Gestor::getCurrentPluginAddress()
{
	if (plgInfo!=NULL)
		return this->plgInfo->a;
	return Address();
}

Address Gestor::getLocalAddr()
{
	if (this->rpcCrida!=NULL)
		return this->rpcCrida->getLocalAddr();
	return Address();
}

/* TODO: tornar a afegir sendCall/getRes. (-->threads) 1 de simultania
int Gestor::sendCall(CallData cd)
{
	STACKTRACE_INSTRUMENT();
	
	int idCall=start(&cd);
	
	return idCall;
}


CallData Gestor::getResult(int idCall)
{
	STACKTRACE_INSTRUMENT();
	
	CallData *res=(CallData*)join(idCall);
	cout<<"resp call"<<endl; cout.flush();
	if((short)res->id==-2)
	{
		SerializedException se(res->dsize, (byte*)res->data,false);	
		se.materializeAndThrow();
	}
	return *res;
}*/

