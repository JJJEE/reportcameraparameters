#pragma once
#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <Plugins/GestorImageExceptions.h>
#include <Plugins/GestorImageTypes.h>
#include <Sockets/Address.h>
#include <string>

using namespace std;

class GestorImage: public Gestor
{
	private:
//		IPDeviceID servei;	
//		
//		RPC *dbrpc;
//		string pluginType;
//	
//		void findBestPluginForDevice(IPDeviceID id);
		
	public:
//		Address a;
		GestorImage(Address orig, string type, Canis *cn=NULL);
		GestorImage(Address orig, int type, Canis *cn=NULL);
		GestorImage(string file, Canis *cn=NULL);
		virtual ~GestorImage();
		
		IPDeviceInfo getDeviceInformation(IPDeviceInfo dev);
		void startSession(IPDeviceID dev);
		void endSession(IPDeviceID dev);

		IPCodecInfo getCodecInUse();
		void setCodecInUse(IPCodecInfo codec);

		IPFrameBufferSize getFrameBufferSize();
		void setFrameBufferSize(IPFrameBufferSize size);
		float getFrameBufferPercentInUse();
		int getFrameBufferFramesInUse();

		IPStreamingMode getStreamingMode();
		void setStreamingMode(IPStreamingMode mode);

		IPFrame getCompressedNextFrame();
		IPFrame getDecompressedNextFrame();

		IPFramesPerSecond getFPS();
		void setFPS(IPFramesPerSecond fps);
};
