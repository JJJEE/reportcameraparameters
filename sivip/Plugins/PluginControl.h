#ifndef __SIRIUS__BASE__PLUGINCONTROL_H
#define __SIRIUS__BASE__PLUGINCONTROL_H

#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Plugins/Plugin.h>
#include <Plugins/GestorControlTypes.h>

class PluginControl: public Plugin
{
public:
	static const word startSessionServiceId = 0; 
	static const word endSessionServiceId = 1; 
	static const word getPTZServiceId = 2; 
	static const word setPTZServiceId = 3; 
	static const word setInputServiceId = 4; 
	static const word getInputServiceId = 5; 
	static const word setOutputServiceId = 6; 
	static const word getOutputServiceId = 7; 
	static const word setCommandBufferSizeServiceId = 8; 
	static const word getCommandBufferSizeServiceId = 9; 
	static const word getCommandBufferPercentInUseServiceId = 10; 
	static const word getCommandBufferCommandsInUseServiceId = 11; 
	static const word getMetadataValueServiceId = 12; 
	static const word setMetadataValueServiceId = 13; 
	static const word getConfigParamServiceId = 14; 
	static const word setConfigParamServiceId = 15; 
	static const word getAllConfigParamsServiceId = 16;
	static const word moveServiceId = 17;
	static const word getConfigParamRecursiveServiceId = 18; 
	
	static const word serviceCount =  19; 

private:
	void init();

private:		
	static RPCPacket* startSessionService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* endSessionService(PluginControl *_this, Address *a, void *params);

	static RPCPacket* getPTZService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* setPTZService(PluginControl *_this, Address *a, void *params);

	static RPCPacket* setInputService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* getInputService(PluginControl *_this, Address *a, void *params);

	static RPCPacket* setOutputService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* getOutputService(PluginControl *_this, Address *a, void *params);

	static RPCPacket* setCommandBufferSizeService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* getCommandBufferSizeService(PluginControl *_this, Address *a, void *params);

	static RPCPacket* getCommandBufferPercentInUseService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* getCommandBufferCommandsInUseService(PluginControl *_this, Address *a, void *params);
	
	static RPCPacket* getMetadataValueService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* setMetadataValueService(PluginControl *_this, Address *a, void *params);

	static RPCPacket* getConfigParamService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* getConfigParamRecursiveService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* setConfigParamService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* getAllConfigParamsService(PluginControl *_this, Address *a, void *params);
	static RPCPacket* moveService(PluginControl *_this, Address *a, void *params);

protected:
	PluginControl(Address addr, string type);

public:
	PluginControl(Address addr);
	PluginControl(string file);

	virtual void startSession(CPDeviceID id, Address *a) = 0;
	virtual void endSession(CPDeviceID id, Address *a) = 0;

	virtual CPPTZ getPTZ(Address *a) = 0;
	virtual void setPTZ(CPPTZ ptz, Address *a) = 0;

	virtual void setInput(CPInput in, Address *a) = 0;
	virtual CPInput getInput(CPInput in, Address *a) = 0;

	virtual void setOutput(CPOutput out, Address *a) = 0;
	virtual CPOutput getOutput(CPOutput out, Address *a) = 0;

	virtual void setCommandBufferSize(CPCommandBufferSize size, Address *a) = 0;
	virtual CPCommandBufferSize getCommandBufferSize(Address *a) = 0;

	virtual float getCommandBufferPercentInUse(Address *a) = 0;
	virtual int getCommandBufferCommandsInUse(Address *a) = 0;

	virtual CPMetadata getMetadataValue(CPMetadata data, Address *a) = 0;
	virtual void setMetadataValue(CPMetadata data, Address *a) = 0;

	virtual CPConfigParam getConfigParam(CPConfigParam p, Address *a) = 0;
	virtual CPConfigParamSeq getConfigParamRecursive(CPConfigParam p, Address *a) = 0;
	virtual void setConfigParam(CPConfigParam p, Address *a) = 0;
	virtual CPConfigParamSeq getAllConfigParams(Address *a) = 0;
	virtual void move(CPMovement move, Address *a) = 0;
};

#endif
