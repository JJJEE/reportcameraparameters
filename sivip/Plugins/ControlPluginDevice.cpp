#include <ControlPluginDevice.h>
#include <XML/XML.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

#pragma mark *** ControlPluginDevice::IO

ControlPluginDevice::IO::IO() : digitalValue(ControlPluginDevice::IO::InvalidValue),
	analogValue(ControlPluginDevice::IO::InvalidValue), 
	temperatureValue(ControlPluginDevice::IO::InvalidValue)
{
}

ControlPluginDevice::IO::IO(int d, int a, int t) : digitalValue(d),
	analogValue(a), temperatureValue(t)
{
}

int ControlPluginDevice::IO::getValue(int type)
{
	switch (type)
	{
		case ControlPluginDevice::IO::DigitalInput:
		case ControlPluginDevice::IO::DigitalOutput:
		case ControlPluginDevice::IO::RelayOutput:
			return this->digitalValue;
		
		case ControlPluginDevice::IO::AnalogInput:
		case ControlPluginDevice::IO::AnalogOutput:
			return this->analogValue;
		
		case ControlPluginDevice::IO::TemperatureInput:
			return this->temperatureValue;
	}
	
	return ControlPluginDevice::IO::InvalidValue;
}

void ControlPluginDevice::IO::setValue(int type, int value)
{
	switch (type)
	{
		case ControlPluginDevice::IO::DigitalInput:
		case ControlPluginDevice::IO::DigitalOutput:
		case ControlPluginDevice::IO::RelayOutput:
			this->digitalValue=value;
		
		case ControlPluginDevice::IO::AnalogInput:
		case ControlPluginDevice::IO::AnalogOutput:
			this->analogValue=value;
		
		case ControlPluginDevice::IO::TemperatureInput:
			this->temperatureValue=value;
	}
}

void ControlPluginDevice::IO::setValueFromXML(XML *xml, string path, int type)
{
	string cdata = xml->getNodeData(path);
	
	if (cdata.length() == 0)
		return;
		
	int value=(int)strtol(cdata.c_str(), NULL, 10);
	
	this->setValue(type, value);
}

bool ControlPluginDevice::IO::hasValidValue(int type)
{
	switch (type)
	{
		case ControlPluginDevice::IO::DigitalInput:
		case ControlPluginDevice::IO::DigitalOutput:
		case ControlPluginDevice::IO::RelayOutput:
			return (this->digitalValue != ControlPluginDevice::IO::InvalidValue);
		
		case ControlPluginDevice::IO::AnalogInput:
		case ControlPluginDevice::IO::AnalogOutput:
			return (this->analogValue != ControlPluginDevice::IO::InvalidValue);
		
		case ControlPluginDevice::IO::TemperatureInput:
			return (this->temperatureValue != ControlPluginDevice::IO::InvalidValue);
	}
	
	return false;
}

#pragma mark *** ControlPluginDevice

ControlPluginDevice::ControlPluginDevice() : id(-1),
	address(IP(string("0.0.0.0")), 0), nInputs(-1), nOutputs(-1),
	cacheTime(defaultCacheTime), inited(false), modelConfig(NULL)
{
}

ControlPluginDevice::ControlPluginDevice(CPDeviceID id, Address addr, 
	string modelConfigFile) : id(id), address(addr), nInputs(-1),
	nOutputs(-1), cacheTime(defaultCacheTime), inited(false), modelConfig(NULL)
{
}

ControlPluginDevice::~ControlPluginDevice()
{
	if (this->modelConfig!=NULL)
	{
		delete this->modelConfig;
		this->modelConfig=NULL;
	}
}
