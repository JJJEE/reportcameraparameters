#include <Plugins/PluginMetadata.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;

Mutex PluginMetadata::filterListToogleMutex;

void PluginMetadata::init()
{
	this->numServices=MetadataModuleServices::serviceCount;
	this->services=new serviceDef[this->numServices];
	memset(this->services, 0, sizeof(serviceDef)*this->numServices);
	
	this->services[MetadataModuleServices::startSessionServiceId].call = (DefaultPluginCall)startSessionService;
	this->services[MetadataModuleServices::endSessionServiceId].call = (DefaultPluginCall)endSessionService;
	this->services[MetadataModuleServices::getAttributeServiceId].call = (DefaultPluginCall)getAttributeService;
	this->services[MetadataModuleServices::setAttributeServiceId].call = (DefaultPluginCall)setAttributeService;
	this->services[MetadataModuleServices::getObjectMetadataServiceId].call = (DefaultPluginCall)getObjectMetadataService;
	this->services[MetadataModuleServices::addFilterServiceId].call = (DefaultPluginCall)addFilterService;
	this->services[MetadataModuleServices::getFiltersServiceId].call = (DefaultPluginCall)getFiltersService;
	this->services[MetadataModuleServices::delFilterServiceId].call = (DefaultPluginCall)delFilterService;

}

RPCPacket* PluginMetadata::startSessionService(PluginMetadata *_this, Address *a, void *params)
{
	MetadataModuleTypes::startSessionParams parm(params);
	
	_this->startSession(parm.devId, a);
	
	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginMetadata::endSessionService(PluginMetadata *_this, Address *a, void *params)
{
	MetadataModuleTypes::endSessionParams parm(params);
	
	_this->endSession(parm.devId, a);
	
	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginMetadata::getAttributeService(PluginMetadata *_this, Address *a, void *params)
{
	MetadataModuleTypes::getAttributeParams parm(params);
	
	string v=_this->getAttribute(parm.getName(), a);
	parm.setValue(v);

	byte *pkData=(byte*)parm.toNetwork();
	dword pkDataSize=parm.serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginMetadata::setAttributeService(PluginMetadata *_this, Address *a, void *params)
{
	MetadataModuleTypes::setAttributeParams parm(params);

	_this->setAttribute(parm.getName(), parm.getValue(), a);
	
	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginMetadata::getObjectMetadataService(PluginMetadata *_this, Address *a, void *params)
{
	// Teoricament no rebem params :P
	
	MetadataModuleTypes::getObjectMetadataParams *objs=_this->getObjectMetadata(a);

//	cout << "getObjectMetadataService: " << objs->nObj << " objects" << endl;
//
//	cout << "PM: nObjs: " << objs->nObj << " size: " << objs->size() << endl;

	byte *pkData=(byte*)objs->toNetwork();
	dword pkDataSize=objs->serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	delete objs;
		
	return pk;
}

RPCPacket* PluginMetadata::addFilterService(PluginMetadata *_this, Address *a, void *params)
{
	MetadataModuleFilter *f=MetadataModuleFilter::createFilter(params);

	// Rebem la peticio => la local rebuda (la de l'altre extrem) passa a remote
	f->toogleMode();

	if (f->getLocalAddress()==NULL)
		f->setAddress(f);

	// No podem tocar la llista sense estar totalment segurs que esta
	// destooglada
	PluginMetadata::filterListToogleMutex.lock();
	_this->addFilter((MetadataModuleFilter *)f, a);
	PluginMetadata::filterListToogleMutex.unlock();

	// NO tornem a remote, pq el q ens guardem ha de tenir en local l'adreça local!!!
//	// Tornem a remote
//	f->toogleMode();
	
	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}

RPCPacket* PluginMetadata::getFiltersService(PluginMetadata *_this, Address *a, void *params)
{
	// Teoricament no rebem params :P
	MetadataModuleTypes::getFiltersParams *fl=_this->getFilters(a);
	
	PluginMetadata::filterListToogleMutex.lock();

	// toogle de tots
	fl->toogleMode();
	
	void *ret=fl->toNetwork();
	
	// Destooglem (es fa sobre la llista i afecta tots els filtres...)
	fl->toogleMode();

	PluginMetadata::filterListToogleMutex.unlock();

	byte *pkData=(byte*)ret;
	dword pkDataSize=fl->serializationSize();
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);

	delete [] pkData;
	
	return pk;
}

RPCPacket* PluginMetadata::delFilterService(PluginMetadata *_this, Address *a, void *params)
{
	MetadataModuleFilter *f=MetadataModuleFilter::createFilter(params);

	// Rebem la peticio => la local rebuda (la de l'altre extrem) passa a remote
	f->toogleMode();

	if (f->getLocalAddress()==NULL)
		f->setAddress(f);

	// No podem tocar la llista sense estar totalment segurs que esta
	// destooglada
	PluginMetadata::filterListToogleMutex.lock();
	_this->delFilter((MetadataModuleFilter *)f, a);
	PluginMetadata::filterListToogleMutex.unlock();

	// NO tornem a remote, pq el q ens guardem ha de tenir en local l'adreça local!!!
//	// Tornem a remote
//	f->toogleMode();
		
	byte *pkData=NULL;
	dword pkDataSize=0;
	RPCPacket *pk=new RPCPacket(_this->address, RPCPacket::responsePacketId,
		pkData, pkDataSize, _this->type, 0, true);
	return pk;
}


PluginMetadata::PluginMetadata(Address addr, string type):Plugin(addr, type)
{
	init();
}

PluginMetadata::PluginMetadata(Address addr):Plugin(addr, "MetadataPlugin")
{
	init();
}

PluginMetadata::PluginMetadata(string file):Plugin(file)
{
	init();	
}

