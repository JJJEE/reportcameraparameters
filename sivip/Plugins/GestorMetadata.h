#ifndef __SIRIUS__BASE__PLUGINS__GESTORMETADATA_H
#define __SIRIUS__BASE__PLUGINS__GESTORMETADATA_H

#include <MetadataModule/MetadataModuleTypes.h>
#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <Sockets/Address.h>
#include <string>

using namespace std;

class GestorMetadata : public Gestor
{
protected:
//	struct pluginInfo
//	{
//		Address a;
//		string type;
//		short typeId;
//		
//		pluginInfo(Address a, string type, short typeId);
//		~pluginInfo();
//	};
//	
//	pluginInfo *findBestPluginForDevice(dword devId);
//
//	pluginInfo *plgInfo;
//	dword sessionDev;		// Device amb el q tenim establerta sessio

public:
	GestorMetadata(Address orig, string type, Canis *cn=NULL);
	GestorMetadata(Address orig, int type, Canis *cn=NULL);
	GestorMetadata(string file, Canis *cn=NULL);
	virtual ~GestorMetadata();
	
	void startSession(dword devId);
	void endSession(dword devId);
	string getAttribute(string name);
	void setAttribute(string name, string value);
	MetadataModuleTypes::getObjectMetadataParams* getObjectMetadata();
	void addFilter(MetadataModuleFilter *f);
	MetadataModuleTypes::getFiltersParams* getFilters();
	void delFilter(MetadataModuleFilter *f);
};

#endif
