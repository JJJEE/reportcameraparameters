#include <Plugins/GetBestPluginThread.h>
#include <ServiceFinder/ServiceFinder.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

// Thread de cerca de plugins
#pragma mark *** Thread de cerca de plugins
GetBestPluginThread::GetBestPluginThread()
{
}

GetBestPluginThread::~GetBestPluginThread()
{
}
	
void* GetBestPluginThread::execute(int id, void *args)
{
	GetBestPluginThread::args *a=(GetBestPluginThread::args *)args;
	GetBestPluginThread::result *r=new GetBestPluginThread::result;

	try
	{
		if (a->pluginType==string(""))
		{
			RPCPacket *p;
			if (a->devId!=0xffffffff)
				p=ServiceFinder::getNearestSubsysForDevice(a->pluginTypeId,
					a->devId, a->cn->getInterfaceMask(), a->cn);
			else
			{
//				cout << "GestBestPlugin: subsysIP: " << a->moduleAddr.toString() << endl;
				
				// Busquem el mes proper al modul en absolut. Si pot ser,
				// que estigui a la mateixa maquina (mascara 32)
				if (a->moduleAddr.getIP().toDWord()!=0)
					p=ServiceFinder::getNearestSubsys(a->pluginTypeId, a->moduleAddr.getIP(), 32, a->cn);
				else
					p=ServiceFinder::getBestSubsys(a->pluginTypeId, a->cn);
			}
				
			r->plugin=p;
			r->e=NULL;
			return r;
		}
		else
		{
			RPCPacket *p;
			if (a->devId!=0xffffffff)
				p=ServiceFinder::getNearestSubsysForDevice(a->pluginType,
					a->devId, a->cn->getInterfaceMask(), a->cn);
			else
			{
//				cout << "GestBestPlugin: subsysIP: " << a->moduleAddr.toString() << endl;
				
				// Busquem el mes proper al modul en absolut. Si pot ser,
				// que estigui a la mateixa maquina (mascara 32)
				if (a->moduleAddr.getIP().toDWord()!=0)
					p=ServiceFinder::getNearestSubsys(a->pluginType, a->moduleAddr.getIP(), 32, a->cn);
				else
					p=ServiceFinder::getBestSubsys(a->pluginType, a->cn);
			}
				
			a->pluginTypeId=p->origen;
			r->plugin=p;
			r->e=NULL;
			return r;
		}
	}
	catch (Exception &e)
	{
		cout << "GetBestPluginThread type " << a->pluginType << "(" << a->pluginTypeId << "), " << e.getClass() << ": " << e.getMsg() << endl;
		
		if(e.getMsg() != string("Could not find best subsystem either by means of Canis nor Central Directory") && e.getMsg().find("Unregistered type")==string::npos && e.getMsg().find("No Subsystem found")==string::npos)
		{
			cout << "\tGetBestPluginThread, not a SubSysFinder Exception, serialize  "<< endl;
			r->e=e.serialize();
		}
		else
		{
			r->e=NULL;
		}
		r->plugin=NULL;
		return r;
	}

	r->e=NULL;
	r->plugin=NULL;
	return r;
}
