#include <Plugins/GestorImageTypes.h>
#include <iostream>
#include <math.h>
#include <Utils/debugStackTrace.h>
#include <Exceptions/NotEnoughMemoryException.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;

#pragma mark *** IPCodecInfo
IPCodecInfo::IPCodecInfo() : fourcc(0), quality(0), bitrate(0)
{
}

IPCodecInfo::IPCodecInfo(void *buf)
{
	this->toLocal(buf);
}

IPCodecInfo::IPCodecInfo(dword fourcc, dword quality, dword bitrate) :
	fourcc(fourcc), quality(quality), bitrate(bitrate)
{
}

IPCodecInfo::~IPCodecInfo()
{
}

void IPCodecInfo::toLocal(void *buf)
{
	byte *b=(byte*)buf;
	
	this->fourcc=*(dword*)b;
	Endian::from(Endian::xarxa, &this->fourcc, sizeof(dword));
	b+=sizeof(dword);
	
	this->quality=*(dword*)b;
	Endian::from(Endian::xarxa, &this->quality, sizeof(dword));
	b+=sizeof(dword);
	
	this->bitrate=*(dword*)b;
	Endian::from(Endian::xarxa, &this->bitrate, sizeof(dword));
	b+=sizeof(dword);
}

void* IPCodecInfo::toNetwork(void *buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPCodecInfo serialization buffer");
	}
	
	byte *b=bbuf;

	*((dword*)b)=this->fourcc;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	*((dword*)b)=this->quality;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	*((dword*)b)=this->bitrate;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	return bbuf;
}

dword IPCodecInfo::serializationSize()
{
	return sizeof(dword)*3;
}

#pragma mark *** IPCodecInfoSeq
IPCodecInfoSeq::IPCodecInfoSeq()
{
}

IPCodecInfoSeq::IPCodecInfoSeq(void *buf)
{
	this->toLocal(buf);
}

IPCodecInfoSeq::~IPCodecInfoSeq()
{
}

void IPCodecInfoSeq::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	dword nItems=*(dword*)b;
	Endian::from(Endian::xarxa, &nItems, sizeof(dword));
	b+=sizeof(dword);

	for(dword i=0; i<nItems; i++)
	{
		IPCodecInfo item(b);
		push_back(item);

		b+=item.serializationSize();
	}
}

void* IPCodecInfoSeq::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPCodecInfoSeq serialization buffer");
	}
		
	byte *b=bbuf;

	dword nItems=this->size();

	*((dword*)b)=nItems;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);

	list<IPCodecInfo>::iterator lIt;
	for(lIt=begin(); lIt!=end(); lIt++)
	{
		(*lIt).toNetwork(b);
		b += (*lIt).serializationSize();
	}
	return bbuf;
}

dword IPCodecInfoSeq::serializationSize()
{
	dword s=sizeof(dword);

	list<IPCodecInfo>::iterator lIt;
	for(lIt=begin(); lIt!=end(); lIt++)
		s+=(*lIt).serializationSize();

	return s;
}

#pragma mark *** IPFrameInfo
IPFrameInfo::IPFrameInfo() : x(0), y(0), bpp(0)
{
}

IPFrameInfo::IPFrameInfo(void *buf)
{
	this->toLocal(buf);
}


IPFrameInfo::IPFrameInfo(dword x, dword y, dword bpp): x(x), y(y), bpp(bpp)
{
}

IPFrameInfo::~IPFrameInfo()
{
}

void IPFrameInfo::toLocal(void *buf)
{
	byte *b=(byte*)buf;
	
	this->x=*(dword*)b;
	Endian::from(Endian::xarxa, &this->x, sizeof(dword));
	b+=sizeof(dword);
	
	this->y=*(dword*)b;
	Endian::from(Endian::xarxa, &this->y, sizeof(dword));
	b+=sizeof(dword);
	
	this->bpp=*(dword*)b;
	Endian::from(Endian::xarxa, &this->bpp, sizeof(dword));
	b+=sizeof(dword);
}

void *IPFrameInfo::toNetwork(void *buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFrameInfo serialization buffer");
	}
	
	byte *b=bbuf;

	*((dword*)b)=this->x;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	*((dword*)b)=this->y;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	*((dword*)b)=this->bpp;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	return bbuf;
}

dword IPFrameInfo::serializationSize()
{
	return sizeof(dword)*3;
}

#pragma mark *** IPFrameInfoSeq
IPFrameInfoSeq::IPFrameInfoSeq()
{
}

IPFrameInfoSeq::IPFrameInfoSeq(void *buf)
{
	this->toLocal(buf);
}

IPFrameInfoSeq::~IPFrameInfoSeq()
{
}

void IPFrameInfoSeq::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	dword nItems=*(dword*)b;
	Endian::from(Endian::xarxa, &nItems, sizeof(dword));
	b+=sizeof(dword);

	for(dword i=0; i<nItems; i++)
	{
		IPFrameInfo item(b);
		push_back(item);

		b+=item.serializationSize();
	}
}

void* IPFrameInfoSeq::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFrameInfoSeq serialization buffer");
	}
		
	byte *b=bbuf;

	dword nItems=this->size();

	*((dword*)b)=nItems;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);

	list<IPFrameInfo>::iterator lIt;
	for(lIt=begin(); lIt!=end(); lIt++)
	{
		(*lIt).toNetwork(b);
		b += (*lIt).serializationSize();
	}
	return bbuf;
}

dword IPFrameInfoSeq::serializationSize()
{
	dword s=sizeof(dword);

	list<IPFrameInfo>::iterator lIt;
	for(lIt=begin(); lIt!=end(); lIt++)
		s+=(*lIt).serializationSize();

	return s;
}


#pragma mark *** IPDeviceID
IPDeviceID::IPDeviceID() : id(-1)
{
}

IPDeviceID::IPDeviceID(void *buf)
{
	this->toLocal(buf);
}

IPDeviceID::IPDeviceID(int id) : id(id)
{
}

IPDeviceID::~IPDeviceID()
{
}

void IPDeviceID::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->id=*(int*)b;
	Endian::from(Endian::xarxa, &this->id, sizeof(int));
	b+=sizeof(int);
}

void* IPDeviceID::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPDeviceID serialization buffer");
	}
	
	byte *b=bbuf;

	*((int*)b)=this->id;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	return bbuf;
}

dword IPDeviceID::serializationSize()
{
	return sizeof(int);
}

bool IPDeviceID::operator < (const IPDeviceID id) const
{
	return (this->id < id.id);
}

// Sense scope??
ostream& operator << (ostream& stream, IPDeviceID id)
{
	stream << id.id;
	return stream;
}

#pragma mark *** IPDeviceInfo
IPDeviceInfo::IPDeviceInfo() : id(-1), make(""), model(""), res(), codecs()
{
}

IPDeviceInfo::IPDeviceInfo(void *buf)
{
	this->toLocal(buf);
}

IPDeviceInfo::IPDeviceInfo (int id, string make, string model,
	IPFrameInfoSeq res, IPCodecInfoSeq codecs) :
	id(id), make(make), model(model), res(res), codecs(codecs)
{
}

IPDeviceInfo::IPDeviceInfo(IPDeviceID id, string make, string model,
	IPFrameInfoSeq res, IPCodecInfoSeq codecs) :
	id(id), make(make), model(model), res(res), codecs(codecs)
{
}

IPDeviceInfo::~IPDeviceInfo()
{
}

void IPDeviceInfo::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->id.toLocal(b);
	b+=this->id.serializationSize();
	
	int makeLen=*(int*)b;
	Endian::from(Endian::xarxa, &makeLen, sizeof(int));
	b+=sizeof(int);	

	this->make=string((char*)b, makeLen);
	b+=makeLen;
	
	int modelLen=*(int*)b;
	Endian::from(Endian::xarxa, &modelLen, sizeof(int));
	b+=sizeof(int);	

	this->model=string((char*)b, modelLen);
	b+=modelLen;
	
	this->res.toLocal(b);
	b+=this->res.serializationSize();
	
	this->codecs.toLocal(b);
	b+=this->codecs.serializationSize();
}

void* IPDeviceInfo::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPDeviceInfo serialization buffer");
	}
	
	byte *b=bbuf;

	this->id.toNetwork(b);
	b += id.serializationSize();
		
	*((int*)b)=this->make.length();
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);
	
	memmove(b, this->make.c_str(), this->make.length());
	b+=this->make.length();
		
	*((int*)b)=this->model.length();
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	memmove(b, this->model.c_str(), this->model.length());
	b+=this->model.length();

	this->res.toNetwork(b);
	b+=this->res.serializationSize();
	
	this->codecs.toNetwork(b);
	b+=this->codecs.serializationSize();
	
	return bbuf;
}

dword IPDeviceInfo::serializationSize()
{
	return id.serializationSize() + sizeof(int) + this->make.length() +
		sizeof(int) + this->model.length() + this->res.serializationSize() +
		this->codecs.serializationSize();
}

#pragma mark *** IPFrameBufferSize
IPFrameBufferSize::IPFrameBufferSize()
{
}

IPFrameBufferSize::IPFrameBufferSize(void *buf)
{
	this->toLocal(buf);
}

IPFrameBufferSize::IPFrameBufferSize(dword nFrames): nFrames(nFrames)
{
}

IPFrameBufferSize::~IPFrameBufferSize()
{
}

void IPFrameBufferSize::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->nFrames=*(dword*)b;
	Endian::from(Endian::xarxa, &this->nFrames, sizeof(dword));
	b+=sizeof(dword);
}

void* IPFrameBufferSize::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFrameBufferSize serialization buffer");
	}
	
	byte *b=bbuf;

	*((dword*)b)=this->nFrames;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	return bbuf;
}

dword IPFrameBufferSize::serializationSize()
{
	return sizeof(dword);
}


#pragma mark *** IPFrame
IPFrame::IPFrame() : frameInfo(), codecInfo(), isKey(false), frameLength(0),
	frame(NULL)
{
}

IPFrame::IPFrame(void *buf) : frameLength(0), frame(NULL)
{
	this->toLocal(buf);
}

IPFrame::IPFrame(IPFrameInfo frameInfo, IPCodecInfo codecInfo, bool isKey,
	dword frameLength, void *frame) : frameInfo(frameInfo),
	codecInfo(codecInfo), isKey(isKey), frameLength(frameLength), frame(frame)
{
}

IPFrame::IPFrame(const IPFrame& f)
{
	this->frameInfo=f.frameInfo;
	this->codecInfo=f.codecInfo;
	this->isKey=f.isKey;
	this->frameLength=f.frameLength;
	this->frame=NULL;

	if(f.frame!=NULL)
	{
		this->frame = new byte[this->frameLength];
		if (this->frame==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFrame frame buffer");
		memmove(this->frame, f.frame, this->frameLength);
	}
}

//IPFrame &IPFrame::operator=(const IPFrame& f)
//{
//	frameInfo=f.frameInfo;
//	codecInfo=f.codecInfo;
//	isKey=f.isKey;
//
////	cout<<" delete IPFrame perator= frame:"<<(void*)frame<<endl;
////	if(frame!=NULL)
////		delete[] (byte*)frame;
//	frameLength=f.frameLength;
//	if(f.frame!=NULL)
//	{
//		frame = new byte[frameLength];
//		memmove(frame, f.frame, frameLength);
//	}
//	else
//		frame=NULL;
//	return *this;
//}

IPFrame::~IPFrame()
{
	if(this->frame!=NULL)
	{
		delete [] (byte*)this->frame;
		this->frame=NULL;
		this->frameLength=0;
	}
}

void IPFrame::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	if (b==NULL)
	{
		cout << "Invalid NULL buffer for IPFrame toLocal" << endl;
		return;
	}

	this->frameInfo.toLocal(b);
	b+=this->frameInfo.serializationSize();
	
	this->codecInfo.toLocal(b);
	b+=this->codecInfo.serializationSize();
		
	this->isKey=*(bool*)b;
	b+=sizeof(bool);
	
	this->frameLength=*(dword*)b;
	Endian::from(Endian::xarxa, &this->frameLength, sizeof(dword));
	b+=sizeof(dword);

	if(this->frame!=NULL)
	{
		delete [] (byte*)this->frame;
		this->frame=NULL;
	}
	
	// TODO 20061222: en ocasions frameLength te valors xungus. A saber 
	// d'on ve el problema. Arreglem guarrament.
	// 20090530: Considerem xungu per sobre 5MB.
	if (this->frameLength>5*1024*1024)
	{
		this->frameLength=4;
		this->frame = new byte[this->frameLength];
		if (this->frame==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFrame frame buffer");

		// Indiquem com si no hi hagues frame
		((dword*)this->frame)[0]=0;
	}
	else
	{
		this->frame = new byte[this->frameLength];
		if (this->frame==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFrame frame buffer");
		memmove(this->frame, b, this->frameLength);
	}

	b+=this->frameLength;
}

void* IPFrame::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFrame serialization buffer");
	}
	
	byte *b=bbuf;

	this->frameInfo.toNetwork(b);
	b+=this->frameInfo.serializationSize();

	this->codecInfo.toNetwork(b);
	b+=this->codecInfo.serializationSize();

	*((bool*)b)=this->isKey;
	b+=sizeof(bool);
	
	*((dword*)b)=this->frameLength;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);

	memmove(b, this->frame, this->frameLength);
	
	return bbuf;
}

dword IPFrame::serializationSize()
{
	return this->frameInfo.serializationSize() +
		this->codecInfo.serializationSize() + sizeof(bool) + sizeof(dword) +
		this->frameLength;
}


#pragma mark *** IPStreamingMode
IPStreamingMode::IPStreamingMode() : mode(IPStreamingMode::STREAM)
{
}

IPStreamingMode::IPStreamingMode(void *buf)
{
	this->toLocal(buf);
}

IPStreamingMode::IPStreamingMode(dword mode): mode(mode)
{
}

IPStreamingMode::~IPStreamingMode()
{
}

void IPStreamingMode::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->mode=*(dword*)b;
	Endian::from(Endian::xarxa, &this->mode, sizeof(dword));
	b+=sizeof(dword);
}

void* IPStreamingMode::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFrameBufferSize serialization buffer");
	}
	
	byte *b=bbuf;

	*((dword*)b)=this->mode;
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	return bbuf;
}

dword IPStreamingMode::serializationSize()
{
	return sizeof(dword);
}

#pragma mark *** IPFramesPerSecond
IPFramesPerSecond::IPFramesPerSecond() : whole(25), frac(0)
{
}

IPFramesPerSecond::IPFramesPerSecond(void *buf)
{
	this->toLocal(buf);
}

IPFramesPerSecond::IPFramesPerSecond(word fpswhole, word fpsfrac) :
	whole(fpswhole), frac(fpsfrac)
{
}

IPFramesPerSecond::IPFramesPerSecond(double fps)
{
	whole=(word)fps;
	frac=(word)((fps-floor(fps))*1000.0);
}

IPFramesPerSecond::~IPFramesPerSecond()
{
}


void IPFramesPerSecond::toLocal(void* buf)
{
	byte *b=(byte*)buf;
	
	this->whole=*(word*)b;
	Endian::from(Endian::xarxa, &this->whole, sizeof(word));
	b+=sizeof(word);
	
	this->frac=*(word*)b;
	Endian::from(Endian::xarxa, &this->frac, sizeof(word));
	b+=sizeof(word);
}

void* IPFramesPerSecond::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" IPFramesPerSecond serialization buffer");
	}
	
	byte *b=bbuf;

	*((word*)b)=this->whole;
	Endian::to(Endian::xarxa, b, sizeof(word));
	b+=sizeof(word);
	
	*((word*)b)=this->frac;
	Endian::to(Endian::xarxa, b, sizeof(word));
	b+=sizeof(word);
	
	return bbuf;
}

dword IPFramesPerSecond::serializationSize()
{
	return sizeof(word)*2;
}

