#include <Plugins/GestorRecodeExceptions.h>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

RecodePluginException::RecodePluginException(int code, string msg): GestorException(code, msg)
{}

RecodePluginException::RecodePluginException(SerializedException &se): GestorException(se)
{}

string RecodePluginException::getClass()
{
	string c=string("RecodePluginException");
	return c;
}


RPInvalidParamException::RPInvalidParamException(int code, string msg):RecodePluginException(code,msg)
{}	

RPInvalidParamException::RPInvalidParamException(SerializedException &se):RecodePluginException(se)
{}	

string RPInvalidParamException::getClass()
{
	string c=string("RPInvalidParamException");
	return c;
}


RPSessionAlreadyStablishedException::RPSessionAlreadyStablishedException(int code, string msg):RecodePluginException(code,msg)
{}

RPSessionAlreadyStablishedException::RPSessionAlreadyStablishedException(SerializedException &se):RecodePluginException(se)
{}

string RPSessionAlreadyStablishedException::getClass()
{
	string c=string("RPSessionAlreadyStablishedException");
	return c;
}


RPSessionNotStablishedException::RPSessionNotStablishedException(int code, string msg):RecodePluginException(code,msg)
{}

RPSessionNotStablishedException::RPSessionNotStablishedException(SerializedException &se):RecodePluginException(se)
{}

string RPSessionNotStablishedException::getClass()
{
	string c=string("RPSessionNotStablishedException");
	return c;
}

