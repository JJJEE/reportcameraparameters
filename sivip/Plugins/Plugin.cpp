#include <Threads/Thread.h>
#include <Plugins/Plugin.h>
#include <Utils/Types.h>
#include <Plugins/GestorException.h>
#include <Plugins/GestorImageExceptions.h>
#include <ModuleInterface/ModuleInterface.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <exceptions/DeprecatedOperationException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <Exceptions/UnknownSerializedException.h>
#include <Utils/RPC.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <version.h>

#define __20100409_TESTING_QUEUE_LIMIT 1

Mutex Plugin::lastWULock;
map<string, WorkUnit*> Plugin::lastWorkUnitReceived;

Plugin::Plugin(Address addr, string type, Canis *cn) : address(addr), type(0), cn(cn), freeCanis(false), nWorkers(32), devAuth(NULL)
{
	STACKTRACE_INSTRUMENT();
	cout<<"plugin1"<<endl;

	if (cn==NULL)
	{
		throw DeprecatedOperationException("Plugin::Plugin(Address, string,"
			" Canis*) no longer accepts NULL Canis. Please initialize"
			" Canis with the correct settings for type and multicast group"
			" before initializing Plugin");
	}

	this->type=this->cn->getType();

	serviceSocket=new SocketUDP(this->address, SOCK_SERVE);
	
	// Fem que les respostes surtin del socket de servei.
	PluginServiceThread::responseSocket=serviceSocket;
	
	this->workers=new WorkerThreadPool<PluginServiceThread>(this->nWorkers);
	this->workers->startWorkers();
	cout<<"plugin2"<<endl;
	Plugin::setActive(true);
}

Plugin::Plugin(Address addr, short type, Canis *cn) : address(addr), type(type), cn(cn), freeCanis(false), nWorkers(32), devAuth(NULL)
{
	STACKTRACE_INSTRUMENT();

	if (cn==NULL)
	{
		throw DeprecatedOperationException("Plugin::Plugin(Address, string,"
			" Canis*) no longer accepts NULL Canis. Please initialize"
			" Canis with the correct settings for type and multicast group"
			" before initializing Plugin");
	}

	serviceSocket=new SocketUDP(this->address, SOCK_SERVE);
	
	// Fem que les respostes surtin del socket de servei.
	PluginServiceThread::responseSocket=serviceSocket;
	
	this->workers=new WorkerThreadPool<PluginServiceThread>(this->nWorkers);
	this->workers->startWorkers();
	Plugin::setActive(true);
}

Plugin::Plugin(string xmlFile) : nWorkers(32), devAuth(NULL)
{
	STACKTRACE_INSTRUMENT();

	this->cn=new Canis(xmlFile, true);
	this->freeCanis=true;

	FILE *f=fopen(xmlFile.c_str(),"rb");

	if (f==NULL)
		throw FileException(string("File ")+xmlFile+string(" not found"));

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char *buf=new char[len];
	
	if (buf==NULL)
	{
		throw (Exception(0, "Not enough memory to read configuration file"));
	}
		
	fread(buf, len, 1, f);
	fclose(f);
	
	string xmlConts(buf, len);
	delete [] buf;
	XML *config=xmlParser::parse(xmlConts);
	
	xmlNode *n;

	n = config->getNode("/[0]/Service/NumberOfWorkers");
	if(n!=NULL)
		this->nWorkers=atoi(n->getCdata().c_str());	
	
	n = config->getNode("/[0]/Service/IP");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file - no Service/IP found"));
	}
	
	IP ip(n->getCdata());	

	n = config->getNode("/[0]/Service/Port");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file - no Service/IP found"));
	}
	
	int port = atoi(n->getCdata().c_str());
	this->address=Address(ip, port);

	// No llegim del fitxer de config perque ja ho ha llegit el canis ;)
	this->type=this->cn->getType();

	this->devAuth = new DeviceAuthentication(config->getRoot());

	serviceSocket=new SocketUDP(this->address, SOCK_SERVE);

	// Fem que les respostes surtin del socket de servei.
	PluginServiceThread::responseSocket=serviceSocket;
	
	this->workers=new WorkerThreadPool<PluginServiceThread>(this->nWorkers);
	this->workers->startWorkers();

	if(!this->configurePriorityFailover(config, cn, this->address))
	{
		cout<<"prio not configured"<<config->toString();
		Plugin::setActive(true);
		this->cn->startSend();
	}

	delete config;
}

Plugin::~Plugin()
{
	STACKTRACE_INSTRUMENT();
	
	if (freeCanis)
	{
		delete cn;
		this->freeCanis=false;
	}
	
	if (serviceSocket!=NULL)
	{
		delete serviceSocket;
		serviceSocket=NULL;
	}
}

void  Plugin::startPriorityService()
{
	cout << "Starting Canis announce thread." << endl;
	Plugin::setActive(true);
	this->cn->startSend();
}

void Plugin::stopPriorityService()
{
	cout << "Stopping Canis announce thread." << endl;
	Plugin::setActive(false);
	this->cn->stopSend();
}

void Plugin::serve()
{
	STACKTRACE_INSTRUMENT();

	RPCPacket *p = NULL;
	Address responseAddr(IP("0.0.0.0"), 0);
	PluginServiceThread::WorkUnitData *wud;

	Timer statsTimer;
	TimerInstant statsElapsed;
	statsTimer.start();
	
serveStart:

	this->serveStartUp();
serveRecover:
	serviceSocket->setTimeout(60000); // el CentDir fa un keepAlive cada 2 min :P	
	
	try
	{
		// Atencio, no es pot fer un << de getId a Win
//		cout << "[" << Thread::getId() << "] Module::serve(): started up" << endl;
		statsElapsed=statsTimer.time();
		if (statsElapsed.seconds()>2.0)
		{
			int nQueued=this->workers->getQueueLength();
			cout << "[" << StrUtils::getDateString() << "] "  << __FILE__ << ", line " << __LINE__ << ": Plugin queue stats\n" << 
			WorkerThread::getWorkingCount() << "/" << WorkerThread::workerThreadCount << " active threads\n" << 
			nQueued << " queued requests" << endl;
#ifdef __20100409_TESTING_QUEUE_LIMIT
			if (nQueued>100)
			{
				cout << "[" << StrUtils::getDateString() << "] "  << __FILE__ << ", line " << __LINE__ << ": CUIDADIN nQueued!!" << endl;
				// Avortem (pq genera core) i si esta catchada la signal
				// o algo, es fa l'exit.
				abort();
				exit(-1);
			}
#endif /* __20100409_TESTING_QUEUE_LIMIT */
			statsTimer.start();				
		}

		while (p=RPC::receivePacket(serviceSocket, &responseAddr, NULL, NULL, 1000))
		{
			statsElapsed=statsTimer.time();
			if (statsElapsed.seconds()>2.0)
			{
				int nQueued=this->workers->getQueueLength();
				cout << "[" << StrUtils::getDateString() << "] "  << __FILE__ << ", line " << __LINE__ << ": Plugin queue stats\n" << 
				WorkerThread::getWorkingCount() << "/" << WorkerThread::workerThreadCount << " active threads\n" << 
				nQueued << " queued requests" << endl;
#ifdef __20100409_TESTING_QUEUE_LIMIT
				if (nQueued>100)
				{
					cout << "[" << StrUtils::getDateString() << "] "  << __FILE__ << ", line " << __LINE__ << ": CUIDADIN nQueued!!" << endl;
					// Avortem (pq genera core) i si esta catchada la signal
					// o algo, es fa l'exit.
					abort();
					exit(-1);
				}
#endif /* __20100409_TESTING_QUEUE_LIMIT */
				statsTimer.start();				
			}
		
		// Atencio, no es pot fer un << de getId a Win
//			cout << "[" << Thread::getId() << "] Module::serve(): received packet" << endl;

			if (p->id == (word)ModuleInterface::isAliveServiceId)
			{
				cout<<"=PiA="<<endl;
			//	RPCPacket *response=new RPCPacket(this->address, (word)-2, se->bytes(), se->size(), this->type, p->origen);
				RPCPacket *response=new RPCPacket(this->address, (word)RPCPacket::responsePacketId, NULL, 0, this->type, 0, false);

				RPC::sendResponse(*response, &responseAddr,
						PluginServiceThread::responseSocket);
				delete response;
				delete p;
				continue;
			}	
			
			wud=new PluginServiceThread::WorkUnitData(p, this);
//			wud->p=p;	// JA NO --- L'ha d'alliberar el thread
//			wud->plugin=this;
			
			WorkUnit *wu=new WorkUnit(wud, responseAddr);
			
			Plugin::lastWULock.lock();
			try
			{
				Plugin::lastWorkUnitReceived[responseAddr.toString()]=wu;
			}
			catch (std::exception &stde)
			{
				cout  << __FILE__ << ", line " << __LINE__ << ": std in lastWURecived["<<responseAddr.toString()<<"] = "<< (void*) wu <<":" << stde.what() << endl;
			}
			catch (...)
			{
				cout  << __FILE__ << ", line " << __LINE__ << ": unknown exception in lastWURecived["<<responseAddr.toString()<<"] = "<< (void*) wu << endl;
			}
			
			Plugin::lastWULock.unlock();
			
			this->workers->queueWorkUnit(wu);
		}
	}
	catch (SocketTimeoutException &se)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": " << se.getClass() << ": " << se.getMsg() << endl;

		cout << "Recovered" << endl;
		goto serveRecover;
	}
	catch (SocketException &se)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": " << se.getClass() << ": " << se.getMsg() << endl;

		delete serviceSocket;		
		serviceSocket=new SocketUDP(this->address, SOCK_SERVE);
		PluginServiceThread::responseSocket=serviceSocket; 

		this->serveShutDown();

		cout << "Recovered" << endl;

		goto serveStart;
	}
	catch (Exception &e)
	{
		cout  << __FILE__ << ", line " << __LINE__ << ": Recoverable " << e.getClass() << ": " << e.getMsg() << endl;
		// Aqui responseAddr hauria de tenir un valor valid
		if (responseAddr.getPort()==0)
		{
			cout << "Impossible to recover, quitting..." << endl;

			return;
		}

		// Muntem un RPCPacket d'Excepcio
		SerializedException *se=e.serialize();
		
		// TODO: Atencio, que passa amb el tipus desti? (ara 0)
		RPCPacket *response=new RPCPacket(this->address, (word)-2, se->bytes(), se->size(), this->type, p->origen);

		RPC::sendResponse(*response, &responseAddr,
			PluginServiceThread::responseSocket);
		delete se;
		delete response;
		
		if (p!=NULL)
			delete p;
			
		this->serveShutDown();

		cout << "Recovered" << endl;

		goto serveStart;
	}

	this->serveShutDown();
}

void Plugin::serveStartUp()
{
}

void Plugin::serveShutDown()
{
}

WorkUnit *Plugin::getLastWorkUnitForOrigin(string origin)
{
	STACKTRACE_INSTRUMENT();
	WorkUnit *wu=NULL;
	Plugin::lastWULock.lock();
	try
	{
		map<string, WorkUnit*>::iterator wuIt=Plugin::lastWorkUnitReceived.find(origin);
		
		if (wuIt!=Plugin::lastWorkUnitReceived.end())
			wu=wuIt->second;
	}
	catch (...)
	{
	}
	
	Plugin::lastWULock.unlock();
	return wu;
}

WorkUnit *Plugin::safelyRemoveWorkUnitForOrigin(string origin,
	WorkUnit *removableWorkUnit)
{
	STACKTRACE_INSTRUMENT();
	WorkUnit *wu=NULL;
	Plugin::lastWULock.lock();
	try
	{
		map<string, WorkUnit*>::iterator wuIt=Plugin::lastWorkUnitReceived.find(origin);
		
		if (wuIt!=Plugin::lastWorkUnitReceived.end())
		{
			wu=wuIt->second;
			if (wu==removableWorkUnit)
				Plugin::lastWorkUnitReceived.erase(wuIt);
		}
	}
	catch (Exception &e)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": " << e.getClass() << ": " << e.getMsg() << endl;
	}
	catch (std::exception &stde)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": " << stde.what() << endl;
	}
	catch (...)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": unknown exception" << endl;
	}
	
	Plugin::lastWULock.unlock();
	return wu;
}	

Address Plugin::getAddress()
{
	return this->address;
}

short Plugin::getType()
{
	return this->type;
}

Canis *Plugin::getCanis()
{
	return this->cn;
}

RPCPacket* Plugin::service(RPCPacket *inPkt, Address *a)
{
	STACKTRACE_INSTRUMENT();
	if (services==NULL)//( || ((short)inPkt->id)<0 || inPkt->id>=numServices)
	{
		return NULL;
	}
	
	RPCPacket *result=NULL;
	
	try
	{
		if (inPkt->id == (word)ModuleInterface::isAliveServiceId)
			result=Plugin::isAlive(this, a, inPkt->getData());
		else if (inPkt->id == (word)ModuleInterface::getVersionServiceId)
			result=Plugin::getVersion(this, a, inPkt->getData());
		else if (inPkt->id>=0 && inPkt->id<this->numServices)
			result=this->services[inPkt->id].call(this, a, inPkt->getData());
		else
		{
			string excMsg=string("Out of range call id ") +
				StrUtils::decToString(inPkt->id) + string(" for this plugin.");
			cout<<excMsg<<endl;
			throw IdNotFoundException(excMsg);
		}
	}
	catch (Exception &e)
	{
		// Muntem un RPCPacket d'Excepcio
		SerializedException *se=e.serialize();
		
		// TODO: Atencio, que passa amb el tipus desti? (ara 0)
		RPCPacket *p=new RPCPacket(this->address, (word)-2, se->bytes(),
			se->size(), this->type, inPkt->origen, true);

		delete se;
		
		return p;
	}

	return result;
}

// PluginServiceThread
#pragma mark *** PluginServiceThread

SocketUDP* PluginServiceThread::responseSocket=NULL;

PluginServiceThread::PluginServiceThread() : WorkerThread()
{
	STACKTRACE_INSTRUMENT();
}

PluginServiceThread::PluginServiceThread(WorkUnitQueue *wuQueue) : WorkerThread(wuQueue)
{
	STACKTRACE_INSTRUMENT();
}

void* PluginServiceThread::execute(int id, void* params)
{
	STACKTRACE_INSTRUMENT();

	while (this->running && this->wuQueue!=NULL)
	{
		WorkUnit *wu=this->wuQueue->get();
		if (wu==NULL)
			continue;

		WorkUnitData *wud=(WorkUnitData*)wu->getData();
		if (wud==NULL)
		{
			delete wu;
			continue;
		}

		WorkerThread::incWorkingCount();
		RPCPacket *response=NULL;
		
		try
		{
			if (wu->getAgeInMillis() < 15000)
				response=wud->plugin->service(wud->p, wu->getResponseAddressPtr());
			else
				cout << __FILE__ << ", line " << __LINE__ << ": Request passed away before being attended. Skipping (recv from " + wu->getResponseAddressPtr()->toString() << ", service " << wud->p->id << ", " << wu->getAgeInMillis() << "ms)" << endl;
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Unexpected Exception: " << e.getClass() << ": " << e.getMsg() << endl;
			STACKTRACE_DUMP();
			// Si tenim una excepcio no esperada, igualment la serialitzem,
			// perque el nostre client esta esperant alguna resposta...
			try
			{
				SerializedException *se=e.serialize();
		
				if(se!=NULL)
				{		
					byte *sebytes=se->bytes();
					dword sesize=se->size();
					short porigen=wud->p->origen;
			
					response=new RPCPacket(wud->plugin->getAddress(),
					RPCPacket::exceptionPacketId, sebytes, sesize,
					wud->plugin->getType(), porigen, true);
					
					delete se;
				}
			}
			catch (UnknownSerializedException &use)
			{
				cout << __FILE__ << ", line " << __LINE__ << ": Unexpected Exception: " << e.getClass() << ": " << e.getMsg() << " on service " << wud->p->id << " from " << wud->p->a->toString() << endl;
			}
		}
		catch (std::exception &stde)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Unexpected Exception: " << stde.what() << endl;
			STACKTRACE_DUMP();
		}
		catch (...)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Unexpected Exception: (unknown)" << endl;
			STACKTRACE_DUMP();
		}

		// Important, primer 
//		if (wud->keepAliveTh!=NULL)
//			wud->keepAliveTh->remove(wu->getResponseAddress());
			
		if (response!=NULL)
		{
			// Ens assegurem que ens esperen :)
			WorkUnit *lastWU = Plugin::getLastWorkUnitForOrigin(wu->getResponseAddressPtr()->toString());
		
			try
			{
				if (wud->requestAlive && lastWU==wu)
				{
					// Marquem la resposta amb el servei d'origen
					response->origen=wud->p->id;
					RPC::sendResponse(*response, wu->getResponseAddressPtr(),
						PluginServiceThread::responseSocket);
				}
				else
					cout << __FILE__ << ", line " << __LINE__ << ": Request passed away while being attended. Skipping (recv from " + wu->getResponseAddressPtr()->toString() << ", service " << wud->p->id << ", " << wu->getAgeInMillis() << "ms)" << endl;
				
			}
			catch (Exception e)
			{
				// No en fem res, si no hem pogut enviar, ja donara timeout
				// a l'altre extrem
			}
		
			Plugin::safelyRemoveWorkUnitForOrigin(wu->getResponseAddressPtr()->toString(), wu);
			delete response;
		}

		WorkerThread::decWorkingCount();
		
//		delete wud->p;
		delete wud;
		delete wu;
	}

	return NULL;
}

RPCPacket* Plugin::isAlive(Plugin *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	cout<<"======================== Plugin::isAlive ========================"<<endl;
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}

RPCPacket* Plugin::getVersion(Plugin *_this, Address *a, void *params)
{
	STACKTRACE_INSTRUMENT();
	string version=string("v") + siriusVersion + string(" (") +
		siriusBuild + string(")");
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, (byte*)version.c_str(), version.length(), _this->type, 0, true);
	return pk;
}
