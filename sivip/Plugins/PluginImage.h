#ifndef __SIRIUS__BASE__PLUGINIMAGE_H
#define __SIRIUS__BASE__PLUGINIMAGE_H

#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Plugins/Plugin.h>
#include <Plugins/GestorImageTypes.h>

class PluginImage : public Plugin
{
public:
	static const word getDeviceInformationServiceId=0;
	static const word startSessionServiceId=1;
	static const word endSessionServiceId=2;
	static const word getCodecInUseServiceId=3;
	static const word setCodecInUseServiceId=4;
	static const word getFrameBufferSizeServiceId=5;
	static const word setFrameBufferSizeServiceId=6;
	static const word getFrameBufferPercentInUseServiceId=7;
	static const word getFrameBufferFramesInUseServiceId=8;
	static const word getStreamingModeServiceId=9;
	static const word setStreamingModeServiceId=10;
	static const word getCompressedNextFrameServiceId=11;
	static const word getDecompressedNextFrameServiceId=12;
	static const word getFPSServiceId=13;
	static const word setFPSServiceId=14;
	
	static const word serviceCount = 15; 

private:
	void init();

private:		
	// Metodes d'atencio de la crida per xarxa
	static RPCPacket* startSessionService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* endSessionService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* getDeviceInformationService(PluginImage *_this, Address *a, void *params);

	static RPCPacket* getCodecInUseService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* setCodecInUseService(PluginImage *_this, Address *a, void *params);

	static RPCPacket* getFrameBufferSizeService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* setFrameBufferSizeService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* getFrameBufferPercentInUseService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* getFrameBufferFramesInUseService(PluginImage *_this, Address *a, void *params);

	static RPCPacket* getStreamingModeService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* setStreamingModeService(PluginImage *_this, Address *a, void *params);

	static RPCPacket* getCompressedNextFrameService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* getDecompressedNextFrameService(PluginImage *_this, Address *a, void *params);

	static RPCPacket* getFPSService(PluginImage *_this, Address *a, void *params);
	static RPCPacket* setFPSService(PluginImage *_this, Address *a, void *params);

protected:
	PluginImage(Address addr, string type);

public:
	PluginImage(Address addr);
	PluginImage(string file);

	// Metodes abstractes a implementar pel plugin
	virtual IPDeviceInfo getDeviceInformation(IPDeviceInfo dev, Address *a) = 0;
	virtual void startSession(IPDeviceID id, Address *a) = 0;
	virtual void endSession(IPDeviceID id, Address *a) = 0;

	virtual IPCodecInfo getCodecInUse(Address *a) = 0;
	virtual void setCodecInUse(IPCodecInfo codec, Address *a) = 0;

	virtual IPFrameBufferSize getFrameBufferSize(Address *a) = 0;
	virtual void setFrameBufferSize(IPFrameBufferSize size, Address *a) = 0;
	virtual float getFrameBufferPercentInUse(Address *a) = 0;
	virtual int getFrameBufferFramesInUse(Address *a) = 0;

	virtual IPStreamingMode getStreamingMode(Address *a) = 0;
	virtual void setStreamingMode(IPStreamingMode mode, Address *a) = 0;

	virtual IPFrame getCompressedNextFrame(Address *a) = 0;
	virtual IPFrame getDecompressedNextFrame(Address *a) = 0;

	virtual IPFramesPerSecond getFPS(Address *a) = 0;
	virtual void setFPS(IPFramesPerSecond fps, Address *a) = 0;
};

#endif
