#ifndef __BASE__PLUGINS__GETBESTPLUGINTHREAD_H
#define __BASE__PLUGINS__GETBESTPLUGINTHREAD_H

#include <Threads/Thread.h>
#include <Utils/Canis.h>
#include <Utils/RPCPacket.h>
#include <Utils/SerializedException.h>

class GetBestPluginThread : public Thread
{
public:
	struct args
	{
		Canis *cn;
		string pluginType;
		short pluginTypeId;
		dword devId;
		
		Address moduleAddr;
		word moduleTypeId;
		
		args() : cn(NULL), pluginType(""), pluginTypeId(0), devId(0xffffffff)
		{
		}
	};
	
	struct result
	{
		RPCPacket *plugin;
		SerializedException *e;
	};

	GetBestPluginThread();
	~GetBestPluginThread();
	
	virtual void* execute(int id, void *args);
};

#endif
