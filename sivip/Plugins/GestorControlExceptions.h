#pragma once
#include <Utils/debugNew.h>
#include <Plugins/GestorException.h>

class ControlPluginException: public GestorException
{
	public:
		ControlPluginException(string msg);
		ControlPluginException(int code, string msg);
		ControlPluginException(SerializedException &se);
		virtual string getClass();
};

class CPInvalidParamException: public ControlPluginException
{
	public:
		CPInvalidParamException(string msg);
		CPInvalidParamException(int code, string msg);
		CPInvalidParamException(SerializedException &se);
		virtual string getClass();
};

class CPSessionAlreadyStablishedException: public ControlPluginException
{
	public:
		CPSessionAlreadyStablishedException(string msg);
		CPSessionAlreadyStablishedException(int code, string msg);
		CPSessionAlreadyStablishedException(SerializedException &se);
		virtual string getClass();
};

class CPSessionNotStablishedException: public ControlPluginException
{
	public:
		CPSessionNotStablishedException(string msg);
		CPSessionNotStablishedException(int code, string msg);
		CPSessionNotStablishedException(SerializedException &se);
		virtual string getClass();
};

