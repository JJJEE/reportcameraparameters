#include <Plugins/GestorRecodeTypes.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Utils/StrUtils.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>

using namespace std;

#pragma mark *** RPCodecInfo

RPCodecInfo::RPCodecInfo() : name(""), mode(RPCodecInfo::decode),
	width(0), height(0)
{
}

RPCodecInfo::RPCodecInfo(void *buf)
{
	this->toLocal(buf);
}

RPCodecInfo::RPCodecInfo(string name, int mode, int width, int height) :
	name(name), mode(mode), width(width), height(height)
{
	if (this->mode!=RPCodecInfo::encode && this->mode!=RPCodecInfo::decode)
		this->mode=RPCodecInfo::decode;
}

RPCodecInfo::~RPCodecInfo()
{
}

void RPCodecInfo::toLocal(void* buf)
{
	byte *b=(byte*)buf;

	dword nameLen=*(dword*)b;
	Endian::from(Endian::xarxa, &nameLen, sizeof(dword));
	b+=sizeof(dword);

	this->name=string((char*)b, nameLen);
	b+=nameLen;
		
	this->mode=*(int*)b;
	Endian::from(Endian::xarxa, &this->mode, sizeof(int));
	b+=sizeof(int);

	this->width=*(int*)b;
	Endian::from(Endian::xarxa, &this->width, sizeof(int));
	b+=sizeof(int);

	this->height=*(int*)b;
	Endian::from(Endian::xarxa, &this->height, sizeof(int));
	b+=sizeof(int);
}

void* RPCodecInfo::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" RPCodecInfo serialization buffer");
	}
	
	byte *b=bbuf;

	*((dword*)b)=this->name.length();
	Endian::to(Endian::xarxa, b, sizeof(dword));
	b+=sizeof(dword);
	
	memmove(b, this->name.c_str(), this->name.length());
	b+=this->name.length();
		
	*((int*)b)=this->mode;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	*((int*)b)=this->width;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	*((int*)b)=this->height;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	return bbuf;
}

dword RPCodecInfo::serializationSize()
{
	return sizeof(dword) + name.length() + sizeof(int)*3;
}

#pragma mark *** RPSession
RPSession::RPSession() : in(), out()
{
}

RPSession::RPSession(void *buf)
{
	this->toLocal(buf);
}

RPSession::RPSession(RPCodecInfo in, RPCodecInfo out) : in(in), out(out)
{
	// TODO: 20090530 ???? eing????
	in.mode=RPCodecInfo::encode;
	out.mode=RPCodecInfo::decode;
}

RPSession::~RPSession()
{
}

void RPSession::toLocal(void* buf)
{
	byte *b=(byte*)buf;

	in.toLocal(b);
	b+=in.serializationSize();

	out.toLocal(b);
	b+=out.serializationSize();
}

void* RPSession::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" RPSession serialization buffer");
	}
	
	byte *b=bbuf;

	// TODO: 20090530 ???? eing????
	in.mode=RPCodecInfo::decode;
	out.mode=RPCodecInfo::encode;

	in.toNetwork(b);
	b+=in.serializationSize();
	
	out.toNetwork(b);
	b+=out.serializationSize();
	
	return bbuf;
}

dword RPSession::serializationSize()
{
	return in.serializationSize() + out.serializationSize();
}

#pragma mark *** RPFrame
RPFrame::RPFrame() : dsize(0), data(NULL), ownData(false)
{
}

RPFrame::RPFrame(void *buf) : dsize(0), data(NULL)
{
	this->toLocal(buf);
}

RPFrame::RPFrame(int size, int width, int height, void *data,
	bool isKey, bool dontCopy, int padding) : 
	dsize(size), data(data), width(width), height(height), bitDepth(),
	isKey(isKey)
{
	this->padding=padding;
	if (this->padding<0) this->padding=RPFrame::defaultPadding;
	
	if(data!=NULL && this->dsize>0 && !dontCopy)
	{
		this->data = new byte[this->dsize+this->padding];
		ownData=true;
		
		if (this->data!=NULL)
		{
			memmove(this->data, data, this->dsize);
			memset(((byte*)this->data) + this->dsize, 0, this->padding);
		}
	}
	else
		ownData=false;
		
	if (this->data==NULL)
	{
		this->dsize=0;
		ownData=false;
	}
}

RPFrame::RPFrame(const RPFrame& f)
{
	this->dsize=f.dsize;
	this->width=f.width;
	this->height=f.height;
	this->bitDepth=f.bitDepth;
	this->isKey=f.isKey;
	this->padding=f.padding;

//	cout<<" RPFrame::RPFrame(RPFrame &f) data:"<<(void*)f.data<<" size:"<<f.dsize<<endl;

	if(f.data!=NULL && dsize>0)
	{
		this->data = new byte[this->dsize+this->padding];
//		cout<<" RPFrame::RPFrame(RPFrame &f) data copy "<<(void*)data <<endl;

		ownData=true;
		if (this->data!=NULL)
		{
			memmove(this->data, f.data, this->dsize);
			memset(((byte*)this->data) + this->dsize, 0, this->padding);
		}
	}
	else
	{
		this->dsize=0;
		this->data=NULL;
		ownData=false;
	}
}

RPFrame::~RPFrame()
{
	if(this->data != NULL && ownData)
	{
//		cout<<" RPFrame::~RPFrame delete data "<<(void*)data <<endl;
		delete [] (byte*)this->data;
		this->data=NULL;
		this->dsize=0;
	}
}

void RPFrame::setOwnData(bool own)
{
	this->ownData=own;
}

RPFrame & RPFrame::operator=(const RPFrame &f)
{
	this->dsize=f.dsize;
	this->width=f.width;
	this->height=f.height;
	this->bitDepth=f.bitDepth;
	this->isKey=f.isKey;
	this->padding=f.padding;

//	cout<<" RPFrame::RPFrame(RPFrame &f) data:"<<(void*)f.data<<" size:"<<f.dsize<<endl;

	if(f.data!=NULL && dsize>0)
	{
		this->data = new byte[this->dsize+this->padding];
//		cout<<" RPFrame::RPFrame(RPFrame &f) data copy "<<(void*)data <<endl;

		ownData=true;
		if (this->data!=NULL)
		{
			memmove(this->data, f.data, this->dsize);
			memset(((byte*)this->data) + this->dsize, 0, this->padding);
		}
	}
	else
	{
		this->dsize=0;
		this->data=NULL;
		ownData=false;
	}
	return *this;
}

void RPFrame::toLocal(void* buf)
{
	byte* b=(byte*)buf;

	this->dsize=*(int*)b;
	Endian::from(Endian::xarxa, &this->dsize, sizeof(int));
	b+=sizeof(int);

	this->width=*(int*)b;
	Endian::from(Endian::xarxa, &this->width, sizeof(int));
	b+=sizeof(int);

	this->height=*(int*)b;
	Endian::from(Endian::xarxa, &this->height, sizeof(int));
	b+=sizeof(int);

	this->bitDepth=*(int*)b;
	Endian::from(Endian::xarxa, &this->bitDepth, sizeof(int));
	b+=sizeof(int);

	this->isKey=*(bool*)b;
	b+=sizeof(bool);

	if(this->data!=NULL)
		delete [] (byte*)this->data;
	
	this->data=new byte[this->dsize];
	this->ownData=true;

	memmove(this->data, buf, this->dsize);
	b+=this->dsize;
}

void* RPFrame::toNetwork(void* buf)
{
	byte *bbuf=(byte*)buf;
	if (bbuf==NULL)
	{
		bbuf=new byte[this->serializationSize()];
		if (bbuf==NULL)
			throw NotEnoughMemoryException("Not enough memory to allocate"
				" RPFrame serialization buffer");
	}
	
	byte *b=bbuf;

	*((int*)b)=this->dsize;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	*((int*)b)=this->width;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	*((int*)b)=this->height;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	*((int*)b)=this->bitDepth;
	Endian::to(Endian::xarxa, b, sizeof(int));
	b+=sizeof(int);

	*((bool*)b)=this->isKey;
	b+=sizeof(bool);

	memmove(b, this->data, this->dsize);
	b+=this->dsize;

	return bbuf;
}

dword RPFrame::serializationSize()
{
	return sizeof(int)*4 + sizeof(bool) + this->dsize;
}

