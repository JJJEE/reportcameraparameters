#include <Logger/Logger.h>
#include <Exceptions/FileNotOpenException.h>
#include <Exceptions/InvalidChannelException.h>

#include <iostream>
#include <ios>

using namespace std;

Logger::Logger(int channel) : ostream(cout.rdbuf())
{
	switch (channel)
	{
		case Logger::out:
			this->rdbuf(cout.rdbuf());
			break;
		
		case Logger::err:
			this->rdbuf(cerr.rdbuf());
			break;

		default:
			throw InvalidChannelException("Channel is not standard output or standard error");
			break;
	}
}

Logger::Logger(string file, bool append) : ostream(cout.rdbuf())
{
	this->f.open(file.c_str(), ios::out|(append? ios::app : ios::out));
	if (!this->f.is_open())
	{
		throw FileNotOpenException("Could not open or create file");
	}
	this->sborig=this->rdbuf(f.rdbuf());
}

Logger::~Logger()
{
	this->rdbuf(sborig);
	if (this->f.is_open())
		this->f.close();
}