#ifndef __SIRIUS__BASE__LOGGER__LOGGER_H
#define __SIRIUS__BASE__LOGGER__LOGGER_H

#include <ostream>
#include <fstream>

class Logger : virtual public std::ostream
{
protected:
	std::streambuf *sborig;
	std::fstream f;
	
public:
	static const int out=1;
	static const int err=2;

	Logger(int channel=Logger::out);
	Logger(std::string file, bool append=false);
	~Logger();
};

#endif
