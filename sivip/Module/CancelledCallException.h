#pragma once
#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class CancelledCallException : public Exception
{
 public:
	CancelledCallException (string msg, int code=0);
	CancelledCallException (int code, string msg);
	CancelledCallException (SerializedException &se);

	~CancelledCallException ();
	
	virtual string getClass();
};

