#include <Module/Module.h>
#include <util/Queue/Queue.h>
#include <WorkerThreads/WorkerThreadPool>
#include <Utils/Exception.h>
#include <Utils/FileException.h>
#include <XML/xmlNode.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Canis.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>
#include <Utils/StrUtils.h>
#include <Exceptions/UnknownSerializedException.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#include <signal.h>
#endif

#define __20100427_TESTING_QUEUE_LIMIT 1

string Module::__class("Module");
Mutex Module::lastWULock;
RWLock Module::deleteWULock;
map<string, WorkUnit*> Module::lastWorkUnitReceived;

Module::Module(Address addr, short type, Canis *cn) : address(addr), type(type), moduleInterface(NULL), cn(cn), freeCanis(false), config(NULL), nWorkers(32)
{
	STACKTRACE_INSTRUMENT();

	if (cn==NULL)
	{
		this->cn=new Canis(this->address, this->type);
		this->freeCanis=true;
	}

	serviceSocket=new SocketUDP(this->address, SOCK_SERVE);
	
	// Fem que les respostes surtin del socket de servei.
	ModuleServiceThread::responseSocket=serviceSocket;
	
	this->keepAliveTh=new ModuleServiceKeepAliveThread(this->type, 3000, this->serviceSocket);
	this->keepAliveTh->startRunning();

	this->workers=new WorkerThreadPool<ModuleServiceThread>(this->nWorkers);
	this->workers->startWorkers();
	
	Module::setActive(true);
}

Module::Module(string xmlFile) : moduleInterface(NULL), config(NULL), nWorkers(32)
{
	STACKTRACE_INSTRUMENT();

	// El creem en discover only perque depen de si hi ha failoverThread o no
	this->freeCanis=true;
	cn=new Canis(xmlFile, true);

	FILE *f=fopen(xmlFile.c_str(),"rb");

	if (f==NULL)
		throw FileException(string("File ")+xmlFile+string(" not found"));

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char *buf=new char[len];
	
	if (buf==NULL)
	{
		throw (Exception(0, "Not enough memory to read configuration file"));
	}
		
	fread(buf, len, 1, f);
	fclose(f);
	
	string xmlConts(buf, len);
	delete [] buf;
//	20100310: Posat a la classe, per poder-ho aprofitar :P
//	XML *config=xmlParser::parse(xmlConts);
	this->config=xmlParser::parse(xmlConts);
	
	xmlNode *n;

	n = config->getNode("/[0]/Service/NumberOfWorkers");
	if(n!=NULL)
		this->nWorkers=atoi(n->getCdata().c_str());	
	
	n = config->getNode("/[0]/Service/IP");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file - no Service/IP found"));
	}
	
	IP ip(n->getCdata());	

	n = config->getNode("/[0]/Service/Port");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file - no Service/IP found"));
	}
	
	int port = atoi(n->getCdata().c_str());
	this->address=Address(ip, port);

	// No llegim del fitxer de config perque ja ho ha llegit el canis ;)
	this->type=cn->getType();

//	20100310: Posat a la classe, per poder-ho aprofitar :P
//	delete config;

	serviceSocket=new SocketUDP(this->address, SOCK_SERVE);

	// Fem que les respostes surtin del socket de servei.
	ModuleServiceThread::responseSocket=serviceSocket;
	
	this->keepAliveTh=new ModuleServiceKeepAliveThread(this->type, 3000, this->serviceSocket);
	this->keepAliveTh->startRunning();

	this->workers=new WorkerThreadPool<ModuleServiceThread>(this->nWorkers);
	this->workers->startWorkers();

	if(!this->configurePriorityFailover(config, cn, this->address))
	{
		Module::setActive(true);
		this->cn->startSend();
	}

	// Acabem les inicialitzacions del failover
/*	if (Module::failoverTh!=NULL)
	{
		Module::setActive(false);
		Module::failoverTh->start();
	}
	else
	{
		Module::setActive(true);
		this->cn->startSend();
	}*/	
}

void Module::startPriorityService()
{
	cout << "Starting Canis announce thread." << endl;
	Module::setActive(true);
	this->cn->startSend();
}

void Module::stopPriorityService()
{
	cout << "Stopping Canis announce thread." << endl;
	Module::setActive(false);
	this->cn->stopSend();
}


Module::~Module()
{
	STACKTRACE_INSTRUMENT();
	
	if (freeCanis)
	{
		delete cn;
		this->freeCanis=false;
	}
	
	if (serviceSocket!=NULL)
	{
		delete serviceSocket;
		serviceSocket=NULL;
	}
	
	delete config;
	config=NULL;
}


void Module::serve()
{
	STACKTRACE_INSTRUMENT();

	cout << "--> Module::serve()" << endl;

	RPCPacket *p = NULL;
	Address responseAddr(IP("0.0.0.0"), 0);
	WorkUnit *wu=NULL;
	ModuleServiceThread::WorkUnitData *wud=NULL;
	Timer statsTimer;
	TimerInstant statsElapsed;
	statsTimer.start();
	
	Timer queueStatsTimer;
	queueStatsTimer.start();
		
serveStart:

	this->serveStartUp();
serveRecover:
	serviceSocket->setTimeout(60000); // el CentDir fa un keepAlive cada 2 min :P	
	try
	{
		// Atencio, no es pot fer un << de getId a Win
//		cout << "[" << Thread::getId() << "] Module::serve(): started up" << endl;
		statsElapsed=queueStatsTimer.time();
		if (statsElapsed.seconds()>2.0)
		{
			int nQueued=this->workers->getQueueLength();
			cout << "[" << StrUtils::getDateString() << "] "  << __FILE__ << ", line " << __LINE__ << ": Module queue stats\n" << 
			WorkerThread::getWorkingCount() << "/" << WorkerThread::workerThreadCount << " active threads\n" << 
			nQueued << " queued requests (Thread lines:" << workers->getThreadStates() <<")" << endl;
			
			this->statsOutput();
			
#ifdef __20100427_TESTING_QUEUE_LIMIT
			if (nQueued>70)
			{
				cout << "[" << StrUtils::getDateString() << "] "  << __FILE__ << ", line " << __LINE__ << ": CUIDADIN nQueued!!" << endl;
				// Avortem (pq genera core) i si esta catchada la signal
				// o algo, es fa l'exit.
				abort();
				exit(-1);
			}
#endif /* __20100427_TESTING_QUEUE_LIMIT */

			queueStatsTimer.start();				
		}

		while (p=RPC::receivePacket(serviceSocket, &responseAddr, NULL, NULL, 1000))
		{
		// Atencio, no es pot fer un << de getId a Win
//			cout << "[" << Thread::getId() << "] Module::serve(): received packet" << endl;
			
			statsElapsed=queueStatsTimer.time();
			if (statsElapsed.seconds()>2.0)
			{
				int nQueued=this->workers->getQueueLength();
				cout << "[" << StrUtils::getDateString() << "] "  << __FILE__ << ", line " << __LINE__ << ": Module queue stats\n" << 
				WorkerThread::getWorkingCount() << "/" << WorkerThread::workerThreadCount << " active threads\n" << 
				nQueued << " queued requests (Thread lines:" << workers->getThreadStates() <<")" << endl;

			this->statsOutput();
			
#ifdef __20100427_TESTING_QUEUE_LIMIT
				if (nQueued>70)
				{
					cout << "[" << StrUtils::getDateString() << "] "  << __FILE__ << ", line " << __LINE__ << ": CUIDADIN nQueued!!" << endl;
					// Avortem (pq genera core) i si esta catchada la signal
					// o algo, es fa l'exit.
					abort();
					exit(-1);
				}
#endif /* __20100427_TESTING_QUEUE_LIMIT */

				queueStatsTimer.start();				
			}

			if(p == NULL)
			{
				cout<<"Module::serve() : p==NULL in RPC::receivePacket!"<<endl;
				continue;
			}
			if (!Module::serviceActive)
			{
				cout << "Received request from " << responseAddr.toString()
					<< ", but service not active" << endl;

				delete p;
				continue;
			}
			
/*			if (p->getDestType() != this->type)
			{
				cout << "Received request from " << responseAddr.toString()
					<< ", but not for me (destination type: "
					<< p->getDestType() << ")" << endl;

				delete p;
				continue;
			}
*/			

			if (p->id == (word)ModuleInterface::isAliveServiceId)
			{
				cout<<"=MiA="<<endl;
			//	RPCPacket *response=new RPCPacket(this->address, (word)-2, se->bytes(), se->size(), this->type, p->origen);
				RPCPacket *response=new RPCPacket(this->address, (word)RPCPacket::responsePacketId, NULL, 0, this->type, 0, false);

				RPC::sendResponse(*response, &responseAddr,
						ModuleServiceThread::responseSocket);
				delete response;
				delete p;
				continue;
			}	

			wud=NULL;
			wud=new ModuleServiceThread::WorkUnitData(p, this->moduleInterface,
				this->keepAliveTh);
//			wud->p=p;	// L'ha d'alliberar el thread
//			wud->modInt=this->moduleInterface;
//			wud->keepAliveTh=this->keepAliveTh;
			
			string serviceDesc=string("Service ") +
				StrUtils::decToString(p->id) + string("/") +
				StrUtils::decToString(this->moduleInterface->getNumServices());
				
			if (this->keepAliveTh!=NULL)
			{
				this->keepAliveTh->add(responseAddr, serviceDesc);
			
				statsElapsed=statsTimer.time();
				
				if (statsElapsed.seconds() >= 2.0)
				{
					cout << this->keepAliveTh->getStatsString();
					statsTimer.start();
				}
			}
			
			wu=NULL;
			wu=new WorkUnit(wud, responseAddr);
			
			deleteWULock.wlock();
			Module::lastWULock.lock();
			WorkUnit *previous = NULL;
			string saddr=responseAddr.toString();
			try
			{
				map<string, WorkUnit*>::iterator it = Module::lastWorkUnitReceived.find(saddr);
				
				//independentment de si hi ha una anterior. Cambiém abans de cancel·lar coses: 
				//l'isCancelled es comprova fora del lastWULock
				if(it != Module::lastWorkUnitReceived.end())
					previous = it->second;
				Module::lastWorkUnitReceived[saddr]=wu;
			}	
			catch (std::exception &stde)
			{
				cout  << __FILE__ << ", line " << __LINE__ << ": std in lastWURecived["<<responseAddr.toString()<<"] = "<< (void*) wu <<":" << stde.what() << endl;
			}
			catch (...)
			{
				cout  << __FILE__ << ", line " << __LINE__ << ": unknown exception in lastWURecived["<<responseAddr.toString()<<"] = "<< (void*) wu << endl;
			}
			Module::lastWULock.unlock();
			this->workers->queueWorkUnit(wu);
			
				
			if(previous == NULL)
				deleteWULock.unlock();
			else
			{
				try
				{
					//hi ha una petició anterior sense respondre->cancel·lem
					string out=string("Module::serve: new request received from ")+saddr+string(", cancelling previous...");
					cout<<out<<endl;
					((ModuleServiceThread::WorkUnitData*)previous->getData())->requestAlive=false;//per si encara està a la cua
					WorkerThread *wt = previous->getAssignedThread();
					if(wt != NULL)
					{
						wt->cancelCurrentWorkUnit();
					}
					//else	
					//	cout<<"Module::service: no assigned thread, not signalling"<<endl;
				}	
				catch (std::exception &stde)
				{
					cout  << __FILE__ << ", line " << __LINE__ << ": std cancelling previous wu: "<< (void*) previous <<":" << stde.what() << endl;
				}
				catch (...)
				{
					cout  << __FILE__ << ", line " << __LINE__ << ": unknown exception cancelling previous wu:["<<responseAddr.toString()<<"] = "<< (void*) previous << endl;
				}
				deleteWULock.unlock();
			}
		}
	}
	catch (SocketTimeoutException &se)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": " << se.getClass() << ": " << se.getMsg() << endl;

/*		cout << "STE Recover:"<<(void*)serviceSocket << endl;
		delete serviceSocket;		
		serviceSocket=new SocketUDP(this->address, SOCK_SERVE);
		ModuleServiceThread::responseSocket=serviceSocket; 
		this->keepAliveTh->setSocket(serviceSocket);

		cout << "STE Recovered:"<<(void*)serviceSocket<<" "<<StrUtils:: getDateString()<<", Socket"<< endl;
*/
		//this->serveShutDown();
		cout << "Recovered" << endl;

		goto serveRecover;
	}
	catch (SocketException &se)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": " << se.getClass() << ": " << se.getMsg() << endl;

//		cout << "SE Recover:"<<(void*)serviceSocket << endl;
		delete serviceSocket;		
		serviceSocket=new SocketUDP(this->address, SOCK_SERVE);
		ModuleServiceThread::responseSocket=serviceSocket; 
		this->keepAliveTh->setSocket(serviceSocket);

//		cout << "SE Recovered" <<(void*)serviceSocket<< endl;

		this->serveShutDown();

		cout << "Recovered" << endl;

		goto serveStart;
	}
	catch (Exception &e)
	{
		cout  << __FILE__ << ", line " << __LINE__ << ": Recoverable " << e.getClass() << ": " << e.getMsg() << endl;
		// Aqui responseAddr hauria de tenir un valor valid
		if (responseAddr.getPort()==0)
		{
			cout << "Impossible to recover, quitting..." << endl;

			return;
		}

		// Muntem un RPCPacket d'Excepcio
		SerializedException *se=e.serialize();
		
		// TODO: Atencio, que passa amb el tipus desti? (ara 0)
		RPCPacket *response=new RPCPacket(this->address, (word)-2, se->bytes(), se->size(), this->type, p->origen);

		RPC::sendResponse(*response, &responseAddr,
			ModuleServiceThread::responseSocket);
		delete se;
		delete response;
		
		if (p!=NULL)
			delete p;
			
		this->serveShutDown();

		cout << "Recovered" << endl;

		goto serveStart;
	}
	catch (std::exception &stde)
	{
		cout  << __FILE__ << ", line " << __LINE__ << ": Recoverable stde:" << stde.what() << endl;
		// Aqui responseAddr hauria de tenir un valor valid
		// TODO: mirar si apareix i per que, i arreglar :P
/*		if (responseAddr.getPort()==0)
		{
			cout << "Impossible to recover, quitting..." << endl;

			return;
		}

		// Muntem un RPCPacket d'Excepcio
		SerializedException *se=e.serialize();
		
		RPCPacket *response=new RPCPacket(this->address, (word)-2, se->bytes(), se->size(), this->type, p->origen);

		RPC::sendResponse(*response, &responseAddr,
			ModuleServiceThread::responseSocket);
		delete se;
		delete response;
		
		if (p!=NULL)
			delete p;
*/	
		this->serveShutDown();

		cout << "Recovered" << endl;

		goto serveStart;
	}
	catch (...)
	{
		cout  << __FILE__ << ", line " << __LINE__ << ": unknown exception, quitting..." << endl;
		return;
	}

	this->serveShutDown();

	cout << "<-- Module::serve()" << endl;
}

void Module::serveStartUp()
{
}

void Module::serveShutDown()
{
}

WorkUnit *Module::getLastWorkUnitForOrigin(string origin)
{
	WorkUnit *wu=NULL;
	Module::lastWULock.lock();
	try
	{
		map<string, WorkUnit*>::iterator wuIt=Module::lastWorkUnitReceived.find(origin);
		
		if (wuIt!=Module::lastWorkUnitReceived.end())
			wu=wuIt->second;
	}
	catch (...)
	{
	}
	
	Module::lastWULock.unlock();
	return wu;
}

WorkUnit *Module::safelyRemoveWorkUnitForOrigin(string origin,
	WorkUnit *removableWorkUnit)
{
	WorkUnit *wu=NULL;
	Module::lastWULock.lock();
	try
	{
		map<string, WorkUnit*>::iterator wuIt=Module::lastWorkUnitReceived.find(origin);
		
		if (wuIt!=Module::lastWorkUnitReceived.end())
		{
			wu=wuIt->second;
			if (wu==removableWorkUnit)
				Module::lastWorkUnitReceived.erase(wuIt);
		}
	}
	catch (Exception &e)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": " << e.getClass() << ": " << e.getMsg() << endl;
	}
	catch (std::exception &stde)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": " << stde.what() << endl;
	}
	catch (...)
	{
		cout << __FILE__ << ", line " << __LINE__ << ": unknown exception" << endl;
	}
	
	Module::lastWULock.unlock();
	return wu;
}	

Address Module::getAddress()
{
	return this->address;
}

short Module::getType()
{
	return this->type;
}

Canis *Module::getCanis()
{
	return cn;
}

string Module::getClass()
{
	return __class;
}

void Module::statsOutput()
{
	return;
}

// ModuleServiceThread
#pragma mark *** ModuleServiceThread

SocketUDP* ModuleServiceThread::responseSocket=NULL;

ModuleServiceThread::ModuleServiceThread() : WorkerThread()
{
	STACKTRACE_INSTRUMENT();
}

ModuleServiceThread::ModuleServiceThread(WorkUnitQueue *wuQueue) :
	WorkerThread(wuQueue)
{
	STACKTRACE_INSTRUMENT();
}


void* ModuleServiceThread::execute(int id, void* params)
{
	STACKTRACE_INSTRUMENT();
#ifdef THREAD_TRACE_DEBUG
	int *ttdDebug = Thread::ttdGetPointer();
#endif

	bool doDebug = false;
	int *debug = WorkerThreadPool<ModuleServiceThread>::getMyDebugPtr();
	doDebug = (debug != NULL);

	while (this->running && this->wuQueue!=NULL)
	{
		if(doDebug)
			*debug = 1;
		
		//per si ens han cancel·lat la anterior
		//WorkerThreadPool<ModuleServiceThread>::clearCancelled();

		this->working=false;
		WorkUnit *wu=this->wuQueue->get();
		if (wu==NULL)
			continue;

		this->cancelled=false;
		this->working=true;

		wu->setAssignedThread(this);

		if(doDebug)
			*debug = 2;
		WorkUnitData *wud=(WorkUnitData*)wu->getData();
		if (wud==NULL || !wud->requestAlive)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Request "
				<< (void *)wu << " data "<< (void*)wud << " NULL or NOT "
				"alive - responseAddr: "
				<< wu->getResponseAddressPtr()->toString() << endl;

			if (wud!=NULL)
			{
				cout << __FILE__ << ", line " << __LINE__ << ": Packet info: "
					"id: " << wud->p->id << ", source "
					<< wud->p->a->toString() << endl;
				if (wud->keepAliveTh!=NULL)
				{
					wud->keepAliveTh->remove(wu->getResponseAddress(), true);
//	#ifdef WITH_DEBUG_GDB
//					cout<<"."<<endl;
//					::kill(getpid(), SIGTRAP);
//					for (;;);
//	#endif
				}
				Module::deleteWULock.rlock();
				delete wud;
			}
			else
				Module::deleteWULock.rlock();
			delete wu;
			Module::deleteWULock.unlock();
			continue;
		}

		WorkerThread::incWorkingCount();
		RPCPacket *response=NULL;
		
		try
		{
			if(doDebug)
				*debug = 3;
			if(this->cancelled)
			{
				cout << __FILE__ << ", line " << __LINE__ << ": Request cancelled before being attended. Skipping (recv from " + wu->getResponseAddressPtr()->toString() << ", service " << wud->p->id << ", " << wu->getAgeInMillis() << "ms)" << endl;
			}
			else if (wu->getAgeInMillis() < 15000)
			{
				response=wud->modInt->service(wud->p, wu->getResponseAddressPtr());
			}
			else
				cout << __FILE__ << ", line " << __LINE__ << ": Request passed away before being attended. Skipping (recv from " + wu->getResponseAddressPtr()->toString() << ", service " << wud->p->id << ", " << wu->getAgeInMillis() << "ms)" << endl;
			if(doDebug)
				*debug = 4;
#ifdef THREAD_TRACE_DEBUG
			// Fem que no surti al llistat de debugs, no ens interessa
			// un cop ha ates una peticio
			if (ttdDebug!=NULL)
				*ttdDebug=0;
#endif
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Unexpected Exception: " << e.getClass() << ": " << e.getMsg() << " on service " << wud->p->id << " from " << wud->p->a->toString() << endl;
			STACKTRACE_DUMP();
			
			// Si tenim una excepcio no esperada, igualment la serialitzem,
			// perque el nostre client esta esperant alguna resposta...
			try
			{
				SerializedException *se=e.serialize();
		
				if(se!=NULL)
				{		
					byte *sebytes=se->bytes();
					dword sesize=se->size();
					short porigen=wud->p->origen;
			
					response=new RPCPacket(wud->modInt->getAddress(),
					RPCPacket::exceptionPacketId, sebytes, sesize,
					wud->modInt->getType(), porigen, true);
					
					delete se;
				}
			}
			catch (UnknownSerializedException &use)
			{
				cout << __FILE__ << ", line " << __LINE__ << ": Unexpected Exception: " << e.getClass() << ": " << e.getMsg() << " on service " << wud->p->id << " from " << wud->p->a->toString() << endl;
			}
		}
		catch (std::exception &stde)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Unexpected Exception: " << stde.what()  << " on service " << wud->p->id << " from " << wud->p->a->toString() << endl;
			STACKTRACE_DUMP();
		}
		catch (...)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Unexpected Exception: (unknown)"  << " on service " << wud->p->id << " from " << wud->p->a->toString() << endl;
			STACKTRACE_DUMP();
		}
	
#ifdef THREAD_TRACE_DEBUG
			// Fem que no surti al llistat de debugs, no ens interessa
			// un cop ha ates una peticio
			if (ttdDebug!=NULL)
				*ttdDebug=0;
#endif
		// Important, primer 
		if (wud->keepAliveTh!=NULL)
			wud->keepAliveTh->remove(wu->getResponseAddress());
			
		if(doDebug)
			*debug = 5;
		if (response!=NULL)
		{
			// Ens assegurem que ens esperen :)
			if(!this->cancelled)
			{
				WorkUnit *lastWU = Module::getLastWorkUnitForOrigin(wu->getResponseAddressPtr()->toString());

				try
				{
					//				cout << "ModuleServiceThread::execute: response to " <<
					//					wu->getResponseAddressPtr()->toString() << endl;
					// Enviem resposta per responseSocket. Si pel que fos fos
					// NULL, sendResponse ja crea un socket i el tanca.
					if (wud->requestAlive && lastWU==wu)
					{
						// Marquem la resposta amb el servei d'origen
						response->origen=wud->p->id;
						RPC::sendResponse(*response, wu->getResponseAddressPtr(),
								ModuleServiceThread::responseSocket);
					}
					else
						cout << __FILE__ << ", line " << __LINE__ << ": Request passed away while being attended. Skipping (recv from " + wu->getResponseAddressPtr()->toString() << ", service " << wud->p->id << ", " << wu->getAgeInMillis() << "ms)" << endl;
				}
				catch (Exception &e)
				{
					// No en fem res, si no hem pogut enviar, ja donara timeout
					// a l'altre extrem
					cout << __FILE__ << ", line " << __LINE__ << ": " << e.getClass() << ": " << e.getMsg() << " on service " << wud->p->id << " from " << wud->p->a->toString() << endl;
				}
			}
			else
			{
				cout << __FILE__ << ", line " << __LINE__ << ": Request cancelled while being attended, not sending response (recv from " + wu->getResponseAddressPtr()->toString() << ", service " << wud->p->id << ", " << wu->getAgeInMillis() << "ms)" << endl;
			}
			//Module::safelyRemoveWorkUnitForOrigin(wu->getResponseAddressPtr()->toString(), wu);
			delete response;
		}
		Module::safelyRemoveWorkUnitForOrigin(wu->getResponseAddressPtr()->toString(), wu);

		if(doDebug)
			*debug = 6;

		WorkerThread::decWorkingCount();
		
//		delete wud->p;
		Module::deleteWULock.rlock();
		delete wud;
		delete wu;
		Module::deleteWULock.unlock();
	}

	return NULL;
}

