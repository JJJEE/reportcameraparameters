#include <Module/ModuleServiceKeepAliveThread.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <unistd.h>
#include <list>

using namespace std;

ModuleServiceKeepAliveThread::KeepAliveStats::KeepAliveStats() : sampleCount(0),
	minLen(0), maxLen(0), avgLen(0.0), minTime(0.0), avgTime(0.0), maxTime(0.0),
	attendCount(0), receiveCount(0), discardCount(0.0)
{
}

string ModuleServiceKeepAliveThread::KeepAliveStats::toString()
{
	string stats;
	
	stats+=string("Request queue stats("+StrUtils::getDateString()+")\n-------------------");
	stats+=string("\n   Sample count: ") + StrUtils::decToString(this->sampleCount);
	stats+=string("\n Minimum length: ") + StrUtils::decToString(this->minLen);
	stats+=string("\n Average length: ") + StrUtils::floatToString(this->avgLen);
	stats+=string("\n Maximum length: ") + StrUtils::decToString(this->maxLen);
	stats+=string("\n Received count: ") + StrUtils::decToString(this->receiveCount);
	stats+=string("\n Attended count: ") + StrUtils::decToString(this->attendCount);
	stats+=string("\nDiscarded count: ") + StrUtils::decToString(this->discardCount);
	stats+=string("\n   Minimum time: ") + StrUtils::floatToString(this->minTime);
	stats+=string("\n   Average time: ") + StrUtils::floatToString(this->avgTime);
	stats+=string("\n   Maximum time: ") + StrUtils::floatToString(this->maxTime);
	stats+=string("\n\n");

	return stats;
}

void ModuleServiceKeepAliveThread::KeepAliveStats::sample(dword currentLen, double sampleTime)
{
	// Es crida lockat :P

	dword newSampleCount=this->sampleCount+1;
	double multNew = 1.0/(double)newSampleCount;
	double multCurr = 1.0 - multNew;
	
	if (this->sampleCount==0)
	{
		this->minLen = currentLen;
		this->maxLen = currentLen;
		this->minTime = sampleTime;
		this->maxTime = sampleTime;
	}
	else
	{
		if (currentLen < this->minLen) this->minLen=currentLen;
		if (currentLen > this->maxLen) this->maxLen=currentLen;
		if (sampleTime < this->minTime) this->minTime=sampleTime;
		if (sampleTime > this->maxTime) this->maxTime=sampleTime;
	}
	
	this->avgLen = this->avgLen * multCurr + (double)currentLen * multNew;
	this->avgTime = this->avgTime * multCurr + sampleTime * multNew;
	
	this->sampleCount++;
}

void ModuleServiceKeepAliveThread::KeepAliveStats::received()
{
	this->receiveCount++;
}

void ModuleServiceKeepAliveThread::KeepAliveStats::attended()
{
	this->attendCount++;
}

void ModuleServiceKeepAliveThread::KeepAliveStats::discarded()
{
	this->discardCount++;
}


ModuleServiceKeepAliveThread::ModuleServiceKeepAliveThread(short type, int millis, SocketUDP *sock)
{
	STACKTRACE_INSTRUMENT();
	
	this->type=type;
	this->millis=millis;
	this->sock=sock;
	if (this->sock==NULL)
		this->sock=new SocketUDP(Address(IP("0.0.0.0"), 0), SOCK_SERVE);
		
	this->pk=new RPCPacket(Address(IP("0.0.0.0"), 0),
		RPCPacket::keepAlivePacketId);
		
	this->running=false;
}

ModuleServiceKeepAliveThread::~ModuleServiceKeepAliveThread()
{
	STACKTRACE_INSTRUMENT();
	
	this->addrListLock.lock();
	this->addrList.clear();
	this->stopRunning();
	this->addrListLock.unlock();
	
	this->join();
	
	if (this->pk!=NULL)
		delete this->pk;
}

void ModuleServiceKeepAliveThread::setSocket(SocketUDP *s)
{
	this->sock=s;	
}

void ModuleServiceKeepAliveThread::add(Address addr, string desc)
{
	STACKTRACE_INSTRUMENT();
	
	this->addrListLock.lock();

	stats.received();

	// No comprovem si hi es o no perque el que ens interessa es actualitzar el
	// timer
	Timer t;
	t.start();
	KeepAliveInfo ka(addr, t, desc);
	this->addrList[addr.toString()]=ka;

	this->addrListLock.unlock();
}

void ModuleServiceKeepAliveThread::discard(Address addr)
{
	STACKTRACE_INSTRUMENT();
	
	this->addrListLock.lock();
	
	stats.discarded();
	
	map<string, KeepAliveInfo>::iterator mIt=
		this->addrList.find(addr.toString());
	
	if (mIt!=this->addrList.end())
	{
		TimerInstant ti=mIt->second.t.time();
		stats.sample(this->addrList.size(), ti.seconds());
		this->addrList.erase(mIt);
	}
	
	this->addrListLock.unlock();
}

void ModuleServiceKeepAliveThread::remove(Address addr, bool verbose)
{
	STACKTRACE_INSTRUMENT();
	
	this->addrListLock.lock();
	
	stats.attended();
	
	map<string, KeepAliveInfo>::iterator mIt=
		this->addrList.find(addr.toString());
		
	if (mIt!=this->addrList.end())
	{
		if (verbose)
		{
			KeepAliveInfo ka=mIt->second;
			TimerInstant ti=ka.t.time();

			cout << "\n" <<  __FILE__ << ", line " << __LINE__
				<< ": Removing MSKAT entry: " << ": " << mIt->first 
				<< " (" << ti.seconds() << "s)" << (ka.desc.length()>0?
					string(" (")+ka.desc+string(")"):"") << endl;
		}
	
		TimerInstant ti=mIt->second.t.time();
		stats.sample(this->addrList.size(), ti.seconds());
		this->addrList.erase(mIt);
	}
	
	this->addrListLock.unlock();
}

void ModuleServiceKeepAliveThread::startRunning()
{
	STACKTRACE_INSTRUMENT();
	
	this->running=true;
	this->start();
}

void ModuleServiceKeepAliveThread::stopRunning()
{
	STACKTRACE_INSTRUMENT();
	
	this->running=false;
}

void* ModuleServiceKeepAliveThread::execute(int id, void* params)
{
	STACKTRACE_INSTRUMENT();
	
	list <KeepAliveInfo> removable;
	
	while (this->running)
	{
		removable.clear();
		
		this->addrListLock.lock();
		int i=1;
		int s=this->addrList.size();
		
		try
		{
			bool printed=false;
			for (map<string, KeepAliveInfo>::iterator lIt=this->addrList.begin();
				lIt!=this->addrList.end(); lIt++)
			{
				KeepAliveInfo ka=lIt->second;
				TimerInstant ti=ka.t.time();
	
				if (ti.seconds()>=1.0)
				{
					cout << "\nMSKAT "<< i << "/" << s << ": " << lIt->first
						<< " (" << ti.seconds() << "s)" << (ka.desc.length()>0?
							string(" (")+ka.desc+string(")"):"");
					cout.flush();
					printed=true;
					// Enviem el KeepAlive nomes si fa mes d'un segon que esta
					// la peticio a la cua
					int len=0;
					byte *data=(byte*)this->pk->packet(&len);
					
					if (ti.seconds() > 15.0)
						removable.push_back(ka);
						
					this->sock->write(ka.addr, data, len);
					
					delete [] data;
				}
				else
				{
					cout << "\nMSKAT "<< i << "/" << s << ": " << lIt->first 
						<< " (" << ti.seconds() << "s) - NOT SENDING";
					cout.flush();
					printed=true;
				}
				i++;
			}
			if (printed)
				cout << endl;
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": " << e.getClass()
				<< ": " << e.getMsg() << endl;
		}
		catch (std::exception &stde)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": " << stde.what()
				<< endl;
		}
		catch (...)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Unknown exception."
				<< endl;
		}
		
		this->addrListLock.unlock();
		
		i=1;
		s=removable.size();
		if (s>0)
		{
			cout << "MSKAT: Removing timed out keep alives (older than 15s)"
				<< endl;
		}
		for (list<KeepAliveInfo>::iterator lIt=removable.begin();
			lIt!=removable.end(); lIt++)
		{
			KeepAliveInfo toutKAInfo=*lIt;
			cout << "MSKAT Timeout: " << i << "/" << s << ": " <<
				toutKAInfo.addr.toString() <<
				(toutKAInfo.desc.length()>0?
					string(" (")+toutKAInfo.desc+string(")"):"")
				<< endl;
			i++;
			this->discard(toutKAInfo.addr);
		}
		
		usleep(this->millis*1000);
	}
	
	return NULL;
}

string ModuleServiceKeepAliveThread::getStatsString()
{
	this->addrListLock.lock();
	string s=stats.toString();
	this->addrListLock.unlock();
	return s;
}
