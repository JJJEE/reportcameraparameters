
#include <Module/ModuleIsAliveThread.h>
#include <ModuleAccess/ModuleAccess.h>
#include <ServiceFinder/ServiceFinder.h>


ModuleIsAliveThread::ModuleIsAliveThread(Canis *cn, int* filter, int nFilters): checkFilter(filter), nFilters(nFilters), cn(cn)
{
	
}

void* ModuleIsAliveThread::execute(int id, void* params)
{
//	cout<<"ModuleIsAliveThread::execute starting"<<endl;
	while(true)
	{
		for(int i=0;i<nFilters;i++)
		{
			int type = checkFilter[i];
			list<Canis::subsys> systems=cn->getTypeList(type);
			list<Canis::subsys>::iterator it=systems.begin(); 

			for (list<Canis::subsys>::iterator it=systems.begin(); 
					it!=systems.end(); it++)
			{

				Canis::subsys s=*it;
				Address addr(s.ip, s.port);
				map<string, int>::iterator ssIt = deadSubsystems.find(addr.toString());

				if(ssIt != deadSubsystems.end())
				{
					if(ssIt->second > minCheckCount && ssIt->second < (minCheckCount+maxDeadCount))
					{
						cout<<"ModuleIsAliveThread::execute NOT re-checking isAlive:"<<addr.toString()<<"("<<ssIt->second<<")"<<endl;
						deadSubsystems[addr.toString()]=ssIt->second+1;
					}
					else //torném a comprobar
					{
						cout<<"ModuleIsAliveThread::execute re-checking isAlive:"<<addr.toString()<<"("<< ssIt->second <<")"<<endl;
						bool isAlive = ModuleAccess::isAlive(addr, type);
						deadSubsystemsLock.wlock();
						if(isAlive)
							deadSubsystems.erase(addr.toString());
						else
						{
							if(ssIt->second < minCheckCount)
								deadSubsystems[addr.toString()] = ssIt->second + 1;
							else
								deadSubsystems[addr.toString()] = minCheckCount +1;
						}
						deadSubsystemsLock.unlock();
					}
				}
				else
				{	
//					cout<<"ModuleIsAliveThread::execute checking isAlive:"<<addr.toString()<<endl;
					if((type != ServiceFinder::centralDirectoryTypeId && type != ServiceFinder::dbGatewayTypeId) || systems.size() > 1)
					{
						if(!ModuleAccess::isAlive(addr, type))
						{
							cout<<"ModuleIsAliveThread::execute WARNING:"<<addr.toString()<<" not Alive"<<endl;
							deadSubsystemsLock.wlock();
							deadSubsystems[addr.toString()] = 1;
							deadSubsystemsLock.unlock();
						}
					}
					//else
					//	cout<<"ModuleIsAliveThread::execute not checking:"<<type<<", "<<systems.size()<<" subsystems"<<endl;
				}
			}
		}
		sleep(1);
	}	
}

bool ModuleIsAliveThread::isAlive(Address addr)
{
	deadSubsystemsLock.rlock();
	map<string, int>::iterator ssIt = deadSubsystems.find(addr.toString());
	if(ssIt != deadSubsystems.end() && ssIt->second > 0)
	{
		deadSubsystemsLock.unlock();
//		cout<<"ModuleIsAliveThread::isAlive:"<<addr.toString()<<" false"<<endl;
		return false;
	}
	deadSubsystemsLock.unlock();
//	cout<<"ModuleIsAliveThread::isAlive:"<<addr.toString()<<" true"<<endl;
	return true;
}

