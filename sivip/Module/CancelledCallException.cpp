#include <Module/CancelledCallException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

CancelledCallException::CancelledCallException(string msg, int code): Exception(msg, code)
{

}

CancelledCallException::CancelledCallException(int code, string msg): Exception(msg, code)
{

}

CancelledCallException::CancelledCallException(SerializedException &se): Exception(se)
{

}

CancelledCallException::~CancelledCallException()
{
   
}

string CancelledCallException::getClass()
{
	string c=string("CancelledCallException");
	return c;
}

