#ifndef __SIRIUS__BASE__MODULE_H
#define __SIRIUS__BASE__MODULE_H

#include <Utils/debugNew.h>
#include <ModuleInterface/ModuleInterface.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>
#include <Threads/Thread.h>
#include <Threads/Mutex.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>
#include <Utils/Canis.h>
#include <WorkerThreads/WorkUnitQueue.h>
#include <WorkerThreads/WorkerThread.h>
#include <WorkerThreads/WorkerThreadPool.h>
#include <Module/ModuleServiceKeepAliveThread.h>
#include <Module/ModulePriorityFailover.h>
#include <Module/ModulePriorityServices.h>

#include <string>

using namespace std;

// Fwd decl per fer servir dins de ModuleServiceThread
class Module;

class ModuleServiceThread : public WorkerThread
{
	friend class Module;

protected:
	// TODO: Fer servir per evitar unable to create socket :P
	static SocketUDP *responseSocket;		// static?

public:
	class WorkUnitData
	{
	public:
		bool requestAlive;
		RPCPacket *p;
		ModuleInterface *modInt;
		ModuleServiceKeepAliveThread *keepAliveTh;
		
		WorkUnitData() : p(NULL), modInt(NULL), keepAliveTh(NULL),
			requestAlive(true) {};
		WorkUnitData(RPCPacket *p, ModuleInterface *modInt, 
			ModuleServiceKeepAliveThread *keepAliveTh) :
			p(p), modInt(modInt), keepAliveTh(keepAliveTh),
			requestAlive(true) {};
			
		virtual ~WorkUnitData()
		{
			if (p!=NULL)
				delete p;
		};
	};

	ModuleServiceThread();
	ModuleServiceThread(WorkUnitQueue *wuQueue);
	
	virtual void* execute(int id, void* params);
};

class Module: public ModulePriorityServices
{
	friend class ModuleServiceThread;
protected:
	static string __class;
	
	Address address;
	short type;
	SocketUDP *serviceSocket;

	Canis *cn;
	bool freeCanis;
	
	XML *config;
	
	ModuleInterface *moduleInterface;

	int nWorkers;
	WorkerThreadPool<ModuleServiceThread> *workers;
	
	ModuleServiceKeepAliveThread *keepAliveTh;

	static Mutex lastWULock;
	static map<string, WorkUnit*> lastWorkUnitReceived;
	
protected:
	static RWLock deleteWULock;

public:
	Module(Address addr, short type, Canis *cn=NULL);
	Module(string xmlFile);
	
	virtual ~Module();
	virtual void startPriorityService();
	virtual void stopPriorityService();
	
	virtual void serve();
	virtual void serveStartUp();
	virtual void serveShutDown();
	
	static WorkUnit *getLastWorkUnitForOrigin(string origin);
	static WorkUnit *safelyRemoveWorkUnitForOrigin(string origin,
		WorkUnit *removableWorkUnit);
	
	Address getAddress();
	short getType();
	
	Canis *getCanis();
	
	string getClass();
	
	virtual void statsOutput();
};

#endif
