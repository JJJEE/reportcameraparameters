
#pragma once

#include <Utils/debugNew.h>
#include <Utils/Canis.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>
#include <Utils/PtrPool.h>
#include <Threads/RWlock.h>

#include <list>
#include <map>
#include <string>


class ModuleIsAliveThread:public Thread
{
	protected:
		static const int maxDeadCount = 10;
		static const int minCheckCount = maxDeadCount*2;

		map<string, int> deadSubsystems;
		RWLock deadSubsystemsLock;
		int *checkFilter;
		int nFilters;
		Canis *cn;

	public:

		ModuleIsAliveThread(Canis *cn, int* filter, int nFilters);

		void* execute(int id, void* params);
		bool isAlive(Address addr);
};

