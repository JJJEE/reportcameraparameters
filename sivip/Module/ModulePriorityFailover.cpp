#include <Module/ModulePriorityFailover.h>
#include <Module/Module.h>
#include <util/Queue/Queue.h>
#include <WorkerThreads/WorkerThreadPool>
#include <Utils/Exception.h>
#include <Utils/FileException.h>
#include <XML/xmlNode.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Canis.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>
#include <Utils/StrUtils.h>
#include <ModuleAccess/ModuleAccess.h>
#include <Exceptions/UnknownSerializedException.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif

#include <iostream>

using namespace std;

ModulePriorityFailover::ModulePriorityFailover(word type, int priority,
	map<string, int> priorities, Canis *cn, ModulePriorityServices *m)
{
	cout << "Creating ModulePriorityFailover with type: " << type << ", "
		"priority: " << priority << endl;

	this->type=type;
	this->priority=priority;
	this->priorities=priorities;
	this->cn=cn;
	this->running=running;
	this->priorityServiceActive=false;
	this->m=m;
	srand(time(NULL));
}

ModulePriorityFailover::~ModulePriorityFailover()
{
}

void ModulePriorityFailover::startPriorityService()
{
//	cout << "Starting Canis announce thread." << endl;
//	Module::setActive(true);
//	this->cn->startSend();
	m->startPriorityService();
}

void ModulePriorityFailover::stopPriorityService()
{
	//cout << "Stopping Canis announce thread." << endl;
	//Module::setActive(false);
	//this->cn->stopSend();
	m->stopPriorityService();
}

void *ModulePriorityFailover::execute(int id, void* params)
{
	int failureCount=0;
	this->running=true;
	
	cout << __FILE__ << ", line " << __LINE__ << ":ModulePriorityFailover::execute" << endl;
	// Abans de comen�ar esperem entre 7,5 i 12,5s, perque es pugui omplir el
	// Canis.
	int randSleep=2500 - (rand()%5000);
	usleep((10000+randSleep)*1000);
	
	while (this->running)
	{
		try
		{
			cout << __FILE__ << ", line " << __LINE__ << ":ModulePriorityFailover::execute" << endl;
			list<Canis::subsys> act = this->cn->getTypeList(this->type);
			
			list<Canis::subsys>::iterator actIt;
			bool higherPriorityFound=false;

			for (actIt=act.begin(); actIt!=act.end(); actIt++)
			{
				Canis::subsys alive = *actIt;
				
				Address actAddr(IP(alive.ip), alive.port);
				string actKey = actAddr.toString();
				
				map<string, int>::iterator prioIt =
					this->priorities.find(actKey);

				if (prioIt != this->priorities.end())
					cout << "Checking " << actKey << ": " << prioIt->second
						<< endl;
				else
					cout << "Checking " << actKey << ": (null)" << endl;
				
				if (prioIt != this->priorities.end()
					&& prioIt->second < this->priority)
				{
					int actPrio = prioIt->second;
					if(ModuleAccess::isAlive(actAddr, this->type))
					{
						cout << "Higher priority (" << actPrio << ") module found."
							<< endl;

						higherPriorityFound=true;

						//if (this->cn->isSending())
						if(this->priorityServiceActive)
						{
							this->stopPriorityService();
							this->priorityServiceActive=false;
						}
						break;
					}
					else
					{
						cout << "WARNING: Higher priority (" << actPrio << ") module found, but not responding to isAlive() request"
							<< endl;
					}
				}
			}
			
			if (!higherPriorityFound)
			{
				cout << "Higher priority module not found." << endl;
			
				// Necessitem 3 fallades minim
				failureCount++;

				//if (!this->cn->isSending() && failureCount>=3)
				if (!this->priorityServiceActive && failureCount>=3)
				{
					this->startPriorityService();
					this->priorityServiceActive=true;
				}
			}
			else
				failureCount=0;

			// 2s de variabilitat, 1 amunt i 1 avall
			int randItSleep=1000 - (rand()%2000);
			usleep((3000 + randItSleep)*1000);
		}
		catch (Exception &e)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": " << e.getClass() 
				<< ": " << e.getMsg() << endl;
		}
		catch (std::exception &stde)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": " << stde.what() 
				<< endl;
		}
		catch (...)
		{
			cout << __FILE__ << ", line " << __LINE__ << ": Unknown exception."
				<< endl;
		}
	}
	return NULL;
}
