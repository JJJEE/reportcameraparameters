#ifndef __SIRIUS__BASE__MODULE_PRIORITY_SERVICES_H
#define  __SIRIUS__BASE__MODULE_PRIORITY_SERVICES_H

#include <Sockets/Address.h>
#include <XML/XML.h>
#include <Utils/Canis.h>
#include <map>
using namespace std;

class ModulePriorityFailover;

class ModulePriorityServices
{
protected:
	XML *prioritiesConfig;
	string modulePrioPrefix;
	static ModulePriorityFailover *failoverTh;

	static bool serviceActive;

	ModulePriorityFailover* createPriorityFailover(Canis *cn, int myPrio, map<string, int> priorities);
public:
	ModulePriorityServices();

	bool configurePriorityFailover(XML *config, Canis *cn, Address address);

	virtual void startPriorityService();
	virtual void stopPriorityService();
	
	virtual void serveStartUp();
	virtual void serveShutDown();
	
	static void setActive(bool act);
	static bool isActive();

};

#endif
