#include <Module/ModulePriorityServices.h>
#include <Module/ModulePriorityFailover.h>
#include <XML/xmlParser.h>
#include <Utils/Exception.h>
#include <Utils/FileException.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#include <signal.h>
#endif


ModulePriorityFailover *ModulePriorityServices::failoverTh=NULL;
bool ModulePriorityServices::serviceActive=false;

ModulePriorityServices::ModulePriorityServices():prioritiesConfig(NULL), modulePrioPrefix(string("/[0]/ModulePriorityFailover")){}

void ModulePriorityServices::startPriorityService()
{
}

void ModulePriorityServices::stopPriorityService()
{
}

void ModulePriorityServices::serveStartUp()
{
}

void ModulePriorityServices::serveShutDown()
{
}

void ModulePriorityServices::setActive(bool act)
{
	ModulePriorityServices::serviceActive=act;
}

bool ModulePriorityServices::isActive()
{
	return ModulePriorityServices::serviceActive;
}

bool ModulePriorityServices::configurePriorityFailover(XML *config, Canis *cn, Address address)
{
	// Creem el failoverThread si toca...
	map<string, int> priorities;
	
	xmlNode *fileNode = config->getNode("/[0]/ModulePriorityFailover/File");

	this->prioritiesConfig = config;
	this->modulePrioPrefix = string("/[0]/ModulePriorityFailover");

	if (fileNode!=NULL)
	{
		string prioFile=fileNode->getCdata();
		
		FILE *pf=fopen(prioFile.c_str(),"rb");
	
		if (pf==NULL)
			throw FileException(string("File ")+prioFile+string(" not found"));
	
		fseek(pf,0,SEEK_END);
		int pLen=ftell(pf);
		fseek(pf,0,SEEK_SET);
		
		char *prioBuf=new char[pLen];
		
		if (prioBuf==NULL)
		{
			throw (Exception(0, "Not enough memory to read configuration file"));
		}
			
		fread(prioBuf, pLen, 1, pf);
		fclose(pf);
		
		string prioXMLConts(prioBuf, pLen);
		delete [] prioBuf;
		
		try
		{
			this->prioritiesConfig=xmlParser::parse(prioXMLConts);
			this->modulePrioPrefix=string("/[0]");
		}
		catch (...)
		{
			this->prioritiesConfig = config;
		}
	}

	xmlNode *enableNode = this->prioritiesConfig->getNode(this->modulePrioPrefix + string("/Enable"));
	xmlNode *prioNode = this->prioritiesConfig->getNode(this->modulePrioPrefix + string("/Priority"));
	
	if(enableNode!=NULL && prioNode!=NULL
		&& enableNode->getCdata()==string("true"))
	{
		int myPrio = atoi(prioNode->getCdata().c_str());
		string myAddr = address.toString();
		cout << "Loading base priority (" << myAddr << "): " << prioNode->getCdata() << endl;
		
		// Si despres tambe apareix a la definicio, prevaldra
		priorities[myAddr]=myPrio;
		
		int prioRow=0;
		
		xmlNode *n=this->prioritiesConfig->getNode(this->modulePrioPrefix + 
			string("/PriorityDefinitions/PriorityDefinition[") + 
			StrUtils::decToString(prioRow) + string("]"));
		while (n!=NULL)
		{
			string servType = this->prioritiesConfig->getNode(
				this->modulePrioPrefix + 
				string("/PriorityDefinitions/PriorityDefinition[") + 
				StrUtils::decToString(prioRow) +
				string("]/Service/Type"))->getCdata();

			if (servType == config->getNode("/[0]/Service/SubsystemType")->getCdata());
			{
				int prio=atoi(this->prioritiesConfig->getNode(
					this->modulePrioPrefix +
					string("/PriorityDefinitions/"
					"PriorityDefinition[") + 
					StrUtils::decToString(prioRow) + 
					string("]/Priority"))->getCdata().c_str());

				string servAddr=this->prioritiesConfig->getNode(
					this->modulePrioPrefix + 
					string("/PriorityDefinitions/"
					"PriorityDefinition[") + 
					StrUtils::decToString(prioRow) + 
					string("]/Service/IP"))->getCdata() + string(":") +
					this->prioritiesConfig->getNode(
					this->modulePrioPrefix + string("/PriorityDefinitions/"
					"PriorityDefinition[") + 
					StrUtils::decToString(prioRow) + 
					string("]/Service/Port"))->getCdata();

				priorities[servAddr]=prio;
				
				if (servAddr == myAddr)
					myPrio = prio;
		
//				cout << "Loaded prio (" << servAddr << "): " << priorities[servAddr] << endl;
			}
			
			prioRow++;
			n=this->prioritiesConfig->getNode(this->modulePrioPrefix + 
				string("/PriorityDefinitions/PriorityDefinition[") + 
				StrUtils::decToString(prioRow) + string("]"));
		}
	
		if (priorities.size() > 1)
		{
			 ModulePriorityServices::failoverTh = createPriorityFailover(cn, myPrio, priorities);
			 ModulePriorityServices::failoverTh->start();
		}else
		{
			ModulePriorityServices::failoverTh = NULL;
		}
		cout << "Loaded priority (" << myAddr << "): " << priorities[myAddr] << " total:"<<priorities.size()<< endl;
	}

	return ModulePriorityServices::failoverTh != NULL;
}

ModulePriorityFailover* ModulePriorityServices::createPriorityFailover(Canis *cn, int myPrio, map<string, int> priorities)
{
	cout<<"Module::createPriorityFailover"<<endl;
	return new ModulePriorityFailover(cn->getType(), myPrio,
			priorities, cn, this);
}

