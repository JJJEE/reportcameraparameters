#ifndef __SIRIUS__BASE__MODULEGESTOR_H
#define __SIRIUS__BASE__MODULEGESTOR_H

#include <Utils/debugNew.h>
#include <Module/Module.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>
#include <Utils/Canis.h>

#include <string>

using namespace std;

class ModuleGestor : public Module
{
public:
	ModuleGestor(Address addr, short type, Canis *cn=NULL);
	ModuleGestor(string xmlFile);

	~ModuleGestor();
};

#endif
