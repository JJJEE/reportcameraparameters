#ifndef __SIRIUS__BASE__MODULESERVICEKEEPALIVETHREAD_H
#define __SIRIUS__BASE__MODULESERVICEKEEPALIVETHREAD_H

#include <Utils/debugNew.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>
#include <Threads/Thread.h>
#include <Threads/Mutex.h>
#include <Utils/Timer.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>
#include <map>

using namespace std;

class ModuleServiceKeepAliveThread : public Thread
{
public:
	class KeepAliveStats
	{
		dword sampleCount;
		dword minLen, maxLen;
		double avgLen, minTime, avgTime, maxTime;
		dword attendCount, discardCount, receiveCount;

	public:	
		KeepAliveStats();
		
		string toString();
		
		void sample(dword currentLen, double sampleTime);
		void received();
		void attended();		
		void discarded();		
	};

protected:
	class KeepAliveInfo
	{
	public:
		Address addr;
		Timer t;
		string desc;
		
		KeepAliveInfo() : addr(), t(), desc() {};
		KeepAliveInfo(Address &a, Timer &t, string desc=string("")) :
			addr(a), t(t), desc(desc) {};
	};

	map<string, KeepAliveInfo> addrList;
	Mutex addrListLock;
	bool running;
	
	short type;
	int millis;
	SocketUDP *sock;
	
	RPCPacket *pk;
	
	KeepAliveStats stats;

	void discard(Address addr);
	
public:
	ModuleServiceKeepAliveThread(short type, int millis=3000, SocketUDP *sock=NULL);
	~ModuleServiceKeepAliveThread();

	void setSocket(SocketUDP *s);
	
	void add(Address addr, string desc=string(""));
	void remove(Address addr, bool verbose=false);
	
	void startRunning();
	void stopRunning();
	
	virtual void* execute(int id, void* params);
	
	string getStatsString();
};


#endif
