#ifndef __SIRIUS__BASE__MODULEPRIORITYFAILOVER_H
#define __SIRIUS__BASE__MODULEPRIORITYFAILOVER_H

#include <Utils/debugNew.h>
#include <ModuleInterface/ModuleInterface.h>
//#include <Module/Module.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>
#include <Threads/Thread.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>
#include <Utils/Canis.h>
#include <WorkerThreads/WorkUnitQueue.h>
#include <WorkerThreads/WorkerThread.h>
#include <WorkerThreads/WorkerThreadPool.h>
#include <Module/ModuleServiceKeepAliveThread.h>
#include <Module/ModulePriorityServices.h>

#include <string>
#include <map>

using namespace std;

class ModulePriorityFailover : public Thread
{
protected:
	word type;
	int priority;
	map<string, int> priorities;
	Canis *cn;
	ModulePriorityServices *m;

	bool running, priorityServiceActive;
	
public:
	ModulePriorityFailover(word type, int priority, map<string, int> priorities,
		Canis *cn, ModulePriorityServices *m);
	
	~ModulePriorityFailover();

	virtual void startPriorityService();
	virtual void stopPriorityService();
	
	virtual void *execute(int id, void* params);
};

#endif
