#include <Module/ModuleGestor.h>
#include <ModuleInterface/ModuleInterfaceGestor.h>
#include <Utils/Exception.h>
#include <Utils/FileException.h>
#include <XML/xmlNode.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Canis.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>

ModuleGestor::ModuleGestor(Address addr, short type, Canis *cn) : Module(addr, type, cn)
{
	__class=string("ModuleGestor");
}

ModuleGestor::ModuleGestor(string xmlFile) : Module(xmlFile)
{
	__class=string("ModuleGestor");
}

ModuleGestor::~ModuleGestor()
{
}
