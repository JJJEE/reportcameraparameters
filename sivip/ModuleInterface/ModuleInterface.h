#ifndef __SIRIUS__BASE__MODULEINTERFACE_H
#define __SIRIUS__BASE__MODULEINTERFACE_H

#include <Utils/debugNew.h>
#include <Endian/Endian.h>
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
#include <Utils/Types.h>
#include <Utils/Canis.h>
#include <XML/XML.h>

#include <string>
#include <map>
#include <iostream>

using namespace std;

class ModuleInterface;

typedef RPCPacket* (*DefaultModuleCall)(ModuleInterface*, Address*, void*);

class ModuleInterface
{
public:
	static const int isAliveServiceId=-100;
	static const int getVersionServiceId=-101;

protected:
	class serviceDef
	{
		public:
			dword inSize, outSize;	// de moment no es fan servir...
			DefaultModuleCall call;
	};

	dword numServices;
	serviceDef *services;

	// Nomes referencia a la de Module, no en fem delete NI RES
	XML *config;

	Address address;
	short type;
	
	Canis *cn;
	bool freeCanis;

public:
	virtual RPCPacket* service(RPCPacket *inPkt, Address *a);

	ModuleInterface(Address address, short type, Canis *cn=NULL);

	void setConfig(XML *config);

	Address getAddress();
	short getType();

	Canis *getCanis();
	
	dword getNumServices();	

	// Serveis generics
	static RPCPacket* isAlive(ModuleInterface *_this, Address *a, void *params);
	static RPCPacket* getVersion(ModuleInterface *_this, Address *a, void *params);
};

#endif
