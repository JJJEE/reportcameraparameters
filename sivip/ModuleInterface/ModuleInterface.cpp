#include <ModuleInterface/ModuleInterface.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <Exceptions/IdNotFoundException.h>
#include <version.h>

RPCPacket* ModuleInterface::service(RPCPacket *inPkt, Address *a)
{
	STACKTRACE_INSTRUMENT();
	if (services==NULL)//( || ((short)inPkt->id)<0 || inPkt->id>=numServices)
	{
		return NULL;
	}
	
	RPCPacket *result=NULL;
	
	try
	{
		if (inPkt->id == (word)ModuleInterface::isAliveServiceId)
		{
			result=ModuleInterface::isAlive(this, a, inPkt->getData());
		}
		else if (inPkt->id == (word)ModuleInterface::getVersionServiceId)
		{
			result=ModuleInterface::getVersion(this, a, inPkt->getData());
		}
		else if (inPkt->id>=0 && inPkt->id<this->numServices)
			result=services[inPkt->id].call(this, a, inPkt->getData());
		else
		{
			string excMsg=string("Out of range call id ") +
				StrUtils::decToString(inPkt->id) + string(" for this module.");
			cout<<excMsg<<endl;
			throw IdNotFoundException(excMsg);
		}
	}
	catch (Exception &e)
	{
		//cout << __FILE__ << ", line " << __LINE__ << ": " << e.getClass() << ": " << e.getMsg() << " on service " << inPkt->id << " from " << a->toString() << endl;
		STACKTRACE_DUMP();

		// Muntem un RPCPacket d'Excepcio
		SerializedException *se=e.serialize();
		
		// TODO: Atencio, que passa amb el tipus desti? (ara 0)
		RPCPacket *p=new RPCPacket(this->address, (word)-2, se->bytes(),
			se->size(), this->type, inPkt->origen, true);

		delete se;
		
		return p;
	}

	return result;
}

ModuleInterface::ModuleInterface(Address address, short type, Canis *cn) : numServices(0), services(NULL), address(address), type(type), cn(cn), freeCanis(false)
{
	STACKTRACE_INSTRUMENT();

	if (cn==NULL)
	{
		this->cn=new Canis(address, type, true);
		this->freeCanis=true;
	}
}

void ModuleInterface::setConfig(XML *config)
{
	this->config=config;
}

Address ModuleInterface::getAddress()
{
	STACKTRACE_INSTRUMENT();

	return address;
}

short ModuleInterface::getType()
{
	STACKTRACE_INSTRUMENT();

	return type;
}

Canis *ModuleInterface::getCanis()
{
	STACKTRACE_INSTRUMENT();

	return cn;
}

dword ModuleInterface::getNumServices()
{
	return this->numServices;
}

RPCPacket* ModuleInterface::isAlive(ModuleInterface *_this, Address *a, void *params)
{
	cout<<"======================== ModuleInterface::isAlive ========================"<<endl;
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, NULL, 0, _this->type, 0, false);
	return pk;
}


RPCPacket* ModuleInterface::getVersion(ModuleInterface *_this, Address *a, void *params)
{
	string version=string("v") + siriusVersion + string(" (") +
		siriusBuild + string(")");
	RPCPacket *pk=new RPCPacket(_this->address, (word)RPCPacket::responsePacketId, (byte*)version.c_str(), version.length(), _this->type, 0, true);
	return pk;
}
