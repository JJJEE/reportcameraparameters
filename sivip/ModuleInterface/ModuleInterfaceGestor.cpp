#include <ModuleInterface/ModuleInterfaceGestor.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>
#include <Sockets/SocketException.h>
#include <Utils/ServiceException.h>
#include <Utils/SerializedException.h>
#include <Utils/StrUtils.h>
#include <Exceptions/IdNotFoundException.h>
#include <Exceptions/InvalidParameterException.h>
#include <algorithm>
//DEBUG declaracions per WrkThrPool::line
#include <WorkerThreads/WorkerThreadPool.h>
#include <WorkerThreads/WorkerThreadPool>
#include <Module/Module.h>
#include <SessionManager/SessionManager>
#include <Module/CancelledCallException.h>

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

RPCPacket* ModuleInterfaceGestor::service(RPCPacket *inPkt, Address *a)
{
	STACKTRACE_INSTRUMENT();

	bool doDebug = false;
	int *debug = WorkerThreadPool<ModuleServiceThread>::getMyDebugPtr();
	doDebug = (debug != NULL);

	if(doDebug)
		*debug = 10;

	if (inPkt->id==(word)ModuleInterface::isAliveServiceId)
		return ModuleInterface::isAlive(this, a, inPkt->getData());
	else if (inPkt->id==(word)ModuleInterface::getVersionServiceId)
		return ModuleInterface::getVersion(this, a, inPkt->getData());
	else if (services==NULL || ((short)inPkt->id)<0 || inPkt->id>=numServices)
	{
		return NULL;
	}

	gestorList.rlock();

//	cout<<" MIG::service:"<<inPkt->id<<" srch gestor for: " << a->toString() <<endl;
	if(doDebug)
		*debug = 20;
	Sessio *ses=NULL;
	Gestor *g=NULL;
	
	try
	{
		ses = SessionManager<ModuleInterfaceGestor::Sessio>::retain(a->toString());
		g = ses->g;

		//if (it==gestors.end())
	}catch(IdNotFoundException nfe)
	{
		//		cout<<" MIG::service:"<<inPkt->id<<" not found gestor for: " << a->toString() << "--- trying sessionless " <<endl;
		try
		{
			try
			{
				gestorList.unlock();
				gestorList.wlock();
				if(find(sessionlessId.begin(), sessionlessId.end(), inPkt->id)==sessionlessId.end()) 
				{
					cout<<"MIG:Sessio no establerta id:"<<inPkt->id<<" for:"<<a->toString()<<endl;
					gestorList.unlock();
					SerializedException *se=getSessionNotStablished();

					if(se==NULL)
						return NULL;

					byte *sebytes=se->bytes();
					dword sesize=se->size();
					short porigen=inPkt->origen;

					//RPCPacket *p=new RPCPacket(address, (word)-2, se->bytes(), se->size(), type, p->origen, true);
					RPCPacket *p=new RPCPacket(address, (word)-2, sebytes, sesize, type, porigen, true);


					delete se;
					return p;
				}
				cout<<"MIG:new Gestor:"<<a->toString()<<" creats:"<<SessionManager<ModuleInterfaceGestor::Sessio>::numItems()<<" id:"<<inPkt->id<<" type:"<<type<<endl;
				list<int>::iterator iter;

				if(doDebug)
					*debug = 30;
				deleteGestors(0);
				while(SessionManager<ModuleInterfaceGestor::Sessio>::numItems()>limitGestors)
				{
					gestorList.unlock();
					usleep(5000);
					gestorList.wlock();
					deleteGestors();
				}

				g = createGestor(address, type, CanisGestors);
				ses = new Sessio();
				ses->g = g;
				if(doDebug)
					*debug = 40;
				try
				{
					SessionManager<ModuleInterfaceGestor::Sessio>::manage(a->toString(), ses, 2); //per la crida i per la sessio en si
				}catch(IdAlreadyInUseException e)
				{
					//ens l'han insertat al upgradar a l'wlock
					delete g;
					ses->g=NULL;
					delete ses;
					ses = SessionManager<ModuleInterfaceGestor::Sessio>::retain(a->toString());
					g = ses->g;
				}
			}
			catch(SocketException &e)
			{
				if(doDebug)
					*debug = 35;
				if(e.getCode()==2 || e.getMsg()==string("Unable to create socket"))
				{
					cout<<"MIG: \"Unable to create socket\" new Gestor:"<<a->toString()<<" creats:"<<SessionManager<ModuleInterfaceGestor::Sessio>::numItems()<<" id:"<<inPkt->id<<" type:"<<type<<endl;
					if(!deleteGestors())
					{
						cout<<" DecModInterface no gestor deleted, abort num:"<<SessionManager<ModuleInterfaceGestor::Sessio>::numItems()<<endl;
						abort();
					}
					g = createGestor(address, type, CanisGestors);
					ses = new Sessio();
					ses->g = g;
					if(doDebug)
						*debug = 45;
					SessionManager<ModuleInterfaceGestor::Sessio>::manage(a->toString(), ses, 2); //per la crida i per la sessio en si
				}
				else
				{
					cout <<"SocketException :"<<e.getCode()<<" msg:"<<e.getMsg()<<endl;
					//gestorList.unlock(true, "new gestor socket Exception");
					//e.serialize()->materializeAndThrow();
					throw;
				}
			}
		}
		catch(Exception &e)
		{
			cout <<"new gestor -- Exception :"<<e.getCode()<<" msg:"<<e.getMsg()<<endl;
			gestorList.unlock();
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
		catch(...)
		{
			cout <<"new gestor -- unknown exception"<<endl;
			gestorList.unlock();
			throw;
		}
	}
	catch(...)
	{
		gestorList.unlock();
		throw;
	}

	gestorList.unlock();
	
	if (g==NULL)
	{
		//cout<<" MIG g==NULL"<<endl;
		ServiceException e(0, string("Could not find or allocate Gestor for service request from ")+a->toString());
		SerializedException *se=e.serialize();
		
		byte *sebytes=se->bytes();
		dword sesize=se->size();
		short porigen=inPkt->origen;

		//RPCPacket *p=new RPCPacket(address, (word)-2, se->bytes(), se->size(), type, p->origen, true);
		RPCPacket *p=new RPCPacket(address, (word)-2, sebytes, sesize, type, porigen, true);

		delete se;
		return p;
	}

		
	
	RPCPacket *result=NULL;
//	cout<<" MIG g:"<<(void*)g<<endl;
	
	try
	{
		if(a!=NULL)
		{
//			cout<<" MIG: setkeepalive:"<<(void*)a<<endl;
//			cout<<"---"<<a->toString()<<endl;
			g->setKeepAliveAddress(a);
//			cout<<" MIG:time()"<<endl;
			ses->time();  // LRU :)
		}
//			cout<<" service[" << a->toString() <<"]:"<<inPkt->id<<endl;
		if(doDebug)
			*debug = 50;
		if(WorkerThreadPool<WorkerThread>::isCancelled())
		{
			//cout << __FILE__ << " line " << __LINE__ 
			//	<< "ModuleInterface::service() cancelled request" << endl;
			throw CancelledCallException("ModuleInterface::service");
		}else if (inPkt->id>=0 && inPkt->id<this->numServices) 
			result=services[inPkt->id].call(this, g, inPkt->getData());
		else
		{
			string excMsg=string("Out of range call id ") +
				StrUtils::decToString(inPkt->id) + string(" for this module.");
			cout << "ModuleInterface::service(): " << excMsg << endl;
			throw IdNotFoundException(excMsg);
		}
		if(doDebug)
			*debug = 60;

//			cout<<" /service[" << a->toString() <<"] -- " << (result==NULL?"null":StrUtils::decToString(result->id))<<endl;
	}
	catch (SocketException &e)
	{
		bool cancel = WorkerThreadPool<ModuleServiceThread>::isCancelled();
		if(!cancel)
			cout << "ModuleInterfaceGestor: Service.call exception " << __FILE__ << " line " << __LINE__<< " : "
					<< e.getClass() << "::" << e.getMsg() << " : " << a->toString() << endl;
		if(doDebug)
			*debug = 70;

		if(e.getCode()==2 || e.getMsg()==string("Unable to create socket"))
		{
			cout<<"MIG: \"Unable to create socket\" service.call, from:"<<a->toString()<<" creats:"<<SessionManager<ModuleInterfaceGestor::Sessio>::numItems()<<" id:"<<inPkt->id<<" type:"<<type<<endl;
			if(!deleteGestors())
			{
				cout<<" ModInterfaceiGest: no gestor deleted, abort num:"<<SessionManager<ModuleInterfaceGestor::Sessio>::numItems()<<endl;
				//abort();
				raise(SIGKILL);
			}
		}

//			cout<<" clearPluginInfo [" << a->toString() <<"]:"<<endl;
		if (g!=NULL && !cancel)
			g->clearPlgInfo();
		else if(g == NULL)
			cout << __FILE__ << " line " << __LINE__ 
				<< "ModuleInterface::service(): g is null" << endl;
		
		// I fem que el client doni timeout.
		// g->clearKeepAliveAddress();
//			cout<<" /clearPluginInfo [" << a->toString() <<"]:"<<endl;

//			e.serialize()->materializeAndThrow(true);
		

//			if(e.getCode()==2)
//			{//TODO: arreglar XD
//				deleteGestors(3); 
//				cout<<" retry service:"<<inPkt->id<<endl;
//				result=services[inPkt->id].call(this, g, inPkt->getData());
//				cout<<" /service"<<endl;
//			}
//			else
//			{
//				e.serialize()->materializeAndThrow(true);
//			}
	}
	catch (NotEnoughMemoryException &neme)
	{
		cout << __FILE__ << " line " << __LINE__<< ": " <<neme.getClass()<<": "<<neme.getMsg()<<": "<<a->toString()<<endl;
		
		gestorList.wlock();
		try{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(ses);
		}catch( Exception &e)
		{
				cout << "WARNING: SessionManager::release exception " << __FILE__ << " line " << __LINE__<<" : "
					<<e.getClass()<<"::"<<e.getMsg()<<" : "<<a->toString()<<endl;
		}
		gestorList.unlock();
		return NULL;
	}
	catch (InvalidParameterException &ipe)
	{
		gestorList.wlock();
		try{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(ses);
		}catch( Exception &e)
		{
				cout << "WARNING: SessionManager::release exception " << __FILE__ << " line " << __LINE__<<" : "
					<<e.getClass()<<"::"<<e.getMsg()<<" : "<<a->toString()<<endl;
		}
		gestorList.unlock();
		throw;
	}
	catch (CancelledCallException &cce)
	{
		gestorList.wlock();
		try{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(ses);
		}catch( Exception &e)
		{
				cout << "WARNING: SessionManager::release exception " << __FILE__ << " line " << __LINE__<<" : "
					<<e.getClass()<<"::"<<e.getMsg()<<" : "<<a->toString()<<endl;
		}
		gestorList.unlock();
		return NULL;
	}
	catch (Exception &e)
	{
		if(doDebug)
			*debug = 80;
		cout<<"Service Exception s:"<<inPkt->id<<":"<<e.getClass()<<":"<<e.getMsg()<<endl;

		gestorList.wlock();
		try{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(ses);
		}catch( Exception &e)
		{
				cout << "WARNING: SessionManager::release exception " << __FILE__ << " line " << __LINE__<<" : "
					<<e.getClass()<<"::"<<e.getMsg()<<" : "<<a->toString()<<endl;
		}
		gestorList.unlock();

		// Muntem un RPCPacket d'Excepcio
		SerializedException *se=e.serialize();

		byte *sebytes=se->bytes();
		dword sesize=se->size();
		short porigen=inPkt->origen;
		
		// TODO: Atencio, que passa amb el tipus desti? (ara 0)
		RPCPacket *p=new RPCPacket(this->address, (word)-2, sebytes,
			sesize, this->type, porigen, true);

		delete se;
		
		return p;
	}catch(...)
	{
		if(doDebug)
			*debug = 85;
		cout<<"unknown Exception:"<<__FILE__<<":"<<__LINE__<<endl;

		gestorList.wlock();
		try{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(ses);
		}catch( Exception &e)
		{
				cout << "WARNING: SessionManager::release exception " << __FILE__ << " line " << __LINE__<<" : "
					<<e.getClass()<<"::"<<e.getMsg()<<" : "<<a->toString()<<endl;
		}
		gestorList.unlock();
		throw;
	}

	if(inPkt->id ==  ModuleInterfaceGestor::endSessionServiceId)
	{
		gestorList.wlock();
		cout<<"MIG: end Session:"<<a->toString()<<" creats:"<<SessionManager<ModuleInterfaceGestor::Sessio>::numItems()<<endl;//" : "<<(void*)g<<" id:"<<inPkt->id<<endl;

		//el de l'StartSession, per alliverar-lo del tot "en condicions normals"
		try{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(ses);
		}catch( Exception &e)
		{
			cout << "WARNING: SessionManager::release exception " << __FILE__ << " line " << __LINE__<<" : "
				<<e.getClass()<<"::"<<e.getMsg()<<" : "<<a->toString()<<endl;
		}

		try{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(ses);
		}catch( Exception &e)
		{
			cout << "WARNING: SessionManager::release exception " << __FILE__ << " line " << __LINE__<<" : "
				<<e.getClass()<<"::"<<e.getMsg()<<" : "<<a->toString()<<endl;
		}
		deleteGestors(0);
		gestorList.unlock();
	}
	else
	{

		gestorList.wlock();
		try{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(ses);
		}catch( Exception &e)
		{
			cout << "WARNING: SessionManager::release exception " << __FILE__ << " line " << __LINE__<<" : "
				<<e.getClass()<<"::"<<e.getMsg()<<" : "<<a->toString()<<endl;
		}
		gestorList.unlock();
	}

	if(doDebug)
		*debug = 90;

	return result;
}

ModuleInterfaceGestor::ModuleInterfaceGestor(Address address, short type, Canis *cn, int startSessId, int endSessId) : ModuleInterface(address, type, cn), startSessionServiceId(startSessId), endSessionServiceId(endSessId), CanisGestors(cn), freeCanis(false), startSessionId(-100) //per evitar isAlives, getVersion i altres :P //gestorList(true), 
{
	if (cn==NULL)
	{
		CanisGestors=new Canis(address, type, true);
		freeCanis=true;
	}
}

ModuleInterfaceGestor::~ModuleInterfaceGestor()
{
	if (freeCanis)
		delete CanisGestors;

}


//COMPTE: cridar amb el wlock pillat :P
bool ModuleInterfaceGestor::deleteGestors(int dels)
{
	struct timeval now;
	gettimeofday(&now,NULL);

//	try
//	{
		if(SessionManager<ModuleInterfaceGestor::Sessio>::numItems()==0) // no hi ha gestors oberts ni sockets --> reiniciem :P
		{
			if(dels>0)
			{
				cout << "\n\n" << __FILE__ << " line " << __LINE__
					<< ": un puto exit!" << endl;
				exit(1); //DEBUG --  no hauria de passar mai...
			}
			return false;
		}

		string aux;
		list<string> toRelease;

		SessionManager<ModuleInterfaceGestor::Sessio>::Iterator it=SessionManager<ModuleInterfaceGestor::Sessio>::iterator();
		while (it.hasNext())
		{
			ModuleInterfaceGestor::Sessio *ses=it.next();

			if(ses->ended ||
					(now - (ses->creacio)).tv_sec > timeout)
			{
				cout<<"MIG: releasing "<<(ses->ended? string("ended"):string("timed out"))<<" gestor for:"<<it.getLastId()<<endl;
				toRelease.push_back(it.getLastId());
			}
			SessionManager<ModuleInterfaceGestor::Sessio>::releaseFromIterator(it.getLastId());
		}
		SessionManager<ModuleInterfaceGestor::Sessio>::releaseIterator();

		for(list<string>::iterator i = toRelease.begin();i!=toRelease.end();i++)
		{
			SessionManager<ModuleInterfaceGestor::Sessio>::release(*i);
		}

		if(dels<=0)
		{
			return true;
		}

		while(dels>=0)
		{
			Sessio *oldest=NULL;
			string aux;
			SessionManager<ModuleInterfaceGestor::Sessio>::Iterator it=SessionManager<ModuleInterfaceGestor::Sessio>::iterator();
			while (it.hasNext())
			{
				ModuleInterfaceGestor::Sessio *s=it.next();

				if(oldest == NULL || s->creacio < oldest->creacio)
				{
					oldest=s;
					aux=it.getLastId();
				}
				SessionManager<ModuleInterfaceGestor::Sessio>::releaseFromIterator(it.getLastId());
			}
			SessionManager<ModuleInterfaceGestor::Sessio>::releaseIterator();
			
			if(oldest == NULL || oldest->g==NULL) // 多? --> reiniciem :P
			{
				if(aux==string(""))
				{
					return false;
				}
			}

			dels--;
			cout<<"MIG: forcing releasie of oldest gestor for:"<<aux<<endl;
			SessionManager<ModuleInterfaceGestor::Sessio>::release(aux);
		}
		
//	}
//	catch( Exception &e)
//	{
//		gestorList.unlock(true, "DeleteGestor, 5");
//		e.serialize()->materializeAndThrow(true);
//	}
	return true;
}
/*
SerializedException* ModuleInterfaceGestor::getSessionNotStablished()
{
	 cout<<"SerializedException* ModuleInterfaceGestor::getSessionNotStablished()\n";
}
Gestor* ModuleInterfaceGestor::createGestor(Address orig, int type, Canis *cn)
{
	 cout<<"Gestor* ModuleInterfaceGestor::createGestor(Address orig, int type, Canis *cn)\n";
}
void ModuleInterfaceGestor::deleteGestor(Gestor *g)
{
	 cout<<"void ModuleInterfaceGestor::deleteGestor(Gestor *g)\n";
}
*/
