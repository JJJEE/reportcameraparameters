#ifndef __SIRIUS__BASE__MODULEINTERFACEGESTOR_H
#define __SIRIUS__BASE__MODULEINTERFACEGESTOR_H

#include <Utils/debugNew.h>
#include <ModuleInterface/ModuleInterface.h>
#include <Endian/Endian.h>
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
#include <Utils/Types.h>
#include <Plugins/Gestor.h>
#include <SessionManager/SessionManager.h>

#include <string>
#include <map>
#include <iostream>

using namespace std;

class ModuleInterfaceGestor;

typedef RPCPacket* (*DefaultModuleGestorCall)(ModuleInterfaceGestor*, Gestor*, void*);

class ModuleInterfaceGestor : public ModuleInterface
{
protected:
	static const int limitGestors=5000;//el treiem per proves :P // limit de gestors actius. generalment es queda sense sockets abans, peró a v egades no i porta problemes ^_^
	static const int timeout=180; //3 min :P
	int startSessionServiceId;
	int endSessionServiceId;

	class serviceDef
	{
		public:
			dword inSize, outSize;	// de moment no es fan servir...
			DefaultModuleGestorCall call;
	};

	class Sessio
	{
		public:
			Gestor *g;
			struct timeval creacio;
			bool ended;
			Sessio():g(NULL), ended(false)
			{
				gettimeofday(&creacio,NULL);
			}

			Sessio(Gestor* g, struct timeval creacio):g(g),  creacio(creacio), ended(false)
			{}

			~Sessio()
			{
				if(g != NULL)
				{
					delete g;
					g=NULL;
				}
			}

			void time()
			{
				gettimeofday(&creacio,NULL);
			}

			void end()
			{
				ended=true;
			}
	};

	serviceDef *services;

	list<int> sessionlessId;
	int startSessionId;
	RWLock gestorList;

	// No serveix: porta a inicialitzacions de canis per defecte => grup
	// d'IP no configurable
//	Canis CanisGestors;
	bool freeCanis;
	Canis *CanisGestors;

public:
	virtual RPCPacket* service(RPCPacket *inPkt, Address *a);
	virtual SerializedException* getSessionNotStablished() = 0;
	virtual Gestor* createGestor(Address orig, int type, Canis *cn=NULL) = 0;

	ModuleInterfaceGestor (Address address, short type, Canis *cn, int startSessId=0, int endSessId=1);
	
	~ModuleInterfaceGestor();

	//COMPTE: cridar amb el wlock pillat :P
	bool deleteGestors(int dels=5);
};

#endif
