#pragma once
#include <Utils/debugNew.h>
#include <string>
#include <Utils/Exception.h>

class SMSessionNotStablishedException : public Exception
{
 public:
   SMSessionNotStablishedException(string msg, int code=0);
   SMSessionNotStablishedException(int code, string msg);
   SMSessionNotStablishedException(SerializedException &se);
   ~SMSessionNotStablishedException();
   
   virtual string getClass();
};

