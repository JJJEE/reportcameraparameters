#pragma once

#include <Utils/debugNew.h>
#include <StreamingModule/StreamChannel.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>

class Streamer
{
protected:
	Address dstAddress; // Multicast o Unicast (UDP)
	StreamChannel *streamChannel;	// Canal d'streaming per on s'envia - Basicament es un RPC, per la segmentacio de paquets ja implementada
	
public:
	Address getDestination();
	void setDestination(Address addr);
	
	virtual void streamData(void *data, dword size) = 0;

};
