#include <StreamingModule/StreamChannelRPC.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

// Rep el primer paquet llegible sencer des d'un stream.
// *** Atencio, ara per ara assumeix que els paquets arriben en ordre. Res descabellat en una xarxa local...
// TODO 20060811: Caldria mirar d'afegir a l'RPCPacket una mena de numero de sequencia estil TCP, o un ID de paquet, per no haver de fer assumpcions aixi. Atencio pq caldria fer-ho en C++ i en Java.
RPCPacket *StreamChannelRPC::recievePacketSync()
{
	dword lastOffset=0;
	byte *assyBuf=NULL;
	dword nPkts=1, rcvPkts=0;
	dword currentAssyBufLen=0;
	
	while (rcvPkts<nPkts)
	{
		Address src;
		RPCPacket *slice=((RPC*)this)->receiveIndividualPacket(((Socket*)this->socket), &src); 
		
		if (slice->id!=RPCPacket::segmentedPacketId)
		{
			if (assyBuf!=NULL)
				delete [] assyBuf;
			return slice;
		}
				
		RPCPacket::segmentationInfo *segInfo=slice->extractSegmentationInfo();
		
		if (lastOffset<=segInfo->offset)
		{
			// Arriba un paquet nou!
			if (assyBuf!=NULL)
				delete [] assyBuf;
				
			currentAssyBufLen=segInfo->totalSize;
			assyBuf=new byte[segInfo->totalSize];
			rcvPkts=0;
		}
		
		nPkts=segInfo->nPackets;
		slice->addToAssembledPacketBuffer(assyBuf, currentAssyBufLen);
		rcvPkts++;
	}	
	
	RPCPacket *p=new RPCPacket(assyBuf);
	
	delete [] assyBuf;
	
	return p;
}

StreamChannelRPC::StreamChannelRPC(Address addr) : RPC(addr)
{
	this->addr=addr;
}

void StreamChannelRPC::sendData(void *data, dword size)
{
	Address srcAddr(IP("0.0.0.0"), 0);
	
	RPCPacket pk(srcAddr, 0, (byte*)data, size, 0, 0, true);
	
	sendPacket(*this->socket, pk);
}

dword StreamChannelRPC::receiveData(void **data)
{
	RPCPacket *p=recievePacketSync();
	
	byte *d=new byte[p->getSize()];
	memmove(d, p->getData(), p->getSize());
	
	(*data)=(void*)d;
	
	dword s=p->getSize();

	delete p;
	
	return s;
}
