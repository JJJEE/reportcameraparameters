#pragma once

#include <Utils/debugNew.h>
#include <Utils/RPC.h>
#include <StreamingModule/StreamChannel.h>

class StreamChannelRPC : public StreamChannel, protected RPC
{	
protected:
	// Rep el primer paquet llegible sencer des d'un stream.
	RPCPacket *recievePacketSync();
	
public:
	StreamChannelRPC(Address addr);
	
	virtual void sendData(void *data, dword size);

	virtual dword receiveData(void **data);
};
