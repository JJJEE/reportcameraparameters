#include <StreamingModule/SMSessionAlreadyStablishedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

SMSessionAlreadyStablishedException::SMSessionAlreadyStablishedException(string msg, int code): Exception(msg, code)
{

}

SMSessionAlreadyStablishedException::SMSessionAlreadyStablishedException(int code, string msg): Exception(msg, code)
{

}

SMSessionAlreadyStablishedException::SMSessionAlreadyStablishedException(SerializedException &se): Exception(se)
{

}

SMSessionAlreadyStablishedException::~SMSessionAlreadyStablishedException()
{
   
}

string SMSessionAlreadyStablishedException::getClass()
{
	string c=string("SMSessionAlreadyStablishedException");
	return c;
}

