/*
 *  StreamingModuleInterface.cpp
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <StreamingModule/StreamingModuleInterface.h>
#include <Utils/ServiceException.h>
#include <Utils/SerializedException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>

#include <StreamingModule/SMSessionAlreadyStablishedException.h>
#include <StreamingModule/SMSessionNotStablishedException.h>

#pragma mark *** Variables Estatiques
map<string,StreamingModuleInterface::sessionInfo> StreamingModuleInterface::sessions;

StreamingModuleInterface::StreamingModuleInterface(Address streamingModuleAddressUni, Address streamingModuleAddressMulti, short streamingModuleType)
{
	this->smAddrUni=streamingModuleAddressUni;
	this->smAddrMulti=streamingModuleAddressMulti;
	this->smType=streamingModuleType;

	memset(services, 0, sizeof(serviceDef)*StreamingModuleInterface::NUM_SERVICES);
	
	services[StreamingModuleInterface::startSessionServiceId].call=(StreamingModuleCall)StreamingModuleInterface::startSession;
	services[StreamingModuleInterface::endSessionServiceId].call=(StreamingModuleCall)StreamingModuleInterface::endSession;
	services[StreamingModuleInterface::getStreamListServiceId].call=(StreamingModuleCall)StreamingModuleInterface::getStreamList;
	services[StreamingModuleInterface::getStreamInfoServiceId].call=(StreamingModuleCall)StreamingModuleInterface::getStreamInfo;
	services[StreamingModuleInterface::startStreamServiceId].call=(StreamingModuleCall)StreamingModuleInterface::startStream;
	services[StreamingModuleInterface::stopStreamServiceId].call=(StreamingModuleCall)StreamingModuleInterface::stopStream;
	services[StreamingModuleInterface::getSpeedServiceId].call=(StreamingModuleCall)StreamingModuleInterface::getSpeed;
	
}

RPCPacket *StreamingModuleInterface::service(RPCPacket *inPkt, Address *a)
{
//	cout << "CentralDirectoryServiceWrapper::service()" << endl;
	if (((short)inPkt->id)<0 || inPkt->id>=NUM_SERVICES)
	{
		return NULL;
	}
	

	RPCPacket *result;
	
	try
	{
		result=services[inPkt->id].call(this, inPkt->getData(), a);
	}
	catch (Exception &e)
	{
	//	cout << "StreamingModuleInterface " << e.getClass() << ": " << e.getMsg() << endl;
		// Muntem un RPCPacket d'Excepcio
		SerializedException *se=e.serialize();
		if(e.getClass()==string("SocketTimeoutException"))
		{
	//		cout << " - StreamingModuleInterface STOExc -" << endl;
			return NULL;
		}
		// TODO: Atencio, que passa amb el tipus desti? (ara 0)
		RPCPacket *p=new RPCPacket(this->smAddrUni, (word)-2, se->bytes(), se->size(), this->smType,
				inPkt->origen, true);

		delete se;
		
		return p;
	}
	
	return result;
}

RPCPacket* StreamingModuleInterface::startSession(StreamingModuleInterface *_this, void *params, Address *a)
{

	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes!=sessions.end())
	{
		throw (SMSessionAlreadyStablishedException(0, string("Session already stablished for client ")+a->toString()));
	}
	
	sessionInfo sInfo;
	sessions[a->toString()]=sInfo;
		
	RPCPacket *p=new RPCPacket(_this->smAddrUni, (word)-1, NULL, 0, _this->smType, 0, true);
	
	return p;
}

RPCPacket* StreamingModuleInterface::endSession(StreamingModuleInterface *_this, void *params, Address *a)
{

	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
	{
		throw (SMSessionNotStablishedException(0, string("Session not stablished for client ")+a->toString()));
	}
	
	sessions.erase(itSes);
		
	RPCPacket *p=new RPCPacket(_this->smAddrUni, (word)-1, NULL, 0, _this->smType, 0, true);
	
	return p;
}

RPCPacket* StreamingModuleInterface::getStreamList(StreamingModuleInterface *_this, void *params, Address *a)
{
		
	RPCPacket *p=new RPCPacket(_this->smAddrUni, (word)-1, NULL, 0, _this->smType, 0, true);
	
	return p;
}

RPCPacket* StreamingModuleInterface::getStreamInfo(StreamingModuleInterface *_this, void *params, Address *a)
{
		
	RPCPacket *p=new RPCPacket(_this->smAddrUni, (word)-1, NULL, 0, _this->smType, 0, true);
	
	return p;
}

RPCPacket* StreamingModuleInterface::startStream(StreamingModuleInterface *_this, void *params, Address *a)
{
		
	RPCPacket *p=new RPCPacket(_this->smAddrUni, (word)-1, NULL, 0, _this->smType, 0, true);
	
	return p;
}

RPCPacket* StreamingModuleInterface::stopStream(StreamingModuleInterface *_this, void *params, Address *a)
{
		
	RPCPacket *p=new RPCPacket(_this->smAddrUni, (word)-1, NULL, 0, _this->smType, 0, true);
	
	return p;
}

RPCPacket* StreamingModuleInterface::getSpeed(StreamingModuleInterface *_this, void *params, Address *a)
{
		
	RPCPacket *p=new RPCPacket(_this->smAddrUni, (word)-1, NULL, 0, _this->smType, 0, true);
	
	return p;
}

RPCPacket* StreamingModuleInterface::setSpeed(StreamingModuleInterface *_this, void *params, Address *a)
{
		
	RPCPacket *p=new RPCPacket(_this->smAddrUni, (word)-1, NULL, 0, _this->smType, 0, true);
	
	return p;
}

