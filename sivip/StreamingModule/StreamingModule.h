/*
 *  StreamingModule.h
 *  
 *
 *  Created by David Marí Larrosa on 17/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>
#include <Threads/Thread.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>
#include <StreamingModule/StreamingModuleInterface.h>

class StreamingModule
{
	Address streamingModuleAddrUni, streamingModuleAddrMulti;
	short streamingModuleType;
	SocketUDP *serviceSocket;
	StreamingModuleInterface *smService;
	
public:
	StreamingModule(Address smAddrUni, Address smAddrMulti, short smType);
	StreamingModule(string xmlFile);

	void serve();
	
	Address getAddressUni();
	Address getAddressMulti();
	short getType();
};

class StreamingModuleServiceThread : public Thread
{

public:
	struct serviceArgs
	{
		RPCPacket *p;
		StreamingModuleInterface *smService;
		Address responseAddr;
	};

	StreamingModuleServiceThread();
	
	virtual void* execute(int id, void* params);

};

