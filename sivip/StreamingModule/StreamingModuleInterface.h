/*
 *  StreamingModuleInterface.h
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <Endian/Endian.h> 
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
#include <StreamingModule/Streamer.h> 
#include <string>
#include <map>
#include <list>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

using namespace std;

typedef RPCPacket* (*StreamingModuleCall)(void*, void*, Address*);

class StreamingModuleInterface
{
public:
	static const int NUM_SERVICES=8;

	static const int startSessionServiceId=0;
	static const int endSessionServiceId=1;
	static const int getStreamListServiceId=2;
	static const int getStreamInfoServiceId=3;
	static const int startStreamServiceId=4;
	static const int stopStreamServiceId=5;
	static const int getSpeedServiceId=6;
	static const int setSpeedServiceId=7;

protected:

	struct serviceDef
	{
		dword inSize, outSize;
		StreamingModuleCall call;
	};

	serviceDef services[StreamingModuleInterface::NUM_SERVICES];
  
	Address smAddrUni;
	Address smAddrMulti;
	short smType;
//	Cal alguna mena de llista de streams disponibles, amb la info que faci falta
//	map<string,GestorImage*> gestors;

	// Sessions
	struct sessionInfo
	{
		int dummy;
	};

	static map<string,sessionInfo> sessions;

	struct streamInfo
	{
		qword id;
		dword devId;
		dword type;
		
		dword nStreamers;
		// Array d'streamers on s'envien les dades. Poden ser multicast,
		// unicast o HTTP
		Streamer *streamers;
	};
	
	static list<streamInfo> streams;

public:
	// Tipus de dades dedicats a les diferents funcions
	static const dword streamTypeCamera=0;
	static const dword streamTypeRecording=1;
		
	// Inici de sessio per a que els creadors d'streams enviin la info al modul
	struct StartSessionParams
	{
		qword streamId;
		
		void toLocal(void *b)
		{
			byte *buf=(byte*)b;

			streamId=*((qword*)buf);
			Endian::from(Endian::xarxa, &streamId, sizeof(streamId));
			buf+=sizeof(qword);
		
		}
		
		void* toNetwork(dword *len=NULL)
		{
			if (len!=NULL)
				*len=this->size();

			byte *data=new byte[this->size()];
			byte *buf=data;

			*((qword*)buf)=streamId;
			Endian::to(Endian::xarxa, buf, sizeof(qword));
			buf+=sizeof(qword);
				
			return data;
		}
		
		dword size()
		{
			return sizeof(StartSessionParams);
		}
	};

	// Fi de sessio en la que els creadors d'streams envien la info al modul
	struct EndSessionParams
	{
		qword streamId;
		
		void toLocal(void *b)
		{
			byte *buf=(byte*)b;

			streamId=*((qword*)buf);
			Endian::from(Endian::xarxa, &streamId, sizeof(streamId));
			buf+=sizeof(qword);
		
		}
		
		void* toNetwork(dword *len=NULL)
		{
			if (len!=NULL)
				*len=this->size();

			byte *data=new byte[this->size()];
			byte *buf=data;

			*((qword*)buf)=streamId;
			Endian::to(Endian::xarxa, buf, sizeof(qword));
			buf+=sizeof(qword);
				
			return data;
		}
		
		dword size()
		{
			return sizeof(EndSessionParams);
		}
	};

	struct GetStreamListParams
	{
		struct Stream
		{
			dword devId;
			dword type;
			qword id;
			
			void toLocal(void *b)
			{
				byte *buf=(byte*)b;
	
				devId=*((dword*)buf);
				Endian::from(Endian::xarxa, &devId, sizeof(devId));
				buf+=sizeof(dword);
				
				type=*((dword*)buf);
				Endian::from(Endian::xarxa, &type, sizeof(type));
				buf+=sizeof(dword);
				
				id=*((qword*)buf);
				Endian::from(Endian::xarxa, &id, sizeof(id));
				buf+=sizeof(qword);
			}

			void* toNetwork(dword *len=NULL)
			{
				if (len!=NULL)
					*len=this->size();

				byte *data=new byte[this->size()];
				byte *buf=data;

				*((dword*)buf)=devId;
				Endian::to(Endian::xarxa, buf, sizeof(qword));
				buf+=sizeof(qword);
	
				*((dword*)buf)=type;
				Endian::to(Endian::xarxa, buf, sizeof(qword));
				buf+=sizeof(qword);
	
				*((qword*)buf)=id;
				Endian::to(Endian::xarxa, buf, sizeof(qword));
				buf+=sizeof(qword);
				
				return data;
			}
			
			dword size()
			{
				return sizeof(devId)+sizeof(type)+sizeof(id);
			}
		};
		
		dword devIdFilter;
		qword nStreams;
		Stream *streams;
		
		void toLocal(void *b)
		{
			if (streams!=NULL)
			{
				delete [] streams;
				streams=NULL;
			}
			
			byte *buf=(byte*)b;

			devIdFilter=*((dword*)buf);
			Endian::from(Endian::xarxa, &devIdFilter, sizeof(devIdFilter));
			buf+=sizeof(dword);
		
			nStreams=*((qword*)buf);
			Endian::from(Endian::xarxa, &nStreams, sizeof(nStreams));
			buf+=sizeof(qword);
		
			streams=new Stream[nStreams];
			for (qword s=0; s<nStreams; s++)
			{
				streams[s].toLocal(buf);
				buf+=streams[s].size();
			}
		}
		
		void* toNetwork(dword *len=NULL)
		{
			if (len!=NULL)
				*len=this->size();

			byte *data=new byte[this->size()];
			byte *buf=data;

			*((dword*)buf)=devIdFilter;
			Endian::to(Endian::xarxa, buf, sizeof(dword));
			buf+=sizeof(dword);
				
			*((qword*)buf)=nStreams;
			Endian::to(Endian::xarxa, buf, sizeof(qword));
			buf+=sizeof(qword);
			
			for (qword s=0; s<nStreams; s++)
			{
				dword len;
				void *b=streams[s].toNetwork(&len);
				memmove(buf, b, len);
				buf+=len;
				delete [] (byte*)b;
			}
				
			return data;
		}
		
		dword size()
		{
			return sizeof(devIdFilter)+sizeof(nStreams)+nStreams*sizeof(Stream);
		}
	};

	struct GetStreamInfoParams
	{
	  	struct RPCRequest
		{
			string ip;
			word port;
			qword id;
			
			void toLocal(void *b)
			{
				byte *buf=(byte*)b;
	
				byte iplen=*((byte*)buf);
				buf+=sizeof(byte);
				
				ip=string((char*)buf, (int)iplen);
				buf+=iplen;
				
				port=*((word*)buf);
				Endian::from(Endian::xarxa, &port, sizeof(port));
				buf+=sizeof(word);
				
				id=*((qword*)buf);
				Endian::from(Endian::xarxa, &id, sizeof(id));
				buf+=sizeof(qword);
			}

			void* toNetwork(dword *len=NULL)
			{
				if (len!=NULL)
					*len=this->size();

				byte *data=new byte[this->size()];
				byte *buf=data;

				*((byte*)buf)=(byte)ip.length();
				buf+=sizeof(byte);
				
				memmove(buf, ip.c_str(), (byte)ip.length());
				buf+=(byte)ip.length();
	
				*((word*)buf)=port;
				Endian::to(Endian::xarxa, buf, sizeof(word));
				buf+=sizeof(word);
	
				*((qword*)buf)=id;
				Endian::to(Endian::xarxa, buf, sizeof(qword));
				buf+=sizeof(qword);
				
				return data;
			}
			
			dword size()
			{
				return sizeof(byte)+ip.length()+sizeof(port)+sizeof(id);
			}
		};

	  	struct HTTPRequest
		{
			string url;
			
			void toLocal(void *b)
			{
				byte *buf=(byte*)b;
	
				word urllen=*((word*)buf);
				Endian::from(Endian::xarxa, &urllen, sizeof(urllen));
				buf+=sizeof(word);
				
				url=string((char*)buf, (int)urllen);
				buf+=urllen;
			}

			void* toNetwork(dword *len=NULL)
			{
				if (len!=NULL)
					*len=this->size();

				byte *data=new byte[this->size()];
				byte *buf=data;

				*((word*)buf)=(word)url.length();
				Endian::to(Endian::xarxa, buf, sizeof(word));
				buf+=sizeof(word);
				
				memmove(buf, url.c_str(), (byte)url.length());
				buf+=(byte)url.length();
	
				return data;
			}
			
			dword size()
			{
				return sizeof(word)+url.length();
			}
		};
		
		qword id;
		dword type;
		dword devId;
		
		// Request es on es pot demanar, tant en multicast com en unicast.
		// multicastStream indica on es pot rebre l'stream en multicast, si es
		// que s'esta emetent. (Si no, tots els valors son 0)
		RPCRequest request, multicastStream;
		HTTPRequest HTTPReq;
		
		void toLocal(void *b)
		{
			byte *buf=(byte*)b;

			id=*((qword*)id);
			Endian::from(Endian::xarxa, &id, sizeof(id));
			buf+=sizeof(qword);
		
			type=*((dword*)buf);
			Endian::from(Endian::xarxa, &type, sizeof(type));
			buf+=sizeof(dword);
		
			devId=*((dword*)buf);
			Endian::from(Endian::xarxa, &devId, sizeof(devId));
			buf+=sizeof(dword);
			
			request.toLocal(buf);
			buf+=request.size();
			
			multicastStream.toLocal(buf);
			buf+=multicastStream.size();
			
			HTTPReq.toLocal(buf);
			buf+=HTTPReq.size();
		}
		
		void* toNetwork(dword *len=NULL)
		{
			if (len!=NULL)
				*len=this->size();

			byte *data=new byte[this->size()];
			byte *buf=data;

			*((qword*)buf)=id;
			Endian::to(Endian::xarxa, buf, sizeof(qword));
			buf+=sizeof(qword);

			*((qword*)buf)=type;
			Endian::to(Endian::xarxa, buf, sizeof(qword));
			buf+=sizeof(qword);

			*((dword*)buf)=devId;
			Endian::to(Endian::xarxa, buf, sizeof(dword));
			buf+=sizeof(dword);
			
			dword tonetlen;
			void *b=request.toNetwork(&tonetlen);
			memmove(buf, b, tonetlen);
			delete [] (byte*)b;
			buf+=tonetlen;
			
			b=multicastStream.toNetwork(&tonetlen);
			memmove(buf, b, tonetlen);
			delete [] (byte*)b;
			buf+=tonetlen;
			
			b=HTTPReq.toNetwork(&tonetlen);
			memmove(buf, b, tonetlen);
			delete [] (byte*)b;
			buf+=tonetlen;
			
			return data;
		}
		
		dword size()
		{
			return sizeof(id)+sizeof(type)+sizeof(devId)+request.size()+multicastStream.size()+HTTPReq.size();
		}
	};
	
	// StartStream, nomes per streams unicast. Per multicast i HTTP no cal.
	struct StartStreamParams
	{
		qword streamId;
				
		void toLocal(void *b)
		{
			byte *buf=(byte*)b;

			streamId=*((qword*)buf);
			Endian::from(Endian::xarxa, &streamId, sizeof(streamId));
			buf+=sizeof(qword);
		
		}
		
		void* toNetwork(dword *len=NULL)
		{
			if (len!=NULL)
				*len=this->size();

			byte *data=new byte[this->size()];
			byte *buf=data;

			*((qword*)buf)=streamId;
			Endian::to(Endian::xarxa, buf, sizeof(qword));
			buf+=sizeof(qword);
				
			return data;
		}
		
		dword size()
		{
			return sizeof(StartStreamParams);
		}
	};

	struct StopStreamParams
	{
		qword streamId;
		
		void toLocal(void *b)
		{
			byte *buf=(byte*)b;

			streamId=*((qword*)buf);
			Endian::from(Endian::xarxa, &streamId, sizeof(streamId));
			buf+=sizeof(qword);
		
		}
		
		void* toNetwork(dword *len=NULL)
		{
			if (len!=NULL)
				*len=this->size();

			byte *data=new byte[this->size()];
			byte *buf=data;

			*((qword*)buf)=streamId;
			Endian::to(Endian::xarxa, buf, sizeof(qword));
			buf+=sizeof(qword);
				
			return data;
		}
		
		dword size()
		{
			return sizeof(StopStreamParams);
		}
	};
	
	struct GetSpeedParams
	{
		float speed;
		
		void toLocal(void *b)
		{
			byte *buf=(byte*)b;

			speed=*((float*)buf);
			Endian::from(Endian::xarxa, &speed, sizeof(speed));
			buf+=sizeof(float);
		
		}
		
		void* toNetwork(dword *len=NULL)
		{
			if (len!=NULL)
				*len=this->size();

			byte *data=new byte[this->size()];
			byte *buf=data;

			*((float*)buf)=speed;
			Endian::to(Endian::xarxa, buf, sizeof(float));
			buf+=sizeof(float);
				
			return data;
		}
		
		dword size()
		{
			return sizeof(GetSpeedParams);
		}
	};
	
	struct SetSpeedParams
	{
		float speed;
		
		void toLocal(void *b)
		{
			byte *buf=(byte*)b;

			speed=*((float*)buf);
			Endian::from(Endian::xarxa, &speed, sizeof(speed));
			buf+=sizeof(float);
		
		}
		
		void* toNetwork(dword *len=NULL)
		{
			if (len!=NULL)
				*len=this->size();

			byte *data=new byte[this->size()];
			byte *buf=data;

			*((float*)buf)=speed;
			Endian::to(Endian::xarxa, buf, sizeof(float));
			buf+=sizeof(float);
				
			return data;
		}
		
		dword size()
		{
			return sizeof(SetSpeedParams);
		}
	};
	
	

public:
	StreamingModuleInterface(Address streamingModuleAddressUni, Address streamingModuleAddressMulti, short streamingModuleType);

	RPCPacket* service(RPCPacket *inPkt, Address *a);

	static RPCPacket* startSession(StreamingModuleInterface *_this, void *params, Address *a);
	static RPCPacket* endSession(StreamingModuleInterface *_this, void *params, Address *a);
	static RPCPacket* getStreamList(StreamingModuleInterface *_this, void *params, Address *a);
	static RPCPacket* getStreamInfo(StreamingModuleInterface *_this, void *params, Address *a);
	static RPCPacket* startStream(StreamingModuleInterface *_this, void *params, Address *a);
	static RPCPacket* stopStream(StreamingModuleInterface *_this, void *params, Address *a);
	static RPCPacket* getSpeed(StreamingModuleInterface *_this, void *params, Address *a);
	static RPCPacket* setSpeed(StreamingModuleInterface *_this, void *params, Address *a);
	
};

