/*
 *  StreamingModule.cpp
 *  
 *
 *  Created by David Marí Larrosa on 17/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "StreamingModule.h"
#include <Utils/Exception.h>
#include <XML/xmlNode.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

// Servei de modul de descodificacio que fa servir l'interface
StreamingModule::StreamingModule(Address smAddr, Address smAddrMulti, short smType) : streamingModuleAddrUni(smAddr), streamingModuleAddrMulti(smAddrMulti), streamingModuleType(smType)
{
//	cout << "serviceSocket: " << this->centralDirectoryAddr.toString() << endl;
	serviceSocket=new SocketUDP(this->streamingModuleAddrUni, SOCK_SERVE);
	smService=new StreamingModuleInterface(streamingModuleAddrUni, streamingModuleAddrMulti, streamingModuleType);
}

// Servei de modul de descodificacio que fa servir l'interface, inicialitzat mitjançant un XML
StreamingModule::StreamingModule(string xmlFile)
{
// : streamingModuleAddr(smAddr), streamingModuleType(smType)
//	cout << "serviceSocket: " << this->centralDirectoryAddr.toString() << endl;

	FILE *f=fopen(xmlFile.c_str(),"rb");

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char *buf=new char[len];
	
	if (buf==NULL)
	{
		throw (Exception(0, "Not enough memory to read configuration file"));
	}
		
	fread(buf, len, 1, f);
	fclose(f);
	
	string xmlConts(buf, len);
	delete [] buf;
	XML *config=xmlParser::parse(xmlConts);
	
	xmlNode *n;
	
	n = config->getNode("/StreamingModuleConfig/Service/IP");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file"));
	}
	
	IP ip(n->getCdata());	

	n = config->getNode("/StreamingModuleConfig/Service/MulticastIPGroup");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file"));
	}
	
	IP ipMulti(n->getCdata());	

	n = config->getNode("/StreamingModuleConfig/Service/Port");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file"));
	}
	
	int port = atoi(n->getCdata().c_str());
	streamingModuleAddrUni=Address(ip, port);
	streamingModuleAddrMulti=Address(ipMulti, port);

	n = config->getNode("/StreamingModuleConfig/Service/SubsystemType");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file"));
	}
	
	streamingModuleType=atoi(n->getCdata().c_str());

	delete config;

	serviceSocket=new SocketUDP(this->streamingModuleAddrUni, SOCK_SERVE);
	smService=new StreamingModuleInterface(streamingModuleAddrUni, streamingModuleAddrMulti, streamingModuleType);

}

void StreamingModule::serve()
{
//	cout << "execute()" << endl;

	RPCPacket *p;
//	byte hdrBuf[RPCPacket::hdrSize];
//	byte *pktBuf;
	Address responseAddr(IP("0.0.0.0"), 0);
	StreamingModuleServiceThread th;
	StreamingModuleServiceThread::serviceArgs *sa;
		
serveStart:

	try
	{
//		cout << "pre-while" << endl;
		while (p=RPC::receivePacket(serviceSocket, &responseAddr))
		//serviceSocket->peek(hdrBuf, RPCPacket::hdrSize)==RPCPacket::hdrSize)
		{
//			cout << "received" << endl;
//			p=NULL;
//			pktBuf=NULL;
			
/*			p=new RPCPacket(hdrBuf);
			pktBuf=new byte[RPCPacket::hdrSize+p->getSize()];

			serviceSocket->read(pktBuf, RPCPacket::hdrSize+p->getSize(), &responseAddr);
			p->setData(pktBuf+RPCPacket::hdrSize, true);

			delete [] pktBuf;
*/

			
			
			sa=new StreamingModuleServiceThread::serviceArgs;
			sa->p=p;	// L'ha de alliberar el thread
			sa->smService=smService;
			sa->responseAddr=responseAddr;
			
			int id=th.start(sa);
			th.detach(id);
		}
	}
	catch (SocketException &se)
	{
		cout << se.getClass() << ": " << se.getMsg() << endl;
	}
	catch (Exception &e)
	{
		cout << "Recoverable " << e.getClass() << ": " << e.getMsg() << endl;
		// Aqui responseAddr hauria de tenir un valor valid
		if (responseAddr.getPort()==0)
		{
			cout << "Impossible to recover, quitting..." << endl;

			return;
		}

//		if (response!=NULL)
//			delete response;
			
//		SocketUDP responseSocket(responseAddr);
		
		// Muntem un RPCPacket d'Excepcio
		SerializedException *se=e.serialize();
		
		// TODO: Atencio, que passa amb el tipus desti? (ara 0)
		RPCPacket *response=new RPCPacket(this->streamingModuleAddrUni, (word)-2, se->bytes(), se->size(), this->streamingModuleType,
				p->origen);

		RPC::sendResponse(*response, &responseAddr);
//		int n=responseSocket.write(response->packet(), RPCPacket::hdrSize+response->getSize());
		delete se;
		delete response;
		
		if (p!=NULL)
			delete p;
//		if (pktBuf!=NULL)
//			delete [] pktBuf;
			
		cout << "Recovered" << endl;
		goto serveStart;
	}


//	cout << "peteit execute" << endl;
}


Address StreamingModule::getAddressUni()
{
	return this->streamingModuleAddrUni;
}

Address StreamingModule::getAddressMulti()
{
	return this->streamingModuleAddrMulti;
}

short StreamingModule::getType()
{
	return this->streamingModuleType;
}


// StreamingModuleServiceThread
#pragma mark *** StreamingModuleServiceThread

StreamingModuleServiceThread::StreamingModuleServiceThread()
{
}

void* StreamingModuleServiceThread::execute(int id, void* params)
{
	serviceArgs *sa=(serviceArgs *)params;

//			cout << "smService->service(p)" << endl;

	RPCPacket *response=sa->smService->service(sa->p, &sa->responseAddr);

			// cout  << "smService->service(p, " << sa->responseAddr.toString() << ")" << endl;
//			cout << "returned" << endl;

			// cout  << "responseAddr " << sa->responseAddr.toString() << endl;
//	SocketUDP responseSocket(sa->responseAddr);

//	byte *resBuf=response->packet();
			
//	int n=responseSocket.write(resBuf, RPCPacket::hdrSize+response->getSize());
//			cout << "wrote " << n <<  endl;

	if(response!=NULL)
		RPC::sendResponse(*response, &sa->responseAddr);

//	delete resBuf;
	delete response;
	delete sa->p;
	delete sa;

}

