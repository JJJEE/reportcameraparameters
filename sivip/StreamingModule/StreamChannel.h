#pragma once

#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Sockets/Socket.h>

class StreamChannel
{
protected:
	Address addr;
	
public:

	virtual void sendData(void *data, dword size) = 0;

	virtual dword receiveData(void **data) = 0;
};
