#include <StreamingModule/SMSessionNotStablishedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

SMSessionNotStablishedException::SMSessionNotStablishedException(string msg, int code): Exception(msg, code)
{

}

SMSessionNotStablishedException::SMSessionNotStablishedException(int code, string msg): Exception(msg, code)
{

}

SMSessionNotStablishedException::SMSessionNotStablishedException(SerializedException &se): Exception(se)
{

}

SMSessionNotStablishedException::~SMSessionNotStablishedException()
{
   
}

string SMSessionNotStablishedException::getClass()
{
	string c=string("SMSessionNotStablishedException");
	return c;
}

