#pragma once

#include <Utils/debugNew.h>
#include <StreamingModule/Streamer.h>

class StreamerMulticast : public Streamer
{

public:
	StreamerMulticast(Address group);
	~StreamerMulticast();

	virtual void streamData(void *data, dword size);
	
};
