#pragma once
#include <Utils/debugNew.h>
#include <string>
#include <Utils/Exception.h>

class SMSessionAlreadyStablishedException : public Exception
{
 public:
   SMSessionAlreadyStablishedException(string msg, int code=0);
   SMSessionAlreadyStablishedException(int code, string msg);
   SMSessionAlreadyStablishedException(SerializedException &se);
   ~SMSessionAlreadyStablishedException();
   
   virtual string getClass();
};

