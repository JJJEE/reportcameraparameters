#pragma once

#include <Utils/debugNew.h>
#include <StreamingModule/Streamer.h>

// Atencio cal considerar subclasses per fer unicast per RPC o per HTTP!!

class StreamerUnicast : public Streamer
{

public:
	StreamerUnicast();
	StreamerUnicast(Address dst);
	~StreamerUnicast();

	virtual void streamData(void *data, dword size);
	
};
