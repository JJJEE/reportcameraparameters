#include <StreamingModule/StreamerMulticast.h>
#include <StreamingModule/StreamChannelRPC.h>

StreamerMulticast::StreamerMulticast(Address group)
{
	if (!group.getIP().isMulticast())
		throw Exception("Destination address is not a multicast group");
		
	setDestination(group);
	StreamChannelRPC *scm=new StreamChannelRPC(group);
	streamChannel=scm;
}

StreamerMulticast::~StreamerMulticast()
{
	delete (StreamChannelRPC*)streamChannel;
}

void StreamerMulticast::streamData(void *data, dword size)
{
	streamChannel->sendData(data, size);
}
	