#ifndef __MARINA_EYE_CAM__BASE__QUEUE_H
#define __MARINA_EYE_CAM__BASE__QUEUE_H

#include <Exceptions/IdAlreadyInUseException.h>
#include <Exceptions/IdNotFoundException.h>
#include <Utils/debugNew.h>
#include <Utils/Types.h>
#include <Threads/Condition.h>
#include <list>
#include <iostream>

using namespace std;

template <class T> class Queue
{
protected:
	Condition lNotEmpty;
	list<T> l;

public:
	Queue();
	~Queue();
	
	T get();
	void queue(T el);
	int size();	
};

template <class T> Queue<T>::Queue()
{

};

template <class T> Queue<T>::~Queue()
{
	
};

template <class T> T Queue<T>::get()
{
	T elem=NULL;
	this->lNotEmpty.lock();
	while (this->l.size()==0)		// Esperem a que no estigui buida :P
	{
		this->lNotEmpty.wait();
	}
	elem=this->l.front();
	this->l.pop_front();
	this->lNotEmpty.unlock();
	
	return elem;	
};

template <class T> void Queue<T>::queue(T el)
{
	this->lNotEmpty.lock();
	this->l.push_back(el);
	this->lNotEmpty.signal();
	this->lNotEmpty.unlock();
};

template <class T> int Queue<T>::size()
{
	this->lNotEmpty.lock();
	int res=this->l.size();
	this->lNotEmpty.unlock();
	return res;
};
#endif
