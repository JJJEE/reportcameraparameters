/*
 *  UnsupportedDataTypeException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_BASE_EXCEPTIONS_UNSUPPORTEDDATATYPEEXCEPTION_H_
#define SIRIUS_BASE_EXCEPTIONS_UNSUPPORTEDDATATYPEEXCEPTION_H_


#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class UnsupportedDataTypeException : public Exception
{
 public:
	UnsupportedDataTypeException(string msg, int code=0);
	UnsupportedDataTypeException(int code, string msg);
	UnsupportedDataTypeException(SerializedException &se);

	~UnsupportedDataTypeException();
	
	virtual string getClass();
};
#endif
