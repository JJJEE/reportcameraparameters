/*
 *  UnsupportedContentTypeException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <exceptions/UnsupportedContentTypeException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

UnsupportedContentTypeException::UnsupportedContentTypeException(string msg, int code): Exception(msg, code)
{

}

UnsupportedContentTypeException::UnsupportedContentTypeException(int code, string msg): Exception(msg, code)
{

}

UnsupportedContentTypeException::UnsupportedContentTypeException(SerializedException &se): Exception(se)
{

}

UnsupportedContentTypeException::~UnsupportedContentTypeException()
{
   
}

string UnsupportedContentTypeException::getClass()
{
	string c=string("UnsupportedContentTypeException");
	return c;
}

