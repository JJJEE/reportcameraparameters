/*
 *  DeprecatedOperationException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <exceptions/DeprecatedOperationException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

DeprecatedOperationException::DeprecatedOperationException(string msg, int code): Exception(msg, code)
{

}

DeprecatedOperationException::DeprecatedOperationException(int code, string msg): Exception(msg, code)
{

}

DeprecatedOperationException::DeprecatedOperationException(SerializedException &se): Exception(se)
{

}

DeprecatedOperationException::~DeprecatedOperationException()
{
   
}

string DeprecatedOperationException::getClass()
{
	string c=string("DeprecatedOperationException");
	return c;
}

