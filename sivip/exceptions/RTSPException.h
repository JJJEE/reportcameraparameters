/*
 *  RTSPException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_BASE_EXCEPTIONS_RTSPEXCEPTION_H_
#define SIRIUS_BASE_EXCEPTIONS_RTSPEXCEPTION_H_

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class RTSPException : public Exception
{
 public:
	RTSPException(string msg, int code=0);
	RTSPException(int code, string msg);
	RTSPException(SerializedException &se);

	~RTSPException();
	
	virtual string getClass();
};
#endif
