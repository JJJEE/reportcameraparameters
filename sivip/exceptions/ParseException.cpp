/*
 *  ParseException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <exceptions/ParseException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

ParseException::ParseException(string msg, int code): Exception(msg, code)
{

}

ParseException::ParseException(int code, string msg): Exception(msg, code)
{

}

ParseException::ParseException(SerializedException &se): Exception(se)
{

}

ParseException::~ParseException()
{
   
}

string ParseException::getClass()
{
	string c=string("ParseException");
	return c;
}

