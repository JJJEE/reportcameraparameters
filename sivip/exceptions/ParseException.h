/*
 *  ParseException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_BASE_EXCEPTIONS_PARSEEXCEPTION_H_
#define SIRIUS_BASE_EXCEPTIONS_PARSEEXCEPTION_H_

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class ParseException : public Exception
{
 public:
	ParseException(string msg, int code=0);
	ParseException(int code, string msg);
	ParseException(SerializedException &se);

	~ParseException();
	
	virtual string getClass();
};
#endif
