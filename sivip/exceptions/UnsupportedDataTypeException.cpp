/*
 *  UnsupportedDataTypeException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <exceptions/UnsupportedDataTypeException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

UnsupportedDataTypeException::UnsupportedDataTypeException(string msg, int code): Exception(msg, code)
{

}

UnsupportedDataTypeException::UnsupportedDataTypeException(int code, string msg): Exception(msg, code)
{

}

UnsupportedDataTypeException::UnsupportedDataTypeException(SerializedException &se): Exception(se)
{

}

UnsupportedDataTypeException::~UnsupportedDataTypeException()
{
   
}

string UnsupportedDataTypeException::getClass()
{
	string c=string("UnsupportedDataTypeException");
	return c;
}

