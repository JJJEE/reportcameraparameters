/*
 *  RTSPException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <exceptions/RTSPException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

RTSPException::RTSPException(string msg, int code): Exception(msg, code)
{

}

RTSPException::RTSPException(int code, string msg): Exception(msg, code)
{

}

RTSPException::RTSPException(SerializedException &se): Exception(se)
{

}

RTSPException::~RTSPException()
{
   
}

string RTSPException::getClass()
{
	string c=string("RTSPException");
	return c;
}

