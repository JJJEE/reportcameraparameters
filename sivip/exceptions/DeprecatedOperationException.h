/*
 *  DeprecatedOperationException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_BASE_EXCEPTIONS_DEPRECATEDOPERATIONEXCEPTION_H_
#define SIRIUS_BASE_EXCEPTIONS_DEPRECATEDOPERATIONEXCEPTION_H_

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class DeprecatedOperationException : public Exception
{
 public:
	DeprecatedOperationException(string msg, int code=0);
	DeprecatedOperationException(int code, string msg);
	DeprecatedOperationException(SerializedException &se);

	~DeprecatedOperationException();
	
	virtual string getClass();
};
#endif
