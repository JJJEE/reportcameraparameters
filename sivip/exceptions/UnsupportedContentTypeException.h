/*
 *  UnsupportedContentTypeException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_BASE_EXCEPTIONS_UNSUPPORTEDCONTENTTYPEEXCEPTION_H_
#define SIRIUS_BASE_EXCEPTIONS_UNSUPPORTEDCONTENTTYPEEXCEPTION_H_

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class UnsupportedContentTypeException : public Exception
{
 public:
	UnsupportedContentTypeException(string msg, int code=0);
	UnsupportedContentTypeException(int code, string msg);
	UnsupportedContentTypeException(SerializedException &se);

	~UnsupportedContentTypeException();
	
	virtual string getClass();
};
#endif
