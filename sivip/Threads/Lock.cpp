#include <Threads/Lock.h>
#include <stdlib.h>

#pragma mark *** LockInfo

LockInfo::LockInfo(Lock *l, bool isWriteLock) : unlockInstant(),
	unlockStackTrace(NULL)
{
	STACKTRACE_INSTRUMENT();
	
	this->lockId=Lock::getNextLockId();
	this->lockInstant=Lock::getInstant();
	this->lock=l;
	this->isWLock=isWriteLock;
	this->lockStackTrace=STACKTRACE_METOSTRING();
	this->lockThread=pthread_self();
}

LockInfo::~LockInfo()
{
	STACKTRACE_INSTRUMENT();
	if (this->lockStackTrace!=NULL)
		delete [] this->lockStackTrace;
	
	this->lockStackTrace=NULL;

	if (this->unlockStackTrace!=NULL)
		delete [] this->unlockStackTrace;
	
	this->unlockStackTrace=NULL;
}

Lock* LockInfo::getLock()
{
	STACKTRACE_INSTRUMENT();
	
	return this->lock;
}

dword LockInfo::getLockId()
{
	STACKTRACE_INSTRUMENT();
	
	return this->lockId;
}

pthread_t LockInfo::getLockThread()
{
	STACKTRACE_INSTRUMENT();
	
	return this->lockThread;
}

char *LockInfo::getLockStackTrace()
{
	STACKTRACE_INSTRUMENT();
	
	return this->lockStackTrace;
}

char *LockInfo::getUnlockStackTrace()
{
	STACKTRACE_INSTRUMENT();
	
	return this->unlockStackTrace;
}

bool LockInfo::isLocked()
{
	STACKTRACE_INSTRUMENT();
	
	// La primera condicio no cal
	return (this->lockInstant.getInstantSeconds()!=0.0
		&& this->unlockInstant.getInstantSeconds()==0.0);
}

string LockInfo::getLockInstant()
{
	STACKTRACE_INSTRUMENT();
	
	return this->lockInstant.getInstant();
}

string LockInfo::getUnlockInstant()
{
	STACKTRACE_INSTRUMENT();
	
	return this->unlockInstant.getInstant();
}

void LockInfo::processUnlock()
{
	STACKTRACE_INSTRUMENT();
	
	this->unlockInstant=Lock::getInstant();
	this->unlockStackTrace=STACKTRACE_METOSTRING();
}

#pragma mark *** Lock
#pragma mark *** Estatiques
bool Lock::inited=false;
bool Lock::initing=false;
dword Lock::nextLockId=0;
pthread_mutex_t Lock::genIdLock;
pthread_mutex_t Lock::locksByThreadLock;
Timer Lock::timer(25, true);
map<pthread_t, list<LockInfo*> > Lock::locksByThread;

Lock::Lock()
{
	STACKTRACE_INSTRUMENT();
	
	if (!Lock::inited && !Lock::initing)
	{
		// Inicialitzem. Com que no tenim mecanisme per gestionar la
		// concurrencia, ho fem a lo cutre.
		Lock::initing=true;
		
		pthread_mutexattr_t a;
		pthread_mutexattr_init(&a);
		pthread_mutexattr_settype(&a, PTHREAD_MUTEX_RECURSIVE);
		if (pthread_mutex_init(&Lock::genIdLock, &a)!=0) 
		{
			pthread_mutexattr_destroy(&a);
			cerr << "[" << __FILE__ << ", line " << __LINE__ << 
				"] FATAL: Could not init genIdLock" << endl;
			
			STACKTRACE_DUMP();
			exit(-1);
		}
		
		pthread_mutexattr_destroy(&a);
		
		pthread_mutexattr_init(&a);
		pthread_mutexattr_settype(&a, PTHREAD_MUTEX_RECURSIVE);
		if (pthread_mutex_init(&Lock::locksByThreadLock, &a)!=0) 
		{
			pthread_mutexattr_destroy(&a);
			cerr << "[" << __FILE__ << ", line " << __LINE__ << 
				"] FATAL: Could not init locksByThreadLock" << endl;
			
			STACKTRACE_DUMP();
			exit(-1);
		}
		
		pthread_mutexattr_destroy(&a);
		
		Lock::inited=true;
		Lock::initing=false;
	}
	
	// Espera activa a que el que esta inicialitzant ho faci
	while (initing);
}

Lock::~Lock()
{
	STACKTRACE_INSTRUMENT();
	
	// No destruim mai els mutex pq caldria portar un comptatge de referencies
	// i ens caldria saber numero de referencies, etc. que no podem obtenir
	// de manera fiable sense mecanismes de sync.
}

dword Lock::getNextLockId()
{
	STACKTRACE_INSTRUMENT();
	
	dword nextId=0;
	int err=pthread_mutex_lock(&Lock::genIdLock);
	if (err!=0)
	{
		cerr << "[" << __FILE__ << ", line " << __LINE__ << 
			"] FATAL: Could not lock genIdLock" << endl;
			
		STACKTRACE_DUMP();
		exit(-1);
	}
	nextId=Lock::nextLockId;
	Lock::nextLockId++;
	
	pthread_mutex_unlock(&Lock::genIdLock);
	
	return nextId;
}

TimerInstant Lock::getInstant()
{
	STACKTRACE_INSTRUMENT();
	
	return Lock::timer.time();
}

void Lock::processLock(bool isWrite)
{
	STACKTRACE_INSTRUMENT();
}

void Lock::processUnlock()
{
	STACKTRACE_INSTRUMENT();
}

