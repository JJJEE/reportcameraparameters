#ifndef SIRIUS_THREADS_CONDITION_H_
#define SIRIUS_THREADS_CONDITION_H_

#include <Threads/Thread.h>
#include <Utils/debugNew.h>

#ifdef WIN32
#include <time.h>
#include <sys/timeb.h>
#include <sys/types.h>


#ifndef ETIMEDOUT
#define ETIMEDOUT 60
#endif

#endif

class Condition : public Mutex
{
private:
	class condInfo
	{
		public:
			int refCount;
			pthread_cond_t *c;
			condInfo():refCount(1), c(NULL){}
	};
	condInfo *ci;

	void deleteCond();

public:
	/*
	 * Us:
	 * c.lock();
	 * while(condició
	 *		c.wait();
	 * c.unlock();
	 */


	Condition();
	Condition(Mutex m);
	Condition(const Condition &c2);
	Condition & operator=(const Condition& c);
	~Condition();

	void wait();
	bool wait(struct timespec t);
	void signal(); //desbloquejem un Thread
	void broadcast(); //desbloquejem tots
};
#endif
