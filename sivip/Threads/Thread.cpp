#include <Threads/Thread.h>
#include <Utils/Exception.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <stdio.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <signal.h>
#include <Threads/pthread_ops.h>

#ifdef THREAD_TRACE_DEBUG
#include <Utils/Log.h>
#endif

int Thread::minPrio=-1;
int Thread::maxPrio=-1;
int Thread::defaultPrio=-1;

void Thread::initPrio(pthread_t pthread)
{
	STACKTRACE_INSTRUMENT();
	
//	if(minPrio==-1 && maxPrio==-1 && defaultPrio==-1)
//	{
//		int policy;
//		sched_param param;
//		int res=pthread_getschedparam(pthread, &policy, &param); 
//   		defaultPrio = param.sched_priority;
//		maxPrio=sched_get_priority_max(policy);
//		minPrio=sched_get_priority_min(policy);
//		cout<<" thread::initPrio policy:"<<policy<<" : "<<minPrio<<" - "<<defaultPrio<<" - "<<maxPrio<<endl;
//	}
}

Thread::Thread()
{
	STACKTRACE_INSTRUMENT();
	
	tInf=new threadsInfo();

//	initPrio(getId());

	newPrio=defaultPrio;
	if (tInf==NULL)
	   cout << "Thread::Thread(): tInf NULL!!!!" << endl;

	tInf->lastStarted=-1;
	this->threadName=string("Thread");
}

Thread::~Thread()
{
	STACKTRACE_INSTRUMENT();
	
//	cout<<"Thread::~Thread"<<endl;
	tInf->mllista.lock();
//	cout<<"Thread::locked"<<endl;
	int size=tInf->lthreads.size();
//	cout<<"Thread::size:"<<size<<endl;
	for(int i=0;i<size;i++)
	{
		if(tInf->lthreads[i].estat==Thread::StateRunning)
		{
			pthread_detach(tInf->lthreads[i].pthread);
			tInf->lthreads[i].estat=Thread::StateDetached;
		}
	}
	tInf->refCount--;
	if(tInf->refCount==0)
	{
//		cout<<" petant:"<<(void*)tInf<<endl;
		tInf->mllista.unlock();
		delete tInf;
	}
	else
	{
//		cout<<" no petem:"<<(void*)tInf<<" refs:"<<tInf->refCount<<endl;
		tInf->mllista.unlock();
	}

//	cout<<"Thread::/~Thread"<<endl;
}

void Thread::setName(string name)
{
	STACKTRACE_INSTRUMENT();
	
	this->threadName=name;
}

string Thread::getName()
{
	STACKTRACE_INSTRUMENT();
	
	return this->threadName;
}

pthread_t Thread::getId()
{
	// 20090507: Atencio, perque STACKTRACE fa servier el getId => no es pot
	// fer un STACKTRACE aqui.
	// STACKTRACE_INSTRUMENT();
	
	return pthread_self();
}

pthread_t Thread::getId(int id)
{
	STACKTRACE_INSTRUMENT();
	
	if(id>=0 && id<tInf->lthreads.size() )
	{
		tInf->mllista.lock();
		pthread_t res = tInf->lthreads[id].pthread;
		tInf->mllista.unlock();
		return res;
	}
	return pthread_t();
}

bool Thread::currentThreadEqualsPthread(pthread_t t)
{
	return (pthread_equal(Thread::getId(), t)!=0);
}

int Thread::start(void *args, OnException e, bool debug )
{
	STACKTRACE_INSTRUMENT();
	string debugStr("");
	
	if(debug)
	{
		debugStr = string( "[") + StrUtils::decToString(toLong(Thread::getId())) + string("] Thread::start()" );
		cout<<debugStr<<endl;
	}
	struct thread* t=new thread(Thread::StateRunning);
	
	if (t==NULL)
		throw NotEnoughMemoryException("Not enough memory to allocate thread structures");

	pthread_attr_t attr;
	
	pthread_attr_init(&attr);

	size_t sSize;
//	if(debug)
//		cout << "["<<toVoidPtr(Thread::getId())<<"] Thread::start pthread_attr_getstacksize" << endl;
	pthread_attr_getstacksize(&attr, &sSize);

	sSize+=sSize/2;
//	if(debug)
//		cout << "["<<toVoidPtr(Thread::getId())<<"]Thread::start pthread_attr_setstacksize" << endl;
	if (pthread_attr_setstacksize(&attr, sSize)<0)
		cout << "["<<toVoidPtr(Thread::getId())<<"]Que no me seto la stack a " << sSize << " y la dejo en " << sSize*2/3 << endl;

//	if(debug)
//		cout << "["<<toVoidPtr(Thread::getId())<<"]Thread::start pthread_attr_setdetach" << endl;
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	if(debug)
	{
		debugStr = string( "[") + StrUtils::decToString(toLong(Thread::getId())) + string("]----> Thread::start() preLock" );
		cout<<debugStr<<endl;
	}
	tInf->mllista.lock();
	if(debug)
	{
		debugStr = string( "[") + StrUtils::decToString(toLong(Thread::getId())) + string("]----> Thread::start() locked" );
		cout<<debugStr<<endl;
	}
	int id;
	
	for(id=0;id<tInf->lthreads.size();id++)
	{
		if(tInf->lthreads[id].estat==Thread::StateFree)
		{
//			if(debug)
//				cout << "["<<toVoidPtr(Thread::getId())<<"]Thread::start() found freed thread:"<<id << endl;
			break;
		}
	}

//	if(debug)
//		cout << "["<<toVoidPtr(Thread::getId())<<"]Thread::start() new ThreadInfo id:"<<id << endl;
	threadInfo *ti= new threadInfo(id, this, args, e);
	ti->debug = debug;
//		int policy;
//		sched_param param;
//		int res=pthread_getschedparam(t->pthread, &policy, &param); 
//		param.sched_priority=newPrio;
//		res=pthread_setschedparam(t->pthread, policy, &param); 
    //   	cout <<" starting new thread setSched res::"<<res<<" policy: " << policy <<" priority : " <<param.sched_priority <<endl;

	if(debug)
	{
		debugStr = string( "[") + StrUtils::decToString(toLong(Thread::getId())) + string("]Thread::start() id:") + StrUtils::decToString(id) + string(" pthread_create");
		cout<<debugStr<<endl;
	}
	pthread_create(&(t->pthread), &attr,Thread::run, ti);
	
#ifdef THREAD_TRACE_DEBUG
	t->ttdDebug=0;
	Thread::registerDebug(t->pthread, &t->ttdDebug);
#endif
	
	//initPrio(t->pthread);
	if(id>=tInf->lthreads.size())
	{
//		if(debug)
//			cout << "["<<toVoidPtr(Thread::getId())<<"]Thread::start() id:"<<id <<" lthreads push_back"<< endl;
		tInf->lthreads.push_back(*t);
	}else
	{
//		if(debug)
//			cout << "["<<toVoidPtr(Thread::getId())<<"]Thread::start() id:"<<id <<" lthreads set"<< endl;
		tInf->lthreads[id]=*t;
	}
	tInf->lastStarted=id;

//	if(debug)
//		cout << "["<<toVoidPtr(Thread::getId())<<"]----> Thread::start() preUnlock" << endl;
	tInf->mllista.unlock();
	if(debug)
	{
		debugStr = string( "[") + StrUtils::decToString(toLong(Thread::getId())) + string("]----> Thread::start() unlocked" );
		cout<<debugStr<<endl;
	}

	pthread_attr_destroy(&attr);

	return (id);
}

void Thread::exitThread(int id, threadsInfo *ti)
{
	STACKTRACE_INSTRUMENT();
	
	if(id>ti->lthreads.size())
	{
		cout<<" ExitThread:: invalid ID"<<endl;
		return;
	}
	ti->mllista.lock();

	if(ti->lthreads[id].estat==Thread::StateRunning)
		ti->lthreads[id].estat=Thread::StateExit;
	else //if(ti->lthreads[id].estat==Thread::StateDetached || ti->lthreads[id].estat==Thread::StateJoined)
		ti->lthreads[id].estat=Thread::StateFree;

#ifdef THREAD_TRACE_DEBUG
	Thread::unregisterDebug(ti->lthreads[id].pthread);
#endif

	ti->mllista.unlock();
//	pthread_exit(value);
}

void Thread::detach()
{
	STACKTRACE_INSTRUMENT();
	
	detach(tInf->lastStarted);
}

void Thread::detach(int id)
{
	STACKTRACE_INSTRUMENT();
	
	if(id>=0 && id<tInf->lthreads.size())
	{
		tInf->mllista.lock();
		if(tInf->lthreads[id].estat==Thread::StateRunning || tInf->lthreads[id].estat==Thread::StateExit)
		{
			pthread_detach(tInf->lthreads[id].pthread);

			if(tInf->lthreads[id].estat==Thread::StateRunning)
				tInf->lthreads[id].estat=Thread::StateDetached;
			else
				tInf->lthreads[id].estat=Thread::StateFree;
		}
		tInf->mllista.unlock();
	}
}

void Thread::detachAll()
{
	STACKTRACE_INSTRUMENT();
	
	int size=tInf->lthreads.size();
	for(int i=0;i<size;i++)
	{
		this->detach(i);
	}
}

void* Thread::run(void* p)
{
	STACKTRACE_INSTRUMENT();
	
	struct threadInfo *i = ((struct threadInfo*)p);
	bool debug = i->debug;
	if(debug)
		cout<<"["<<toVoidPtr(Thread::getId())<<"] Thread::run id:"<<i->threadID<<endl;
//	delete (struct threadInfo*)p;
	threadsInfo *tInfLoc=i->thread->tInf;

	void* res=NULL;

	bool exception=false;

	if(debug)
		cout<<"["<<toVoidPtr(Thread::getId())<<"] Thread::run prelock:"<<i->threadID<<endl;
	tInfLoc->mllista.lock();
	if(debug)
		cout<<"["<<toVoidPtr(Thread::getId())<<"] Thread::run locked:"<<i->threadID<<endl;
	tInfLoc->refCount++;
//	if(this->debug)
//	cout<<" thread::run preunlock:"<<i->threadID<<endl;
	tInfLoc->mllista.unlock();
	if(debug)
		cout<<"["<<toVoidPtr(Thread::getId())<<"] Thread::run unlocked:"<<i->threadID<<endl;

	try
	{
		do{
			exception=false;
			try{
				if(debug)
					cout<<"["<<toVoidPtr(Thread::getId())<<"] Thread::run:"<<i->threadID<<endl;
				res=(void*)i->thread->execute(i->threadID, i->args);
				if(debug)
					cout<<"["<<toVoidPtr(Thread::getId())<<"] Thread::run end:"<<i->threadID<<endl;
			}catch(Exception &e)
			{
			//	cout<<"Thread "<<i->threadID<<" exception: " << e.getClass() << ": " << e.getCode() <<": "<<e.getMsg()<<endl;
				cout<<"["<<toVoidPtr(Thread::getId())<<"] Caught Thread exception: " << e.getClass() << ": " << e.getCode() <<": "<<e.getMsg()<<endl;
				cout<<"["<<toVoidPtr(Thread::getId())<<"]            OnException:" << i->exc << " exitPrc:" << Thread::OnExceptionExitProcess <<" exitTH:"<<Thread::OnExceptionExitThread<<" restart:"<<Thread::OnExceptionRestart<<endl;
				if(i->exc==Thread::OnExceptionExitProcess)
				{
				cout<<"Caught Thread exception: " << e.getClass() << ": " << e.getCode() <<": "<<e.getMsg()<<" exit process--> abort()"<<endl;
					delete i;
					abort();
				}
				exception=true;
			}
			catch(std::exception &e)
			{
				cout<<"["<<toVoidPtr(Thread::getId())<<"] Caught Thread std::exception: " << e.what() <<endl;
				if(i->exc==Thread::OnExceptionExitProcess)
				{
					delete i;
					abort();
				}
				exception=true;
			}
		} while(exception && i->exc==Thread::OnExceptionRestart);
//		cout<<" call exitThread:"<<i->threadID<<endl;
		// delete i->pinfo;

		if(debug)
			cout<<"["<<toVoidPtr(Thread::getId())<<"]Thread::run exitTHread:"<<i->threadID<<endl;
		i->thread->exitThread(i->threadID, tInfLoc);

		tInfLoc->mllista.lock();
		tInfLoc->refCount--;
		if(tInfLoc->refCount==0)
		{
//			cout<<" petant:"<<(void*)tInfLoc<<endl;
			tInfLoc->mllista.unlock();
			delete tInfLoc;
		}
		else
		{
//			cout<<" no petem:"<<(void*)tInfLoc<<" refs:"<<tInfLoc->refCount<<endl;
			tInfLoc->mllista.unlock();
		}

//		i->thread->exitThread(i->threadID, tInfLoc);

		delete i;

		return res;

	}catch(Exception &e)
	{
		cout<<" ----- Thread "<<i->threadID<<" global exception: " << e.getClass() << ": " << e.getCode() <<": "<<e.getMsg()<<endl;
		cout<<"           OnException:" << i->exc << " exitPrc:" << Thread::OnExceptionExitProcess <<" exitTH:"<<Thread::OnExceptionExitThread<<" restart:"<<Thread::OnExceptionRestart<<endl;
		//	e.serialize()->materializeAndThrow();
		delete i;
		return NULL;
	}
}

void* Thread::execute(int id, void *args) {return NULL;}

void* Thread::join()
{
	STACKTRACE_INSTRUMENT();
	
	void* res=join(tInf->lastStarted);
	return res;
}

void* Thread::join(int id)
{
	STACKTRACE_INSTRUMENT();
	
//	cout << "--> Thread::join(" << id << "), l.size() " << tInf->lthreads.size() << endl;
	if(id>=0 && id<tInf->lthreads.size())
	{
		void* ret=(void*)NULL;

		tInf->mllista.lock();

		// Ens assegurem que no faci un join amb ell mateix...
		if((tInf->lthreads[id].estat==Thread::StateRunning ||
				tInf->lthreads[id].estat==Thread::StateExit) &&
			pthread_equal(tInf->lthreads[id].pthread, Thread::getId())==0)
		{
			// Fem una copia, perque anem a alliberar el thread.
			struct thread aux=tInf->lthreads[id];
			
			if(tInf->lthreads[id].estat==Thread::StateRunning)
				tInf->lthreads[id].estat=Thread::StateJoined;
			else
				tInf->lthreads[id].estat=Thread::StateFree;
			tInf->mllista.unlock();

			// fem el join fora del lock, pq bloqueja :)	
			pthread_join(aux.pthread, &ret);
		}
		else
			tInf->mllista.unlock();
	
//		cout << "	<-- Thread::join()" << endl;
		return ret;
	}

//	cout << "<-- Thread::join()" << endl;
	return (void*)(NULL);
}

vector<void*> Thread::joinAll()
{
	STACKTRACE_INSTRUMENT();
	
	vector<void*> res;

	int size=tInf->lthreads.size();
	for(int i=0;i<size;i++)
	{
		res.push_back(this->join(i));
	}

	return res;
}

int Thread::joinableThreads()
{
	STACKTRACE_INSTRUMENT();

	int res=0;

	tInf->mllista.lock();
	int size=tInf->lthreads.size();
	for(int i=0;i<size;i++)
		if(tInf->lthreads[i].estat==Thread::StateExit)
			res++;
	tInf->mllista.unlock();
	return res;
}

int Thread::getJoinableThread()
{
	STACKTRACE_INSTRUMENT();

	int res=0;

	tInf->mllista.lock();
	int size=tInf->lthreads.size();
	for(int i=0;i<size;i++)
		if(tInf->lthreads[i].estat==Thread::StateExit)
		{
			tInf->mllista.unlock();
			return i;
		}
	tInf->mllista.unlock();
	return -1;
}

void Thread::kill()
{
	STACKTRACE_INSTRUMENT();
	
	kill(tInf->lastStarted);
}

void Thread::kill(int id)
{
	STACKTRACE_INSTRUMENT();
	
	if(id>=0 && id<tInf->lthreads.size() && pthread_equal(tInf->lthreads[id].pthread, Thread::getId())==0)
	{
		tInf->mllista.lock();
		if(tInf->lthreads[id].estat!=Thread::StateExit && tInf->lthreads[id].estat!=Thread::StateFree)
		{
			pthread_cancel(tInf->lthreads[id].pthread);
			tInf->lthreads[id].estat=Thread::StateFree;
		}
		tInf->mllista.unlock();
	}
}

void Thread::killAll()
{
	STACKTRACE_INSTRUMENT();
	
	int size=tInf->lthreads.size();
	for(int i=0;i<size;i++)
	{ 
		this->kill(i);
	}
}

void Thread::signal(int id, int sig)
{
	STACKTRACE_INSTRUMENT();
	
	if(id>=0 && id<tInf->lthreads.size())
	{
		tInf->mllista.lock();
		if(tInf->lthreads[id].estat!=Thread::StateExit && tInf->lthreads[id].estat!=Thread::StateFree)
		{
			int err=pthread_kill(tInf->lthreads[id].pthread, 0);
			if (err!=0)
			{
				cout << "Thread::signal: error en checking: " << err << endl;
			}
			
			err=pthread_kill(tInf->lthreads[id].pthread, sig);
			if (err!=0)
			{
				cout << "Thread::signal: error en signal: " << err << endl;
			}
		}
		tInf->mllista.unlock();
	}
}

int Thread::runningInstances()
{
	STACKTRACE_INSTRUMENT();
	
	tInf->mllista.lock();
	int size=tInf->lthreads.size();
	int instances=0;
	for(int i=0;i<size;i++)
	{ 
		if(tInf->lthreads[i].estat!=Thread::StateExit && tInf->lthreads[i].estat!=Thread::StateFree)
			instances++;
	}
	tInf->mllista.unlock();
	return instances;
}

void Thread::setThreadPriority(int id, int prio)
{
	STACKTRACE_INSTRUMENT();
	
//	if(prio<minPrio)
//		prio=minPrio;
//	else if(prio>maxPrio)
//		prio=maxPrio;
//	if(id>=0 && id<tInf->lthreads.size())
//	{
//		tInf->mllista.lock();
//		if(tInf->lthreads[id].estat==Thread::StateRunning || tInf->lthreads[id].estat==Thread::StateJoined || tInf->lthreads[id].estat==Thread::StateDetached)
//		{
//#ifdef WIN32 //TODO : en win?
//#else
//			int policy;
//			sched_param param;
//			int res=pthread_getschedparam(tInf->lthreads[id].pthread, &policy, &param); 
//        	cout <<"getSched "<<res<<" policy: " << policy <<" priority : " <<param.sched_priority <<endl;
//			param.sched_priority=prio;
//			res=pthread_setschedparam(tInf->lthreads[id].pthread, policy, &param); 
//			cout<<" setSched : "<<res<<endl;
//#endif
//			tInf->lthreads[id].priority=prio;
//		}
//		tInf->mllista.unlock();
//	}
}

int Thread::getThreadPriority(int id)
{
	STACKTRACE_INSTRUMENT();
	
//	if(id>=0 && id<tInf->lthreads.size())
//	{
//		tInf->mllista.lock();
//		if(tInf->lthreads[id].estat!=Thread::StateFree)
//		{
//#ifdef WIN32 //TODO : en win?
//#else
//			int policy;
//			sched_param param;
//			int res=pthread_getschedparam(tInf->lthreads[id].pthread, &policy, &param); 
//  //      	cout <<"getSched "<<res<<" policy: " << policy <<" priority : " <<param.sched_priority <<endl;
//			tInf->mllista.unlock();
//			return param.sched_priority;
//#endif
//		}
//		tInf->mllista.unlock();
//	}
	return -1;
}

void Thread::changeThreadPriority(int id, int prio)
{
	STACKTRACE_INSTRUMENT();
	
//	if(prio<minPrio)
//		prio=minPrio;
//	else if(prio>maxPrio)
//		prio=maxPrio;
//	if(id>=0 && id<tInf->lthreads.size())
//	{
//		tInf->mllista.lock();
//		if(tInf->lthreads[id].estat==Thread::StateRunning || tInf->lthreads[id].estat==Thread::StateJoined || tInf->lthreads[id].estat==Thread::StateDetached)
//		{
//#ifdef WIN32 //TODO : en win?
//#else
//			int policy;
//			sched_param param;
//			int res=pthread_getschedparam(tInf->lthreads[id].pthread, &policy, &param); 
//    //    	cout <<"getSched "<<res<<" policy: " << policy <<" priority : " <<param.sched_priority <<endl;
//			param.sched_priority += prio;
//			res=pthread_setschedparam(tInf->lthreads[id].pthread, policy, &param); 
//	//		cout<<" setSched : "<<res<<endl;
//#endif
//			tInf->lthreads[id].priority=prio;
//		}
//		tInf->mllista.unlock();
//	}
}

void Thread::setNewThreadPriority(int prio)
{
	STACKTRACE_INSTRUMENT();
	
//	newPrio=prio;	
//	if(newPrio<minPrio)
//		newPrio=minPrio;
//	else if(newPrio>maxPrio)
//		newPrio=maxPrio;
}


#ifdef THREAD_TRACE_DEBUG

Mutex Thread::ttdDebugMutex(true);
map <pthread_t, int*> Thread::ttdDebugMap;	// pthread_t -> thread::ttdDebug

void Thread::registerDebug(pthread_t th, int *debug)
{
	Thread::ttdDebugMutex.lock();
	Thread::ttdDebugMap[th]=debug;
	Thread::ttdDebugMutex.unlock();
}

void Thread::unregisterDebug(pthread_t th)
{
	Thread::ttdDebugMutex.lock();
	Thread::ttdDebugMap.erase(th);
	Thread::ttdDebugMutex.unlock();
}

int *Thread::ttdGetPointer()
{
	pthread_t th = Thread::getId();
	int *d=NULL;

	if (th==NULL)
		return d;
		
	Thread::ttdDebugMutex.lock();

	map<pthread_t, int*>::iterator pIt = Thread::ttdDebugMap.find(th);
	if (pIt!=Thread::ttdDebugMap.end())
	{
		d = pIt->second;
	}

	Thread::ttdDebugMutex.unlock();
	
	return d;
}

void Thread::ttdDump()
{
	string s=string("\n") + Log::getDateLogTag() + string("SFD_DEBUG: ");

	Thread::ttdDebugMutex.lock();
	
	map<pthread_t, int*>::iterator pIt;
	
	for (pIt = Thread::ttdDebugMap.begin(); pIt != Thread::ttdDebugMap.end();
		pIt++)
	{
		int *d = pIt->second;
		if (d!=NULL)
		{
			int f = ((*d)>>20)&0xfff;
			int l = (*d)&0xfffff;
			if (f!=0)
				s+=string("[0x") + StrUtils::hexToString((int)((void*)d))+ string(":0x") + StrUtils::hexToString(f) + string("L") + StrUtils::decToString(l) + string("]|");
		}
	}
	
	Thread::ttdDebugMutex.unlock();	

	s+= string("\n");

	cout << s;
}

#endif
