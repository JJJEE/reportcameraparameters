#ifndef SIRIUS_THREADS_THREAD
#define SIRIUS_THREADS_THREAD

//#pragma once
//#pragma message("Thread.h")
#include <Utils/debugNew.h>
#include <Utils/Types.h>
#ifdef WIN32
//#include <windows.h>
#include <Utils/WindowsInclude.h>
//#include <windows.h>
#endif
#include <pthread.h>
#include <iostream>
#include <vector>
#include <Threads/Mutex.h>
#include <Threads/Condition.h>

using namespace std;

class Thread
{
public:
	static int minPrio, maxPrio, defaultPrio;

	static const int StateFree = 0;
	static const int StateRunning = 1;
	static const int StateJoined = 2;
	static const int StateDetached = 3;
	static const int StateExit = 4;

//	enum OnException {RESTART=0, EXIT_THREAD, EXIT_PROCESS};
	static const int OnExceptionRestart = 0;
	static const int OnExceptionExitThread = 1;
	static const int OnExceptionExitProcess = 2;

protected:
	typedef int TState;
	typedef int OnException;
	
//	enum TState {FREE=0, RUNNING, JOINED, DETACHED, EXIT};
	struct thread
	{
		pthread_t pthread;
		int priority;
		TState estat;

		thread(TState estat): estat(estat) {};

#ifdef THREAD_TRACE_DEBUG
		// Codis de debug:
		// 12 Bits mes alts indiquen fitxer, 20 bits baixos linia.
		// L'id de fitxer en realitat ��s segons context.
		int ttdDebug;
#endif
	};

#ifdef THREAD_TRACE_DEBUG
public:
	static Mutex ttdDebugMutex;
	static map <pthread_t, int*> ttdDebugMap;	// pthread_t -> thread::ttdDebug
	static void registerDebug(pthread_t th, int *debug);
	static void unregisterDebug(pthread_t th);
	static int *ttdGetPointer();
	static void ttdDump();
#endif


public:
	class threadsInfo
	{
	public:
		int refCount;
		vector<thread> lthreads;
		Mutex mllista; //mutex per la llista de threads
		int lastStarted;
		threadsInfo():refCount(1){};
	};

protected:

	string threadName;
	threadsInfo *tInf;

	static void* run(void* p);

	struct threadInfo
	{
		int threadID;
		Thread *thread;
		void *args;
		OnException exc;
		bool debug;

		threadInfo(int id, Thread *t, void *a, OnException exc=Thread::OnExceptionExitThread): threadID(id), thread(t), args(a), exc(exc), debug(false){};
		void setArgs(void *a){args=a;};
	};
	int newPrio;

private:	// No desprivar :)
	static void exitThread(int id, threadsInfo *ti);
	static void initPrio(pthread_t pthread);

public:
	Thread();
	virtual ~Thread();
	Thread(void *args);

	void setName(string name);
	string getName();
	
	static pthread_t getId();
	pthread_t getId(int id);
	static bool currentThreadEqualsPthread(pthread_t t);

	int start(void *args=NULL, OnException e=Thread::OnExceptionExitThread, bool debug = false);
	virtual void* execute(int id, void *args);//funció que s'executa com a codi delthread

	void kill(int id);   //mata el thread indicat si no es el que l'ha cridat
	void killAll();		//mata tots els threads excepte el que l'ha cridat 
	void signal(int id, int sig);
	void detach(int id);
	void detachAll();	//detacha tots els threads

	void* join(int id); 
	vector<void*> joinAll(); //fa un join amb tots els threads excepte amb ell mateix
	
	int joinableThreads();	//num de threads que han acabat -->llestor per fer un join
	int getJoinableThread();//id d'un dels threads que han acabat

	void kill();	//operen sobre l'ultim thread creat, per comoditat. Not Thread Safe
	void detach();
	void* join(); 

	int runningInstances();

	void setThreadPriority(int id, int prio);
	int getThreadPriority(int id);
	void changeThreadPriority(int id, int prio); // +- variació 
//	void setThreadPriority(int prio);
	void setNewThreadPriority(int prio);

};

#endif
