#ifndef SIRIUS_THREADS_MUTEX_H_
#define SIRIUS_THREADS_MUTEX_H_

#include <pthread.h>
#ifdef WIN32
//#include <windows.h>
#include <Utils/WindowsInclude.h>
#endif
#include <string>

#ifdef WITH_INSTRUMENTEDLOCK
#include <Threads/InstrumentedLock.h>
#else
#include <Threads/NonInstrumentedLock.h>
#endif

using namespace std;

#ifdef WITH_INSTRUMENTEDLOCK
class Mutex : public InstrumentedLock
#else
class Mutex : public NonInstrumentedLock
#endif
{
	public:
		class mutexInfo
		{
			public:
				int refCount;
				int holdCount;
				bool inited;
				pthread_t currentHolder;
				pthread_mutex_t *m;
				mutexInfo() : refCount(1), holdCount(0), m(NULL), inited(false)
				{
					this->currentHolder = pthread_t();
				}
		};
		mutexInfo *mi;
		bool recursive;
		string holdLoc;

		void deleteMutex();
		void createMutex();
	public:
		Mutex(bool recursive=false);
		Mutex(const Mutex &m2);
		Mutex & operator=(const Mutex& m2);
		virtual ~Mutex();

//		void lock();
		void lock(bool out=false, string loc=string(""));
		bool tryLock(bool out=false, string loc=string(""));
//		void unlock();
		void unlock(bool out=false, string loc=string(""));
		
		bool isHeldByCurrentThread();
};

#include <Utils/debugNew.h>
#endif
