//#pragma message("InstLock il.h")
#include <Threads/InstrumentedLock.h>
//#pragma message("InstLock stdl.h")
#include <stdlib.h>
//#pragma message("InstLock pthr_op.h")
#include <Threads/pthread_ops.h>
//#pragma message("/InstLock")

#pragma mark *** InstrumentedLock
InstrumentedLock::InstrumentedLock() : Lock()
{
	STACKTRACE_INSTRUMENT();
}

InstrumentedLock::~InstrumentedLock()
{
	STACKTRACE_INSTRUMENT();	
}

void InstrumentedLock::processLock(bool isWrite)
{
	STACKTRACE_INSTRUMENT();
	
	LockInfo *li=new LockInfo(this, isWrite);

	int err=pthread_mutex_lock(&Lock::locksByThreadLock);
	if (err!=0)
	{
		cerr << "[" << __FILE__ << ", line " << __LINE__ << 
			"] FATAL: Could not lock locksByThreadLock" << endl;
			
		STACKTRACE_DUMP();
		exit(-1);
	}

	// Encuem
	map<pthread_t, list<LockInfo*> >::iterator mIt = 
		Lock::locksByThread.find(pthread_self());
	
	if (mIt==Lock::locksByThread.end())
	{
		list<LockInfo*> l;
		l.push_back(li);
		Lock::locksByThread[pthread_self()]=l;
	}
	else
	{
		list<LockInfo*> *l=&mIt->second;
		l->push_back(li);
	}
	
	pthread_mutex_unlock(&Lock::locksByThreadLock);
}

void InstrumentedLock::processUnlock()
{
	STACKTRACE_INSTRUMENT();
	
	int err=pthread_mutex_lock(&Lock::locksByThreadLock);
	if (err!=0)
	{
		cerr << "[" << __FILE__ << ", line " << __LINE__ << 
			"] FATAL: Could not lock locksByThreadLock" << endl;
			
		STACKTRACE_DUMP();
		exit(-1);
	}

	// Encuem
	map<pthread_t, list<LockInfo*> >::iterator mIt = 
		Lock::locksByThread.find(pthread_self());
	
	if (mIt==Lock::locksByThread.end())
	{
		cerr << "[" << __FILE__ << ", line " << __LINE__ << 
			"] FATAL: Unlock for a thread with no locks" << endl;
			
		STACKTRACE_DUMP();
		exit(-1);
	}
	
	list<LockInfo*> *l=&mIt->second;
	
	// Busquem l'ultim LockInfo que ens tingui a nosaltres
	bool unlockProcessed=false;
	list<LockInfo*>::reverse_iterator rlIt = l->rbegin();
	for (; rlIt!=l->rend() && !unlockProcessed; rlIt++)
	{
		LockInfo *li=*rlIt;
		
//		cout << pthread_self() << " vs " << li->getLockThread() << ", "
//			<< li->isLocked() << ", " << (void*)li->getLock() << " vs " <<
//			(void*)this << endl;
		
		if (pthread_equal(pthread_self(), li->getLockThread())
			&& li->isLocked() && li->getLock() == this)
		{
			li->processUnlock();
			unlockProcessed=true;
		}
	}

	if (!unlockProcessed)
	{
		cerr << "[" << __FILE__ << ", line " << __LINE__ << 
			"] FATAL: Lock not found" << endl;
			
		STACKTRACE_DUMP();
		exit(-1);
	}

	pthread_mutex_unlock(&Lock::locksByThreadLock);
}

