#include <Threads/RWlock.h>
#include <Utils/Exception.h>
#include <errno.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>

using namespace std;

RWLock::RWLock()
{
	this->rwi=new rwInfo();
//	this->rwi->rw=new pthread_rwlock_t;
//	pthread_rwlockattr_t a;
//	pthread_rwlockattr_init(&a);
//	pthread_rwlockattr_setpshared(&a,PTHREAD_PROCESS_SHARED);
	int err=pthread_rwlock_init(&this->rwi->rw, NULL);
	if(err)
	{
		cerr << "FATAL: Could not init RWLock" << endl;
		throw Exception("FATAL: Could not init RWLock");
	}
//	pthread_rwlockattr_destroy(&a);
}

RWLock::~RWLock()
{
	this->rwi->refCount--;
	if(this->rwi->refCount==0)
	{
		deleteMutex();
	}
}

void RWLock::deleteMutex()
{
//	cout<<"RWLock destroy "<<(void*) &l<<endl; // StrUtils::hexDump(string(((char*)&l), sizeof(pthread_rwlock_t) ))<<endl;
	int err=pthread_rwlock_destroy(&this->rwi->rw);
	if(err!=0)
	{
		if(err==EBUSY)
		{
			wlock();
			unlock();
		//	throw(Exception("~RWLock: RWLock ocupat"));
			int err=pthread_rwlock_destroy(&this->rwi->rw);
		}
		else
		{
			throw(Exception("~RWLock: RWLock Invalid"));
		}
	}
	delete this->rwi;
}

void RWLock::rlock(bool out, string loc)
{
	if(out)
		cout<<"RWLock rlock "<<(void*) &this->rwi->rw<<" at: "<<loc<<endl; // StrUtils::hexDump(string(((char*)&l), sizeof(pthread_rwlock_t) ))<<endl;

//	struct timespec waitTime;
//	waitTime.tv_sec=time(NULL)+60;
//	waitTime.tv_nsec=0;

	int err=pthread_rwlock_rdlock(&this->rwi->rw);
//	int err=pthread_rwlock_timedrdlock(&this->rwi->rw, &waitTime);

	if(err!=0)
	{
		if(err == ETIMEDOUT)
		{
			cout << "RWLock::rlock : Error: rlockTimeout"<< endl;
			throw(Exception(string("RWLock::rlock timed out")));
		}
		else
			throw(Exception(string("rlock: RWLock Invalid:")+ string(strerror(err))));
	}
}

bool RWLock::tryrLock(bool out, string loc)
{
	if(out)
		cout<<"RWLock tryrlock "<<(void*) &this->rwi->rw<<" at: "<<loc<<endl; // StrUtils::hexDump(string(((char*)&l), sizeof(pthread_rwlock_t) ))<<endl;
	int err=pthread_rwlock_tryrdlock(&this->rwi->rw);
	if(err==0)
		return true;
	else if(err==EBUSY)
		return false;
	else
		throw(Exception(string("tryrlock: RWLock Invalid:")+ string(strerror(err))));
}

void RWLock::wlock(bool out, string loc)
{
	if(out)
		cout<<"RWLock wlock "<<(void*) &this->rwi->rw<<" at: "<<loc<<endl; // StrUtils::hexDump(string(((char*)&l), sizeof(pthread_rwlock_t) ))<<endl;
/*	struct timespec waitTime;
	waitTime.tv_sec=time(NULL)+60;
	waitTime.tv_nsec=0;
*/

	int err=pthread_rwlock_wrlock(&this->rwi->rw);
//	int err=pthread_rwlock_timedwrlock(&this->rwi->rw, &waitTime);

	if(err!=0)
	{
		if(err == ETIMEDOUT)
		{
			cout << "RWLock::wlock : Error: wlockTimeout"<< endl;
			throw(Exception(string("RWLock::wlock  timed out")));
		}
		else
		throw(Exception(string("wlock: RWLock Invalid:")+ string(strerror(err))));
	}
}

bool RWLock::trywLock(bool out, string loc)
{
	if(out)
		cout<<"RWLock trywlock "<<(void*) &this->rwi->rw<<" at: "<<loc<<endl; // StrUtils::hexDump(string(((char*)&l), sizeof(pthread_rwlock_t) ))<<endl;
	int err=pthread_rwlock_trywrlock(&this->rwi->rw);
	if(err==0)
		return true;
	else if(err==EBUSY)
		return false;
	else
		throw(Exception(string("trywlock: RWLock Invalid:")+ string(strerror(err))));
}

void RWLock::unlock(bool out, string loc)
{
	if(out)
		cout<<"RWLock unlock "<<(void*) &this->rwi->rw<<" at: "<<loc<<endl; // StrUtils::hexDump(string(((char*)&l), sizeof(pthread_rwlock_t) ))<<endl;
	int err=pthread_rwlock_unlock(&this->rwi->rw);
	if(err!=0)
	{	
		if(err==EPERM)
			throw(Exception(string("unlock: RWLock en possessió d'un altre thead :")+ string(strerror(err))));
		else if(err==EINVAL)
			throw(Exception(string("unlock: RWLock invalid:")+ string(strerror(err))));
		else
			throw(Exception(string("unlock: RWLock error desconegut:")+ string(strerror(err))));
	}
}

