#ifndef __SIRIUS__BASE__THREADS__INSTRUMENTEDLOCK_H
#define __SIRIUS__BASE__THREADS__INSTRUMENTEDLOCK_H

//#pragma message("InstLock_h")
#include <Threads/Lock.h>
//#pragma message("/InstLock_h")
using namespace std;
class Lock;
class InstrumentedLock : public Lock
{
public:
	InstrumentedLock();
	virtual ~InstrumentedLock();

	virtual void processLock(bool isWrite=false);
	virtual void processUnlock();
};

#endif
