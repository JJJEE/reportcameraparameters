#ifndef __PTHREAD_OPS_H
#define __PTHREAD_OPS_H

#include <pthread.h>
#include <string>

using namespace std;

// definim els operadors que faltin...
#ifdef WIN32
#include <iostream>

bool operator < (const pthread_t &left, const pthread_t &right);
bool operator > (const pthread_t &left, const pthread_t &right);
std::ostream &operator << (std::ostream &stream, pthread_t &thread);
const void *toVoidPtr (const pthread_t &thread);
#else
const void *toVoidPtr (const pthread_t &thread);
#endif
long toLong (const pthread_t &thread);
int toInt (const pthread_t &thread);
string toString (const pthread_t &thread);
#endif
