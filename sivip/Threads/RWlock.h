#ifndef SIRIUS_THREADS_RWLOCK_H_
#define SIRIUS_THREADS_RWLOCK_H_

#include <Utils/debugNew.h>
#include <pthread.h>
#ifdef WIN32
#include <Utils/WindowsInclude.h>
#endif

#include <string>
using namespace std;

class RWLock
{
private:
	class rwInfo
	{
		public:
			int refCount;
			pthread_rwlock_t rw;
			rwInfo():refCount(1){}
	};
	rwInfo *rwi;
	void deleteMutex();

public:
	RWLock();
	virtual ~RWLock();
	void rlock(bool out=false, string loc=string(""));
	bool tryrLock(bool out=false, string loc=string(""));
	void wlock(bool out=false, string loc=string(""));
	bool trywLock(bool out=false, string loc=string(""));
	void unlock(bool out=false, string loc=string(""));
};
#endif
