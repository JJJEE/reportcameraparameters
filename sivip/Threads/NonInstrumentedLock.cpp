#include <Threads/NonInstrumentedLock.h>

#pragma mark *** NonInstrumentedLock
NonInstrumentedLock::NonInstrumentedLock() : Lock()
{
	STACKTRACE_INSTRUMENT();
}

NonInstrumentedLock::~NonInstrumentedLock()
{
	STACKTRACE_INSTRUMENT();	
}
