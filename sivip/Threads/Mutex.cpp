#include <Threads/Mutex.h>
#include <Threads/Thread.h>
#include <Utils/Exception.h>
#include <errno.h>
#include <iostream>
#include <stdlib.h>
#include <Utils/StrUtils.h>
#include <Threads/pthread_ops.h>

//#include <Utils/debugStackTrace.h>

/*
#ifndef 
	int pthread_mutex_timedlock(pthread_mutex_t *restrict mutex, const struct timespec *restrict abs_timeout)
	{
		struct timespec remain 
		int result;
		do
		{
			result = pthread_mutex_trylock(&mutex);
			if (result == EBUSY)
			{
				timespec ts;
				ts.tv_sec = 0;
				ts.tv_nsec = 10000000;

				int status = -1;
				while (status == -1)
					status = nanosleep(&ts, &ts);
			}
			else
				break;
		}
		while (result != 0  && );

			return result;
	}

#endif
*/
using namespace std;

Mutex::Mutex(bool recursive):recursive(recursive),mi(NULL)
{
	holdLoc=string("");
	createMutex();
//	cout<<"init mi:"<<(void*)mi<<" m:"<<(void*)mi->m<< endl;
}

void Mutex::createMutex()
{
//	if (mi!=NULL && mi->m==NULL)
//		delete mi;
		
	if(mi==NULL)
		mi=new mutexInfo();
	if(mi->m==NULL)
		mi->m=new pthread_mutex_t;
	
	if(mi->inited)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ <<" WARNING: createMutex on already inited mutexInfo"<<endl;
		return;
	}
	pthread_mutexattr_t a;
	pthread_mutexattr_init(&a);
	
	if(recursive)
		pthread_mutexattr_settype(&a, PTHREAD_MUTEX_RECURSIVE);
	else
		pthread_mutexattr_settype(&a, PTHREAD_MUTEX_ERRORCHECK);
	
	if (pthread_mutex_init(mi->m,&a)!=0) 
	{
		cerr << "FATAL: Could not init mutex" << endl;
		exit(-1);
	}
	mi->inited=true;
	
	pthread_mutexattr_destroy(&a);
}

Mutex::Mutex(const Mutex &m2):NonInstrumentedLock()
{
	mi=m2.mi;
	mi->refCount++;
}

Mutex & Mutex::operator=(const Mutex &m2)
{
	if (mi!=NULL)
	{
		mi->refCount--;
		if(mi->refCount<=0)
		{
			deleteMutex();
		}
	}
	mi=m2.mi;
	mi->refCount++;
	return *this;
}

Mutex::~Mutex()
{
	mi->refCount--;
	if(mi->refCount<=0)
	{
		deleteMutex();
	}
}

void Mutex::deleteMutex()
{
	if (mi==NULL)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ <<" WARNING: deleteMutex mi==NULL"<<endl;
		return;
	}
//	cout<<"delete mi:"<<(void*)mi<<" m:"<<(void*)mi->m<< endl;
	if(!mi->inited)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ <<" WARNING: deleteMutex on a non inited mutexInfo"<<endl;
	}

	if(mi->holdCount>0)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ <<" WARNING: deleteMutex on a locked mutexInfo"<<endl;
	}

	int err=pthread_mutex_destroy(mi->m);
	
	if(err!=0)
	{
		if(err==EBUSY)
		{
//			cout<<" delete Mutex error: ocupat"<<endl;
		//	lock();
//			cout<<" Mutex error: ocupat locked"<<endl;
			int err=pthread_mutex_destroy(mi->m);
			//			if(err!=0)
			//				throw(Exception("Mutex ocupat"));
		}
		else if(err==EINVAL)
		{
			throw(Exception(string("Mutex Invalid (deleteMutex)")));
		}
	}
	delete mi->m;

	mi->m=NULL;
//	cout<<"deleted 2 mi:"<<(void*)mi<<" m:"<<(void*)mi->m<< endl;
	delete mi;
	mi=NULL;
//	cout<<"deleted 3 mi:"<<(void*)mi<<endl;

}

void Mutex::lock(bool out, string loc)
{
	if(mi==NULL || mi->m==NULL)
	{
		cout<<"WARNING: Mutex: mi==NULL -- " << loc <<" mi:"<<(void*)mi<<" mi->m:"<<(mi==NULL?"null":(void*)mi->m)<< endl;
		createMutex();
	}

	if(!mi->inited)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ <<" WARNING: lock on a non inited mutexInfo"<<endl;
	}
	
	pthread_mutex_t *m;
	m=mi->m;
	int err;
/*	struct timespec waitTime;
	waitTime.tv_sec=time(NULL)+60;
	waitTime.tv_nsec=0;
*/	if((err=pthread_mutex_lock(m))!=0)
//	if((err=pthread_mutex_timedlock(m, &waitTime))!=0)
	{
		if(err == EDEADLK)
		{
			cout << "Mutex::lock : Error: mutex no recursiu ja lockat"<< endl;
//			throw(Exception(string("Mutex::lock : Error: mutex no recursiu ja lockat")));
		}
		else if(err == EINVAL)
		{
			cout << "Mutex invalid (lock): delete and create (inited:"<<mi->inited<<")"<< endl;
			deleteMutex();
			createMutex();
			pthread_mutex_t *m;
			m=mi->m;
			if(pthread_mutex_lock(m)!=0)
			{
				cout << "Mutex invalid (lock) mi:"<<(void*)mi<<" m:"<<(void*)m<< endl;
				//		abort();
				throw(Exception(string("Mutex Invalid (lock)")));
			}
		}
		else if(err == ETIMEDOUT)
		{
			cout << "Mutex::lock : Error: mutex lockTimeout holder:";
			if(mi==NULL)
				cout<<"NULL!"<<endl;
			else
				cout<<mi->currentHolder<<" from "<<holdLoc<<" :"<<mi->holdCount<<endl;
			throw(Exception(string("Mutex::lock timed out")));
		}
		else
		{
			throw(Exception(string("Mutex::lock unknown exception, error:")+StrUtils::decToString(err)));
		}
	}
	holdLoc=loc;
	
	this->processLock();
	mi->holdCount++;
	mi->currentHolder=pthread_self();
}

bool Mutex::tryLock(bool out, string loc)
{
	if (mi==NULL || mi->m==NULL)
	{
		createMutex();
	}	

	if(!mi->inited)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ <<" WARNING: tryLock on a non inited mutexInfo"<<endl;
	}
	
	int err=pthread_mutex_trylock(mi->m);
	if(err==0)
	{
		mi->holdCount++;
		mi->currentHolder=pthread_self();
		this->processLock();
		return true;
	}
	else if(err==EBUSY)
		return false;
	else
		throw(Exception("Mutex Invalid (tryLock)"));
}

void Mutex::unlock(bool out, string loc)
{
	bool created=false;
	if (mi==NULL || mi->m==NULL)
	{
		cout<<" _______ Mutex unlock() ini mi==NULL (err!!!) "<<" mi:"<<(void*)mi<<" mi->m:"<<(mi==NULL?"null":(void*)mi->m)<<endl;
		created=true;
		createMutex();
	}	

	if(!mi->inited)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ <<" WARNING: unlock on a non inited mutexInfo"<<endl;
	}
	
	if (this->isHeldByCurrentThread())
	{
		mi->holdCount--;
		if (mi->holdCount<=0)
		{
			mi->currentHolder = pthread_t();
		}
		if(mi->holdCount<0)// per si els createMutex()
		{
			cout<<"Mutex::unlock:WARNING:holdCount<0 mi:"<<(void*)mi<<" cnt:"<< mi->holdCount<<endl;
			mi->holdCount=0;
		}
	}
	else
	{
		cout<<"Mutex::unlock:WARNING: !heldByCurrentThread mi:"<<(void*)mi<<" cnt:"<< mi->holdCount<<endl;
	}
	
	if(!created) //if (created) , pthread_mutex_t no està lockat segur, i es un error de la classe Mutex()
	{
		int err=pthread_mutex_unlock(mi->m);
		if(err!=0)
		{
			this->processUnlock();

			if(err==EPERM)
				throw(Exception("Mutex en possessió d'un altre thread"));
			else
				throw(Exception(string("Mutex Invalid (unlock)")));
		}
	}
	// Pot donar "problemes d'ordre" i marcar els unlocks en un ordre diferent
	// al que s'han fet - ja que ja estem fora del lock - pero ens interessa
	// registrar-ho fora del lock, perque si no no podriem fer l'anterior.
	this->processUnlock();
	// Com que a win no pot ser NULL, creem un de buit i l'assignem
}

bool Mutex::isHeldByCurrentThread()
{
	if(mi == NULL || !mi->inited)
	{
		cout << __FILE__ << ", line " << std::dec << __LINE__ <<" WARNING: isHeldByCurrentThread on a non inited mutexInfo (mi:"<<(void*)mi<<")"<<endl;
	}
	return (mi!=NULL && (pthread_equal(mi->currentHolder, pthread_self())!=0));
}
