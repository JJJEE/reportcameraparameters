#include <Threads/Condition.h>
#include <Utils/Exception.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <iostream>

using namespace std;


Condition::Condition():Mutex(true)
{
	ci=new condInfo();
	ci->c=new pthread_cond_t;

	int err=0;
	do
	{
		err=pthread_cond_init(ci->c,NULL);
	}while(err==EAGAIN);

	if(err!=0)
		throw(Exception("Impossible crear nova condicio : "+string(strerror(err))));
}

Condition::Condition(Mutex mutx):Mutex(mutx)
{
	ci=new condInfo();
	ci->c=new pthread_cond_t;

	int err=0;
	do
	{
		err=pthread_cond_init(ci->c,NULL);
	}while(err==EAGAIN);

	if(err!=0)
		throw(Exception("Impossible crear nova condicio : "+string(strerror(err))));
}


Condition::Condition(const Condition &c2):Mutex(c2)
{
	ci=c2.ci;
	ci->refCount++;
}

Condition & Condition::operator=(const Condition &c2)
{
	Mutex::operator=(c2);
//	memcpy(&this->c, &c2.c, sizeof(pthread_cond_t));
	ci->refCount--;
	if(ci->refCount==0)
	{
		deleteCond();
	}
	ci=c2.ci;
	ci->refCount++;
	return *this;
}

Condition::~Condition()
{
	ci->refCount--;
	if(ci->refCount==0)
	{
		deleteCond();
	}
/*	int i=pthread_cond_destroy(&c);
	if(i!=0)
	{
		if(i==EBUSY)
			throw(Exception("~Condition: Variable de Condicio ocupada"));
		else
			throw(Exception("~Condition: Variable de Condicio Invalida"));
	}
*/
}

void Condition::deleteCond()
{
	int err=pthread_cond_destroy(ci->c);
	if(err!=0)
	{
		if(err==EBUSY)
		{
			cout<<" Mutex error: ocupat"<<endl;
			lock();
			cout<<" Mutex error: ocupat locked"<<endl;
			unlock();
			int err=pthread_cond_destroy(ci->c);
			//			if(err!=0)
			//				throw(Exception("Mutex ocupat"));
			//throw(Exception("~Condition: Variable de Condicio ocupada"));
		}
		else if(err==EINVAL)
		{
			throw(Exception("~Condition: Variable de Condicio Invalida (~Condition)"));
			//throw(Exception(string("Mutex Invalid (~Mutex) ")));
		}
	}
	delete ci->c;
	delete ci;
}

void Condition::wait()
{
	int prevHold=mi->holdCount; //haria de ser sempre 1
	mi->holdCount = 0;
	mi->currentHolder = pthread_t();

	int res = pthread_cond_wait(ci->c, mi->m);

	mi->holdCount = prevHold;
	mi->currentHolder=pthread_self();

	if(res !=0)
		throw(Exception("wait: Variable de Condicio Invalida"));
}

bool Condition::wait(struct timespec t)
{
//	struct tm res=*localtime((const time_t*)&(t.tv_sec));
//		cout<<" wait until:" <<res.tm_wday<<" "<<res.tm_mday<<"/"<<res.tm_mon<<"/"<<1900+res.tm_year<<" "<<res.tm_hour<<":"<<res.tm_min<<":"<<res.tm_sec<< endl;

	//20110520: aquí perdém el lock, hem de alliberar-lo i recuperar el lock tal qual el teniem
	int prevHold=mi->holdCount; //haria de ser sempre 1
	mi->holdCount = 0;
	mi->currentHolder = pthread_t();

	int i=pthread_cond_timedwait(ci->c, mi->m, &t);

	mi->holdCount = prevHold;
	mi->currentHolder=pthread_self();

	if(i==ETIMEDOUT)
		return true;
	else if(i!=0)
		throw(Exception("wait: Variable de Condicio o temps indicat Invalids"));

	return false;
}

void Condition::signal()
{
	if(pthread_cond_signal(ci->c)!=0)
		throw(Exception("signal: Variable de Condicio Invalida"));
}

void Condition::broadcast()
{
	if(pthread_cond_broadcast(ci->c)!=0)
		throw(Exception("broadcast: Variable de Condicio Invalida"));
}

