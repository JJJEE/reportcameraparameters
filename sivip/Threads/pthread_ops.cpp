#include <Threads/pthread_ops.h>
#include <Utils/StrUtils.h>

#ifdef WIN32

bool operator < (const pthread_t &left, const pthread_t &right)
{
   return left.p < right.p;
}

bool operator > (const pthread_t &left, const pthread_t &right)
{
   return left.p > right.p;
}

std::ostream &operator << (std::ostream &stream, pthread_t &thread)
{
	stream << thread.p;
	return stream;
}

const void *toVoidPtr (const pthread_t &thread)
{
	return (void*)thread.p;
}

#else

const void *toVoidPtr (const pthread_t &thread)
{
	return (void*)thread;
}

#endif

long toLong (const pthread_t &thread)
{
	return (long)toVoidPtr(thread);
}

int toInt (const pthread_t &thread)
{
	return (int)toVoidPtr(thread);
}

string toString (const pthread_t &thread)
{
	return StrUtils::decToString(toLong(thread));
}
