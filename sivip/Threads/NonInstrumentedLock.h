#ifndef __SIRIUS__BASE__THREADS__NONINSTRUMENTEDLOCK_H
#define __SIRIUS__BASE__THREADS__NONINSTRUMENTEDLOCK_H

#include <Threads/Lock.h>
//#include <Threads/Thread.h>
using namespace std;

class Lock;
class NonInstrumentedLock : public Lock
{
public:
	NonInstrumentedLock();
	virtual ~NonInstrumentedLock();
};

#endif
