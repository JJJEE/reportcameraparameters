#ifndef __SIRIUS__BASE__THREADS__LOCK_H
#define __SIRIUS__BASE__THREADS__LOCK_H
//#pragma message("Lock_h pth")
#include <pthread.h>
//#pragma message("Lock_h typ")
#include <Utils/Types.h>
//#pragma message("Lock_h dbStrace")
#include <Utils/debugStackTrace.h>
//#pragma message("Lock_h timer")
#include <Utils/Timer.h>
//#pragma message("/Lock_h")
#ifdef WIN32
//#include <Utils/WindowsInclude.h>
#endif

#include <map>
#include <list>
#include <string>
#include <iostream>

using namespace std;

class LockInfo;
class TimerInstant;

class Lock
{
protected:
	static bool inited, initing;
	static dword nextLockId;
	static pthread_mutex_t genIdLock, locksByThreadLock;
	static Timer timer;
	static map<pthread_t, list<LockInfo*> > locksByThread;
	
public:
	Lock();
	virtual ~Lock();

	static dword getNextLockId();
	static TimerInstant getInstant();
	virtual void processLock(bool isWrite=false);
	virtual void processUnlock();
};

class LockInfo
{
protected:
	Lock *lock;
	bool isWLock;
	dword lockId;
	pthread_t lockThread;
	char *lockStackTrace, *unlockStackTrace;
	TimerInstant lockInstant, unlockInstant;

public:
	LockInfo(Lock *l, bool isWriteLock=false);
	virtual ~LockInfo();
	
	Lock *getLock();
	dword getLockId();
	pthread_t getLockThread();
	char *getLockStackTrace();
	char *getUnlockStackTrace();
	bool isWriteLock();
	bool isLocked();
	string getLockInstant();
	string getUnlockInstant();
	void processUnlock();
};


#endif
