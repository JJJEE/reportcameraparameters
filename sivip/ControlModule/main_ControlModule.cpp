#include <Utils/Types.h>
#include <Plugins/PluginControl.h>
#include <Utils/Canis.h>
#include <ControlModule.h>
#include <signal.h>
#include <Utils/debugStackTrace.h>
#include <version.h>

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMP();
	abort();
}

#endif
#ifndef WIN32
void sigpipe(int)
{
	cout << "SegPipe" << endl;
}
#endif

int main()
{

#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif
#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
#endif
#ifndef WIN32
	signal(SIGPIPE, sigpipe);
#endif

	STACKTRACE_INSTRUMENT();

	cout << siriusRelease << endl;

	bool exc;

	do
	{
		exc=false;
		try
		{
#ifdef WIN32
			ControlModule m("\\\\.PSF\\Untitled\\aena\\devel\\src\\Projecte VC++\\Sirius\\debug\\ControlModuleConfig.xml");
#else
			ControlModule m("ControlModuleConfig.xml");
#endif
//			Canis cn("ControlModuleCanisConfig.xml");

			m.serve();
		}
		catch (Exception e)
		{
			exc=true;
			cout << "Exception ocurred during initialization: " << e.getMsg() << endl;
		}
		if(exc)
			cout<<"----------- end conmod, no exception ->restart"<<endl;
	}while(exc);
	
	return 0;
}

