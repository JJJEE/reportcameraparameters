#pragma once
#include <Utils/debugNew.h>
#include <Threads/Thread.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>
#include <Module/ModuleGestor.h>
#include <ControlModule/ControlModuleInterface.h>
#include <Utils/Canis.h>

class ControlModule:public ModuleGestor
{

	public:
		ControlModule(Address addr, short type, Canis *cn=NULL);
		ControlModule(string file);

};

