#pragma once
#include <Utils/debugNew.h>
#include <Utils/DBGateway.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/DBGateway.h>
#include <Utils/Timer.h>
#include <Sockets/SocketTCP.h>
#include <Sockets/SocketUDP.h>
//#include <ControlModule/SonyControlplugin.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <map>
#include <list>
#include <iostream>
#include <string>
//#include <RecordingModule/RecordingModuleInterface.h>

using namespace std;

class IntercomManager: public Thread
{
		friend class DevTimeoutThread;
	private:
		string configFile;

		bool stayRunning;
		Canis *cn;
//		ControlModuleAccess *cma;
		Mutex cmaLock, amaLock;
		AlarmManager *am;
		DBGateway *db;
		SocketTCP *socket;
//		int id;

		Address addr;
		int type;

		

	public:

		class device
		{
			public:
			int id;
			AMAlarmId call, answer, hang;
			AlarmModuleAccess *ama;
			AMAlarmValue av;
			bool raised;
			Timer timeout;

			Timer devTimeout;
			bool devTimeoutRaised;

			device(int id)
			{
				this->id=id;
				this->call.devId=id;
				this->call.type=AMAlarmId::Filter;
				this->call.strId="Intercom.call";
				this->call.intId=-1;

				this->answer.devId=id;
				this->answer.type=AMAlarmId::Filter;
				this->answer.strId="Intercom.answer";
				this->answer.intId=-1;

				this->hang.devId=id;
				this->hang.type=AMAlarmId::Filter;
				this->hang.strId="Intercom.hang";
				this->hang.intId=-1;

				this->ama=NULL;
				this->raised=false;	

				this->devTimeoutRaised=true;
				devTimeout.start();
				devTimeout.setStartSeconds(0);
			}
		};

		map<string, device*> devs;
		RWLock devList;
		AlarmModuleAccess *ama;

		static Timer lastReload;
		Mutex reload;
		static const int minReloadTime= 60;
		
		class DevTimeoutThread: public Thread
		{
			IntercomManager *im;
			//static Timer devCheck;
			//Mutex devCheckLock;
			public:
				DevTimeoutThread(IntercomManager *im);
				void* execute(int, void*);
		};
		static const int devCheckTime= 10;
		static const int devTimeout= 10;

		DevTimeoutThread *dtt;


	public:
		IntercomManager(string configFile);
		~IntercomManager();

		void* execute(int, void*);
		void serve();
};

