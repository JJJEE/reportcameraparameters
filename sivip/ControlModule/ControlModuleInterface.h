#pragma once
#ifdef WIN32
#include <Utils/windowsDefs.h>
#endif

#ifdef LINUX
#include <time.h>
#include <sys/time.h>
#endif
#include <Utils/debugNew.h>
#include <Endian/Endian.h>
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
#include <Utils/Canis.h>
#include <Plugins/GestorControl.h>
#include <ModuleInterface/ModuleInterfaceGestor.h>
#include <string>
#include <map>

using namespace std;


class ControlModuleInterface: public ModuleInterfaceGestor
{
	public:
		static const int serviceCount=19;

		static const int startSessionServiceId=0;
		static const int endSessionServiceId=1;

		static const int getPTZServiceId=2;
		static const int setPTZServiceId=3;

		static const int setInputServiceId=4;
		static const int getInputServiceId=5;

		static const int setOutputServiceId=6;
		static const int getOutputServiceId=7;

		static const int setCommandBufferSizeServiceId=8;
		static const int getCommandBufferSizeServiceId=9;

		static const int getCommandBufferPercentInUseServiceId=10;
		static const int getCommandBufferCommandsInUseServiceId=11;
			
		static const int getMetadataValueServiceId=12;
		static const int setMetadataValueServiceId=13;

		static const int getConfigParamServiceId=14;
		static const int setConfigParamServiceId=15;
		static const int getAllConfigParamsServiceId=16;

		static const int moveServiceId=17;
		static const int getConfigParamRecursiveServiceId=18;
		

//		static void loadSchedule(int devId, int recId);
//		static void loadSchedules(int id);
		
	public:
		ControlModuleInterface(Address addr, short type, Canis *cn=NULL, string file=string(""));
		~ControlModuleInterface();
		SerializedException* getSessionNotStablished();
		Gestor* createGestor(Address orig, int type, Canis *cn=NULL);

//		RPCPacket* service(RPCPacket *inPkt, Address *a);
//		bool deleteGestor(int dels=5);

		static RPCPacket* startSession(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* endSession(ControlModuleInterface *cm, GestorControl *g, void* params);

		static RPCPacket* getPTZ(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* setPTZ(ControlModuleInterface *cm, GestorControl *g, void* params);

		static RPCPacket* setInput(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* getInput(ControlModuleInterface *cm, GestorControl *g, void* params);

		static RPCPacket* setOutput(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* getOutput(ControlModuleInterface *cm, GestorControl *g, void* params);

		static RPCPacket* setCommandBufferSize(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* getCommandBufferSize(ControlModuleInterface *cm, GestorControl *g, void* params);

		static RPCPacket* getCommandBufferPercentInUse(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* getCommandBufferCommandsInUse(ControlModuleInterface *cm, GestorControl *g, void* params);
			
		static RPCPacket* getMetadataValue(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* setMetadataValue(ControlModuleInterface *cm, GestorControl *g, void* params);

		static RPCPacket* getConfigParam(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* setConfigParam(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* getAllConfigParams(ControlModuleInterface *cm, GestorControl *g, void* params);

		static RPCPacket* move(ControlModuleInterface *cm, GestorControl *g, void* params);
		static RPCPacket* getConfigParamRecursive(ControlModuleInterface *cm, GestorControl *g, void* params);
};

