#include <Utils/ServiceException.h>
#include <Utils/SerializedException.h>
#include <ControlModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <Sockets/SocketException.h>
#include <Utils/Timer.h>

#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>



ControlModuleInterface::ControlModuleInterface(Address addr, short type, Canis *cn, string file) : ModuleInterfaceGestor(addr, type, cn)
{

	numServices=serviceCount;
	services=new serviceDef[serviceCount];
	int id=startSessionServiceId;
	cout<<"startSession id:"<<id<<endl;
	sessionlessId.push_back(id);
    startSessionId = ControlModuleInterface::startSessionServiceId;

	services[startSessionServiceId].call =(DefaultModuleGestorCall)startSession;
	services[endSessionServiceId].call =(DefaultModuleGestorCall) endSession;

	services[getPTZServiceId].call =(DefaultModuleGestorCall) getPTZ;
	services[setPTZServiceId].call =(DefaultModuleGestorCall) setPTZ;

	services[setInputServiceId].call =(DefaultModuleGestorCall) setInput;
	services[getInputServiceId].call =(DefaultModuleGestorCall) getInput;

	services[setOutputServiceId].call =(DefaultModuleGestorCall) setOutput;
	services[getOutputServiceId].call =(DefaultModuleGestorCall) getOutput;

	services[setCommandBufferSizeServiceId].call =(DefaultModuleGestorCall) setCommandBufferSize;
	services[getCommandBufferSizeServiceId].call =(DefaultModuleGestorCall) getCommandBufferSize;

	services[getCommandBufferPercentInUseServiceId].call =(DefaultModuleGestorCall) getCommandBufferPercentInUse;
	services[getCommandBufferCommandsInUseServiceId].call =(DefaultModuleGestorCall) getCommandBufferCommandsInUse;

	services[getMetadataValueServiceId].call =(DefaultModuleGestorCall) getMetadataValue;
	services[setMetadataValueServiceId].call =(DefaultModuleGestorCall) setMetadataValue;

	services[getConfigParamServiceId].call =(DefaultModuleGestorCall) getConfigParam;
	services[setConfigParamServiceId].call =(DefaultModuleGestorCall) setConfigParam;
	services[getAllConfigParamsServiceId].call =(DefaultModuleGestorCall) getAllConfigParams;

	services[moveServiceId].call =(DefaultModuleGestorCall) move;
	services[getConfigParamRecursiveServiceId].call = (DefaultModuleGestorCall)getConfigParamRecursive;

}

ControlModuleInterface::~ControlModuleInterface()
{
}


SerializedException* ControlModuleInterface::getSessionNotStablished()
{
	CPSessionNotStablishedException sne(0,"Sessió no establerta");
	return sne.serialize();
}

Gestor* ControlModuleInterface::createGestor(Address orig, int type, Canis *cn)
{
	cout<<"CMI::CreateGestor"<<endl;
	GestorControl *gc=new GestorControl(orig, type, cn);
	cout<<"/CMI::CreateGestor"<<endl;
	return gc;
}

RPCPacket* ControlModuleInterface::startSession(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPDeviceID id(params);
	
	cout << "ControlModuleInterface::startSession ----> g->startSession()" << endl;
	g->startSession(id);
	cout << "ControlModuleInterface::startSession <---- g->startSession()" << endl;

	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}

RPCPacket* ControlModuleInterface::endSession(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPDeviceID id(params);
	
	g->endSession(id);

	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}


RPCPacket* ControlModuleInterface::getPTZ(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPPTZ res=g->getPTZ();

	byte *b=(byte*)res.toNetwork();
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, b, res.serializationSize(), t, 0, true);
	
	delete [] b;
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)res, sizeof(*res), cm->type, 0, true);
	return p;
}

RPCPacket* ControlModuleInterface::setPTZ(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPPTZ ptz(params);
	
	g->setPTZ(ptz);
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}


RPCPacket* ControlModuleInterface::setInput(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPInput in(params);
	
	g->setInput(in);
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}

RPCPacket* ControlModuleInterface::getInput(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPInput in(params);
	
	CPInput res=g->getInput(in);

	byte *b=(byte*)res.toNetwork();
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, b, res.serializationSize(), t, 0, true);
	
	delete [] b;
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)res, sizeof(*res), cm->type, 0, true);
	return p;
}


RPCPacket* ControlModuleInterface::setOutput(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPOutput out(params);
	
	g->setOutput(out);
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}

RPCPacket* ControlModuleInterface::getOutput(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPOutput out(params);

	CPOutput res=g->getOutput(out);

	byte *b=(byte*)res.toNetwork();
		
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, b, res.serializationSize(), t, 0, true);
	
	delete [] b;
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)res, sizeof(*res), cm->type, 0, true);
	return p;
}


RPCPacket* ControlModuleInterface::setCommandBufferSize(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPCommandBufferSize size(params);
	
	g->setCommandBufferSize(size);
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}

RPCPacket* ControlModuleInterface::getCommandBufferSize(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPCommandBufferSize res=g->getCommandBufferSize();

	byte *b=(byte*)res.toNetwork();
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, b, res.serializationSize(), t, 0, true);
	
	delete [] b;
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)res, sizeof(*res), cm->type, 0, true);
	return p;
}


RPCPacket* ControlModuleInterface::getCommandBufferPercentInUse(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	float res=g->getCommandBufferPercentInUse();

	Endian::to(Endian::xarxa, &res, sizeof(float));
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, (byte*)&res, sizeof(float), t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)res, sizeof(float), cm->type, 0, true);
	return p;
}

RPCPacket* ControlModuleInterface::getCommandBufferCommandsInUse(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	int res=g->getCommandBufferCommandsInUse();

	Endian::to(Endian::xarxa, &res, sizeof(int));
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, (byte*)&res, sizeof(int), t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)res, sizeof(int), cm->type, 0, true);
	return p;
}

		
RPCPacket* ControlModuleInterface::getMetadataValue(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPMetadata m(params);

	CPMetadata res=g->getMetadataValue(m);

	byte *b=(byte*)res.toNetwork();
	
	Address	a=cm->address;
	short t=cm->type;
	dword size=res.serializationSize();
	RPCPacket *p=new RPCPacket(a, (word)-1, b, size, t, 0, true);

	delete [] b;
	
	return p;
}

RPCPacket* ControlModuleInterface::setMetadataValue(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPMetadata m(params);

	g->setMetadataValue(m);
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}


RPCPacket* ControlModuleInterface::getConfigParam(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPConfigParam cp(params);

	
//	cout  << "\tControlModuleInterface::getConfigParam(" << cp.path << "): "
//		"pre call to gestor " << endl;
	CPConfigParam res=g->getConfigParam(cp);
//	cout  << "\tControlModuleInterface::getConfigParam(" << cp.path << "): "
//		"called to gestor " << endl;
	
	byte *b=(byte*)res.toNetwork();
	
	Address	a=cm->address;
	short t=cm->type;
	dword size=res.serializationSize();
	RPCPacket *p=new RPCPacket(a, (word)-1, b, size, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)v, res.size(), cm->type, 0, true);
	
	delete [] b;
	
	return p;
}

RPCPacket* ControlModuleInterface::setConfigParam(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPConfigParam cp(params);

	g->setConfigParam(cp);
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
//	cout<<" CMI::setConfigParam - return -"<<endl;
	return p;
}

RPCPacket* ControlModuleInterface::getAllConfigParams(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPConfigParamSeq res=g->getAllConfigParams();

	byte *b=(byte*)res.toNetwork();
	
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, b, res.serializationSize(), t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)v, sz, cm->type, 0, true);
	delete [] b;
	
	return p;
}

RPCPacket* ControlModuleInterface::move(ControlModuleInterface *cm, GestorControl *g, void* params)
{
	CPMovement move(params);
	
	g->move(move);
	
//	cout <<"CMInterface::moving "<<move->x<<" "<<move->y<<" "<<move->z<<endl;
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, NULL, 0, t, 0, true);
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, NULL, 0, cm->type, 0, true);
	return p;
}

RPCPacket* ControlModuleInterface::getConfigParamRecursive(ControlModuleInterface *cm, GestorControl *g, void* params)
{
//	cout  << "\t--> ControlModuleInterface::getConfigParamRecursive(" << params << ") " << endl;
	CPConfigParam cp(params);

//	cout  << "\tControlModuleInterface::getConfigParamRecursive(" << cp.path << "): "
//		"pre call to gestor " << endl;
	CPConfigParamSeq res=g->getConfigParamRecursive(cp);
//	cout  << "\tControlModuleInterface::getConfigParamRecursive(" << cp.path << "): "
//		"called to gestor " << endl;

	byte *b=(byte*)res.toNetwork();
		
	Address	a=cm->address;
	short t=cm->type;
	RPCPacket *p=new RPCPacket(a, (word)-1, b, res.serializationSize(), t, 0, true);
	
	delete [] b;
	//RPCPacket *p=new RPCPacket(cm->address, (word)-1, (byte*)v, sz, cm->type, 0, true);
//	cout  << "\t<-- ControlModuleInterface::getConfigParamRecursive(" << params << ") " << endl;
	return p;
}

