#include <Utils/StrUtils.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#ifdef WIN32
#include <Utils/WindowsDefs.h>
#endif
#include <Utils/Exception.h>
#include <Utils/FileException.h>
#include <ControlModule/IntercomManager.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


Timer IntercomManager::lastReload;

IntercomManager::IntercomManager(string configFile):
	configFile(configFile), cmaLock(true), amaLock(true), stayRunning(true)
{
	STACKTRACE_INSTRUMENT();

	cn=new Canis(configFile, true);

	FILE *f=fopen(configFile.c_str(),"rb");

	if (f==NULL)
		throw FileException(string("File ")+configFile+string(" not found"));

	fseek(f,0,SEEK_END);
	int len=ftell(f);
	fseek(f,0,SEEK_SET);
	
	char *buf=new char[len];
	
	if (buf==NULL)
	{
		throw (Exception(0, "Not enough memory to read configuration file"));
	}
		
	fread(buf, len, 1, f);
	fclose(f);
	
	string xmlConts(buf, len);
	delete [] buf;
	XML *config=xmlParser::parse(xmlConts);
	
	
	xmlNode *n = config->getNode("/[0]/Service/IP");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file - no Service/IP found"));
	}
	
	IP ip(n->getCdata());	

	n = config->getNode("/[0]/Service/Port");
	if(n==NULL)
	{
		throw(Exception("Invalid configuration file - no Service/IP found"));
	}
	
	int port = atoi(n->getCdata().c_str());
	this->addr=Address(ip, port);

	// No llegim del fitxer de config perque ja ho ha llegit el canis ;)
	this->type=cn->getType();
	

	delete config;

	db=new DBGateway(addr, type, cn);
	//pillem ja la llista de Bario
	
	string s("Select d.id, d.ip, d.puerto FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='Barix' AND m.codigo='Barionet' ");
	
	XML *devInfo=db->call(s, 0);

	n=devInfo->getNode("/result");
	int numCams= atoi((n->getParam("numRows")).c_str());
	
	cout<<"loading "<< numCams<<" devices"<<endl;

	for(int i=0;i<numCams;i++)
	{
		n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/id");
		int id= atoi(n->getCdata().c_str());
		n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/ip");
		
		string ip= n->getCdata().c_str();
		if(ip.find('/')!=string::npos) //eliminem mascara de xarxa
		{
			ip=ip.substr(0,ip.find('/'));
		}
		
		n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/puerto");
		int puerto = atoi(n->getCdata().c_str());
		
		Address a(IP(ip), puerto);

		device *d = new device(id);
		devs[ip] = d;

		cout<<"  dev:"<<ip<<" (:"<<puerto<<") id:"<<id<<endl;

	}
	delete devInfo;
	IntercomManager::lastReload.start();

//	cma=new ControlModuleAccess(cn);
//	ama=new AlarmModuleAccess(cn);
	ama=new AlarmModuleAccess(addr, type, cn);
	ama->startSession(-1);
	am=new AlarmManager(addr.getIP());	
	
	dtt = new DevTimeoutThread(this);
	dtt->start();

	cout<<"waiting for notifications at:"<<addr.toString()<<endl;
	this->socket=new SocketTCP(addr, SOCK_SERVE);
}

IntercomManager::~IntercomManager()
{
//	delete cma;
//	delete ama;
	delete db;
	delete am;
}


void* IntercomManager::execute(int, void* parm)
{
	SocketTCP *s = (SocketTCP*)parm;
	char recvBuf[256];
	//Address from = s->getAddr().toString();
	string from = s->getAddr().getIP().toString();

	cout<<"* run *"<<endl;
	try
	{
//		while(true)
//		{
			string cmd("");
//			while(cmd.length() == 0 || cmd[cmd.length()-1]!='\n')
			if(s->peek(recvBuf, 1) == 0)
			{
				cout<<" socket peek error, read==0"<<endl;
				s->closeSocket();
				delete s;
				return NULL;
			}
			while(cmd[cmd.length()-1]!='\n')
			{
				int r;
//				if((r=s->read(recvBuf, 256))>0)
				if((r=s->read(recvBuf, 1))>0)
				{
					cout<<".";cout.flush();
					cmd +=string(recvBuf, r);
//					cout<<"receiving command ("<<r<<"):"<<cmd<<endl;
				}
				else
				{
					cout<<" socket read error:"<<r<<endl;
					s->closeSocket();
					delete s;
					return NULL;
				}
			}
			cout<<"cmd received, closing"<<endl;
			s->closeSocket();
			//	cmd +=(recvBuf, r);
			cout<<" cmd:"<<cmd<<endl;
			// idIntercom,accio 0 trucada, 1 despenjar, 2 penjar

//			cout<<" recvd "<<cmd.length()<<" from "<<from<<endl;
//			from=Address(string("10.93.88.25"), 8080);
//			from=string("10.93.88.25");
			devList.rlock();
			device *d = devs[from];
			if(d != NULL)
			{
				devList.unlock();
				d->devTimeout.start();
				//string cmd=string(recvBuf, r);
				int pos=cmd.find(",");
				if(pos == string::npos)
				{
					cout<<"invalid message received:"<<cmd<<endl;
				}
				else
				{
					int intercomId = atoi(cmd.substr(0, pos).c_str()); //recvBuf[...]
					cmd=cmd.substr(pos+1);
					int pos2=cmd.find(",");
					int action = -2;
					int destId = 0;
					if(pos2 == string::npos)
					{
						action = atoi(cmd.c_str()); //recvBuf[...]
					}else
					{
						action = atoi(cmd.substr(0,pos2).c_str()); //recvBuf[...]
						destId = atoi(cmd.substr(pos2+1).c_str()); //recvBuf[...]
					}
//					cout<<" Intercom:"<<intercomId<<"@"<<from<<" : "<<action<<endl;
					if(d->ama == NULL)
						d->ama = new AlarmModuleAccess(addr, type, this->cn);
					if(!d->ama->sesStarted())
						try
						{
//							cout<<" starting AlarmModuleAccess session with "<<d->id<<endl;
							d->ama->startSession(d->id);
						}catch(Exception e)
						{
//							cout<<" error while starting session with "<<d->id<<"   :"<<e.getClass()<<" "<<e.getMsg()<<endl;
						}

					if(d->ama->sesStarted())
					{
						//AMAlarmValue av;
						//av.alarmId=d->aid;
						d->av.value = intercomId;
						 //-1 keepalive tcp
						if(action <0 || action > 2)
						{
							cout<<".";
							cout.flush();
							if(d->raised)
							{
								TimerInstant ti=d->timeout.time();
								if(ti.seconds()>= 5)
								{
									d->av.raised = false;
									cout<<"IntercomManager lower"<<endl;
									d->ama->setAlarm(d->av);
									d->raised=false;
								}
							}
							cout<<"IntercomManager keepalive"<<endl;
						}
						else
						{
							string extMsg("");
							switch(action)
							{
								case 0:	
									d->av.alarmId=d->call;
									extMsg="call";
									break;
								case 1:
									d->av.alarmId=d->answer;
									extMsg="answer";
									break;
								case 2:
									d->av.alarmId=d->hang;
									extMsg="hang";
									break;
							}
							
							string interc = string(".")+StrUtils::decToString(intercomId)+string(".")+StrUtils::decToString(destId);
							extMsg += interc;
							d->av.alarmId.strId+= interc + string(":")+this->addr.getIP().toString()+string("|")+StrUtils::decToString(this->addr.getPort())+string(":") + extMsg;
							cout<<" Intercom:"<<intercomId<<"@"<<from<<" : "<<action<<endl;
							cout<<"IntercomManager raise:"<<d->av.alarmId.strId<<endl;
							d->av.raised = true;
							d->ama->setAlarm(d->av);
							cout<<"IntercomManager::raised"<<endl;
							d->raised=true;
							d->timeout.start();
						}
					}
				}
			}
			else
			{
				devList.unlock();

				//device *d = devs[from];
				//if(d == NULL)
				reload.lock();
				TimerInstant ti = IntercomManager::lastReload.time();
				if(ti.seconds() > IntercomManager::minReloadTime)
				{
					try
					{
						cout<<"message from unknown device:"<<from<<", loading"<<endl;
						string s("Select d.id, d.ip, d.puerto FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='Barix' AND m.codigo='Barionet' ");//("AND d.ip = '"+from+"'");
						cout<<s<<endl;

						XML *devInfo=db->call(s, 0);

						xmlNode *n=devInfo->getNode("/result");
						int numCams= atoi((n->getParam("numRows")).c_str());

						cout<<"loading "<< numCams<<" devices"<<endl;//<<devInfo->toString()<<endl;

						devList.wlock();
						try
						{
							for(int i=0;i<numCams;i++)
							{
								n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/id");
								int id= atoi(n->getCdata().c_str());
								n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/ip");

								string ip= n->getCdata().c_str();
								if(ip.find('/')!=string::npos) //eliminem mascara de xarxa
								{
									ip=ip.substr(0,ip.find('/'));
								}

								n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/puerto");
								int puerto = atoi(n->getCdata().c_str());

								Address a(IP(ip), puerto);

								if (devs[ip] == NULL)
								{
									device *d = new device(id);
									devs[ip] = d;
									cout<<"  dev:"<<ip<<" (:"<<puerto<<") id:"<<id<<endl;
								}
								else
									cout<<"  (not adding dev:"<<ip<<" (:"<<puerto<<") id:"<<id<<")"<<endl;
							}
						}catch(...){}
						devList.unlock();
						delete devInfo;
					}catch(...){}
					IntercomManager::lastReload.start();
				}
				else
				{
					cout<<"message from unknown device:"<<from<<", not loading after:"<<ti.seconds()<<endl;
				}
				reload.unlock();
			}
//		}
	}
	catch (Exception &e)
	{
		cout << "IntercomManager::execute read" << e.getClass()
			<< ": " << e.getMsg() << endl;
		delete s;
		return NULL;
	}
				

	delete s;
	cout<<"* close *"<<endl;
	return NULL;
}

void IntercomManager::serve()
{
	sleep(1);
	int i=0;
	while(stayRunning)
	{
		// RPCPacket *r=NULL;
		//	int sz=s->peek(hdrBuf, 256)
		int sz;
		try{//DEBUG
			//				socket->setTimeout(15000);
			//sz=socket->read(recvBuf, 256, &from);

			SocketTCP *s;
			while ((s=socket->establish())!=NULL && stayRunning)
			{
				int i=start((void*)s);
				detach(i);
			}

		}catch(SocketTimeoutException se)
		{
			if(se.getMsg() != string("No bytes to read"))
			{
				cout<<"IntercomManager::serve SocketTimeoutException :"<<se.getMsg()<<endl;
			}
		//	r=NULL;
		}
	}
}

IntercomManager::DevTimeoutThread::DevTimeoutThread(IntercomManager *im):im(im)
{}

void* IntercomManager::DevTimeoutThread::execute(int, void*)
{
	while(im->stayRunning)
	{
		sleep(IntercomManager::devCheckTime);
		im->devList.rlock();
		try
		{
			for (map<string, device*>::iterator it=im->devs.begin() ; it != im->devs.end(); it++ )
			{
				device *dev = it->second;
				cout<<" checking device:"<<it->first<<" d:"<<(void*)dev<<endl;
				if(dev == NULL)
					continue;
				TimerInstant dti = dev->devTimeout.time();
				try
				{
					if(dti.seconds() > IntercomManager::devTimeout && !dev->devTimeoutRaised)
					{
						cout << "WARNING: lost notifications from device:" << it->first << " (" << dti.seconds() << endl;
						AMAlarmId alm;
						alm.devId = -1;
						alm.type = AMAlarmId::Filter;
						alm.strId = string("ModuleAlarm.IntercomManager.Device.")+StrUtils::decToString(dev->id)+string(":")+im->addr.getIP().toString()+string("|0:notification_lost.")+StrUtils::decToString(dev->id); // port 0 perqué l'IntercomManager en si no s'anuncia
						alm.intId = -1;

						AMAlarmValue av;
						av.value = dev->id;
						av.alarmId = alm;
						av.raised = true;

						cout << "raising... dev:"<<av.value<<endl;
						im->ama->setAlarm(av);
						dev->devTimeoutRaised=true;
					}else if(dev->devTimeoutRaised && dti.seconds() < IntercomManager::devTimeout)
					{
						cout << "recovered notifications from device:" << it->first << " (" << dti.seconds() << endl;
						AMAlarmId alm;
						alm.devId = -1;
						alm.type = AMAlarmId::Filter;
						alm.strId = string("ModuleAlarm.IntercomManager.Device.")+StrUtils::decToString(dev->id)+string(":")+im->addr.getIP().toString()+string("|0:notification_lost.")+StrUtils::decToString(dev->id); // port 0 perqué l'IntercomManager en si no s'anuncia
						cout<<"lowering:"<<alm.strId <<endl;
						alm.intId = -1;

						AMAlarmValue av;
						av.value = dev->id;
						av.alarmId = alm;
						av.raised = false;

						im->ama->setAlarm(av);
						dev->devTimeoutRaised=false;
						cout << "raising..."<<endl;
					}
				}
				catch(Exception &e)
				{
					cout<<" Exceptioin while raising alarm:"<<e.getClass()<<" : "<<e.getMsg();
				}
			}
		}
		catch(...){}
		im->devList.unlock();
	}
	return NULL;
}
