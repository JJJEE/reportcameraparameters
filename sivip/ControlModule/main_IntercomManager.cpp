#include <ControlModule/IntercomManager.h>
#include <Utils/Types.h>
#include <Utils/Canis.h>
#include <Utils/debugNew.h>
#include <Utils/debugStackTrace.h>
#include <stdio.h>
#include <signal.h>

#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <version.h>

#ifndef WIN32
void sigattention(int sig)
{
	cout << "Signal: " << sig << endl;

	if (sig==SIGSEGV || sig==SIGBUS || sig==SIGINT)
	{
		NEW_DUMPALLOCATEDMEMORYMAP();
		STACKTRACE_DUMPALL();
	}
	abort();
}
#endif

int main()
{
#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGINT, sigattention);
	signal(SIGSEGV, sigattention);
	signal(SIGBUS, sigattention);
#endif
#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigattention);
	signal(SIGSEGV, sigattention);
	signal(SIGBUS, sigattention);
#endif

	STACKTRACE_INSTRUMENT();

	cout << siriusRelease << endl;
	try
	{
		IntercomManager icm("IntercomManagerConfig.xml");
	
		cout << "Starting service..." << endl;
		icm.serve();
		
	}
	catch (Exception e)
	{
		cout << "Exception ocurred during initialization: " << e.getMsg() << endl;
	}

	return 0;
}

