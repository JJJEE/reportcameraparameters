#pragma once
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include "SonyControlPlugin.h"
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <map>
#include <list>
#include <iostream>
using namespace std;

typedef class SonyControlPlugin* foo;

class SonyControlPluginTouringThread: public Thread
{
	private:
		DBGateway *db;
		SonyControlPlugin *cm;
		int camId;
		int secs, hold;

		Condition c;

		class Call
		{
			public:
				int type;
				int data;
		};

	public:
		bool touring, recalling, movingCamera;

		SonyControlPluginTouringThread(SonyControlPlugin *cm, DBGateway *db, int id);

		void startTour(int id);
		bool checkActiveTour();
		void stopTour();
		void holdTour(int time);
		bool isTouring();

		void startRecall(int time);
		void checkRecall();
		void stopRecall();
		void resetRecall();
		void holdRecall(int time);
		bool isRecalling();

		void checkInit();
		void stop();
		void* execute(int, void*);
		void tour(int id);
		void recall(int time);
	protected:
		static bool compare_cparm(CPConfigParam a, CPConfigParam b);
};

