#include <Utils/Types.h>
#include <Utils/Exception.h>
#include <MitsubishiControlPlugin.h>
#include <signal.h>
#include <Utils/debugStackTrace.h>
#include <signal.h>
#include <version.h>

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMP();
	abort();
}

#endif

#ifndef WIN32
void sigpipe(int)
{
	cout << "SegPipe" << endl;
}

#endif
int main(int argc, char* argv[])
{
#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif

#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
#endif
#ifndef WIN32
	signal(SIGPIPE, sigpipe);
#endif
	bool reload=false;
	for(int i=1;i<argc;i++)
	{
		string p(argv[i]);

		if(p==string("-r"))
			reload=true;
	}
	bool exception;
	do
	{
		exception=false;
		try{
			MitsubishiControlPlugin plugin("MitsubishiControlPluginConfig.xml", reload);
	
			plugin.serve();
		}catch(Exception &e)
		{
			exception=true;
			cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
		}
	}while(exception);
	return 0;
}

