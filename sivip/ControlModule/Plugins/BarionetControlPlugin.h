/*********************************************************************
 * BarionetControlPlugin.h                                           *
 * -----------------------------------------------------------------	*
 *	Marina Eye-Cam Technologies, S.L. All rights reserved.				*
 *********************************************************************/
#ifndef BARIONETCONTROLPLUGIN_H
#define BARIONETCONTROLPLUGIN_H

#ifndef WIN32
#include <sys/time.h>
#else
#include <Utils/WindowsDefs.h>
#endif
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginControl.h>
#include <Plugins/GestorControlTypes.h>
#include <Utils/SessionDispatcher.h>
#include <Utils/ModelInfo.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <Sockets/SocketTCP.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <map>
#include <list>
#include <iostream>
using namespace std;

class BarionetControlPlugin: public PluginControl
{
private:
	static const int BARIONET = 0;
	enum interfaceType {  INPUT, OUTPUT, RELAY };

	map<string, SessionDispatcher> sessions;
	static const int sessionTimeout = 600;

	class DevInfo : public Thread
	{
	private:
		class Command
		{
			public:
				int 	id;
				void* data;
				Command( int id, 
							void* data ) : id( id ), 
												data( data ){}
		};

		struct WaitInfo
		{
			int 			i;
			Condition 	c;
			WaitInfo(){ i = 0; }
		};

		list<Command> 	buffer;
		Mutex 			bLock;
		int 				bufferSize;
		Condition 					queued;	// sirven a la variable bLock
		Condition 					full; 	// sirven a la variable bLock
		BarionetControlPlugin 	*cp;
		AlarmModuleAccess 		am;

		void init( 	CPDeviceID id, 
						BarionetControlPlugin *cp );

	public:
		struct timeval readTime; 
		class InputInfo
		{
			public:
				int digital;
				int analog;
				int temp;

				InputInfo()	:	digital(-1), 
									analog(-1), 
									temp(-1){}
		};

		class OutputInfo
		{
			public:
				int value;
				OutputInfo() :	value(-1){}
		};

		// TODO 20090310: Guardem fins a 5 thrIds, a lo guarro 
		bool 						threadRunning[5];
		int 						nin;
		int 						nout;
		int 						nrel;
		int 						errors;
		int 						*inputs;
		int 						*outputs;
		string 					*relays;
		Mutex 					callMutex;
		struct timeval 		lastReboot;
		bool 						initing;
		bool 						inited;
		bool 						error_logged;
		InputInfo 				*inputValues;
		OutputInfo 				*outputValues;
		CPDeviceID 				id;
		int 						model;
		Address 					cam;
		bool 						checkStarted;
		map<string, string> 	metadata;

		DevInfo(	CPDeviceID id, 
					Address cam, 
					int model, 
					int size, 
					BarionetControlPlugin *cp );
		void 	startCheckingThread();
		void 	queue(	int id, 
							void *data, 
							Address *keepAliveAddr = NULL );
		void 	wait(		Address *keepAliveAddr = NULL );
		void 	*execute(	int i, 
							void* params );
		int 	getSize();
		void 	setSize( int size );
		int 	getUse();
		float getPercentInUse();
		void 	checkUpdateIO();
		void 	updateIO();
	};
	
	map<int, DevInfo*> devs;

	static const int default_buffer_size = 5;
	
protected:

	class CamInit : public Thread
	{
		public:
		struct execInfo
		{
			int id;
			XML *conf;
			BarionetControlPlugin *bcp;
			execInfo(	int id, 
							XML *conf, 
							BarionetControlPlugin *bcp ) :	id(id), 
																		conf(conf), 
																		bcp(bcp){};
		};
		CamInit(){};
		void* execute(	int id, 
							void* parm );
	};
	
	CamInit 	ci;
	string 	configFile;
	XML 		*conf;

	inline SessionDispatcher getSession( Address a );

	void 			loadDBConfig(	CPDeviceID, 
										XML* conf );

	void 			loadParamValuesIntoDB( CPDeviceID &id );

	void 			loadASingleParamValueIntoDB( 	CPDeviceID &id,
															DBGateway *dbLoad,
															interfaceType deviceInterface );

	bool 			valueIsEmptyInDB( 	CPDeviceID &id, 
												DBGateway *dbLoad,
												string dbPath );

	void 			insertValueIntoDB(	CPDeviceID &id,
												DBGateway *dbLoad,
												string dbPath, 
												int numSignals );

	void 			loadModelConfig(	string model, 
											string nodepath, 
											string dbpath, 
											XML *conf );

	SocketTCP 	*devConnect( CPDeviceID id );

	string 		devCmd(	CPDeviceID id, 
								string command, 
								string prefix ); 

	string 		devCmd(	SocketTCP **socket, 
								string command, 
								string prefix ); 
			
	string 		getKeyValueFromAListOfValues(	string list, 
															string param); 
	
	void 			initializeDevInterfaces( 	CPDeviceID id,
														XML *conf, 
														string nodePath, 
														interfaceType deviceInterface );
public:
	DBGateway db;

	BarionetControlPlugin(	string file );
	~BarionetControlPlugin();
	
	virtual void 						startSession(	CPDeviceID id, 
																Address *a );

	virtual void 						endSession(	CPDeviceID id, 
															Address *a );

	virtual CPPTZ 						getPTZ(	Address *a ){};

	virtual void 						setPTZ(	CPPTZ ptz, 
														Address *a ){};

	void 									run_setPTZ(	CPPTZ ptz, 
															CPDeviceID id, 
															Address cam ){};

	virtual void 						setInput(	CPInput in, 
															Address *a );

	void 									run_setInput(){};

	virtual CPInput 					getInput(	CPInput in, 
															Address *a );

	virtual CPInput 					getInput(	CPInput in, 
															int devId );

	virtual void 						setOutput(	CPOutput out, 
															Address *a );

	void 									run_setOutput(	CPOutput in, 
																CPDeviceID id );

	virtual CPOutput 					getOutput(	CPOutput out, 	
															Address *a );

	virtual void 						setCommandBufferSize(	CPCommandBufferSize size, 
																			Address *a );

	virtual CPCommandBufferSize 	getCommandBufferSize(	Address *a );

	virtual float 						getCommandBufferPercentInUse(	Address *a );

	virtual int 						getCommandBufferCommandsInUse(	Address *a );

	virtual CPMetadata 				getMetadataValue(	CPMetadata data, 
																	Address *a );

	virtual void 						setMetadataValue(	CPMetadata data, 
																	Address *a );

	XML* getConfigFromFile( string file );
	
	virtual CPConfigParam 			getConfigParam(	CPConfigParam p, 
																	Address *a );

	virtual CPConfigParamSeq 		getConfigParamRecursive(	CPConfigParam p, 
																				Address *a );
	virtual void 						setConfigParam(	CPConfigParam p, 
																	Address *a );

	void 									run_setConfigParam(	CPConfigParam p, 
																		CPDeviceID id );

	virtual CPConfigParamSeq 		getAllConfigParams(	Address *a );
	
	virtual void 						move(	CPMovement move, Address *a ){};
};

#endif