#include <Utils/StrUtils.h>
#include "SonyControlPluginTouringThread.h"
#ifdef WIN32
#include <Utils/WindowsDefs.h>
#endif
#include <string.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>

//#define DEBUG
#undef DEBUG

SonyControlPluginTouringThread::SonyControlPluginTouringThread(SonyControlPlugin *cm, DBGateway *db, int id):
	cm(cm), touring(false), recalling(false), movingCamera(false), db(db), camId(id), secs(0), hold(0)
{}

void* SonyControlPluginTouringThread::execute(int, void* data)
{
#ifdef DEBUG
	cout << "-----> SCPTT::execute" << endl;
#endif
	Call *call = (Call*) data;
	if(call->type == 0)
	{
		c.lock();

		movingCamera = false;

		if(recalling)
		{
			c.unlock();
			stop();
			c.lock();
		}
		if(!touring)
		{
			c.unlock();
			touring=true;
			tour(call->data);
		}
		else
		{
			c.unlock();
			cout<<" ************** tour thread already started ***********"<<endl;
		}
	}
	else
	{
		c.lock();
		if(touring)
		{
			c.unlock();
			stop();
			c.lock();
		}
		if(!recalling)
		{
			c.unlock();
			recalling=true;
			recall(call->data);
		}
		else
		{
			c.unlock();
			cout<<" ************** recall thread already started ***********"<<endl;
		}
	}

	return NULL;
}

void SonyControlPluginTouringThread::startTour(int id )
{
#ifdef DEBUG
	cout << " SCPTT::StartTour -----------" << endl;
#endif

	c.lock();
	if(touring||recalling)
	{
		while ( movingCamera )
		{
			stop();
		}
		
		c.unlock();

		//sleep(1);
	}else
	{
		c.unlock();
	}
	hold=0;
	Call *call = new Call();
	call->type=0;
	call->data=id;
	int tid = start((void*) call);
	detach(tid);
}

void SonyControlPluginTouringThread::startRecall(int time )
{
#ifdef DEBUG
	cout << " SCPTT::StartRecall(" << time << ") rec:" << recalling << " -----------" << endl;
#endif
	c.lock();

	if(recalling)
	{
		stop();
	}
	c.unlock();

	if(!touring)
	{
		Call *call = new Call();
		call->type=1;
		call->data=time;
		int tid = start((void*) call);
		detach(tid);
	}
}

void SonyControlPluginTouringThread::checkInit()
{
	if(recalling||touring)
		return;
	if(!checkActiveTour())
		checkRecall();
}

bool SonyControlPluginTouringThread::checkActiveTour()
{
#ifdef DEBUG
	cout << " SCPTT::checkRecall-----------" << endl;
#endif

	if(recalling||touring)
		return false;

	bool started = false;
	try
	{
		CPConfigParam cp("/Tours/Active","");
		CPConfigParam setting= cm->getConfigParam(camId, cp);

		if(setting.value != string(""))
		{
			cout<<"SCPTT::checkActiveTour() Active tour found:"<<setting.value<<endl;
			int i=atoi(setting.value.c_str());
			if(i>0)
			{
				started=true;
				startTour(i);
			}
		}
	}catch(Exception &e)
	{
		cout<<"TourThread::checkTour["<<camId<<"]:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
	}
	catch(...)
	{
		cout<<"TourThread::checkTour["<<camId<<"] unknown exception"<<endl;
	}
	return started;
}

void SonyControlPluginTouringThread::checkRecall()
{
#ifdef DEBUG
	cout<<" SCPTT::checkRecall-----------"<<endl;
#endif

	if(recalling||touring)
		return;

	try
	{
		CPConfigParam cp("/Presets/0/RecallTime","");
		CPConfigParam setting= cm->getConfigParam(camId, cp);

		if(setting.value != string(""))
		{
			int i=atoi(setting.value.c_str());
			if(i>0)
			{
				startRecall(i);
			}
		}
	}catch(Exception &e)
	{
		cout<<"TourThread::checkRecall["<<camId<<"]:"<<e.getClass()<<"::"<<e.getMsg()<<endl;
	}
	catch(...)
	{
		cout<<"TourThread::checkRecall["<<camId<<"] unknown exception"<<endl;
	}
}

void SonyControlPluginTouringThread::resetRecall()
{
#ifdef DEBUG
	cout<<" SCPTT::resetRecall -----------"<<endl;
#endif

	c.lock();
	secs=0;
	c.unlock();
}

void SonyControlPluginTouringThread::stop()
{
#ifdef DEBUG
	cout<<" SCPTT::stop -----------"<<endl;
#endif

	c.lock();
	if(touring || recalling)
	{
		touring=false;
		recalling=false;
		c.signal();
	}
	c.unlock();
}

void SonyControlPluginTouringThread::stopTour()
{
	c.lock();
	if(touring)
	{
#ifdef DEBUG
		cout<<" SCPTT::stopping Tour -----------"<<endl;
#endif
		touring=false;
		c.signal();
	}
	c.unlock();
}

void SonyControlPluginTouringThread::stopRecall()
{
	c.lock();
	if(recalling)
	{
#ifdef DEBUG
		cout<<" SCPTT::stopping Recall-----------"<<endl;
#endif
		recalling=false;
		c.signal();
	}
	c.unlock();
}

bool SonyControlPluginTouringThread::compare_cparm(CPConfigParam a, CPConfigParam b) 
{
	const char *ca=a.path.c_str();
	const char *cb=b.path.c_str();
	int ia=a.path.length(), ib=b.path.length();
	int i=0;
	while(i<ia && i<ib )
	{
		if(isdigit(ca[i]) && isdigit(cb[i]))
		{
			char *enda, *endb;
			int inta=strtol(ca+i,&enda,0);
			int intb=strtol(cb+i,&endb,0);
			if(inta != intb)
			{
				return inta<intb;
			}

			if((enda-ca)<(endb-cb))
				i=(enda-ca);
			else
				i=(endb-cb);
		}
		else if(ca[i]!=cb[i])
		{

			return ca[i]<cb[i];
		}
		i++;	
	}

	return ia<ib;
}

void SonyControlPluginTouringThread::tour(int id)
{
#ifdef DEBUG
	cout << " SCPTT::tour -----------" << (void*)db << endl;
#endif
	string path = string("/Tours/") + StrUtils::decToString(id);
	
	int npos = 1;
	cout << "SCPTT::Tour[" << dec << camId << "] " << path << endl;

	CPConfigParam cp(path, "");
	CPConfigParamSeq tour;

	try
	{
		tour = cm->getConfigParamRecursive(cp, camId, NULL);
	}
	catch(Exception &e)
	{
		cout<<"TourThread::Tour["<<camId<<"] recall load tour "<<id<<":"<<e.getClass()<<"::"<<e.getMsg()<<endl;
		return;
	}
	catch(...)
	{
		cout<<"TourThread::Tour["<<camId<<"] recall load tour "<<id<<" unknown exception"<<endl;
		return;
	}

	cout<<" SCPTT::Tour["<<dec<<camId<<"] size:"<<tour.size()<<endl;

	tour.sort(SonyControlPluginTouringThread::compare_cparm);

	for(list<CPConfigParam>::iterator lIt = tour.begin(); lIt!=tour.end(); ++lIt)
	{
		CPConfigParam cp = *lIt;
		cout<<"Tour["<<dec<<camId<<"] "<<id<<":"<<cp.path<<":"<<cp.value<<endl;
	}

	if( tour.size() <= 1 ) // no hi ha cap posicio :P
	{
		cout << "Tour[" << dec << camId << "] " << id << " no positions found in tour" << endl;
		return;
	}

	list<CPConfigParam>::iterator lIt = tour.begin();
	++lIt;
	
	// la config es:

	// /Tours/1
	// /Tours/1/1
	// /Tours/1/1/Id:1
	// /Tours/1/1/Speed:1
	// /Tours/1/1/StayTime:10
	// /Tours/1/2
	// /Tours/1/2/Id:2
	// /Tours/1/2/Speed:1
	// /Tours/1/2/StayTime:10

	// i el sort() ens garantitza que els trobar�m en l'ordre correcte

	try
	{
		db->setDeviceConfigParam(camId, "/Tours/Active", StrUtils::decToString(id));
	}
	catch(Exception &e)
	{
		cout<<"TourThread::Tour["<<camId<<"] /Tours/Active = "<<id<<":"<<e.getClass()<<"::"<<e.getMsg()<<endl;
	}
	catch(...)
	{
		cout<<"TourThread::Tour["<<camId<<"] /Tours/Active = "<<id<<": unknown exception"<<endl;
	}

	int loaded = 0;

	while( touring )
	{
		if( lIt == tour.end() )
		{
			if(loaded <= 1) //posicions carregades correctament <= 1 
			{
				cout<<"Tour["<<dec<<camId<<"] "<<id<<" : "<<loaded<<" positions correctly loaded, exiting..."<<endl;
				break;
			}
			loaded = 0;
			lIt = tour.begin();
			++lIt;
		}

		string basePath = (*lIt).path+string("/");
		++lIt;

		CPConfigParam cpid;
		CPConfigParam cpspeed;
		CPConfigParam cpstayTime;
		int id;
		float speed;
		float stayTime;

		while( lIt != tour.end() ) 
		{
			CPConfigParam cp = *lIt;
			
			if(cp.path.find(basePath) != 0) //hem cambiat de preset que carreguem
			{
				cout<<"Tour["<<dec<<camId<<"] break:"<<cp.path<<" -> "<<basePath<<endl;
				break;
			}

			if( cp.path == ( basePath + "Id" ) )
			{ 
				cout << "Id: " << cp.path << " ," << cp.value << endl;
				char *end = NULL;
				id = strtol(cp.value.c_str(), &end, 0);

				if ( end > cp.value.c_str() )
				{
					cpid = cp;
				}
			}
			else if( cp.path == ( basePath + "Speed" ) )
			{
				cout << "Speed: " << cp.path << " ," << cp.value << endl;
				speed = atof( cp.value.c_str() );

				if(speed != 0.0)
				{
					cpspeed = cp;
				}
			}
			else if( cp.path == ( basePath + "StayTime" ) )
			{
				cout << "Stay: " << cp.path << " ," << cp.value << endl;
				stayTime = atof(cp.value.c_str());

				if(stayTime != 0.0)
				{
					cpstayTime = cp;
				}
			}
			else
				cout << "None: " << cp.path << " ," << cp.value << " pref:" << basePath << endl;

			++lIt;
		}

		if(	( cpid.path != "" )  	&& 
				( cpspeed.path != "" ) 	&& 
				( cpstayTime.path != "" ) 	)
		{
			cout << "Tour[" << dec << camId << "] next preset: " << cpid.value << " speed:" << cpspeed.value << " time:" << cpstayTime.value << endl;
			++loaded;
			CPConfigParam s("/Speed", StrUtils::floatToString(speed));
			CPConfigParam mv("/Presets/" + StrUtils::decToString(id) + "/Recall", "1");

			//Fem trampa i remenem directament la BD, s'hauria de fer amb el get/setCP
			try
			{
				string prevSp = db->getDeviceConfigParam(camId, "/Speed");
				db->setDeviceConfigParam(camId, "/Speed", StrUtils::floatToString(speed));
				movingCamera = true;
				cm->run_setConfigParam(mv, camId, true);
				db->setDeviceConfigParam(camId, "/Speed", prevSp);
			}
			catch(Exception &e)
			{
				cout << "TourThread::Tour[" << camId << "] recall preset " << id << ":" << e.getClass() << "::" << e.getMsg() << endl;
			}
			catch(...)
			{
				cout << "TourThread::Tour[" << camId << "] recall preset " << id << " unknown exception" << endl;
			}

			int secs = 0;

			while( ( secs<stayTime || hold>0 ) && touring )
			{
				sleep(1);
				c.lock();

				++secs;

				if(hold>0)
					--hold;

				c.unlock();
			}
		}
		else if(cpid.path != string("") || cpspeed.path != string("") || cpstayTime.path != string("")) 
		{
			cout << "Tour[" << dec << camId << "] position load error:" << cpid.path << "=" << cpid.value << ", " << cpspeed.path << "=" << cpspeed.value << ", " << cpstayTime.path << "=" << cpstayTime.value << endl;
		}

		movingCamera = false;
	}
	try
	{
		db->setDeviceConfigParam(camId, "/Tours/Active", "");
	}
	catch(Exception &e)
	{
		cout<<"TourThread::Tour["<<camId<<"] /Tours/Active = "<<id<<":"<<e.getClass()<<"::"<<e.getMsg()<<endl;
	}
	catch(...)
	{
		cout<<"TourThread::Tour["<<camId<<"] /Tours/Active = "<<id<<": unknown exception"<<endl;
	}
}

void SonyControlPluginTouringThread::holdRecall(int time)
{
	c.lock();
	this->hold = time;
	c.unlock();
}

void SonyControlPluginTouringThread::holdTour(int time)
{
	c.lock();
	this->hold = time;
	c.unlock();
}

void SonyControlPluginTouringThread::recall(int time)
{

	secs=0;
	while(recalling)
	{
		sleep(1);
		c.lock();
		++secs;

		if(hold>0)
			--hold;
		c.unlock();
		cout<<"recall thread:"<<secs<<"/"<<time<<" hold:"<<hold<<endl;
		if(secs>=time && hold==0)
		{
			CPConfigParam s("/Presets/0/Recall", ""); 
			try
			{
				cm->run_setConfigParam(s, camId);
			}
			catch(Exception &e)
			{
				cout << "TourThread::Recall[" << camId << "]:" << e.getClass() << "::" << e.getMsg() << endl;
			}
			catch(...)
			{
				cout << "TourThread::Recall[" << camId << "] unknown exception" << endl;
			}
			recalling=false;
			return;
		}
	}
}

bool SonyControlPluginTouringThread::isTouring()
{
	return touring;
}

bool SonyControlPluginTouringThread::isRecalling()
{
	return recalling;	
}

