#include <Plugins/GestorControlExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <MitsubishiControlPlugin.h>
#include <ControlModule/ControlModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

MitsubishiControlPlugin::DevInfo::DevInfo(CPDeviceID id, Address cam, string modelStr, int size, MitsubishiControlPlugin *cp): bufferSize(size),
	id(id), cam(cam), modelStr(modelStr), queued(&bLock), full(&bLock), cp(cp), setx(0), sety(0), initing(false)
{
	if(modelStr==string("NM-C150SD"))
	{
		model=NMC150SD;
	}
	else
	if(modelStr==string("NM-C130FD"))
	{
		model=NMC130FD;
	}
	else
	if(modelStr==string("NM-C110"))
	{
		model=NMC110;
	}
	else
	{
		this->modelStr=string("NM-C150SD");
		model=NMC150SD;
	}
	start(NULL);
}

void MitsubishiControlPlugin::DevInfo::queue(int id, void *data, Address *keepAliveAddr)
{

	full.lock();
	try
	{
/*		while(buffer.size()>bufferSize)
		{
	//	full.wait();
			struct timespec t;
			t.tv_sec=time(NULL)+4;
			t.tv_nsec=0;
			if(full.wait(t))
			{
//				cout<<" timeout queue full "<<this->id.id<<" devInfo.queue addr:"<<keepAliveAddr<<" buf sz:"<<buffer.size()<<" max:"<<bufferSize<<" retr:"<<retries<<endl;
				if(keepAliveAddr!=NULL)
				{
//					cout<<"BCP::DevInfo::queue - keepalive cua getRPC ->"<<keepAliveAddr->toString()<<endl;
					RPC::keepAlive(keepAliveAddr, 0);
				}
			}
		}
		
		if(id==CPServ::setPTZ)
			for(list<Command>::iterator it=buffer.begin(); it!=buffer.end(); it++)
				if(it->id==CPServ::setPTZ)
					it=buffer.erase(it);
*/
		buffer.push_back(Command(id, data));
		if(buffer.size()>1)
		{
			cout<<" queued command:"<<id<<" queue size:"<<buffer.size()<<endl;
		}
	}catch(...){}
	
	full.unlock();
	
	queued.signal();
}

int MitsubishiControlPlugin::DevInfo::getSize()
{
	return bufferSize;
}

void MitsubishiControlPlugin::DevInfo::setSize(int size)
{
	if(size>0)
		bufferSize=size;
}

int MitsubishiControlPlugin::DevInfo::getUse()
{
	int i= buffer.size();
	if(i>bufferSize)
		i=bufferSize;
	return i;
}

float MitsubishiControlPlugin::DevInfo::getPercentInUse()
{
	float f=buffer.size()/bufferSize;
	if(f>1)
		f=1;
	return f;
}

void* MitsubishiControlPlugin::DevInfo::execute(int, void *data)
{
	for(;;)
	{
		try
		{
			queued.lock(); 
			Command c(-2,NULL);
			try{
				while(buffer.size()==0)
				{
//				cout<<" queue get "<<id.id<<" 1"<<endl;
					full.signal();

					struct timespec t;
					t.tv_sec=time(NULL)+1;
					t.tv_nsec=0;
					queued.wait(t);
				}
//				cout<<" queue get "<<id.id<<" 2"<<endl;
				c=buffer.front();
				buffer.pop_front();
//				cout<<" queue get "<<id.id<<" 3:"<<buffer.size()<<endl;
			}catch(...){}

			full.signal();

			queued.unlock();

			switch(c.id)
			{
				case CPServ::setConfigParam:
//					cout<<"exec queued command config"<<endl;
					try{
					cp->run_setConfigParam(*((CPConfigParam*)c.data), id, cam);
					}catch(...){}
					delete (CPConfigParam*)c.data;
					break;
				case CPServ::setOutput:
//					cout<<"exec queued command config"<<endl;
					try{
					cp->run_setOutput(*((CPOutput*)c.data), id, cam);
					}catch(...){}
					delete (CPOutput*)c.data;
					break;
				case CPServ::setInput:
//					cout<<"exec queued command config"<<endl;
					try{
					cp->run_setInput(*((CPInput*)c.data), id, cam);
					}catch(...){}
					delete (CPInput*)c.data;
					break;
				case -1:
//					cout<<"exec queued command wait signal:"<<(void*)c.data<<endl;
					((WaitInfo*)c.data)->i=1;
					((WaitInfo*)c.data)->c.lock();
					((WaitInfo*)c.data)->c.signal();
					((WaitInfo*)c.data)->c.unlock();
					break;
				default:
					cout << " ERROR: incorrect queued command id"<<endl;
			}
		}catch(...){}

		if(buffer.size()+1<=bufferSize)
			full.signal();
	}
}


void MitsubishiControlPlugin::DevInfo::wait(Address *keepAliveAddr)
{
	return;
	WaitInfo *w=new WaitInfo();


	queue(-1, w, keepAliveAddr);
//	cout<<" wait queued addr:"<<(void*)keepAliveAddr<<endl;
	w->c.lock();
	try
	{
		while(w->i==0)
		{
			//	w.c.wait();
			struct timespec t;
			t.tv_sec=time(NULL)+4;
			t.tv_nsec=0;
			if(w->c.wait(t))
			{
//				cout<<" timeout wait queue addr:"<<keepAliveAddr<<" retr:"<</*retries<<*/endl;
//				if(w->i!=0)
//					cout<<"++++++++++Should have received Signal!!! "<<(void*)w<<"++++++++"<<endl;
				if(keepAliveAddr!=NULL)
				{
					cout<<" keepalive cua getRPC ->"<<keepAliveAddr->toString()<<endl;
					RPC::keepAlive(keepAliveAddr, 0);
				}
			}
//			else
//				cout<<" wait queue addr signal received"<<(void*)w<<endl;
		}
	}catch(...){}

	w->c.unlock();
	delete w;
}

void* MitsubishiControlPlugin::CamInitStarterThread::execute(int, void*)
{
//	scp->ci.setNewThreadPriority(Thread::defaultPrio-10);

	cout<<"MitsubishiControlPlugin::CamInitStarterThread::execute starting "<<(void*)scp<<endl;
	cout<<"MitsubishiControlPlugin::CamInitStarterThread::execute starting "<<scp->devs.size()<<" threads"<<endl;
	for(map<int, DevInfo*>::iterator it=scp->devs.begin(); it!=scp->devs.end(); it++)
	{
		DevInfo *inf= it->second;
		try
		{	
//			loadSchedules(id);
			cout<<" CamInit:"<<inf->id.id<<" create ExecInfo"<<endl;
			int cid=inf->id.id;
			CamInit::execInfo *ei = new MitsubishiControlPlugin::CamInit::execInfo(cid, conf, inf->modelStr, scp, reload);
			//cout<<" CamInit:"<<inf->id<<endl;
			int thid=scp->ci.start(ei);
		//	ci.changeThreadPriority(thid, -5);
			cout<<" CamInit:"<<inf->id.id<<" started"<<endl;
			//scp->ci.join(thid);//descomentar pq sigui sequencial (debug)
			scp->ci.detach(thid);
			//cout<<" loading cam "<<inf->id<<" CamInit started"<<endl;
		}
		catch(SocketTimeoutException &e)
		{
			//no s'ha trobat la camera?
			cout<<"MCP:: loadDBConfig: could not connect to cam "<<inf->id.id<<" SocketTimeoutException:"<<e.getMsg()<<endl;
		//	devs.erase(id);
		}
		catch(SocketException &e)
		{
			//no s'ha trobat la camera?
			cout<<"MCP:: loadDBConfig: could not connect to cam "<<inf->id.id<<" SocketException:"<<e.getMsg()<<endl;
		//	devs.erase(id);
		}
		catch(Exception &e)
		{
			//no s'ha trobat la camera?
			cout<<"MCP:: loadDBConfig: could not connect to cam "<<inf->id.id<<" Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		//	devs.erase(id);
		}
	}
	return NULL;
}




MitsubishiControlPlugin::MitsubishiControlPlugin(string file, bool reloadConfig):PluginControl(file), db(address, type, cn)
{
	XML *conf = getConfigFromFile( file );

	configFile=conf->getNodeData("[0]/ModelConfigFile");

	delete conf;
	if(configFile==string(""))
		throw(Exception("No Model config file specified in configuration file"));
	cout<<"ConfigFile:"<<configFile<<endl;

	conf = getConfigFromFile( configFile );

	string s("Select d.id, d.ip, d.puerto, m.codigo FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='Mitsubishi' ");//AND m.codigo='Barionet' ");
	
	try
	{
		db.begin();
	}
	catch(Exception &e)
	{
		cout<<"db.begin Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
	}
	XML *devInfo=db.call(s, 0);
	try
	{
		db.commit();
	}
	catch(Exception &e)
	{
		cout<<"db.commit Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
	}

	xmlNode *n=devInfo->getNode("/result");
	int numCams= atoi((n->getParam("numRows")).c_str());
	
	cout<<"load config model:"<< numCams<<endl;
	loadModelConfig("NM-C150SD", "/", "/", conf, reloadConfig);
	loadModelConfig("NM-C130FD", "/", "/", conf, reloadConfig);
	loadModelConfig("NM-C110", "/", "/", conf, reloadConfig);

	cout<<"loading "<< numCams<<" devices "<<endl;

	for(int i=0;i<numCams;i++)
	{
		n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/id");
		int id= atoi(n->getCdata().c_str());
		n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/ip");
		
		string ip= n->getCdata().c_str();
		if(ip.find('/')!=string::npos) //eliminem mascara de xarxa
		{
			ip=ip.substr(0,ip.find('/'));
		}
		
		n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/puerto");
		int puerto = atoi(n->getCdata().c_str());
		
		n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/codigo");
		string model=n->getCdata();
		
		Address a(IP(ip), puerto);
		cout<<"address: "<< a.toString()<<endl;

		int mod=-1;

		devs[id]= new DevInfo(id, a, model, default_buffer_size, this);
			
		cist.conf=conf;
		cist.scp=this;
		cist.reload=reloadConfig;

	}

	delete devInfo;
	delete conf;
	cout<<"----------plugin inicialitzat----------"<<endl;
}

XML* MitsubishiControlPlugin::getConfigFromFile( string file )
{
#ifdef DEBUG
	cout << "-----> MitsubishiCP::getConfigFromFile: " << file << endl;
#endif
	
	char* buffer(NULL);
	std::size_t size(0);

	ifstream fichero( file.c_str(), ifstream::in );

	if ( fichero.is_open() )
	{
  		filebuf *pbuffer = fichero.rdbuf();
  		size = pbuffer->pubseekoff (0, fichero.end, fichero.in);
  		pbuffer->pubseekpos (0, fichero.in);

  		buffer = new char[size];
  		pbuffer->sgetn (buffer, size);
	}
	else
		cout << "Error opening file '" << file.c_str() << endl;

	fichero.close();

	XML *conf = xmlParser::parse( string( buffer, size ) );

	delete buffer;
	buffer = NULL;

#ifdef DEBUG
	cout << "<----- MitsubishiCP::getConfigFromFile" <<  endl;
#endif

	return conf;
}

MitsubishiControlPlugin::~MitsubishiControlPlugin()
{
	for(map<int, DevInfo*>::iterator it=devs.begin(); it!=devs.end(); it++)
	{
		if(it->second!=NULL)
			delete it->second;
	}
}

void MitsubishiControlPlugin::serve()
{
	cist.scp=this;
	cout<<"cist.start():"<<(void*)cist.scp<<endl;
	cist.start();
	Plugin::serve();
}

void MitsubishiControlPlugin::loadModelConfig(string model, string nodepath, string dbpath, XML *conf, bool reload)
{
	if(nodepath==string("/"))
		nodepath=string("/[0]/Models/All/");

	xmlNode *n = conf->getNode(nodepath);

	list<xmlNode*> kids=n->getKidList();
	int pos=nodepath.find("All");
	if(kids.size()>0)
	{
		if(pos==string::npos) //especifica del model
		{
			int dbidx=0;
			for(list<xmlNode*>::iterator it=kids.begin();it!=kids.end(); it++)
			{
				if((*it)->getParam("nodename")==string("inc"))
				{
					loadModelConfig(model, nodepath+(*it)->getName()+string("[")+StrUtils::decToString(dbidx)+string("]")+string("/"), dbpath+StrUtils::decToString(dbidx+1)+string("/"), conf, reload);
					dbidx++;
				}
				else
				{
					loadModelConfig(model, nodepath+(*it)->getName()+string("/"), dbpath+(*it)->getName()+string("/"), conf, reload);
				}
			}
		}
		else
		{	//config generica, hem de tenir en compte el model
			bool inc=false;
			int nidx=0;
			for(list<xmlNode*>::iterator it=kids.begin();it!=kids.end(); it++)
			{
				if((*it)->getParam("nodename")==string("inc"))
					inc=true;
				else
				{
					loadModelConfig(model, nodepath+(*it)->getName()+string("/"), dbpath+(*it)->getName()+string("/"), conf, reload);
				}
				nidx++;
			}

			if(inc)
			{
				string mpath=nodepath;
				mpath.replace(pos,3, model);
				xmlNode *m = conf->getNode(mpath);
				inc=false;
				if(m!=NULL)
				{
					list<xmlNode*> mkids=m->getKidList();
					int dbidx=0;
					for(list<xmlNode*>::iterator it=mkids.begin();it!=mkids.end(); it++) //mirem si tenim incs especifics del model
					{
						if((*it)->getParam("nodename")==string("inc"))
						{
							loadModelConfig(model, mpath+(*it)->getName()+string("[")+StrUtils::decToString(dbidx)+string("]")+string("/"), dbpath+StrUtils::decToString(dbidx+1)+string("/"), conf, reload);
							dbidx++;

							inc=true;
						}
					}
				}

				if(!inc) //si no en tenim, pillem els generics
				{
					int dbidx=0;
					for(list<xmlNode*>::iterator it=kids.begin();it!=kids.end(); it++)
					{
						if((*it)->getParam("nodename")==string("inc"))
						{
							loadModelConfig(model, nodepath+(*it)->getName()+string("[")+StrUtils::decToString(dbidx)+string("]")+string("/"), dbpath+StrUtils::decToString(dbidx+1)+string("/"), conf, reload);
							dbidx++;
						}
					}
				}
			}
		}
	}
	else
	{
		if(pos!=string::npos) //especifica del model
		{
			string mpath=nodepath;
			mpath.replace(pos,3, model);
			xmlNode *m = conf->getNode(mpath);

			if(m!=NULL)
				n=m;
		}
		string s=db.getModelConfigParam("Mitsubishi", model, dbpath);
		if(reload || s != n->getCdata() || s==string(""))
		{
			db.setModelConfigParam("Mitsubishi", model, dbpath, n->getCdata());
		}
	}
}

SessionDispatcher MitsubishiControlPlugin::getSession(Address a)
{
	string k=a.toString();
	map<string,SessionDispatcher>::iterator i=sessions.find(k);
	
	if (i==sessions.end())
	{
		cout<<"BarionetCP::dwiSession Not Stablished "<<endl;
		throw (CPSessionNotStablishedException(0, string("Session no establerta")));
	}
	i->second.initializeTimeOfSession();
	return i->second;
}


void* MitsubishiControlPlugin::CamInit::execute(int thid, void *parm)
{
	cout<<"    CamInit::execute "<<endl;
	execInfo *i=(execInfo*) parm;

	int id=i->id;
	if(i->bcp->devs[id]->initing==false)
	{
		i->bcp->devs[id]->initing=true;
		try
		{
			XML *conf=i->conf; 
			string model=i->model; bool reload=i->reload;
			cout<<" "<<thid<<"   1 CamInit::execute "<<id<<endl;
			i->bcp->loadDBConfig(id, conf, model, reload);
			cout<<" "<<thid<<"   2 CamInit::loaded config: "<<id<<endl;//" loading Schedules"<<endl;
			i->bcp->devs[id]->inited=true;
			i->bcp->devs[id]->error_logged=false;

			//comprova que el GOV no estigui massa baix

			string command=string("http://")+ i->bcp->devs[id]->cam.getIP().toString()+string("/admin/mpeg4.html?app=get");
			int gov=0;
			try{
				HttpClient cl(command);
				HttpStream *stream=cl.sendRequest(string("GET"));
				string body =stream->getNextInformationChunk();
				body=stream->getNextInformationChunk();
				gov = atoi(getParamValue(body, "mpeg4_gov").c_str());
				delete stream;
				if(gov<10)
				{
					string command=string("http://")+ i->bcp->devs[id]->cam.getIP().toString()+string("/admin/mpeg4.html?mpeg4_gov=24");//"/mpeg4.html?app=get");
					cout<<" cam:"<<i->bcp->devs[id]->cam.getIP().toString()<<" gov size:"<<gov<<", forcing gov=24"<<endl<<command<<endl;
					try{
						HttpClient cl(command);
						HttpStream *stream=cl.sendRequest(string("GET"));
						delete stream;
					}catch(Exception e){
						cout<<"command Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
					}
				}
			}catch(Exception e){
				cout<<"command Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			}



			if(reload)
			{
				//desactiva la baixada d'FPS quan no hi ha moviment
				string command=string("http://")+ i->bcp->devs[id]->cam.getIP().toString()+string("/admin/alarm.html?md_down_fps=0");
				try{
					HttpClient cl(command);
					HttpStream *stream=cl.sendRequest(string("GET"));
					delete stream;
				}catch(Exception e){
					cout<<"command Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
				}

				// Activa la detecció de moviment a tota la imatge
				 /*
				string command=string("http://")+ i->bcp->devs[id]->cam.getIP().toString()+string("/admin/alarm.html?md_enable=1&md_mask=0xFFFFFFFFFFFFFFFF");
				try{
					HttpClient cl(command);
					HttpStream *stream=cl.sendRequest(string("GET"));
					delete stream;
				}catch(Exception e){
					cout<<"command Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
				}
				*/
			}

			//	i->bcp->loadSchedules(id);
	//			cout<<" "<<thid<<"   3 CamInit::loaded Schedules"<<id<<":"<<model<<endl;
		}
		catch(Exception &e)
		{
			cout<<" ---------- --------- ------------- BCP::CamInit::exec: "<<i->id<<" Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
/*			if(!i->bcp->devs[id]->error_logged)
			{
				RPC* dbrpc=i->bcp->db.getRPC();
				try
				{
				//	XML *res=i->bcp->db.call("select id from loglevel where nombre='Errors'", dbrpc);
				//	xmlNode *n=res->getNode("/result/[0]/id");
				//	if(n!=NULL)
				//	{
				//		int level=atoi(n->getCdata().c_str());
				//		i->bcp->db.logDev("Error connecting to device",id, level, dbrpc);
				//	}

					i->bcp->db.logDev("Error connecting to device",id, "Errors", dbrpc);
				}catch(...)
				{}
				i->bcp->db.freeRPC(dbrpc);
				i->bcp->devs[id]->error_logged=true;
			}*/
		}
		i->bcp->devs[id]->initing=false;
	}
	delete i;
	return NULL;
}

void MitsubishiControlPlugin::loadDBConfig(CPDeviceID id, XML *conf, string model, bool reload)
{
	cout<<" --- loadDBConfig :"<<StrUtils::decToString(id.id)<<" rel:"<<reload<<endl;
	if(db.getDeviceConfigParam(id.id, string("/Video/1/Resolution/x"))==string("") || reload)
	{

		db.setDeviceConfigParam(id.id, string("/Video/1/Codec/Fourcc"), string("mp4v"));
		db.setDeviceConfigParam(id.id, string("/Video/1/Codec/Bitrate"), string("1024"));
		db.setDeviceConfigParam(id.id, string("/Video/1/FPS"), string("25"));
		db.setDeviceConfigParam(id.id, string("/Video/Standard"), string("PAL"));

		string command;
		if(model == "NM-C150SD" || model == "NM-C130FD")
		{
			db.setDeviceConfigParam(id.id, string("/Video/1/Resolution/x"), string("720"));
			db.setDeviceConfigParam(id.id, string("/Video/1/Resolution/y"), string("576"));
			command=string("http://")+ devs[id.id]->cam.getIP().toString()+string("/admin/mpeg4.html?size=720x480");
		}
		else
		{
			db.setDeviceConfigParam(id.id, string("/Video/1/Resolution/x"), string("640"));
			db.setDeviceConfigParam(id.id, string("/Video/1/Resolution/y"), string("480"));
			command=string("http://")+ devs[id.id]->cam.getIP().toString()+string("/admin/mpeg4.html?size=640x480");
		}

		if(command!=string(""))
		{
			cout<<"--command:"<<command<<endl;
			devs[id.id]->camAccess.lock();
			try{
				HttpClient cl(command);
				//	cl.setHeader("Authorization",string("Basic ")+base64encode("admin:admin"));
				HttpStream *stream=cl.sendRequest(string("GET"));
				//	stream->getNextInformationChunk();
				delete stream;
			}catch(Exception e){

				cout<<"command Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			}
			devs[id.id]->camAccess.unlock();
		}
	}
//	SocketTCP *sock=devConnect(id);
//	string s=db.getDeviceConfigParam(id.id, "/Output");
//	cout<<" /Output :"<<s<<endl;

//	string s=db.getModelConfigParam("Mitsu","Barionet", "/IOMapping/Outputs");
/*	string s=conf->getNodeData("/[0]/Models/Barionet/IOMapping/Outputs");
	cout<<" Outputs :"<<s<<endl;

	int nout=atoi(s.c_str());
	devs[id.id]->outputs=new int[nout];
	for(int i=0;i<nout;i++)
	{
		devs[id.id]->outputs[i]=0;	
	}
	devs[id.id]->nout=nout;

	//s=db.getModelConfigParam("Mitsu","Barionet", "/IOMapping/Inputs");
	s=conf->getNodeData("/[0]/Models/Barionet/IOMapping/Inputs");
	cout<<" Inputs :"<<s<<endl;

	int nin=atoi(s.c_str());
	devs[id.id]->inputs=new int[nin];
	for(int i=0;i<nin;i++)
	{
		devs[id.id]->inputs[i]=0;	
	}
	devs[id.id]->nin=nin;

	//s=db.getModelConfigParam("Mitsu","Barionet", "/IOMapping/Relays");
	s=conf->getNodeData("/[0]/Models/Barionet/IOMapping/Relays");
	cout<<" Relays :"<<s<<endl;

	int nrel=atoi(s.c_str());
	devs[id.id]->relays=new string[nrel];
	for(int i=0;i<nrel;i++)
	{
		devs[id.id]->relays[i]="";	
	}
	devs[id.id]->nrel=nrel;

	devs[id.id]->outputValues=new DevInfo::OutputInfo[nout+nrel];
	devs[id.id]->inputValues=new DevInfo::InputInfo[nin];

	cout<<" --- loaded"<<endl;
//	delete sock;
*/


//	if(reload || !db.checkDeviceConfigParam(id.id,"/Video/Standard"))
//	{
//		cout <<" ******************************* recarreguem config device:"<<id.id<<endl;
		//loadDevConfig(id.id, model, "", "", conf);
		//cout<<endl;
//		cout<<" LoadDBConfig::::"<<id.id<<"::"<<model<<endl;//<<endl<<endl<<conf->toString()<<endl<<endl<<endl;

		if(db.getDeviceConfigParam(id.id, string("/Schedules"))==string(""))
			db.setDeviceConfigParam(id.id, string("/Schedules"), string("1"));
//	}

}

void MitsubishiControlPlugin::startSession(CPDeviceID id, Address *a)
{	
	map<string,SessionDispatcher>::iterator itSes=sessions.find(a->toString());
	bool sessionEnded=false;
	int oldSesID=0;
	
	cout<<"MitsCamCP::StartSession:"<<id.id<<endl;
	
	if(itSes!=sessions.end())
	{
		if(id.id ==itSes->second.getIdNumber())
		{
			cout<<" Session Already Stablished "<<a->toString()<<"  id:"<<id.id<<"   ses:"<<itSes->second.getIdNumber()<<endl;
			throw(CPSessionAlreadyStablishedException(1, " Sessio ja establerta"));
		}
		else
		{
			oldSesID=itSes->second.getIdNumber();
			sessionEnded=true;
			try{
				endSession(itSes->second.getIdNumber(), a);
			}catch(Exception &e){}
		}
	}

	map<int, DevInfo*>::iterator itBuff=devs.find(id.id);
	
	Address cam;
	bool inited=false;
	if(itBuff!=devs.end())
	{
		inited=itBuff->second->inited;
	}

	if(inited)
	{
		cam=itBuff->second->cam;
	}
	else
	{
		//no es a la llista de cameres, mirem si l'han afegit al sistema des de la inicialitzacio
		//i l'afegim
		cout<<"loading camera "<<StrUtils::decToString(id.id)<<endl;
		RPC *rpc=db.getRPC(a,5);
		string model("");
		try
		{

			if(itBuff==devs.end())
			{
				string s("Select d.id, d.ip, d.puerto, m.codigo FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='Mitsubishi' AND d.id=");// AND m.codigo='Barionet'");
				s+=StrUtils::decToString(id.id);

				XML *devInfo=NULL;
				cout << s << endl;
				devInfo=db.query(s, rpc);
				xmlNode *n=devInfo->getNode("/result/row/ip");

				if(n==NULL) //no es enlloc, dispositiu invalid
				{
					db.freeRPC(rpc);
					throw(CPInvalidParamException(0, "Dispositiu invalid"));
				}
				
				cout << n->toString() << endl;

				string sip=n->getCdata();

				if(sip.find('/')!=string::npos)
				{
					sip=sip.substr(0,sip.find('/'));
				}

				IP ip(sip);

				int port=atoi(devInfo->getNodeData("/result/row/puerto").c_str());
				cam=Address(ip, port);

				n=devInfo->getNode("/result/row/codigo");
				string model=n->getCdata();

				int mod=-1;
				devs[id.id]= new DevInfo(id, cam, model, default_buffer_size, this);
				delete devInfo;
			}

			XML *conf = getConfigFromFile( configFile );

			try
			{
				//loadDBConfig(id, f);
				int thid=ci.start(new CamInit::execInfo(id.id, conf, devs[id.id]->modelStr, this, true));
				ci.join(thid);
			}
			catch(SocketException &e)
			{
				//no s'ha trobat la camera?
			}
			delete conf;
			db.freeRPC(rpc);
		}
		catch(SocketTimeoutException &e)
		{
			//		gcpr.unlock();
			db.freeRPC(rpc);
			throw(ControlPluginException(e.getCode(), e.getMsg()));
		}
		catch(Exception &e)
		{
			db.freeRPC(rpc);
//			e.serialize()->materializeAndThrow(true);
			throw;
		}
		catch(std::exception &stde)
		{
			//		gcpr.unlock();
			db.freeRPC(rpc);
			cout << "std::exception: " << stde.what() << endl;
			throw(ControlPluginException(0, string("Invalid device id:")+StrUtils::decToString(id.id)));
		}
		catch(...)
		{
			//		gcpr.unlock();
			db.freeRPC(rpc);
			cout << "Unknown exception: line " << __LINE__ << endl;
			throw(ControlPluginException(0, string("Invalid device id:")+StrUtils::decToString(id.id)));
		}

	}
	cout<<" StartSession for:"<<a->toString()<<" dev:"<<id.id<<endl;
	if(!itBuff->second->inited)
	{
		throw(ControlPluginException(0, string("error connecting to device ")+StrUtils::decToString(id.id)));
	}

	SessionDispatcher ses(cam, id);
	sessions[a->toString()]= ses;
	if(sessionEnded)
		throw(CPSessionAlreadyStablishedException(2, string("Sessio finalitzada pel dispositiu : ")+StrUtils::decToString(oldSesID)));

}

void MitsubishiControlPlugin::endSession(CPDeviceID id, Address *a)
{
	map<string,SessionDispatcher>::iterator i=sessions.find(a->toString());
	
	if(i==sessions.end())
	{
		throw(CPSessionNotStablishedException(0, " Sessio ja establerta"));
	}
	
	sessions.erase(i);
	cout<<"MitsuCP::endSession:"<<id.id<<endl;
	struct timeval now;
	gettimeofday(&now,NULL);
	//map<string,SessionDispatcher>::iterator 
	i= sessions.begin(); 

	while(i!=sessions.end())
	{
		struct timeval diffTime = now - i->second.getTimeOfCreationOfThisSession();
		if ( 	diffTime.tv_sec > sessionTimeout )
			sessions.erase(i++);
		else
			i++;
	}
}


CPPTZ MitsubishiControlPlugin::getPTZ(Address *a)
{
	SessionDispatcher s=getSession(*a);
	
	cout<<"	getPTZ "<<endl;

	throw(CPInvalidParamException(0, "Dispositiu sense funcio de pan/tilt/zoom"));
}

void MitsubishiControlPlugin::setPTZ(CPPTZ ptz, Address *a)
{	
	SessionDispatcher s=getSession(*a);

	cout<<" setPTZ "<<endl;

	throw(CPInvalidParamException(0, "Dispositiu sense funcio de pan/tilt/zoom"));

}


void MitsubishiControlPlugin::setInput(CPInput in, Address *a)
{
	SessionDispatcher s=getSession(*a);
//	Address cam=ses.getIpAddress();	
	cout<<"MitsuCP::setInput:"<<in.id<<" "<<in.value<<endl;

	if(in.id<1 || in.id>devs[s.getIdNumber()]->nin)
	{
		throw(CPInvalidParamException(0, string( "Identificador d'Input invalid")+StrUtils::decToString(in.id)));
	}
	
	if(in.value<0 || in.value>2)
	{
		throw(CPInvalidParamException(0, "Valor invalid"));
	}


	devs[s.getIdNumber()]->queue(CPServ::setInput, (void*)new CPInput(in));
}

void MitsubishiControlPlugin::run_setInput(CPInput in, CPDeviceID id, Address cam)
{
//	devs[id.id]->inputs[in.id-1]=in.value;

//	db.setDeviceConfigParam(id.id, string("/Input/")+StrUtils::decToString(in.id), StrUtils::decToString(in.value));
//	string cmd=string("setio,")+StrUtils::decToString(in.id)+string(",")+StrUtils::decToString(in.value);
//	string res=devCmd(cam, cmd, "");
	
//	cout<<"MitsuCP::setOutput result:"<<res<<endl;
}

CPInput MitsubishiControlPlugin::getInput(CPInput in, Address *a)
{
	cout<<"BarionetCP::getInput"<<endl;
	SessionDispatcher ses=getSession(*a);
	Address cam=ses.getIpAddress();	

	return CPInput(); 
}

void MitsubishiControlPlugin::setOutput(CPOutput out, Address *a) 
{
	cout<<"MitsuCP::setOutput:"<<out.id<<" "<<out.value<<endl;
	
	SessionDispatcher s=getSession(*a);
//	Address cam=ses.getIpAddress();	
	if(out.id<1 || out.id>(devs[s.getIdNumber()]->nout + devs[s.getIdNumber()]->nrel))
	{
		cout<<"MitsuCP::setOutput:InvalidOutput nout:"<<devs[s.getIdNumber()]->nout <<" nrel:"<< devs[s.getIdNumber()]->nrel <<endl;
		throw(CPInvalidParamException(0, string("Identificador d'Output invalid:")+StrUtils::decToString(out.id)));
	}

	devs[s.getIdNumber()]->queue(CPServ::setOutput, (void *) new CPOutput(out));
}

void MitsubishiControlPlugin::run_setOutput(CPOutput out, CPDeviceID id, Address cam)
{

	cout<<" --------- SetOutput dev:"<<id.id<<" out:"<<out.id<<" = "<<out.value<<endl;
}

CPOutput MitsubishiControlPlugin::getOutput(CPOutput out, Address *a)
{
	cout<<"BarionetCP::getOutput "<<out.id<<endl;
	SessionDispatcher ses=getSession(*a);
	Address cam=ses.getIpAddress();	
	return CPOutput();
}

void MitsubishiControlPlugin::setCommandBufferSize(CPCommandBufferSize size, Address *a)
{
	SessionDispatcher s=getSession(*a);
	devs[s.getIdNumber()]->setSize(size.nCommands);

//	cout<<"MitsuCP::setCommandBufferSize:"<<size.nCommands<<endl;
	
}

CPCommandBufferSize MitsubishiControlPlugin::getCommandBufferSize(Address *a)
{
	SessionDispatcher s=getSession(*a);

	return CPCommandBufferSize(devs[s.getIdNumber()]->getSize());
}


float MitsubishiControlPlugin::getCommandBufferPercentInUse(Address *a)
{
	SessionDispatcher s=getSession(*a);

	return devs[s.getIdNumber()]->getPercentInUse();
}

int MitsubishiControlPlugin::getCommandBufferCommandsInUse(Address *a)
{
	SessionDispatcher s=getSession(*a);

	return devs[s.getIdNumber()]->getUse();
}


CPMetadata MitsubishiControlPlugin::getMetadataValue(CPMetadata data, Address *a)
{
	SessionDispatcher s=getSession(*a);


	return CPMetadata(data.name, getMetadataValue(s.getIdNumber(), data.name));
}

string MitsubishiControlPlugin::getMetadataValue(int id, string data)
{
	
//	cout<<"MitsuCP:: getMetadataValue "<<s.getIdNumber()<<" :"<<data<<endl;
	map<string, string> *m=&devs[id]->metadata;
	
//	if(data==string("ActivityDetected"))// && (*m)[data]=="true")
//	{
//			
//		(*m)[data]=string("false");
//	}
	if( m->find(data) == m->end())
	{
		return string("");
	}

	return (*m)[data];
}

void MitsubishiControlPlugin::setMetadataValue(CPMetadata data, Address *a)
{
	SessionDispatcher s=getSession(*a);
	
	devs[s.getIdNumber()]->metadata[data.name]=data.value;
	
	cout<<"MitsuCP::setMetadataValue "<<s.getIdNumber()<<" :"<<data.name<<" "<<data.value<<endl;
}


string MitsubishiControlPlugin::getParamValue(string llista, string param)
{
	list<string> params=StrUtils::split(llista, string("&"));
	list<string>::iterator it;

	for(it=params.begin(); it!=params.end();it++)
	{
		if(it->find(param+string("=")) == 0) 
		{
			return it->substr(param.size()+1,it->size()-(param.size()+1));
		}
	}
	return string("");
}

CPConfigParam MitsubishiControlPlugin::getConfigParam(CPConfigParam p, Address *a)
{
	SessionDispatcher ses=getSession(*a);

	RPC *rpc=db.getRPC(a,5);
	if(rpc==NULL)
		return CPConfigParam("","");
	try
	{
//		string res=db.getModelConfigParam("RecordingCamera", "", p.path, rpc);
		string res = db.getDeviceConfigParam(ses.getIdNumber(), p.path, rpc);
		if(res!="")
		{
			db.freeRPC(rpc);
			return CPConfigParam(p.path, res);
		}

//		res = db.getDeviceConfigParam(ses.getIdNumber(), p.path, rpc);
		res=db.getModelConfigParam(string("Mitsubishi"), devs[ses.getIdNumber()]->modelStr, p.path, rpc);//TODO:???
		//	cout<<"getCP::"<<res<<endl;
		if(res!="")
		{
			db.freeRPC(rpc);
			return CPConfigParam(p.path, res);
		}
		else
		{
			db.freeRPC(rpc);
			return CPConfigParam("", "");
		}
	}catch(Exception &e)
	{
		db.freeRPC(rpc);
//		e.serialize()->materializeAndThrow();
		throw;
	}catch(...)
	{
		db.freeRPC(rpc);
		throw;
	}
}

void MitsubishiControlPlugin::setConfigParam(CPConfigParam p, Address *a)
{
	SessionDispatcher s=getSession(*a);
	
	cout<<" setConfigParam::"<<s.getIpAddress().toString()<<" : "<<p.path<<endl;
	/*****Comprovació de params*****/
//	list<string> params=StrUtils::split(p.path, string("/"));

//	devs[s.getIdNumber()]->queue(CPServ::setConfigParam, (void *) new CPConfigParam(p));
	run_setConfigParam(p, s.getId(), devs[s.getIdNumber()]->cam);
}

void MitsubishiControlPlugin::run_setConfigParam(CPConfigParam p, CPDeviceID id, Address cam)
{
	//Update camara
	/*
	 * - Pan Tilt Limit
	 * - Preset position
	 * - Zoom Type (Opt/Opt+Digt)
	 * - Gain/Brightness/Expos.
	 */
	cout<<"MitsuCP::run_setConfigParam:"<<p.path<<" "<<p.value<<endl;
	
	string path=p.path;

	int pos=path.rfind("/Delete");
	if(pos!=string::npos)
	{
		path=path.substr(0,pos);
		cout<<" setConfigParam Del:"<<path<<endl;
		db.deleteDeviceConfig(id.id, path);
		if(path == "/Schedules")
		{
			cout<<" setConfigParam set"<<endl;
			db.setDeviceConfigParam(id.id, "/Schedules", "1");
		}
		return;
	}


//	list<string> l=StrUtils::pathToList(p.path);	

	string command("");
	if(path == string("/Video/1/Codec/Fourcc"))
	{
	}
	else if(path == string("/Video/1/Codec/Quality"))
	{
		command=string("http://")+ cam.getIP().toString()+string("/admin/mpeg4.html?mpeg4_br=")+p.value;
	}
	else if(path == string("/Video/1/Resolution/x") || path == string("/Video/1/Resolution/y"))
	{
		int x=0, y=0;
		if(path == string("/Video/1/Resolution/x"))
		{
			x=devs[id.id]->setx=atoi(p.value.c_str());
			y=devs[id.id]->sety;
		}else
		{
			x=devs[id.id]->setx;
			y=devs[id.id]->sety=atoi(p.value.c_str());
		}

		string res("");
	/*	if(x==160 && y==120)
			res=string("160x120");
		else if(x==320 && y==240)
			res=string("320x240");
		else if(x==640 && y==480)
			res=string("640x480");
		else if(x==740 && y==480)
			res=string("740x480");
*/
		if(x==176 && y==144)
			res=string("160x120");
		else if(x==352 && y==288)
			res=string("320x240");
		else if(x==640 && y==480)
			res=string("640x480");
		else if(x==740 && y==576 && (devs[id.id]->model == NMC150SD || devs[id.id]->model == NMC130FD))
			res=string("740x480");

		if(res!=string(""))
			command=string("http://")+ cam.getIP().toString()+string("/admin/mpeg4.html?size=")+res;
	}
	else if(path == string("/Video/1/FPS"))
	{
		int val=25;
		char *ptr;
		const char *cs=p.value.c_str();

		float fVal = strtof(cs, &ptr);

		if(ptr!=cs)//comprovem que el valor sigui valid
		{
			if(fVal <= 5.0)
			{
				val=5;
			}else if(fVal<=10.0)
			{
				val=10;
			}else if(fVal<=15.0)
			{
				val=15;
			}else if(fVal<=25.0)
			{
				val=25;
			}
		}
		command=string("http://")+cam.getIP().toString()+string("/admin/mpeg4.html?mpeg4_fps=")+StrUtils::decToString(val);
	}

	if(command!=string(""))
	{
		cout<<"--command:"<<command<<endl;
		devs[id.id]->camAccess.lock();
		try{
		HttpClient cl(command);
	//	cl.setHeader("Authorization",string("Basic ")+base64encode("admin:admin"));
		HttpStream *stream=cl.sendRequest(string("GET"));
	//	stream->getNextInformationChunk();
		delete stream;
		}catch(Exception e){
		
		cout<<"command Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		}
		devs[id.id]->camAccess.unlock();
	}

	db.setDeviceConfigParam(id.id, p.path, p.value);
}


CPConfigParamSeq MitsubishiControlPlugin::getConfigParamRecursive(CPConfigParam p, Address *a)
{
	RPC *rpc=db.getRPC(a,5);
	if(rpc==NULL)
		return CPConfigParamSeq();

	try{
		//	Address cam(IP(sessions[a->toString()].ip), sessions[a->toString()].port);
		SessionDispatcher ses=getSession(*a);

		CPConfigParamSeq seq;

		cout <<" getCPRecurs:"<< p.path<<endl;
		int id=db.getModelConfigNode("Mitsubishi", devs[ses.getIdNumber()]->modelStr,  p.path, rpc);
	 
//		if(id==-1)
//		{
//			id=db.getDeviceConfigNode(ses.getIdNumber(), p.path, rpc);
//		}
		RPC::keepAlive(a, type);
		if(id!=-1)
		{
			string pref=p.path;
			int pos=pref.rfind("/");
			if(pos==pref.size()-1)
			{
				pref=pref.substr(0,pos);
				pos=pref.rfind("/");
			}

			cout <<" getCPRecurs db path:"<< pref<<endl;//<<res->toString()<<endl;;
			if(pos!=string::npos)
				pref=pref.substr(0,pos);

			string s = string("SELECT * FROM getconfigparamrecursive(")+StrUtils::decToString(id)+string(",'")+pref+string("')"); 
			cout <<" getCPRecurs call:"<<s<<endl;
		
			XML *res=db.query(s, rpc);

			cout <<" getCPRecurs db:"<<(void*)res<<endl;
			xmlNode *n=res->getNode("/result");


			int nrows=0;
			if(n!=NULL)
				nrows=n->getKidList().size();
			if(nrows==0)
			{
				cout<<" BarionetCP::GetConfigParamRecursive() error resposta:"<<res->toString()<<endl;
			}

			cout <<" getCPRecurs db rows:"<<nrows<< endl;//res->toString()<<endl;
			for(int i=0;i<nrows;i++)
			{
				seq.push_back(CPConfigParam( res->getNodeData("/result/["+StrUtils::decToString(i)+"]/name"), res->getNodeData("/result/["+StrUtils::decToString(i)+"]/value")));
			}
			delete res;
		}
		RPC::keepAlive(a, type);

		id=db.getDeviceConfigNode(ses.getIdNumber(), p.path, rpc);

		if(id!=-1)
		{
			string pref=p.path;
			int pos=pref.rfind("/");
			if(pos==pref.size()-1)
			{
				pref=pref.substr(0,pos);
				pos=pref.rfind("/");
			}

			cout <<" getCPRecurs db path:"<< pref<<endl;//<<res->toString()<<endl;;
			if(pos!=string::npos)
				pref=pref.substr(0,pos);

			string s = string("SELECT * FROM getconfigparamrecursive(")+StrUtils::decToString(id)+string(",'")+pref+string("')"); 
			cout <<" getCPRecurs call:"<<s<<endl;
			try
			{
				db.begin();
			}
			catch(Exception &e)
			{
				cout<<"db.begin Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			}
			XML *res=db.call(s, 0, rpc);
			RPC::keepAlive(a, type);
			try
			{
				db.commit();
			}
			catch(Exception &e)
			{
				cout<<"db.commit Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			}
			cout <<" getCPRecurs db:"<<(void*)res<<endl;
			xmlNode *n=res->getNode("/result");


			int nrows=0;
			if(n!=NULL)
				nrows=n->getKidList().size();
			if(nrows==0)
			{
				cout<<" BarionetCP::GetConfigParamRecursive() error resposta:"<<res->toString()<<endl;
			}

			cout <<" getCPRecurs db rows:"<<nrows<< endl;//res->toString()<<endl;
			for(int i=0;i<nrows;i++)
			{
				seq.push_back(CPConfigParam( res->getNodeData("/result/["+StrUtils::decToString(i)+"]/name"), res->getNodeData("/result/["+StrUtils::decToString(i)+"]/value")));
			}
			delete res;
		}



		cout <<" getCPRecurs db return Seq:"<< endl;//res->toString()<<endl;
		db.freeRPC(rpc);
		return seq;
	}catch(SocketTimeoutException &e)
	{
		db.freeRPC(rpc);
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}
	catch(Exception &e)
	{
		//		gcpr.unlock();
		db.freeRPC(rpc);
//		e.serialize()->materializeAndThrow();
		throw;
	}
	catch(...)
	{
		db.freeRPC(rpc);
		throw;
	}
}
/*
{
	SessionDispatcher s=getSession(*a);
	Address cam=s.getIpAddress();
	
	CPConfigParamSeq seq;

/ *
	string cmd=string("getio,10")+StrUtils::decToString(out.id);
	//string res=deviceCommand(cam, cmd);
	string res=devCmd(cam, cmd, "state,");
	//string res=devResp(cam,"getio");
	
	cout<<"BarionetCP::getOutput res:"<<res<<endl;
	return CPOutput(out.id, atoi( res.c_str()+10 ));

* /
	for(int i=1;i<=2;i++)
	{
		string path=string("Relay")+StrUtils::decToString(i);
		string res=db.getDeviceConfigParam(s.getIdNumber(), path);
		if(res=="")
		{
			string cmd=string("getio,")+StrUtils::decToString(i);
			string val=devCmd(s.getId(), cmd, "state,");

			int state=atoi(val.c_str()+8);
			res=(state>0)?"on":"off";
		}
		//PUSH
		seq.push_back(CPConfigParam(path, res));
	}

	return seq;
}
*/


CPConfigParamSeq MitsubishiControlPlugin::getAllConfigParams(Address *a)
{
	SessionDispatcher s=getSession(*a);
	Address cam=s.getIpAddress();
	
	CPConfigParamSeq seq;

/*	string cmd=string("getio,10")+StrUtils::decToString(out.id);
	//string res=deviceCommand(cam, cmd);
	string res=devCmd(cam, cmd, "state,");
	//string res=devResp(cam,"getio");
	
	cout<<"BarionetCP::getOutput res:"<<res<<endl;
	return CPOutput(out.id, atoi( res.c_str()+10 ));
*/

	return seq;
}

void MitsubishiControlPlugin::move(CPMovement move, Address *a)
{	
	SessionDispatcher s=getSession(*a);

	int mountAngle=0;
	RPC *rpc=db.getRPC(a,5);
	if(rpc!=NULL)
	{
		string res = db.getDeviceConfigParam(s.getIdNumber(), "/UserMountAngle", rpc);
		db.freeRPC(rpc);
		mountAngle=atoi(res.c_str());
	}

	Address cam=devs[s.getIdNumber()]->cam;
	Address cmdAddr(cam.getIP(), 7000);
	short pan, tilt, zoom;


	if(devs[s.getIdNumber()]->model == NMC150SD)
	{
		string command=string("http://")+cam.getIP().toString()+string("/admin/ptz.cgi?move=");
		if(move.x==0 && move.y==0)
		{
			command+=string("stop");
		}
		else
		{
			if(mountAngle==0)
			{
				if(move.y>0) //horitzontal
					command+=string("up");
				else if(move.y<0)
					command+=string("down");
				if(move.x>0) //vertical
					command+=string("right");
				else if(move.x<0)
					command+=string("left");
			}
			else
			{
				if(move.y>0) //horitzontal
					command+=string("down");
				else if(move.y<0)
					command+=string("up");
				if(move.x>0) //vertical
					command+=string("left");
				else if(move.x<0)
					command+=string("right");
			}
			command+=string("&pspd=32&tspd=32");
		}

		if(move.z>0) //vertical
			command+=string("&zoom=tele");
		else if(move.z<0)
			command+=string("&zoom=wide");
		//	else
		//		command+=string("&zoom=stop");

		command+=string("&ptz_protocol=f2e&continuous=1");

		try{
			cout<<" http client : "<<command<<endl;
			HttpClient cl(command);
			//cl.setHeader("Authorization",string("Basic ")+base64encode("admin:admin"));
			HttpStream *stream=cl.sendRequest(string("GET"));

			stream->getNextInformationChunk();
			delete stream;
			cout<<" http client done! "<<endl;
		}catch(SocketTimeoutException &e)
		{
			cout<<" http client uuuuups: "<<e.getCode()<<" : "<<e.getMsg()<<endl;
			throw(ControlPluginException(e.getCode(), e.getMsg()));
		}
	}


/*
    unsigned char command[7];

    command[0]=0xfa;//0xff;
	command[1]=0x50;//0x01;//address ?
	command[2]=0x01;//0x00;
	command[3]=0x00;
	if(mountAngle==0)
	{
		if(move.y>0) //horitzontal
			command[3] |= 0x08;
		else if(move.y<0)
			command[3] |= 0x10;
		if(move.x>0) //vertical
			command[3] |= 0x02;
		else if(move.x<0)
			command[3] |= 0x04;
	}
	else
	{
		if(move.y>0) //horitzontal
			command[3] |= 0x10;
		else if(move.y<0)
			command[3] |= 0x08;
		if(move.x>0) //vertical
			command[3] |= 0x04;
		else if(move.x<0)
			command[3] |= 0x02;
	}
	if(move.z>0) //vertical
		command[3] |= 0x20;
	else if(move.z<0)
		command[3] |= 0x40;
////////////////////////	
	command[3]=0x80;

////////////
	command[4]=0x00;//a3;
	if(move.y!=0) //horitzontal
	command[5]=0xa3;
	else
	command[5]=0x00;
	unsigned int chksum=((unsigned int)command[1])+((unsigned int)command[2])+((unsigned int)command[3])+((unsigned int)command[4])+((unsigned int)command[5]);
	command[6]=chksum%256;
/////////////////
	if(move.y!=0) //horitzontal
	command[6]=0x6e;
	else
	command[6]=0xcb;
*/	

	// PELCO - P
/*    unsigned char command[8];

    command[0]=0xa0;//0xff;
	command[1]=0x00;//0x01;//address ?
	command[2]=0x00;//0x40|0x10; // Data1 - Camera on
	command[3]=0x00;
//	if(mountAngle==0)
	{
		if(move.y>0) //horitzontal
			command[3] |= 0x08; //tilt down
		else if(move.y<0)
			command[3] |= 0x10; //tilt up
		if(move.x>0) //vertical
			command[3] |= 0x02; //pan right
		else if(move.x<0)
			command[3] |= 0x04; //pan left
	}

	if(move.z>0) //vertical
		command[3] |= 0x20; //zoom tele
	else if(move.z<0)
		command[3] |= 0x40; //zoom wide

	if(move.x!=0) 
		command[4]=0x20; //pan speed
	else
		command[4]=0x00; //pan speed
	if(move.y!=0) //horitzontal
		command[5]=0x20;
	else
		command[5]=0x00;
	command[6]=0xaf;
	unsigned int chksum=command[0]^command[1]^command[2]^command[3]^command[4]^command[5]^command[6];
	command[7]=chksum;

	cout<<"move:"<<move.x<<" "<<move.y<<" "<<move.z<<endl;
	cout<<" send move mout:"<<mountAngle<<"   ->"<<StrUtils::hexDump(string((char*)command, 8));
	SocketTCP cmd(cmdAddr);
	cmd.establish();
	cmd.write(command, 7);
//	char resp[128];
//	int read=cmd.read(resp, 128);
//	cout<<"move rebut resp: "<<read<<" bytes : "<<string(resp, read)<<endl<<endl;
	cout<<"moved"<<endl<<endl;

//	throw(CPInvalidParamException(0, "Dispositiu sense funcio de pan/tilt/zoom"));
*/
}







