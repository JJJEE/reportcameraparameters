/*********************************************************************
 * SonyControlPlugin.h                                               *
 * -----------------------------------------------------------------	*
 *	Marina Eye-Cam Technologies, S.L. All rights reserved.				*
 *********************************************************************/
#ifndef SONYCONTROLPLUGIN_H
#define SONYCONTROLPLUGIN_H
 
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginControl.h>
#include <Plugins/GestorControlTypes.h>
#include "SonyControlPluginTouringThread.h"
#include "SonyControlPluginSchedulingThread.h"
#include <ModuleAccess/AlarmModuleAccess.h>
#include <Utils/SessionDispatcher.h>
#include <Utils/ModelInfo.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <map>
#include <list>
#include <iostream>
#include <math.h>
using namespace std;

typedef class SonyControlPluginTouringThread* bar;

class SonyControlPlugin: public PluginControl
{
private:
	static const int SNC_RZ30			= 0;
	static const int SNC_RX550			= 1;
	static const int SNC_RZ25			= 2;
	static const int SNC_Z20			= 3;
	static const int SNC_P1				= 4;
	static const int SNC_RZ50			= 5;
	static const int SNC_CS50			= 6;
	static const int SNC_CS3			= 7;
	static const int SNC_CS11			= 8;
	static const int SNC_DF50			= 9;
	static const int SNT_V704			= 10;
	static const int SNT_V704_L9EMB	= 11;
	static const int GEN_5				= 12;
	static const int SNC_CH140			= 13;
	static const int SNC_CH180			= 14;
	static const int SNT_EX104			= 15;
	static const int SNC_DH180			= 16;
	static const int SNC_RS86			= 17;
	static const int SNC_EB630			= 18;
	static const int SNC_VB600			= 19;
	static const int SNC_VB600B		= 20;
	static const int SNC_EM632R		= 21;
	static const int SNC_EM600			= 22;
	static const int SNC_EB632R		= 23;
	static const int SNC_WR632 		= 24;
	static const int SNT_EX101			= 25;
	static const int SNT_EP104 		= 26;

	map<string, SessionDispatcher> 	sessions;	// Contenedor de sesiones
	map <string, ModelInfo*> models;					// Contenedor de modelos
	static const int 		sessionTimeout = 600;	// Timeout por defecto para 
																// los dispositivos

	// Clase que gestiona la información de los distintos dispositivos
	class CamInfo	:	public Thread
	{
	private:
		class Command
		{
		public:
			int id;
			void* data;
			Command(int id, void* data)	:	id(id), 
														data(data)
			{}
		};

		struct WaitInfo
		{
			int i;
			Condition c;
			WaitInfo() : i(0)
			{}
		};
		
		list<Command> buffer;
		Mutex bLock;
		
		int bufferSize;

		SonyControlPlugin *cp;
		AlarmModuleAccess am;

		void init( CPDeviceID id, SonyControlPlugin *cp );
		
	public:
		CPDeviceID 	id;					// Identificador del dispositivo
		Address 		cam;					// Dirección del dispositivo
		int 			streamId;			// Identificador del streaming con el disp.
		int 			model;				// Modelo del dispositivo (entero)
		string 		modelStr;			// Modelo del dispositivo (string)

		map<string, string> metadata;
		Condition full; 					//fan servir el bLock
		struct timeval alarmTime;
		SonyControlPluginTouringThread *tour;
		bool initing;
		bool inited;
		bool error_logged;
		bool firstSession;

		// TODO 20090310: Guardem fins a 5 thrIds, a lo guarro 
		bool threadRunning[5];
		
		Address bar;
		Condition queued; 				//fan servir el bLock
		Address bar2;

		// Constructor  CON streamId como parámetro
		inline CamInfo(	CPDeviceID id, 
								Address cam, 
								int streamId, 
								string modelStr, 
								int size, 
								SonyControlPlugin *cp 	);

		// Constructor SIN streamId como parámetro
		inline CamInfo(	CPDeviceID id, 
								Address cam, 
								string modelStr, 
								int size, 
								SonyControlPlugin *cp 	);

		void queue(	int id, 
						void *data, 
						Address *keepAliveAddr = NULL	);

		//esperém que la cua actual de comandes s'hagi acabat d'executar
		void wait( Address *keepAliveAddr = NULL ); 

		inline void startThreads();

		void* execute( int i, void* params );

		// Obtiene el tamaño del buffer
		inline int getSize();

		// Configura el tamaño del buffer
		inline void setSize(int size);

		// Obtiene el tamaño usado del buffer
		inline int getUse();

		// Obtiene el porcentaje de uso del buffer de CamInfo
		inline float getPercentInUse();
	};
	
	map<int, CamInfo*> cams;

protected:

	class CamInit: public Thread
	{
		public:
		struct execInfo
		{
			int id;
			XML *conf;
			string model;
			SonyControlPlugin *scp;
			bool reload;
			execInfo(	int id, 
							XML *conf, 
							string model, 
							SonyControlPlugin *scp, 
							bool reload 	) : 	id(id), 
														conf(conf), 
														model(model), 
														scp(scp), 
														reload(reload)
			{};
		};

		CamInit(){};

		void *execute( int id, void* parm );
	};

	class CamInitStarterThread: public Thread
	{
		public:
			XML *conf;
			SonyControlPlugin *scp;
			list<int> devs;
			bool reload, sequential;
			void* execute( int id, void* parm );
	};

	static const int default_buffer_size 	= 5;
	static const int maxStarterThreads 		= 5;
	XML *conf;
	XML *modelConf;
	CamInit ci;
	CamInitStarterThread cist;

	DBGateway db;
	string file;

	string getBasicAuthStrForDevice( int devId );

	inline SessionDispatcher getSession( Address a );
	
	void checkModelMetadata( string nom );

	void loadModelsConfig( XML *conf, bool reload = false );

	void loadModelConfig(	string model, 
									string nodepath, 
									string dbpath, 
									XML *conf 	);

	
	// Llama a la auténtica función para cargar la conf del dispositivo 
	inline void loadDevConfig(	int id, 
										string model, 
										string nodepath, 
										string dbpath, 
										XML *conf 	);

	// Carga la configuración del dispositivo desde el XML
	void loadDevConfig(	int id, 
								string model, 
								string nodepath, 
								string dbpath, 
								XML *conf, 
								DBGateway *db 	);

	//translate from Camera to DB
	string translateCam( string param, string value, int model );

	//translate from DB to Camera 
	string translateDB( string param, string value, int model );  

	string addPath( string path, string relPath );

	void loadDBConfig(	CPDeviceID, 
								XML *defaultConf, 
								string model, 
								bool reload = true  );
	
	string removeIndex( string path );

	// retorna el valor d'un parametre en una string de 
	// l'estil param1=valor1&param2=valor2&param3=.....
	string getParamValue(string llista, string param); 

	string getConfigParamCamera(	string url, 
											string param, 
											string pModel, 
											int devId = -1 );

	void setConfigParamCamera(string url, int devId=-1);
	void sendMoveCommand(string url, int devId, bool slowMove);
	void loadSchedule(int devId, int recId);
	void loadSchedules(int id);
	void* execute(void *parm);


public:
	// Constructor
	SonyControlPlugin(	string file, 
								bool sequentialReloads, 
								bool reloadDevs, 
								bool reloadModels, 
								list<int> reloadDevList 	);

	// Destructor
	~SonyControlPlugin();

	// (???)
	inline void serve();

	// Inicia sesión con el dispositivo
	void startSession( CPDeviceID id, Address *a );

	// Finaliza la sesión con el dispositivo
	void endSession( CPDeviceID id, Address *a );

	// Llama a la verdadera función para obtener el PTZ de un dispositivo
	inline CPPTZ getPTZ( Address *a );

	// Obtiene el PTZ de un dispositivo
	CPPTZ getPTZ( int id );

	// Configura el PTZ de un dispositivo
	inline void setPTZ( CPPTZ ptz, Address *a );

	// Realiza las acciones del PTZ del dispositivo
	void run_setPTZ(	CPPTZ ptz, 
							CPDeviceID id, 
							bool fromTour = false, 
							bool slowMove = true	);

	// TODO 20150523: Configura la entrada del dispositivo  (???)
	inline void setInput( CPInput in, Address *a );

	// Llama a la auténtica función para obtener la entrada (???)
	inline CPInput getInput( CPInput in, Address *a );

	// Obtiene la entrada del dispositivo (???)
	CPInput getInput( CPInput in, int devId );

	// Configura la salida del dispositivo (???)
	void setOutput( CPOutput out, Address *a );

	// Obtiene la salida del dispositivo (???)
	CPOutput getOutput( CPOutput out, Address *a );

	// Configura el tamaño del CommandBuffer
	inline void setCommandBufferSize(	CPCommandBufferSize size, 
													Address *a 	);

	// Obtiene el tamaño del CommandBuffer
	inline CPCommandBufferSize getCommandBufferSize( Address *a );

	// Obtiene el porcentaje de uso del CommandBuffer
	inline float getCommandBufferPercentInUse( Address *a );

	// Obtiene los comandos en uso del CommandBuffer
	inline int getCommandBufferCommandsInUse( Address *a );

	// Llama a la auténtica función para obtener metadatos
	inline CPMetadata getMetadataValue( CPMetadata data, Address *a );

	// Obtiene el valor de un metadato en un dispositivo
	string getMetadataValue( int id, string data );

	// Llama a la auténtica función para configurar metadatos
	inline void setMetadataValue( CPMetadata data, Address *a );

	// Configura un metadato pasado por parámetro
	inline void setMetadataValue(	int id, string name, string value );

	// Obtiene un parámetro de configuración pasado a la función
	inline CPConfigParam getConfigParam( CPConfigParam p, Address *a );

	XML* getConfigFromFile( string file );

	// Obtiene un parámetro de la configuración.
	CPConfigParam getConfigParam( 	int id, 
												CPConfigParam p, 
												Address *a = NULL );

	// Establece un parámetro en la configuración
	inline void setConfigParam( CPConfigParam p, Address *a );

	// (???)
	void run_setConfigParam(	CPConfigParam p, 
										CPDeviceID id, 
										bool fromTour = false );

	// Llama a la auténtica función para obtener los parámetros de conf.
	inline CPConfigParamSeq getConfigParamRecursive(	CPConfigParam p, 
																		Address *a 	);

	// Función que obtiene los parámetros de configuración recursivamente
	CPConfigParamSeq getConfigParamRecursive(	CPConfigParam p, 
															int devId, 
															Address *a );

	// Función que obtiene todos los parámetros de un dispositivo
	CPConfigParamSeq getAllConfigParams( Address *a );

	// Establece sesión con la cámara para moverla
	inline void move( CPMovement move, Address *a );

	// Ejecuta el movimiento de una cámara. 
	void run_move( CPMovement move, CPDeviceID id );
};

#endif