#include <Utils/Types.h>
#include <Utils/Exception.h>
#include <BarionetControlPlugin.h>
#include <version.h>

#include <signal.h>

#ifndef WIN32
void sigpipe(int i)
{
	cout << "SigPipe:"<<i << endl;
//	stackTrace.dump();
//	abort();
}
#endif

int main()
{
#ifndef WIN32
	signal(SIGPIPE, sigpipe);
#endif
	cout << siriusRelease << endl;
	bool exception;
	do
	{
		exception=false;
		try{
#ifdef WIN32
			BarionetControlPlugin plugin("\\\\.PSF\\Untitled\\aena\\devel\\src\\Projecte VC++\\Sirius\\debug\\BarionetControlPluginConfig.xml");
#else
			BarionetControlPlugin plugin("BarionetControlPluginConfig.xml");
#endif
		plugin.serve();
		}catch(Exception &e)
		{
			exception=true;
			cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
		}
	}while(exception);

	return 0;
}

