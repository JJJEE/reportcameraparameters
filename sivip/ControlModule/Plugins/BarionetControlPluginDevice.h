#ifndef __SIRIUS_CONTROLPLUGIN_BARIONETCONTROLPLUGINDEVICE_H
#define __SIRIUS_CONTROLPLUGIN_BARIONETCONTROLPLUGINDEVICE_H

#include <Plugins/ControlPluginDevice.h>
#include <Http/HttpClient.h>
#include <Http/HttpStream.h>
#include <Utils/Timer.h>

class BarionetControlPluginDevice : public ControlPluginDevice
{
protected:
	Timer 		*lastUpdate;
	string 		statusURL;
	HttpClient 	http;
	HttpStream 	*httpStream;
	
public:
	BarionetControlPluginDevice();
	BarionetControlPluginDevice(	CPDeviceID id, 
											Address addr,
											string modelConfigFile );
	~BarionetControlPluginDevice(){};

	virtual void init(	CPDeviceID id, 
								Address addr, 
								string modelConfigFile );

	virtual int getInput(	int input, 
									int valueType	);

	virtual void setInput(	int input, 
									int valueType, 
									int value );

	virtual int getOutput(	int output, 
									int valueType );

	virtual void setOutput(	int input, 
									int valueType, 
									int value );

	virtual void updateState();
};

#endif
