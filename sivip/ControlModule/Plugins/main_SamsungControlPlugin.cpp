#include <Utils/Types.h>
#include <Utils/Exception.h>
#include <SamsungControlPlugin.h>
#include <Utils/debugStackTrace.h>
#include <csignal>
#include <version.h>
#include <cstdio>
#include <cstdlib>

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMP();
	abort();
}

#endif

#ifndef WIN32
void sigpipe(int)
{
	cout << "SegPipe" << endl;
}

#endif
int main(int argc, char* argv[])
{
#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif

#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
#endif
#ifndef WIN32
	signal(SIGPIPE, sigpipe);
#endif

	STACKTRACE_INSTRUMENT();

	cout << siriusRelease << endl;


	bool sequential=false, reloadDevs=false, reloadModels=false;
	list<int> reloadDevList;
	for(int i=1;i<argc;i++)
	{
		string p(argv[i]);

		if(p==string("-r"))
			reloadDevs=true;
		else if(p==string("-s"))
			sequential=true;
		else if(p==string("-R"))
			reloadModels=true;
		else if(p.find(string("-D"))==0)
		{
			string id=p.substr(2);
			cout<<" id:"<<id<<endl;
			reloadDevList.push_back(atoi(id.c_str()));
		}
		else if(p==string("-h"))
		{
			cout<<"SCP options:"<<endl;
			cout<<"-h  print this help"<<endl;
			cout<<"-r  reload DB config for all devices"<<endl;
			cout<<"-s  force sequential reload of devices"<<endl;
			cout<<"-R  reload DB config for all models"<<endl;
			cout<<"-D<devId>  reload DB config for device with specified ID"<<endl;
			exit(0);
		}
	}
	cout<<" seq:"<< sequential<<" allDevs:"<< reloadDevs<<" allMods:"<< reloadModels<<endl;
	bool exception;
//	do
//	{
		exception=false;
		try{
			SamsungControlPlugin plugin("SamsungControlPluginConfig.xml", sequential, reloadDevs, reloadModels, reloadDevList);
			cout<<" SCP created, serving"<<endl;
			plugin.serve();
		}catch(Exception &e)
		{
			exception=true;
			cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
		}
//	}while(exception);
	return 0;
}

