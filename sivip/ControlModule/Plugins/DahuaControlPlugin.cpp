#include <Plugins/GestorControlExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/md5.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <DahuaControlPlugin.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#include <string>
#ifdef WIN32
#include <Utils/WindowsDefs.h>
#endif
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

// Si DEBUG está definido, aparecerán los mensajes de DEBUG en pantalla.
// Principalmente dichos mensajes aparecen al comienzo y final de cada función.
//#define DEBUG
#undef DEBUG

using namespace std;


bool DahuaListFind(list<int> l, int val)
{
#ifdef DEBUG
	cout << "-----> DahuaListFind(...)" << endl;
#endif	

	for (list<int>::iterator lIt = l.begin(), lend = l.end(); 
			lIt != lend; ++lIt)
	{
		cout<<"DahuaListFind dev "<<(*lIt)<<endl;
		if ((*lIt)==val)
		{
#ifdef DEBUG
			cout << "<----- DahuaListFind(...)" << endl;
#endif	
			return true;
		}
	}
	
	cout<<"DahuaListFind not found "<<val<<endl;

#ifdef DEBUG
	cout << "<----- DahuaListFind(...)" << endl;
#endif	

	return false;
}

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

DahuaControlPlugin::CamInfo::CamInfo(
													CPDeviceID id, 
													Address cam, 
													int streamId, 
													string modelStr, 
													int size, 
													DahuaControlPlugin *cp
												):
												 	bufferSize(size), 
													cp(cp), 
													id(id), 
													cam(cam), 
													streamId(streamId), 
													modelStr(modelStr), 
													queued(bLock), 
													full(&bLock), 
													inited(false),
													initing(false), 
													firstSession(true), 
													am(cp->address, "AlarmModule", cp->cn)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::CamInfo1" << endl;
#endif	

	init(id, cp);

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::CamInfo1" << endl;
#endif	
}

DahuaControlPlugin::CamInfo::CamInfo(	CPDeviceID id, 
														Address cam, 
														string modelStr, 
														int size, 
														DahuaControlPlugin *cp)	:
															bufferSize(size), 
															cp(cp), 
															id(id), 
															cam(cam), 
															streamId(0), 
															modelStr(modelStr), 
															queued(bLock), 
															full(&bLock), 
															inited(false),
															initing(false), 
															firstSession(true), 
															am(	cp->address, 
																	"AlarmModule", 
																	cp->cn)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::CamInfo2" << endl;
#endif	

	init(id, cp);

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::CamInfo2" << endl;
#endif
}

void DahuaControlPlugin::CamInfo::init(	CPDeviceID id, 
														DahuaControlPlugin *cp 	)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::init" << endl;
#endif

	model=-1;

	if (	modelStr == "SD6AL230F-HNI"	)	
	{
	 		model = SD6AL230F_HNI;	
	}	
	else
	{
			cerr << "Model not supported ..." << endl;
	}
	for (int i=0; i < NUM_THREADS; ++i)		
	{
		this->threadRunning[i]	= false;
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::init" << endl;
#endif
}

void DahuaControlPlugin::CamInfo::startThreads()
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::startThreads" << endl;
#endif

	cout << "Starting caminfo threads" << endl;

	if (!this->threadRunning[0])
	{
		start((void*)0);
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::startThreads" << endl;
#endif
}

void DahuaControlPlugin::CamInfo::queue(	int id, 
															void *data, 
															Address *keepAliveAddr	)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::queue" << endl;
#endif

	full.lock();
	
	try
	{
		while(buffer.size() > bufferSize)
		{
			struct timespec t;
			t.tv_sec 	= 	time(NULL) + 4;
			t.tv_nsec	=	0;

			if(	full.wait(t)	)
			{
				cout << 	" timeout queue full devInfo.queue addr:"
							" " << keepAliveAddr << endl;
			}
		}
		
		buffer.push_back(Command(id, data));
	}
	catch(...){}
	full.unlock();

	queued.signal();

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::queue" << endl;
#endif
}

int DahuaControlPlugin::CamInfo::getSize()
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::getSize" << endl;
	cout << "<----- DahuaCP::CamInfo::getSize" << endl;
#endif

	return bufferSize;
}

void DahuaControlPlugin::CamInfo::setSize(int size)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::setSize" << endl;
#endif

	if (size > 0)
	{
		bufferSize = size;
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::setSize" << endl;
#endif
}

int DahuaControlPlugin::CamInfo::getUse()
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::getUse" << endl;
	cout << "<----- DahuaCP::CamInfo::getUse" << endl;
#endif

	if (	buffer.size() > bufferSize )
	{
			return(	bufferSize 	);
	}	

	else
	{
		return (	buffer.size() );
	}
}

float DahuaControlPlugin::CamInfo::getPercentInUse()
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::getPercentInUse()" << endl;
#endif

	float f = buffer.size() / bufferSize;
	if (f > 1)
	{
		f = 1;
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::getPercentInUse()" << endl;
#endif
	return f;
}

void *DahuaControlPlugin::CamInfo::execute(int, void *data)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::execute: " << (int) data << endl;
#endif
	
	int thrId = (int)data;

	if (thrId < NUM_THREADS)
	{
		this->threadRunning[thrId] = true;
	}

	if (thrId == 0)
	{
		//Camera command queue
		while(1)
		{
			try
			{
				queued.lock(); 
				Command c(-2, NULL);
				try
				{
				
					while(buffer.size()==0)
					{
						full.signal();

						struct timespec t;
						t.tv_sec 	= time(NULL) + 1;
						t.tv_nsec 	= 0;
						queued.wait(t);
					}
					c = buffer.front();
					buffer.pop_front();
				
				}
				catch(Exception e)
				{
					cout << " CamInfo::execute:  exc: " << e.getClass() << ":"
								"" << e.getMsg() << endl;
				}
				catch(...){}
				
				queued.unlock();

				switch(c.id)
				{
					case CPServ::setConfigParam:
						cout << "exec queued command config" << endl;
						cp->run_setConfigParam(*((CPConfigParam*)c.data), id);
						delete (CPConfigParam*)c.data;
						break;		
					
					case -1:
						cout << "  -- exec queued command i=1" << endl;
						((WaitInfo*)c.data)->i=1;
						((WaitInfo*)c.data)->c.lock();
						((WaitInfo*)c.data)->c.signal();
						((WaitInfo*)c.data)->c.unlock();
						break;

					default:
						cout << " ERROR: incorrect queued command id" << endl;
						break;
				}
			}
			catch(Exception e)
			{
				cout << " CamInfo::execute:  exc:" << e.getClass() << ":" 
							"" << e.getMsg() << endl;
			}
			catch(...){}

			if(buffer.size()+1 <= bufferSize)
			{
				full.signal();
			}
		}
	}

	else if(thrId == 1)
	{
		int inputs = 0;
		while (inputs == 0)
		{
			string s1("Dahua");
			string s2=cp->cams[id.id]->modelStr;
			string s3("/IOMapping/Inputs");
			string s=cp->db.getModelConfigParam(s1,s2,s3);

			char *c=(char*)s.c_str(), *l=NULL;
			inputs = strtol (c, &l, 10);

			// es "0" si l!=\0, es que hi ha agut un error de parsing 
			// (i.e., error d'accés a la BD)
			if(inputs==0 && *l=='\0' ) 
			{
				cout<<" Cam::AMThread "<< id.id<<"  -  no inputs :"<<s <<endl;

				if (thrId < NUM_THREADS)
				{
					this->threadRunning[thrId]=false;
				}
#ifdef DEBUG
				cout << "<----- DahuaCP::CamInfo::execute" << endl;
#endif
				return NULL;
			}
		}

		cout<<" Cam::AMThread found inputs:"<<id.id <<":"<<inputs<<endl;
		while(1)
		{
			try
			{
				cout<<" Cam::AMThread StartSession "<<id.id <<endl;
				am.startSession(AMDeviceID(id.id));
				break;
			}
			catch(Exception &e)
			{
				cout 	<<	" DahuaCP::AlarmModule.startSession : " 
							"" << e.getClass() << " - " << e.getMsg() << endl;

				if(e.getClass() == "AMSessionAlreadyStablishedException")
				{
					break;
				}
				sleep(30);
			}
		}
		//si es perd la sessió, l'AlmModClient ja la recupera
		int values[inputs];

		for(int i = 0; i < inputs; ++i)
		{
			values[i] = 0;
		}

		while(1)
		{
			AMAlarmId alm;
			alm.devId=id.id;
			alm.type=AMAlarmId::Input;
			try
			{
				{
					int devId=id.id;

					cout << " --- dev:" << devId << " - " << inputs << endl;

					AMAlarmId aid(AMAlarmId::Input, 0);
					aid.devId=devId;

					for(int i = 0; i < inputs; ++i)
					{
						aid.intId = i;

						CPInput in=cp->getInput(CPInput(i,0), devId);
						
						if(in.value != values[i])
						{
							values[i] 	= in.value;
							AMAlarmValue av;
							av.alarmId 	= aid;
							av.value 	= in.value;
							cout << " setAlarm cam:" << devId << " input:" 
										"" << i << " value:" << in.value << endl;
							am.setAlarm(av);
						}
					}
				}
				sleep(1);
			}
			catch(Exception &e)
			{
				if(e.getClass() == "SocketException")
				{
					//camera no connectada
					sleep(30);
				}
				else if(e.getClass() == "CPInvalidParamException")
				{
					//output invalid, es problema de config del sirius
					sleep(30);
				}
				else
				{
					sleep(1);
				}
			}
		}
	}
	
	if (thrId < NUM_THREADS)
	{
		this->threadRunning[thrId] = false;
	}
#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::execute" << endl;
#endif
}

void DahuaControlPlugin::CamInfo::wait(Address *keepAliveAddr)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInfo::wait" << endl;
#endif

	WaitInfo *w = new WaitInfo();

	queue(-1, w, keepAliveAddr);

	w->c.lock();
	try
	{
		while(w->i == 0)
		{
			struct timespec t;
			t.tv_sec 	= time(NULL) + 4;
			t.tv_nsec 	= 0;
			if (w->c.wait(t))
			{
				if(keepAliveAddr != NULL)
				{
					RPC::keepAlive(keepAliveAddr, 0);
				}
			}
		}
	}
	catch(...){}

	w->c.unlock();
	delete w;
	w = NULL;

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInfo::wait" << endl;
#endif
}

void *DahuaControlPlugin::CamInitStarterThread::execute(int, void *)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInitStarterThread::execute" << endl;
#endif

	int starterThreads 	= 0;
	cout 	<< 	"CamInitStarterThread:: detach scp:" 	<<	endl;
	for(map<int,CamInfo*>::iterator it = scp->cams.begin(), end = scp->cams.end();
			it != end; ++it)
	{
		CamInfo *inf= it->second;
		try
		{	
			if(!sequential)
			{
				while(starterThreads >= maxStarterThreads)
				{
					int thId = scp->ci.getJoinableThread();
					while(thId == -1)
					{
						usleep(10000);
						thId = scp->ci.getJoinableThread();
					}
					cout << "******************************************  "
							"CamInitStarterThread:: join thread " << thId << endl;
					scp->ci.join(thId);
					cout << "******************************************  "
							"CamInitStarterThread:: joined thread " << thId << endl;
					--starterThreads;
				}
			}
			int cid=inf->id.id;
			bool reloadDev = reload || DahuaListFind(devs, cid);
			cout << "  ------------------------------------------- CamInit:"
						"" << inf->id.id << " create ExecInfo reload:" << 
						"" << reloadDev << endl;
			CamInit::execInfo *ei = 
						new DahuaControlPlugin::CamInit::execInfo(
																					cid, 
																					conf, 
																					inf->modelStr, 
																					scp, 
																					reloadDev
																				);
			int thid = scp->ci.start(ei);

			cout<<" CamInit:"<<inf->id.id<<" started"<<endl;
			if(sequential)
			{
				scp->ci.join(thid); //descomentar pq sigui sequencial (debug)
			}
			++starterThreads;
		}

		catch(SocketTimeoutException &e)
		{
			//no s'ha trobat la camera?
			cout << "DahuaCP:: loadDBConfig: could not connect to cam " 
						"" << inf->id.id << " SocketTimeoutException:" 
						"" << e.getMsg() << endl;
		}
		catch(SocketException &e)
		{
			//no s'ha trobat la camera?
			cout << "DahuaCP:: loadDBConfig: could not connect to cam "
						"" << inf->id.id << " SocketException:" 
						"" << e.getMsg() << endl;
		}
		catch(Exception &e)
		{
			//no s'ha trobat la camera?
			cout << "DahuaCP:: loadDBConfig: could not connect to cam "
						"" << inf->id.id << " Exception:" 
						"" << e.getClass() << ":" << e.getMsg() << endl;
		}
	}

	cout<<"CamInitStarterThread:: detach "<<endl;

	if(!reload && devs.size()==0)
	{
		scp->ci.detachAll();
	}
	else
	{
		scp->ci.joinAll();
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInitStarterThread::execute" << endl;
#endif
}

DahuaControlPlugin::DahuaControlPlugin(	string file, 
															bool sequentialReloads, 
															bool reloadDevs, 
															bool reloadModels, 
															list<int> reloadDevList	)	:	
																PluginControl(file), 
																db(address, type, cn), 
																file(file)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::DahuaControlPlugin" << endl;
#endif

	conf = getConfigFromFile( file );

	string configFile =	conf->getNodeData(	"[0]/ModelConfigFile"	);
	string devConfig 	=	conf->getNodeData(	"[0]/DeviceConfigFile"	);

	delete conf;
	conf = NULL;

	modelConf = getConfigFromFile( configFile );

	cout<<"loadsModCfg"<<endl;
	loadModelsConfig(modelConf,reloadModels);

	conf = getConfigFromFile( devConfig );

	string s("Select d.id, d.ip, d.stream, d.puerto, m.codigo FROM "
				"dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id "
				"AND d.modelo=m.id AND f.nombre='Dahua'");

	cout << "db.query: " << s << endl;
	XML *devInfo=	db.query(s);
	xmlNode *n 	=	devInfo->getNode("/result");
	int numCams	= 	atoi((n->getParam("numRows")).c_str());

	cout << " loading " << numCams << " Cams" << endl;


	for(int i=0; i<numCams; ++i)
	{
		cout << " loading cam " << i << endl;
		n = devInfo->getNode("/result/[" + StrUtils::decToString(i) + "]/id");

		int id= atoi(n->getCdata().c_str());
		n = devInfo->getNode("/result/[" + StrUtils::decToString(i) + "]/ip");

		string ip = n->getCdata().c_str();
		cout<<" loading cam "<<i<<" id:"<<id<<" ip:"<<ip<<endl;

		 // Se elimina la máscara de red
		if(ip.find('/')!=string::npos)
		{
			ip=ip.substr(0,ip.find('/'));
		}

		n=devInfo->getNode("/result/[" + StrUtils::decToString(i) + "]/puerto");
		int puerto = atoi(n->getCdata().c_str());

		n=devInfo->getNode("/result/[" + StrUtils::decToString(i) + "]/stream");
		int stream = atoi(n->getCdata().c_str());

		n=devInfo->getNode("/result/[" + StrUtils::decToString(i) + "]/codigo");
		string model=n->getCdata();

		Address a(IP(ip), puerto);
		cout<<" loading cam "<<i<<" ip:"<<ip<<" new camInfo:"<<endl;
		if(models.find(model) == models.end())
		{
			models[model] = new ModelInfo();
		}
		cams[id]= new CamInfo(id, a, stream, model, default_buffer_size, this);
		cout << " loading cam " << i << "  id: " << id << " new CI" << endl;
		cout << "                                         - " << ""
					"" << (void*)cams[id] << endl;
	}

	cout << "----------plugin inicialitzat----------"<<endl;

	delete devInfo;

	cist.conf 		= conf;
	cist.scp 		= this;
	cist.reload 	= reloadDevs;
	cist.devs 		= reloadDevList;
	cist.sequential= sequentialReloads;
	cist.start();

	if(!sequentialReloads 			&& 
		!reloadDevs 					&& 
		!reloadModels 					&& 
		(reloadDevList.size()==0))
	{
		cist.detach();
	}
	else
	{
		cist.join();
		exit(0);
	}
#ifdef DEBUG
	cout << "<----- DahuaCP::DahuaControlPlugin" << endl;
#endif
}

XML* DahuaControlPlugin::getConfigFromFile( string file )
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getConfigFromFile: " << file << endl;
#endif
	
	char* buffer(NULL);
	std::size_t size(0);

	ifstream fichero( file.c_str(), ifstream::in );

	if ( fichero.is_open() )
	{
  		filebuf *pbuffer = fichero.rdbuf();
  		size = pbuffer->pubseekoff (0, fichero.end, fichero.in);
  		pbuffer->pubseekpos (0, fichero.in);

  		buffer = new char[size];
  		pbuffer->sgetn (buffer, size);
	}
	else
		cout << "Error opening file '" << file.c_str() << endl;

	fichero.close();

	XML *conf = xmlParser::parse( string( buffer, size ) );

	delete buffer;
	buffer = NULL;

#ifdef DEBUG
	cout << "<----- DahuaCP::getConfigFromFile" <<  endl;
#endif

	return conf;
}

DahuaControlPlugin::~DahuaControlPlugin()
{
#ifdef DEBUG
	cout << "-----> DahuaCP::~DahuaControlPlugin" << endl;
#endif

	for(	map<int, CamInfo*>::iterator it = cams.begin(), end = cams.end(); 
			it != end; ++it)
	{
		if(it->second != NULL)
		{
			delete it->second;
			it->second = NULL;
		}
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::~DahuaControlPlugin" << endl;
#endif
}

void DahuaControlPlugin::serve()
{
#ifdef DEBUG
	cout << "-----> DahuaCP::serve" << endl;
#endif

	Plugin::serve();

#ifdef DEBUG
	cout << "<----- DahuaCP::serve" << endl;
#endif
}

string DahuaControlPlugin::getBasicAuthStrForDevice(int devId)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getBasicAuthStrForDevice" << endl;
#endif
	
#ifdef DEBUG
	cout << "<----- DahuaCP::getBasicAuthStrForDevice" << endl;
#endif

	return string("");
}

/******************************************************************
* getDigestAuthStrForDevice -- Calcula el parámetro 'response' 	*
*	necesario para la autenticación DIGEST por MD5, y devuelve		*
*	la lista de parámetros necesarios para realizar la conexión 	*
*	DIGEST.
* --------------------------------------------------------------- *
* Parámetros:																		*
*		- httpResponse: recoge los parámetros {realm, cnonce, qop}	*
*						que provienen del servidor								*
*		- digest_url:	dirección a la que queremos acceder				*
*		- {digest_user, digest_password}: datos de acceso				*
* Devuelve:																			*
*		- digest_parameters: string que contiene la configuración 	*
*			de parámetros necesarios para conectar por DIGEST 			*
******************************************************************/
string DahuaControlPlugin::getDigestAuthStrForDevice(
					map<string, string> httpResponse, 
					string digest_url, 
					string digest_user, 
					string digest_password)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getDigestAuthStrForDevice" << endl;
#endif

	string digest_parameters= 	"";
	// {realm, nonce, qop} provienen de la respuesta HTTP del servidor
	string digest_realm		=	""; 							
	string digest_nonce 		=	"";
	string digest_qop 		= 	"";
	// {cnonce} Id único generado por el cliente
	// (Obviamente en este caso no es único) --> TODO 20150427
	string digest_cnonce 	= 	"5ac1a94986b6d4ca"; 
	// {nonce-count} Número de serie hexadecimal para la petición 
	// ("en teoría" autoincremental) --> TODO 20150427
	string digest_nc 			= 	"00000001"; 
	// {response}	Validación hash
	string digest_response	= 	""; 
	// {tempPhrase, A1, A2} variables temporales que ayudan a la generación 
	// de <response>
	string tempPhrase			=	"";
	string A1					= 	"";
	string A2					= 	"";
	
	typedef map<string, string>::const_iterator digest_mapIterator;

	// Procedimietno de generación de 'response':
	// 1. Obtenemos los parámetros {nonce, qop, realm}
	digest_mapIterator digest_iter = httpResponse.begin();

	digest_nonce 	= digest_iter->second;
	++digest_iter;
	digest_qop 		= digest_iter->second;
	++digest_iter;
	digest_realm 	= digest_iter->second;

	// 2. Una vez obtenidos los anteriores parámetros, se puede obtener el 
	// parámetro 'response' usando la siguiente fórmula (todos los parámetros en 
	// minúsculas, excepto GET):
	//    A1 = md5(usuario:realm:password)
	//    A2 = md5(request-method:URL) // request-method = GET, POST, etc.
	//    response = md5(A1:nonce:nc:cnonce:qop:A2)
	tempPhrase 	= digest_user + ":" + digest_realm + ":" + digest_password;
	A1 			= md5(tempPhrase);
	tempPhrase 	= "GET:" + digest_url;
	A2 			= md5(tempPhrase);
	tempPhrase 	= 	A1 				+ ":" + 
						digest_nonce 	+ ":" + 
						digest_nc 		+ ":" + 
						digest_cnonce 	+ ":" + 
						digest_qop 		+ ":" + A2;
	digest_response = md5(tempPhrase);

	// 3. Devolvemos los parámetros necesarios para la segunda petición HTTP al 
	//	servidor con la siguiente estructura:
	//  {Authorization: Digest username="...", realm="...", nonce="...", 
	//				uri="...", response="...", qop=..., nc=..., cnonce=...}

	digest_parameters = 	" Digest username=" 	+ digest_user 		+
	             			", realm=" 				+ digest_realm 	+
	             			", nonce=" 				+ digest_nonce 	+
	             			", uri=" 				+ digest_url 		+
	             			", response=" 			+ digest_response +
	             			", qop=" 				+ digest_qop 		+
	             			", nc=" 					+ digest_nc 		+
	             			", cnonce=" 			+ digest_cnonce;

#ifdef DEBUG
	cout << "<----- DahuaCP::getDigestAuthStrForDevice" << endl;
#endif
	return digest_parameters;
}

SessionDispatcher DahuaControlPlugin::getSession(Address a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getSession" << endl;
#endif

	map<string,SessionDispatcher>::iterator i=sessions.find(	a.toString()	);

	if (i == sessions.end())
	{
		throw (CPSessionNotStablishedException(
															0, 
															string("Session no establerta")
															));
	}

	i->second.initializeTimeOfSession();

#ifdef DEBUG
	cout << "<----- DahuaCP::getSession" << endl;
#endif

	return i->second;
}

void DahuaControlPlugin::checkModelMetadata(string nom)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::checkModelMetadata" << endl;
#endif

	// Correccions a les funcions del model segons si s'indica que
	// te DEPA o no (/Capabilities/Metadata/DEPA)
	string depaSupport 			= db.getModelConfigParam(
																"Dahua", 
																nom, 
																"/Capabilities/Metadata/DEPA"
																		);
	string funcionesIdQuery 	= 	
	   "SELECT m.funciones from modelo m, fabricante f where m.codigo='" + 
	   	nom + "' AND m.fabricante = f.id AND f.nombre='Dahua'";			
	string hasMetadataQuery 	= 
		"SELECT fm.id FROM funcionesmodelo fm, funcionesmodelometadata fmm "
		"WHERE fm.id=fmm.id AND fm.id=(" + funcionesIdQuery + ")";
	XML *hasMetadata 				= db.query(hasMetadataQuery);
	xmlNode *hasMetadataNode	= hasMetadata->getNode("/result");
	int nMeta 						= 
		atoi( (hasMetadataNode->getParam("numRows")).c_str() );

	delete hasMetadata;
	hasMetadata = NULL;

	// Query que trobara el mateix funcionesmodelo sense metadata o
	// amb, segons si el nostre te o no metadata.
	string alternativeQuery 	= 
		"SELECT fm2.id FROM funcionesmodelo fm LEFT OUTER JOIN "
		"funcionesmodelovideo fmv ON fm.id=fmv.id LEFT OUTER JOIN "
		"funcionesmodeloaudio fma ON fm.id=fma.id LEFT OUTER JOIN "
		"funcionesmodeloio fmio ON fm.id=fmio.id LEFT OUTER JOIN "
		"funcionesmodelometadata fmm ON fm.id=fmm.id, "
		"funcionesmodelo fm2 LEFT OUTER JOIN funcionesmodelovideo fmv2 "
		"ON fm2.id=fmv2.id LEFT OUTER JOIN funcionesmodeloaudio fma2 ON "
		"fm2.id=fma2.id LEFT OUTER JOIN funcionesmodeloio fmio2 ON "
		"fm2.id=fmio2.id LEFT OUTER JOIN funcionesmodelometadata fmm2 ON "
		"fm2.id=fmm2.id WHERE fm.id=(" + funcionesIdQuery + ") "
		"AND (fmv.id IS NULL) = (fmv2.id IS NULL) "
		"AND (fma.id IS NULL) = (fma2.id IS NULL) "
		"AND (fmio.id IS NULL) = (fmio2.id IS NULL) "
		"AND fmm2.id IS " +(nMeta == 0 ? string("NOT ") : string("")) + 
		"NULL;";

	if ((nMeta==0 && depaSupport==string("true")) 	|| 
		 (nMeta >0 && depaSupport==string("false")))
	{
		XML *alt 			= db.query(alternativeQuery);
		xmlNode *altNode	= alt->getNode("/result");
		int nAlt 			= atoi((altNode->getParam("numRows")).c_str());

		string funcionesId = altNode->getNodeData(string("/result/[0]/id"));
		if (nAlt>0)
		{
			string update = "UPDATE modelo SET funciones='" + funcionesId + 
				"' FROM fabricante f WHERE codigo='" + nom + 
				"' AND fabricante=f.id AND f.nombre='Dahua';";
			XML *upd =db.update(update);
			delete upd;
			upd = NULL;
		}

		delete alt;
		alt = NULL;
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::checkModelMetadata" << endl;
#endif
}

void DahuaControlPlugin::loadModelsConfig(XML *conf, bool reload)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::loadModelsConfig" << endl;
#endif

	cout<<" ----------- DahuaCP:: load Models Config ---------- " << endl;
	string s("select m.codigo from modelo m, fabricante f "
				"where m.fabricante=f.id AND f.nombre='Dahua'");

	XML *devInfo=db.call(s, 0);

	xmlNode *n 	=	devInfo->getNode("/result");
	int numMod 	= 	atoi((n->getParam("numRows")).c_str());

	string nom(""); 
	for(int i = 0; i < numMod; ++i)
	{
		nom = devInfo->getNodeData(string("/result/[") + 
							StrUtils::decToString(i)+string("]/codigo"));

		if(	reload 						|| 
				!db.checkModelConfigParam("Dahua", nom, "/Capabilities")	)
		{
			cout << 	"       ------------------- DahuaCP:: load Model Config - "
						"" << nom << endl;
			loadModelConfig(nom, "/", "/", conf);
			checkModelMetadata(nom);
		}
	}

	delete devInfo;
	devInfo = NULL;

#ifdef DEBUG
	cout << "<----- DahuaCP::loadModelsConfig" << endl;
#endif
}

void DahuaControlPlugin::loadModelConfig(
														string model, 
														string nodepath, 
														string dbpath, 
														XML *conf
													)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::loadModelConfig" << endl;
#endif

	if(nodepath == "/")
	{
		nodepath = "/[0]/Models/All/";
	}

	xmlNode *n = conf->getNode(nodepath);

	list<xmlNode*> kids 	= n->getKidList();
	int pos 					= nodepath.find("All");

	//fulla, o node amb dades a introduir al nodoconfig
	if(kids.size()==0 || n->getCdata() != "") 
	{
		// generica, busquem especifica del model
		if(pos!=string::npos) 
		{
			nodepath.replace(pos,3, model);
			xmlNode *m = conf->getNode(nodepath);

			if(m!=NULL)
			{
				n=m;
			}
		}

		string s = db.getModelConfigParam("Dahua", model, dbpath);
		if(s != n->getCdata() || s == "")
		{
			db.setModelConfigParam("Dahua", model, dbpath, n->getCdata());
		}
	}

	kids 	= n->getKidList();
	pos 	= nodepath.find("All");

	if(kids.size()>0)
	{
		if(pos==string::npos) //especifica del model
		{
			int dbidx = 0;
			for(	list<xmlNode*>::iterator it = kids.begin(), end = kids.end(); 
					it != end; ++it)
			{
				if((*it)->getParam("nodename")==string("inc"))
				{
					loadModelConfig(
											model, 
											nodepath + (*it)->getName() + "[" + 
												StrUtils::decToString(dbidx) + "]/",
											dbpath + StrUtils::decToString(dbidx+1) + "/", 
											conf
										);
					++dbidx;
				}
				else
				{
					loadModelConfig(
											model, 
											nodepath + (*it)->getName() + "/", 
											dbpath + (*it)->getName() + "/", 
											conf);
				}
			}
		}
		else
		{	//config generica, hem de tenir en compte el model
			bool inc 	= false;
			int nidx 	= 0;
			for(	list<xmlNode*>::iterator it = kids.begin(), end = kids.end(); 
					it != end; ++it)
			{
				if((*it)->getParam("nodename")==string("inc"))
				{
					inc=true;
				}
				else
				{
					loadModelConfig(
										model, 
										nodepath + (*it)->getName() + "/", 
										dbpath+(*it)->getName() + "/", 
										conf
										);
				}
				++nidx;
			}

			if(inc)
			{
				string mpath 	= nodepath;
				mpath.replace(pos,3, model);
				xmlNode *m 		= conf->getNode(mpath);
				inc 				= false;
				if(m!=NULL)
				{
					list<xmlNode*> mkids=m->getKidList();

					int dbidx=0;
					//mirem si tenim incs especifics del model
					for(list<xmlNode*>::iterator it = mkids.begin(), end=mkids.end(); 
							it != end; ++it) 
					{
						if((*it)->getParam("nodename")==string("inc"))
						{
							loadModelConfig(
												model, 
												mpath + (*it)->getName() + "[" + 
													StrUtils::decToString(dbidx) + "]/",
												dbpath + StrUtils::decToString(dbidx+1)+"/", 
												conf
												);
							++dbidx;

							inc=true;
						}
					}
				}

				//si no en tenim, pillem els generics
				if(!inc) 
				{
					int dbidx=0;
					//cout<<" loadModelConfig: config all no model inc"<<endl;
					for(list<xmlNode*>::iterator it = kids.begin(), end = kids.end(); 
							it != end; ++it)
					{
						if((*it)->getParam("nodename")==string("inc"))
						{
							loadModelConfig(
											model, 
											nodepath + (*it)->getName() + "[" + 
												StrUtils::decToString(dbidx) + "]/", 
											dbpath + StrUtils::decToString(dbidx+1) + "/", 
											conf
											);
							++dbidx;
						}
					}
				}
			}
		}
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::loadModelConfig" << endl;
#endif

}

void DahuaControlPlugin::loadDevConfig(
													int id, 
													string model, 
													string nodepath, 
													string dbpath, 
													XML *conf
													)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::loadDevConfig1" << endl;
#endif

	cout << "loadDevConfig --> no db" << endl;
	this->loadDevConfig(id, model, nodepath, dbpath, conf, &this->db);

#ifdef DEBUG
	cout << "<----- DahuaCP::loadDevConfig1" << endl;
#endif
}

void DahuaControlPlugin::loadDevConfig(
													int id, 
													string model, 
													string nodepath, 
													string dbpath, 
													XML *conf, 
													DBGateway *db
													)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::loadDevConfig2" << endl;
#endif

	if (	(nodepath.length() < 13) || 
			(nodepath.substr(5,7) != string("Devices")))
	{
		nodepath = string("/[0]/Devices/")+model;
	}

	xmlNode *n = conf->getNode(nodepath);

	if(n == NULL)
	{
		cout << " LoadDevConfig n == NULL!!! " << nodepath << ""
			" db:" << dbpath << endl;

#ifdef DEBUG
		cout << "<----- DahuaCP::loadDevConfig2" << endl;
#endif
		return;
	}

 	//filter. al principi, pots volguer filtrar tota una branca
	if(n->getParam("filter")!=string(""))
	{
		string filter 	= n->getParam("filter");
		int pos 			= filter.rfind("=");
		if(pos!=string::npos)
		{
			string fdbpath 	= addPath(dbpath, filter.substr(0, pos));
			string value 		= db->getDeviceConfigParam(id, fdbpath);
			string fvalue 		= filter.substr(pos+1);

			if(value!=fvalue)
			{
				cout<<"|";

#ifdef DEBUG
				cout << "<----- DahuaCP::loadDevConfig2" << endl;
#endif
				return;
			}
		}
	}

	list<xmlNode*> kids=n->getKidList();
	// node intermig
	if(kids.size()>0) 
	{
		cout << "node intermig" << endl;

		// index del node dins la branca del xml. 
		// XML->getNode("/foo/bar/["+nidx+"]")
		int nidx 	= 0; 
		int dbidx 	= 1;
		 // index del node dins la DB. pels nodename="inc"
		int absidx 	= 0;

		//per cada fill
		for(	list<xmlNode*>::iterator it = kids.begin(), end = kids.end(); 
				it != end; ++it) 
		{
			// nodes <1>, <2>, ... ,<n>
			if( 	(*it)->getParam("nodename") == "inc") 
			{
				// no depĂ©n de cap resultat, crida recursiva.
				if(	(*it)->getParam("values") == "") 
				{	
					loadDevConfig(
										id,
										model, 
										nodepath + "/" + (*it)->getName() + "[" +
											StrUtils::decToString(nidx) + "]",
										dbpath + "/" + StrUtils::decToString(dbidx), 
										conf, 
										db
										);
					++nidx;
					++dbidx;
				}
				// els valors depenen d'una resposta de la cam carregar de/a la 
				// cam per cada value
				else
				{ 

					string val 		= (*it)->getParam("values");
					//busquem el node al que fa referencia values
					string vnpath 	= addPath(	nodepath + "/" + (*it)->getName(), 
														val); 
					xmlNode *vn 	= conf->getNode(vnpath);

					if(	(vn!=NULL) 								&& 
							(vn->getParam("inquiry") != "")	)
					{
						
						string vdbpath	= addPath(	dbpath + "/" + 
																StrUtils::decToString(dbidx), 
															val);

						//si no existeix el node actual
						if(!db->checkDeviceConfigParam(id, vdbpath)) 
						{
							cout << "No existe el nodo actual" << endl;
						}

						//ja existeix a la BD, recarreguĂ©m les coses a la camera
						else
						{ 
							string values("");
							do
							{
								values 	=	values + "," + 
												db->getDeviceConfigParam(id, vdbpath);
								++dbidx;
								vdbpath 	= 	addPath( dbpath + "/" + 
																StrUtils::decToString(dbidx), 
															val);
							} while(db->checkDeviceConfigParam(id, vdbpath));

							int pos 	=	val.rfind("/");
							if (values != string("") && pos!=string::npos)
							{
								cout << "values != EMPTY" << endl;
							}
						}
						++nidx;
					}
				}
			}
			//node intermig "normal", crida recursiva
			else
			{	
				loadDevConfig(	id, 
									model, 
									nodepath + "/[" + StrUtils::decToString(absidx) + "]", 
									dbpath + "/" + (*it)->getName(), 
									conf, 
									db);
			}
			++absidx;
		}
	}

	//node fulla
	else
	{	
		cout << "node fulla" << endl;

		bool dbCheck=false;

		if(n->getName() == "Area")
		{
			dbCheck = (db->getDeviceConfigParam(id, dbpath + "/xMin") != "") 	&&
						 (db->getDeviceConfigParam(id, dbpath + "/yMin") != "") 	&&
						 (db->getDeviceConfigParam(id, dbpath + "/xMax") != "") 	&&
						 (db->getDeviceConfigParam(id, dbpath + "/yMax") != "");
		}
		else if(n->getName() == "Resolution")
		{
			dbCheck = (db->getDeviceConfigParam(id, dbpath + "/x") != "") 		&&
						 (db->getDeviceConfigParam(id, dbpath + "/y") != "");
		}
		else if ((n->getName() == "Min" || n->getName() == "Max") 				&& 
					(nodepath.find("/ObjectDetection") != string::npos))
		{
			dbCheck = (db->getDeviceConfigParam(id, dbpath + "/x") != "") 		&&
						 (db->getDeviceConfigParam(id, dbpath + "/y") != "");
		}
		else
		{
			dbCheck = (db->getDeviceConfigParam(id, dbpath) != "");
		}
		if(!dbCheck)
		{
			cout<<" !dbCheck " << dbpath << " : " << dbCheck << endl;
		}

		// Si no está en la base de datos, lo cargamos de la cámara
		if( n->getParam("mode") == "reload" || !dbCheck) 
		{
			cout << " loadDevConfig not in db->" << dbpath << endl;

			if(n->getParam("inquiry") != string(""))
			{
				cout << " loadDevConfig inq:" << n->getParam("inquiry") << endl;

				char url[256];
				string param 	= n->getParam("param");
				string inq 		= n->getParam("inquiry");

				string value("");

				sprintf(	url, 
							"http://%s/cgi-bin/about.cgi?msubmenu=capability&action=view", 
				 			cams[id]->cam.getIP().toString().c_str()	);
				list<string> parms = StrUtils::split(param,",");

				for(list<string>::iterator it_param = parms.begin(), end=parms.end(); 
						it_param != end; ++it_param)
				{
					cout << "Params: " << *it_param << endl;
					value += string(",") + getConfigParamCamera( url, 
																				*it_param, 
																				model, 
																				id);
				}
				
				if(value.length() > 1)
				{
					value = value.substr(1);
				}

				cout << " loadDevConfig: setDBCP translate read: "
							"" << n->getParam("param") <<	" " << n->getName() << ": " 
							"" << value << " - " << cams[id]->model << endl;
				cout<< " Param: " << n->getParam("param") << " getName: " 
							"" << n->getName() << " values: " << value << endl;

				value  = translateCam(n->getName(), value, cams[id]->model);

				if(value==string("altParam"))
				{
					/*
					sprintf(	url,
								"http://%s/command/inquiry.cgi?inq=%s", 
								cams[id]->cam.getIP().toString().c_str(), 
								n->getParam("altInquiry").c_str());
					*/
					value	= getConfigParamCamera(	url, 
															n->getParam("altParam"), 
															model, 
															id);
					value	= translateCam(n->getName(), 
												value, 
												cams[id]->model);
				}

				// aixĂł no hauria d'estar hardcodat al codi...
				if(n->getName()==string("Resolution")) 
				{
					int pos;
					if((pos 	= value.find("."))!=string::npos)
					{
						if(db->getDeviceConfigParam(id, "/Video/Standard/") == "pal")
						{
							value = value.substr(0,pos);
						}
						else
						{
							value = value.substr(pos+1);
						}
					}

					if(n->getParam("mode") != "noDB") 
					{
						int pos=value.find(",");
						if(pos!=string::npos)
						{
							db->setDeviceConfigParam(	id, 
																dbpath + "/x", 
																value.substr(0,pos));
							db->setDeviceConfigParam(	id, 
																dbpath + "/y", 
																value.substr(pos+1));
						}
					}
				}
				else if 	( (n->getName() == "Area" ) 										&& 
							(nodepath.find("/ActivityDetection") != string::npos )	|| 
							(nodepath.find("/Alarms") != string::npos) )
				{
					if (n->getParam("mode") != "noDB"	) 
					{
						int pos 	= value.find(",");
						if(pos!=string::npos)
						{
							db->setDeviceConfigParam(	id, 
																dbpath + "/xMin", 
																value.substr(0,pos));
							value =	value.substr(pos+1);
							pos 	=	value.find(",");
							if(pos!=string::npos)
							{
								db->setDeviceConfigParam(	id, 
																	dbpath + "/yMin", 
																	value.substr(0,pos));
								value =	value.substr(pos+1);
								pos 	=	value.find(",");
								if(pos!=string::npos)
								{
									db->setDeviceConfigParam(	id, 
																		dbpath + "/xMax", 
																		value.substr(0,pos));
									value 	=	value.substr(pos+1);
									db->setDeviceConfigParam(	id, 
																		dbpath + "/yMax", 
																		value);
								}
							}
						}
					}
				}
				else if 	(	(n->getName() == "Min") 										|| 
						 		(n->getName() == "Max") 										&& 
						 		(nodepath.find("/ObjectDetection") != string::npos)	)
				{
					if(n->getParam("mode") != "noDB"	)
					{
						int pos 	=	value.find(",");
						if(pos!=string::npos)
						{
							db->setDeviceConfigParam(	id, 
																dbpath + "/x", 
																value.substr(0,pos));
							db->setDeviceConfigParam(	id, 
																dbpath + "/y", 
																value.substr(pos+1));
						}
					}
				}
				else
				{
					if(n->getParam("mode")!=string("noDB"))
					{
						db->setDeviceConfigParam(id, dbpath, value);
					}
				}
			}
		}
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::loadDevConfig2" << endl;
#endif
}

//translate from Camera to DB
string DahuaControlPlugin::translateCam(string param, string value, int model) 
{
#ifdef DEBUG
	cout << "-----> DahuaCP::translateCam" << endl;
#endif	
	if (param == "Fourcc")
	{
		list<string> l 	=	StrUtils::split(value,"-"); //multicodec
		string res("");
		for(list<string>::iterator it=l.begin(), end = l.end(); it != end; ++it)
		{
			if((*it) == "mpeg4" || (*it) == "1"	)
			{
				res 	+=	",mp4v";
			}
			else if((*it) == "0")
			{
				res 	+=	",jpeg";
			}
			else
			{
				res 	+=	"," + (*it);
			}
		}
		if(res.length() >= 1)
		{
			res=res.substr(1);
		}

#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::Fourcc" << endl;
#endif	
		return res;
	}
	else if(param == "Standard")
	{

#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::Standard" << endl;
	#endif	

		if (value == "pal" || value == "1")
		{
			return "PAL";
		}
		else if (value == "ntsc" || value == "0")
		{
			return "NTSC";
		}
		else
		{
			return value;
		}
	}
	else if (param == "Resolution"	)
	{
		int pos=value.find(",");
		string val1, val2;
		if(pos!=string::npos)
		{
			val1 	= value.substr(0, pos);
			val2 	= value.substr(pos+1);
		}
		int p 	=	atoi(val1.c_str());
		int q 	=	atoi(val2.c_str());
#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::Resolution" << endl;
#endif	

		switch(p)
		{
			case 704:
				return string("704,576");
			case 640:
				return string("640,480");
			case 352:
				return string("352,264");
			case 320:
				return string("320,240"); 
			default:
				//millor que res 	
				return string("640,480"); 
		}
	}
	else if(param == "MountAngle" )
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::MountAngle" << endl;
#endif	

		return string("0");
	}
	else if(param == "FPS")
	{

#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::FPS" << endl;
#endif	

		return string("25");
	}
	else if(param == "Sensivity" )
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::Sensivity" << endl;
#endif	
		return ("0");
	}
	else if(param==string("DetectionTime"))
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::DetectionTime" << endl;
#endif	
		return ("0");
	}
	else if(param==string("Threshold"))
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::Threshold" << endl;
#endif	
		return ("0");
	}
	else if(param==string("Mode"))
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::Mode" << endl;
#endif	
		if(value=="mod")
		{
			return string("Moving Object Detection");
		}
		else if(value=="uod")
		{
			return string("Unattended Object Detection");
		}
	}
	else if(param==string("State"))
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::State" << endl;
#endif	

		if(value=="on")
		{
			return string("night");
		}
		else if(value=="off") 
		{
			return string("day");
		}
	}
	else if(param==string("MotionDetectionEnable"))
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::translateCam::MotionDetectionEnable" << endl;
#endif	
		if(value=="on")
		{
			return string("true");
		}
		else
		{
			return string("false");
		}
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::translateCam" << endl;
#endif	
	return value;
}

//translate from DB to Camera
string DahuaControlPlugin::translateDB(string param, string value, int model) 
{
#ifdef DEBUG
	cout << "-----> DahuaCP::translateDB" << endl;
#endif	
	cout<<" TranslateDB Par:"<<param<<":"<<value<<" model:"<<model<<endl;
	if(param==string("Fourcc"))
	{
		list<string> l = StrUtils::split(value,","); //multicodec

		string res;
		for(	list<string>::iterator it = l.begin(), end = l.end(); 
				it != end; ++it)
		{
			if((*it) == string("mp4v"))
			{
				res += string("-mpeg4");
			}
			else
			{
				res+=string("-")+(*it);
			}
		}
		if(res.size()>=1)
		{
			res=res.substr(1);
		}

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::Fourcc" << endl;
#endif	
		return res;
	}
	else if(param==string("Standard"))
	{

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::Standard" << endl;
#endif	
		if(value==string("PAL"))
		{
			return string("pal");
		}
		else if(value==string("NTSC"))
		{
			return string("ntsc");
		}
		else
		{
			return value;
		}
	}
	else if(param==string("Resolution"))
	{
		int pos = value.find(",");
		if(pos!=string::npos)
		{
			value=value.substr(0, pos);
		}
		int p = atoi(value.c_str());

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::Resolution" << endl;
#endif	
		switch(p)
		{
			case 704:
				return string("704,576");
			case 640:
				return string("640,480");
			case 352:
				return string("352,264");
			case 320:
				return string("320,240"); 
			default:
				//millor que res 	
				return string("640,480"); 

		}
	}
	else if(param==string("Sensitivity"))
	{

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::Sensitivity" << endl;
#endif	

	}
	else if(param==string("Threshold"))
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::Threshold" << endl;
#endif	
		return "0";
	}
	else if(param==string("DetectionTime"))
	{
		int pos;
		while((pos=value.find(string(":")))!=string::npos)
		{
			value=value.substr(0,pos)+value.substr(pos+1);
		}

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::DetectionTime" << endl;
#endif	
		return value;
	}
	else if(param==string("Mode"))
	{

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::Mode" << endl;
#endif	
		if(value=="Moving Object Detection")
		{
			return string("mod");
		}
		else if(value=="Unattended Object Detection")
		{
			return string("uod");
		}
	}
	else if(param==string("MountAngle"))
	{
		int p=atoi(value.c_str());

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::MountAngle" << endl;
#endif	
	}
	else if(param == string("State"))
	{

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::State" << endl;
#endif	

		if(value=="day")
		{
			return string("off");
		}
		else if(value=="night")
		{
			return string("on");
		}
	}
	else if(param==string("FPS"))
	{
		
			value==string("25");

	}
	else if(param==string("MotionDetectionEnable"))
	{

#ifdef DEBUG
		cout << "<----- DahuaCP::translateDB::MotionDetectionEnable" << endl;
#endif	
		if(value=="true")
		{
			return string("on");
		}
		else
		{
			return string("off");
		}
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::translateDB" << endl;
#endif	
	return value;
}

// "suma" un path real i un de relatiu.
string DahuaControlPlugin::addPath(string path, string relPath) 
{
#ifdef DEBUG
	cout << "-----> DahuaCP::addPath" << endl;
#endif	
	if(path.size()>0 && path[path.size()-1]=='/')
	{
		path=path.substr(0, path.size()-1);
	}

	while(relPath.find(string("../"))==0)
	{
		relPath=relPath.substr(3);
		int pos=path.rfind("/");
		if(pos!=string::npos)
		{
			path=path.substr(0, pos);
		}
	}

#ifdef DEBUG
	cout << "<----- DahuaCP::addPath" << endl;
#endif	
	return path+string("/")+relPath;
}

void* DahuaControlPlugin::CamInit::execute(int thid, void *parm)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::CamInit::execute" << endl;
#endif	

	cout << "    CamInit::execute " << endl;
	execInfo *i 	=	(execInfo*) parm;

	cout << " " << thid << "   0 CamInit::execute i:" << (void*)i << endl;
	int id 	=	i->id;
	cout << " " << thid << "   0.5 CamInit::execute scp:" << (void*)i->scp <<
			"" << endl;
	cout << " " << thid << "  ++++++  0.5 CamInit::execute cam:" << id << " :"
			"" << (void*)i->scp->cams[id] << endl;
	if(i->scp->cams[id]->initing==false)
	{
		i->scp->cams[id]->initing=true;
		try
		{
			XML *conf 		= 	i->conf; 
			string model 	= 	i->model; 
			bool reload 	= 	i->reload;

			cout << " " << thid << "   1 CamInit::execute " << id << ":" 
				"" << model << endl;
			i->scp->loadDBConfig(id, conf, model, reload);
			i->scp->cams[id]->inited 		 	= true;
			i->scp->cams[id]->error_logged 	= false;
			cout << " " << thid << "   2 CamInit::loaded config: " << id << ":" 
				"" << model << endl;
		}
		catch(Exception &e)
		{
			cout<<" ---------- --------- ------------- DahuaCP::CamInit::exec: "
				"" << i->id << ":" << i->model << " Exception:" 
				"" << e.getClass() << ":" << e.getMsg() << endl;
		}

		i->scp->cams[id]->initing 	= false;

	}
	delete i;
	i = NULL;

	cout << " " << thid << "  ++++++  return CamInit::execute cam:" << id << 
		"" << endl;

#ifdef DEBUG
	cout << "<----- DahuaCP::CamInit::execute" << endl;
#endif	
}

void DahuaControlPlugin::loadDBConfig(	CPDeviceID id, 
														XML *conf, 
														string model, 
														bool reload )
{
#ifdef DEBUG
	cout << "-----> DahuaCP::loadDBConfig" << endl;
#endif	

	Address cam 		= cams[id.id]->cam;
	string modelStr 	= cams[id.id]->modelStr;
	cout << "loadDBConfig:" << cam.toString() << "::" << id.id << "::" 
				"" << model << endl;
	
	DBGateway *dbLoad = NULL;
	try
	{
		if(reload)
		{
			cout << "deleteDevCfg" << endl;
			try
			{
				if(dbLoad == NULL)
				{
					dbLoad = new DBGateway(this->address, type, cn);
				}
				dbLoad->deleteDeviceConfig(id.id, "/");
			}
			catch(Exception &e)
			{
				cout<< "db.deleteDevCfg Exception:" << e.getClass() << ":" 
						"" << e.getMsg() << endl;
			}
			cout<<"/deleteDevCfg"<<endl;
		}
	}
	catch(Exception &e)
	{
		cout << 	"  DahuaCP::CamInit::exec: " << id.id << 
					"  deleteDevConfig Exception:" << e.getClass() << ":" 
					"" << e.getMsg() << endl;
	}

	if(dbLoad == NULL)
	{
		dbLoad = new DBGateway(this->address, type, cn);
	}
	bool dbCheck 		= true;
	bool xfound 		= false;
	bool yfound 		= false;
	bool fourccfound 	= false;
	bool fpsfound 		= false;
	bool qualFound 	= false;
	bool brFound 		= false;

	int cid 				= dbLoad->getDeviceConfigNode(id.id, "/Video");

	if(cid == -1)
	{
		cout << 	" error while checking params in db for cam:" << id.id << 
					" " << cams[id.id]->cam.toString() << endl;
		dbCheck 			= false;
	}
	else
	{
		string s = 	"SELECT * FROM getconfigparamrecursive(" + 
						StrUtils::decToString(cid) + ",'')"; 
		XML *res=dbLoad->call(s, 0);
		xmlNode *n=res->getNode("/result");

		if(n == NULL)
		{
			cout << " error while checking params in db for cam:" << id.id << " : "
						"" << res->toString() << endl;
		}
		else
		{
			int nrows=n->getKidList().size();
			string value("");
			string name("");
			for(int i = 0; i < nrows; ++i)
			{
				value = res->getNodeData("/result/[" + StrUtils::decToString(i) + 
							"]/value");
				name 	= res->getNodeData("/result/[" + StrUtils::decToString(i) + 
							"]/name");

				if(value == string(""))
				{
					continue;
				}
				if (	(value != "") && (value != "0")	)
				{
					if			(name.find("x") != string::npos)
					{
						xfound 		= true;
					}
					else if	(name.find("y") != string::npos)
					{
						yfound 		= true;
					}
					else if 	(name.find("Fourcc") != string::npos)
					{
						fourccfound = true;
					}
					else if 	(name.find("FPS") != string::npos)
					{
						fpsfound 	= true;
					}
					else if 	(name.find("Quality") != string::npos)
					{
						qualFound 	= true;
					}
					else if 	(name.find("Bitrate") != string::npos)
					{
						brFound 		= true;
					}
				}
			}
			dbCheck = 	xfound 		&&  
							yfound 		&&  
							fourccfound	&&  
							fpsfound 	&&  
							(qualFound ||  brFound);
			if(!dbCheck)
			{
				cout << "Camera config not found, reloading : dbCheck = "
							"x: " 		<< xfound 		<<
							" && y: " 	<< yfound 		<<
							" && fcc:" 	<< fourccfound <<
							" && fps:"	<< fpsfound 	<<
							" && ( ql:" << qualFound << " ||  br:" << brFound << ")"
							"" << endl;
			}
		}
	}
	
	if(reload || !dbCheck)
	{
		cout << " ******************************* recarreguem config device:"
					"" << id.id << endl;
	 	if(dbLoad==NULL)
	 	{
			dbLoad = new DBGateway(this->address, type, cn);
	 	}
		try
		{
			loadDevConfig(id.id, model, "", "", conf, dbLoad);
		}
		catch(Exception e)
		{
			cout << "CamInit::loadDevConfig 2 - cam:" << id.id << " db:" 
						"" << (void*)dbLoad << " " << e.getClass() << "::" 
						"" << e.getMsg() << endl;
		}

		cout<<" LoadDBConfig::::"<<id.id<<"::"<<model<<endl;

		try
		{
			if 	(dbLoad->getDeviceConfigParam(id.id, "/Presets") == "")
			{
				dbLoad->setDeviceConfigParam(id.id, "/Presets", "1");
			}

			if 	(dbLoad->getDeviceConfigParam(id.id, "/Schedules") == "")
			{
				dbLoad->setDeviceConfigParam(id.id, "/Schedules", "1");
			}

			if 	(dbLoad->getDeviceConfigParam(id.id, "/Tours") == "")
			{
				dbLoad->setDeviceConfigParam(id.id, "/Tours", "1");
			}

			dbLoad->setDeviceConfigParam(id.id, "/Output/1", "0");
		}
		catch(Exception e)
		{
			cout << "CamInit::exec 3 - cam:" << id.id << "  " << e.getClass() << 
						"::" << e.getMsg() << endl;
		}
	}

	if(dbLoad!=NULL)
	{
		delete dbLoad;
		dbLoad = NULL;
	}

	cout<<"DahuaCP::config loaded:"<<id.id<<endl;

#ifdef DEBUG
	cout << "<----- DahuaCP::loadDBConfig" << endl;
#endif	
}

void DahuaControlPlugin::startSession(CPDeviceID id, Address *a)
{	
#ifdef DEBUG
	cout << "-----> DahuaCP::startSession" << endl;
#endif	

	cout << "DahuaCP::StartSession:" << dec << id.id << " a:" 
				"" << a->toString()<<endl;
	map<string,SessionDispatcher>::iterator itSes = sessions.find(a->toString());
	bool sessionEnded 	= false;
	int oldSesID 			= 0;

	if(itSes != sessions.end())
	{
		if(id.id == itSes->second.getIdNumber())
		{
			throw(CPSessionAlreadyStablishedException(1, " Sessio ja establerta"));
		}
		else
		{
			//tenia sessiĂł amb un altre dispositiu -> tanquĂ©m i iniciem la nova 
			oldSesID 		= itSes->second.getIdNumber(); 
			sessionEnded	= true;
			try
			{
				endSession(itSes->second.getIdNumber(), a);
			}
			catch(Exception &e){}
		}
	}

	map<int, CamInfo*>::iterator itBuff 	= cams.find(id.id);

	Address cam;

	if (	itBuff != cams.end() )
	{
		cout << "DahuaCP::StartSession cam found " 
					"" << (void*) itBuff->second << endl;

		bool firstSession = itBuff->second->firstSession;
		cam 					= itBuff->second->cam;

		// Si no estan iniciats els threads, que s'iniciin
		cams[id.id]->startThreads();


		if(firstSession)
		{
			ModelInfo *mi 	= models[cams[id.id]->modelStr];
			RPC *rpc 		= NULL;
			try
			{

				//cout << "Entramos en el TRY" << endl;

				RPC *rpc=db.getRPC(a,5);
				if(isnan(mi->minx))
				{
					mi->minx = atof(db.getModelConfigParam("Dahua", 
																		cams[id.id]->modelStr, 
																		"/Limits/Min/x", 
																		rpc).c_str());
				}
				if(isnan(mi->miny))
				{
					mi->miny = atof(db.getModelConfigParam("Dahua", 
																		cams[id.id]->modelStr, 
																		"/Limits/Min/y",
																		rpc).c_str());
				}
				if(isnan(mi->minz))
				{
					mi->minz = atof(db.getModelConfigParam("Dahua", 
																		cams[id.id]->modelStr, 
																		"/Limits/Min/z", 
																		rpc).c_str());
				}
				if(isnan(mi->maxx))
				{
					mi->maxx = atof(db.getModelConfigParam("Dahua", 
																		cams[id.id]->modelStr, 
																		"/Limits/Max/x", 
																		rpc).c_str());
				}
				if(isnan(mi->maxy))
				{
					mi->maxy = atof(db.getModelConfigParam("Dahua", 
																		cams[id.id]->modelStr, 
																		"/Limits/Max/y", 
																		rpc).c_str());
				}
				if(isnan(mi->maxz))
				{
					mi->maxz = atof(db.getModelConfigParam("Dahua", 
																		cams[id.id]->modelStr, 
																		"/Limits/Max/z", 
																		rpc).c_str());
				}
				db.freeRPC(rpc);
				rpc 	= NULL;

			}
			catch(Exception &e)
			{
				cout << "DahuaCP::startSession(" << id.id << ") " 
							"" << e.getClass() << ":" << e.getMsg() << endl;
				if(rpc!=NULL)
				{
					db.freeRPC(rpc);
				}
			}
			catch(...)
			{
				cout << "DahuaCP::startSession(" << id.id << ") unknown exception" 
							"" << endl;
				if(rpc!=NULL)
				{
					db.freeRPC(rpc);
				}
			}
		}
	}
	else
	{
		// no es a la llista de cameres, mirem si l'han afegit al sistema 
		// des de la inicialitzacio i l'afegim
		cout << "DahuaCP::StartSession:" << id.id << " NOT inited" << endl;
		
		// Separem perque hi ha variables que "col·lisionen"
		{
			RPC *rpc 	= db.getRPC(a,5);
			string model("");

			try
			{
				cout << "DahuaCP:: load new Camera:" << id.id << endl;
				string s("Select d.id, d.ip, d.puerto, m.codigo "
							"FROM dispositivo d, modelo m, fabricante f "
							"WHERE d.fabricante=f.id AND d.modelo=m.id AND "
									"f.nombre='Dahua' AND d.id=");
				s 					+= StrUtils::decToString(id.id);
				XML *devInfo 	= 	db.call(s, 0, rpc);
				xmlNode *n 		= 	NULL;
				if(devInfo != NULL)
				{
					cout<<"devInfo!= NULL"<<endl;
					cout<<devInfo->toString()<<endl;
					n 	=	devInfo->getNode("/result/row/ip");
				}

				RPC::keepAlive(a, type);
	
				if(n==NULL) //no es enlloc, dispositiu invalid
				{
					db.freeRPC(rpc);
					cout << "Disp Invalid: Query: " << s << ", xml: " 
								"" << (devInfo != NULL ? devInfo->toString() : "") << 
								"" << endl;
					if(devInfo != NULL)
					{
						delete devInfo;
						devInfo = NULL;
					}
					throw(CPInvalidParamException(0, "Dispositiu invalid"));
				}
				string sip 	=	n->getCdata();
				int pos 		=	sip.find('/');
				if(pos!=string::npos)
				{
					sip 	=	sip.substr(0,pos);
				}
	
				IP ip(sip);
	
				n 			=	devInfo->getNode("/result/row/puerto");
				int port =	atoi(n->getCdata().c_str());
				cam 		= 	Address(ip, port);

				n 			=	devInfo->getNode("/result/row/codigo");
				model 	=	n->getCdata();
	
				delete devInfo;
				devInfo = NULL;
	
				cout << "DahuaCP:: load new Camera:" << id.id << ":" 
							"" << cam.toString() << " " << model << endl;
	
				s = 	"SELECT m.raizinfo "
						"FROM modelo m, fabricante f "
						"WHERE m.fabricante=f.id AND f.nombre='Dahua' "
						"AND m.codigo ='" + model + "'";
				XML *paramNode =	db.call(s, 0, rpc);
				RPC::keepAlive(a, type);
				cout<<"SetMCP db Call:"<<paramNode->toString()<<endl;
				n=paramNode->getNode("/result/row/raizinfo");

				if(n->getCdata() == string(""))
				{
					loadModelConfig(model, "/", "/", modelConf);
					checkModelMetadata(model);
				}
				cout << "DahuaCP:: loaded new Camera" << endl;
				db.freeRPC(rpc);
			}
			catch(SocketTimeoutException &e)
			{
				cout << "DahuaCP:: STE -- not loaded new Camera" << endl;
				db.freeRPC(rpc);
				throw(ControlPluginException(e.getCode(), e.getMsg()));
			}
			catch(Exception &e)
			{
				cout << "DahuaCP:: E -- not loaded new Camera " 
						"" << e.getClass() << ":"	""	<< e.getMsg() << endl;
				
				db.freeRPC(rpc);
				throw(ControlPluginException(	0, 
														"Invalid device id:" + 
														StrUtils::decToString(id.id)));
			}
			catch(...)
			{
				cout<<"DahuaCP:: (...) -- not loaded new Camera"<<endl;
				
				db.freeRPC(rpc);
				throw(ControlPluginException(	0, 
														"Invalid device id:" + 
														StrUtils::decToString(id.id)));
			}
			cams[id.id] = new CamInfo(id, cam, model, default_buffer_size, this);
			cams[id.id]->startThreads();
		}

		try
		{	
			XML *conf = getConfigFromFile( file );

			string configFile=conf->getNodeData("[0]/DeviceConfigFile");

			delete conf;
			conf 	= NULL;

			conf = getConfigFromFile( configFile );
			 
			cout<<"DahuaCP:: load new Camera::loadDBConfig:"<<id.id<<endl;

			int idth 	= ci.start(new CamInit::execInfo(id.id, 
																		conf, 
																		cams[id.id]->modelStr, 
																		this, 
																		true));
			ci.detach(idth);
		}
		catch(SocketException &e)
		{
			//no s'ha trobat la camera?
			cams.erase(id.id);

			throw(CPInvalidParamException(0,
												"No s'ha pogut comunicar amb la camera"));

		}
		catch(...){
			throw(ControlPluginException(	0, 
													"Invalid device id:" + 
													StrUtils::decToString(id.id)));
		}

	}	

	cout<<" StartSession for:"<<a->toString()<<" dev:"<<id.id<<endl;

	SessionDispatcher ses(cam, id);
	sessions[a->toString()]= ses;

#ifdef DEBUG
	cout << "<----- DahuaCP::startSession" << endl;
#endif	
}

void DahuaControlPlugin::endSession(CPDeviceID id, Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::endSession" << endl;
#endif	

	map<string,SessionDispatcher>::iterator i=sessions.find(a->toString());

	if(i==sessions.end())
	{
#ifdef DEBUG
		cout << "<----- DahuaCP::endSession" << endl;
#endif	

		return;
	}

	sessions.erase(i);
	struct timeval now;
	gettimeofday(&now,NULL);
	i 	= sessions.begin(); 

	while(i!=sessions.end())
	{
		struct timeval diffTime = now - i->second.getTimeOfCreationOfThisSession();
		if ( 	diffTime.tv_sec > sessionTimeout )
			sessions.erase(i++);
		else
			i++;
	}
#ifdef DEBUG
	cout << "<----- DahuaCP::endSession" << endl;
#endif	
}


CPPTZ DahuaControlPlugin::getPTZ(Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getPTZ" << endl;
#endif	

	SessionDispatcher s 	= getSession(*a);
	Address cam = s.getIpAddress();

#ifdef DEBUG
	cout << "<----- DahuaCP::getPTZ" << endl;
#endif	

	return getPTZ(s.getIdNumber());
}

CPPTZ DahuaControlPlugin::getPTZ(int id)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getPTZ" << endl;
#endif	

#ifdef DEBUG
	cout << "<----- DahuaCP::getPTZ" << endl;
#endif	
	return CPPTZ(0,0,0);
}

void DahuaControlPlugin::setPTZ(CPPTZ ptz, Address *a)
{	
#ifdef DEBUG
	cout << "-----> DahuaCP::setPTZ" << endl;
#endif	

#ifdef DEBUG
	cout << "<----- DahuaCP::setPTZ" << endl;
#endif	
}

void DahuaControlPlugin::run_setPTZ(	CPPTZ ptz, 
												CPDeviceID id, 
												bool fromTour, 
												bool slowMove)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::run_setPTZ" << endl;
#endif	

#ifdef DEBUG
	cout << "<----- DahuaCP::run_setPTZ" << endl;
#endif	
}

void DahuaControlPlugin::setInput(CPInput in, Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::setInput" << endl;
#endif	

	SessionDispatcher s = getSession(*a); 

#ifdef DEBUG
	cout << "<----- DahuaCP::setInput" << endl;
#endif	
}

CPInput DahuaControlPlugin::getInput(CPInput in, Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getInput1" << endl;
#endif	
	SessionDispatcher s=getSession(*a);

#ifdef DEBUG
	cout << "<----- DahuaCP::getInput1" << endl;
#endif	
	return getInput(in, s.getIdNumber());
}

CPInput DahuaControlPlugin::getInput(CPInput in, int devId)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getInput2" << endl;
#endif	

	return CPInput(0, 0);
}

void DahuaControlPlugin::setOutput(CPOutput out, Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::setOutput" << endl;
#endif	

#ifdef DEBUG
	cout << "<----- DahuaCP::setOutput" << endl;
#endif	
}

CPOutput DahuaControlPlugin::getOutput(CPOutput out, Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getOutput" << endl;
#endif	

	return CPOutput(-1,-1);
}

void DahuaControlPlugin::setCommandBufferSize(CPCommandBufferSize size, 
															Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::setCommandBufferSize1" << endl;
#endif	

	SessionDispatcher s = getSession(*a);
	cams[s.getIdNumber()]->setSize(size.nCommands);

#ifdef DEBUG
	cout << "<----- DahuaCP::setCommandBufferSize1" << endl;
#endif	
}

CPCommandBufferSize DahuaControlPlugin::getCommandBufferSize(Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getCommandBufferSize2" << endl;
#endif	

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- DahuaCP::getCommandBufferSize2" << endl;
#endif	

	return CPCommandBufferSize(cams[s.getIdNumber()]->getSize());
}

float DahuaControlPlugin::getCommandBufferPercentInUse(Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getCommandBufferPercentInUse" << endl;
#endif	

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- DahuaCP::getCommandBufferPercentInUse" << endl;
#endif	

	return cams[s.getIdNumber()]->getPercentInUse();
}

int DahuaControlPlugin::getCommandBufferCommandsInUse(Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getCommandBufferCommandsInUse" << endl;
#endif	

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- DahuaCP::getCommandBufferCommandsInUse" << endl;
#endif	

	return cams[s.getIdNumber()]->getUse();
}

CPMetadata DahuaControlPlugin::getMetadataValue(CPMetadata data, Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getMetadataValue1" << endl;
#endif	

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- DahuaCP::getMetadataValue1" << endl;
#endif	

	return CPMetadata(data.name, getMetadataValue(s.getIdNumber(), data.name));
}

string DahuaControlPlugin::getMetadataValue(int id, string data)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getMetadataValue2" << endl;
#endif	

#ifdef DEBUG
	cout << "<----- DahuaCP::getMetadataValue2" << endl;
#endif	
	return ("true");
}

void DahuaControlPlugin::setMetadataValue(CPMetadata data, Address *a)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::setMetadataValue1" << endl;
#endif	

#ifdef DEBUG
	cout << "<----- DahuaCP::setMetadataValue1" << endl;
#endif	
}

void DahuaControlPlugin::setMetadataValue(int id, string name, string value)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::setMetadataValue2" << endl;
#endif	

#ifdef DEBUG
	cout << "<----- DahuaCP::setMetadataValue2" << endl;
#endif	
}

// ------------------------------------------------------------------------ //
// Función que recoge los parámetros del dispositivo  							 //
// ------------------------------------------------------------------------ //
string DahuaControlPlugin::getParamValue(	string http_response, 
															string params_2_find)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getParamValue" << endl;
#endif	

	string response("");			// La respuesta que vamos a devolver
	size_t comma_pos 	= 0;		// Ej: Video.Encoding: MJPEG, MPEG4, H.264
	bool match_found  = false; // Indica si hemos encontrado una coincidencia

	// Versión 1.0 -- Hardcoding de parámetros
	// TODO20150427	Recoger los parámetros dinámicamente a través de consultas
	// con la cámara
	if 		(params_2_find == "Video.Format")
	{
		response = string("PAL");
	}
	else if 	(params_2_find == "Video.Encoding")
	{
		response = string("MPEG4");
	}
	else if 	(params_2_find == "Video.TargetBitrate")
	{
		response = string("1536");
	}
	else if 	(params_2_find == "Video.Resolution")
	{
		response = string("1280x720");
	}
	else
	{
		cout << "**** Not supported parameter: " << params_2_find << endl;
		cin.get();
	}
#ifdef DEBUG
	cout << "<----- DahuaCP::getParamValue" << endl;
#endif	

	return response;
}


string DahuaControlPlugin::getConfigParamCamera(	string url, 
																string param, 
																string pModel, 
																int devId)
{
#ifdef DEBUG
	cout << "-----> DahuaCP::getConfigParamCamera: " << url << ", " << param << ""
			", " << pModel << ", " << devId << endl;
#endif	

	HttpStream *stream = NULL;
	string body("");

	try
	{
		// Conectamos con la cámara
		HttpClient cli(url);

		HttpStream *stream = cli.sendRequest(string("GET"));

      // Configuramos los parámetros del servidor
      string digest_user 		= "admin";
      string digest_password 	= "admin";
      string digest_url 		= "/cgi-bin/about.cgi?msubmenu=capability&"
      									"action=view";
		map<string, string> http_response = stream->getRequestDataSamsung();
   
     // Conexión con el códec Dahua
      /// 1.1. Obtenemos la respuesta {nonce, qop, realm}
      string http_petition = this->getDigestAuthStrForDevice(http_response, 
      																		digest_url, 
      																		digest_user, 
      																		digest_password);
      cli.setHeader("Authorization", http_petition);
      stream = cli.sendRequest();

 		// Hay que realizar dos consultas:
 		// Primera consulta recibimos la cabecera del mensaje
      stream->getNextInformationChunkSamsung();
		// Segunda consulta recibimos el mensaje en sí
      body 	= stream->getNextInformationChunkSamsung();

      delete stream;
		stream = NULL;

#ifdef DEBUG			
		cout << "<----- DahuaCP::getConfigParamCamera" << endl;
#endif
		return getParamValue(body, param);
		
	}
	catch(SocketTimeoutException &e)
	{
		if(stream!=NULL)
		{
			delete stream;
			stream = NULL;
		}
#ifdef DEBUG
		cout << "<----- DahuaCP::getConfigParamCamera" << endl;
#endif
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}
	catch(...)
	{
#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParamCamera" << endl;
#endif	
		cout << "Throw Exception: Heap Error 121" << endl;

		if(stream!=NULL)
		{
			delete stream;
			stream = NULL;
		}
	}
}

void DahuaControlPlugin::setConfigParamCamera(string url, int devId)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::setConfigParamCamera" << endl;
#endif	

#ifdef DEBUG	
	cout << "<----- DahuaCP::setConfigParamCamera" << endl;
#endif	
}

void DahuaControlPlugin::sendMoveCommand(string url, int devId, bool slowMove)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::sendMoveCommand" << endl;
#endif	

#ifdef DEBUG	
	cout << "<----- DahuaCP::sendMoveCommand" << endl;
#endif
}

CPConfigParam DahuaControlPlugin::getConfigParam(CPConfigParam p, Address *a)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::getConfigParam1" << endl;
#endif

	SessionDispatcher ses=getSession(*a);

#ifdef DEBUG	
	cout << "<----- DahuaCP::getConfigParam1" << endl;
#endif

	return getConfigParam(ses.getIdNumber(), p, a);
}

CPConfigParam DahuaControlPlugin::getConfigParam(int id, 
																CPConfigParam p, 
																Address *a)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::getConfigParam2" << endl;
#endif

	RPC *rpc = db.getRPC(a,5);
	if(rpc == NULL)
	{

#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParam2" << endl;
#endif

		return CPConfigParam("","");
	}
	try
	{
		cout << " getConfigParam:" << cams[id]->modelStr << " : " << p.path << 
					" rpc:" << rpc->getLocalAddr().toString() << endl;
		xmlNode *modNode = modelConf->getNode("/[0]/Models/All" + p.path);

		if(modNode != NULL && modNode->getKidList().size() == 0)
		{
			string cp = db.getModelConfigParam(	"Dahua", 
															cams[id]->modelStr, 
															p.path, 
															rpc);
			cout << " getConfigParam: de model:" << p.path << ":" << cp << 
						" rpc:" << rpc->getLocalAddr().toString() << endl;
			db.freeRPC(rpc);

#ifdef DEBUG	
			cout << "<----- DahuaCP::getConfigParam2" << endl;
#endif
			return CPConfigParam(p.path, cp);
		}

		cout << " getConfigParam:" << cams[id]->modelStr << " id:" << id << 
					": " << p.path << " rpc:" << rpc->getLocalAddr().toString() << 
					"" << endl;
		string res = db.getDeviceConfigParam(id, p.path, rpc);
		cout << " getConfigParam , freeRPC:" << rpc->getLocalAddr().toString() << 
					"" << endl;
		db.freeRPC(rpc);

#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParam2" << endl;
#endif
		return CPConfigParam(p.path, res);
	}
	catch(Exception &e)
	{
		cout << " getConfigParam:" << cams[id]->modelStr << " id:" << id << 
					": " << p.path << " Exception:" << e.getClass() << "::" <<  ""
					"" << e.getMsg() << " rpc:" << rpc->getLocalAddr().toString() <<
					"" << endl;
		db.freeRPC(rpc);

#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParam2" << endl;
#endif
		throw;
	}
	catch(...)
	{
		db.freeRPC(rpc);
#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParam2" << endl;
#endif
		throw;
	}
}

void DahuaControlPlugin::setConfigParam(CPConfigParam p, Address *a)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::setConfigParam" << endl;
#endif


	SessionDispatcher s = getSession(*a);

	cout<<" setConfigParam::"<<s.getIpAddress().toString()<<endl;

	//Comprobar validesa dels params aquĂ­

	cams[s.getIdNumber()]->queue(CPServ::setConfigParam, (void *) new CPConfigParam(p));
	cout<<" setConfigParam:: return "<<s.getIpAddress().toString()<<endl;


#ifdef DEBUG	
	cout << "<----- DahuaCP::setConfigParam" << endl;
#endif
}

 void DahuaControlPlugin::run_setConfigParam(	CPConfigParam p, 
 															CPDeviceID id, 
 															bool fromTour)
 {
#ifdef DEBUG	
 	cout << "-----> DahuaCP::run_setConfigParam" << endl;
#endif

 	Address cam = cams[id.id]->cam;

	//Update camara
	/*
	 * - Pan Tilt Limit
	 * - Preset position
	 * - Zoom Type (Opt/Opt+Digt)
	 * - Gain/Brightness/Expos.
	 */

 	string s 		=	"/[0]/Devices/" + cams[id.id]->modelStr;
 	list<string> l =	StrUtils::pathToList(p.path);		

 	for(	list<string>::iterator it = l.begin(), end = l.end(); 
 			it != end; ++it)
 	{
 		s += string("/") + (*it);
 	
		xmlNode *aux=conf->getNode(s);
		if(	(	(aux != NULL) 								&& 
					(aux->getParam("nodename")  == "inc")	)	)
		{
			it++;
			if(it!=l.end() && (*it) == "Enable")
			{
				it++;
				if(it == l.end())
				{
					p.path = p.path.substr(0,p.path.rfind("/Enable"));
					break;
				}
				it--;
				continue;
			}
			db.setDeviceConfigParam(id.id, p.path, p.value);

#ifdef DEBUG	
			cout << "<----- SonyCP::run_setConfigParam" << endl;
#endif
			return;
		}
	}

	cout << " setCP Peticio:" << p.path << ":" << p.value << " conf:" 
				"" << (void*)conf << endl;

	list<string> lp 	=	StrUtils::pathToList(p.path);
	string path 		=	string("");

	for(	list<string>::iterator it=lp.begin(), end = lp.end(); 
			it != end; ++it)
	{
		path += "/" + (*it);
	}

	int pos = path.rfind("/Delete");
	if(pos != string::npos)
	{
		path = path.substr(0,pos);
	}

	if(conf->getNode("/[0]/Devices/" + cams[id.id]->modelStr+path) != NULL)
	{
		if(p.path.rfind("/Delete") != string::npos)
		{
			cout << " setConfigParam Del:" << path << endl;
			db.deleteDeviceConfig(id.id, path);
			loadDevConfig(	id.id, 
								cams[id.id]->modelStr, 
								"/[0]/Devices/" + cams[id.id]->modelStr+path, 
								path, 
								conf);
			//TODO2006: arreglar filtres ( probar FPS[0], FPS[1], etc...)
		}
		else
		{
			db.setDeviceConfigParam(id.id, p.path, p.value);
			loadDevConfig(	id.id, 
								cams[id.id]->modelStr, 
								"/[0]/Devices/" + cams[id.id]->modelStr + path, 
								p.path, 
								conf);
			cout << endl;
		}
	}
	else
	{	
		// coses que no depenen del SCPDeviceConfig.xml
		if(p.path.rfind("/Delete")!=string::npos)
		{
			cout<<" setConfigParam Del:"<<path<<endl;
			db.deleteDeviceConfig(id.id, path);
		}
		else
		{
			list<string>::iterator it=l.begin();
			db.setDeviceConfigParam(id.id, p.path, p.value);
		}
	}
#ifdef DEBUG	
	cout << "<----- DahuaCP::run_setConfigParam" << endl;
#endif
 }

CPConfigParamSeq DahuaControlPlugin::getConfigParamRecursive(CPConfigParam p, 
																				Address *a)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::getConfigParamRecursive1" << endl;
#endif

	SessionDispatcher ses 	= getSession(*a);
	int devId 	= ses.getIdNumber();

#ifdef DEBUG	
	cout << "<----- DahuaCP::getConfigParamRecursive1" << endl;
#endif

	return this->getConfigParamRecursive(p,devId, a); 
}

CPConfigParamSeq DahuaControlPlugin::getConfigParamRecursive(CPConfigParam p, 
																				int devId, 
																				Address *a)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::getConfigParamRecursive2" << endl;
#endif

	RPC *rpc = db.getRPC(a,5);
	if(rpc == NULL)
	{
		return CPConfigParamSeq();
	}
	try
	{
		CPConfigParamSeq seq;
		cout << "getConfigParamRecursive(" << p.path << ") address:" 
					"" << (void*)a << " rpc:" << rpc->getLocalAddr().toString() << ""
					"" << endl;

		if(modelConf->getNode(string("/[0]/Models/All")+p.path) != NULL )
		{
			int id = db.getModelConfigNode(	"Dahua", 
														cams[devId]->modelStr,  
														p.path, 
														rpc);

			cout << " -- /getConfigNodeModel:" << id << " :" << p.path << " rpc:" 
						"" << rpc->getLocalAddr().toString() << endl;
			if(id!=-1)
			{
				string pref = p.path;
				int pos 		= pref.rfind("/");

				if(pos == pref.size()-1)
				{
					pref 	= pref.substr(0,pos);
					pos 	= pref.rfind("/");
				}

				if(pos!=string::npos)
				{
					pref 	=	pref.substr(0,pos); 	//pq ja l'afegeix el db.gcpr
				}

				string s 	= "SELECT * FROM getconfigparamrecursive(" + 
									StrUtils::decToString(id) + ",'" + pref + "')"; 
				XML *res 	= db.call(s, 0, rpc);
				xmlNode *n 	= res->getNode("/result");
				int nrows 	= 0;
				if(n!=NULL)
				{
					nrows=n->getKidList().size();
				}
				if(	(nrows == 0) 			&& 
						(n->getCdata() != "") )
				{
					cout << " DahuaCP::GetConfigParamRecursive() error resposta:" 
								"" << res->toString() << endl;
					throw(ControlPluginException(0, 
													"Database error:" + (res->toString())));
				}

				for (int i=0; i<nrows; ++i)
				{
					seq.push_back(CPConfigParam(	res->getNodeData(
						"/result/[" +StrUtils::decToString(i)+ "]/name"
																				 ), 
															res->getNodeData(
						"/result/["+StrUtils::decToString(i)+"]/value"
																				 )
																					));
				}
				delete res;
				res = NULL;
			}
		}

		cout << " getConfigParamRecursive -- getDevConfigNode:" << devId << ""
					" path:" << p.path << " rpc:" << ""
					"" << rpc->getLocalAddr().toString() << endl;
		int id = db.getDeviceConfigNode(devId, p.path, rpc);
		cout << " getConfigParamRecursive -- /getDevConfigNode:" << id << ""
					" path:" << p.path << " rpc:" << ""
					"" << rpc->getLocalAddr().toString() << endl;
		if(id != -1)
		{
			string pref =	p.path;
			int pos 		=	pref.rfind("/");
			if(pos == pref.size()-1)
			{
				pref 	= pref.substr(0,pos);
				pos 	= pref.rfind("/");
			}

			if(pos != string::npos)
			{
				pref 	=	pref.substr(0,pos);
			}

			string s 	= "SELECT * FROM getconfigparamrecursive(" + 
								StrUtils::decToString(id) + ",'" + pref + "')";
			XML *res 	= db.call(s, 0, rpc);
			xmlNode *n 	= res->getNode("/result");

			int nrows 	= 0;
			if(n!=NULL)
			{
				nrows 	= n->getKidList().size();
			}
			if(nrows==0 && n->getCdata()!=string(""))
			{
				cout << " DahuaCP::GetConfigParamRecursive() error resposta:" 
							"" << res->toString() << endl;
				throw(ControlPluginException(0, 
													"Database error:" + res->toString()));
			}

			for(int i=0; i<nrows; ++i)
			{
				seq.push_back(CPConfigParam(res->getNode(
					"/result/[" + StrUtils::decToString(i) + "]/name"
																	 )->getCdata(), 
													 res->getNode(
					"/result/[" + StrUtils::decToString(i) + "]/value"
																	 )->getCdata()));
			}
			delete res;
			res = NULL;
		}

		cout << "/getConfigParamRecursive free rpc:" 
					"" << rpc->getLocalAddr().toString() << endl;
		db.freeRPC(rpc);

#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParamRecursive2" << endl;
#endif
		return seq;
	}
	catch(SocketTimeoutException &e)
	{
		cout << " getConfigParamRecursive: " << devId << ":" << p.path << ""
					" Exception:" << e.getClass() << "::" << e.getMsg() << " rpc:"
					"" << rpc->getLocalAddr().toString() << endl;
		db.freeRPC(rpc);

#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParamRecursive2" << endl;
#endif
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}
	catch(Exception &e)
	{
		cout << " getConfigParamRecursive: " << devId << ":" << p.path << ""
					" Exception:" << e.getClass() << "::" << e.getMsg() << " rpc:"
					"" << rpc->getLocalAddr().toString() << endl;
		db.freeRPC(rpc);

#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParamRecursive2" << endl;
#endif
		throw;
	}
	catch(...)
	{
		db.freeRPC(rpc);

#ifdef DEBUG	
		cout << "<----- DahuaCP::getConfigParamRecursive2" << endl;
#endif
		throw;
	}

#ifdef DEBUG	
	cout << "<----- DahuaCP::getConfigParamRecursive2" << endl;
#endif
}

CPConfigParamSeq DahuaControlPlugin::getAllConfigParams(Address *a)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::getAllConfigParams" << endl;
#endif

	try
	{
		SessionDispatcher ses 	= getSession(*a);
		CPConfigParamSeq seq;
		string s 	= "SELECT m.raizinfo "
							"FROM modelo m, fabricante f "
							"WHERE m.fabricante = f.id AND f.nombre='Dahua' "
							"AND m.codigo ='" + cams[ses.getIdNumber()]->modelStr + "'";
		XML *res 	= db.call(s, 0);
		xmlNode *n 	= res->getNode("/result/row/raizinfo");

		if(n!=NULL)
		{
			string nodeid= (n->getCdata());

			s  			= "SELECT * FROM getconfigparamrecursive(" 
								"" + nodeid + ",'')"; 
			res 			= db.call(s, 0);
			n 				= res->getNode("/result");
			int nrows 	= 0;
			if(n!=NULL)
			{
				nrows = n->getKidList().size();
			}
			if ( 	(nrows == 0) 				&& 
					(n->getCdata() != "") )
			{
				cout << " DahuaCP::GetAllConfigParams() error resposta:" << 
							"" << res->toString() << endl;
			}

			for(int i=0; i<nrows; ++i)
			{
				seq.push_back(CPConfigParam(res->getNodeData(
									"/result/[" + StrUtils::decToString(i) + "]/name"
																		  ), 
													 res->getNodeData(
									"/result/[" + StrUtils::decToString(i) + "]/value"
																		  )));
			}
		}

		s 				= "SELECT * FROM getallconfigparamsfordevice(" + 
							StrUtils::decToString(ses.getIdNumber()) + ")"; 
		res 			= db.call(s, 0);
		n 				= res->getNode("/result");
		int nrows 	= 0;
		if(n!=NULL)
		{
			nrows=n->getKidList().size();
		}
		if(nrows==0)
		{
			cout << " DahuaCP::GetAllConfigParams() error resposta:" << "" 
						"" << res->toString() << endl;
		}

		for(int i=0; i<nrows; ++i)
		{
			seq.push_back(CPConfigParam(res->getNodeData(
										"/result/[" + StrUtils::decToString(i) + "]/name"
																	  ), 
												 res->getNodeData(
				 						"/result/[" + StrUtils::decToString(i) + "]/value"
				 													   )));
		}

#ifdef DEBUG	
		cout << "<----- DahuaCP::getAllConfigParams" << endl;
#endif

		return seq;
	}
	catch(SocketTimeoutException &e)
	{
#ifdef DEBUG	
		cout << "<----- DahuaCP::getAllConfigParams" << endl;
#endif
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}
}

void DahuaControlPlugin::move(CPMovement move, Address *a)
{	
#ifdef DEBUG	
	cout << "-----> DahuaCP::move" << endl;
#endif

	SessionDispatcher s = getSession(*a);

	cams[s.getIdNumber()]->queue(CPServ::move, (void *) new CPMovement(move));

#ifdef DEBUG	
	cout << "<----- DahuaCP::move" << endl;
#endif
}

void DahuaControlPlugin::run_move(CPMovement move, CPDeviceID id)
{
#ifdef DEBUG	
	cout << "-----> DahuaCP::run_move" << endl;
#endif

#ifdef DEBUG	
	cout << "<----- DahuaCP::run_move" << endl;
#endif
}
