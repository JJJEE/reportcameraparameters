/*********************************************************************
 * RTSPControlPlugin.h                                            	*
 * -----------------------------------------------------------------	*
 *	Marina Eye-Cam Technologies, S.L. All rights reserved.				*
 *********************************************************************/
#ifndef RTSPCONTROLPLUGIN_H
#define RTSPCONTROLPLUGIN_H
 
#pragma once
#include <Utils/DBGateway.h>
#include <Plugins/PluginControl.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <Utils/SessionDispatcher.h>
#include <Utils/ModelInfo.h>
#include <math.h>

class RTSPControlPlugin: public PluginControl
{
private:
 
	static const int SD6AL230F_HNI	= 	0; 
	static const int SPE_100			= 	1; 
	static const int SNC_CS50			= 	2; 
	static const int SNC_DH180			= 	3;

	static const int NUM_THREADS 		= 	5;	// TODO 20090310: Guardem fins a 5 thrIds, a lo guarro
	
	map<string, SessionDispatcher> sessions;
	map <string, ModelInfo*> models;

	static const int sessionTimeout	=	600;

	class CamInfo: public Thread
	{
	private:

		class Command
		{
			public:
				int 	id;
				void 	*data;
				Command (int id, void* data) : id(id), data(data){}
		};

		struct WaitInfo
		{
			int i;
			Condition c;
			WaitInfo(){ i=0;}
		};
		
		list<Command> 		buffer;
		Mutex 				bLock;
		int 					bufferSize;
		RTSPControlPlugin *cp;
		AlarmModuleAccess am;

		void init(	CPDeviceID id, 
						RTSPControlPlugin *cp);

	public:

		CPDeviceID 	id;
		Address 		cam;
		int 			streamId;

		int 			model;
		string 		modelStr;

		map<string, string> metadata;
		Condition 	full; 	//fan servir el bLock
		struct timeval alarmTime;
		

		bool 	initing;
		bool 	inited;
		bool 	error_logged;
		bool 	firstSession;
		
		bool 	threadRunning[NUM_THREADS];
		
		Address 		bar;
		Condition 	queued; 	//fan servir el bLock
		Address 		bar2;

		CamInfo 			(CPDeviceID id, 
							 Address cam, 
							 int streamId, 
							 string modelStr, 
							 int size, 
							 RTSPControlPlugin *cp);

		CamInfo 			(CPDeviceID id, 
							 Address cam, 
							 string modelStr, 
							 int size, 
							 RTSPControlPlugin *cp);

		void queue 		(int id, 
							 void *data, 
							 Address *keepAliveAddr = NULL);

		//esperém que la cua actual de comandes s'hagi acabat d'executar
		void 	wait 		(Address *keepAliveAddr = NULL);
		void 	startThreads();
		void* execute 	(int i, 
							 void *params);

		int 	getSize();
		void 	setSize(	int size );
		int 	getUse();
		float getPercentInUse();
	};
	
	map<int, CamInfo*> cams;


protected:

	class CamInit: public Thread
	{
	public:
		struct execInfo
		{
			int id;
			XML *conf;
			string model;
			RTSPControlPlugin *scp;
			bool reload;

			execInfo(int id, 
						XML *conf, 
						string model, 
						RTSPControlPlugin *scp, 
						bool reload
						) : id(id), 
							 conf(conf), 
							 model(model), 
							 scp(scp), 
							 reload(reload){};
		};

		CamInit(){};

		void* execute(int id, void* parm);
	};

	class CamInitStarterThread: public Thread
	{
	public:

		XML *conf;
		RTSPControlPlugin *scp;
		list<int> devs;
		bool reload, sequential;
		void* execute(int id, void* parm);
	};

	static const int default_buffer_size	=	NUM_THREADS;
	static const int maxStarterThreads		=	NUM_THREADS;
	XML *conf, *modelConf;
	CamInit ci;
	CamInitStarterThread cist;
	DBGateway db;
	string file;

	string 	getBasicAuthStrForDevice 	(int devId);
	string 	getDigestAuthStrForDevice 	(map<string, string> httpResponse, 
													 string digest_url, 
													 string digest_user, 
													 string digest_password);

	SessionDispatcher 	getSession 				(Address a);
	
	void 		checkModelMetadata	(string nom);

	void 		loadModelsConfig 		(XML *conf, 
										 	 bool reload = false);

	void 		loadModelConfig 		(string model, 
											 string nodepath, 
											 string dbpath, 
											 XML *conf);

	void 		loadDevConfig 			(int id, 
										 	 string model, 
										 	 string nodepath, 
										 	 string dbpath, 
										 	 XML *conf);

	void 		loadDevConfig 			(int id, 
											 string model, 
										 	 string nodepath, 
										  	 string dbpath, 
										  	 XML *conf, 
										 	 DBGateway *db);
	//translate from Camera to DB
	string  	translateCam 			(string param, 
							  			 	 string value, 
							  			 	 int model); 
	//translate from DB to Camera 
	string  	translateDB 			(string param, 
						 				  	 string value, 
										  	 int model); 

	string 	addPath 					(string path, 
										 	 string relPath);

	void 		loadDBConfig 			(CPDeviceID, 
							 				 XML *defaultConf, 
											 string model, 
											 bool reload = true);
	
	string  	removeIndex 			(string path);

	// getParamValue -- retorna el valor d'un parametre en una string  
	// 	de l'estil param1=valor1&param2=valor2&param3=.....
	string 	getParamValue 			(string llista, 
										 	 string param); 

	string 	getConfigParamCamera	(string url,
	 									 	 string param, 
	 									  	 string pModel, 
	 									 	 int devId = -1);

	void 	 	setConfigParamCamera (string url, 
										 	 int devId = -1);

	void 		sendMoveCommand 		(string url, 
										 	 int devId, 
										 	 bool slowMove);

	void 		loadSchedule 			(int devId,
	 										 int recId);

	void 		loadSchedules 			(int id);

	void* 	execute 					(void *parm);

public:

	CPPTZ 			getPTZ 			(int id);
	CPConfigParam 	getConfigParam (int id, 
											 CPConfigParam p, 
											 Address *a = NULL);

	RTSPControlPlugin 				(string file, 
											 bool sequentialReloads, 
											 bool reloadDevs, 
											 bool reloadModels, 
											 list<int> reloadDevList);
	~RTSPControlPlugin();


	void 				serve();

	virtual void 	startSession	(CPDeviceID id, 
											 Address *a);

	virtual void 	endSession 		(CPDeviceID id, 
											 Address *a);

	virtual CPPTZ 	getPTZ 			(Address *a);

	virtual void 	setPTZ 			(CPPTZ ptz, 
											 Address *a);

	void 				run_setPTZ 		(CPPTZ ptz, 
											 CPDeviceID id,
											 bool fromTour = false, 
											 bool slowMove = true);

	virtual void 		setInput 	(CPInput in, 
											 Address *a);

	virtual CPInput 	getInput 	(CPInput in, 
											 Address *a);

	virtual CPInput 	getInput 	(CPInput in, 
											 int devId);

	virtual void 		setOutput 	(CPOutput out, 
											 Address *a);
	virtual CPOutput 	getOutput 	(CPOutput out, 
											 Address *a);

	virtual void 		setCommandBufferSize (CPCommandBufferSize size, 
														 Address *a);

	virtual CPCommandBufferSize getCommandBufferSize(Address *a);

	virtual float 		getCommandBufferPercentInUse 	(Address *a);

	virtual int 		getCommandBufferCommandsInUse (Address *a);

	virtual CPMetadata getMetadataValue 	(CPMetadata data, 
														 Address *a);

	virtual string 	getMetadataValue 		(int id, 
														 string data);

	virtual void 		setMetadataValue 		(CPMetadata data, 
														 Address *a);

	virtual void 		setMetadataValue 		(int id, 
														 string name, 
														 string data);

	virtual CPConfigParam getConfigParam 	(CPConfigParam p, 
														 Address *a);

	virtual void 		setConfigParam 		(CPConfigParam p, 
														 Address *a);

	void 					run_setConfigParam 	(CPConfigParam p, 
														 CPDeviceID id, 
														 bool fromTour = false);

	XML* getConfigFromFile( string file );
	
	virtual CPConfigParamSeq getConfigParamRecursive (CPConfigParam p, 
																	  Address *a);

	virtual CPConfigParamSeq getConfigParamRecursive (CPConfigParam p, 
																	  int devId, 
																	  Address *a);

	virtual CPConfigParamSeq getAllConfigParams 			(Address *a);

	virtual void 		move 						(CPMovement move, 
														 Address *a);

	void 					run_move 				(CPMovement move, 
														 CPDeviceID id);

};

#endif