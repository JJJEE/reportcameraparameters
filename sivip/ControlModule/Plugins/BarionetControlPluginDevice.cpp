#include <BarionetControlPluginDevice.h>
#include <Utils/File.h>
#include <Utils/StrUtils.h>
#include <XML/xmlParser.h>
#include <Exceptions/NotEnoughMemoryException.h>
#include <Exceptions/InvalidParameterException.h>
#include <cstdlib>

BarionetControlPluginDevice::BarionetControlPluginDevice() : ControlPluginDevice()
{
	lastUpdate = new Timer(1);

	if ( lastUpdate == NULL )
		throw NotEnoughMemoryException("Not enough memory to allocate BarionetControlPluginDevice data");

	lastUpdate->start();	
}

BarionetControlPluginDevice::BarionetControlPluginDevice(	CPDeviceID id,
																				Address addr, 
																				string modelConfigFile ) :	ControlPluginDevice( id, 
																																				addr, 
																																				modelConfigFile )
{
	this->init( this->id, 
					this->address, 
					modelConfigFile );
	lastUpdate = new Timer(1);
	if ( lastUpdate == NULL )
		throw NotEnoughMemoryException("Not enough memory to allocate BarionetControlPluginDevice data");

	lastUpdate->start();	
}

void BarionetControlPluginDevice::init(	CPDeviceID id, 
														Address addr, 
														string modelConfigFile )
{
	if (this->inited)	return;

	this->id 		= id;
	this->address 	= addr;
	
	File *f = new File( modelConfigFile );
	
	fileOffset fSize 	= f->size();
	byte *data 			= new byte[fSize];
	
	if ( data == NULL )
	{
		delete f;
		f = NULL;
		throw NotEnoughMemoryException("Not enough memory to read model config file");
	}
	
	f->read( data, 
				fSize );
	delete f;
	f = NULL;

	string xmlStr( (char*)data, 
						fSize );
	delete[] data;
	data = NULL;

	this->modelConfig = xmlParser::parse( xmlStr );
	
	this->nInputs 	= (int)strtol( this->modelConfig->getNode("/[0]/Models/Barionet/IOMapping/Inputs")->getCdata().c_str(), 
											NULL, 
											10 );
	int nOut 		= (int)strtol(	this->modelConfig->getNode("/[0]/Models/Barionet/IOMapping/Outputs")->getCdata().c_str(), 
											NULL, 
											10 );
	int nRel 		= (int)strtol(	this->modelConfig->getNode("/[0]/Models/Barionet/IOMapping/Relays")->getCdata().c_str(), 
											NULL, 
											10 );
	
	this->nOutputs = nOut + nRel;
	this->firstRelay = nOut;
	
	ControlPluginDevice::IO initIO;
	
	this->inputs.clear();
	this->inputs.resize( this->nInputs, 
								initIO );

	this->outputs.clear();
	this->outputs.resize( 	this->nOutputs, 
									initIO );
	
	this->inited = true;
}

int BarionetControlPluginDevice::getInput( 	int input, 
															int valueType )
{
	if (	( input <= 0 ) 				|| 
			( input>this->nInputs )	)
	{
		throw InvalidParameterException("Invalid input number");
	}
	
	this->updateState();
	
	return this->inputs[input-1].getValue( valueType );
}

void BarionetControlPluginDevice::setInput(	int input, 
															int valueType, 
															int value )
{
	if (	( input <= 0 ) 				|| 
			( input>this->nInputs ) )
	{
		throw InvalidParameterException("Invalid input number");
	}
	
	this->inputs[input-1].setValue(	valueType, 
												value );
}

int BarionetControlPluginDevice::getOutput(int output, int valueType)
{
	if (	( output <= 0 ) 				|| 
			( output>this->nOutputs )	)
	{
		throw InvalidParameterException("Invalid output number");
	}
	
	this->updateState();
		
	return this->outputs[output-1].getValue( valueType );
}

void BarionetControlPluginDevice::setOutput(int output, int valueType, int value)
{
	if (	( output <= 0 ) 				|| 
			( output>this->nOutputs )	)
	{
		throw InvalidParameterException("Invalid output number");
	}

	this->outputs[output-1].setValue( 	valueType, 
													value );
}

void BarionetControlPluginDevice::updateState()
{
	TimerInstant ti=lastUpdate->time();
	
	if (	ti.useconds() < ControlPluginDevice::cacheTime )	return;
	
	this->statusURL = "http://" + this->address.getIP().toString() + "/status.xml";
		
	try
	{
		this->http.setURL( this->statusURL );
		int retries = 0;

		while ( retries < 3 )
		{
			this->httpStream = this->http.sendRequest();
			
			// Cabecera
			this->httpStream->getNextInformationChunk();
	
			if(	( this->httpStream->getRequestStatus() >= 400 ) 	&& 
					( this->httpStream->getRequestStatus() < 500 )	)
			{
				// Hi ha hagut algun error de manca d'arxiu
				// Posem el "firmware" custom
				delete httpStream;
				httpStream = NULL;
	
				string cmd = "./tftpBario.pl " + this->address.getIP().toString();
				system( cmd.c_str() );
	
				++retries;
				continue;
			}
		
			// Cuerpo
			string xmlStatusStr = this->httpStream->getNextInformationChunk();

			XML *xmlStatus = NULL;
			try
			{
				xmlStatus = xmlParser::parse(xmlStatusStr);
			}
			catch ( Exception &e )
			{
				++retries;
				continue;
			}

			for (int o = 0; o < this->firstRelay; ++o)
			{
				string path = 	"/barionet/outputs/output[" +	StrUtils::decToString(o) + "]";
			
				this->outputs[o].setValueFromXML(	xmlStatus, 
																path, 
																ControlPluginDevice::IO::DigitalOutput );
			}

			for (int o=this->firstRelay; o<this->nOutputs; o++)
			{
				string path = "/barionet/relays/relay[" +	StrUtils::decToString(o) + "]";
			
				this->outputs[o].setValueFromXML(	xmlStatus, 
																path, 
																ControlPluginDevice::IO::RelayOutput );
			}

			static string baseInputPaths[3] = 
			{				
				"/barionet/inputs/digital/input",
				"/barionet/inputs/analog/percentual",
				"/barionet/inputs/temperature/celsius1000"
			};
						
			for ( int i = 0; i < this->nInputs; ++i )
			{
				string pathIndex = "[" + StrUtils::decToString(i) + "]";
			
				this->inputs[i].setValueFromXML(	xmlStatus,
															baseInputPaths[0] + pathIndex, 
															ControlPluginDevice::IO::DigitalInput );
				this->inputs[i].setValueFromXML( xmlStatus,
															baseInputPaths[1] + pathIndex, 
															ControlPluginDevice::IO::AnalogInput );
				this->inputs[i].setValueFromXML( xmlStatus,
															baseInputPaths[2] + pathIndex, 
															ControlPluginDevice::IO::TemperatureInput );
			}
			
			lastUpdate->start();	
		}
	
	}
	catch (...){}
}
