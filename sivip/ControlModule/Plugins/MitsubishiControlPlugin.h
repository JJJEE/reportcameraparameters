/*********************************************************************
 * MitsubishiControlPlugin.h                                         *
 * -----------------------------------------------------------------	*
 *	Marina Eye-Cam Technologies, S.L. All rights reserved.				*
 *********************************************************************/
#ifndef MITSUBISHICONTROLPLUGIN_H
#define MITSUBISHICONTROLPLUGIN_H

#ifndef WIN32
#include <sys/time.h>
#else
#include <Utils/WindowsDefs.h>
#endif
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginControl.h>
#include <Plugins/GestorControlTypes.h>
#include <Utils/SessionDispatcher.h>
#include <Sockets/SocketTCP.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <map>
#include <list>
#include <iostream>
using namespace std;

class MitsubishiControlPlugin: public PluginControl
{
	//static const int C150SD=0;
	enum Model {UNKNOWN=0L, NMC150SD, NMC130FD, NMC110};

	map<string, SessionDispatcher> sessions;
	static const int sessionTimeout=600;

	class DevInfo: public Thread
	{
		private:
			class Command
			{
				public:
					int id;
					void* data;
					Command(int id, void* data):id(id), data(data){}
			};

			struct WaitInfo
			{
				int i;
				Condition c;
				WaitInfo(){ i=0;}
			};

			list<Command> buffer;
			Mutex bLock;
			
			int bufferSize;
			Condition queued, full; //fan servir el bLock

			MitsubishiControlPlugin *cp;

		public:

			int nin, nout, nrel;
			int setx, sety;
			Mutex callMutex;
			Mutex camAccess;


			CPDeviceID id;
			int model;
			string modelStr;
			Address cam;
			map<string, string> metadata;
			bool initing, inited, error_logged;

			DevInfo(CPDeviceID id, Address cam, string model, int size, MitsubishiControlPlugin *cp);
			void queue(int id, void *data, Address *keepAliveAddr=NULL);
			void wait(Address *keepAliveAddr=NULL); //esperém que la cua actual de comandes s'hagi acabat d'executar
			void* execute(int i, void* params);
			int getSize();
			void setSize(int size);
			int getUse();
			float getPercentInUse();
	};
	
	map<int, DevInfo*> devs;

	static const int default_buffer_size=5;

	
protected:
	class CamInit: public Thread
	{
		public:
		struct execInfo
		{
			int id;
			XML *conf;
			string model;
			MitsubishiControlPlugin *bcp;
			bool reload;
			execInfo(int id, XML *conf, string model, MitsubishiControlPlugin *bcp, bool reload):id(id), conf(conf), model(model), bcp(bcp), reload(reload){};
		};
		CamInit()
		{
//				cout<<"  CamInit()"<<endl;
		};

		void* execute(int id, void* parm);
	};

	class CamInitStarterThread: public Thread
	{
		public:
		XML *conf;
		MitsubishiControlPlugin *scp;
		bool reload;
		void* execute(int id, void* parm);
	};

	XML *conf;
	CamInit ci;
	CamInitStarterThread cist;

	SessionDispatcher getSession(Address a);
	
	void loadDBConfig(CPDeviceID, XML* conf, string modeli, bool reload);
	void loadModelConfig(string model, string nodepath, string dbpath, XML *conf, bool reload);

	//string devResp(Address a, string prefix); // prefix de la resposta (pels status updates)
	
	static string getParamValue(string llista, string param); //retorna el valor d'un parametre en una string de l'estil param1=valor1&param2=valor2&param3=.....

	string configFile;
public:
	DBGateway db;
	MitsubishiControlPlugin(string file, bool reloadConfig);
	~MitsubishiControlPlugin();
	
	void serve();

	virtual void startSession(CPDeviceID id, Address *a);
	virtual void endSession(CPDeviceID id, Address *a);

	virtual CPPTZ getPTZ(Address *a);
	virtual void setPTZ(CPPTZ ptz, Address *a);
	void run_setPTZ(CPPTZ ptz, CPDeviceID id, Address cam);

	virtual void setInput(CPInput in, Address *a);
	void run_setInput(CPInput in, CPDeviceID id, Address cam);
	virtual CPInput getInput(CPInput in, Address *a);

	virtual void setOutput(CPOutput out, Address *a);
	void run_setOutput(CPOutput in, CPDeviceID id, Address cam);
	virtual CPOutput getOutput(CPOutput out, Address *a);
	
	XML* getConfigFromFile( string file );

	virtual void setCommandBufferSize(CPCommandBufferSize size, Address *a);
	virtual CPCommandBufferSize getCommandBufferSize(Address *a);

	virtual float getCommandBufferPercentInUse(Address *a);
	virtual int getCommandBufferCommandsInUse(Address *a);

	virtual CPMetadata getMetadataValue(CPMetadata data, Address *a);
	string getMetadataValue(int id, string data);
	virtual void setMetadataValue(CPMetadata data, Address *a);

	virtual CPConfigParam getConfigParam(CPConfigParam p, Address *a);
	virtual CPConfigParamSeq getConfigParamRecursive(CPConfigParam p, Address *a);
	virtual void setConfigParam(CPConfigParam p, Address *a);
	void run_setConfigParam(CPConfigParam p, CPDeviceID id, Address cam);
	virtual CPConfigParamSeq getAllConfigParams(Address *a);
	
	virtual void move(CPMovement move, Address *a);
};

#endif