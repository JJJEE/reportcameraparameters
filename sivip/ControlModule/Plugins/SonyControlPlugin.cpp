/*********************************************************************
 * SonyControlPlugin.cpp                                             *
 * ----------------------------------------------------------------- *
 *	Marina Eye-Cam Technologies, S.L. All rights reserved.       *
 *********************************************************************/
#include <Plugins/GestorControlExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include "SonyControlPlugin.h"
#include <Utils/debugStackTrace.h>
#include <iostream>
#include <string>
#ifdef WIN32
#include <Utils/WindowsDefs.h>
#endif
#ifndef WIN32
#include <unistd.h>
#endif
#include <cstring>
#include <cstdio>
#include <cstdlib>

using namespace std;

// Si DEBUG está definido, aparecerán los mensajes de DEBUG en pantalla.
// Principalmente dichos mensajes aparecen al comienzo y final de cada función.
#undef DEBUG
//#define DEBUG

/******************************************************************
 * listFind -- (???)																*
 *																						*
 * 		l -- (???)																*
 *			val -- (???)															*
 ******************************************************************/
bool listFind(list<int> l, int val)
{
#ifdef DEBUG
	cout << "-----> listFind: " << val << endl;
#endif

	list<int>::iterator lIt, lEnd;
	
	for ( lIt = l.begin(), lEnd = l.end(); lIt != lEnd; ++lIt)
	{
		cout<<"listFind dev "<<(*lIt)<<endl;
		if ((*lIt)==val)
			return true;
	}
	
	cout<<"listFind not found "<<val<<endl;

#ifdef DEBUG
	cout << "<----- listFind" << endl;
#endif
	return false;
}

// operadores declarados en otro fichero
extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

/******************************************************************
 * init -- Rutinas de inicialización 										*
 *																						*
 * 		id -- Identificador del dispositivo 							*
 *			cp -- (???)																*
 ******************************************************************/
void SonyControlPlugin::CamInfo::init(	CPDeviceID id, SonyControlPlugin *cp )
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:init: " << dec << id.id << endl;
#endif

	tour=new SonyControlPluginTouringThread(cp, &(cp->db), id.id);
	model = -1;
	if 		( modelStr == "SNC-RZ30" )					model = SNC_RZ30;
	else if 	( modelStr == "SNC-RX550" )				model = SNC_RX550;
	else if 	( modelStr == "SNC-RZ25" )					model = SNC_RZ25;
	else if( ( modelStr == "SNC-Z20" ) 		|| 
				( modelStr == "SNC-RZ20" )	)				model = SNC_Z20;
	else if 	( modelStr == "SNC-P1" ) 					model = SNC_P1;
	else if 	( modelStr == "SNC-RZ50" )					model = SNC_RZ50;
	else if 	( modelStr == "SNC-CS50" )					model = SNC_CS50;
	else if 	( modelStr == "SNC-CS3" )					model = SNC_CS3;
	else if(	( modelStr == "SNC-CS11" )		||
				( modelStr == "SNC-CS10" )	)
	{
		modelStr = "SNC-CS11";
																	model = SNC_CS11;
	}
	else if(	( modelStr == "SNC-DF50" )	||
				( modelStr == "SNC-DF80" )	)
	{
		modelStr = "SNC-DF50";
																	model = SNC_DF50;
	}
	else if(	( modelStr == "SNC-DF40" )	||
				( modelStr == "SNC-DF70" )	)				model = SNC_DF50;
	else if 	( modelStr == "SNC-CH140" ) 				model = SNC_CH140;
	else if 	( modelStr == "SNC-CH180" )				model = SNC_CH180;
	else if 	( modelStr == "SNT-EX104" )				model = SNT_EX104;
	else if 	( modelStr == "SNC-DH180" ) 				model = SNC_DH180;
	else if 	( modelStr == "SNT-V704" )					model = SNT_V704;
	else if 	( modelStr == "SNT-V704-L9EMB" )			model = SNT_V704_L9EMB;
	else if 	( modelStr == "SNC-RS86" )   				model = SNC_RS86;
	else if 	( modelStr == "SNC-EB630" )				model = SNC_EB630;
	else if 	( modelStr == "SNC-VB600" )				model = SNC_VB600;
	else if 	( modelStr == "SNC-VB600B" )				model = SNC_VB600B;
	else if 	( modelStr == "SNC-EM632R" ) 				model = SNC_EM632R;
	else if 	( modelStr == "SNC-EM600" )				model = SNC_EM600;
	else if 	( modelStr == "SNC-EB632R" )				model = SNC_EB632R;
	else if 	( modelStr == "SNC-WR632" )				model = SNC_WR632;
	else if 	( modelStr == "SNT-EX101" )				model = SNT_EX101;
	else if 	( modelStr == "SNT-EP104" )				model = SNT_EP104;

	for (int i=0; i<5; ++i)
	{
		this->threadRunning[i]=false;
	}

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo:init" << endl;
#endif
}

/******************************************************************
 * CamInfo -- Constructor 														*
 *																						*
 * 		id -- Identificador del dispositivo 							*
 *			cam -- Dirección del dispositivo 								*
 *			streamId -- Identificador del stream							*
 *			modelStr -- Modelo del dispositivo 								*
 *			size -- (???)															*
 *			cp -- (???)								 								*
 ******************************************************************/
SonyControlPlugin::CamInfo::CamInfo(	CPDeviceID id, 
													Address cam, 
													int streamId, 
													string modelStr, 
													int size, 
													SonyControlPlugin *cp 	)	: 
														bufferSize(size), 
														cp(cp), 
														id(id), 
														cam(cam), 
														streamId(streamId), 
														modelStr(modelStr), 
														queued(bLock), 
														full(&bLock), 
														inited(false),
														initing(false), 
														firstSession(true), 
														am(	cp->address, 
																"AlarmModule", 
																cp->cn 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo::CamInfo1" << endl;
#endif

	init(id, cp);

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo::CamInfo1" << endl;
#endif
}

/******************************************************************
 * CamInfo -- Constructor 														*
 *																						*
 * 		id -- Identificador del dispositivo 							*
 *			cam -- Dirección del dispositivo 								*
 *			modelStr -- Modelo del dispositivo 								*
 *			size -- (???)															*
 *			cp -- (???)								 								*
 ******************************************************************/
SonyControlPlugin::CamInfo::CamInfo(	CPDeviceID id, 
													Address cam, 
													string modelStr, 
													int size, 
													SonyControlPlugin *cp 	)	:
																		bufferSize(size), 
																		cp(cp), 
																		id(id), 
																		cam(cam), 
																		streamId(0), 
																		modelStr(modelStr), 
																		queued(bLock), 
																		full(&bLock), 
																		inited(false),
																		initing(false), 
																		firstSession(true), 
																		am(	cp->address, 
																				"AlarmModule", 
																				cp->cn 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo::CamInfo2" << endl;
#endif

	init(id, cp);

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo::CamInfo2" << endl;
#endif
}

/******************************************************************
 * queue -- (???)			 														*
 *																						*
 * 		id -- Identificador del dispositivo 							*
 *			data -- (???)															*
 *			keepAliveAddr -- (???)								 				*
 ******************************************************************/
void SonyControlPlugin::CamInfo::queue(	int id, 
														void *data, 
														Address *keepAliveAddr 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:queue: " << id << endl;
#endif

	full.lock();

	try
	{
		while(buffer.size()>bufferSize)
		{
			struct timespec t;
			t.tv_sec=time(NULL)+4;
			t.tv_nsec=0;

			if(full.wait(t))
			{
				cout 	<< " timeout queue full devInfo.queue addr:" 
						<< keepAliveAddr << " retr:" << endl;
			}
		}

		if (id == CPServ::setPTZ)
		{
			for(	list<Command>::iterator it=buffer.begin(), itEnd = buffer.end(); 
					it != itEnd; ++it)
				if(it->id==CPServ::setPTZ)
				{
					delete (CPPTZ*)it->data;
					it->data = NULL;
					it = buffer.erase(it);
				}
		}

		else if (id == CPServ::move)
		{
			for(	list<Command>::iterator it=buffer.begin(), itEnd = buffer.end(); 
					it != itEnd; ++it)
				if(it->id==CPServ::move)
				{
					delete (CPPTZ*)it->data;
					it->data = NULL;
					it=buffer.erase(it);
				}
		}

		buffer.push_back(Command(id, data));
	}
	catch(...){}

	full.unlock();

	queued.signal();

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo:queue" << endl;
#endif
}

/******************************************************************
 * execute -- (???)			 													*
 *																						*
 * 		int -- (???)															*
 *			data -- (???)															*
 ******************************************************************/
void *SonyControlPlugin::CamInfo::execute(int, void *data)
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:execute" <<  endl;
#endif
	
	int thrId = (int)data;

	if (thrId < 5)
	{
		this->threadRunning[thrId] = true;
	}

	if(thrId == 0)
	{

		//Camera command queue
		for(;;)
		{
			try
			{
				queued.lock(); 
				Command c(-2,NULL);
				try
				{
					while(buffer.size()==0)
					{
						full.signal();

						struct timespec t;
						t.tv_sec=time(NULL)+1;
						t.tv_nsec=0;
						queued.wait(t);
					}
					c=buffer.front();
					buffer.pop_front();
				
				}
				catch(Exception e)
				{
					cout 	<< " CamInfo::execute: exc: " << e.getClass() << ": " 
							<< e.getMsg() << endl;
				}
				catch(...){}
				
				queued.unlock();

				switch(c.id)
				{
					case CPServ::setPTZ:
						cout<<"exec queued command ptz"<<endl;
						cp->run_setPTZ(*((CPPTZ*)c.data), id);
						delete (CPPTZ*)c.data;
						c.data = NULL;
						break;
					case CPServ::setConfigParam:
						cout<<"exec queued command config"<<endl;
						cp->run_setConfigParam( *((CPConfigParam*) c.data), id );
						delete (CPConfigParam*)c.data;
						c.data = NULL;
						break;
					case CPServ::move:
						cout<<"exec queued command ptz"<<endl;
						cp->run_move(*((CPMovement*)c.data), id);
						delete (CPMovement*)c.data;
						c.data = NULL;
						break;
					case -1:
						cout<<"  -- exec queued command i=1"<<endl;
						((WaitInfo*)c.data)->i=1;
						((WaitInfo*)c.data)->c.lock();
						((WaitInfo*)c.data)->c.signal();
						((WaitInfo*)c.data)->c.unlock();
						break;
					default:
						cout << " ERROR: incorrect queued command id"<<endl;
						break;
				}
			}
			catch(Exception e)
			{
				cout 	<< " CamInfo::execute: exc: " << e.getClass() << ": " 
						<< e.getMsg() << endl;
			}
			catch(...){}

			if(buffer.size()+1 <= bufferSize)
			{
				full.signal();
			}
		}
	}
	else if(thrId==1)
	{
		int inputs=0;
		while (inputs==0)
		{
			string s1("SONY");
			string s2=cp->cams[id.id]->modelStr;
			string s3("/IOMapping/Inputs");
			string s=cp->db.getModelConfigParam(s1,s2,s3);

			char *c=(char*)s.c_str(), *l=NULL;
			inputs = strtol (c, &l, 10) ;

			//es "0" si l!=\0, es que hi ha agut un error de parsing 
			//(i.e., error d'accés a la BD)
			if(inputs==0 && *l=='\0' ) 
			{
				cout<<" Cam::AMThread "<< id.id<<"  -  no inputs :"<<s <<endl;

				if (thrId<5)
					this->threadRunning[thrId]=false;

#ifdef DEBUG
				cout << "<----- SonyCP:CamInfo:execute" <<  endl;
#endif
				return NULL;
			}
		}

		cout<<" Cam::AMThread found inputs:"<<id.id <<":"<<inputs<<endl;
		for(;;)
		{
			try
			{
				cout<<" Cam::AMThread StartSession "<<id.id <<endl;
				am.startSession(AMDeviceID(id.id));
				break;
			}
			catch(Exception &e)
			{
				cout 	<< " SCP::AlarmModule.startSession : " << e.getClass()
						<< " - " << e.getMsg() << endl;

				if ( e.getClass() == "AMSessionAlreadyStablishedException" )
					break;

				sleep(30);
			}
		}
		//si es perd la sessió, l'AlmModClient ja la recupera
		int values[inputs];
		for( int i = 0; i < inputs; ++i )
		{
			values[i]=0;
		}
		for(;;)
		{
			AMAlarmId alm;
			alm.devId=id.id;
			alm.type=AMAlarmId::Input;
			try
			{
				{
					int devId=id.id;

					cout<<" --- dev:"<<devId<<" - "<<inputs<<endl;

					AMAlarmId aid(AMAlarmId::Input, 0);
					aid.devId=devId;

					for(int i=0; i < inputs; ++i)
					{
						aid.intId=i;

						CPInput in = cp->getInput(CPInput(i,0), devId);
						
						if(in.value!=values[i])
						{
							values[i] = in.value;
							AMAlarmValue av;
							av.alarmId=aid;
							av.value=in.value;
							cout 	<< " setAlarm cam: " << devId << " input: " << i 
									<< " value:" << in.value << endl;
							am.setAlarm(av);
						}
					}
				}
				sleep(1);
			}catch(Exception &e)
			{
				if(e.getClass() == string("SocketException"))
				{
					//camera no connectada
					sleep(30);
				}
				else if(e.getClass() == string("CPInvalidParamException"))
				{
					//output invalid, es problema de config del sirius
					sleep(30);
				}else
					sleep(1);
			}
		}
	}
	
	if (thrId<5)
	{
		this->threadRunning[thrId]=false;
	}

#ifdef DEBUG
				cout << "<----- SonyCP:CamInfo:execute" <<  endl;
#endif

	return NULL;
}

/******************************************************************
 * startThreads -- Inicia los hilos que gestionarán los 				*
 *				distintos dispositivos.			 								*
 ******************************************************************/
void SonyControlPlugin::CamInfo::startThreads()
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:startThreads" <<  endl;
#endif

	if ( !this->threadRunning[0] )
	{
		start( (void*)0 );
	}	

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo:startThreads" <<  endl;
#endif	
}

/******************************************************************
 * wait -- (???)																	*
 *																						*
 *			keepAliveAddr -- (???)												*
 ******************************************************************/
void SonyControlPlugin::CamInfo::wait( Address *keepAliveAddr )
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:wait" <<  endl;
#endif

	WaitInfo *w=new WaitInfo();

	queue(-1, w, keepAliveAddr);

	w->c.lock();
	try
	{
		while(w->i==0)
		{
			struct timespec t;
			t.tv_sec = time(NULL) + 4;
			t.tv_nsec=0;
			if(w->c.wait(t))
			{
				if( keepAliveAddr != NULL )
				{
					RPC::keepAlive(keepAliveAddr, 0);
				}
			}
		}
	}
	catch(...){}

	w->c.unlock();
	delete w;
	w = NULL;

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo:wait" <<  endl;
#endif
}

/******************************************************************
 * getSize -- Obtiene el tamaño del buffer.								*
 ******************************************************************/
int SonyControlPlugin::CamInfo::getSize()
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:getSize" <<  endl;
	cout << "<----- SonyCP:CamInfo:getSize" <<  endl;
#endif

	return bufferSize;

}

/******************************************************************
 * setSize -- Configura el tamaño del buffer.							*
 *																						*
 *			size -- Nuevo tamaño para configurar el buffer.				*
 ******************************************************************/
void SonyControlPlugin::CamInfo::setSize(int size)
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:setSize" <<  endl;
#endif

	if( size > 0 )
	{
		bufferSize = size;		
	}

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo:setSize" <<  endl;
#endif
}

/******************************************************************
 * setSize -- Obtiene el tamaño usado del buffer.						*
 ******************************************************************/
int SonyControlPlugin::CamInfo::getUse()
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:getUse" <<  endl;
#endif

	int i = buffer.size();
	if(i > bufferSize)
	{
		i = bufferSize;
	}

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo:getUse" <<  endl;
#endif
	return i;
}

/******************************************************************
 * setSize -- Obtiene el porcentaje de uso del buffer de CamInfo.	*
 ******************************************************************/
float SonyControlPlugin::CamInfo::getPercentInUse()
{
#ifdef DEBUG
	cout << "-----> SonyCP:CamInfo:getPercentInUse" <<  endl;
#endif

	float f = ( buffer.size() / bufferSize );
	if( f > 1 )
	{
		f = 1;
	}

#ifdef DEBUG
	cout << "<----- SonyCP:CamInfo:getPercentInUse" <<  endl;
#endif
	return f;
}

/******************************************************************
 * execute -- (???)																*
 *																						*
 *			int -- No se hace nada con esta variable						*
 *			void* -- No se hace nada con esta variable					*
 ******************************************************************/
void *SonyControlPlugin::CamInitStarterThread::execute( int, void* )
{
#ifdef DEBUG
	cout << "-----> SonyCP:CIST:execute" <<  endl;
#endif

	int starterThreads=0;
	cout<<"CamInitStarterThread:: detach scp:"<<endl;

	for(	map<int, CamInfo*>::iterator it=scp->cams.begin(), 
			itEnd = scp->cams.end(); it != itEnd; ++it)
	{
		CamInfo *inf= it->second;
		try
		{	
			if(!sequential)
			{
				while(starterThreads >= maxStarterThreads)
				{
					int thId=scp->ci.getJoinableThread();
					while(thId==-1)
					{
						usleep(10000);
						thId=scp->ci.getJoinableThread();
					}
					cout 	<<	"******************************************  "
							<< "CamInitStarterThread:: join thread " << thId << endl;
					scp->ci.join(thId);
					cout  << "******************************************  "
							<< "CamInitStarterThread:: joined thread " << thId << endl;
					--starterThreads;
				}
			}
			int cid = inf->id.id;
			bool reloadDev = reload || listFind(devs, cid);// != devs.end();
			cout << "  ------------------------------------------- CamInit:"
					<< inf->id.id << " create ExecInfo reload:" << reloadDev 
					<< endl;

			CamInit::execInfo *ei = new SonyControlPlugin::CamInit::execInfo(	cid, 
														conf, 
														inf->modelStr, 
														scp, 
														reloadDev );

			int thid=scp->ci.start(ei);

			cout << " CamInit:" << inf->id.id << " started" << endl;

			if(sequential)
			{
				scp->ci.join(thid); //descomentar pq sigui sequencial (debug)
			}

			++starterThreads;
		}
		catch(SocketTimeoutException &e)
		{
			//no s'ha trobat la camera?
			cout 	<< "SCP:: loadDBConfig: could not connect to cam " 
					<< inf->id.id << " SocketTimeoutException:" 
					<< e.getMsg() << endl;
		}
		catch(SocketException &e)
		{
			//no s'ha trobat la camera?
			cout 	<< "SCP:: loadDBConfig: could not connect to cam "
					<< inf->id.id << " SocketException:" << e.getMsg() << endl;
		}
		catch(Exception &e)
		{
			//no s'ha trobat la camera?
			cout 	<< "SCP:: loadDBConfig: could not connect to cam " 
					<< inf->id.id << " Exception:" << e.getClass() << ":" 
					<< e.getMsg() << endl;
		}
	}

	cout<<"CamInitStarterThread:: detach "<<endl;

	if(	( !reload ) 	&& 
			( devs.size() == 0 )	)
	{
		scp->ci.detachAll();
	}
	else
	{
		scp->ci.joinAll();
	}

#ifdef DEBUG
	cout << "<----- SonyCP:CIST:execute" <<  endl;
#endif

	return NULL;
}

/******************************************************************
 * SonyControlPlugin -- Constructor											*
 *																						*
 *				file -- Archivo de configuración (XML)						*
 *				sequentialReloads -- (???)										*
 *				reloadDevs -- Recarga de dispositivos (???)				*
 *				reloadModels -- Recarga de modelos (???)					*
 *				reloadDevList -- Recarga de la lista de dispositivos	*
 ******************************************************************/
SonyControlPlugin::SonyControlPlugin( 	string file, 
					bool sequentialReloads, 
					bool reloadDevs, 
					bool reloadModels, 
					list<int> reloadDevList 	)	:	PluginControl( file ),db( address, type, cn ), file( file )
{
#ifdef DEBUG
	cout << "-----> SonyCP:SonyControlPlugin" <<  endl;
#endif

	conf = getConfigFromFile( file );
	string configFile = conf->getNodeData("[0]/ModelConfigFile");
	string devConfig = conf->getNodeData("[0]/DeviceConfigFile");

	delete conf;
	conf = NULL;

	modelConf = getConfigFromFile( configFile );

	loadModelsConfig(modelConf, reloadModels);

	string s("Select d.id, d.ip, d.stream, d.puerto, m.codigo "
				"FROM dispositivo d, modelo m, fabricante f "
				"WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='SONY'");

	cout << "db.query "<< s << endl;

	XML *devInfo = db.query(s);

	xmlNode *n = devInfo->getNode("/result");
	int numCams = atoi((n->getParam("numRows")).c_str());

	cout << " loading " << numCams << " Cams" << endl;


	for(int i=0; i<numCams; ++i)
	{
		cout << " loading cam " << i << endl;

		string defaultPath = "/result/[" + StrUtils::decToString( i );

		n = devInfo->getNode( defaultPath + "]/id" );
		int id = atoi( n->getCdata().c_str() );

		n = devInfo->getNode( defaultPath + "]/ip" );
		string ip = n->getCdata().c_str();

		if ( ip.find('/') != string::npos ) 
		{
			ip = ip.substr(0,ip.find('/')); 		//eliminem mascara de xarxa
		}

		cout << " loading cam: " << i << " [" << id << "] - " << ip << endl;

		n = devInfo->getNode( defaultPath + "]/puerto" );
		int puerto = atoi( n->getCdata().c_str() );

		n = devInfo->getNode( defaultPath + "]/stream" );
		int stream = atoi( n->getCdata().c_str() );

		n = devInfo->getNode( defaultPath + "]/codigo" );
		string model = n->getCdata();

		Address a( IP(ip), puerto );

		if ( models.find(model) == models.end() )
		{
			models[model] = new ModelInfo();
		}

		cams[id] = new CamInfo(	id, 
										a, 
										stream, 
										model, 
										default_buffer_size, 
										this );

		cout << " loading cam " << i << "  id: " << id << " new CI" << endl;
		cout << "                                         - " 
					<< (void*)cams[id] << endl;
	}

	delete devInfo;
	devInfo = NULL;

	cout<<"----------plugin inicialitzat----------"<<endl;

	conf = getConfigFromFile( devConfig );

	cist.conf = conf;
	cist.scp = this;
	cist.reload = reloadDevs;
	cist.devs = reloadDevList;
	cist.sequential = sequentialReloads;
	cist.start();

	if(	!sequentialReloads 	&& 
			!reloadDevs 			&& 
			!reloadModels 			&& 
			reloadDevList.size() == 0 )
	{
		cist.detach();
	}
	else
	{
		cist.join();
		exit(0);
	}

#ifdef DEBUG
	cout << "<----- SonyCP:SonyControlPlugin" <<  endl;
#endif
}

XML* SonyControlPlugin::getConfigFromFile( string file )
{
#ifdef DEBUG
	cout << "-----> SonyCP:SonyControlPlugin: " << file << endl;
#endif
	
	char* buffer(NULL);
	std::size_t size(0);

	ifstream fichero( file.c_str(), ifstream::in );

	if ( fichero.is_open() )
	{
  		filebuf *pbuffer = fichero.rdbuf();
  		size = pbuffer->pubseekoff (0, fichero.end, fichero.in);
  		pbuffer->pubseekpos (0, fichero.in);

  		buffer = new char[size];
  		pbuffer->sgetn (buffer, size);
	}
	else
		cout << "Error opening file '" << file.c_str() << endl;

	fichero.close();

	XML *conf = xmlParser::parse( string( buffer, size ) );

	delete buffer;
	buffer = NULL;

#ifdef DEBUG
	cout << "<----- SonyCP:getConfigFromFile" <<  endl;
#endif

	return conf;
}

/******************************************************************
 * SonyControlPlugin -- Destructor											*
 ******************************************************************/
SonyControlPlugin::~SonyControlPlugin()
{
#ifdef DEBUG
	cout << "-----> SonyCP:~SonyControlPlugin" <<  endl;
#endif

	for(	map<int, CamInfo*>::iterator it = cams.begin(), itEnd = cams.end(); 
			it != itEnd; ++it)
	{
		if(it->second!=NULL)
		{
			delete it->second;
			it->second = NULL;
		}
	}

#ifdef DEBUG
	cout << "<----- SonyCP:~SonyControlPlugin" <<  endl;
#endif
}

/******************************************************************
 * getBasicAuthStrForDevice -- Devuelve un string en base64 		*
 * 	necesario para realizar la conexión con el dispositivo 		*
 * 	mediante HTTP Basic.														*
 *																						*
 *				devId -- Identificador del dispositivo. 					*
 ******************************************************************/
string SonyControlPlugin::getBasicAuthStrForDevice( int devId )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getBasicAuthStrForDevice: " << devId << endl;
#endif

	string str("");
	DeviceAuthentication::AuthInfo ai;

	if (this->devAuth!=NULL)
	{
		try
		{
			ai=this->devAuth->getAuthByCamId(devId);
		}
		catch (InvalidParameterException &ipe)
		{
		}

		if (ai.username.length()>0)
			str=ai.username + string(":") + ai.password;
		else
			str=string("admin:admin");
	}
	else
		str=string("admin:admin");

#ifdef DEBUG
	cout << "<----- SonyCP:getBasicAuthStrForDevice" <<  endl;
#endif

	if (str.length()>0)
		return string("Basic ")+base64encode(str);

	return string("");
}

/******************************************************************
 * getSession -- (???) 															*
 *																						*
 *				a -- (???)															*
 ******************************************************************/
SessionDispatcher SonyControlPlugin::getSession( Address a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getSession" << endl;
#endif

	map<string,SessionDispatcher>::iterator i=sessions.find(a.toString());

	if (i==sessions.end())
	{
		throw ( CPSessionNotStablishedException(0, string("Session no establerta") ) );
	}

	i->second.initializeTimeOfSession();

#ifdef DEBUG
	cout << "<----- SonyCP:getSession" <<  endl;
#endif

	return i->second;
}

/******************************************************************
 * checkModelMetadata -- Comprueba los metadatos del modelo. 		*
 *																						*
 *				nom -- Nombre del modelo.										*
 ******************************************************************/
void SonyControlPlugin::checkModelMetadata( string nom )
{
#ifdef DEBUG
	cout << "-----> SonyCP:checkModelMetadata: " << nom << endl;
#endif

	// Correccions a les funcions del model segons si s'indica que
	// te DEPA o no (/Capabilities/Metadata/DEPA)
	string depaSupport = db.getModelConfigParam(	"SONY", 
																nom, 
																"/Capabilities/Metadata/DEPA");

	string funcionesIdQuery=string("SELECT m.funciones from modelo m, fabricante f where m.codigo='")+nom+string("' AND m.fabricante=f.id AND f.nombre='SONY'");			

	string hasMetadataQuery=string("SELECT fm.id FROM funcionesmodelo fm, funcionesmodelometadata fmm WHERE fm.id=fmm.id AND fm.id=(")+funcionesIdQuery+string(")");

	XML *hasMetadata=db.query(hasMetadataQuery);
	
	xmlNode *hasMetadataNode=hasMetadata->getNode("/result");
	int nMeta=atoi((hasMetadataNode->getParam("numRows")).c_str());

	delete hasMetadata;
	hasMetadata = NULL;

	// Query que trobara el mateix funcionesmodelo sense metadata o
	// amb, segons si el nostre te o no metadata.
	string alternativeQuery=string("SELECT fm2.id FROM funcionesmodelo fm LEFT OUTER JOIN funcionesmodelovideo fmv ON fm.id=fmv.id LEFT OUTER JOIN funcionesmodeloaudio fma ON fm.id=fma.id LEFT OUTER JOIN funcionesmodeloio fmio ON fm.id=fmio.id LEFT OUTER JOIN funcionesmodelometadata fmm ON fm.id=fmm.id, funcionesmodelo fm2 LEFT OUTER JOIN funcionesmodelovideo fmv2 ON fm2.id=fmv2.id LEFT OUTER JOIN funcionesmodeloaudio fma2 ON fm2.id=fma2.id LEFT OUTER JOIN funcionesmodeloio fmio2 ON fm2.id=fmio2.id LEFT OUTER JOIN funcionesmodelometadata fmm2 ON fm2.id=fmm2.id WHERE fm.id=(")+funcionesIdQuery+string(") AND (fmv.id IS NULL) = (fmv2.id IS NULL) AND (fma.id IS NULL) = (fma2.id IS NULL) AND (fmio.id IS NULL) = (fmio2.id IS NULL) AND fmm2.id IS ")+(nMeta==0?string("NOT "):string(""))+string("NULL;");

	if ((nMeta==0 && depaSupport==string("true")) || (nMeta>0 && depaSupport==string("false")))
	{
		XML *alt=db.query(alternativeQuery);
		xmlNode *altNode=alt->getNode("/result");
		int nAlt=atoi((altNode->getParam("numRows")).c_str());

		string funcionesId = altNode->getNodeData(string("/result/[0]/id"));
		if (nAlt>0)
		{
			string update=string("UPDATE modelo SET funciones='")+funcionesId+string("' FROM fabricante f WHERE codigo='")+nom+string("' AND fabricante=f.id AND f.nombre='SONY';");
			XML *upd=db.update(update);
			delete upd;
			upd = NULL;
		}

		delete alt;
		alt = NULL;
	}

#ifdef DEBUG
	cout << "<----- SonyCP:checkModelMetadata" <<  endl;
#endif
}

/******************************************************************
 * loadModelsConfig -- Carga la configuración de todos los 			*
 *				modelos a partir del XML (SCPModelConfig.xml)			*
 *																						*
 *				conf -- (???)														*
 *				reload -- (???)													*
 ******************************************************************/
void SonyControlPlugin::loadModelsConfig( XML *conf, bool reload )
{
#ifdef DEBUG
	cout << "-----> SonyCP:loadModelsConfig: " << reload << endl;
#endif

	cout<<" ----------- SonyCP:: load Models Config ---------- " << endl;
	string s("select m.codigo from modelo m, fabricante f where m.fabricante=f.id AND f.nombre='SONY'");

	XML *devInfo=db.call(s, 0);

	xmlNode *n=devInfo->getNode("/result");
	int numMod= atoi((n->getParam("numRows")).c_str());

	for(int i=0; i<numMod; ++i)
	{
		string nom = devInfo->getNodeData(string("/result/[")+StrUtils::decToString(i)+string("]/codigo"));

		if(reload || !db.checkModelConfigParam("SONY", nom, "/Capabilities"))
		{
			cout<<"       ------------------- SonyCP:: load Model Config - "<< nom << endl;
			loadModelConfig(nom, "/", "/", conf);
			checkModelMetadata(nom);
		}
	}

	delete devInfo;
	devInfo = NULL;

#ifdef DEBUG
	cout << "<----- SonyCP:loadModelsConfig" <<  endl;
#endif
}

/******************************************************************
 * loadModelConfig -- Carga la configuración de un modelo 		 	*
 *																						*
 *				model -- El modelo que nos interesa							*
 *				nodepath -- (???)													*
 *				dbpath -- (???)													*
 *				conf -- (???)														*
 ******************************************************************/
void SonyControlPlugin::loadModelConfig(	string model, 
														string nodepath, 
														string dbpath, 
														XML *conf)
{
#ifdef DEBUG
	cout << "-----> SonyCP:loadModelConfig: " << model 	<< ", " 
															<< nodepath << ", "
															<< dbpath	<< endl;
#endif

	if(nodepath==string("/"))
		nodepath=string("/[0]/Models/All/");

	xmlNode *n = conf->getNode(nodepath);

	list<xmlNode*> kids=n->getKidList();
	int pos=nodepath.find("All");


	if(kids.size()==0 || n->getCdata()!=string("")) //fulla, o node amb dades a introduir al nodoconfig
	{
		if(pos!=string::npos) // generica, busquem especifica del model
		{
			nodepath.replace(pos,3, model);
			xmlNode *m = conf->getNode(nodepath);

			if(m!=NULL)
				n=m;
		}

		string s=db.getModelConfigParam("SONY", model, dbpath);
		if(s != n->getCdata() || s==string(""))
		{
			db.setModelConfigParam("SONY", model, dbpath, n->getCdata());
		}
	}

	kids=n->getKidList();
	pos=nodepath.find("All");

	if(kids.size()>0)
	{
		if(pos==string::npos) //especifica del model
		{
			int dbidx=0;
			for(	list<xmlNode*>::iterator it = kids.begin(), itEnd = kids.end();
					it != itEnd; ++it)
			{
				if((*it)->getParam("nodename")==string("inc"))
				{
					loadModelConfig(model, nodepath + (*it)->getName() + string("[") + StrUtils::decToString(dbidx) + string("]") + string("/"),
							dbpath + StrUtils::decToString(dbidx+1) + string("/"), conf);
					++dbidx;
				}
				else
				{
					loadModelConfig(model, nodepath + (*it)->getName() + string("/"), dbpath + (*it)->getName() + string("/"), conf);
				}
			}
		}
		else
		{	//config generica, hem de tenir en compte el model
			bool inc=false;
			int nidx=0;
			for(	list<xmlNode*>::iterator it = kids.begin(), itEnd = kids.end();
					it != itEnd; ++it)
			{
				if((*it)->getParam("nodename")==string("inc"))
					inc=true;
				else
				{
					loadModelConfig(model, nodepath + (*it)->getName() + string("/"), dbpath+(*it)->getName() + string("/"), conf);
				}
				++nidx;
			}

			if(inc)
			{
				string mpath=nodepath;
				mpath.replace(pos,3, model);
				xmlNode *m = conf->getNode(mpath);
				inc=false;
				if(m!=NULL)
				{
					list<xmlNode*> mkids=m->getKidList();

					int dbidx=0;
					//mirem si tenim incs especifics del model
					for(	list<xmlNode*>::iterator it = mkids.begin(), 
							itEnd = mkids.end(); it != itEnd; ++it) 
					{
						if((*it)->getParam("nodename")==string("inc"))
						{
							loadModelConfig(model, mpath+(*it)->getName()+string("[")+StrUtils::decToString(dbidx)+string("]")+string("/"),
									dbpath+StrUtils::decToString(dbidx+1)+string("/"), conf);
							++dbidx;

							inc=true;
						}
					}
				}

				if(!inc) //si no en tenim, pillem els generics
				{
					int dbidx=0;

					for(	list<xmlNode*>::iterator it = kids.begin(), 
							itEnd = kids.end(); it!=kids.end(); ++it)
					{
						if((*it)->getParam("nodename")==string("inc"))
						{
							loadModelConfig(model, nodepath+(*it)->getName()+string("[")+StrUtils::decToString(dbidx)+string("]")+string("/"), dbpath+StrUtils::decToString(dbidx+1)+string("/"), conf);
							++dbidx;
						}
					}
				}
			}
		}
	}

#ifdef DEBUG
	cout << "<----- SonyCP:loadModelConfig" <<  endl;
#endif
}
 
/******************************************************************
 * loadDevConfig -- Llama a la auténtica función para cargar 		*
 * 				la configuración del dispositivo.					 	*
 *																						*
 *				id -- Identificador del dispositivo.						*
 *				model -- Modelo del dispositivo								*
 *				nodepath -- (???)													*
 *				dbpath -- (???)													*
 *				conf -- (???)														*
 ******************************************************************/
void SonyControlPlugin::loadDevConfig(	int id, 
													string model, 
													string nodepath, 
													string dbpath, 
													XML *conf 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:loadDevConfig1: " 	<< id 		<< ", " 
															<< model 	<< ", "
															<< nodepath << ", "
															<< dbpath 	<< endl;
#endif

	this->loadDevConfig(	id, model, nodepath, dbpath, conf, &this->db );

#ifdef DEBUG
		cout << "<----- SonyCP:loadDevConfig1" <<  endl;
#endif
}

/******************************************************************
 * loadDevConfig -- Carga la configuración de un dispositivo.	 	*
 *																						*
 *				id -- Identificador del dispositivo.						*
 *				model -- Modelo del dispositivo								*
 *				nodepath -- (???)													*
 *				dbpath -- (???)													*
 *				conf -- (???)														*
 *				db -- Base de datos												*
 ******************************************************************/
void SonyControlPlugin::loadDevConfig(	int id, 
													string model, 
													string nodepath, 
													string dbpath, 
													XML *conf, 
													DBGateway *db 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:loadDevConfig2: " 	<< id 		<< ", " 
															<< model 	<< ", "
															<< nodepath << ", "
															<< dbpath 	<< endl;
#endif

	if(nodepath.length()<13)
		nodepath=string("/[0]/Devices/")+model;
	else if(nodepath.substr(5,7)!=string("Devices"))
		nodepath=string("/[0]/Devices/")+model;

	xmlNode *n = conf->getNode(nodepath);

	if(n==NULL)
	{
		cout<<" LoadDevConfig n==NULL!!! "<<nodepath<<" db:"<<dbpath<<endl;

#ifdef DEBUG
		cout << "<----- SonyCP:loadDevConfig2" <<  endl;
#endif

		return;
	}

	// filtro. al principio, puede querer filtrar toda una rama
	if(n->getParam("filter")!=string("")) 
	{
		string filter=n->getParam("filter");
		int pos=filter.rfind("=");
		if(pos!=string::npos)
		{
			string fdbpath= addPath(dbpath, filter.substr(0, pos));
			string value=db->getDeviceConfigParam(id, fdbpath);
			string fvalue=filter.substr(pos+1);

			if(value!=fvalue)
			{
				cout<<"|";

#ifdef DEBUG
		cout << "<----- SonyCP:loadDevConfig2" <<  endl;
#endif
				return;
			}
		}
	}

	list<xmlNode*> kids=n->getKidList();

	// nodo intermedio
	if ( kids.size() > 0 )
	{
		int nidx 	= 0; 		//index del node dins la branca del xml. 
									// XML->getNode("/foo/bar/["+nidx+"]")
		int dbidx 	= 1;

		int absidx 	= 0; 		//index del node dins la DB. pels nodename="inc" 

		// para cada hijo
		for(	list<xmlNode*>::iterator it = kids.begin(), itEnd = kids.end(); 
				it != itEnd; ++it) 
		{

			if ( (*it)->getParam("nodename") == "inc" ) // nodes <1>,<2>,...,<n>
			{
				// no depende de ningún resultado, llamada recursiva
				if((*it)->getParam("values")==string("")) 
				{	
					loadDevConfig(id, model, nodepath+string("/")+(*it)->getName()+string("[")+StrUtils::decToString(nidx)+string("]"),
							dbpath+string("/")+StrUtils::decToString(dbidx), conf, db);
					++nidx;
					++dbidx;
				}

				// els valors depenen d'una resposta de la cam carregar de/a 
				// la cam per cada value
				else
				{ 

					string val=(*it)->getParam("values");

					// buscamos el nodo al que hace referencia 'values'
					string vnpath=addPath(nodepath+string("/")+(*it)->getName() , val); 
					xmlNode *vn = conf->getNode(vnpath);


					if(vn!=NULL && vn->getParam("inquiry")!=string(""))
					{
						string vdbpath=addPath(dbpath+string("/")+StrUtils::decToString(dbidx) , val);

						//	Current node does not exist
						if(!db->checkDeviceConfigParam(id, vdbpath)) 
						{

							char url[256];
							string param = n->getParam("param");
							if(model == string("SNT-V704") || model == string("SNT-V704-L9EMB"))
							{
								char c[64];
								sprintf(c, param.c_str(), cams[id]->streamId+1);
								param=string(c);
							}

							string inq = n->getParam("inquiry");
							if(model == string("SNT-V704") || model == string("SNT-V704-L9EMB"))
							{
								char c[64];
								sprintf(c, inq.c_str(), cams[id]->streamId+1);
								inq=string(c);
							}

							sprintf(url,"http://%s/command/inquiry.cgi?inq=%s", cams[id]->cam.getIP().toString().c_str(), inq.c_str());

							string value=getConfigParamCamera(url, param, model, id);
							value=translateCam(vn->getName(), value, cams[id]->model);

							// guarrade per uns temes de les cameres SONY amb les 
							// resolucions
							if(value==string("altParam")) 
							{
								sprintf(url,"http://%s/command/inquiry.cgi?inq=%s", cams[id]->cam.getIP().toString().c_str(), vn->getParam("altInquiry").c_str());
								value=getConfigParamCamera(url, vn->getParam("altParam"), model, id);
								value=translateCam(vn->getName(), value, cams[id]->model);
							}

							// lista de valores separados por ",". Reponsabilidad de 
							// translateCam
							list<string> v=StrUtils::split(value,","); 
							for(	list<string>::iterator i = v.begin(), iEnd = v.end(); 
									i != iEnd; ++i)
							{
								string np= nodepath+string("/")+(*it)->getName()+string("[")+StrUtils::decToString(nidx)+string("]");
								string dbp=dbpath+string("/")+StrUtils::decToString(dbidx);

								vdbpath=addPath(dbpath+string("/")+StrUtils::decToString(dbidx) , val);

								cout<<" loadDevConfig set db:"<<vdbpath<<endl;

								// guardamos en la BD el valor adecuado que hemos 
								// traído del nodo 'values'
								db->setDeviceConfigParam(id, vdbpath, *i); 

								loadDevConfig(id, model, np, dbp, conf, db);

								++dbidx;
							}
						}
						// ya existe en la BD, recargamos las cosas en la cámara
						else
						{ 
							string values("");
							do{
								values=values+string(",")+ db->getDeviceConfigParam(id, vdbpath);

								++dbidx;
								vdbpath=addPath(dbpath+string("/")+StrUtils::decToString(dbidx) , val);
							}while(db->checkDeviceConfigParam(id, vdbpath));
							int pos=val.rfind("/");
							if(values!=string("") && pos!=string::npos)
							{
								values=values.substr(1);

								values=translateDB(val.substr(pos+1), values, cams[id]->model);

								char url[256];
								string param = vn->getParam("param");
								if(vn->getParam("setparam") != string(""))
									param = vn->getParam("setparam");
								if(model == string("SNT-V704") || model == string("SNT-V704-L9EMB"))
								{
									char c[64];
									sprintf(c, param.c_str(), cams[id]->streamId+1);
									param=string(c);
								}

								// ya existe en la BD, recargamos las cosas en la cámara

								sprintf(	url,	
											"http://%s/command/%s?%s=%s", 
											cams[id]->cam.getIP().toString().c_str(), 
											vn->getParam("set").c_str(), 
											param.c_str(), values.c_str()	);

								// nos aseguramos que sigue en la cámara
								setConfigParamCamera(url, id); 
							}
						}
						++nidx;
					}
				}
			}
			// nodo intermedio "normal", llamada recursiva
			else
			{	
				loadDevConfig(id, model, nodepath+string("/[")+StrUtils::decToString(absidx)+string("]"),
						dbpath+string("/")+(*it)->getName(), conf, db);
			}
			++absidx;
		}
	}

	// nodo hoja
	else
	{	
		bool dbCheck=false;
		if(n->getName()==string("Area"))
		{
			dbCheck = (db->getDeviceConfigParam(id, dbpath+string("/xMin")) != string("") ) &&
				(db->getDeviceConfigParam(id, dbpath+string("/yMin")) != string("") ) &&
				(db->getDeviceConfigParam(id, dbpath+string("/xMax")) != string("") ) &&
				(db->getDeviceConfigParam(id, dbpath+string("/yMax")) != string("") );
		}
		else if(n->getName()==string("Resolution"))
		{
			dbCheck = (db->getDeviceConfigParam(id, dbpath+string("/x")) != string("") ) &&
				(db->getDeviceConfigParam(id, dbpath+string("/y")) != string("") );
		}
		else if((n->getName()==string("Min") || n->getName()==string("Max")) && nodepath.find("/ObjectDetection")!=string::npos)
		{
			dbCheck = (db->getDeviceConfigParam(id, dbpath+string("/x")) != string("") ) &&
				(db->getDeviceConfigParam(id, dbpath+string("/y")) != string("") );
		}
		else
		{
			dbCheck = (db->getDeviceConfigParam(id, dbpath) != string("") );
		}

		if(!dbCheck)
		{
			cout << " !dbCheck " << dbpath << " : " << dbCheck << endl;
		}

		// si no está en la BD, lo cargamos de la cámara
		if ( 	( n->getParam("mode") == "reload" ) 	|| 
				( !dbCheck )	) 
		{
			cout << " loadDevConfig not in db->" << dbpath << endl;

			if (	n->getParam("inquiry") != "" )
			{
				cout << " loadDevConfig inq: " << n->getParam("inquiry") << endl;

				char url[256];
				string param 	= n->getParam("param");
				string inq 		= n->getParam("inquiry");

				if(model == string("SNT-V704") || model == string("SNT-V704-L9EMB"))
				{
					char c1[64];
					char c2[64];
					sprintf(c1, param.c_str(), cams[id]->streamId+1);
					sprintf(c2, inq.c_str(), 	cams[id]->streamId+1);

					param = string(c1);
					inq 	= string(c2);
				}

				string value("");
				list<string> parms = StrUtils::split(param,",");

				// Consulta a la cámara el parámetro que nos interesa
				sprintf( url,
							"http://%s/command/inquiry.cgi?inq=%s", 
							cams[id]->cam.getIP().toString().c_str(), 
							inq.c_str() 	);

				for(	list<string>::iterator p = parms.begin(), pEnd = parms.end(); 
						p != pEnd; ++p)
				{
					value += "," + getConfigParamCamera(url, *p, model, id);
				}
				
				if(value.length() > 1)
				{
					value = value.substr(1);
				}

				cout << 	" loadDevConfig: setDBCP translate read:"	
							"" <<	n->getParam("param")	<< " " << n->getName() << ":"
							"" << value << " - " << cams[id]->model << endl;

				cout << 	" Param: " << n->getParam("param") << " getName: " 
							"" << n->getName() << " values: " << value << endl;

				value = translateCam( n->getName(), value, cams[id]->model );

				if ( value == "altParam" )
				{
					sprintf( 	url,
									"http://%s/command/inquiry.cgi?inq=%s", 
									cams[id]->cam.getIP().toString().c_str(), 
									n->getParam("altInquiry").c_str() 	);

					value = getConfigParamCamera(	url, 
															n->getParam("altParam"), 
															model, 
															id );

					value = translateCam(n->getName(), value, cams[id]->model);
				}

				// aixĂł no hauria d'estar hardcodat al codi...
				if ( n->getName() == "Resolution" ) 
				{
					int pos = value.find(".");
					if 	( pos != string::npos )
					{
						if ( db->getDeviceConfigParam(id, "/Video/Standard/") == "pal" )
						{
							value=value.substr(0,pos);
						}
						else
						{
							value=value.substr(pos+1);
						}
					}

					if ( n->getParam("mode") != "noDB" ) 
					{
						int pos = value.find(",");

						if( pos != string::npos)
						{
							db->setDeviceConfigParam(	id, 
																dbpath + "/x", 
																value.substr(0,pos) 	);
							db->setDeviceConfigParam(	id, 
																dbpath + "/y", 
																value.substr(pos+1) 	);
						}
					}
				}

				else if ( ( n->getName() == "Area" ) 		&& 
							 ( ( nodepath.find("/ActivityDetection") != string::npos ) || 
							 ( nodepath.find("/Alarms") != string::npos ) ) )
				{
					if ( n->getParam("mode") != "noDB" )
					{
						int pos = value.find(",");
						if(pos != string::npos)
						{
							db->setDeviceConfigParam(	id, 
																dbpath + "/xMin", 
																value.substr(0,pos) 	);
							value = value.substr(pos+1);
							pos = value.find(",");

							if(pos != string::npos)
							{
								db->setDeviceConfigParam(	id, 
																	dbpath + "/yMin", 
																	value.substr(0,pos) );
								value = value.substr(pos+1);
								pos 	= value.find(",");

								if ( pos != string::npos )
								{
									db->setDeviceConfigParam(	id, 
																		dbpath + "/xMax", 
																		value.substr(0,pos) );
									value=value.substr(pos+1);
									db->setDeviceConfigParam(	id, 
																		dbpath + "/yMax", 
																		value );
								}
							}
						}
					}
				}
				else if (	(	n->getName() == "Min" || n->getName() == "Max") && 
								( nodepath.find("/ObjectDetection") != string::npos )	)
				{
					if(n->getParam("mode")!=string("noDB"))
					{
						int pos=value.find(",");
						if(pos!=string::npos)
						{
							db->setDeviceConfigParam(	id, 
																dbpath + "/x", 
																value.substr(0,pos) );
							db->setDeviceConfigParam(	id, 
																dbpath + "/y", 
																value.substr(pos+1) );
						}
					}
				}
				else
				{
					if(n->getParam("mode") != "noDB")
					{
						db->setDeviceConfigParam( id, dbpath, value );
					}
				}
			}
			// no existe inquiry, nos aseguramos que la cámara está en el modo
			// que nos conviene			
			else
			{ 
				if ( n->getParam("mode") != "noDB" )
				{
					db->setDeviceConfigParam( id, dbpath, n->getCdata() );
				}

				if ( n->getParam("set") != "" )
				{
					string value = translateDB(	n->getName(), 
															n->getCdata(), 
															cams[id]->model 	);
					string p = n->getParam("param");
					if( n->getParam("setparam") != "")
					{
						p = n->getParam("setparam");
					}
					list<string> pars = StrUtils::split(p, ",");
					for(	list<string>::iterator i 		= pars.begin(), 
														  iEnd 	= pars.end(); 
							i != iEnd; ++i )
					{
						char url[256];
						string param = *i;
						if (	( model == "SNT-V704") 			|| 
								( model == "SNT-V704-L9EMB" )	)
						{
							char c[64];
							sprintf( c, param.c_str(), cams[id]->streamId+1 );
							param = string(c);
						}
						sprintf(	url,
									"http://%s/command/%s?%s=%s", 
									cams[id]->cam.getIP().toString().c_str(), 
									n->getParam("set").c_str(), 
									param.c_str(), value.c_str() 	);

						// no existe inquiry, nos aseguramos que la cámara está en el modo que nos conviene

						setConfigParamCamera(url, id); //ens assegurĂ©m que sigui a la cam
					}
				}
			}
		}

		// ya está en la BD, nos aseguramos que está en la cámara
		else
		{
			string value("");

			if( n->getName() == "Area" )
			{
				value = db->getDeviceConfigParam( id, dbpath + "/xMin" );

				value += "," + db->getDeviceConfigParam( id, dbpath + "/yMin" );
				value += "," + db->getDeviceConfigParam( id, dbpath + "/xMax" );
				value += "," + db->getDeviceConfigParam( id, dbpath + "/yMax" );
			}

			else if 	(n->getName() == "Resolution" )
			{
				value = db->getDeviceConfigParam( id, dbpath + "/x" );
				value += "," + db->getDeviceConfigParam( id, dbpath + "/y" );
			}

			else if( (n->getName() == "Min" || n->getName() == "Max") 	&& 
						( nodepath.find("/ObjectDetection") != string::npos )	)
			{
				value = db->getDeviceConfigParam( id, dbpath + "/x" );
				value += "," + db->getDeviceConfigParam( id, dbpath + "/y" );
			}
			else
			{
				value = db->getDeviceConfigParam(id, dbpath);
			}

			value = translateDB(n->getName(), value, cams[id]->model);

			string p = n->getParam("param");

			if ( n->getParam("setparam") != "")
			{
				p = n->getParam("setparam");
			}

			list<string> pars = StrUtils::split(p, ";");
			for(	list<string>::iterator 	i 		= pars.begin(), 
													iEnd 	= pars.end(); 
					i != iEnd; ++i )
			{
				char url[256];
				string param = *i;
				if 	(	( model == "SNT-V704") 			|| 
							( model == "SNT-V704-L9EMB" )	)
				{
					char c[64];
					sprintf(c, param.c_str(), cams[id]->streamId+1);
					param = string(c);
				}

				// Si la cadena está vacía 
				if ( n->getParam("set").length() == 0)
				{
					sprintf(	url,
								"http://%s/command/camera.cgi?%s=%s", 
								cams[id]->cam.getIP().toString().c_str(), 
								param.c_str(), 
								value.c_str() 	);
				}
				else
				{
					sprintf(	url,
								"http://%s/command/%s?%s=%s", 
								cams[id]->cam.getIP().toString().c_str(), 
								n->getParam("set").c_str(), 
								param.c_str(),
								value.c_str() 	);
				}

				//ens assegurĂ©m que sigui a la cam
				setConfigParamCamera(url, id); 
			}
		}
	}

#ifdef DEBUG
	cout << "<----- SonyCP:loadDevConfig2" <<  endl;
#endif
}

/******************************************************************
 * translateCam -- Traslada de la Cámara a la Base de datos. 	 	*
 *																						*
 *				param -- Parámetro que queremos trasladar.				*
 *				value -- Valor del parámetro.									*
 *				model -- Modelo del dispositivo.								*
 ******************************************************************/
string SonyControlPlugin::translateCam( string param, string value, int model ) 
{
#ifdef DEBUG
	cout << "-----> SonyCP:translateCam: " << param << ", "
														<< value << ", "
														<< model << endl;
#endif

	// Codec en uso por la cámara
	if ( param == "Fourcc" )
	{
		list<string> l=StrUtils::split(value,"-"); //multicodec
		string fourcc("");

		for(	list<string>::iterator it = l.begin(), end = l.end(); 
				it != end; ++it)
		{
			if (	( (*it) == "mpeg4"	) || 
					( (*it) == "1"	)	)
			{
				fourcc += ",mp4v";
			}
				
			else if ( (*it) == "0" )
			{
				fourcc += ",jpeg";
			}
			else
			{
				fourcc += "," + (*it);
			}
		}

		if(fourcc.length() >= 1)
		{
			fourcc = fourcc.substr(1);
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> Fourcc: " << fourcc << endl;
#endif
		return fourcc;
	}

	// Estándar de vídeo usado por la cámara
	else if(param==string("Standard"))
	{
		string standard("");

		// PAL
		if 		(	( value == "pal" ) 	|| 
						( value == "1" )	)
		{
			standard = "PAL";
		}
		// NTSC
		else if 	(	( value == "ntsc" ) 	|| 
						( value == "0" ) 	)
		{
			standard = "NTSC";
		}
		// OTRO
		else
		{
			standard = value;
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> Standard: " << standard << endl;
#endif
		return standard;
	}

	// Resolución usada por la cámara
	else if ( param == "Resolution" )
	{
		int pos = value.find(",");

		string resolution("");
		string val1("");
		string val2("");

		if ( pos != string::npos )
		{
			val1 = value.substr(0, pos);
			val2 = value.substr(pos+1);
		}

		int p = atoi(val1.c_str());
		int q = atoi(val2.c_str());

		if(model == SNT_V704)
		{
			switch(p)
			{
				//Firmware V2.0
				case 0:
				case 3:
					resolution = "720,576";
					break;
				case 1:
				case 4:
					resolution = "640,480";
					break;
				case 2:
					resolution = "320,240";
					break;
				default:
					resolution = "640,480";
					break;
			}
		} // Model == SNT_V704

		else
		{
			switch(p)
			{
				case 0:
				case 1:
				case 2:
					resolution = "736,544.763,480"; //pal.ntsc
					break;
				case 8:
					resolution = "altParam"; //continuem la guarrada de l'altParam
					break;
				case 3:
				case 4:
				case 5:
				case 640:
					resolution = "640,480";
					break;
				case 608:
					resolution = "608,456";
					break;
				case 576:
					resolution = "576,432"; 
					break;
				case 544:
					resolution = "544,408";
					break;
				case 512:
					resolution = "512,384"; 
					break;
				case 480:
					resolution = "480,360";
					break;
				case 448:
					resolution = "448,336"; 
					break;
				case 416:
					resolution = "416,312";
					break;
				case 384:
					resolution = "384,288"; 
					break;
				case 352:
					resolution = "352,264";
					break;
				case 6:
				case 320:
					resolution = "320,240"; 
					break;
				case 288:
					resolution = "288,216";
					break;
				case 256:
					resolution = "256,192";
					break;
				case 224:
					resolution = "224,168"; 
					break;
				case 192:
					resolution = "192,144";
					break;
				case 7:
				case 160:
					resolution = "160,120"; 
					break;
				default:
				{
					if(q >= 120) // resolucio "minima"
					{
						resolution =	StrUtils::decToString(p) + 
											"," + 
											StrUtils::decToString(q);
					}
					else
					{
						resolution = "640,480"; //millor que res
					}
					break;
				} // default
			} // switch
		} // else

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> Resolution: " << resolution << endl;
#endif
		return resolution;
	}

	// Ángulo de montaje de la cámara
	else if ( param == "MountAngle" )
	{
		string mountangle("");

		if (	( value == "on" )			||
				( value == "desktop")	||
				( value == "8101046602FF" )	)
		{
			mountangle = "180";
		}
		else
		{
			mountangle = "0";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> MountAngle: " << mountangle << endl;
#endif
		return mountangle;
	}

	// Imágenes por segundo proporcionadas por la cámara
	else if ( param == "FPS" )
	{
		string fps("");

		if (	( value == "fastest" )	||
				( value == "0"	)	)
		{
			fps = "25";
		}
		else
		{
			fps = "0";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> FPS: " << fps << endl;
#endif
		return fps;
	}

	// Sensibilidad de la cámara
	else if 	( param == "Sensivity" )
	{
		string sensivity("");

		switch(model)
		{
			case SNC_RZ30:
			{
				sensivity = StrUtils::floatToString(atof(value.c_str())/8.0);
				break;
			}
			case SNC_RZ25:
			{
				sensivity = StrUtils::floatToString(atof(value.c_str())/10.0);
				break;
			}
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> Sensivity: " << sensivity << endl;
#endif
		return sensivity;
	}

	// Tiempo de detección de la cámara
	else if ( param == "DetectionTime" )
	{
		string detectiontime("");

		if(value.size() >= 6)
		{
			detectiontime = 	value.substr(0,2) + ":" + 
									value.substr(2,2) + ":" + 
									value.substr(4,2);
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> DetectionTime: " << detectiontime << endl;
#endif
		return detectiontime;
	}

	// Umbral proporcionado por la cámara
	else if ( param == "Threshold" )
	{
		string threshold("");

		switch(model)
		{
			case SNC_RZ30:
			case SNC_Z20:
			case SNC_CS3:
			{
				threshold = StrUtils::floatToString(atof(value.c_str())/255.0);
				break;
			}
			case SNC_RZ25:
			case SNC_P1:
			case SNC_CS11:
			case SNC_DF50:
			{
				threshold = StrUtils::floatToString(atof(value.c_str())/1000.0);
				break;
			}
			case SNC_RZ50:
			case SNC_RX550:
			case SNC_CS50:
			{
				list<string> par=StrUtils::split(value, string(","));
				float res=0;
				for(	list<string>::iterator param = par.begin(), pEnd = par.end(); 
						param != pEnd; ++param)
				{
					res += (atof(param->c_str())/99.0);
				}

				threshold = StrUtils::floatToString(res/par.size());
				break;
			}
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> Threshold: " << threshold << endl;
#endif
		return threshold;
	}

	// Modo de la cámara (Detección de objetos)
	else if ( param == "Mode" )
	{
		string mode("");

		if ( value == "mod" )
		{
			mode = "Moving Object Detection";
		}
		else if ( value == "uod" )
		{
			mode = "Unattended Object Detection";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> Mode: " << mode << endl;
#endif
		return mode;
	}

	// Estado de la cámara (día / noche)
	else if ( param == "State" )
	{
		string state("");

		if ( value == "on" )
		{
			state = "night";
		}
		else if(value=="off")
		{
			state = "day";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> State: " << state << endl;
#endif
		return state;
	}

	// Detección de movimiento
	else if ( param == "MotionDetectionEnable")
	{
		string mde("");

		if(value=="on")
		{
			mde = "true";
		}
		else
		{
			mde = "false";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> MotionDetectionEnable: " << mde << endl;
#endif
		return mde;
	}

#ifdef DEBUG
		cout << "<----- SonyCP:translateCam -> NoParam: " << value << endl;
#endif
	return value;
}

/******************************************************************
 * translateCam -- Traslada de la base de datos a la cámara. 	 	*
 *																						*
 *				param -- Parámetro que queremos trasladar.				*
 *				value -- Valor del parámetro.									*
 *				model -- Modelo del dispositivo.								*
 ******************************************************************/
string SonyControlPlugin::translateDB( string param, string value, int model ) 
{
#ifdef DEBUG
		cout << "-----> SonyCP:translateDB: " << 	param << ", " << 
																value << ", " <<
																model << endl;
#endif

	// Códec usado por la cámara (JPEG, MPEG4, H264)
	if ( param == "Fourcc" )
	{
		list<string> l=StrUtils::split(value,","); //multicodec
		string fourcc("");

		for(	list<string>::iterator it=l.begin(), itEnd = l.end(); 
				it != itEnd; ++it)
		{
			
			// Códec SNT_V704
			if ( model == SNT_V704 )
			{
				// MPEG4
				if 		(	(*it) == "mp4v" )
				{
					fourcc += "-1";
				}
				// JPEG
				else if 	( 	(*it) == "jpeg"	)
				{
					fourcc += "-0";
				}
			}

			// Cualquier otro modelo
			else
			{
				// MPEG4
				if (	(*it) == "mp4v" )
				{
					fourcc += "-mpeg4";
				}
				// Cualquier otro Códec
				else
				{
					fourcc += "-" + (*it);
				}
			}
		}

		if(fourcc.size() >= 1)
		{
			fourcc = fourcc.substr(1);
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> Fourcc: " << fourcc << endl;
#endif
		return fourcc;
	}

	// Estándar de vídeo usado por la cámara (PAL / NTSC)
	else if ( param == "Standard" )
	{
		string standard("");

		// PAL
		if 		( value == "PAL" )
		{
			standard = (	( model == SNT_V704 ) ? "1" : "pal" );
		}
		// NTSC
		else if 	( value == "NTSC")
		{
			standard = (	( model == SNT_V704 ) ? "0" : "ntsc" );
		}
		// OTRO
		else
		{
			standard = value;
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> Standard: " << standard << endl;
#endif
		return standard;
	}

	// Resolución usada por la cámara
	else if ( param == "Resolution" )
	{
		int pos=value.find(",");
		string resolution("");

		if ( pos != string::npos)
		{
			value = value.substr(0, pos);
		}

		int p = atoi(value.c_str());

		switch(model)
		{
			case SNC_P1:
				resolution = value + ",1";
				break;
			case SNT_V704:
				switch(p)
				{
					case 704:
					case 720:
						resolution = "3";
						break;
					case 640:
						resolution = "4";
						break;
					case 320:
						resolution = "2";
						break;
					default: //??
						resolution = "4";
						break;
				}
			case SNC_CS50:
				switch(p)
				{
					case 704:
					case 720:
						resolution = "720,2";
						break;
					case 640:
						resolution = "640,2";
						break;
					case 320:
						resolution = "320,2";
						break;
					case 160:
						resolution = "160,2";
						break;
					default: //??
						resolution = "640,2";
						break;
				}
			default:
				switch(p)
				{
					case 736:
					case 763:
						resolution = "2"; // 0, 1, 2
						break;
					case 640:
						resolution = "5"; //3, 4, 5, 640,0
						break;
					case 320:
						resolution = "6"; //6, 320,0
						break;
					case 160:
						resolution = "7"; //7, 160,0
						break;
					default:
						resolution = "8"; //7, 160,0
						break;
				}
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> Resolution: " << resolution << endl;
#endif
		return resolution;
	}

	// Sensibilidad usada por la cámara
	else if ( param == "Sensitivity" )
	{
		string sensivity("");

		switch(model)
		{
			case SNC_RZ30:
				sensivity = StrUtils::decToString((int)(atof(value.c_str())*8.0));
				break;
			case SNC_RZ25:
				sensivity = StrUtils::decToString((int)(atof(value.c_str())*10.0));
				break;
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> Sensivity: " << sensivity << endl;
#endif
		return sensivity;
	}

	// Umbral en uso por la cámara
	else if ( param == "Threshold" )
	{
		string threshold("");

		switch(model)
		{
			case SNC_RZ30:
			case SNC_Z20:
			case SNC_CS3:
				threshold = StrUtils::decToString((int)(atof(value.c_str())*255.0));
				break;
			case SNC_RZ25:
			case SNC_P1:
			case SNC_CS11:
			case SNC_DF50:
				threshold = StrUtils::decToString((int)(atof(value.c_str())*1000.0));
				break;
			case SNC_RZ50:
			case SNC_RX550:
			case SNC_CS50:
				threshold = StrUtils::decToString((int)(atof(value.c_str())*99.0));
				break;
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> Threshold: " << threshold << endl;
#endif
		return threshold;
	}

	// Tiempo de detección
	else if( param == "DetectionTime" )
	{
		int pos;
		while((pos = value.find(string(":"))) != string::npos)
		{
			value = value.substr(0,pos) + value.substr(pos+1);
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> DetectionTime: " << value << endl;
#endif
		return value;
	}

	// Modo usado por la cámara (Detección de movimiento)
	else if ( param == "Mode" )
	{
		string mode("");

		if(value=="Moving Object Detection")
		{
			mode = "mod";
		}
		else if(value=="Unattended Object Detection")
		{
			mode = "uod";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> Mode: " << mode << endl;
#endif
		return mode;
	}

	// Ángulo de montaje usado por la cámara (0 / 180 grados)
	else if ( param == "MountAngle" )
	{
		string mountangle("");
		int p = atoi(value.c_str());

		switch(model)
		{
			case SNC_RZ50:
			case SNC_RZ25:
			case SNC_RX550:
			case SNC_CS11:
			case SNC_P1:
			case SNC_Z20:
				if(p > -90 && p <= 90)
				{
					mountangle = "off";
				}
				else
				{
					mountangle = "on";
				}
				break;
			case SNC_RZ30:
			case SNC_CS3:
				if(p > -90 && p <= 90)
				{
					mountangle = "ceiling";
				}
				else 
				{
					mountangle = "desktop";
				}
				break;
			case SNC_CS50:
				break;
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> MountAngle: " << mountangle << endl;
#endif
		return mountangle;
	}

	// Estado usado por la cámara (modo día / noche)
	else if ( param == "State" )
	{
		string state("");

		if 		( value == "day" )
		{
			state = "off";
		}
		else if 	( value == "night" )
		{
			state = "on";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> State: " << state << endl;
#endif
		return state;
	}

	// Imágenes por segundo recuperadas desde la cámara
	else if ( param == "FPS" )
	{
		string fps("");

		if (	( model == SNT_V704 ) && 
				( value == "25" )		)
		{
			fps = "0";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> FPS: " << fps << endl;
#endif
		return fps;
	}

	// Detección de movimiento (activido / desactivado)
	else if ( param == "MotionDetectionEnable" )
	{
		string mde("");

		if ( value == "true" )
		{
			mde = "on";
		}
		else
		{
			mde = "off";
		}

#ifdef DEBUG
		cout << "<----- SonyCP:translateDB -> MotionDetectionEnable: " << mde << endl;
#endif
		return mde;
	}

#ifdef DEBUG
	cout << "<----- SonyCP:translateDB -> NoParam: " << value << endl;
#endif
	return value;
}

/******************************************************************
 * translateCam -- "Suma" un path real y uno relativo.				*
 *																						*
 *				path -- Path real.												*
 *				relPath -- Path relativo.										*
 ******************************************************************/
string SonyControlPlugin::addPath( string path, string relPath ) 
{
#ifdef DEBUG
		cout << "-----> SonyCP:addPath: " << path << ", " << relPath << endl;
#endif

	if ( 	( path.size() > 0 ) 	&& 
			( path[path.size() - 1] == '/' )	)
	{
		path = path.substr(0, path.size() - 1);
	}

	while ( relPath.find("../") == 0 )
	{
		relPath = relPath.substr(3);
		int pos = path.rfind("/");
		if(pos != string::npos)
		{
			path = path.substr(0, pos);
		}
	}

#ifdef DEBUG
		cout << "<----- SonyCP:addPath" << endl;
#endif

	return path + "/" + relPath;
}

/******************************************************************
 * execute -- "Suma" un path real y uno relativo.	(???)				*
 *																						*
 *				thid -- Id del thread (supongo).								*	
 *				parm -- Parámetro (supongo).									*
 ******************************************************************/
void *SonyControlPlugin::CamInit::execute( int thid, void *parm )
{
#ifdef DEBUG
		cout << "-----> SonyCP:CamInit:execute: " << thid << endl;
#endif

	execInfo *i=(execInfo*) parm;

	cout<<" "<<thid<<"   0 CamInit::execute i:"<<(void*)i<<endl;
	int id=i->id;
	cout<<" "<<thid<<"   0.5 CamInit::execute scp:"<<(void*)i->scp<<endl;
	cout<<" "<<thid<<"  ++++++  0.5 CamInit::execute cam:"<<id<<" :"<<(void*)i->scp->cams[id]<<endl;
	if(i->scp->cams[id]->initing==false)
	{
		i->scp->cams[id]->initing=true;

		try
		{
			XML *conf=i->conf; 
			string model=i->model; bool reload=i->reload;

			cout<<" "<<thid<<"   1 CamInit::execute "<<id<<":"<<model<<endl;
			i->scp->loadDBConfig(id, conf, model, reload);
			i->scp->cams[id]->inited=true;
			i->scp->cams[id]->error_logged=false;
			cout<<" "<<thid<<"   2 CamInit::loaded config: "<<id<<":"<<model<<endl;
		}
		catch(Exception &e){
			cout<<" ---------- --------- ------------- SCP::CamInit::exec: "<<i->id<<":"<<i->model<<" Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		}

		i->scp->cams[id]->initing=false;

	}
	delete i;
	i = NULL;
	cout<<" "<<thid<<"  ++++++  return CamInit::execute cam:"<<id<<endl;

#ifdef DEBUG
		cout << "<----- SonyCP:execute" << endl;
#endif

	return NULL;
}

/******************************************************************
 * loadDBConfig -- Carga la configuración de la base de datos.		*
 *																						*
 *				id -- Id del dispositivo. 										*	
 *				conf -- XML (???)													*
 *				model -- Modelo del dispositivo que queremos cargar.	*
 *				reload -- Recargar (¿qué?)										*
 ******************************************************************/
void SonyControlPlugin::loadDBConfig(	CPDeviceID id, 
													XML *conf, 
													string model, 
													bool reload 	)
{
#ifdef DEBUG
		cout << "-----> SonyCP:loadDBConfig: " << dec 	<< id.id << ", " 
																		<< model << ", " 
																		<< reload << endl;
#endif

	Address cam = cams[id.id]->cam;
	string modelStr=cams[id.id]->modelStr;
	cout<<"loadDBCofig:"<< cam.toString()<<"::"<<id.id<<"::"<<model<<endl;
	
	//
	//TODO: canviar el SCPDevConfig pq fagi be els streams
	if(model==string("SNT-V704") || model==string("SNT-V704-L9EMB"))
	{
		char url[256];
		sprintf(url,"http://%s/command/inquiry.cgi?inq=video%i", cams[id.id]->cam.getIP().toString().c_str(),cams[id.id]->streamId+1);
		string parm=string("Vi")+StrUtils::decToString(cams[id.id]->streamId+1)+string("ImageSize");
		string bdy=getConfigParamCamera(url, parm, model, id.id);
		string setCodec("");

		if(bdy==string("0"))
			setCodec=string("3");
		else if(bdy==string("1"))
			setCodec=string("4");

		if(setCodec!=string(""))
		{
			sprintf(url,"http://%s/command/videoset.cgi?ChannelNO=%i&ImageSize=%s", cams[id.id]->cam.getIP().toString().c_str(), cams[id.id]->streamId+1, setCodec.c_str());

			setConfigParamCamera(url, id.id);
		}
	}

	DBGateway *dbLoad = NULL;
	try
	{
		if(reload)
		{
			cout<<"deleteDevCfg"<<endl;
			try
			{
				if(dbLoad==NULL)
					dbLoad = new DBGateway(this->address, type, cn);
				dbLoad->deleteDeviceConfig(id.id, "/");
			}
			catch(Exception &e)
			{
				cout<<"db.deleteDevCfg Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
			}
			cout<<"/deleteDevCfg"<<endl;
		}
	}
	catch(Exception &e)
	{
		cout<<"  SCP::CamInit::exec: "<<id.id<<"  deleteDevConfig Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
	}

	if(dbLoad==NULL)
		dbLoad = new DBGateway(this->address, type, cn);
	bool dbCheck=true;
	{
		bool xfound=false, yfound=false, fourccfound=false, fpsfound=false, qualFound=false, brFound=false;

		int cid=dbLoad->getDeviceConfigNode(id.id, "/Video");




		if(cid==-1)
		{


			cout<<" error while checking params in db for cam:"<<id.id<<" "<<cams[id.id]->cam.toString()<<endl;
			dbCheck=false;
		}
		else
		{



			string s = string("SELECT * FROM getconfigparamrecursive(")+StrUtils::decToString(cid)+string(",'')"); 
			XML *res=dbLoad->call(s, 0);
			xmlNode *n=res->getNode("/result");




			if(n==NULL)
			{
				cout<<" error while checking params in db for cam:"<<id.id<<" : "<<res->toString()<<endl;
			}
			else
			{







				int nrows=n->getKidList().size();

				for(int i=0; i<nrows; ++i)
				{
					string value= res->getNodeData("/result/["+StrUtils::decToString(i)+"]/value");
					string name = res->getNodeData("/result/["+StrUtils::decToString(i)+"]/name");

					if(value == string(""))
						continue;
					if(name.find(string("x")) != string::npos && value != string("") && value != string("0"))
						xfound=true;
					else if(name.find(string("y")) != string::npos && value != string("") && value != string("0"))
						yfound=true;
					else if(name.find(string("Fourcc")) != string::npos && value != string("") && value != string("0"))
						fourccfound=true;
					else if(name.find(string("FPS")) != string::npos && value != string("") && value != string("0"))
						fpsfound=true;
					else if(name.find(string("Quality")) != string::npos && value != string("") && value != string("0"))
						qualFound=true;
					else if(name.find(string("Bitrate")) != string::npos && value != string("") && value != string("0"))
						brFound=true;

				}
				dbCheck = xfound &&  yfound &&  fourccfound &&  fpsfound &&  (qualFound ||  brFound);
				if(!dbCheck)
					cout<<"Camera config not foud, reloading : dbCheck = x:"<<xfound<<" && y:"<< yfound<<" && fcc:"<< fourccfound<<" && fps:"<< fpsfound<<" && ( ql:"<<qualFound<<" ||  br:"<<brFound<<")"<<endl;
			}
		}
	}

	if(reload || !dbCheck)
	{
		cout <<" ******************************* recarreguem config device:"<<id.id<<endl;
	 	if(dbLoad==NULL)
			dbLoad = new DBGateway(this->address, type, cn);
		try
		{
			loadDevConfig(id.id, model, "", "", conf, dbLoad);
		}
		catch(Exception e)
		{
			cout<<"CamInit::loadDevConfig 2 - cam:"<<id.id<<" db:"<<(void*)dbLoad<<" "<<e.getClass()<<"::"<<e.getMsg()<<endl;
		}

		cout<<" LoadDBConfig::::"<<id.id<<"::"<<model<<endl;

		try
		{
			if(dbLoad->getDeviceConfigParam(id.id, string("/Presets"))==string(""))
				dbLoad->setDeviceConfigParam(id.id, string("/Presets"), string("1"));

			if(dbLoad->getDeviceConfigParam(id.id, string("/Schedules"))==string(""))
				dbLoad->setDeviceConfigParam(id.id, string("/Schedules"), string("1"));

			if(dbLoad->getDeviceConfigParam(id.id, string("/Tours"))==string(""))
				dbLoad->setDeviceConfigParam(id.id, string("/Tours"), string("1"));

			dbLoad->setDeviceConfigParam(id.id, string("/Output/1"), "0");
		}
		catch(Exception e)
		{
			cout<<"CamInit::exec 3 - cam:"<<id.id<<"  "<<e.getClass()<<"::"<<e.getMsg()<<endl;
		}
	}

	if(dbLoad!=NULL)
	{
		delete dbLoad;
		dbLoad = NULL;
	}

	cout<<"SonyCP::config loaded:"<<id.id<<endl;

#ifdef DEBUG
	cout << "<----- SonyCP:loadDBConfig" << endl;
#endif
}

/******************************************************************
 * getParamValue -- Devuelve el valor de un parámetro en un 		*
 *				string del estilo:												*
 *						"param1=valor1&param2=valor2&param3=....."		*
 *																						*
 *				llista -- La lista de parámetros.							*
 *				params -- los parámetros que nos interesan.				*
 ******************************************************************/
string SonyControlPlugin::getParamValue( string llista, string params )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getParamValue: " 	<< llista << ", " 
															<< params << endl;
#endif

	string response("");
	bool found_param = false;

	list<string> paramsLlista = StrUtils::split(llista, string("&"));
	list<string> par = StrUtils::split(params, string(","));
	list<string>::iterator it, itEnd;

	for(	it = paramsLlista.begin(), itEnd = paramsLlista.end(); 
			it != itEnd; ++it)
	{
		for(	list<string>::iterator param = par.begin(), paramEnd = par.end(); 
				param != paramEnd; ++param)
		{
			if(it->find((*param)+string("=")) == 0) 
			{
				response = it->substr( 	param->size() + 1 ,
												it->size() - (param->size() + 1 ) );
				found_param = true;
				break;
			}

		}
		if (found_param)
		{
			break;
		}	
	}

#ifdef DEBUG
	cout << "<----- SonyCP:getParamValue" << endl;
#endif
	return response;
}

/******************************************************************
 * getConfigParamCamera -- Devuelve el valor de un parámetro 		*
 * 			consultado directamente a la cámara.						*
 *																						*
 *				url -- URL del dispositivo.									*
 *				param -- Parámetro que nos interesa.						*
 *				pModel -- Modelo del dispositivo. (???)					*
 *				devId -- Identificador del dispositivo.					*
 ******************************************************************/
string SonyControlPlugin::getConfigParamCamera(	string url, 
																string param, 
																string pModel, 
																int devId 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:getConfigParamCamera: " 	<< url 		<< ", " 
																	<< param 	<< ", " 
																	<< pModel	<< ", "
																	<< devId 	<< endl;
#endif

	HttpStream *stream = NULL;

	try
	{
		HttpClient cl(url);
		cl.setHeader("Authorization", this->getBasicAuthStrForDevice(devId));
		HttpStream * stream=cl.sendRequest(string("GET"));

		stream->getNextInformationChunk();
		string body = stream->getNextInformationChunk();

		delete stream;
		stream = NULL;

#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParamCamera" << endl;
#endif
		return getParamValue(body, param);

	}catch(SocketTimeoutException &e)
	{
		if(stream!=NULL)
		{
			delete stream;
			stream = NULL;
		}

#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParamCamera" << endl;
#endif
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}
}

/******************************************************************
 * setConfigParamCamera -- (???)												*
 *																						*
 *				url -- URL del dispositivo.									*
 *				devId -- Identificador del dispositivo.					*
 ******************************************************************/
void SonyControlPlugin::setConfigParamCamera( string url, int devId )
{
#ifdef DEBUG
	cout << "-----> SonyCP:setConfigParamCamera: " 	<< url 		<< ", " 
																	<< devId 	<< endl;
#endif
	HttpStream * stream=NULL;

	try
	{
		HttpClient cl(url);
		cl.setHeader("Authorization", this->getBasicAuthStrForDevice(devId));
		stream=cl.sendRequest(string("GET"));

		stream->getNextInformationChunk();

		delete stream;
		stream = NULL;

	}
	catch(SocketTimeoutException &e)
	{
		if(stream!=NULL)
		{
			delete stream;
			stream = NULL;
		}

#ifdef DEBUG
	cout << "<----- SonyCP:setConfigParamCamera" << endl;
#endif
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}

#ifdef DEBUG
	cout << "<----- SonyCP:setConfigParamCamera" << endl;
#endif
}

/******************************************************************
 * sendMoveCommand -- Envía un comando de movimiento a un			*
 *					un dispositivo.												*
 *																						*
 *				url -- URL del dispositivo.									*
 *				devId -- Identificador del dispositivo.					*
 *				slowMove -- (???)													*
 ******************************************************************/
void SonyControlPlugin::sendMoveCommand( string url, int devId, bool slowMove )
{
#ifdef DEBUG
	cout << "-----> SonyCP:sendMoveCommand: " 	<< url 	<< ", " 
																<< devId << ", "
																<< slowMove << endl;
#endif	
	HttpStream *stream=NULL;
	int retries=0;
	do
	{
		try{
			cout<<"send:"<<url<<endl;
			HttpClient cl(url);
			cl.setHeader("Authorization", this->getBasicAuthStrForDevice(devId));
			if(slowMove)
			{
				cl.setConnectionTimeout(20000);
			}
			stream=cl.sendRequest(string("GET"));

			stream->getNextInformationChunk();

			delete stream;
			stream = NULL;

			break;
		}catch(SocketTimeoutException &e)
		{
			if(stream!=NULL)
			{
				delete stream;
				stream=NULL;
			}	
			++retries;
			if(retries>5)
			{
#ifdef DEBUG
	cout << "<----- SonyCP:sendMoveCommand" << endl;
#endif
				throw;
			}
		}
	}
	while(slowMove);//espera resultat nomes si ve d'un tour

#ifdef DEBUG
	cout << "<----- SonyCP:sendMoveCommand" << endl;
#endif
}


/******************************************************************
 * serve -- (???)																	*
 ******************************************************************/
void SonyControlPlugin::serve()
{
#ifdef DEBUG
	cout << "-----> SonyCP:serve" << endl;
#endif

	Plugin::serve();

#ifdef DEBUG
	cout << "<----- SonyCP:serve" << endl;
#endif
}

/******************************************************************
 * startSession -- Inicia sesión con un dispositivo.					*
 *																						*
 *			id -- Identificador del dispositivo.							*
 *			a -- Dirección de la base de datos.								*
 ******************************************************************/
void SonyControlPlugin::startSession( CPDeviceID id, Address *a )
{	
#ifdef DEBUG
	cout << "-----> SonyCP:startSession: " << dec << id.id << endl;
#endif

	map<string,SessionDispatcher>::iterator itSes=sessions.find(a->toString());
	bool sessionEnded=false;
	int oldSesID;

	cout<<"SonyCP::StartSession firstSession 1 ID: "<< id.id  <<endl;
	if(itSes!=sessions.end())
	{
		if(id.id ==itSes->second.getIdNumber())
		{
			cout<<"SonyCP::StartSession firstSession 2 ID: "<< id.id  <<endl;
			throw(CPSessionAlreadyStablishedException(1, string(" Sessio ja establerta")));
		}
		else
		{
			cout<<"SonyCP::StartSession firstSession 3 ID: "<< id.id  <<endl;
			//tenia sessiĂł amb un altre dispositiu -> tanquĂ©m i iniciem la nova 
			oldSesID=itSes->second.getIdNumber(); 
			sessionEnded=true;
			try{
				cout<<"SonyCP::StartSession firstSession 4 ID: "<< id.id  <<endl;
				endSession(itSes->second.getIdNumber(), a);
			}catch(Exception &e){}
		}
	}

	map<int, CamInfo*>::iterator itBuff=cams.find(id.id);

	Address cam;

	if(itBuff!=cams.end())
	{
		cout<<"SonyCP::StartSession cam found "<<(void*) itBuff->second <<endl;
		bool firstSession=itBuff->second->firstSession;

		cam=itBuff->second->cam;

		// Si no estan iniciats els threads, que s'iniciin
		cams[id.id]->startThreads();

		if(firstSession)
		{
			cout<<"SonyCP::StartSession firstSession for ID:" << id.id  << endl;
			ModelInfo *mi = models[cams[id.id]->modelStr];
			RPC *rpc=NULL;
			try
			{
				if(isnan(mi->maxx))
				{
					RPC *rpc=db.getRPC(a,5);
					if(isnan(mi->minx))
						mi->minx = atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Limits/Min/x"), rpc).c_str());
					if(isnan(mi->miny))
						mi->miny = atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Limits/Min/y"), rpc).c_str());
					if(isnan(mi->minz))
						mi->minz = atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Limits/Min/z"), rpc).c_str());

					if(isnan(mi->maxx))
						mi->maxx = atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Limits/Max/x"), rpc).c_str());
					if(isnan(mi->maxy))
						mi->maxy = atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Limits/Max/y"), rpc).c_str());
					if(isnan(mi->maxz))
						mi->maxz = atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Limits/Max/z"), rpc).c_str());
					db.freeRPC(rpc);
					rpc=NULL;
				}
				//if(mi->maxx > mi->miny || mi->maxy > mi->miny || mi->maxz > mi->minz)
				if(mi->maxx > mi->minx || mi->maxy > mi->miny || mi->maxz > mi->minz)
				{
					cams[id.id]->tour->checkInit();
					itBuff->second->firstSession=false;
				}
			}catch(Exception &e)
			{
				cout<<"SCP::startSession("<<id.id<<") "<<e.getClass()<<":"<<e.getMsg()<<endl;
				if(rpc!=NULL)
					db.freeRPC(rpc);
			}catch(...)
			{
				cout<<"SCP::startSession("<<id.id<<") unknown exception"<<endl;
				if(rpc!=NULL)
					db.freeRPC(rpc);
			}
		}
	}
	else
	{
		//no es a la llista de cameres, mirem si l'han afegit al sistema des de la inicialitzacio i l'afegim
		cout<<"SonyCP::StartSession:"<<id.id<<" NOT inited"<<endl;
		cout<<"SonyCP::StartSession firstSession 5 ID: "<< id.id  <<endl;
		
		// Separem perque hi ha variables que "col·lisionen"
		{
			RPC *rpc=db.getRPC(a,5);
			string model("");
			char *buf2=NULL;
			try
			{
				cout<<"SCP:: load new Camera:"<<id.id<<endl;
				string s("Select d.id, d.ip, d.puerto, m.codigo FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='SONY' AND d.id=");
				s+=StrUtils::decToString(id.id);
	
				XML *devInfo=db.call(s, 0, rpc);
				xmlNode *n=NULL;
				if(devInfo != NULL)
				{
					cout<<"devInfo!= NULL"<<endl;
					cout<<devInfo->toString()<<endl;
					n=devInfo->getNode("/result/row/ip");
				}
				RPC::keepAlive(a, type);
	
				if(n==NULL) //no es enlloc, dispositiu invalid
				{
					db.freeRPC(rpc);
					cout << "Disp Invalid: Query: " << s << ", xml: " << (devInfo!=NULL?devInfo->toString():"") << endl;
					if(devInfo != NULL)
					{
						delete devInfo;
						devInfo = NULL;
					}
					throw(CPInvalidParamException(0, "Dispositiu invalid"));
				}
				string sip=n->getCdata();
				int pos=sip.find('/');
				if(pos!=string::npos)
				{
					sip=sip.substr(0,pos);
				}
	
				IP ip(sip);
	
				n=devInfo->getNode("/result/row/puerto");
				int port=atoi(n->getCdata().c_str());
				cam=Address(ip, port);
	
				n=devInfo->getNode("/result/row/codigo");
				model=n->getCdata();
	
				delete devInfo;
				devInfo = NULL;
	
				cout<<"SCP:: load new Camera:"<<id.id<<":"<<cam.toString()<<" "<<model<<endl;
	
				s = string("SELECT m.raizinfo FROM modelo m, fabricante f WHERE m.fabricante=f.id AND f.nombre='SONY' AND m.codigo ='") + model + string("'");
				XML *paramNode=db.call(s, 0, rpc);
				RPC::keepAlive(a, type);
				cout<<"SetMCP db Call:"<<paramNode->toString()<<endl;
				n=paramNode->getNode("/result/row/raizinfo");
				if(n->getCdata()==string(""))
				{
					loadModelConfig(model, "/", "/", modelConf);
					checkModelMetadata(model);
				}
				cout<<"SCP:: loaded new Camera"<<endl;
				db.freeRPC(rpc);
			}
			catch(SocketTimeoutException &e)
			{
				cout<<"SCP:: STE -- not loaded new Camera"<<endl;
				db.freeRPC(rpc);
				throw(ControlPluginException(e.getCode(), e.getMsg()));
			}
			catch(Exception &e)
			{
				cout<<"SCP:: E -- not loaded new Camera "<<e.getClass()<<":"<<e.getMsg()<<endl;
				//		gcpr.unlock();
				db.freeRPC(rpc);
				throw(ControlPluginException(0, string("Invalid device id:")+StrUtils::decToString(id.id)));
			}
			catch(...)
			{
				cout<<"SCP:: (...) -- not loaded new Camera"<<endl;
				//		gcpr.unlock();
				db.freeRPC(rpc);
				throw(ControlPluginException(0, string("Invalid device id:")+StrUtils::decToString(id.id)));
			}
	
			cams[id.id]= new CamInfo(id, cam, model, default_buffer_size, this);
			cams[id.id]->startThreads();
		}

		try
		{	



			XML *conf = getConfigFromFile( file );
			string configFile = conf->getNodeData("[0]/DeviceConfigFile");

			delete conf;
			conf = NULL;

		 	//conf = getConfigFromFile( file );

		 	conf = getConfigFromFile( configFile );
			 
			cout << "SCP:: load new Camera::loadDBConfig: " << id.id << endl;

			int idth = ci.start(new CamInit::execInfo(	id.id, 
																		conf, 
																		cams[id.id]->modelStr, 
																		this, 
																		true) );
			ci.detach(idth);
		}
		catch(SocketException &e)
		{
			//no s'ha trobat la camera?
			cams.erase(id.id);
			throw(CPInvalidParamException(0,"No s'ha pogut comunicar amb la camera"));
		}
		catch(...)
		{
			throw(ControlPluginException(0, string("Invalid device id:")+StrUtils::decToString(id.id)));
		}
	}	

	cout<<" StartSession for:"<<a->toString()<<" dev:"<<id.id<<endl;

	SessionDispatcher ses(cam, id);
	sessions[a->toString()]= ses;

#ifdef DEBUG
	cout << "<----- SonyCP:startSession" << endl;
#endif
}

/******************************************************************
 * endSession -- Finaliza la sesión con el dispositivo.				*
 *																						*
 *			id -- Identificador del dispositivo.							*
 *			a -- Dirección de la base de datos.								*
 ******************************************************************/
void SonyControlPlugin::endSession( CPDeviceID id, Address *a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:startSession: " << dec << id.id << endl;
#endif

	map<string,SessionDispatcher>::iterator i=sessions.find(a->toString());
	map<string,SessionDispatcher>::const_iterator iEnd = sessions.end();

	if(i==sessions.end())
	{
		return;
	}

	sessions.erase(i);
	struct timeval now;
	gettimeofday(&now,NULL);
	i = sessions.begin(); 

	while(i!=sessions.end())
	{
		struct timeval diffTime = now - i->second.getTimeOfCreationOfThisSession();
		if ( 	diffTime.tv_sec > sessionTimeout )
			sessions.erase(i++);
		else
			++i;
	}

#ifdef DEBUG
	cout << "<----- SonyCP:endSession" << endl;
#endif
}

/******************************************************************
 * getPTZ -- Llama a la verdadera función para obtener el PTZ de 	*
 *			un dispositivo.														*
 *																						*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/
CPPTZ SonyControlPlugin::getPTZ( Address *a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getPTZ1" << endl;
#endif

	SessionDispatcher s 	= getSession(*a);
	Address cam = s.getIpAddress();

#ifdef DEBUG
	cout << "<----- SonyCP:getPTZ1" << endl;
#endif
	return getPTZ(s.getIdNumber());			
}

/******************************************************************
 * getPTZ -- Obtiene el PTZ de un dispositivo							*
 *																						*
 *			id -- Identificador del dispositivo								*
 ******************************************************************/
CPPTZ SonyControlPlugin::getPTZ( int id )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getPTZ2: " << id << endl;
#endif

	CamInfo *ci = cams[id]; 
	ModelInfo *mi = models[ci->modelStr];
	Address cam=ci->cam;
	float x,y,z;
	RPC *rpc=db.getRPC(NULL,5);
	if (rpc == NULL)
	{
#ifdef DEBUG
	cout << "<----- SonyCP:getPTZ2: (rpc = NULL)" << endl;
#endif
		return CPPTZ();
	}
	float ox=mi->ox, oy=mi->oy, oz=mi->oz;
	float sx=mi->sx, sy=mi->sy, sz=mi->sx, optic=mi->optic;
	try
	{
		if(isnan(ox))
			ox = mi->ox = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Offsets/x"), rpc).c_str());
		if(isnan(oy))
			oy = mi->oy = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Offsets/y"), rpc).c_str());
		if(isnan(oz))
			oz = mi->oz = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Offsets/z"), rpc).c_str());

		if(isnan(sx))
			sx = mi->sx = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Scale/x"), rpc).c_str());
		if(isnan(sy))
			sy = mi->sy = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Scale/y"), rpc).c_str());
		if(isnan(sz))
			sz = mi->sz = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Scale/z"), rpc).c_str());
		if(isnan(optic))
			optic = mi->optic = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Optics/OpticalZoom"), rpc).c_str());
		db.freeRPC(rpc);
	}catch(Exception &e)
	{
		db.freeRPC(rpc);
		throw;
	}

	if(sx==0) sx=1;
	if(sy==0) sy=1;
	if(sz==0) sz=1;

	cout<<"	getPTZ Offsets: "<<ox<<","<<oy<<","<<oz<<"   "<<sx<<","<<sy<<","<<sz<<endl;
	try
	{
		switch (ci->model)
		{
		case SNC_RZ30:
			{
				int zt [24]={0x0000, 0x1781, 0x213B, 0x2752, 0x2F03, 0x315D, 0x3364, 0x34FF, 0x362C, 0x373D,
					0x386A, 0x3929, 0x3A20, 0x3AFA, 0x3BBA, 0x3C5E, 0x3CCB, 0x3D70, 0x3DF8, 0x3E66, 0x3ED3,
					0x3F25, 0x3F93, 0x4000};

				int zp [12]={0x4000, 0x5E00, 0x6800, 0x6D00, 0x7000, 0x7200, 0x7380, 0x7480, 0x7580, 0x7600,
					0x76C0, 0x7700};

				int zn [12]={0x4000, 0x5E80, 0x6880, 0x6DC0, 0x70C0, 0x72C0, 0x7440, 0x7540, 0x7600, 0x76C0,
					0x7740, 0x77C0};

				char url[256];
				sprintf(url,"http://%s/command/visca-inquiry.cgi?visca=81090612FF", cam.getIP().toString().c_str());

				HttpClient cl(url);

				HttpStream *stream=cl.sendRequest(string("GET"));
				stream->getNextInformationChunk();
				string bdy=stream->getNextInformationChunk();

				delete stream;
				stream = NULL;

				char p[5]={bdy[5],bdy[7],bdy[9],bdy[11],0};
				char t[5]={bdy[13],bdy[15],bdy[17],bdy[19],0};
				short pan = strtol(p,NULL,16);
				short tilt = strtol(t,NULL,16);
				if(tilt<=0)
					y = (tilt*(67.5/(0xFFFF-0xFCC4)));
				else
					y = (tilt*(47.5/0x03CC));

				if(pan<=0)
					x=(pan*(170.0/(0xFFFF-0xF670)));
				else
					x=(pan*(170.0/0x0990));

				sprintf(url,"http://%s/command/visca-inquiry.cgi?visca=81090447FF", cam.getIP().toString().c_str());
				HttpClient clz=HttpClient(url);
				HttpStream *strz=clz.sendRequest(string("GET"));
				strz->getNextInformationChunk();
				bdy=strz->getNextInformationChunk();

				delete strz;
				strz = NULL;

				char zv[5]={bdy[5],bdy[7],bdy[9],bdy[11],0};
				short zoom = strtol(zv,NULL,16);
				int idx;
				z=0;
				for(idx=0; idx<24; ++idx)
				{
					if(zt[idx]<=zoom)
						z=idx;
					else
						break;
				}
				idx=(int)z;
				if(z<24 && zoom>zt[idx])
				{
					z += ((float)(zoom-zt[idx])) / ((float)(zt[idx+1]-zt[idx]));
					++z;
				}
				else if(z==24 && zoom>zt[idx])
				{
					int *zv; 
					if(db.getDeviceConfigParam(id, string("/Video/Standard")) == string("PAL"))
						zv=zp;
					else
						zv=zn;

					for(idx=0; idx<12; ++idx)
					{
						if(zv[idx]<=zoom)
							z=idx;
						else
							break;
					}
					idx=(int)z;
					if(z<12 && zoom>zv[idx])
					{
						z += ((float)(zoom-zv[idx])) / ((float)(zv[idx+1]-zv[idx]));
					}
					++z;
					z *= 24;
				}

				x=-x;
				y=-y;
				break;
			}
		case SNC_RZ25:
		case SNC_RZ50:
		case SNC_RX550:
			{

				char url[256];
				sprintf(url,"http://%s/command/inquiry.cgi?inq=ptzf", cam.getIP().toString().c_str());

				string bdy=getConfigParamCamera(url, "AbsolutePTZF", "", id);
							cout<<" Get:"<<bdy<<endl;

				if(bdy.length()<14)
					break;
				short pan=strtol(bdy.substr(0,4).c_str(),NULL,16);
							cout<<" Get pan:"<<bdy.substr(0,4)<<" short "<<pan<<endl;

				short tilt=strtol(bdy.substr(5,4).c_str(),NULL,16);
							cout<<" Get tilt:"<<bdy.substr(5,4)<<" short "<<tilt<<endl;

				short zoom=strtol(bdy.substr(10,4).c_str(),NULL,16);

				if(ci->model==SNC_RX550)
				{
					x=((float)pan*(360.0/ ((float)0x3FBF)));

					if(tilt>0)
						y = ((float)tilt*(48.0/(float)0x0880));
					else
						y = ((float)tilt*(48.0/(2175.0)));

					cout <<"	x:"<<x<<" : "<<pan<<" = 0x"<<hex<<pan<<dec<<endl; 
					cout <<"	y:"<<y<<" : "<<tilt<<" = 0x"<<hex<<tilt<<dec<<endl; 
					unsigned short zt[26]={ 
						0x0000, 0x1760, 0x214C, 0x2722, 0x2B22, 0x2E20, 0x3080, 0x3278, 0x3426, 0x359E, 0x36EE, 0x381C, 0x392E,
						0x3A26, 0x3B08, 0x3BD4, 0x3C8C, 0x3D2E, 0x3DBC, 0x3E58, 0x3EA2, 0x3F00, 0x3F4E, 0x3F92, 0x3FCC, 0x4000};

						unsigned short zd[12]={0x4000, 0x6000, 0x6A80, 0x7000, 0x7300, 0x7540, 0x76C0, 0x7800, 0x78C0, 0x7980, 0x7A00, 0x7AC0};

						int idx;
						z=0;
						for(idx=0; idx<26; ++idx)
						{
							if(zt[idx]<=zoom)
								z=idx;
							else
								break;
						}
						idx=(int)z;
						++z;
						if(z<26 && zoom>zt[idx])
						{
							z+=((float)(zoom-zt[idx])) / ((float)(zt[idx+1]-zt[idx]));
						}
						else if(z==26 && zoom>zt[idx])
						{
							for(idx=0; idx<12; ++idx)
								if(zd[idx]<=zoom)
								{
									z=idx;
								}
								else
								{
									break;
								}
								idx=(int)z;
								if(z<12 && zoom>zd[idx])
									z+=((float)(zoom-zd[idx])) / ((float)(zd[idx+1]-zd[idx]));
								++z;
								z*=26;
						}
				}
				else // SNC_RZ25  SNC_RZ50
				{
					if(pan<=0)
						x = (pan*(170.0/(0xFFFF-0xF670)));
					else
						x = (pan*(170.0/0x0990));


					if(tilt<=0)
						y = (tilt*(67.5/(0xFFFF-0xFCC4)));
					else
						y = (tilt*(47.5/0x03CC));

					int zt[26]={ 0x0000, 0x1760, 0x214C, 0x2722, 0x2B22, 0x2E20, 0x3080, 0x3278, 0x3426, 0x359E,
						0x36EE, 0x381C, 0x392E, 0x3A26, 0x3B08, 0x3BD4, 0x3C8C, 0x3D2E, 0x3DBC, 0x3E58, 0x3EA2, 
						0x3F00, 0x3F4E, 0x3F92, 0x3FCC, 0x4000};

					int zp[12]={0x4000, 0x5E00, 0x6800, 0x6D00, 0x7000, 0x7200, 0x7380, 0x7480, 0x7580, 0x7600,
						0x76C0, 0x7700};

					int zn[12]={0x4000, 0x5E80, 0x6880, 0x6DC0, 0x70C0, 0x72C0, 0x7440, 0x7540, 0x7600, 0x76C0,
						0x7740, 0x77C0};

					int idx;
					z=0;
					for(idx=0; idx<26; ++idx)
					{
						if(zt[idx]<=zoom)
							z=idx;
						else
							break;
					}
					idx=(int)z;
					++z;
					if(z<26 && zoom>zt[idx])
					{
						z+=((float)(zoom-zt[idx])) / ((float)(zt[idx+1]-zt[idx]));
					}
					else if(z==26 && zoom>zt[idx])
					{
						int *zv; 
						if(db.getDeviceConfigParam(id, string("/Video/Standard")) == string("PAL"))
							zv=zp;
						else
							zv=zn;

						for(idx=0; idx<12; ++idx)
						{
							if(zv[idx]<=zoom)
								z=idx;
							else
								break;
						}
						idx=(int)z;
						if(z<12 && zoom>zv[idx])
						{
							z += ((float)(zoom-zv[idx])) / ((float)(zv[idx+1]-zv[idx]));
						}
						++z;
						z*=26;
					}
				}
				break;		
			}
		case SNC_Z20:
			{
				int zt [18/*28*/]={0x0000, 0x1606, 0x2151, 0x2860, 0x2A8A, 0x2CB5, 0x3060, 0x32D3, 0x3545, 0x3727, 0x38A9, 0x3A42,
					0x3B4B, 0x3C85, 0x3D75, 0x3E4E, 0x3FA0, 0x4000};
				int zd[12]={0x4000, 0x6000, 0x6A80, 0x7000, 0x7340, 0x7540, 0x76C0,
					0x7800, 0x7900, 0x7980, 0x7A40, 0x7AC0};

				x=y=0; //sense pan/tilt 

				char url[256];
				sprintf(url,"http://%s/command/visca-inquiry.cgi?visca=81090447FF", cam.getIP().toString().c_str());
				HttpClient clz=HttpClient(url);

				//			cout <<" get zoom"<<endl;
				HttpStream *strz=clz.sendRequest(string("GET"));
				strz->getNextInformationChunk();
				string bdy=strz->getNextInformationChunk();

				delete strz;
				strz = NULL;

				char zv[5]={bdy[5],bdy[7],bdy[9],bdy[11],0};
				//			cout <<" zoom:"<<zv<<endl;
				short zoom = strtol(zv,NULL,16);

				int idx;
				z=0;
				for(idx=0; idx<18; ++idx)
				{
					if(zt[idx]<=zoom)
						z=idx;
					else
						break;
				}
				idx=(int)z;
				++z;
				if(z<18 && zoom>zt[idx])
				{
					z+=((float)(zoom-zt[idx])) / ((float)(zt[idx+1]-zt[idx]));
				}
				else if(z==18 && zoom>zt[idx])
				{
					for(idx=0; idx<12; ++idx)
					{
						if(zd[idx]<=zoom)
							z=idx;
						else
							break;
					}
					idx=(int)z;
					if(z<12 && zoom>zd[idx])
						z+=((float)(zoom-zd[idx])) / ((float)(zd[idx+1]-zd[idx]));
					++z;
					z*=18;
				}

				break;
			}
		case SNC_RS86:
		case SNC_WR632:
			{
				char url[256];
				sprintf(url,"http://%s/command/inquiry.cgi?inq=ptzf", cam.getIP().toString().c_str());

				string bdy=getConfigParamCamera(url, "AbsolutePTZF", "", id);
				cout<<" Get:"<<bdy<<endl;

				if(bdy.length()<14)
					break;
				short pan=strtol(bdy.substr(0,4).c_str(),NULL,16);
				cout<<" Get pan:"<<bdy.substr(0,4)<<" short "<<pan<<endl;

				short tilt=strtol(bdy.substr(5,4).c_str(),NULL,16);
				cout<<" Get tilt:"<<bdy.substr(5,4)<<" short "<<tilt<<endl;

				short zoom=strtol(bdy.substr(10,4).c_str(),NULL,16);


				x=((float)pan/45.0);//*(180.0/ ((float)0x1FE0)));

				y = ((float)tilt/45.0);//*((60.0/(float)2720.0));

				cout <<"	x:"<<x<<" : "<<pan<<" = 0x"<<hex<<pan<<dec<<endl; 
				cout <<"	y:"<<y<<" : "<<tilt<<" = 0x"<<hex<<tilt<<dec<<endl; 

				unsigned short zo_18[18]={ 
					0x0000, 0x1606, 0x2151, 0x2860, 0x2CB5, 0x3060, 0x32D3, 0x3545, 0x3727, 0x38A9, 0x3A42, 0x3B4B, 0x3C85, 0x3D75, 0x3E4E, 0x3EF7, 0x3FA0, 0x4000};
				unsigned short zo_36[36]={ 
					0x0000, 0x166F, 0x1FF0, 0x257D, 0x2940, 0x2C02, 0x2E2B, 0x2FEE, 0x316A, 0x32B2, 0x33D4, 0x34D9, 0x35C8, 0x36A4, 0x3773, 0x3836, 0x38F0, 0x39A0, 0x3A49, 0x3AE8, 0x3B7F, 0x3C0C, 0x3C8E, 0x3D06, 0x3D73, 0x3DD4, 0x3E2C, 0x3E7C, 0x3E2C, 0x3F00, 0x3F38, 0x3F68, 0x3F94, 0x3FBD, 0x3FDF, 0x4000};

				unsigned short zd[12]={0x4000, 0x6000, 0x6A80, 0x7000, 0x7300, 0x7540, 0x76C0, 0x7800, 0x78C0, 0x7980, 0x7A00, 0x7AC0};

					unsigned short *zt = zo_18;
					if(optic > 18.1)
					{
						zt = zo_36;
						cout<<"zoom "<<optic<<":"<<(int)optic<<" zo_36"<<endl;
					}
					else
						cout<<"zoom "<<optic<<":"<<(int)optic<<" zo_18"<<endl;

				int idx;
				z=0;
				for(idx=0; idx<optic; ++idx)
				{
					if(zt[idx]<=zoom)
						z=idx;
					else
						break;
				}
				idx=(int)z;
				++z;
				if(z<optic && zoom>zt[idx])
				{
					z+=((float)(zoom-zt[idx])) / ((float)(zt[idx+1]-zt[idx]));
				}
				else if(z==optic && zoom>zt[idx])
				{
					for(idx=0; idx<12; ++idx)
						if(zd[idx]<=zoom)
						{
							z=idx;
						}
						else
						{
							break;
						}
					idx=(int)z;
					if(z<12 && zoom>zd[idx])
					{
						z+=((float)(zoom-zd[idx])) / ((float)(zd[idx+1]-zd[idx]));
					}
					++z;
					z *= optic;
				}
				break;
			}
		case SNC_P1:
		case SNC_CS50:
		case SNC_CS3:
		case SNC_CS11:
		case SNC_DF50:
		default:
			{
				x=y=z=0;
			}
		}
		cout<<"	getPTZ pre offsets x:"<<x<<" y:"<<y<<":"<<ci->modelStr<<endl;
		x+=ox;
		y+=oy;
		z+=oz;

		cout<<"	getPTZ        offs x:"<<x<<" y:"<<y<<":"<<ci->modelStr<<endl;

		x*=sx;
		y*=sy;
		z*=sz;
		cout<<"              getPTZ x:"<<x<<" y:"<<y<<" z:"<<z<<endl;
	}catch(SocketTimeoutException &e)
	{
		cout<<"  getPTZ:  SocketTinmeoutExc - ContrPlugException:"<<e.getCode()<<":"<<e.getMsg()<<" Class:"<<e.getClass()<<endl;
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}

#ifdef DEBUG
	cout << "<----- SonyCP:getPTZ2" << endl;
#endif
	return CPPTZ(x,y,z);
}

/******************************************************************
 * setPTZ -- Configura el PTZ de un dispositivo							*
 *																						*
 *			ptz -- Movimiento PTZ. 												*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/
void SonyControlPlugin::setPTZ( CPPTZ ptz, Address *a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:setPTZ" << endl;
#endif

	SessionDispatcher s = getSession(*a);
	cams[s.getIdNumber()]->queue(	CPServ::setPTZ, 
									(void *) new CPPTZ(ptz)	);		
#ifdef DEBUG
	cout << "<----- SonyCP:setPTZ" << endl;
#endif	
}

/******************************************************************
 * run_setPTZ -- Realiza las acciones del PTZ del dispositivo		*
 *																						*
 *			ptz -- Movimiento PTZ. 												*
 *			id -- Identificador del dispositivo afectado.				*
 *			fromTour -- (???)														*
 *			slowMove -- (???)														*
 ******************************************************************/
void SonyControlPlugin::run_setPTZ(	CPPTZ ptz, 
												CPDeviceID id, 
												bool fromTour, 
												bool slowMove 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:run_setPTZ: " 	<< dec 	<< id.id 	<< ", " 
												 					<< fromTour << ", "
												 					<< slowMove << endl;
#endif

	CamInfo *ci = cams[id.id]; 
	ModelInfo *mi = models[ci->modelStr];
	Address cam=ci->cam;
	short pan, tilt, zoom;
	float ox=mi->ox, oy=mi->oy, oz=mi->oz;
	float sx=mi->sx, sy=mi->sy, sz=mi->sz;
	float minx=mi->minx, miny=mi->miny, minz=mi->minz;
	float maxx=mi->maxx, maxy=mi->maxy, maxz=mi->maxz, optic=mi->optic;
	float speed;
	RPC *rpc=db.getRPC(NULL,5);

	if(rpc==NULL)
	{
#ifdef DEBUG
	cout << "<----- SonyCP:run_setPTZ" << endl;
#endif
		return;
	}

	ci->tour->resetRecall();
	ci->tour->checkRecall();
	cout<<"setPTZ fromTour:"<<fromTour<<" touring:"<<ci->tour->touring<<endl;
	if(!fromTour && ci->tour->touring)
	{
		string rt = db.getDeviceConfigParam(id.id, string("/Presets/0/RecallTime"), rpc);
		if(rt != "")
		{
			int i=atoi(rt.c_str());
			if(i>0)
			{
				cout<<"hold Tour:"<<i<<endl;
				ci->tour->holdTour(i);
			}else
			{
				cout<<"stop Tour:"<<i<<endl;
				ci->tour->stopTour();
			}
		}else
		{
			cout<<"stop Tour(2)"<<endl;
			ci->tour->stopTour();
		}
	}
	try
	{
		if(isnan(ox))
			ox = mi->ox = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Offsets/x"), rpc).c_str());
		if(isnan(oy))
			oy = mi->oy = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Offsets/y"), rpc).c_str());
		if(isnan(oz))
			oz = mi->oz = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Offsets/z"), rpc).c_str());

		if(isnan(sx))
			sx = mi->sx = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Scale/x"), rpc).c_str());
		if(isnan(sy))
			sy = mi->sy = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Scale/y"), rpc).c_str());
		if(isnan(sz))
			sz = mi->sz = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Scale/z"), rpc).c_str());

		if(sx==0) sx=1;
		if(sy==0) sy=1;
		if(sz==0) sz=1;

		if(slowMove)
			speed = atof(db.getDeviceConfigParam(id.id, string("/Speed"), rpc).c_str());
		else
			speed = 1;
		cout<<"speed:"<<endl;
		if(speed>1)
			speed=1;
		else if(speed<0)
			speed=0;

		if(isnan(minx))
			minx = mi->minx = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Limits/Min/x"), rpc).c_str());
		if(isnan(miny))
			miny = mi->miny = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Limits/Min/y"), rpc).c_str());
		if(isnan(minz))
			minz = mi->minz = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Limits/Min/z"), rpc).c_str());

		if(isnan(maxx))
			maxx = mi->maxx = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Limits/Max/x"), rpc).c_str());
		if(isnan(maxy))
			maxy = mi->maxy = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Limits/Max/y"), rpc).c_str());
		if(isnan(maxz))
			maxz = mi->maxz = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Limits/Max/z"), rpc).c_str());
		if(isnan(optic))
			optic = mi->optic = atof(db.getModelConfigParam(string("SONY"), ci->modelStr, string("/Optics/OpticalZoom"), rpc).c_str());
		db.freeRPC(rpc);
	}catch(Exception &e)
	{
		db.freeRPC(rpc);
		throw;
	}

	CPPTZ aux;
	if(!(ptz.bx && ptz.by && ptz.bz))
		aux=getPTZ(id.id);

		cout<<"run_setPTZ pre scale i offsets x:"<<ptz.x<<" y:"<<ptz.y<<" z:"<<ptz.z<<" ox:"<<ox<<" oy:"<<oy<<" oz:"<<oz<<"  :  sx:"<<sx<<" sy:"<<sy<<" sz:"<<sz<<endl;

	if(ptz.bx)
	{
		if(ptz.x<minx)
			ptz.x=minx;
		else if(ptz.x>maxx)
			ptz.x=maxx;
	}
	else
		ptz.x=aux.x;

	if(ptz.by)
	{
		if(ptz.y<miny)
			ptz.y=miny;
		else if(ptz.y>maxy)
			ptz.y=maxy;
	}
	else
		ptz.y=aux.y;

	if(ptz.bz)
	{
		if(ptz.z<minz)
			ptz.z=minz;
		else if(ptz.z>maxz)
			ptz.z=maxz;
	}
	else
		ptz.y=aux.y;

	cout<<"run_setPTZ pre scale i offsets x:"<<ptz.x<<" y:"<<ptz.y<<" z:"<<ptz.z<<" ox:"<<ox<<" oy:"<<oy<<" oz:"<<oz<<"  :  sx:"<<sx<<" sy:"<<sy<<" sz:"<<sz<<endl;

	ptz.x/=sx;
	ptz.y/=sy;
	ptz.z/=sz;

		cout<<"run_setPTZ post scale i pre offsets x:"<<ptz.x<<" y:"<<ptz.y<<" z:"<<ptz.z<<" ox:"<<ox<<" oy:"<<oy<<" oz:"<<oz<<"  :  sx:"<<sx<<" sy:"<<sy<<" sz:"<<sz<<endl;

	ptz.x-=ox;
	ptz.y-=oy;
	ptz.z-=oz;

	cout<<"run_setPTZ Pan:"<<ptz.x<<"		Tilt:"<<ptz.y<<"		Zoom:"<<ptz.z<<"	model:"<<ci->model<<"	modStr:"<<ci->modelStr<<endl;

	switch (ci->model)
	{
		case SNC_RZ30:
		{
			ptz.x=-ptz.x;
			ptz.y=-ptz.y;

			int zt [25]={0x0000, 0x1781, 0x213B, 0x2752, 0x2B2A, 0x2F03, 0x315D, 0x3364, 0x34FF, 0x362C, 0x373D,
				0x386A, 0x3929, 0x3A20, 0x3AFA, 0x3BBA, 0x3C5E, 0x3CCB, 0x3D70, 0x3DF8, 0x3E66, 0x3ED3,
				0x3F25, 0x3F93, 0x4000};

			int zp [12]={0x4000, 0x5E00, 0x6800, 0x6D00, 0x7000, 0x7200, 0x7380, 0x7480, 0x7580, 0x7600,
				0x76C0, 0x7700};

			int zn [12]={0x4000, 0x5E80, 0x6880, 0x6DC0, 0x70C0, 0x72C0, 0x7440, 0x7540, 0x7600, 0x76C0,
				0x7740, 0x77C0};

			if(ptz.x<=0)
				pan=(short)(ptz.x*(((float)(0xFFFF-0xF670))/170));
			else
				pan=(short)(ptz.x*(((float)0x0990)/170));

			if(ptz.y<=0)
				tilt=(short)(ptz.y*(((float)(0xFFFF-0xFCC4))/67.5));//67.5
			else
				tilt=(short)(ptz.y*(((float)0x03CC)/47.5));//47.5

			char cpan[9];
			sprintf(cpan, "0%.01X0%.01X0%.01X0%.01X", (pan>>12)&0xf, (pan>>8)&0xf, (pan>>4)&0xf, pan&0xf);
			char ctilt[9];
			sprintf(ctilt, "0%.01X0%.01X0%.01X0%.01X", (tilt>>12)&0xf, (tilt>>8)&0xf, (tilt>>4)&0xf, tilt&0xf);

			char command[32];
			sprintf(command,"81010602%.02X%.02X%s%sFF", (int)(18.0*speed), (int)(14.0*speed), cpan, ctilt);

			char url[256];
			sprintf(url,"http://%s/command/visca-ptzf.cgi?visca=%s", cam.getIP().toString().c_str(), command);
			cout<<"		PT:"<<command<<" speed:"<<speed<<":"<<url<<endl;

			sendMoveCommand(url, id.id, slowMove);

			//zoom
			float z=ptz.z;

			if(z>0)
			{
				if(z<=25)
				{
					--z;
					zoom=zt[(int)z];
					if((z-(int)z)!=0)
						zoom=zoom + (short)((z-(int)z)*(zt[(int)z+1]-zt[(int)z]));
				}
				else if(z>=25 && z<=300)
				{
					z=z/25;
					--z;
					if(db.getDeviceConfigParam(id.id, string("/Video/Standard"))==string("PAL"))
					{
						zoom=zp[(int)z];
						if(z<12 && z-(int)z>0)
							zoom=zoom + (short)((z-(int)z)*(zp[(int)z+1]-zp[(int)z]));
					}else
					{
						zoom=zn[(int)z];
						if(z<12 && z-(int)z>0)
							zoom=zoom + (short)((z-(int)z)*(zn[(int)z+1]-zn[(int)z]));
					}
				}

				char czoom[9];
				sprintf(czoom, "0%.01X0%.01X0%.01X0%.01X", (zoom>>12)&0xf, (zoom>>8)&0xf, (zoom>>4)&0xf, zoom&0xf);
				sprintf(url,"http://%s/command/visca-ptzf.cgi?visca=81010447%sFF", cam.getIP().toString().c_str(), czoom);
				setConfigParamCamera(url, id.id);
				break;
			}
			break;
		}
		case SNC_RZ50:
		case SNC_RZ25:
		{
			int zt [25]={0x0000, 0x1781, 0x213B, 0x2752, 0x2B2A, 0x2F03, 0x315D, 0x3364, 0x34FF, 0x362C, 0x373D,
				0x386A, 0x3929, 0x3A20, 0x3AFA, 0x3BBA, 0x3C5E, 0x3CCB, 0x3D70, 0x3DF8, 0x3E66, 0x3ED3,
				0x3F25, 0x3F93, 0x4000};

			int zp [12]={0x4000, 0x5E00, 0x6800, 0x6D00, 0x7000, 0x7200, 0x7380, 0x7480, 0x7580, 0x7600,
				0x76C0, 0x7700};

			int zn [12]={0x4000, 0x5E80, 0x6880, 0x6DC0, 0x70C0, 0x72C0, 0x7440, 0x7540, 0x7600, 0x76C0,
				0x7740, 0x77C0};

			if(ptz.x<=0)
				pan=(short)(ptz.x*(((float)(0xFFFF-0xF670))/170));
			else
				pan=(short)(ptz.x*(((float)0x0990)/170));

			if(ptz.y<=0)
				tilt=(short)(ptz.y*(((float)(0xFFFF-0xFCC4))/67.5));
			else
				tilt=(short)(ptz.y*(((float)0x03CC)/47.5));

			char cpan[9];
			sprintf(cpan, "0%.01X0%.01X0%.01X0%.01X", (pan>>12)&0xf, (pan>>8)&0xf, (pan>>4)&0xf, pan&0xf);
			char ctilt[9];
			sprintf(ctilt, "0%.01X0%.01X0%.01X0%.01X", (tilt>>12)&0xf, (tilt>>8)&0xf, (tilt>>4)&0xf, tilt&0xf);

			char command[32];
			sprintf(command,"81010602%.02X%.02X%s%sFF", (int)(17.0*speed), (int)(13.0*speed), cpan, ctilt);

			char url[256];
			sprintf(url,"http://%s/command/visca-ptzf.cgi?visca=%s", cam.getIP().toString().c_str(), command);

			sendMoveCommand(url, id.id, slowMove);

			//zoom
			if(ci->model==SNC_RZ50)
			{
				int zt[26]={ 0x0000, 0x1760, 0x214C, 0x2722, 0x2B22, 0x2E20, 0x3080, 0x3278, 0x3426, 0x359E,
					0x36EE, 0x381C, 0x392E, 0x3A26, 0x3B08, 0x3BD4, 0x3C8C, 0x3D2E, 0x3DBC, 0x3E58, 0x3EA2, 
					0x3F00, 0x3F4E, 0x3F92, 0x3FCC, 0x4000};

				int zp[12]={0x4000, 0x5E00, 0x6800, 0x6D00, 0x7000, 0x7200, 0x7380, 0x7480, 0x7580, 0x7600,
					0x76C0, 0x7700};

				int zn[12]={0x4000, 0x5E80, 0x6880, 0x6DC0, 0x70C0, 0x72C0, 0x7440, 0x7540, 0x7600, 0x76C0,
					0x7740, 0x77C0};

				float z=ptz.z;

				if(z>0)
				{
					if(z<=26)
					{
						--z;
						zoom=zt[(int)z];
						if((z-(int)z)!=0)
							zoom=zoom + (short)((z-(int)z)*(zt[(int)z+1]-zt[(int)z]));
					}
					else if(z>=26 && z<=300)
					{
						z=z/26;
						--z;
						if(db.getDeviceConfigParam(id.id, string("/Video/Standard"))==string("PAL"))
						{
							zoom=zp[(int)z];
							if(z<12 && z-(int)z>0)
								zoom=zoom + (short)((z-(int)z)*(zp[(int)z+1]-zp[(int)z]));
						}else
						{
							zoom=zn[(int)z];
							if(z<12 && z-(int)z>0)
								zoom=zoom + (short)((z-(int)z)*(zn[(int)z+1]-zn[(int)z]));
						}
					}
					char czoom[9];
					sprintf(czoom, "0%.01X0%.01X0%.01X0%.01X", (zoom>>12)&0xf, (zoom>>8)&0xf, (zoom>>4)&0xf, zoom&0xf);
					sprintf(url,"http://%s/command/visca-ptzf.cgi?visca=81010447%sFF", cam.getIP().toString().c_str(), czoom);
					setConfigParamCamera(url, id.id);
					break;
				}

			}else
			{
				float z=ptz.z;

				if(z>0)
				{
					if(z<=25)
					{
						--z;
						zoom=zt[(int)z];
						if((z-(int)z)!=0)
							zoom=zoom + (short)((z-(int)z)*(zt[(int)z+1]-zt[(int)z]));
					}
					else if(z>=25 && z<=300)
					{
						z=z/25;
						--z;
						if(db.getDeviceConfigParam(id.id, string("/Video/Standard"))==string("PAL"))
						{
							zoom=zp[(int)z];
							if(z<12 && z-(int)z>0)
								zoom=zoom + (short)((z-(int)z)*(zp[(int)z+1]-zp[(int)z]));
						}else
						{
							zoom=zn[(int)z];
							if(z<12 && z-(int)z>0)
								zoom=zoom + (short)((z-(int)z)*(zn[(int)z+1]-zn[(int)z]));
						}
					}

					char czoom[9];
					sprintf(czoom, "0%.01X0%.01X0%.01X0%.01X", (zoom>>12)&0xf, (zoom>>8)&0xf, (zoom>>4)&0xf, zoom&0xf);
					sprintf(url,"http://%s/command/visca-ptzf.cgi?visca=81010447%sFF", cam.getIP().toString().c_str(), czoom);
					setConfigParamCamera(url, id.id);
					break;
				}
			}
			break;
		}
		case SNC_RX550:
		{
			unsigned short zt[26]={ 
				0x0000, 0x1760, 0x214C, 0x2722, 0x2B22, 0x2E20, 0x3080, 0x3278, 0x3426, 0x359E, 0x36EE, 0x381C, 0x392E,
				0x3A26, 0x3B08, 0x3BD4, 0x3C8C, 0x3D2E, 0x3DBC, 0x3E58, 0x3EA2, 0x3F00, 0x3F4E, 0x3F92, 0x3FCC, 0x4000};

			unsigned short zd[12]={0x4000, 0x6000, 0x6A80, 0x7000, 0x7300, 0x7540, 0x76C0, 0x7800, 0x78C0, 0x7980, 0x7A00, 0x7AC0};

			pan=(short)(ptz.x*(((float)0x3FBF)/360.0));


			if(ptz.y>0)
				tilt = (short)(ptz.y*(((float)(0x0880))/48.0));
			else
				tilt = (ptz.y*((2175.0)/48.0));
			cout <<"	x:"<<ptz.x<<" : "<<dec<<pan<<" = "<<hex<<pan<<dec<<endl; 
			cout <<"	y:"<<ptz.y<<" * "<<((-2175.0)/48.0)<<" : "<<dec<<tilt<<" = "<<hex<<tilt<<dec<<endl; 

			char url[256];

			//zoom
			float z=ptz.z;

			if(z>0)
			{
				if(z<=26)
				{
					--z;
					zoom=zt[(int)z];
					if((z-(int)z)!=0)
						zoom=zoom + (short)((z-(int)z)*(zt[(int)z+1]-zt[(int)z]));
				}
				else if(z>=26 && z<=312)
				{
					z=z/26;
					--z;
					if(db.getDeviceConfigParam(id.id, string("/Video/Standard"))==string("PAL"))
					{
						zoom=zd[(int)z];
						if(z<12 && z-(int)z>0)
							zoom=zoom + (short)((z-(int)z)*(zd[(int)z+1]-zd[(int)z]));

					}
				}

				int cSpeed = speed*24.0;
				if(cSpeed==24)
				{
					sprintf(url,"http://%s/command/ptzf.cgi?AbsolutePTZF=%4.04X,%4.04X,%4.04X", cam.getIP().toString().c_str(),pan&0xffff, tilt&0xffff, zoom&0xffff);
					cout<<"		RZ550:"<<url<<endl;
					setConfigParamCamera(url, id.id);
				}
				else
				{
					sprintf(url,"http://%s/command/ptzf.cgi?AbsolutePanTilt=%4.04X,%4.04X,%d", cam.getIP().toString().c_str(),pan&0xffff, tilt&0xffff, cSpeed);
					cout<<"		RX550 1:"<<url<<endl;
					sendMoveCommand(url, id.id, slowMove);

					sprintf(url,"http://%s/command/ptzf.cgi?AbsoluteZoom=%4.04X", cam.getIP().toString().c_str(), zoom&0xffff);
					cout<<"		RX550 2:"<<url<<endl;
					setConfigParamCamera(url, id.id);
				}

			}
			break;
		}
		case SNC_Z20:
		{
			int zt [18]={0x0000, 0x1606, 0x2151, 0x2860, 0x2A8A, 0x2CB5, 0x3060, 0x32D3, 0x3545, 0x3727, 0x38A9, 0x3A42,
				0x3B4B, 0x3C85, 0x3D75, 0x3E4E, 0x3FA0, 0x4000};

			int zd[12]={0x4000, 0x6000, 0x6A80, 0x7000, 0x7340, 0x7540, 0x76C0, 0x7800, 0x7900, 0x7980, 0x7A40,
				0x7AC0 };

			float z=ptz.z;

			if(z>0)
			{
				if(z<=18)
				{
					--z;
					zoom=zt[(int)z];
					if((z-(int)z)!=0)
						zoom=zoom + (short)((z-(int)z)*(zt[(int)z+1]-zt[(int)z]));
				}
				else if(z>=18 && z<=216)
				{
					z=z/18;
					--z;
					if(db.getDeviceConfigParam(id.id, string("/Video/Standard"))==string("PAL"))
					{
						zoom=zd[(int)z];
						if(z<12 && z-(int)z>0)
							zoom=zoom + (short)((z-(int)z)*(zd[(int)z+1]-zd[(int)z]));
					}
				}


				char czoom[9];
				sprintf(czoom, "0%.01X0%.01X0%.01X0%.01X", (zoom>>12)&0xf, (zoom>>8)&0xf, (zoom>>4)&0xf, zoom&0xf);

				char url[256];
				sprintf(url,"http://%s/command/visca-ptzf.cgi?visca=81010447%sFF", cam.getIP().toString().c_str(), czoom);
				setConfigParamCamera(url, id.id);
			}
			break;
		}
		case SNC_RS86:
		case SNC_WR632:
		{	
			//Compte: no funcionarà per SNC-RH124/164 perqué son retrassats

			unsigned short zo_18[18]={ 
				0x0000, 0x1606, 0x2151, 0x2860, 0x2CB5, 0x3060, 0x32D3, 0x3545, 0x3727, 0x38A9, 0x3A42, 0x3B4B, 0x3C85, 0x3D75, 0x3E4E, 0x3EF7, 0x3FA0, 0x4000};
			unsigned short zo_36[36]={ 
				0x0000, 0x166F, 0x1FF0, 0x257D, 0x2940, 0x2C02, 0x2E2B, 0x2FEE, 0x316A, 0x32B2, 0x33D4, 0x34D9, 0x35C8, 0x36A4, 0x3773, 0x3836, 0x38F0, 0x39A0, 0x3A49, 0x3AE8, 0x3B7F, 0x3C0C, 0x3C8E, 0x3D06, 0x3D73, 0x3DD4, 0x3E2C, 0x3E7C, 0x3E2C, 0x3F00, 0x3F38, 0x3F68, 0x3F94, 0x3FBD, 0x3FDF, 0x4000};

			unsigned short zd[12]={0x4000, 0x6000, 0x6A80, 0x7000, 0x7300, 0x7540, 0x76C0, 0x7800, 0x78C0, 0x7980, 0x7A00, 0x7AC0};

			pan=(short)(ptz.x*45.0);

			tilt = (ptz.y*45.0);

			cout <<"	x:"<<ptz.x<<" : "<<dec<<pan<<" = "<<hex<<pan<<dec<<endl; 
			cout <<"	y:"<<ptz.y<<" : "<<dec<<tilt<<" = "<<hex<<tilt<<dec<<endl; 

			char url[256];

			//zoom
			float z=ptz.z;

			if(z>0)
			{
				unsigned short *zt = zo_18;
				if(optic > 18.1)
				{
					zt = zo_36;
					cout<<"zoom "<<optic<<":"<<(int)optic<<" zo_36"<<endl;
				}
				else
					cout<<"zoom "<<optic<<":"<<(int)optic<<" zo_18"<<endl;
				if(z<=optic)
				{
					--z;
					zoom=zt[(int)z];
					if((z-(int)z)!=0)
						zoom=zoom + (short)((z-(int)z)*(zt[(int)z+1]-zt[(int)z]));
				}
				else if(z>=optic && z<= maxz)
				{
					z=z/optic;
					float maxd = maxz / optic;
					--z;
					if(db.getDeviceConfigParam(id.id, string("/Video/Standard"))==string("PAL"))
					{
						zoom=zd[(int)z];
						if(z<maxd && z-(int)z>0)
							zoom=zoom + (short)((z-(int)z)*(zd[(int)z+1]-zd[(int)z]));

					}
				}

				int cSpeed = speed*24.0;
				if(cSpeed==24)
				{
					sprintf(url,"http://%s/command/ptzf.cgi?AbsolutePTZF=%4.04X,%4.04X,%4.04X", cam.getIP().toString().c_str(),pan&0xffff, tilt&0xffff, zoom&0xffff);
					cout<<"		RS86:"<<url<<endl;
					setConfigParamCamera(url, id.id);
				}else
				{
					sprintf(url,"http://%s/command/ptzf.cgi?AbsolutePanTilt=%4.04X,%4.04X,%d", cam.getIP().toString().c_str(),pan&0xffff, tilt&0xffff, cSpeed);
					cout<<"		RS86 1:"<<url<<endl;
					sendMoveCommand(url, id.id, slowMove);
					sprintf(url,"http://%s/command/ptzf.cgi?AbsolutePTZF=%4.04X,%4.04X,%4.04X", cam.getIP().toString().c_str(),pan&0xffff, tilt&0xffff, zoom&0xffff);
					cout<<"		RS86 2:"<<url<<endl;
					setConfigParamCamera(url, id.id);
				}

			}
			break;
		}
		case SNC_P1:
		case SNC_CS50:
		case SNC_CS3:
		case SNC_CS11:
		case SNC_DF50:
		{
			throw(CPInvalidParamException(0, "Camara sense funcio de pan/tilt/zoom"));
			break;
		}
		default:
			break;
	}

#ifdef DEBUG
	cout << "<----- SonyCP:run_setPTZ" << endl;
#endif
}

/******************************************************************
 * setInput -- Configura la entrada del dispositivo  (???)			*
 *																						*
 *			in -- Entrada para el dispositivo. (???) ¿Qué significa?	*
 *			a -- Dirección de la base de datos								*
 *																						*
 * TODO 20150523																	*
 ******************************************************************/	
void SonyControlPlugin::setInput(CPInput in, Address *a)
{
#ifdef DEBUG
	cout << "-----> SonyCP:setInput" << endl;
#endif

	SessionDispatcher s = getSession(*a); //operacion invalida per la RZ-30

#ifdef DEBUG
	cout << "<----- SonyCP:setInput" << endl;
#endif
}

/******************************************************************
 * getInput -- Llama a la auténtica función para obtener la 		*
 *				entrada. (???)														*
 *																						*
 *			in -- Entrada para el dispositivo. (???) ¿Qué significa?	*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/	
CPInput SonyControlPlugin::getInput(CPInput in, Address *a)
{
#ifdef DEBUG
	cout << "-----> SonyCP:getInput1: " << endl;
#endif

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- SonyCP:getInput1" << endl;
#endif
	return (	getInput(in, s.getIdNumber()) );
}

/******************************************************************
 * getInput -- Obtiene la entrada del dispositivo (???)				*
 *																						*
 *			in -- Entrada para el dispositivo. (???) ¿Qué significa?	*
 *			devId -- Identificador del dispositivo.						*
 ******************************************************************/	
CPInput SonyControlPlugin::getInput( CPInput in, int devId )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getInput2: " << devId << endl;
#endif

	if(in.id == 0)
	{
		CPInput response;

		switch(cams[devId]->model)
		{
			case SNC_Z20:
			case SNC_RZ30:
			case SNC_CS3:
			{
				if( getMetadataValue(devId, "ActivityDetected") == "true" )
				{
					response = CPInput(1,1);
				}
				else
				{
					response = CPInput(1,0);
				}
				break;
			}
			case SNC_RZ50:
			case SNC_RZ25:
			case SNC_RX550:
			case SNC_CS50:
			{
				char url[256];
				string mode("");
				if( db.getDeviceConfigParam(devId,"/Alarms/ObjectDetection/Mode") == "Unattended Object Detection" )
				{
					sprintf(	url,
								"http://%s/uddata.cgi", 
								cams[devId]->cam.getIP().toString().c_str()	);	
				}
				else
				{
					sprintf(	url,
								"http://%s/mddata.cgi", 
								cams[devId]->cam.getIP().toString().c_str()	);
				}

				HttpClient cl(url);
				cl.setHeader("Authorization", this->getBasicAuthStrForDevice(devId));
				HttpStream *stream = cl.sendRequest("GET");
				stream->getNextInformationChunk();						// cabecera
				string body = stream->getNextInformationChunk();	// cuerpo

				delete stream;
				stream = NULL;

				list<string> values = StrUtils::split(body,"\r\n");
				bool found_value = false;


				for(list<string>::iterator i = values.begin(), iEnd = values.end(); 
						i != iEnd; ++i)
				{

					int p = i->find("=");

					if( p == string::npos )
					{
						continue;
					}

					string param = i->substr(0, p-1);
					if( param != "Window" )
					{
						continue;
					}

					string value = i->substr(p);
					if(atoi(value.c_str()) == 0)
					{
						found_value = true;
						response = CPInput(in.id, (int)atoi(value.c_str()));
						break;
					}
				}

				if (!found_value)
				{
					response = CPInput(in.id, 0);	
				}
				
				break;
			}
		}

#ifdef DEBUG
	cout << "<----- SonyCP:getInput2" << endl;
#endif
		return response;
	}
	else
	{
		int max = 0;
		switch(cams[devId]->model)
		{
			case SNC_Z20:
			case SNC_CS3:
				max = 1;
				break;
			case SNC_RZ50:
			case SNC_RZ25:
			case SNC_RX550:
			case SNC_CS50:
				max = 2;
				break;
			case SNC_RZ30:
				max = 3;
				break;
		}

		if(in.id > max)
		{
			throw ( CPInvalidParamException(	0, 
													"SCP:Identificador d'Input invalid:" + 
													StrUtils::decToString(in.id) ) );
		}

		char url[256];
		sprintf(	url,
					"http://%s/command/inquiry.cgi?inq=sensor", 
					cams[devId]->cam.getIP().toString().c_str()	);

		string res = getConfigParamCamera(	url, 
														"Sensor" +
														StrUtils::decToString(in.id), 
														"", 
														devId );
		int value = atoi(res.c_str());

#ifdef DEBUG
	cout << "<----- SonyCP:getInput2" << endl;
#endif
		return CPInput(in.id, value);
	}

	throw(CPInvalidParamException(0, string("SCP:Identificador d'Output invalid:")+StrUtils::decToString(in.id)));
}

/******************************************************************
 * setOutput -- Configura la salida del dispositivo (???)			*	
 *																						*
 *			out -- Salida para el dispositivo. (???) ¿Qué significa?	*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/	
void SonyControlPlugin::setOutput( CPOutput out, Address *a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:setOutput" << endl;
#endif

	SessionDispatcher ses = getSession(*a);
	string value("on");

	if( out.value == 0 )
	{
		value = "off";
	}

	// Ho controlem per DB
	db.setDeviceConfigParam(	ses.getIdNumber(), 
										"/Output/" + 
										StrUtils::decToString(out.id), 
										StrUtils::decToString(out.value)	); 
	Address cam = ses.getIpAddress();	
	switch (cams[ses.getIdNumber()]->model)
	{
		case SNC_RZ30:
		case SNC_RZ25:
		case SNC_RX550:
		case SNC_Z20:
		{
			if(out.id<1 || out.id>2)
			{
				throw(CPInvalidParamException(0, string("SCP:Identificador d'Output invalid:")+StrUtils::decToString(out.id)));
			}

			char url[256];						

			sprintf(url,"http://%s/command/alarmout.cgi?AlarmOut%iFunc=on", cam.getIP().toString().c_str(), out.id); //activem l'alarma correesponent
			setConfigParamCamera(url, ses.getIdNumber());

			sprintf(url,"http://%s/command/alarmout.cgi?Ao%iMode=manual", cam.getIP().toString().c_str(), out.id);
			setConfigParamCamera(url, ses.getIdNumber());

			sprintf(url,"http://%s/command/main.cgi?trigger=alarmout%i%s", cam.getIP().toString().c_str(), out.id, value.c_str()); //trigger
			setConfigParamCamera(url, ses.getIdNumber());

			break;
		}
		case SNC_RZ50:
		{
			if(out.id<1 || out.id>2)
			{
				throw(CPInvalidParamException(0, string("SCP:Identificador d'Output invalid:")+StrUtils::decToString(out.id)));
			}

			char url[256];						
			sprintf(url,"http://%s/command/alarmout.cgi?AlarmOut%iFunc=on", cam.getIP().toString().c_str(), out.id); //activem l'alarma correesponent
			setConfigParamCamera(url, ses.getIdNumber());

			if(out.value==0)
				sprintf(url,"http://%s/command/alarmout.cgi?Ao%iManualFunc=off", cam.getIP().toString().c_str(), out.id);
			else
				sprintf(url,"http://%s/command/alarmout.cgi?Ao%iManualFunc=on", cam.getIP().toString().c_str(), out.id);
			setConfigParamCamera(url, ses.getIdNumber());
			break;
		}
		case SNC_P1:
		{
			if(out.id!=1)
			{
				throw(CPInvalidParamException(0, string("SCP:Identificador d'Output invalid:")+StrUtils::decToString(out.id)));
			}

			char url[256];						

			sprintf(url,"http://%s/command/alarmout.cgi?AlarmOutFunc=on", cam.getIP().toString().c_str()); //activem l'alarma correesponent
			setConfigParamCamera(url, ses.getIdNumber());

			sprintf(url,"http://%s/command/alarmout.cgi?AoMode=manual", cam.getIP().toString().c_str());
			setConfigParamCamera(url, ses.getIdNumber());

			sprintf(url,"http://%s/command/main.cgi?trigger=alarmout-%s", cam.getIP().toString().c_str(), value.c_str()); //trigger
			setConfigParamCamera(url, ses.getIdNumber());
			break;
		}
	}
#ifdef DEBUG
	cout << "<----- SonyCP:setOutput: " << endl;
#endif
}

/******************************************************************
 * setOutput -- Obtiene la salida del dispositivo (???)				*	
 *																						*
 *			out -- Salida para el dispositivo. (???) ¿Qué significa?	*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/	
CPOutput SonyControlPlugin::getOutput( CPOutput out, Address *a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getOutput: " << endl;
#endif

	string res("");
	string valor("");
	
	SessionDispatcher ses = getSession(*a);

	switch (cams[ses.getIdNumber()]->model)
	{
		case SNC_RX550:
		{
			char url[256];
			sprintf(url,"http://%s/command/inquiry.cgi?inq=alarmoutstatus", cams[ses.getIdNumber()]->cam.getIP().toString().c_str()); // Timeout??
			res=getConfigParamCamera(url, string("Ao")+StrUtils::decToString(out.id)+string("Status"), "", ses.getIdNumber());
			int value=atoi(res.c_str());

#ifdef DEBUG
	cout << "<----- SonyCP:getOutput" << endl;
#endif
			return CPOutput(out.id, value);
			break;
		}

		case SNC_RZ30:
		case SNC_RZ25:
		case SNC_RZ50:
		case SNC_Z20:
		case SNC_P1:
		default:
		{
			res=db.getDeviceConfigParam(ses.getIdNumber(), string("/Output/")+StrUtils::decToString(out.id)); //per DB

			if(res != "")
			{
#ifdef DEBUG
	cout << "<----- SonyCP:getOutput" << endl;
#endif
				return CPOutput(out.id, atoi(res.c_str()));
			}
			break;
		}
	}

#ifdef DEBUG
	cout << "<----- SonyCP:getOutput" << endl;
#endif
	return CPOutput(-1,-1);
}

/******************************************************************
 * setCommandBufferSize -- Configura el tamaño del CommandBuffer.	*
 *																						*
 *			size -- Nuevo tamaño													*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/	
void SonyControlPlugin::setCommandBufferSize(	CPCommandBufferSize size, 
																Address *a 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:setCommandBufferSize" << endl;
#endif

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- SonyCP:setCommandBufferSize" << endl;
#endif
	cams[s.getIdNumber()]->setSize(size.nCommands);
}

/******************************************************************
 * getCommandBufferSize -- Obtiene el tamaño del CommandBuffer.	*
 *																						*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/	
CPCommandBufferSize SonyControlPlugin::getCommandBufferSize(Address *a)
{
#ifdef DEBUG
	cout << "-----> SonyCP:getCommandBufferSize" << endl;
#endif

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- SonyCP:getCommandBufferSize" << endl;
#endif
	return CPCommandBufferSize(cams[s.getIdNumber()]->getSize());
}

/******************************************************************
 * getCommandBufferPercentInUse -- Obtiene el porcentaje de uso 	*
 * 				del CommandBuffer.											*
 *																						*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/	
float SonyControlPlugin::getCommandBufferPercentInUse(Address *a)
{
#ifdef DEBUG
	cout << "-----> SonyCP:getCommandBufferPercentInUse" << endl;
#endif

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- SonyCP:getCommandBufferPercentInUse" << endl;
#endif
	return cams[s.getIdNumber()]->getPercentInUse();
}

/******************************************************************
 * getCommandBufferCommandsInUse -- Obtiene los comandos en uso 	*
 *					del CommandBuffer.											*
 *																						*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/	
int SonyControlPlugin::getCommandBufferCommandsInUse(Address *a)
{
#ifdef DEBUG
	cout << "-----> SonyCP:getCommandBufferCommandsInUse" << endl;
#endif

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- SonyCP:getCommandBufferCommandsInUse" << endl;
#endif
	return cams[s.getIdNumber()]->getUse();
}

/******************************************************************
 * getMetadataValue -- Llama a la auténtica función para obtener 	*
 *					metadatos.														*
 *																						*
 *			data -- El metadato a recuperar									*
 *			a -- Dirección de la base de datos								*
 ******************************************************************/	
CPMetadata SonyControlPlugin::getMetadataValue(CPMetadata data, Address *a)
{
#ifdef DEBUG
	cout << "-----> SonyCP:getMetadataValue1" << endl;
#endif

	SessionDispatcher s = getSession(*a);

#ifdef DEBUG
	cout << "<----- SonyCP:getMetadataValue1" << endl;
#endif
	return CPMetadata(data.name, getMetadataValue(s.getIdNumber(), data.name));
}

/******************************************************************
 * getMetadataValue -- Obtiene el valor de un metadato				*
 *																						*
 *			id -- Identificador del dispositivos							*
 *			data -- El metadato a recuperar									*
 ******************************************************************/	
string SonyControlPlugin::getMetadataValue( int id, string data )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getMetadataValue2: " << id << ", " << data << endl;
#endif

	map<string, string> *m=&cams[id]->metadata;

	if(data==string("ActivityDetected"))
	{
		switch(cams[id]->model)
		{
			case SNC_Z20:
			case SNC_RZ30:
			{
				struct timeval t;
				gettimeofday(&t, NULL);
				if(((*m)[data]=="true" && (t.tv_sec > cams[id]->alarmTime.tv_sec+3)) || (t.tv_sec == cams[id]->alarmTime.tv_sec+3 && t.tv_usec == cams[id]->alarmTime.tv_usec))
					(*m)[data]=string("false");
				break;
			}
			case SNC_RZ50:
			case SNC_RZ25:
			case SNC_RX550:
			{
				char url[256];
				sprintf(url,"http://%s/mddata.cgi", cams[id]->cam.getIP().toString().c_str());

				HttpClient cl(url);
				cl.setHeader("Authorization", this->getBasicAuthStrForDevice(id));

				HttpStream *stream=cl.sendRequest(string("GET"));
				string body=stream->getNextInformationChunk();

				delete stream;
				stream = NULL;

				list<string> values=StrUtils::split(body,"\r\n");
				(*m)[data]=string("false");

				for(	list<string>::iterator i = values.begin(), iEnd = values.end(); 
						i != iEnd; ++i)
				{
					int p=i->find("=");
					if(p==string::npos)
						continue;
					string param=i->substr(0,p-1);
					if(param!=string("Window"))
						continue;
					string value=i->substr(p);

					if(atof(value.c_str())==0)
						continue;
					(*m)[data]=string("true");
					break;
				}
				break;
			}
			default:
			{
				break;
			}
		}
	}
#ifdef DEBUG
	cout << "<----- SonyCP:getMetadataValue2" << endl;
#endif

	return (*m)[data];
}

/******************************************************************
 * setMetadataValue -- Llama a la auténtica función  para 			*
 *					configurar metadatos.										*
 *																						*
 *			data -- (???)															*
 *			a -- Dirección del dispositivo a configurar	(¿Seguro?)	*
 ******************************************************************/	
void SonyControlPlugin::setMetadataValue(CPMetadata data, Address *a)
{
#ifdef DEBUG
	cout << "-----> SonyCP:setMetadataValue1" << endl;
#endif

	SessionDispatcher s = getSession(*a);
	setMetadataValue(s.getIdNumber(), data.name, data.value);

#ifdef DEBUG
	cout << "<----- SonyCP:setMetadataValue1" << endl;
#endif			
}

// 
/******************************************************************
 * setMetadataValue -- Configura un metadato pasado por parámetro *
 *																						*
 *			id -- Identificador del dispositivo.							*
 *			name -- Nombre del metadato a configurar.						*
 *			value -- Valor a dar al metadato anterior.					*
 ******************************************************************/	
void SonyControlPlugin::setMetadataValue(	int id, string name, string value )
{
#ifdef DEBUG
	cout << "-----> SonyCP:setMetadataValue2: " << id << ", " 
															<< name << ", " 
															<< value << endl;
#endif

	cams[id]->metadata[name] = value;
	if (	( name == "ActivityDetected" ) 	&& 
			( value == "true"	)	)
	{
		gettimeofday(&(cams[id]->alarmTime), NULL);
	}

#ifdef DEBUG
	cout << "<----- SonyCP:setMetadataValue2" << endl;
#endif
}

/******************************************************************
 * getConfigParam -- Establece sesión con la base de datos y		*
 * 			llama a la auténtica funcíón que recuperará el 			*
 *				parámetro.															*
 *																						*
 *			p -- El parámetro a obtener.										*
 *			a -- Dirección de la base de datos.								*
 ******************************************************************/	
CPConfigParam SonyControlPlugin::getConfigParam( CPConfigParam p, Address *a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getConfigParam1" << endl;
#endif

	SessionDispatcher ses = getSession(*a);

#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParam1" << endl;
#endif
	return getConfigParam(ses.getIdNumber(), p, a);
}

/******************************************************************
 * getConfigParam -- Obtiene un parámetro de la configuración.		*
 *																						*
 *			id -- Identificador del dispositivo								*
 *			p -- El parámetro a obtener.										*
 *			a -- Dirección del dispositivo a configurar	(¿Seguro?)	*
 ******************************************************************/	
CPConfigParam SonyControlPlugin::getConfigParam(	int id, 
																	CPConfigParam p, 
																	Address *a 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:getConfigParam: " << id << endl;
#endif

	RPC *rpc=db.getRPC(a,5);

	if(rpc==NULL)
	{
#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParam" << endl;
#endif
		return CPConfigParam("","");
	}

	try
	{
		cout<<" getConfigParam:"<<cams[id]->modelStr<<" : "<< p.path<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
		xmlNode *modNode = modelConf->getNode(string("/[0]/Models/All")+p.path);

		if(modNode != NULL && modNode->getKidList().size() == 0)
		{
			string cp=db.getModelConfigParam(string("SONY"), cams[id]->modelStr, p.path, rpc);
				cout<<" getConfigParam: de model:"<<p.path<<":"<<cp<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
			db.freeRPC(rpc);

#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParam" << endl;
#endif
			return CPConfigParam(p.path, cp);
		}

		cout<<" getConfigParam:"<<cams[id]->modelStr<<" id:"<<id<<": "<< p.path<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
		string res=db.getDeviceConfigParam(id, p.path, rpc);
		cout<<" getConfigParam , freeRPC:"<<rpc->getLocalAddr().toString()<<endl;
		db.freeRPC(rpc);

#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParam" << endl;
#endif
		return CPConfigParam(p.path, res);
	}
	catch(Exception &e)
	{
		cout<<" getConfigParam:"<<cams[id]->modelStr<<" id:"<<id<<": "<< p.path<<" Exception:"<<e.getClass()<<"::"<<e.getMsg()<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
		db.freeRPC(rpc);
		throw;
	}
	catch(...)
	{
		db.freeRPC(rpc);
		throw;
	}
}

/******************************************************************
 * setConfigParam -- Establece un parámetro en la configuración.	*
 *																						*
 *			p -- El parámetro a configurar (¿Uno o varios?)				*
 *			a -- Dirección del dispositivo a configurar	(¿Seguro?)	*
 ******************************************************************/	
void SonyControlPlugin::setConfigParam( CPConfigParam p, Address *a )
{
#ifdef DEBUG
	cout 	<< "-----> SonyCP:setConfigParam: " << p.path << ", " << p.value << endl;
#endif

	SessionDispatcher s = getSession(*a);
	cams[s.getIdNumber()]->queue(	CPServ::setConfigParam, 
									(void *) new CPConfigParam(p)	);
#ifdef DEBUG
	cout << "<----- SonyCP:setConfigParam" << endl;
#endif
}

/******************************************************************
 * run_setConfigParam -- (???)												*
 *																						*
 *			p -- El parámetro a configurar (¿Uno o varios?)				*
 *			id -- identificador del dispositivo a configurar.			*
 * 		fromTour -- (???)														*
 ******************************************************************/	
void SonyControlPlugin::run_setConfigParam(	CPConfigParam p, 
															CPDeviceID id, 
															bool fromTour 	)
{
#ifdef DEBUG
	cout 	<< "-----> SonyCP:run_setConfigParam: " << p.path << ", " << p.value 
			<< ", " << id.id << endl;
#endif

	Address cam=cams[id.id]->cam;

	//Update camara
	/*
	 * - Pan Tilt Limit
	 * - Preset position
	 * - Zoom Type (Opt/Opt+Digt)
	 * - Gain/Brightness/Expos.
	 */

	string s=string ("/[0]/Devices/")+cams[id.id]->modelStr;
	list<string> l=StrUtils::pathToList(p.path);	
	for(	list<string>::iterator it = l.begin(), itEnd = l.end(); 
			it != itEnd; ++it)
	{
		s+=string("/")+(*it);

		xmlNode *aux=conf->getNode(s);
		if(aux!=NULL && aux->getParam("nodename")==string("inc"))
		{
			++it;
			if(it!=l.end() && (*it)==string("Enable"))
			{
				++it;
				if(it==l.end())
				{
					p.path=p.path.substr(0,p.path.rfind("/Enable"));
					break;
				}
				--it;
				continue;
			}
			db.setDeviceConfigParam(id.id, p.path, p.value);

#ifdef DEBUG
	cout << "<----- SonyCP:run_setConfigParam" << endl;
#endif
			return;
		}
	}

	cout 	<< " setCP Peticio: " << p.path << "->" << p.value << " conf:" 
			<< (void*)conf << endl;

	list<string> lp 	= StrUtils::pathToList(p.path);
	string path 		= "";

	for(	list<string>::iterator it = lp.begin(), itEnd = lp.end();
			it != itEnd; ++it)
	{
		path += "/" + (*it);
	}

	int pos = path.rfind("/Delete");

	if(pos!=string::npos)
	{
		path=path.substr(0,pos);
	}

	//cout << "### " << conf->getNode("/[0]/Devices/" << endl;
	cout << ": " << cams[id.id]->modelStr << path << endl;

	// Si el campo no está vacío en la BD
	if ( conf->getNode("/[0]/Devices/" + cams[id.id]->modelStr + path ) != NULL )
	{
		if(p.path.rfind("/Delete")!=string::npos)
		{
			cout<<" setConfigParam Del:"<<path<<endl;
			db.deleteDeviceConfig(id.id, path);
			loadDevConfig(id.id, cams[id.id]->modelStr, string("/[0]/Devices/")+cams[id.id]->modelStr+path, path, conf);
			//TODO: arreglar filtres ( probar FPS[0], FPS[1], etc...)
		}
		else
		{
			cout << " setDevConfigParam: " << p.path << " = " << p.value << endl;
			db.setDeviceConfigParam(id.id, p.path, p.value);

			cout 	<<	"  LoadDevConf: " << "/[0]/Devices/" + cams[id.id]->modelStr + 
						p.path << " path: " << p.path << endl;

			loadDevConfig(	id.id, 
								cams[id.id]->modelStr, 
								"/[0]/Devices/" + cams[id.id]->modelStr + path, 
								p.path, 
								conf 	);
			cout << endl;
		}
	}
	else
	{	//coses que no depenen del SCPDeviceConfig.xml
		if(p.path.rfind("/Delete")!=string::npos)
		{
			cout<<" setConfigParam Del:"<<path<<endl;
			db.deleteDeviceConfig(id.id, path);
		}
		else
		{
			list<string>::iterator it=l.begin();

			if((*it)==string("Presets"))
			{
				++it;
				if(it!=l.end())
				{
					string n=*it;
					int nidx=atoi(n.c_str());

					++it;
					if(it!=l.end() && (*it) == string("Set"))
					{	//per defecte, pillem la pos. actual
						cout<<"save Preset:"<<p.path<<" n:"<<n<<endl;
						CPPTZ ptz=getPTZ(id.id);
						//if(p.path.length()>0 && p.path[p.path.length()-1]=='/')
						p.path=p.path.substr(0,p.path.rfind("/Set"));
						if(db.getDeviceConfigParam(id.id, p.path+string("/Name"))==string(""))
							db.setDeviceConfigParam(id.id, p.path+string("/Name"), StrUtils::decToString(nidx));
						db.setDeviceConfigParam(id.id, p.path+string("/x"), StrUtils::floatToString(ptz.x));
						db.setDeviceConfigParam(id.id, p.path+string("/y"), StrUtils::floatToString(ptz.y));
						db.setDeviceConfigParam(id.id, p.path+string("/z"), StrUtils::floatToString(ptz.z));
					}
					else if(it!=l.end() && (*it) == string("Recall"))
					{
						cout<<" recall Preset"<<p.path<<" n:"<<n<<endl;
						p.path=p.path.substr(0, p.path.rfind("/Recall"));
						string s = db.getDeviceConfigParam(id.id, p.path+string("/x"));
						if(s != string(""))
						{
							float x=atof(s.c_str());
							float y=atof(db.getDeviceConfigParam(id.id, p.path+string("/y")).c_str());
							float z=atof(db.getDeviceConfigParam(id.id, p.path+string("/z")).c_str());
							CPPTZ ptz(x,y,z);
							run_setPTZ(ptz, id, fromTour, fromTour);
						}
					}
					else if(nidx == 0 && it!=l.end() && (*it) == string("RecallTime"))
					{
						if(p.value != string(""))
						{
							int i=atoi(p.value.c_str());
							if(i>0)
							{
								cams[id.id]->tour->startRecall(i);
							}
							else if(i==0)
							{
								cams[id.id]->tour->stopRecall();
							}
						}
						db.setDeviceConfigParam(id.id, p.path, p.value);
					}
					else if(nidx == 0 && it!=l.end() && (*it) == string("Hold"))
					{
						if(p.value != string(""))
						{
							int i=atoi(p.value.c_str());
							if(i>0)
							{
								if(cams[id.id]->tour->touring)
									cams[id.id]->tour->holdTour(i);
								else
									cams[id.id]->tour->holdRecall(i);
							}
						}
						db.setDeviceConfigParam(id.id, p.path, p.value);
					}
					else
						db.setDeviceConfigParam(id.id, p.path, p.value);
				}
#ifdef DEBUG
	cout << "<----- SonyCP:run_setConfigParam" << endl;
#endif
				return;
			}

			else if((*it)==string("Tours"))
			{
				++it;
				if(it!=l.end())
				{
					if((*it)==string("Stop"))
					{
						cams[id.id]->tour->stop();
						db.setDeviceConfigParam(id.id, "/Tours/Active",string(""));
					}
					else
					{
						string n=*it;
						cout<<" Tours:"<<p.path<<":"<<p.value<<endl;//" last:"<<last<<endl;
						int nidx=atoi(n.c_str());

						++it;
						if(it==l.end())
						{
							cout<<" Add Tour:"<<p.path<<" n:"<<n<<endl;
							db.setDeviceConfigParam(id.id, p.path, p.value);
						}
						else if((*it) == string("Start"))
						{
							if(n==string("0"))
							{
								cams[id.id]->tour->stop();
								db.setDeviceConfigParam(id.id, "/Tours/Active","");
							}
							else
							{
								// presets que hi ha a la cam
								cams[id.id]->tour->startTour(atoi(n.c_str()));
								db.setDeviceConfigParam(id.id, "/Tours/Active",n);
							}
						}
						else if((*it) == string("Remove"))
						{
							cams[id.id]->tour->stop();
							p.path=p.path.substr(0, p.path.rfind("/Remove")); //path tour
							db.deleteDeviceConfig(id.id, p.path);
						}
						else
							db.setDeviceConfigParam(id.id, p.path, p.value);
					}
#ifdef DEBUG
	cout << "<----- SonyCP:run_setConfigParam" << endl;
#endif
					return;
				}
			}

			// Modificamos los parámetros en el dispositivo
			loadDevConfig(	id.id, 
								cams[id.id]->modelStr, 
								"/[0]/Devices/" + cams[id.id]->modelStr + path, 
								p.path, 
								conf 	);		

			// Modificamos los parámetros en la base de datos
			db.setDeviceConfigParam(id.id, p.path, p.value);
		}
	}
#ifdef DEBUG
	cout << "<----- SonyCP:run_setConfigParam" << endl;
#endif
}

/******************************************************************
 * getConfigParamRecursive -- Llama a la auténtica función para 	*
 * 					obtener los parámetros de configuración			*
 *																						*
 *			p -- El parámetro a obtener (¿Uno o varios?)					*
 * 		a -- dirección del dispositivo (¿Seguro?)						*
 ******************************************************************/
CPConfigParamSeq SonyControlPlugin::getConfigParamRecursive(CPConfigParam p, 
																				Address *a 	)
{
#ifdef DEBUG
	cout << "-----> SonyCP:getConfigParamRecursive" << endl;
#endif

	SessionDispatcher ses = getSession(*a);
	int devId  = ses.getIdNumber();

#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParamRecursive" << endl;
#endif
	return this->getConfigParamRecursive(p, devId, a); 
}

/******************************************************************
 * getConfigParamRecursive -- (???)											*
 *																						*
 *			p -- El parámetro a configurar (¿Uno o varios?)				*
 *			devID -- identificador del dispositivo 						*
 * 		a -- dirección del dispositivo (¿Seguro?)						*
 ******************************************************************/
CPConfigParamSeq SonyControlPlugin::getConfigParamRecursive(	CPConfigParam p, 
																					int devId, 
																					Address *a 	)
{
#ifdef DEBUG
	if (a != NULL)
		cout << "-----> SonyCP:getConfigParamRecursive: " << devId << endl;
#endif

	RPC *rpc = db.getRPC(a, 5);

	if(rpc == NULL)
	{
#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParamRecursive" << endl;
#endif
		return CPConfigParamSeq();
	}
	try{
		CPConfigParamSeq seq;

		cout<<"getConfigParamRecursive("<<p.path<<") address:"<<(void*)a<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;

		if(modelConf->getNode(string("/[0]/Models/All")+p.path) != NULL )
		{
			int id=db.getModelConfigNode("SONY", cams[devId]->modelStr,  p.path, rpc);
					cout <<" -- /getConfigNodeModel:"<<id<<" :"<<p.path<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
			if(id!=-1)
			{
				string pref=p.path;
				int pos=pref.rfind("/");
				if(pos==pref.size()-1)
				{
					pref=pref.substr(0,pos);
					pos=pref.rfind("/");
				}

				if(pos!=string::npos)
					pref=pref.substr(0,pos); //pq ja l'afegeix el db.gcpr

				string s = string("SELECT * FROM getconfigparamrecursive(")+StrUtils::decToString(id)+string(",'")+pref+string("')"); 

				XML *res=db.call(s, 0, rpc);
				xmlNode *n=res->getNode("/result");

				int nrows=0;
				if(n!=NULL)
					nrows=n->getKidList().size();
				if(nrows==0 && n->getCdata()!=string(""))
				{
					cout<<" SCP::GetConfigParamRecursive() error resposta:"<<res->toString()<<endl;
					throw(ControlPluginException(0, string("Database error:")+(res->toString())));
				}

				for(int i=0; i<nrows; ++i)
				{
					seq.push_back(CPConfigParam( res->getNodeData("/result/["+StrUtils::decToString(i)+"]/name"), res->getNodeData("/result/["+StrUtils::decToString(i)+"]/value")));
				}

				delete res;
				res = NULL;
			}
		}

		cout<<" getConfigParamRecursive -- getDevConfigNode:"<<devId<<" path:"<<p.path<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
		int id=db.getDeviceConfigNode(devId, p.path, rpc);
		cout<<" getConfigParamRecursive -- /getDevConfigNode:"<<id<<" path:"<<p.path<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
		if(id!=-1)
		{
			string pref=p.path;
			int pos=pref.rfind("/");
			if(pos==pref.size()-1)
			{
				pref=pref.substr(0,pos);
				pos=pref.rfind("/");
			}

			if(pos!=string::npos)
				pref=pref.substr(0,pos);

			string s = string("SELECT * FROM getconfigparamrecursive(")+StrUtils::decToString(id)+string(",'")+pref+string("')");

			XML *res=db.call(s, 0, rpc);
			xmlNode *n=res->getNode("/result");

			int nrows=0;
			if(n!=NULL)
				nrows=n->getKidList().size();
			if(nrows==0 && n->getCdata()!=string(""))
			{
				cout<<" SCP::GetConfigParamRecursive() error resposta:"<<res->toString()<<endl;
				//				gcpr.unlock();
				throw(ControlPluginException(0, string("Database error:")+res->toString()));
			}

			for(int i=0; i<nrows; ++i)
			{
				seq.push_back(CPConfigParam( res->getNode("/result/["+StrUtils::decToString(i)+"]/name")->getCdata(), res->getNode("/result/["+StrUtils::decToString(i)+"]/value")->getCdata()));
			}

			delete res;
			res = NULL;
		}

		cout<<"/getConfigParamRecursive free rpc:"<<rpc->getLocalAddr().toString()<<endl;
		db.freeRPC(rpc);

#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParamRecursive" << endl;
#endif
		return seq;
	}
	catch(SocketTimeoutException &e)
	{
		//		gcpr.unlock();
		cout<<" getConfigParamRecursive: "<<devId<<":"<< p.path<<" Exception:"<<e.getClass()<<"::"<<e.getMsg()<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
		db.freeRPC(rpc);
#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParamRecursive" << endl;
#endif
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}
	catch(Exception &e)
	{
		cout<<" getConfigParamRecursive: "<<devId<<":"<< p.path<<" Exception:"<<e.getClass()<<"::"<<e.getMsg()<<" rpc:"<<rpc->getLocalAddr().toString()<<endl;
		db.freeRPC(rpc);
#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParamRecursive" << endl;
#endif
		throw;
	}
	catch(...)
	{
		db.freeRPC(rpc);
#ifdef DEBUG
	cout << "<----- SonyCP:getConfigParamRecursive" << endl;
#endif
		throw;
	}
}

/******************************************************************
 * getAllConfigParams -- Obtiene todos los parámetros de 			*
 *						configuración de un dispositivo 						*
 *																						*
 *			a -- dirección del dispositivo.	(¿Seguro?)					*
 ******************************************************************/
CPConfigParamSeq SonyControlPlugin::getAllConfigParams( Address *a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:getAllConfigParams" << endl;
#endif

	try{
		//	Address cam(IP(sessions[a->toString()].ip), sessions[a->toString()].port);
		SessionDispatcher ses=getSession(*a);

		CPConfigParamSeq seq;


		string s = string("SELECT m.raizinfo FROM modelo m, fabricante f WHERE m.fabricante=f.id AND f.nombre='SONY' AND m.codigo ='") + cams[ses.getIdNumber()]->modelStr + string("'");

		XML *res=db.call(s, 0);

		xmlNode *n=res->getNode("/result/row/raizinfo");
		if(n!=NULL)
		{
			string nodeid= (n->getCdata());

			s = string("SELECT * FROM getconfigparamrecursive(")+nodeid+string(",'')"); 
			res=db.call(s, 0);
			n=res->getNode("/result");

			int nrows=0;
			if(n!=NULL)
				nrows=n->getKidList().size();
			if(nrows==0 && n->getCdata()!=string(""))
			{
				cout<<" SCP::GetAllConfigParams() error resposta:"<<res->toString()<<endl;
			}

			for(int i=0; i<nrows; ++i)
			{
				seq.push_back(CPConfigParam( res->getNodeData("/result/["+StrUtils::decToString(i)+"]/name"), res->getNodeData("/result/["+StrUtils::decToString(i)+"]/value")));
			}
		}

		s = string("SELECT * FROM getallconfigparamsfordevice(")+StrUtils::decToString(ses.getIdNumber())+string(")"); 
		res=db.call(s, 0);
		n=res->getNode("/result");

		int nrows=0;
		if(n!=NULL)
			nrows=n->getKidList().size();
		if(nrows==0)
		{
			cout<<" SCP::GetAllConfigParams() error resposta:"<<res->toString()<<endl;
		}

		for(int i=0; i<nrows; ++i)
		{
			seq.push_back(CPConfigParam( res->getNodeData("/result/["+StrUtils::decToString(i)+"]/name"), res->getNodeData("/result/["+StrUtils::decToString(i)+"]/value")));
		}
#ifdef DEBUG
	cout << "<----- SonyCP:getAllConfigParams" << endl;
#endif
		return seq;
	}catch(SocketTimeoutException &e)
	{
#ifdef DEBUG
	cout << "<----- SonyCP:getAllConfigParams" << endl;
#endif
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}

}

/******************************************************************
 * run_move -- Establece sesión con la cámara para moverla			*
 *																						*
 * 		move -- Movimiento a realizar en el dispositivo.			*
 *			a -- dirección del dispositivo. (¿Seguro?)					*
 ******************************************************************/
void SonyControlPlugin::move( CPMovement move, Address *a )
{
#ifdef DEBUG
	cout << "-----> SonyCP:move" << endl;
#endif

	SessionDispatcher s = getSession(*a);
	cams[s.getIdNumber()]->queue(CPServ::move, (void *) new CPMovement(move));	

#ifdef DEBUG
	cout << "<----- SonyCP:move" << endl;
#endif
}

/******************************************************************
 * run_move -- Ejecuta el movimiento de una cámara. 					*
 *																						*
 * 		move -- Movimiento a realizar en el dispositivo.			*
 *			id -- id del dispositivo.											*
 ******************************************************************/
void SonyControlPlugin::run_move( CPMovement move, CPDeviceID id )
{
#ifdef DEBUG
	cout << "-----> SonyCP:run_move" << endl;
#endif

	CamInfo *ci = cams[id.id]; 
	Address cam=cams[id.id]->cam;
	short pan, tilt, zoom;

	float sx, sy, sz;
	RPC *rpc=db.getRPC(NULL, -1);
	if(rpc==NULL)
	{
#ifdef DEBUG
	cout << "<----- SonyCP:run_move" << endl;
#endif
		return;
	}
	ci->tour->resetRecall();
	if(move.x==0 && move.y==0 && move.z==0)
	{
		ci->tour->checkRecall();
		if(ci->tour->touring)
		{
			string rt = db.getDeviceConfigParam(id.id, string("/Presets/0/RecallTime"), rpc);
			if(rt != "")
			{
				int i=atoi(rt.c_str());
				if(i>0)
				{
					cout<<"hold Tour:"<<i<<endl;
					ci->tour->holdTour(i);
				}else
				{
					cout<<"stop Tour:"<<i<<endl;
					ci->tour->stopTour();
				}
			}else
			{
				cout<<"stop Tour(2):"<<endl;
				ci->tour->stopTour();
			}
		}
	}
	try
	{
		sx=atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Scale/x"), rpc).c_str());
		sy=atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Scale/y"), rpc).c_str());
		sz=atof(db.getModelConfigParam(string("SONY"), cams[id.id]->modelStr, string("/Scale/z"), rpc).c_str());
		db.freeRPC(rpc);
	}catch(Exception &e)
	{
		db.freeRPC(rpc);
#ifdef DEBUG
	cout << "<----- SonyCP:run_move" << endl;
#endif
		throw;
	}
	cout<<"run_move Pan:"<<move.x<<"		Tilt:"<<move.y<<"		Zoom"<<move.z<<"	sx:"<<sx<<" sy:"<<sy<< "sz:"<<sz<<endl;

	move.x*= sx;
	move.y*= sy;
	//	move.z*= -sz;


	switch (cams[id.id]->model)
	{
		case SNC_RZ50:
		case SNC_RZ25:
		case SNC_RX550: //???
			move.x=-move.x;
			move.y=-move.y;
		case SNC_RZ30:
		{
			pan=(int)floor(move.x*18.0);
			tilt=(int)floor(move.y*14.0);
			zoom=(int)floor(move.z*7.0);
			string cmd;

			if(pan>0)
				cmd="01";//right
			else if(pan==0)
				cmd="03";
			else
			{
				cmd="02";//left
				pan=-pan;
			}
			if(tilt>0)
				cmd+="02";//up
			else if(tilt==0)
				cmd+="03";
			else
			{
				cmd+="01";//down
				tilt=-tilt;
			}

			cmd=StrUtils::decToString(tilt)+cmd;
			if(tilt<10)
				cmd="0"+cmd;

			cmd=StrUtils::decToString(pan)+cmd;
			if(pan<10)
				cmd="0"+cmd;

			cmd="81010601"+cmd+"FF";

			char url[256];
			sprintf(url,"http://%s/command/visca-ptzf.cgi?visca=%s", cam.getIP().toString().c_str(), cmd.c_str());
			cout<<"move:"<<url<<endl;

			setConfigParamCamera(url, id.id);

			if(zoom>0)
			{
				cmd=string("810104072")+StrUtils::decToString(zoom)+string("FF");
			}
			else if(zoom==0)
			{
				cmd="8101040700FF";
			}else
			{
				zoom=-zoom;
				cmd=string("810104073")+StrUtils::decToString(zoom)+string("FF");
			}
			sprintf(url,"http://%s/command/visca-ptzf.cgi?visca=%s", cam.getIP().toString().c_str(), cmd.c_str());

			setConfigParamCamera(url, id.id);
			break;
		}
		case SNC_Z20:
		{
			zoom=(int)floor(move.z*7.0);

			string cmd;
			char url[256];
			if(zoom>0)
			{
				if(zoom>7) zoom=7;
				cmd=string("810104072")+StrUtils::decToString(zoom)+string("FF");
			}
			else if(zoom==0)
				cmd="8101040700FF";
			else
			{
				zoom=-zoom;
				if(zoom>7) zoom=7;
				cmd=string("810104073")+StrUtils::decToString(zoom)+string("FF");
			}
			sprintf(url,"http://%s/command/ptzf.cgi?visca=%s", cam.getIP().toString().c_str(), cmd.c_str());
			cout<<"zoom Z20:"<<url<<endl;

			setConfigParamCamera(url, id.id);
			break;
		}
		case SNC_RS86:
		case SNC_WR632:
		{
			pan=(int)floor(move.x*100.0);
			tilt=(int)floor(move.y*100.0);
			zoom=(int)floor(move.z*100.0);
			cout<<"run_move:"<<pan<<","<<tilt<<","<<zoom<<endl;
			if(pan>100)
				pan=100;
			if(pan<-100)
				pan=-100;
			if(tilt>100)
				tilt=100;
			if(tilt<-100)
				tilt=-100;
			if(zoom>100)
				zoom=100;
			if(zoom<-100)
				zoom=-100;

			cout<<"run_move:"<<pan<<","<<tilt<<","<<zoom<<endl;
			string cmd = "http://"+cam.getIP().toString()+"/command/ptzf.cgi?ContinuousPanTiltZoom="+StrUtils::decToString(pan)+","+StrUtils::decToString(tilt)+","+StrUtils::decToString(zoom);

			cout<<"run_move:"<<cmd<<endl;

			setConfigParamCamera(cmd, id.id);
			break;
		}
		case SNC_P1:
		case SNC_CS50:
		case SNC_CS3:
		case SNC_CS11:
		default:
		{}
	}

#ifdef DEBUG
	cout << "<----- SonyCP:run_move" << endl;
#endif
}


