#include <Plugins/GestorControlExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <L9EMBControlPlugin.h>
#include <ControlModule/ControlModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

L9EMBControlPlugin::DevInfo::DevInfo(	CPDeviceID id, 
													Address cam, 
													vector<DevInfo::servData> servList, 
													int size, L9EMBControlPlugin *cp	)
														:	id(id), 
															cam(cam), 
															servList(servList), 
															cp(cp), 
															run(false), 
															train(0), 
															camera(0), 
															activeId(-1)
{}

void* L9EMBControlPlugin::DevInfo::execute(int, void*)
{
	SocketTCP *socket=NULL;
	int servIdx=0;
	Address serv;
	int servId;

	while(run && cp->active)
	{
		do
		{
			try
			{
				serv = servList[servIdx].serv;
				servId = servList[servIdx].servId;
				this->activeId=servId;

				cout<<"L9EMBCP::DevInfo("<<id.id<<")::execute connecting to["<<servIdx<<"]:"<<serv.toString()<<endl;

				socket=cp->devConnect(serv);
				cout<<"L9EMBCP::DevInfo("<<id.id<<")::execute, connected to["<<servIdx<<"]:"<<serv.toString()<<endl;

				string resp = StrUtils::readLine(socket);
				cout<<"L9EMBCP::DevInfo("<<id.id<<")::execute "<<serv.toString()<<" connected:"<<resp<<" active:"<<activeId<<endl;
			}
			catch(Exception &e)
			{
				cout<<" L9EMBCP::DevInfo("<<id.id<<")::execute: Exception: "<<e.getClass()<<": "<<e.getMsg()<<" while connecting to:"<<serv.toString()<<endl;

				if(socket != NULL)
					delete socket;

				socket=NULL;
				++servIdx;

				if(servIdx>=servList.size())
				{
					sleep(10);
					servIdx=0;
				}

			}
			catch(std::exception &stde)
			{
				cout<<" L9EMBCP::DevInfo("<<id.id<<")::execute: std::exception: "<<stde.what()<<" while connecting to:"<<serv.toString()<<endl;

				if(socket != NULL)
					delete socket;

				socket=NULL;
				++servIdx;

				if(servIdx>=servList.size())
				{
					sleep(10);
					servIdx=0;
				}
			}
		}
		while(socket==NULL && run && cp->active);

		try
		{
			while(run && cp->active)
			{
				try
				{
					string resp = StrUtils::readLine(socket);//cp->readLine(socket);
					if(resp == string("ERROR\r\n"))
					{
						cout<<"L9EMBCP::DevInfo("<<id.id<<")::execute: command Error:"<<resp<<endl;
						lastResponse = resp;
						c.lock();
						c.signal();
						c.unlock();
					}
					else if (resp == string("OK\r\n"))
					{
						cout<<"L9EMBCP::DevInfo("<<id.id<<")::execute: command OK:"<<resp<<endl;
					}
					else if(resp.find("CLT_VISOR_ERR") != string::npos)
					{
						cout<<"L9EMBCP::DevInfo("<<id.id<<")::execute: command OK, answer:"<<resp<<endl;
						lastResponse = resp;
						c.lock();
						c.signal();
						c.unlock();
					}
					else if(resp.find("VISOR_ADMINDLG_CLT_WATCHDOG") == 0)
					{
						cout<<"L9EMBCP::DevInfo("<<id.id<<")::execute: VISOR_ADMINDLG_CLT_WATCHDOG keepalive:"<< resp<<endl;
					}
					else
					{
						cout<<"L9EMBCP::DevInfo("<<id.id<<")::execute: unknown answer:"<<resp<<endl;
					}
				}
				catch(SocketTimeoutException &e)
				{
					cout<<" L9EMBCP::DevInfo("<<id.id<<")::execute: Socket Timeout"<<endl;
				}

				if(command != string(""))
				{
					try
					{
						cout<<" L9EMBCP::DevInfo("<<id.id<<")::execute: command:"<<command<<endl;
						string cmd = command;
						socket->write((void*)cmd.c_str(), cmd.size());
						command = "";
					}
					catch(Exception &e)
					{
						cout<<" L9EMBCP::DevInfo("<<id.id<<")::command write exception: "<<e.getClass()<<": "<<e.getMsg()<<endl;
					}
					catch(std::exception &stde)
					{
						cout<<" L9EMBCP::DevInfo("<<id.id<<")::command write exception: "<<stde.what()<<endl;
					}
					catch(...){}
				}
			}
		}
		catch(Exception &e)
		{
			cout<<" L9EMBCP::DevInfo("<<id.id<<")::execute: Exception: "<<e.getClass()<<": "<<e.getMsg()<<", reconnecting..."<<endl;
			delete socket;
			socket = NULL;
			servIdx=0; //hem perdut el client a mig fer, comprovem en ordre
		}
		catch(...)
		{
			delete socket;
			socket = NULL;
			servIdx=0; //hem perdut el client a mig fer, comprovem en ordre
		}
	}

	try
	{
		socket->closeSocket();
	}catch(...){}
	delete socket;

	run=false;	
	return NULL;
}

void L9EMBControlPlugin::DevInfo::startRunning()
{
	if(run == false)
	{
		run=true;
		int id=this->start();
		this->detach(id);
	}
}

void L9EMBControlPlugin::DevInfo::stopRunning()
{
	run=false;	
}


string L9EMBControlPlugin::DevInfo::sendCommand(string cmd)
{
	c.lock();
	lastResponse = "";
	command = cmd;
	int totalWaited = 0;
	while(lastResponse == "" && totalWaited < maxCmdWait)
	{
		struct timespec t;
		t.tv_sec=time(NULL)+1;
		t.tv_nsec=0;
		c.wait(t);
		totalWaited++;
	}
	string resp = lastResponse;
	c.unlock();
	return resp;
}

int L9EMBControlPlugin::DevInfo::connect( int train, int cam )
{
	int servId = this->activeId;
	cout<<"devInfo::connect:active Id:"<<servId<<endl;
	string cmd = "";	
	if(cam != 0)
	{
		cmd = string("SRV_CAMERA_TO_OUTPUT(")+StrUtils::decToString(train)+string(",")+
			StrUtils::decToString(cam)+string(",")+StrUtils::decToString(servId)+
			string(",")+StrUtils::decToString(cp->L9EMBOper)+string(")\r\n");
	}
	else
	{
		cmd = string("SRV_CLEAR_OUTPUT(")+StrUtils::decToString(servId)+
			string(",")+StrUtils::decToString(cp->L9EMBOper)+string(")\r\n");
	}
	cout<<"L9EMBControlPlugin::DevInfo::connect("<<train<<","<<cam<<") : "<<cmd<<endl;
	
	string resp = sendCommand(cmd);
	cout<<"L9EMBControlPlugin::DevInfo::connect resp:"<<resp<<endl;
	int a = resp.find("(");
	int b = resp.find(")");
	if(a!=string::npos && b!=string::npos && a<b)
	{
		string codeStr = resp.substr(a+1,b-(a+1));
		int code = atoi(codeStr.c_str());
		cout<<"L9EMBControlPlugin::DevInfo::connect resp code:"<<codeStr<<":"<<code<<endl;
		if(cam != 0 && code == 23) //23:output already in use 
		{
			cout<<"L9EMBControlPlugin::DevInfo::connect output in use, clearing..."<<endl;
			string clearCmd = string("SRV_CLEAR_OUTPUT(")+StrUtils::decToString(servId)+
				string(",")+StrUtils::decToString(cp->L9EMBOper)+string(")\r\n");
			sendCommand(clearCmd);
			resp = sendCommand(cmd);
			int a = resp.find("(");
			int b = resp.find(")");
			if(a!=string::npos && b!=string::npos && a<b)
			{
				string codeStr = resp.substr(a+1,b-(a+1));
				cout<<"L9EMBControlPlugin::DevInfo::connect retry resp:"<<resp<<" code:"<<codeStr<<endl;
				int code = atoi(codeStr.c_str());
				this->train=train;
				this->camera=cam;
				return code;
			}else if(resp == "ERROR\r\n")
			{
				cout<<"L9EMBControlPlugin::DevInfo::connect retry error"<<endl;
				throw ControlPluginException("command_error");
			}
			cout<<"L9EMBControlPlugin::DevInfo::connect retry unknown answer:"<<resp<<endl;
			return -1;
		}else if(cam == 0 && (code == 4 || code == 17))//4:live stream not found  17:video connection lost
		{
			this->train=train;
			this->camera=cam;
			return 0;
		}
		this->train=train;
		this->camera=cam;
		return code;
	}else if(resp == "ERROR\r\n")
	{
		throw ControlPluginException("command_error");
	}
	cout<<"L9EMBControlPlugin::DevInfo::connect unknown response:"<<resp<<endl;
	return -1;
}

L9EMBControlPlugin::L9EMBControlPlugin(string file, bool reloadConfig):PluginControl(file), db(address, type, cn), L9EMBOper(1), active(false)
{
	XML *conf = getConfigFromFile( file );

	loading.lock();

	string operador=conf->getNodeData("[0]/L9Embarcat/Operador");
	if(operador != "")
	{
		L9EMBOper = atoi(operador.c_str());
		cout<<"ID d'Operador:"<<L9EMBOper<<endl;
	}

	delete conf;
	

	string s("Select d.id, d.ip, d.puerto, d.raizconfig, m.codigo FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='SONY' AND m.codigo='SNT-V704-L9EMB' ");
	
	XML *devInfo=db.call(s, 0);

	xmlNode *n=devInfo->getNode("/result");
	int numCams= atoi((n->getParam("numRows")).c_str());
	
	cout<<"loading "<< numCams<<" devices "<<endl;

	for(int i=0;i<numCams;i++)
	{
		int devId = -1;
		RPC *rpc = NULL;
		XML *conf=NULL;
		try
		{
			n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/id");
			devId = atoi(n->getCdata().c_str());
			n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/ip");
			
			string ip= n->getCdata().c_str();
			if(ip.find('/')!=string::npos) //eliminem mascara de xarxa
			{
				ip=ip.substr(0,ip.find('/'));
			}
			
			n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/puerto");
			int puerto = atoi(n->getCdata().c_str());
			
			n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/codigo");
			
			Address a(IP(ip), puerto);
			cout<<"address: "<< a.toString()<<endl;

			int mod=-1;

			n=devInfo->getNode("/result/["+StrUtils::decToString(i)+"]/raizconfig");
			int rconf = atoi(n->getCdata().c_str());
			rpc = db.getRPC(NULL,5);
			cout<<"loading config: "<< a.toString()<<endl;
			if(rpc != NULL)
				loadDevice(rpc, devId, a, rconf);
			else
				cout<<"could not load device config (db.getRPc == NULL)"<<endl;
		}
		catch(Exception &e)
		{
			cout<<"Exception loading config for device "<<devId<<" : "<<e.getClass()<<"::"<<e.getMsg()<<endl;
		}
		if(rpc != NULL)
		{
			db.freeRPC(rpc);
			rpc=NULL;
		}
		if(conf != NULL)
		{
			delete conf;
			conf=NULL;
		}
	}
	delete devInfo;
	loading.unlock();
	cout<<"----------plugin inicialitzat----------"<<endl;
}

XML* L9EMBControlPlugin::getConfigFromFile( string file )
{
#ifdef DEBUG
	cout << "-----> L9EMBCP::getConfigFromFile: " << file << endl;
#endif
	
	char* buffer(NULL);
	std::size_t size(0);

	ifstream fichero( file.c_str(), ifstream::in );

	if ( fichero.is_open() )
	{
  		filebuf *pbuffer = fichero.rdbuf();
  		size = pbuffer->pubseekoff (0, fichero.end, fichero.in);
  		pbuffer->pubseekpos (0, fichero.in);

  		buffer = new char[size];
  		pbuffer->sgetn (buffer, size);
	}
	else
		cout << "Error opening file '" << file.c_str() << endl;

	fichero.close();

	XML *conf = xmlParser::parse( string( buffer, size ) );

	delete buffer;
	buffer = NULL;

#ifdef DEBUG
	cout << "<----- L9EMBCP::getConfigFromFile" <<  endl;
#endif

	return conf;
}

L9EMBControlPlugin::~L9EMBControlPlugin()
{
	for(map<int, DevInfo*>::iterator it=devs.begin(); it!=devs.end(); it++)
	{
		if(it->second!=NULL)
			delete it->second;
	}
}

void L9EMBControlPlugin::loadDevice(RPC *rpc, int devId, Address a, int rconf)
{
	vector<DevInfo::servData> servList;	
	int id=db.getConfigNode(rconf, "/Video/L9Embarcat/", rpc);
	if(id!=-1)
	{
		string s = string("SELECT * FROM getconfigparamrecursive(")+StrUtils::decToString(id)+string(",'') order by name");
		cout<<"load:"<<s<<endl;
		conf=db.call(s, 0, rpc);
		xmlNode *n=conf->getNode("/result");
		if(n==NULL)
		{
			throw Exception("Config not loaded");
		}
		string ip="";
		int port=-1;
		int servId=-1;
		int nrows=n->getKidList().size();
		//config base a /Video/L9Embarcat
		cout<<"parsing "<<nrows<<endl;
		for(int i=0;i<nrows;i++)
		{
			try
			{
				string name = "";
				string val = "";

				n = conf->getNode("/result/["+StrUtils::decToString(i)+"]/name");
				if(n!=NULL)
					name = n->getCdata();
				n = conf->getNode("/result/["+StrUtils::decToString(i)+"]/value");
				if(n!=NULL)
					val = n->getCdata();
				if(name == string("/L9Embarcat/IP"))
				{
					ip = val;
				}else if(name == "/L9Embarcat/Port")
				{
					port = atoi(val.c_str());
				}else if(name == "/L9Embarcat/Id")
				{
					servId = atoi(val.c_str());
				}
			}
			catch(Exception &e)
			{
				cout<<"Exception parsing primary server for device "<<devId<<" : "<<e.getClass()<<"::"<<e.getMsg()<<" config ip:"<<ip<<" port:"<<port<<" id:"<<id<<endl;
			}
			catch(std::exception &stde)
			{
				cout<<"std::exception parsing primary server for device "<<devId<<" : "<<stde.what()<<" config ip:"<<ip<<" port:"<<port<<" id:"<<id<<endl;
			}
		}

		if(ip != string("") && port != -1 && servId != -1)
		{
			try
			{
				DevInfo::servData serv(Address(ip, port), servId);
				servList.push_back(serv);
				cout <<" primary server for dev "<<devId<<" @"<<ip<<":"<<port<<" id:"<<servId<<endl;
			}
			catch(Exception &e)
			{
				cout<<"Exception parsing primary server for device "<<devId<<" : "<<e.getClass()<<"::"<<e.getMsg()<<" config ip:"<<ip<<" port:"<<port<<" id:"<<id<<endl;
			}
		}else if(ip != string("") || port != -1 || servId != -1)
		{
			cout<<"Exception parsing primary server for device "<<devId<<" ip:"<<ip<<" port:"<<port<<" id:"<<servId<<endl;
		}

		//configs extres a /Video/L9Embarcat/n/
		//	nrows=n->getKidList().size();
		ip="";
		port=-1;
		servId=-1;
		string index = "";
		cout<<"parsing "<<nrows<<" again"<<endl;
		for(int i=0;i<nrows;i++)
		{
			string name = "";
			string val = "";
			try
			{
				n = conf->getNode("/result/["+StrUtils::decToString(i)+"]/name");
				if(n!=NULL)
					name = n->getCdata();
				n = conf->getNode("/result/["+StrUtils::decToString(i)+"]/value");
				if(n!=NULL)
					val = n->getCdata();
				//cout<<"node:"<<name<<":"<<val<<endl; 
				int pos = name.rfind("/IP");
				string currentIdx="";
				int found=0;
				if(pos != string::npos)
				{
					found=1;
				}else
				{
					pos = name.rfind("/Port");
					if(pos != string::npos)
					{
						found=2;
					}else
					{
						pos = name.rfind("/Id");
						if(pos != string::npos)
						{
							found=3;
						}
					}
				}

				if(pos != string::npos && pos > 12) // "/L9Embarcat/".length() = 12
				{
					currentIdx = name.substr(0,pos).substr(12);
					if(currentIdx != string(""))
					{
						switch (found)
						{
							case 1:
								ip = val;
								break;
							case 2:
								port = atoi(val.c_str());
								break;
							case 3:
								servId = atoi(val.c_str());
								break;
						}

						if(index != string("") && index != currentIdx)
						{
							cout<<"warning: incomplete config for device:"<<devId<<" at /L9Embarcat/"<<index<<endl;
							if(found!=1)
								ip="";
							if(found!=2)
								port=-1;
							if(found!=3)
								servId=-1;
						}
						index=currentIdx;
					}
				}
			}
			catch(Exception &e)
			{
				cout<<"Exception parsing server for device "<<devId<<" : "<<e.getClass()<<"::"<<e.getMsg()<<endl;
				cout<<"				name:"<<name<<" value:"<< val<<" config index:"<<index<<" ip:"<<ip<<" port:"<<port<<" id:"<<devId<<endl;
			}
			catch(std::exception &stde)
			{
				cout<<"std::exception parsing server for device "<<devId<<" : "<<stde.what()<<endl;
				cout<<"				name:"<<name<<" value:"<< val<<" config index:"<<index<<" ip:"<<ip<<" port:"<<port<<" id:"<<devId<<endl;
			}
			if(ip != string("") && port != -1 && servId != -1)
			{
				cout <<"new secondary server for dev "<<devId<<" @"<<ip<<":"<<port<<" id:"<<servId<<" index:"<<index<<endl;
				try
				{
					L9EMBControlPlugin::DevInfo::servData serv(Address(ip, port), servId);
					servList.push_back(serv);
				}
				catch(Exception &e)
				{
					cout<<"Exception parsing server for device "<<devId<<" : "<<e.getClass()<<"::"<<e.getMsg()<<" config ip:"<<ip<<" port:"<<port<<" id:"<<id<<endl;
				}
				ip="";
				port=-1;
				servId=-1;
				index = "";
			}
		}
		if(servList.size()==0)
			cout<<"Error: no servers found for device:"<<devId<<":"<<a.toString()<<", config:"<<(conf==NULL?string("NULL"):conf->toString())<<endl;
		else
		{
			devs[devId]= new DevInfo(devId, a, servList, default_buffer_size, this);
			//devs[devId]->start();
		}
	}
	else
	{
		cout<<"could not load device config (no '/Video/L9Embarcat/' node found)"<<endl;
	}

	cout<<"Loaded device:"<<a.toString()<<" servs:"<<servList.size()<<endl;
}


SessionDispatcher L9EMBControlPlugin::getSession(Address a)
{
	string k=a.toString();
	map<string,SessionDispatcher>::iterator i=sessions.find(k);
	
	if (i==sessions.end())
	{
		cout<<"L9EMBCP::dwiSession Not Stablished "<<endl;
		throw (CPSessionNotStablishedException(0, string("Session no establerta")));
	}
	i->second.initializeTimeOfSession();
	return i->second;
}

SocketTCP* L9EMBControlPlugin::devConnect(Address a)
{
	SocketTCP *sock = new SocketTCP(a);
	sock->establish();
	sock->setTimeout(1000);
	//TODO: llegir resposta de connexió?
	return sock;
}

string L9EMBControlPlugin::getErrorStr(int code)
{
	if(code == 0)
	{
		return string("no_error");	
	}else if(code == 1)
	{
		return string("train_not_found");
	}else if(code == 2)
	{
		return string("camera_not_found");
	}else if(code == 3)
	{
		return string("operator_not_found");
	}else if(code == 4)
	{
		return string("live_stream_not_found");
	}else if(code == 6)
	{
		return string("max_connections_reached");
	}else if(code == 7)
	{
		return string("max_connection_reached_for_this_train");
	}else if(code == 8)
	{
		return string("max_streams_for_the_streamer");
	}else if(code == 9)
	{
		return string("already_connected");
	}else if(code == 10)
	{
		return string("max_streams_reached");
	}else if(code == 14)
	{
		return string("server_on_standby_mode");
	}else if(code == 16)
	{
		return string("database_recovery_in_progress"); //retry_later
	}else if(code == 17)
	{
		return string("video_connection_lost");
	}else if(code == 18)
	{
		return string("too_many_commands");
	}else if(code == 19)
	{
		return string("max_simultaneous_streams_for_DVR");
	}else if(code == 20)
	{
		return string("too_many_commands_from_client");
	}else if(code == 21)
	{
		return string("output_not_found");
	}else if(code == 22)
	{
		return string("output_changed");
	}else if(code == 23)
	{
		return string("output_already_in_use");
	}else if(code == 30)
	{
		return string("VW_equipment_is_unreachable");
	}else if(code == 31)
	{
		return string("output_switch_error");
	}else if(code == 32)
	{
		return string("internal_error");
	}else// if(code == 255)
	{
		return string("unknown_error");
	}
}

void L9EMBControlPlugin::startPriorityService()
{
	Plugin::setActive(true);
	this->cn->startSend();
	cout<<"L9EMBControlPlugin::startPriorityService:"<<devs.size()<<endl;
	loading.lock();
	try
	{
		this->active=true;
	
		for(map<int, DevInfo*>::iterator it=devs.begin(); it!=devs.end(); it++)
		{
			cout<<"		dev:"<<it->first<<endl;
			((DevInfo*)it->second)->startRunning();
		}
	}
	catch(Exception &e)
	{
		cout<<" L9EMBCP::serveStartUp: Exception: "<<e.getClass()<<": "<<e.getMsg()<<endl;
	}catch(std::exception &stde)
	{
		cout<<" L9EMBCP::serveStartUp: std::exception:"<<stde.what()<<endl;
	}catch(...){}
	loading.unlock();
}

void L9EMBControlPlugin::stopPriorityService()
{
	Plugin::setActive(false);
	this->cn->stopSend();
	cout<<"L9EMBControlPlugin::stopPriorityService:"<<devs.size()<<endl;
	this->active=false;
}

void L9EMBControlPlugin::startSession(CPDeviceID id, Address *a)
{	
	map<string,SessionDispatcher>::iterator itSes=sessions.find(a->toString());
	bool sessionEnded=false;
	int oldSesID=0;
	
	cout<<"RecordCamCP::StartSession:"<<id.id<<endl;
	
	if(itSes!=sessions.end())
	{
		if(id.id ==itSes->second.getIdNumber())
		{
			cout<<" Session Already Stablished "<<a->toString()<<"  id:"<<id.id<<"   ses:"<<itSes->second.getIdNumber()<<endl;
			throw(CPSessionAlreadyStablishedException(1, " Sessio ja establerta"));
		}
		else
		{
			oldSesID=itSes->second.getIdNumber();
			sessionEnded=true;
			try{
				endSession(itSes->second.getIdNumber(), a);
			}catch(Exception &e){}
		}
	}

	map<int, DevInfo*>::iterator itBuff=devs.find(id.id);
	
	Address cam;
	bool inited=false;
	if(itBuff!=devs.end())
	{
		cam=itBuff->second->cam;
	}
	else
	{
		//no es a la llista de cameres, mirem si l'han afegit al sistema des de la inicialitzacio
		//i l'afegim
		cout<<"loading camera "<<StrUtils::decToString(id.id)<<endl;
		loading.lock();
		RPC *rpc=db.getRPC(a,5);
		try
		{

			string s("Select d.id, d.ip, d.puerto, d.raizconfig, m.codigo FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='SONY' AND m.codigo='SNT-V704-L9EMB' AND d.id = ");
			s+=StrUtils::decToString(id.id);

			XML *devInfo=db.call(s, 0, rpc);
			xmlNode *n=devInfo->getNode("/result/row/ip");

			cout<<"loading camera "<<StrUtils::decToString(id.id)<<" config:"<<devInfo->toString()<<endl;

			if(n==NULL) //no es enlloc, dispositiu invalid
			{
				db.freeRPC(rpc);
				throw(CPInvalidParamException(0, "Dispositiu invalid"));
			}

			string sip=n->getCdata();

			if(sip.find('/')!=string::npos)
			{
				sip=sip.substr(0,sip.find('/'));
			}

			IP ip(sip);

			int port=atoi(devInfo->getNodeData("/result/row/puerto").c_str());
			cam=Address(ip, port);

			n=devInfo->getNode("/result/row/raizconfig");
			int rconf = atoi(n->getCdata().c_str());

			cout<<"loadDevice("<<(void*) rpc<<","<<id.id<<","<<cam.toString()<<","<<rconf<<")"<<endl;
			loadDevice(rpc, id.id, cam, rconf);
			delete devInfo;
			db.freeRPC(rpc);
			loading.unlock();
		}catch(SocketTimeoutException &e)
		{
			//		gcpr.unlock();
			db.freeRPC(rpc);
			loading.unlock();
			throw(ControlPluginException(e.getCode(), e.getMsg()));
		}catch(...)
		{
			//		gcpr.unlock();
			db.freeRPC(rpc);
			loading.unlock();
			throw(ControlPluginException(0, string("Invalid device id:")+StrUtils::decToString(id.id)));
		}

	}

	SessionDispatcher ses(cam, id);
	ses.controlPlugin = new ControlModuleAccess(string("ControlPlugin::SONY"), cn);
	cout<<" StartSession for:"<<a->toString()<<" dev:"<<id.id<<endl;
	ses.controlPlugin->startSession(id);

	sessions[a->toString()]= ses;
	if(sessionEnded)
		throw(CPSessionAlreadyStablishedException(2, string("Sessio finalitzada pel dispositiu : ")+StrUtils::decToString(oldSesID)));

}

void L9EMBControlPlugin::endSession(CPDeviceID id, Address *a)
{
	map<string,SessionDispatcher>::iterator i=sessions.find(a->toString());
	cout<<" L9EMB::endSession: "<<id.id<<" : "<<i->second.getIpAddress().toString()<<endl;
	
	if(i==sessions.end())
	{
		throw(CPSessionNotStablishedException(0, " Sessio ja establerta"));
	}
	
	i->second.controlPlugin->endSession(id);
	delete i->second.controlPlugin;
	sessions.erase(i);
	struct timeval now;
	gettimeofday(&now,NULL);
	//map<string,SessionDispatcher>::iterator 
	i= sessions.begin(); 
	while(i!=sessions.end())
	{
		struct timeval diffTime = now - i->second.getTimeOfCreationOfThisSession();
		if ( 	diffTime.tv_sec > sessionTimeout )
			sessions.erase(i++);
		else
			i++;
	}

}


CPPTZ L9EMBControlPlugin::getPTZ(Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getPTZ: "<<ses.getIpAddress().toString()<<endl;
	
	return ses.controlPlugin->getPTZ();
}

void L9EMBControlPlugin::setPTZ(CPPTZ ptz, Address *a)
{	
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::setPTZ: "<<ses.getIpAddress().toString()<<endl;
	ses.controlPlugin->setPTZ(ptz);
}


void L9EMBControlPlugin::setInput(CPInput in, Address *a)
{
	SessionDispatcher ses = getSession(*a);

	cout<<" L9EMB::setInput("<<in.id<<","<<in.value<<"): "<<ses.getIpAddress().toString()<<endl;
	
	if(in.id == L9EMBInputID)
	{
		int tren = in.value/1000;
		int cam = in.value % 1000;
		int error = devs[ses.getIdNumber()]->connect(tren, cam);
		if(error != 0)
		{
			string exc = getErrorStr(error);
			throw ControlPluginException(exc);
		}
	}
	else
		ses.controlPlugin->setInput(in);
}


CPInput L9EMBControlPlugin::getInput(CPInput in, Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getInput: "<<ses.getIpAddress().toString()<<" "<<in.id<<endl;
	if(in.id == L9EMBInputID)
	{
		in.value = devs[ses.getIdNumber()]->train*1000 + devs[ses.getIdNumber()]->camera;
		return in;
	}
	else
		return ses.controlPlugin->getInput(in);
}

void L9EMBControlPlugin::setOutput(CPOutput out, Address *a) 
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::setOutput("<<out.id<<" "<<out.value<<") "<<ses.getIpAddress().toString()<<endl;

	ses.controlPlugin->setOutput(out);
}

CPOutput L9EMBControlPlugin::getOutput(CPOutput out, Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getOutput("<<out.id<<") "<<ses.getIpAddress().toString()<<endl;
	return ses.controlPlugin->getOutput(out);
}

void L9EMBControlPlugin::setCommandBufferSize(CPCommandBufferSize size, Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::setCommandBufferSize: "<<ses.getIpAddress().toString()<<endl;
	
	ses.controlPlugin->setCommandBufferSize(size);
}

CPCommandBufferSize L9EMBControlPlugin::getCommandBufferSize(Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getCommandBufferSize: "<<ses.getIpAddress().toString()<<endl;
	return ses.controlPlugin->getCommandBufferSize();
}


float L9EMBControlPlugin::getCommandBufferPercentInUse(Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getCommandBufferPercentInUse: "<<ses.getIpAddress().toString()<<endl;
	return ses.controlPlugin->getCommandBufferPercentInUse();
}

int L9EMBControlPlugin::getCommandBufferCommandsInUse(Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getCommandBufferCommandsInUse: "<<ses.getIpAddress().toString()<<endl;
	return ses.controlPlugin->getCommandBufferCommandsInUse();
}


CPMetadata L9EMBControlPlugin::getMetadataValue(CPMetadata data, Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getMetadataValue: "<<ses.getIpAddress().toString()<<endl;
	return ses.controlPlugin->getMetadataValue(data);
}

void L9EMBControlPlugin::setMetadataValue(CPMetadata data, Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::setMetadataValue: "<<ses.getIpAddress().toString()<<endl;
	
	ses.controlPlugin->setMetadataValue(data);
}


CPConfigParam L9EMBControlPlugin::getConfigParam(CPConfigParam p, Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getConfigParam: "<<ses.getIpAddress().toString()<<endl;

	return ses.controlPlugin->getConfigParam(p);
}

void L9EMBControlPlugin::setConfigParam(CPConfigParam p, Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::setConfigParam: "<<ses.getIpAddress().toString()<<endl;
	
	return ses.controlPlugin->setConfigParam(p);
}


CPConfigParamSeq L9EMBControlPlugin::getConfigParamRecursive(CPConfigParam p, Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getConfigParamRecursive: "<<ses.getIpAddress().toString()<<endl;

	return ses.controlPlugin->getConfigParamRecursive(p);
}

CPConfigParamSeq L9EMBControlPlugin::getAllConfigParams(Address *a)
{
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::getAllConfigParams: "<<ses.getIpAddress().toString()<<endl;
	return ses.controlPlugin->getAllConfigParams();
}

void L9EMBControlPlugin::move(CPMovement move, Address *a)
{	
	SessionDispatcher ses=getSession(*a);
	cout<<" L9EMB::move: "<<ses.getIpAddress().toString()<<endl;

	ses.controlPlugin->move(move);
}




