/*********************************************************************
 * L9EMBControlPlugin.h                                              *
 * -----------------------------------------------------------------	*
 *	Marina Eye-Cam Technologies, S.L. All rights reserved.				*
 *********************************************************************/
#ifndef L9EMBCONTROLPLUGIN_H
#define L9EMBCONTROLPLUGIN_H

#ifndef WIN32
#include <sys/time.h>
#else
#include <Utils/WindowsDefs.h>
#endif
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <Plugins/PluginControl.h>
#include <Plugins/GestorControlTypes.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <Utils/SessionDispatcher.h>
#include <Sockets/SocketTCP.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <Threads/Mutex.h>
#include <map>
#include <vector>
#include <iostream>
using namespace std;


class L9EMBControlPlugin: public PluginControl
{
	static const int V704_L9EMB=0;

	map<string, SessionDispatcher> sessions;
	static const int sessionTimeout=600;
	static const int L9EMBInputID=100;
	static const int maxCmdWait=10;
	int L9EMBOper;

	class DevInfo: public Thread
	{
		private:
			L9EMBControlPlugin *cp;
			int activeId;
		public:
			class servData
			{
				public:
				Address serv;
				int servId;

				servData(Address serv, int id):serv(serv), servId(id){};
			};
			int nin, nout, nrel;
			Mutex callMutex;

			CPDeviceID id;
			Address cam;
			vector<servData> servList;
			bool error_logged;
			int train, camera; 
			bool run;

			string command, lastResponse;
			Mutex cmdlock;
			Condition c;


			DevInfo(CPDeviceID id, Address cam, vector<DevInfo::servData> servList, int size, L9EMBControlPlugin *cp);
			virtual void* execute(int, void*);
			void startRunning();
			void stopRunning();

			string sendCommand(string s);

			int connect(int train, int cam);
			void clear();
	};
	
	map<int, DevInfo*> devs;

	static const int default_buffer_size=5;

protected:
	XML *conf;
	Mutex loading;

	SessionDispatcher getSession(Address a);
	SocketTCP* devConnect(Address a);
	string getErrorStr(int code);

public:
	bool active;
	DBGateway db;
	L9EMBControlPlugin(string file, bool reloadConfig);
	~L9EMBControlPlugin();

	virtual void startPriorityService();		
	virtual void stopPriorityService();		
	XML* getConfigFromFile( string file );

	void loadDevice(RPC *rpc, int devId, Address a, int rconf);

	virtual void startSession(CPDeviceID id, Address *a);
	virtual void endSession(CPDeviceID id, Address *a);

	virtual CPPTZ getPTZ(Address *a);
	virtual void setPTZ(CPPTZ ptz, Address *a);

	virtual void setInput(CPInput in, Address *a);
	void run_setInput(CPInput in, CPDeviceID id, Address cam);
	virtual CPInput getInput(CPInput in, Address *a);

	virtual void setOutput(CPOutput out, Address *a);
	void run_setOutput(CPOutput in, CPDeviceID id, Address cam);
	virtual CPOutput getOutput(CPOutput out, Address *a);

	virtual void setCommandBufferSize(CPCommandBufferSize size, Address *a);
	virtual CPCommandBufferSize getCommandBufferSize(Address *a);

	virtual float getCommandBufferPercentInUse(Address *a);
	virtual int getCommandBufferCommandsInUse(Address *a);

	virtual CPMetadata getMetadataValue(CPMetadata data, Address *a);
	virtual void setMetadataValue(CPMetadata data, Address *a);

	virtual CPConfigParam getConfigParam(CPConfigParam p, Address *a);
	virtual CPConfigParamSeq getConfigParamRecursive(CPConfigParam p, Address *a);
	virtual void setConfigParam(CPConfigParam p, Address *a);
	virtual CPConfigParamSeq getAllConfigParams(Address *a);
	
	virtual void move(CPMovement move, Address *a);
};

#endif