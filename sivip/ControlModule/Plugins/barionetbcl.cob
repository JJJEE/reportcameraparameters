CoB1
�   alarms.dat<Z�  Barionet.bas !a  BARIONET.TOK>5�  barionetMarina.jar	�.O�  cards.dat% 4�  datasize.dat
{Y�  ERRORS.HLP���  ips.dat
���  reader.dat
�	v�  status.xml�1	 uixmlstatus.html <*>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                '========================================================================
' Control d'accessos.
' 	- Port TCP 10005 - Comandes per donar d'alta o borrar IPs i accions de targes
'		- "0x00000000000000" => delete all IPs
'		- "0x00aabbccddpppp" => delete ip a.b.c.d:p
'		- "0x10aabbccddpppp" => add ip a.b.c.d:p
'		- "0x2f000000000000" => delete all Cards
'		- "0x2Rtttttttt0000" => Eliminar funcio tarja 0xtttttttt del reader R
'		- "0x3RttttttttXXnr" => Canvis al relay r quan es detecti tarja t al reader R
'			- "0x3Rtttttttt00nr" => sortida r (0-7) o relay r (a-f (10-11)) a n(0/1)
'			- "0x3Rttttttttssnr" => n pulses sortida/relay r, ss decimes de segon
'		- "0x40f00000000000" => Eliminar totes les alarmes
'		- "0x40i00000000000" => Eliminar alarmes per input i (0-7)
'		- "0x50ivvv____XXnr" => Afegir alarma per input i, valor v, mode i comparacio ____. Si es dona, fer accio XXnr (com accions targes), i tambe es notifica per UDP.
'		 input i =   1 "bit" per indicar si volem comprovar que s'ha passat 
' 						alguna tarja ebn els ultims __ segons (els dels 11 bits)
'					 3 "bits" per nº input
'			____ = 	 2 "bits" mode (00 = Analog, 01 = Digital, 10 = Temperature)
'					 3 "bits" mascara de comparacio 001 =, 010 >, 100 <
'					11 "bits" decimes de segon que cal que es compleixi per activar alarma
'========================================================================

'---------------------------
' Variables
'---------------------------

	DIM rdr$(5)				' interface amb el lector
	DIM cmdPort$(18)		' On escoltar info de comandes
	
	' Buffers diversos
	DIM rdrBuf$(16)
	DIM cmdBuf$(15)
	DIM tmpBuf$(256)
	DIM reg$(32)
	DIM cmd$(32)
	DIM char$(32)
	DIM ip$(32)
	DIM crd$(32)
	DIM cCmd$(32)
	DIM card$(32)
	DIM cInf0$(32)
	DIM cInf1$(32)
	DIM rgCrd$(32)
	DIM valor$(32)
	DIM relay$(32)
	DIM fileBuf$(32)
	
	DIM par1$(32)		' Parametre 1 string, "en general"
	DIM ret$(32)		' Retorn string
	DIM par1			' Parametre 1 numeric
	DIM fni, ret		' enter temporal(funcions), i valor de retorn
	
	' Llistes d'IPs i targes(+ lectors), 100 i 750 unitats de cada
	DIM _mIPList$(1200), _mcrdList$(9000), _mrdrList$(750)

	' Llista d'alarmes (max 128, 6 bytes/alarma, 16 alarmes per cada input de mitja)
	DIM _malList$(768)
	DIM alMillis(128)

	' Valors entrades
	DIM inps(8,3)	' 8 inputs, als tres modes

	' altres temporals gansos
	DIM _mdumpTmp$(1024)

	DIM ipInfSize, cInfSize, aInfSize, ipListLen, crdListLen, alListLen
	DIM reader, ipCount, crdCount, alCount
	DIM rdrLen, cmdLen, i, c, off, totLen, found, replaced, matched, v, r, dSecs
	DIM alData, alCrd, alInp, alVal, alMode, alComp, alDecs, alTmp, alAction
	DIM cBit0, cBit1, cBit2, cRes0, cRes1, cRes2, deleted
	DIM a, size
	DIM lastTmr, crdTmr, curTmr, crdOffTmr(128)
	DIM a2, alCd2, alDc2, alIn2, alMd2, off2, doAct, noAl, noAlTmr, maxAlDecs
	
	ipInfSize=12
	cInfSize=12
	aInfSize=6
	
	ipListLen=1200/ipInfSize
	crdListLen=9000/cInfSize
	alListLen=768/aInfSize

	ipCount=0
	crdCount=0
	alCount=0

	rdr$ = "RDR:"
	cmdPort$="TCP:0.0.0.0:10005"	

'---------------------------
' Inici programa
'---------------------------

	' Llegim les targes i IPs registrades
	syslog "Marina Eye-Cam Access Control BCL controller"

	for i=1 to alListLen
		alMillis(i)=0
		crdOffTmr(i)=0
	next i
	noAl=0
	noAlTmr=_TMR_(0)
	maxAlDecs=-1
	lastTmr=_TMR_(0)
	crdTmr=-1

	syslog "["+sprintf$("%3t", _TMR_(0))+"] Loading cards"
	gosub 9040
	syslog "["+sprintf$("%3t", _TMR_(0))+"] Loading IPs"
	gosub 9060
	syslog "["+sprintf$("%3t", _TMR_(0))+"] Loading Alarms"
	gosub 9080

	syslog "["+sprintf$("%3t", _TMR_(0))+"] Opening devices"
    Open rdr$ as 0
    Open cmdPort$ as 1
	open "UDP:0.0.0.0:10006" AS 7
	
'---------------------------
' Bucle principal
'---------------------------

	syslog "["+sprintf$("%3t", _TMR_(0))+"] Starting operation"

10	' Llegim del lector, del port de comandes, i els valors de les entrades
	read 0,rdrBuf$,1
    rdrLen=lastlen(0)
    
    read 1,cmdBuf$,1
    cmdLen=lastlen(1)
	
	if alCount > 0 then
	    ' Gestio d'alarmes
    	gosub 10600
	else
		crdTmr=-1
    endif
	lastTmr=curTmr
	curTmr=_TMR_(0)
    
    ' Si no hi ha cap lectura, tornem a llegir...
    if AND(rdrLen=0, cmdLen=0) then goto 10

	' Lectura al port de comandes
	if cmdLen>0 then
		tmpBuf$=cmdBuf$
		syslog "["+sprintf$("%3t", _TMR_(0))+"] Received info on command port ("+STR$(cmdLen)+" bytes): "+tmpBuf$, 0

		if cmdLen=14 then
			cmd$=mid$(cmdBuf$, 1, 1)
			if cmd$="0" then gosub 10100
			if cmd$="1" then gosub 10200
			if cmd$="2" then gosub 10300
			if cmd$="3" then gosub 10400
			if cmd$="4" then gosub 10700
			if cmd$="5" then gosub 10800
		else
			syslog "["+sprintf$("%3t", _TMR_(0))+"] Invalid command", 0
		endif
	endif
        
	' Lectura al lector
	if rdrLen>0 then
		tmpBuf$=""
		for i=1 to rdrLen
			tmpBuf$=tmpBuf$+sprintf$("%02x", midget(rdrBuf$, i, 1))
		next i
		syslog "["+sprintf$("%3t", _TMR_(0))+"] Received info on Reader port ("+STR$(rdrLen)+" bytes): "+tmpBuf$, 0
		if rdrLen=5 then gosub 10500
	endif

    goto 10


'=============================================
' Subrutines
'=============================================

'---------------------------
' Dump llista targes
'---------------------------

9000
	 _mdumpTmp$=""
	for c=1 to crdCount
		off=1+(c-1)*cInfSize

		_mdumpTmp$=_mdumpTmp$+mid$(_mcrdList$, off, cInfSize)+"("+mid$(_mrdrList$, c, 1)+"),"
	next c 
	syslog "Card List: "+_mdumpTmp$, 0
	return

'---------------------------
' Converteix una cadena HEX en el seu valor enter
'---------------------------
9010
	ret=0
	
	for fni=1 to len(par1$)
		char$=lcase$(mid$(par1$, fni, 1))
		
		if and(asc(char$)>=asc("0"), asc(char$)<=asc("9")) then
			ret = shl(ret, 4)
			ret = ret + asc(char$) - asc("0")
		else
			if asc(char$)<=asc("f") then
				ret = shl(ret, 4)
				ret = ret + 10 + asc(char$) - asc("a")
			endif
		endif
	next fni
	return

'---------------------------
' Notificar lectura tarja + granted/no granted a la llista d'IPs
'---------------------------

9020
	for i=1 to ipCount
		off=1+(i-1)*ipInfSize
		
		par1$=mid$(_mipList$, off, 2)
		gosub 9010
		ip$=STR$(ret)

		par1$=mid$(_mipList$, off+2, 2)
		gosub 9010
		ip$=ip$+"."+STR$(ret)

		par1$=mid$(_mipList$, off+4, 2)
		gosub 9010
		ip$=ip$+"."+STR$(ret)

		par1$=mid$(_mipList$, off+6, 2)
		gosub 9010
		ip$=ip$+"."+STR$(ret)

		par1$=mid$(_mipList$, off+8, 4)
		gosub 9010
		
		if matched=0 then
			tmpBuf$=card$+"|"+STR$(reader)+"|"+STR$(matched)
		else
			tmpBuf$=card$+"|"+STR$(reader)+"|"+STR$(matched)+"|"+cInf0$+cInf1$
		endif
		write 7,tmpBuf$,len(tmpBuf$),ip$,ret
		
		syslog "Notify: "+ip$+":"+STR$(ret), 0
	next i
	return

'---------------------------
' Notificacio alarma
'---------------------------
9022
	for i=1 to ipCount
		off=1+(i-1)*ipInfSize
		
		par1$=mid$(_mipList$, off, 2)
		gosub 9010
		ip$=STR$(ret)

		par1$=mid$(_mipList$, off+2, 2)
		gosub 9010
		ip$=ip$+"."+STR$(ret)

		par1$=mid$(_mipList$, off+4, 2)
		gosub 9010
		ip$=ip$+"."+STR$(ret)

		par1$=mid$(_mipList$, off+6, 2)
		gosub 9010
		ip$=ip$+"."+STR$(ret)

		par1$=mid$(_mipList$, off+8, 4)
		gosub 9010
		
		par1=alAction
		gosub 9100
		par1$=ret$
		par1=4
		gosub 9110
				
		tmpBuf$=STR$(alInp-1)+"("+STR$(alMode-1)+"):"+STR$(inps(alInp,alMode))
		
		if cBit2=1 then tmpBuf$=tmpBuf$+"<"
		if cBit1=1 then tmpBuf$=tmpBuf$+">"
		if cBit0=1 then tmpBuf$=tmpBuf$+"="
				
		tmpBuf$=tmpBuf$+STR$(alVal)+"~"+STR$(alDecs)+"00ms|"+ret$
		
		write 7,tmpBuf$,len(tmpBuf$),ip$,ret
		
		syslog "Notify alarm: "+ip$+":"+STR$(ret), 0
	next i
	return


'---------------------------
' Gravar llista de targes a la flash
'---------------------------

9030
	par1=crdCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=ret$

	par1=ipCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=fileBuf$+ret$

	par1=alCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=fileBuf$+ret$
	
	open "F_W:datasize.dat" AS 2
	write 2,fileBuf$
	close 2

	open "F_W:cards.dat" AS 2
	write 2,_mcrdList$,crdCount*cInfSize
	close 2

	open "F_W:reader.dat" AS 2
	write 2,_mrdrList$,crdCount
	close 2

	syslog "Saved Card List", 0
	return

'---------------------------
' Llegir llista de targes de la flash
'---------------------------

9040
	open "F_R:datasize.dat" AS 2
	read 2,fileBuf$
	close 2

	par1$=mid$(fileBuf$, 1, 8)
	gosub 9010
	crdCount=ret
	
	if crdCount<=0 then return

	open "F_R:cards.dat" AS 2
	read 2,_mcrdList$
	close 2
	
	open "F_R:reader.dat" AS 2
	read 2,_mrdrList$
	close 2
	
	syslog "Loaded "+STR$(crdCount)+" cards"
	
	return

'---------------------------
' Gravar llista d'IPs a la flash
'---------------------------

9050
	par1=crdCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=ret$

	par1=ipCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=fileBuf$+ret$

	par1=alCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=fileBuf$+ret$
	
	open "F_W:datasize.dat" AS 2
	write 2,fileBuf$
	close 2

	open "F_W:ips.dat" AS 2
	write 2,_mipList$,ipCount*ipInfSize
	close 2

	syslog "Saved IP List", 0
	return

'---------------------------
' Llegir llista d'IPs de la flash
'---------------------------

9060
	open "F_R:datasize.dat" AS 2
	read 2,fileBuf$
	close 2

	par1$=mid$(fileBuf$, 9, 8)
	gosub 9010
	ipCount=ret

	if ipCount<=0 then return

	open "F_R:ips.dat" AS 2
	read 2,_mipList$
	close 2
	
	syslog "Loaded "+STR$(ipCount)+" IPs"
	
	return

'---------------------------
' Gravar llista d'alarmes a la flash
'---------------------------

9070
	par1=crdCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=ret$

	par1=ipCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=fileBuf$+ret$

	par1=alCount
	gosub 9100
	par1$=ret$
	par1=8
	gosub 9110
	fileBuf$=fileBuf$+ret$
	
	open "F_W:datasize.dat" AS 2
	write 2,fileBuf$
	close 2

	open "F_W:alarms.dat" AS 2

	for i=1 to alCount
		off=1+(i-1)*aInfSize
		
		fileBuf$=""
		v=0

9071	if v<aInfSize then
			fni=aInfSize-v
			if fni>4 then fni=4
			if fni=3 then fni=2

			size=fni
			
			par1=midget(_malList$, off+v, -size)
			gosub 9100
			par1$=ret$
			par1=size*2
			gosub 9110
			fileBuf$=fileBuf$+ret$
			
			v=v+size
		
			goto 9071
		endif
		write 2,fileBuf$
	next i
	close 2

	syslog "Saved Alarm List", 0
	return

'---------------------------
' Llegir llista d'alarmes de la flash
'---------------------------

9080
	open "F_R:datasize.dat" AS 2
	read 2,fileBuf$
	close 2

	par1$=mid$(fileBuf$, 17, 8)
	gosub 9010
	alCount=ret

	if alCount<=0 then return

	open "F_R:alarms.dat" AS 2
	for i=1 to alCount
		off=1+(i-1)*aInfSize
		
		v=0
		read 2, fileBuf$, aInfSize*2
		
9081	if v<aInfSize then
			fni=aInfSize-v
			if fni>4 then fni=4
			if fni=3 then fni=2
			size=fni
			par1$=mid$(fileBuf$, 1+v*2, size*2)
			gosub 9010

			syslog "barioload v: "+STR$(v)+", aInfSize: "+STR$(aInfSize)+", off: "+STR$(off)+" size: "+STR$(size)+", ret: "+STR$(ret), 0
			
			midset _malList$, off+v, -size, ret
			
			v=v+fni
			
			goto 9081
		endif
	next i
	close 2
	
	syslog "Loaded "+STR$(alCount)+" Alarms"
	return

'---------------------------
' Executa una BarionetCardAction definida a: dSecs, v, r
'---------------------------
9090
	' Si teniem 0xffff, no fem res
	if and(dSecs>=255,v>=15,r>=15) then return 

	if dSecs=0 then
		syslog "Set out/relay "+STR$(r)+" to "+STR$(v), 0
		if v<>0 then v=1
		if and(r>3, r<10) then r=3
		if r<=3 then
			ioctl 101+r,v
		else
			if r>=10 then
				r=r-10
				ioctl r+1,v
			endif
		endif
	else
		if dSecs<2 then dSecs=2

		syslog "Pulse out/relay "+STR$(r)+" for "+STR$(dSecs)+" tenths of a second, "+STR$(v)+" times", 0
		
		if and(r>3, r<10) then
			r=3
			for i=1 to v
				syslog "pulse number "+STR$(i)+"...", 0
				ioctl 101+r,dSecs
				if v>1 then delay dSecs*100+100
			next i
		else
			if r>=10 then
				r=r-10
				for i=1 to v
					syslog "pulse number "+STR$(i)+"...", 0
					ioctl r+1,dSecs
					if v>1 then delay dSecs*100+100
				next i
			endif
		endif
	endif
	return

'---------------------------
' Converteix un valor enter en la seva cadena HEX
'---------------------------
9100
	ret$=""
	
9101
	
	fni=and(par1, 15)
	par1=shr(par1, 4)
	
	fni=fni+asc("0")
	
	if fni>asc("9") then fni=fni+7	' 7 = A - 9
	
	ret$ = chr$(fni)+ret$

	if par1<>0 then goto 9101

	return

'---------------------------
' Padding amb 0s per l'esquerra, d'una cadena (par1$) fins a longitud par1
'---------------------------
9110
	ret$=par1$
	
	if len(ret$)>=par1 then return
9111
	
	ret$ = "0"+ret$

	if len(ret$)<par1 then goto 9111

	return



'---------------------------
' Eliminar IP
'---------------------------

10100
	if ipCount>0 then
		syslog "Delete IP", 0

		if cmdBuf$="00000000000000" then
			ipCount=0
			' Gravem la llista d'IPs a la flash
			gosub 9050
			return
		endif

		totLen=ipListLen*ipInfSize
		
		ip$=mid$(cmdBuf$, 3, ipInfSize)
		
		for i=1 to ipCount
			off=1+(i-1)*ipInfSize
	
			if and(midget(_mipList$, off, -4)=midget(ip$, 1, -4), midget(_mipList$, off+4, -4)=midget(ip$, 5, -4), midget(_mipList$, off+8, -4)=midget(ip$, 9, -4)) then
				
				midcpy _mipList$, off, totLen-(off+ipInfSize), _mipList$, off+ipInfSize
				ipCount=ipCount-1
				syslog "Remaining "+STR$(ipCount)+" IPs", 0

				' Gravem la llista d'IPs a la flash
				gosub 9050
				return				
			endif
		next i

		syslog "Delete IP: IP not found.", 0
	endif
	return



'---------------------------
' Afegir IP
'---------------------------

10200
	if ipCount<ipListLen then
		syslog "Add IP", 0

		found=0
		totLen=ipListLen*ipInfSize

		ip$=mid$(cmdBuf$, 3, ipInfSize)

		for i=1 to ipCount
			off=1+(i-1)*ipInfSize

			if and(midget(_mipList$, off, -4)=midget(ip$, 1, -4), midget(_mipList$, off+4, -4)=midget(ip$, 5, -4), midget(_mipList$, off+8, -4)=midget(ip$, 9, -4)) then
				found=1
				syslog "IP already in list", 0
				i=ipCount+1
			endif
		next i
		
		if found=0 then
			off=1+ipCount*ipInfSize
			midcpy _mipList$, off, ipInfSize, ip$
			ipCount=ipCount+1
		endif

		syslog "Total "+STR$(ipCount)+" IPs", 0

		' Gravem la llista d'IPs a la flash
		gosub 9050
	else
		syslog "Add IP: ERROR: IP Buffer full. Ignoring request.", 0
	endif
	return


'---------------------------
' Eliminar tarja
'---------------------------

10300

	if crdCount>0 then
		syslog "Delete Card", 0

		if ucase$(cmdBuf$)="2F000000000000" then
			crdCount=0
			' Gravem la llista de targes a la flash
			gosub 9030
			return
		endif

		totLen=crdListLen*cInfSize
		
		rdr$=mid$(cmdBuf$, 2, 1)
		crd$=mid$(cmdBuf$, 3, 8)
		cCmd$=mid$(cmdBuf$, 3, cInfSize)
		
		for c=1 to crdCount
			off=1+(c-1)*cInfSize
	
			if and(midget(_mcrdList$, off, -4)=midget(crd$, 1, -4), midget(_mcrdList$, off+4, -4)=midget(crd$, 5, -4), midget(_mrdrList$, c, 1)=midget(rdr$, 1, 1)) then
			
				midcpy _mcrdList$, off, totLen-(off+cInfSize), _mcrdList$, off+cInfSize
				midcpy _mrdrList$, c, crdListLen-(off+1), _mrdrList$, off+1
				
				crdCount=crdCount-1
				syslog "Remaining "+STR$(crdCount)+" cards", 0

				' Gravem la llista de targes a la flash
				gosub 9030
				return				
			endif
		next c

		syslog "Delete Card: Card not found.", 0
	endif
	return




'---------------------------
' Afegir canvis a relay quan es detecta tarja
'---------------------------

10400
	if crdCount<crdListLen then
		syslog "Add Card", 0
		
		replaced=0
		
		' Afegim o reemplacem si ja existeix
		totLen=crdListLen*cInfSize
		
		rdr$=mid$(cmdBuf$, 2, 1)
		crd$=mid$(cmdBuf$, 3, 8)
		cCmd$=mid$(cmdBuf$, 3, cInfSize)
		
		for c=1 to crdCount
			off=1+(c-1)*cInfSize
	
			if and(midget(_mcrdList$, off, -4)=midget(cCmd$, 1, -4), midget(_mcrdList$, off+4, -4)=midget(cCmd$, 5, -4), midget(_mrdrList$, c, 1)=midget(rdr$, 1, 1)) then
			
				replaced=1
				syslog "Card already in list", 0
				' , replacing (at off "+STR$(off)+", len "+STR$(cInfSize)+")", 0

				midcpy _mcrdList$, off, cInfSize, cCmd$
				midcpy _mrdrList$, c, 1, rdr$
				
				c=crdCount+1
			endif
		next c
		
		if replaced=0 then
			off=1+crdCount*cInfSize

			midcpy _mcrdList$, off, cInfSize, cCmd$
			midcpy _mrdrList$, crdCount+1, 1, rdr$
				
			crdCount=crdCount+1
		endif

		' Gravem la llista de targes a la flash
		gosub 9030

		syslog "Total "+STR$(crdCount)+" cards", 0
	else
		syslog "Add Card: ERROR: Card Buffer full. Ignoring request.", 0
	endif
	
	return




'---------------------------
' Comprovar tarja
'---------------------------

10500
	card$=MID$(tmpBuf$, 3, 8)
	par1$=MID$(tmpBuf$, 1, 2)
	gosub 9010
	reader=and(shr(ret, 7), 1)
	syslog "Check card "+card$+" at reader "+STR$(reader), 0
	
	matched=0
	
	for c=1 to crdCount
		off=1+(c-1)*cInfSize

		if and(midget(_mcrdList$, off, -4)=midget(card$, 1, -4), midget(_mcrdList$, off+4, -4)=midget(card$, 5, -4), midget(_mrdrList$, c, 1)=reader+asc("0")) then

			matched=1

			midcpy cInf0$, 1, 2, _mcrdList$, off+8
			midcpy cInf1$, 1, 2, _mcrdList$, off+10

			' com que registra cInf0$ i cInf1$, cal fer-ho dp d'inicialitzar-los
			gosub 9020

			valor$=mid$(cInf1$, 1, 1)
			relay$=mid$(cInf1$, 2, 1)
			
			par1$=valor$
			gosub 9010
			v=ret

			par1$=relay$
			gosub 9010
			r=ret

			par1$=cInf0$
			gosub 9010
			dSecs=ret
			
			' Execucio de l'accio
			gosub 9090
			crdTmr=_TMR_(0)
	
			return
		endif
	next c
	
	if matched=0 then
		syslog "No matching card found", 0
		gosub 9020
	endif	
	
	return
	
	
'---------------------------
' Comprovar alarmes
'---------------------------

10600
    ' digital

    for i=1 to 8
    	inps(i,1)=iostate(200+i)
    next i
    
	' analogic
    for i=1 to 8
    	inps(i,2)=iostate(500+i)
    next i
    
    ' temperatura
    for i=1 to 8
    	inps(i,3)=iostate(600+i)
    next i
        
	if or(noAl=0, crdTmr=-1) then noAlTmr=_TMR_(0)
    noAl=1
        
	for a=1 to alCount
		off=1+(a-1)*aInfSize
		
		' El que cal controlar, d'entrada
		alData=midget(_malList$, off, -4)

		alCrd=and(shr(alData, 31), 1)
		alInp=and(shr(alData, 28), 7)
		alVal=and(shr(alData, 16), 4095)
		alMode=and(shr(alData, 14), 3)
		alComp=and(shr(alData, 11), 7)
		alDecs=and(alData, 2047)
		
		if alDecs>maxAlDecs then maxAlDecs=alDecs
	
		alMode=alMode+1
		alInp=alInp+1
		cBit0=and(alComp, 1)
		cBit1=shr(and(alComp, 2), 1)
		cBit2=shr(and(alComp, 4), 2)
		cRes0=0
		cRes1=0
		cRes2=0

		if and(cBit0=1,inps(alInp,alMode)=alVal) then cRes0=1

		if and(cBit1=1,inps(alInp,alMode)>alVal) then cRes1=1

		if and(cBit2=1,inps(alInp,alMode)<alVal) then cRes2=1

		if or (cRes0=1, cRes1=1, cRes2=1) then
			' El que caldra fer si es compleix
			alAction=midget(_malList$, off+4, -2)

			fni=alMillis(a)
			' calculem offset des que s'ha passat la tarja fins q s'ha activat
			' la condicio d'alarma (p.e. s'obre la porta)
			if and(fni=0, crdTmr>0) then crdOffTmr(a)=curTmr-crdTmr
			
			alMillis(a)=alMillis(a)+(curTmr-lastTmr)

			if and(fni<(alDecs*100), alMillis(a)>=(alDecs*100)) then

				noAl=0
				' Nomes enviem alarma si
				' a) no volem que s'hagi passat tarja i no s'ha passat en els
				'		ultims alDecs*100 ms.
				' b) volem que s'hagi passat tarja i s'ha passat en els
				'		ultims alDecs*100 ms.
				' c) i en els dos casos anteriors, no hi ha cap altra alarma
				'		sobre la mateixa entrada en el mateix mode, que actui
				'		sobre la mateixa sortida i que sigui mes llarga - i que
				'		encara no s'hagi complert...
				
				doAct=1
				if and(alCrd=0, crdOffTmr(a)<>0) then doAct=0
				if and(alCrd=1, crdOffTmr(a)=0) then doAct=0

				syslog "doAct: "+STR$(doAct)

				if doAct=1 then
					dSecs=and(shr(alAction, 8), 255)
					v=and(shr(alAction, 4), 15)
					r=and(alAction, 15)
					
					' "anul·lem" la ultima lectura
					crdTmr=-1

					gosub 9022	' Notificacio
					gosub 9090	' Execucio de l'accio
				endif
			endif

		else
			alMillis(a)=0
			crdOffTmr(a)=0
		endif
	next a
	
	if and(alCount>0, noAl=1, curTmr-noAlTmr > maxAlDecs*100) then crdTmr=-1
	
	return
	

'---------------------------
' Eliminar alarma
'---------------------------

10700
	if alCount>0 then
		syslog "Delete Alarm", 0
		
		maxAlDecs=-1
		
		if ucase$(cmdBuf$)="40F00000000000" then
			alCount=0
			for i=1 to alListLen
				alMillis(i)=0
				crdOffTmr(i)=0
			next i
			' Gravem la llista d'alarmes a la flash
			gosub 9070
			return
		endif
		
		totLen=alListLen*aInfSize
		
		par1$=mid$(cmdBuf$, 3, 1)
		gosub 9010
		alInp=ret

		deleted=0
		for a=1 to alCount
			off=1+(a-1)*aInfSize
	
			if and(shr(midget(_malList$, off, -1), 4), 15)=alInp then
			
				deleted=deleted+1

				midcpy _malList$, off, totLen-(off+aInfSize), _malList$, off+aInfSize
				alMillis(a)=0
				crdOffTmr(a)=0
				
				alCount=alCount-1

				if a<alCount then a=a-1	' Per tornar a avaluar l'actual
			endif
		next a

		if deleted>0 then
			syslog "Deleted "+STR$(deleted)+" alarms, remaining "+STR$(alCount)+" alarms", 0
			' Gravem la llista d'alarmes a la flash
			gosub 9070
		else
			syslog "Delete Alarm: Alarm not found.", 0
		endif	
	endif
	return


'---------------------------
' Afegir alarma
'---------------------------

10800
	if alCount<alListLen then
		syslog "Add Alarm", 0

		found=0
		totLen=alListLen*aInfSize

		par1$=mid$(cmdBuf$, 3, 1)
		gosub 9010
		alInp=ret
	
		par1$=mid$(cmdBuf$, 4, 3)
		gosub 9010
		alVal=ret
	
		par1$=mid$(cmdBuf$, 7, 4)
		gosub 9010
		alTmp=ret
		alMode=and(shr(alTmp, 14), 3)
		alComp=and(shr(alTmp, 11), 7)
		alDecs=and(alTmp, 2047)
	
		par1$=mid$(cmdBuf$, 11, 4)
		gosub 9010
		alAction=ret
	
		midset tmpBuf$, 1, -2, or(shl(alInp, 12), alVal)
		midset tmpBuf$, 3, -2, alTmp
		midset tmpBuf$, 5, -2, alAction

		syslog "Adding alarm: "+STR$(alInp)+", "+STR$(alVal)+", "+STR$(alMode)+", "+STR$(alComp)+", "+STR$(alDecs)+", "+STR$(alAction), 0

		for a=1 to alCount
			off=1+(a-1)*aInfSize

			if and(midget(_malList$, off, -4)=midget(tmpBuf$, 1, -4), midget(_malList$, off+4, -2)=midget(tmpBuf$, 5, -2)) then
				found=1
				syslog "Alarm already in list", 0
			endif
		next a
		
		if found=0 then
			off=1+alCount*aInfSize
			midcpy _malList$, off, aInfSize, tmpBuf$

			alCount=alCount+1
		endif

		syslog "Total "+STR$(alCount)+" Alarms", 0

		' Gravem la llista d'alarmes a la flash
		gosub 9070
	else
		syslog "Add Alarm: ERROR: Alarm Buffer full. Ignoring request.", 0
	endif
	return
	
End TB=h:� _TMR_     PAR1     *FNI      .RET      2ALMIL�   �0INPS    3IPINF    �7CINFS    �7AINFS    �7IPLIS    �7CRDLI    �7ALLIS    �7READE    �7IPCOU    �7CRDCO    �7ALCOU    �7RDRLE    �7CMDLE    �7I        �7C        �7OFF      �7TOTLE    �7FOUND    �7REPLA    �7MATCH    �7V        �7R        �7DSECS    �7ALDAT    �7ALCRD    �7ALINP    �7ALVAL    �7ALMOD    �7ALCOM    �7ALDEC     8ALTMP    8ALACT    8CBIT0    8CBIT1    8CBIT2    8CRES0    8CRES1    8CRES2     8DELET    $8A        (8SIZE     ,8LASTT    08CRDTM    48CURTM    88CRDOF�   <8A2       @:ALCD2    D:ALDC2    H:ALIN2    L:ALMD2    P:OFF2     T:DOACT    X:NOAL     \:NOALT    `:MAXAL    d:           RDR$   CMDPO  RDRBU + CMDBU ; TMPBU J REG$   JCMD$   jCHAR$  �IP$    �CRD$   �CCMD$  �CARD$  
CINF0  *CINF1  JRGCRD  jVALOR  �RELAY  �FILEB  �PAR1$  �RET$   
_MIPL�6_MCRD(#�_MRDR�+_MALL �-_MDUM �3                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 f	g	h	i	�fj	(#gk	 hm	n	o	�	RDR: �	TCP:0.0.0.0:10005 ?Marina Eye-Cam Access Control BCL controller r	kdr	�r	r�	�	`�	�	`�	?[ \%3t `] Loading cards  ?[ \%3t `] Loading IPs  f	?[ \%3t `] Loading Alarms  ?[ \%3t `] Opening devices -�.-�.-UDP:0.0.0.0:10006 .?[ \%3t `] Starting operation 0�p	20�q	2o +E�	F�	��	`Hp	q	q�	�?[ \%3t `] Received info on command port ( Xq bytes):  �q	�	Z��	0  ��	1  ��	2  ��	3  E�	4  ��	5  LE?[ \%3t `] Invalid command FFp�	 r	p�	�\%02x %�rr?[ \%3t `] Received info on Reader port ( Xp bytes):  �p	 �F�	 s	nt	sg�	�Z�tg( Z�s), s?Card List:  �!c	b	#��	^Z�bH'�	'0 '�	'9 c	6cc	c'�'0 E'�	'f c	6cc	c
'�'a FFb!r	mt	rf�	Z�t !�	Xc�	Z�t !�	�. Xc�	Z�t !�	�. Xc�	Z�t !�	�. Xc�	Z�t !x	�	�| Xl| XxE�	�| Xl| Xx| ��F1�#��c?Notify:  �: Xcr!r	mt	rf�	Z�t !�	Xc�	Z�t !�	�. Xc�	Z�t !�	�. Xc�	Z�t !�	�. Xc�	Z�t !a	� �	�a	 h�	X~( X�): Xe~��	�	�< �	�	�> �	�	�= �	�X~ X�00ms| �1�#��c?Notify alarm:  �: Xcr!a	n �	�a	 h�	�a	m �	�a	 h�	��a	o �	�a	 h�	��-F_W:datasize.dat .1�/-F_W:cards.dat .1�ng/-F_W:reader.dat .1�n/?Saved Card List !-F_R:datasize.dat .0�/�	Z� !n	cn	!-F_R:cards.dat .0�/-F_R:reader.dat .0�/?Loaded  Xn cards !a	n �	�a	 h�	�a	m �	�a	 h�	��a	o �	�a	 h�	��-F_W:datasize.dat .1�/-F_W:ips.dat .1�mf/?Saved IP List !-F_R:datasize.dat .0�/�	Z�	 !m	cm	!-F_R:ips.dat .0�/?Loaded  Xm IPs !a	n �	�a	 h�	�a	m �	�a	 h�	��a	o �	�a	 h�	��-F_W:datasize.dat .1�/-F_W:alarms.dat .r	ot	rh�	 y	yhb	hybb	b	b	�	ba	%�ty� �	�a	� h�	��y	y��
F1�r/?Saved Alarm List !-F_R:datasize.dat .0�/�	Z� !o	co	!-F_R:alarms.dat .r	ot	rhy	0�hyhb	hybb	b	b	�	b�	Z�y� !?barioload v:  Xy, aInfSize:  Xh, off:  Xt size:  X�, ret:  XcD�ty�cy	yb�Fr/?Loaded  Xo Alarms !H{	� y	z	!{	?Set out/relay  Xz to  Xyyy	Hzz
z	z	<ezyEz	
z	z
<zyFFE{{	?Pulse out/relay  Xz for  X{ tenths of a second,  Xy times Hzz
z	r	y?pulse number  Xr... <ez{y+{ddrEz	
z	z
r	y?pulse number  Xr... <z{y+{ddrFFF!�	 b	Haa	7ab	b'0 b'9 b	b�	Yb�a!�	�#�	a!�	0 �#�ay!m?Delete IP �	00000000000000 m	 �!Fu	if�	Z�fr	mt	rfH%�t	%�%�t	%�%�t	%�	&�tutf�tfm	m?Remaining  Xm IPs  �!Fr?Delete IP: IP not found. F!mi?Add IP v	u	if�	Z�fr	mt	rfH%�t	%�%�t	%�%�t	%�	v	?IP already in list r	mFrv	t	mf&�tf�m	mF?Total  Xm IPs  �E?Add IP: ERROR: IP Buffer full. Ignoring request. F!n?Delete Card _�	2F000000000000 n	 2!Fu	jg�	Z��	Z��	Z�gs	nt	sgH%�t	%�%�t	%�%�s	%�&�tutg�tg&�sjt�tn	n?Remaining  Xn cards  2!Fs?Delete Card: Card not found. F!nj?Add Card w	u	jg�	Z��	Z��	Z�gs	nt	sgH%�t	%�%�t	%�%�s	%�w	?Card already in list &�tg�&�s�s	nFsw	t	ng&�tg�&�n�n	nF 2?Total  Xn cards E?Add Card: ERROR: Card Buffer full. Ignoring request. F!�	Z��	Z� !l	H7c?Check card  � at reader  Xlx	s	nt	sgH%�t	%�%�t	%�%�s	l'0 x	&��t&��t
 ��	Z��	Z��	� !y	c�	� !z	c�	� !{	c w�	`!Fsx	?No matching card found  �F!r	er	;� rrr	er	;�rrr	er	;XrrI�	�	�	`�	�	ot	�h|	%�t}	H7|~	H7|	H7|��	H7|�	H7|�	H|����	��	�~	~�	H��	7H��	7H��	�	�	H�	e~�	�	H�	e~��	H�	e~��	I�	�	�	�	%�tb	d�Hb	���	��d�	d���Hb�dd�	�d�	�	H}	���	H}	��	�	?doAct:  X��	{	H7�� y	H7�z	H��	 � wFFEd�	��	F�Ho�	���d�	!o?Delete Alarm �	_�	40F00000000000 o	r	kdr	�r	r �	!Fu	kh�	Z� !~	c�	�	ot	�hH7%�t	~�	�&�tuth�thd�	��	o	o�o�	�F��?Deleted  X� alarms, remaining  Xo alarms  �	E?Delete Alarm: Alarm not found. FF!ok?Add Alarm v	u	kh�	Z� !~	c�	Z� !	c�	Z� !�	c�	H7��	H7��	H���	Z� !�	cD�I6~D��D��?Adding alarm:  X~,  X,  X�,  X�,  X�,  X��	ot	�hH%�t	%�%�t	%�v	?Alarm already in list F�v	t	oh&�th�o	oF?Total  Xo Alarms  �	E?Add Alarm: ERROR: Alarm Buffer full. Ignoring request. F!"PK   6s�6            	  META-INF/��   PK           PK   6s�6               META-INF/MANIFEST.MF�M��LK-.�K-*��ϳR0�3��r.JM,IM�u�	����+h8�*8������(x�%�i�r�r PK�%�tF   F   PK   4s�6            =   com/eye_cam/sirius/barionet/BarionetAccessControlConfig.class�WYpG�zu��xm����8�j���:lG�edIv6�I�����Z��gg63��D 1�p�@0�H0��B�Cd�JQ�E�k�����S��3��c�R���������������� �xO���aC�u�`4�V<�eL��x:���4|J�$�븀�$�i�����g�lh��sV�D3:v"'��L�����[�r�58Qt<W�>xQ�:
����	]���(�u4㴆O���(>��xAË�i����4|^�4����$�^���5EO04^2���a�Z�}״sGꏚ���O��,Cm�3M��M[�S0�,�l4�\�S���Y��̰3qa���R�7L�aG5y��eE�7�"�
�v4k�>��N�͊����O.1	�7����ؾ�X4͘��9���8��W��cc#c�|�񹰝bn���_�d�kq|��q���^���g�|���F��3]��N��[K.����0���a	7�o��-\�n|��Θ�ΌF�8���Q܈�{����:�Vf4��G1��Z���x��D��xS��c�F4R'i-�u�7+�*ݹ�O�S�x�d�hyB��aC`]���ϥ}M�7�~A{S{���W�Q�2�_����o��=~���*o���QE"Oߖ��\��EӢ�0<���D7���@��K��h�����a8�>�Va.��q��߳���ђ@f���u\���?o�ӕըD�ף�ᾛ+���B��<�xX��B�Bq�%�S<*;�U�=<X�C]Wf9��S��[�+E���Y�m�7g�ez��+7+b���Z%9�:���AlK�,��5����ڍgi���N�/}~Ű�T�q�ӹe�Kw�'�Z�M�ꬰK"�Y1פ�L�\�	����WYs�%-zl`��,�]J:O�������@'o�ͱc=�d�D����	��{��ES�i����Qb.�֖��xVޤݼ�/#Q$=�yJь�<oZ��ul�ժ�����X��N)#f�V7�W>�|��M�Z���0<�L���G�f[%)�6��ƻTj�FѦ\o�Rz,��U0
�K���C<�} #+��"I<(u#��X]�%x�~X0EZK+c3�f�/�c+�L[�)O�?�E�����!���K�~���z�F2����d�Ly�)�-|Hj.��9
-o(ꬲE%��o,9L��j�a`�J��r�B�x�#%#���垮@cI˴� �%��� o�)Z�|Jp�ls9ᒬ ��~�:4}u@]M5�QIt����m=V�����ɜ1�\��o��k���$���X�&�'B���@	�
G*AC.���B��z1���ܵ���*�h,�v9<�u]Qޒp[��S�=�;�1�WDJKZZ��+9R�3ŻR�`�b�ԃ�$�*l<�o�L߰R��6,'��"_��]��S�X_����5)=4u&��ؑ葴��Q9L|x��n}x��&������$���
���[�8>���^5T.�����tZG���Ӈ^�#%5���P*ȝE
�'�?8kŜIjk�y��!����A��=A�5�d�sv��n�J2=�D�	�a�:�O��|VLOS�xĒ���a�.=����&GI���E�eU'�y3����tdg��-E{��]k)��o���c��1%��SF���+�.�
���aSEu�Kd���r�@�T�+���W�����V.���K�T�)�9����NQ��٠�Q�U�G?���+��fx�T�+ſ�h�D��@�V"�7
aS>��Տh���Q�eH h�S����j�#�õ�~LTK�ӊV���_f4~�v{if4�%�{i�WD1�+,d�G���=��w��}�i�YD-ͷQG��]�W4�@�;I��؄]��M{Ic?Hi�i&�@�x��.���M&kړ��w�}0Y�޸a�؛Ԉ�9YOG�Q�@�7�[��K�͊i�j�E4,�I�-�Eo^D����lU�=K��}[3	.b�{K��P<Ɇ�2�{�9���&�Px{ߐ�L-���J�]J�;٠}�(�4�^��Mj�Svy�{@RP��K]��h�jkK�~b���\�^L6lP��W���\`{��=<4�l��z@}x���ԁE<��W��G���dC��!���GZS�7�3�\@��
�>�A��� �գxM8�$uA3N� NSG\@�A&:�G'
���8�kx����u�7�r��ۤ���#�����N�x�A���4ۄA�ClF�(ۏ'Y�Yΰ�8ˆq�Mb�]���f7p��Ƴ�=�Ȳ�@�#���ُˑ�#��#]x.��|z�5����5TK�{h���T#��e+Z�F�ƴ���!�N9t�����a��n��Ә�9�'�PK0-H�D	  �  PK   4s�6            0   com/eye_cam/sirius/barionet/BarionetAction.classmS�NQ]��3�����r)�J�� �J��F#N�cSZ��xy1���'��bLx�D*����C]��@3��g��Z�{�����;�cH�Ёa��0܌v�(c*S&�����Qe���2't�a��8��ieΨ��:��8/�+���H	4����\���@���w�EV 2��zf%\��'�-G�	h�v�v.M�F�
���,��9� ���iYZ��yf4���h�&or��drc*�礣Xڢ#;��N��ؼ��B �PtKy�Vr�SV�.�3�q�G��>�{+����1Љ.{��F��q��㢁K�40���i��+����\;��%6ꨁ��1pX�'�03��|)�2�r�l�l��H{�%���&יo���g⋪�D�KrPe7]��]j ���Zϥ
�̩"�+V�,�w;�F�-NqF�ؔ�6f'��ʊ,%��r^rΓjɤ�,^�9G�|G���q�;��~~�K����=����j/m�Y��̯����o~s�f�4�������=�h�IwaPt��C����Ӥ�ӏ6$�SĽ�l�����4�[t��=N��9I�S�9�zQ����\U���&�[��b�^A0YG�R��z��1_��R4�G�UI�j%i�K���KJ27�2.c��;��D��C��J�����G�U��i���7��Jy�
Z�b����.���m�
"�E*h�[��K�	��_���u��|\=���x����m~��d��<ɼt����;�f�h�C.�LXl@א�=<�#��6у�5a�/PKy���  }  PK   4s�6            /   com/eye_cam/sirius/barionet/BarionetAlarm.class�TMOQ=�_e��TT�B�A-**
*�8Z�����>q��i�S�;vFcp�+7ƍ��M\���4�7������w�=���^��??p��c�8!���H?ܟ@�#=�o�oD~4H��4�1�����K	"�a�JHHc�ƴ	-O20��9a�%G7�Y=_�^��!��'S1���:yYM�33iUP�zeFd�&�j<���I��Դ�<�|+GN�Z��nӪ��s���;ͳTf�0�	"�6�EgiG��њ4L>U.,p;�/�iX���(5.1銛i�p+%�d�	��]����N��Xӎm���h��P��us1Va���V���qC�k�l�YMtD�e�B���2z��'�tJ�1�4q�.�_�%IL2t֖-��eL!%�_�tA1�����Xɰ�r)�Pm �O'���R�y�qOHW��7��_䶸o�_�}�pD�{�P-�n�)���Kt���_B�si���h����Eޢ�c8�\w3S���N����p�$�wT�w��B���؎�褱�&���v�؀��܀w�]�S߻w�'FAL���T��Hۋ�d#�^�O�+�`J�*<��Ux����_!�D;�{�����
�W%�*٦DW^B��G���r�U4��/�J_�A�H��-G��Lv�����Zp��d�ڽ�>\� nb���mBw��N�b��w�g���>��OڀK�#{���S+-�=\Wꭕ.�=�XU��ZlP��q�ֻ�2YzKU='���*�y��-�hM~Ah~m}kh�Z�D�P�ҲH��[+��$kؒ�{��T��	��	<#��;�PK='�@  �  PK   4s�6            9   com/eye_cam/sirius/barionet/BarionetAlarmLogMessage.class�T[OW�l��l
8\ ���؋�)4M�&�C��&4PR��,�b6�];�5%-!�U��ڗ��R�ZEj�)y�������svm�#!�3s�|gf�73����x�m|�cƽ�@���^���*�ט�z��u�{C��)/n`������:��,z1�[����y,򢀄�%���2�;^t�:[�VX���� �����$�C�q��[5�y����3)�T2�gIY�N+����Is�5UW	�V����ݕ��&���e�Fz(0Gg'�H�1�P����b��+y��bE���� �����̚�33k�N��sN~�GG����894�����w&�7�ʤ�2w�˦�1kL�M=�IǕ\NN+=�HE�ǡ&��C���p�UĨ�)iY3�y]1�+I%kQ0��i��RL�Q��SS]��g�KDY#e��8�{C�	?3��خ�Dϒ�/��غ�ٲM2���"L�DX��X�16D<�'<>���"�DtC�%���AXDUsD�gxHU��˶BlK���rR��9�T��J����Iv��`7������� [}E�-*����������C�aN����k���m.����W��h`�Z�x�lV1RB�:|��X��RC�-U�JZ�O˖���*<+�{�~G쀓��e+�ƺ����Ěl�(��T����;��}T��N�d�Q�1g �����g�hG�Uc�XE�(BV6s
-x���%���o��TiU� M<�)e��*�I��`��-J_�Q�&�*����S����Q��cME��͔��E*j/[��Z �Ɯd?�]���M��pݾw������܅k~��x||B7��ہ�%7�;�p�9�`m_G�B���J!W��Ѱ�F)�.�>)��c�h
Q��A�T���._K�5?�4�ͮǎ�x��]t�(w��)��R�6)��)�k�v�1t����6:��qx��ZH#*��mh��D���,=�!*���Q�F4fh�"����;"�{��'��w\�K���0��1H.��&'I�����O��{l�(ɋU��J����B�+��HT��WB�$1��ك6�R�Ir�*��z��}9�����ތ��州���L��X5Y3ٛ����e���͖�f��(.��w�܌Cf�v���v�62}g��A�:vg�x��{7:�:�	����mta�:��O�$V��R����D�\�PK�<���  b	  PK   4s�6            4   com/eye_cam/sirius/barionet/BarionetCardAction.class���J�@��IҤ���Kq�.�,j�]+��BqSq+�t(#����c�
���P♘U�8��\��Μ���\a?D;V"+� ����Uq��L&���8�K�h��|,�D�'�,�㓡v��W�&�R��F�ŵ퇳�4�|P�}x/�ʵ,����҂򡥺�X	�t�i�q�._R��2�\�a��!��_3N��g�q��ˤm�Ωv)��q�A)�i�VZ����A�6'�M"�ѽ��4���_Up7��V�:]�l� PK��  �  PK   4s�6            8   com/eye_cam/sirius/barionet/BarionetCardLogMessage.class�T�OQ�=���,P�/P@�v[Z��
��T0&��n�/͒v�l[���^�8ꅋM,I0^<�G���!�lvf������̼����>�a��сA�~�0$aXB��p]zD�Q?�7jq7���qaM1)���ŔwD�]�}��Q	��1x�Ni)%��o豌n�c�E۴����z��Lc���
�n9e�g0iZfq��5x6?���=�Kq�Ƅi��R6��'z2C)͋����9a�R�J��Z��<�+�*���\�6��)6��m3gU�����<*`d�`���Ŧ�y���w��e2<�g��t)˭��k�狴C�i�3%3s@�I�6���Vp�{dhx CE��땱����蔰$cet�[�%\fP�\6��𗆞�L�,b��b�'�6��J�) V��w�!~�-�/C�<I'������:0x
��Yd:L�z�8Zz>�-����qUk-�PJ��mAMs����I�.��]�yN<5�ʨ4;�8S�+���3z�v��vѕ� �-��iH{�Ĝ���/Z�H�Ww��J���Rd��	���^ۃgm^�ܾ0�;�݅��q�]ZՕ!�w��&;���2Ԉ�c�{nW��}�#Z�=�"��.��P��S�D��
�(�H6��h��v�q��H�SD|�"ߢ�0@YWɪ��4�T�A�r�������5�ІϧBc$�9�6�!���z�d�r��Mi&!�v�rh7n��u��R�D�$��Fou���te������*�<�	�`�0E_��A�PK쮮�%  �  PK   4s�6            >   com/eye_cam/sirius/barionet/BarionetCardReaderController.class�Xi`T���,y��KH&	$$�	�B�$5�$dq�g��ę	��Uܰn �
*Tk�ZE�Diժ��j]Z�Z�R���V��7o�L0Ώ�޹�,�Y�=�͓�?��*Უ	�31����r�E��í
~m����(B�����w*��*v��.9�m��#�{�؋���g���oCX�R�G�R�n9<�������!<,g�<���Q<&������v��!��c�43��I���O
�������r�Y��Ͽ��y^�+/������6�l�+6�j�k6�����M���-�!%o���6�S>�Q��I�ޗ����cs�� ʩ������	>��g�\_(�Ҏ�q���ʂ��7W
�:��~_0��������Yj��TIR�ȉ�4��I�4d,�W?gY���QJ��"U�1y���L��L��TK	K��,)k��"��(u������ߧ�洶�`P��~�{���Su��-kc���.Y��,�e�g�մ�9^�:��ߪ�o�@f����-Bg�t�/�Hs�j��yB��β%��:׀z�Ok�l_��ݫ����}mM����6�NJs������;�˙\Μ��SN�M�fq������;ܭ�ҀI�����jcH�3JD�-c�(��҈�+ş�'�e5_�\{��GX�i`���Тu��_z��6O0�����Ε{��Q;���^�<���h}T�/R|N�T�����%+�k�jn�|_��62�Q�X<���m��~�����i��q?z)��z��~�@�S���x�c�JEz��x̲cp��@��J��������˄<�&�/�}+-�%G���ó��=>�=W�Q{M��@�6�#7����l3����������uX���� �*:p��N,V�LE�{]����+�ȏc�վ����JEZ�����o���w
���{Q��"�*L¬�"\��*����T��B�w,픣P6���*�(��&nTZ*m�l֚REd�"��b��UE�0+¡�|Q��B��1q���^�B��Ǥ�ܡ���h*Kݑ �򰒁���&�آ��AX*��.�8�2��Ӧ�E*�nW�@��Kp�����[U\�kIVV�b�D�S��SD�"��0�M���0�TW�O�8j��6k����wF�,4gJ#S5�S��%UUM�8�j��	��&OSq6��L���ZU�U��WǪ��>�%���D��b�(��ܦ���8FSq���R1\#T1R�R�hU�cU�e�s���p�������v��L^�l	vf��k��;3;T���C�]f|�.�k��R<Qm!Te�3mˉ�ղ9����q����l����R�'�?��<v���lE-�`\���&���m��+t����ZL���d���H.yGr�ހ�ZCbP7�D��m�wR�0"��W,���h����c+y��:$�e��W:C����hD)��"�Df�`4H),L���G���\Q+�Z��v�p+���
hgur�5��e�n:�<V"ې�嬫�㕩#nt�k�kP�*K��I�n^M�Z�^^�d�Oԯ'm�[�!qD��x+���b���e��G�]w���T����_�w;w�Q��w���N$o�Ȇ�����h�<�h�b~?,�
XJ��z��	�
�+�SI��@�N���MzU�����)���g&�m�W'��k赤�	t;,�������'��O�}>�(�K��)˦�c\�.�&�y?���"߬r�ثym�|��=���� ��󉶈h���}���tX!����@YV^��Ǡa(���?��K	}4ջ�%��c[JGL|��+六#��ͅ��"zs1���뗒�\l2��0�d��$��[��6Μ�_�y�Q�������rv�#���^�X1�v�I����7�-\����r�v����)�7,-4�閬�m�Y��]pH27�Vd�r39�=����P��B7����X&]F&s����c����9��U���ij}F����|�[���q|Y%��� �e��.#�S9ah�:���{c���˔|�z_e��N��Z:G��Anyy8r�]1�}�=J\��ZJ�A���M�0���z�߃G�����Iӄ6./�`�!s_|�7%I�3X_�_b
�ڿ"ԯ��+)��_"h	{����H�1|E�U�0��)/�'��]-9q��rwH�&�e�@�}b�u�\y ���!5,�R����A�Tf<ѵ�����
3x�G���0��G;ʄ�j��i��/8I�U����:�|7�"\ ��3�����-�𶍾��ϖ�!� F,s��ƨ���G��|F8}P$�c�2r���ơd�!FB��'��X��$�WƂ���٧yg:�2#Ůqi�W��D�Ʊb2f�)IƯ��1^K�r�e��w���<��gQ�q����$���04��<t|�4����?�r}������_ރ	5V��b+�+��(T"EMoc�
N�\r6�U�A�Ŗx�O@_�B�]D��(K0R,�S,��ĩ��iX+�H�b��E���m������׷�hO������\q/�H�Q�~���@�!�sS�^k(��sr��K�;K4v�>bepL���D#�a;� ��8�1<� ��ev9�+0U\�Fq5��6�"��J�=	�u�������-/n�����]���I��_�o�fYY�^���ba4gl(9?�䊢��S
I�����ZaǦ����/��=ļc�}���������$��c���q������h_䮟v�#�1�y�hr���D����MM��<K��Q�����͋	�3qn�h�E����x�;��t?��r�r[stcf}B�9������^�8.ola7�4�cBjͳ��D�8�\�o��u��KבOx~�+C�9���'y�[�8��">��vy� u=8I�ɧt�r��\�4ER&�*.9�L�r���Zl}������xR0}ń/�����0uב��2'��`	��:b�籗n�Y0�x��3�;y��x6��71Q����m��w0_���8Y|�&�)�����>�
�N�q���=�[��c����k�l2�"�[L6\e��VS6���p�)כr�Ӕ�]�B�e�=���k*�>��MCq�4��J�i^4�������46�rn�?v�PK��s  �  PK   4s�6            0   com/eye_cam/sirius/barionet/BarionetLogger.class�V�STU~.,\X��� ��X���V~  ��˪A؊����zu?�޻�Qf�/6M?8M59MS�4�o9�Ѩ#�3���gdϹw�]gPf�}�y�s���sy�ߟ�l�'^lF��/J�_�6�S1�E��ɐ�����p����b"�|D��RS��2�bԋ:�+��Iq\N�SUqBǽh�I/q�
�㴊3*t/ZqJń���R�>�O��������'L=5��]v�����,=!�waY�H�F����Kb*G�DZ���Pp�(�7ˤ��8�SA�0����M##���Lb��ֳ����\d�'��m��}�L	aF�M�*�5҆�WA�o1���g<n�qO�����駚��H65!�Q}"I�w����h��RWд�j�$$YV'���E�`��=�2N�{�yRO'�#�i�N)$�bu؋�";_��U����=�T��'C�Z,�{��%�-YJ�l�\#��C�,`m>�KƯ����ءѰ[4<�pʭBëxM�9=�!�s*�qAC)]/Vid4\�%&z4X�du~�5t��XhG���(���pW4\�5;V�>݌/��f�y��>��K�>�M�w����iR7SK��W���GϙB��X��0n���aɵ)�}M�P�[%�$K����Sb�߁�5����+|J��VD\���Χœv&udNG&΋���������I=΄��
wK��XR覣Y2>D����,���)��ҽ��XUSĄ1I`���Qv�S��Ĳ�kI9
+Kb�%���>>���$�<���o�k���1)��݆~n�_��j�&~�6���7��8*A;%��+J�r�A�m�,ĝ%ԛ��P�s(�΢tˢ�Y�OC��Pq� V���V��C�jRo q+�w8a��9��98Dt)u[1��P�<;ɶ�v�{�hw��7��z�M-�y��;<��GR��鼇*ߢ!����!V�`���O=���t�O����o�<�F�j��>��a��n`[PI9�
qL�a�3�#��Q��\�wq��gaƨe��*��y���%aTD:�;� *����o��� �PNy�마8�kΰ:�?�s���n�8��hQ�����*{��'Y�-�Lx_��Ө]��$�2-{��<R2�'5}U��P��b�0����i*uM����Q���#2���IE��94F���x�,�g��^)��t{1���M9L��,dp����S�Y.w:�/�o,{0�4�zz�.s܅1Dт��w����1�9<pt�o��ڌa�|��[<���|Ν_��61_��K��+\#�u|G�#ٿ����'rN��g<�/����m��}�PK�1��H  <  PK   4s�6            7   com/eye_cam/sirius/barionet/BarionetLoggerNotifee.class���A���8T�'��Q_E�q($Z���m���doI���x(q'JS��⛙���L��������7��7� ,G�Q^�(؋�U'��4u\�Q�l.��{%sQgΥH��R:����UK��xG�m��)^��	�H�{�2O�9����o����B�Mrd��B�( 4���6�~PK篹ϳ     PK   4s�6            4   com/eye_cam/sirius/barionet/BarionetLogMessage.class�P�J�@�mҤ��T�x��[+�ų"��)��U6��4YH6�JA<�~��-��e��ٙ7o�����!vB���`�Ƕ��'�զ`8H�ɹzPwR�ҥ�+���.��g��i�>"߱.�=apF�[���+�8х���T�7"�dK�d4�_HϚ�-u�1�;����K�pj�R��d�.$&�TU%25i<|�"t��Y`�g�R�ә��ݣ�s�?���L��)�0�Ӑ*���{j�!�ג1��_�gtH�.�:6Z6&��	PKC�T,  �  PK    6s�6           	                META-INF/��  PK    6s�6�%�tF   F                =   META-INF/MANIFEST.MFPK    4s�60-H�D	  �  =             �   com/eye_cam/sirius/barionet/BarionetAccessControlConfig.classPK    4s�6y���  }  0             t
  com/eye_cam/sirius/barionet/BarionetAction.classPK    4s�6='�@  �  /             �  com/eye_cam/sirius/barionet/BarionetAlarm.classPK    4s�6�<���  b	  9             �  com/eye_cam/sirius/barionet/BarionetAlarmLogMessage.classPK    4s�6��  �  4             �  com/eye_cam/sirius/barionet/BarionetCardAction.classPK    4s�6쮮�%  �  8             -  com/eye_cam/sirius/barionet/BarionetCardLogMessage.classPK    4s�6��s  �  >             �  com/eye_cam/sirius/barionet/BarionetCardReaderController.classPK    4s�6�1��H  <  0             �(  com/eye_cam/sirius/barionet/BarionetLogger.classPK    4s�6篹ϳ     7             =.  com/eye_cam/sirius/barionet/BarionetLoggerNotifee.classPK    4s�6C�T,  �  4             U/  com/eye_cam/sirius/barionet/BarionetLogMessage.classPK      c  �0    <*>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <*>
                                0  BCL file not exisiting or invalid tokencode version (use correct tokenizer version)
1  PRINT was not last statement in line or wrong delimiter used (allowed ',' or ';')
2  Wrong logical operator in IF statement (allowed '=','>','>=','<','<=','<>')
3  ONLY String VARIABLE can be used as parameter in OPEN,READ,PLAY,MIDxxx,EXEC
4  Wrong delimiter/parameter is used in list of parameters for this statement/function
5  ON statement must be followed by GOTO/GOSUB statement
6  First parameter of TIMER statement must be 1..4 (# for ON TIMER# GOSUB...)
7  Wrong element is used in this string/numeric expression, maybe a type mismatch
8  Divided by Zero
9  Wrong label is used in GOTO/GOSUB statement (allowed only a numeric constant)
10 Wrong symbol is used in source code, syntax error, tokenization is impossible
11 Wrong size of string/array is used in DIM (allowed only a numeric constant)
12 Wrong type in DIM statement used (only string variable or long variable/array allowed)
13 DIM was not last statement in line or wrong delimiter used (allowed only ',')
14 Missing bracket in expression or missing quote in string constant
15 Maximum nesting of calculations exceeded (too many brackets)
16 Assignment assumed (missing equal sign)
17 Wrong size of external tokenized TOK file (file might be corrupt)
18 Too many labels needed, tokenization is impossible
19 Identical labels in source code found, tokenization is impossible
20 Undefined label in GOTO/GOSUB statement found, tokenization is impossible
21 Missing THEN in IF/THEN statement
22 Missing TO in FOR/TO statement
23 Run-time warning: Possibly, maximum nesting of FOR-NEXT loops exceeded
24 NEXT statement without FOR statement or wrong index variable in NEXT statement
25 Maximum nesting of GOSUB-RETURN calls exceeded
26 RETURN statement without proper GOSUB statement
27 Lack of memory for temporary 1 kilobyte buffer in WRITE
28 String variable name conflict or too many string variables used
29 Long variable name conflict or too many long variables used
30 Insufficient space in far memory for temp string, variable or program allocation
31 Current Array index bigger then maximal defined index in DIM statement
32 Wrong current number of file/stream handler (allowed only 0..4)
33 Wrong file/stream type/type name or file/stream is already closed
34 This file/stream handler is already used or file/stream already opened
35 Missing AS statement in OPEN AS statement
36 Wrong address in IOCTL or IOSTATE
37 Wrong serial port number in OPEN statement
38 Wrong baudrate parameter for serial port in OPEN statement
39 Wrong parity parameter for serial port in OPEN statement
40 Wrong data bits parameter for serial port in OPEN statement
41 Wrong stop bits parameter for serial port in OPEN statement
42 Wrong serial port type parameter in OPEN statement
43 Run-time warning: You lost data during PLAY -- Please, increase string size
44 For TCP/CIFS file/stream only handler with number 0..5 are allowed
45 Only standard size (256 bytes) string variable allowed for READ and WRITE in STP file
46 Wrong or out of string range parameters in MID$ or MIDxxx
47 Only one STP/F_C file can be opened at a time
48 '&' can be used ONLY at the end of a line
49 Syntax error in multiline IF...ENDIF (maybe wrong nesting)
50 Length of Search Tag must not exceed size of target String Variable for READ
51 DIM string/array variable name already used
52 Wrong user function name or array declaration missing
53 General syntax error: wrong or not allowed delimiter or statement at this position
54 Run-time warning: Lost data during UDP READ -- Please, increase string size
55 Run-time warning: Lost data during UDP receiving -- 1k buffer limit
56 Run-time warning: Impossible to allocate 6 TCP handles, if 6 are needed free up TCP command port and/or serial local ports
57 Run-time warning: Lost data during concatenation of strings -- Please, increase target string size (DIM statement)
58 Run-time warning: Lost data during assignment of string -- Please, increase target string size (DIM statement)
59 Indicated flash page (WEBx) is out of range for this HW
60 COB file (F_C type) exceeds 64k limit
<*>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <*>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        HTTP/1.0 200
Content-type: text/xml

&L(0,"*");<?xml version="1.0" encoding="UTF-8" ?>
<barionet>
	<relays>
		<relay>&LIO(1,"%u",1);</relay>
		<relay>&LIO(1,"%u",2);</relay>
	</relays>
	<outputs>
		<output>&LIO(1,"%u",101);</output>
		<output>&LIO(1,"%u",102);</output>
		<output>&LIO(1,"%u",103);</output>
		<output>&LIO(1,"%u",104);</output>
	</outputs>
	<inputs>
		<digital>
			<input>&LIO(1,"%u",201);</input>
			<input>&LIO(1,"%u",202);</input>
			<input>&LIO(1,"%u",203);</input>
			<input>&LIO(1,"%u",204);</input>
			<input>&LIO(1,"%u",205);</input>
			<input>&LIO(1,"%u",206);</input>
			<input>&LIO(1,"%u",207);</input>
			<input>&LIO(1,"%u",208);</input>
		</digital>
		<analog>
			<absolute>
				<input>&LIO(2,"%u",501);</input>
				<input>&LIO(2,"%u",502);</input>
				<input>&LIO(2,"%u",503);</input>
				<input>&LIO(2,"%u",504);</input>
			</absolute>
			<percentual>
				<input>&LIO(2,"%u",501,100,0,1023,0);</input>
				<input>&LIO(2,"%u",502,100,0,1023,0);</input>
				<input>&LIO(2,"%u",503,100,0,1023,0);</input>
				<input>&LIO(2,"%u",504,100,0,1023,0);</input>		
			</percentual>
		</analog>
		<temperature>
			<absolute>
				<input>&LIO(1,"%u",601);</input>
				<input>&LIO(1,"%u",602);</input>
				<input>&LIO(1,"%u",603);</input>
				<input>&LIO(1,"%u",604);</input>
				<input>&LIO(1,"%u",605);</input>
				<input>&LIO(1,"%u",606);</input>
				<input>&LIO(1,"%u",607);</input>
				<input>&LIO(1,"%u",608);</input>
			</absolute>
			<celsius>
				<input>&LIO(2,"%u",601,1,0,16,0);</input>
				<input>&LIO(2,"%u",602,1,0,16,0);</input>
				<input>&LIO(2,"%u",603,1,0,16,0);</input>
				<input>&LIO(2,"%u",604,1,0,16,0);</input>
				<input>&LIO(2,"%u",605,1,0,16,0);</input>
				<input>&LIO(2,"%u",606,1,0,16,0);</input>
				<input>&LIO(2,"%u",607,1,0,16,0);</input>
				<input>&LIO(2,"%u",608,1,0,16,0);</input>
			</celsius>
			<celsius1000>
				<input>&LIO(2,"%u",601,1000,0,16,0);</input>
				<input>&LIO(2,"%u",602,1000,0,16,0);</input>
				<input>&LIO(2,"%u",603,1000,0,16,0);</input>
				<input>&LIO(2,"%u",604,1000,0,16,0);</input>
				<input>&LIO(2,"%u",605,1000,0,16,0);</input>
				<input>&LIO(2,"%u",606,1000,0,16,0);</input>
				<input>&LIO(2,"%u",607,1000,0,16,0);</input>
				<input>&LIO(2,"%u",608,1000,0,16,0);</input>
			</celsius1000>
		</temperature>
	</inputs>
</barionet>                                                                                                                                                                            HTTP/1.0 200
Content-type: text/html

<?xml version="1.0" encoding="UTF-8" ?>&L(0,"*");<data><rele1>&LIO(1,"%1u",1);</rele1><rele2>&LIO(1,"%1u",2);</rele2><entrada1>&LIO(1,"%u",201);</entrada1><entrada2>&LIO(1,"%u",202);</entrada2><entrada3>&LIO(1,"%u",203);</entrada3><entrada4>&LIO(1,"%u",204);</entrada4><entrada5>&LIO(1,"%u",205);</entrada5><entrada6>&LIO(1,"%u",206);</entrada6><entrada7>&LIO(1,"%u",207);</entrada7><entrada8>&LIO(1,"%u",208);</entrada8><salida1>&LIO(1,"%1u",101);</salida1><salida2>&LIO(1,"%1u",102);</salida2><salida3>&LIO(1,"%1u",103);</salida3><salida4>&LIO(1,"%1u",104);</salida4><analogico1>&LIO(2,"%u.%02u",501,500,0,1023,100);</analogico1><analogico2>&LIO(2,"%u.%02u",502,500,0,1023,100);</analogico2><analogico3>&LIO(2,"%u.%02u",503,500,0,1023,100);</analogico3><analogico4>&LIO(2,"%u.%02u",504,500,0,1023,100);</analogico4><analogicopc1>&LIO(2,"%u",501,100,0,1023,1);</analogicopc1><analogicopc2>&LIO(2,"%u",502,100,0,1023,1);</analogicopc2><analogicopc3>&LIO(2,"%u",503,100,0,1023,1);</analogicopc3><analogicopc4>&LIO(2,"%u",504,100,0,1023,1);</analogicopc4><temperatura1>&LIO(2,"%u.%1u",601,5,0,8,10);</temperatura1><temperatura2>&LIO(2,"%u.%1u",602,5,0,8,10);</temperatura2><temperatura3>&LIO(2,"%u.%1u",603,5,0,8,10);</temperatura3><temperatura4>&LIO(2,"%u.%1u",604,5,0,8,10);</temperatura4><temperatura5>&LIO(2,"%u.%1u",605,5,0,8,10);</temperatura5><temperatura6>&LIO(2,"%u.%1u",606,5,0,8,10);</temperatura6><temperatura7>&LIO(2,"%u.%1u",607,5,0,8,10);</temperatura7><temperatura8>&LIO(2,"%u.%1u",608,5,0,8,10);</temperatura8></data>                                                                                                                              