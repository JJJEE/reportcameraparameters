#pragma once
#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/DBGateway.h>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <ModuleAccess/RecordingModuleAccess.h>
#include <XML/XML.h>
#include <Threads/Thread.h>
#include <Threads/Condition.h>
#include <map>
#include <list>
#include <iostream>
#include <string>
using namespace std;
class SonyControlPlugin;

class SonyControlPluginSchedulingThread: public Thread
{
	private:
		ControlModuleAccess cma;
		Mutex cmaLock;
//		SonyControlPlugin *plg;
		DecodeModuleAccess dma;
//		RecordingModuleAccess rma;
		string configFile;
//		int id;

	public:

		class Hour
		{
			public:
				int hour;
				int mins;
				int sec;

				Hour();
				Hour(string desc); //hora en "HH:MM:SS"
				Hour(int h, int m, int s);
				Hour operator+ (Hour b);
				Hour operator- (Hour b);
				int waitTime(int dayMask, struct tm t);
				bool operator< (Hour b);
				string toString();
				string toContString();
				int secs();
		};
		

		class threadCall
		{
			public:
				int id;
				void* data;
				threadCall(int id, void* data):id(id), data(data){}
				threadCall():id(0), data(NULL){}
		};

		class alarmData
		{
			public:
				int pre;
				float fpsPre;
				int post;
				bool stop;
				alarmData():stop(false){}
		};

		class Schedule
		{
			public:
				Hour start, end;
				int dayMask;
				float fps;
				int devId;
				bool active;
				int type; //0:Normal Recording, 1: monitoring, 2:alarm
				alarmData *alarm;
				
				Schedule();	
//				time_t operator+ (struct timespec tm);
				time_t operator+ (const time_t t);
		};
		bool scheduling;
		Condition c;

		list<Schedule> recordings;

		struct frameInfo
		{
			int size;
			int x;
			int y;
			int bpp;
			int quality;
			int bitrate;
			bool isKey;
			void* frame; 
		};

	public:
		SonyControlPluginSchedulingThread(string file);//, SonyControlPlugin *plg);
//		~SonyControlPluginSchedulingThread();

		void* execute(int, void*);
		void startRecording(Schedule next);
		void endRecording(Schedule end);
		void endRecording(int devId);
		void startSession(RecordingModuleAccess *rma, int devId);
		void startAlarmRecording(Schedule next);
		void AlarmRecording(Schedule next);
		void endAlarmRecording(Schedule end);
		void addSchedule(Schedule);
		void removeSchedules(int id);
		void checkRecordings();
		void recordThread();
		void stop();
		bool recording(int id);
};

