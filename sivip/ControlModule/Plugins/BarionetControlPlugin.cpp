#include <Plugins/GestorControlExceptions.h>
#include <Http/HttpClient.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <Utils/StrUtils.h>
#include <Utils/ServiceException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ServiceFinder/ServiceFinder.h>
#include <BarionetControlPlugin.h>
#include <ControlModule/ControlModuleInterface.h>
#include <Utils/debugStackTrace.h>
#include <Threads/Mutex.h>
#include <AlarmModule/AlarmModuleException.h>
#include <iostream>
#ifndef WIN32
#include <unistd.h>
#endif
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <sstream>
#include <fstream>

using namespace std;

// Si DEBUG está definido, aparecerán los mensajes de DEBUG en pantalla.
// Principalmente dichos mensajes aparecen al comienzo y final de cada función.
#undef DEBUG
//#define DEBUG

Mutex updateIOMutex;

#define __CACHE_TIME 500000 

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

BarionetControlPlugin::DevInfo::DevInfo(	CPDeviceID id, 
														Address cam, 
														int model, 
														int size, 
														BarionetControlPlugin *cp ): 	bufferSize(size),
																								cp(cp), 
																								id(id), 
																								cam(cam), 
																								model(model), 
																								queued(&bLock), 
																								full(&bLock), 
																								inputs(NULL), 
																								outputs(NULL), 
																								inputValues(NULL), 
																								outputValues(NULL), 
																								relays(NULL), 
																								errors(0), 
																								inited(false), 
																								initing(false), 
																								error_logged(false), 
																								checkStarted(false),
																								am(	cp->address, 
																										"AlarmModule", 
																										cp->cn )
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::DevInfo" << endl;
#endif

	lastReboot.tv_sec 	= 0;
	lastReboot.tv_usec	= 0;
	readTime.tv_sec 		= 0;
	readTime.tv_usec		= 0;

	int *a=new int; *a=0;

	start(a);

#ifdef DEBUG
	cout << "<---- BCP::DevInfo::DevInfo" << endl;
#endif
}

void BarionetControlPlugin::DevInfo::startCheckingThread()
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::startCheckingThread" << endl;
#endif
	bLock.lock();
	try
	{
		if(!checkStarted)
		{
			checkStarted = true;
			int *b = new int; 
			*b = 1;

			start(b);
		}
	}
	catch(...){}

	bLock.unlock();

#ifdef DEBUG
	cout << "<---- BCP::DevInfo::startCheckingThread" << endl;
#endif
}

void BarionetControlPlugin::DevInfo::queue(	int id, 
															void *data, 
															Address *keepAliveAddr )
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::queue" << endl;
#endif

	list<Command>::iterator it, itEnd;
	struct timespec t;
	full.lock();
	try
	{
		while(buffer.size()>bufferSize)
		{
			t.tv_sec=time(NULL)+4;
			t.tv_nsec=0;
			if(full.wait(t))
			{
				if(keepAliveAddr!=NULL)
				{
					RPC::keepAlive(keepAliveAddr, 0);
				}
			}
		}
		if(id==CPServ::setPTZ)
			for(it=buffer.begin(), itEnd=buffer.end(); it!=itEnd; ++it)
				if(it->id==CPServ::setPTZ)
					it=buffer.erase(it);

		buffer.push_back(Command(id, data));
		if(buffer.size()>1)
		{
			cout<<" queued barionet command:"<<id<<" queue size:"<<buffer.size()<<endl;
		}
	}
	catch(...){}
	
	full.unlock();
	
	queued.signal();

#ifdef DEBUG
	cout << "<---- BCP::DevInfo::queue" << endl;
#endif
}

int BarionetControlPlugin::DevInfo::getSize()
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::getSize" << endl;
	cout << "<---- BCP::DevInfo::getSize" << endl;
#endif

	return bufferSize;
}

void BarionetControlPlugin::DevInfo::setSize(int size)
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::setSize" << endl;
#endif

	if(size>0)
		bufferSize=size;

#ifdef DEBUG
	cout << "<---- BCP::DevInfo::setSize" << endl;
#endif
}

int BarionetControlPlugin::DevInfo::getUse()
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::getUse" << endl;
#endif
	int i= buffer.size();
	if(i>bufferSize)
		i=bufferSize;

#ifdef DEBUG
	cout << "<---- BCP::DevInfo::getUse" << endl;
#endif

	return i;
}

float BarionetControlPlugin::DevInfo::getPercentInUse()
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::getPercentInUse" << endl;
#endif

	float f=buffer.size()/bufferSize;
	if(f>1)
		f=1;

#ifdef DEBUG
	cout << "<---- BCP::DevInfo::getPercentInUse" << endl;
#endif

	return f;
}

void* BarionetControlPlugin::DevInfo::execute(int, void *data)
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::execute" << endl;
#endif

	int thrId = (data == NULL) ? 0 : *(int*) data;

	if(thrId == 0)
	{
		for(;;)
		{
			try
			{
				queued.lock(); 
				Command c(-2,NULL);
				try
				{
					while( buffer.size() == 0 )
					{
						full.signal();

						struct timespec t;
						t.tv_sec = time(NULL)+1;
						t.tv_nsec = 0;
						queued.wait(t);
					}

					c=buffer.front();
					buffer.pop_front();
				}
				catch(Exception e)
				{
					cout 	<< " DevInfo::execute: exc: " << e.getClass() << ": " 
							<< e.getMsg() << endl;
				}
				catch(...){}

				full.signal();

				queued.unlock();

				switch(c.id)
				{
					case CPServ::setConfigParam:
						cp->run_setConfigParam(*((CPConfigParam*)c.data), id);
						delete (CPConfigParam*)c.data;
						c.data = NULL;
						break;

					case CPServ::setOutput:
						cp->run_setOutput(*((CPOutput*)c.data), id);
						delete (CPOutput*)c.data;
						c.data = NULL;
						break;

					case CPServ::setInput:
						cp->run_setInput();
						delete (CPInput*)c.data;
						c.data = NULL;
						break;

					case -1:
						((WaitInfo*)c.data)->i=1;
						((WaitInfo*)c.data)->c.lock();
						try
						{
							((WaitInfo*)c.data)->c.signal();
						}
						catch(...){
							
						((WaitInfo*)c.data)->c.unlock();
						break;
						}

					default:
						cout << " ERROR: incorrect queued command id"<<endl;
				}
			}
			catch(...){}

			if(buffer.size()+1<=bufferSize)
				full.signal();
		}
	}
	else if(thrId == 1)
	{
		//Alarm Reporting
		int inputs=0;
		while (inputs==0)
		{
			string s=cp->db.getModelConfigParam( "Barix", "Barionet", "/IOMapping/Inputs" );
			cout<<" Dev:AMThread::parsing:"<< s <<endl;
			char *c=(char*)s.c_str(), *l=NULL;
			inputs = strtol (c, &l, 10);
			if(inputs==0 && *l=='\0' ) //es "0" si l!=\0, es que hi ha agut un error de parsing (i.e., error d'accés a la BD)
			{
				cout<<" Dev::AMThread "<< id.id<<"  -  no inputs :"<<s <<endl;

#ifdef DEBUG
				cout << "<---- BCP::DevInfo::execute: error de parsing" << endl;
#endif

				return NULL;
			}
		}

		try
		{
			am.startSession(AMDeviceID(id.id));
		}
	
		catch(Exception &e){}
		
		//si es perd la sessió, l'AlmModClient ja la recupera
		AMAlarmId alm;
		alm.devId=id.id;
		alm.type=AMAlarmId::Input;
		for(;;)
		{
			try
			{
				{
					int devId=id.id;//it->first;

					cout<<" --- dev:"<<devId<<" - "<<inputs<<endl;

					AMAlarmId aid(AMAlarmId::Input, 0);
					aid.devId=devId;

					for(int i=1;i<=inputs ;++i)
					{
						aid.intId=i;

						CPInput in=cp->getInput(CPInput(i,0), devId);
						
						AMAlarmValue av;
						av.alarmId=aid;
						av.value=in.value;
						cout<<" setAlarm cam:"<<devId<<" input:"<<i<<" value:"<<in.value<<endl;
						am.setAlarm(av);
					}
				}
			}
			catch(AMSessionNotStablishedException &e)
			{
				try{
					am.startSession(AMDeviceID(id.id));
				}
				catch(AMSessionNotStablishedException &e){}
			}
			catch(Exception &e)
			{
				cout<<" setAlm exc : "<<e.getClass()<<"::"<<e.getMsg()<<endl;
				if(e.getClass() == string("SocketException"))
				{
					//camera no connectada
					sleep(30);
				}
				else if(e.getClass() == string("CPInvalidParamException"))
				{
					//output invalid, es problema de config del sirius
					sleep(30);
				}
				else
					sleep(1);
			}
			
			sleep(1);
		}
	}

#ifdef DEBUG
				cout << "<---- BCP::DevInfo::execute" << endl;
#endif
	return NULL;
}


void BarionetControlPlugin::DevInfo::wait(Address *keepAliveAddr)
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::wait" << endl;
#endif

	WaitInfo *w = new WaitInfo();


	queue(-1, w, keepAliveAddr);

	w->c.lock();
	try
	{
		while(w->i==0)
		{
			//	w.c.wait();
			struct timespec t;
			t.tv_sec=time(NULL)+4;
			t.tv_nsec=0;
			if(w->c.wait(t))
			{
				if(keepAliveAddr!=NULL)
				{
					cout<<" keepalive cua getRPC ->"<<keepAliveAddr->toString()<<endl;
					RPC::keepAlive(keepAliveAddr, 0);
				}
			}
		}
	}
	catch(...){}

	w->c.unlock();

	delete w;
	w = NULL;

#ifdef DEBUG
	cout << "<---- BCP::DevInfo::wait" << endl;
#endif
}


void BarionetControlPlugin::DevInfo::checkUpdateIO()
{
#ifdef DEBUG
	cout << "----> BCP::DevInfo::checkUpdateIO" << endl;
#endif

	string url = "http://" + cam.getIP().toString() + "/status.xml";
	HttpClient cl(url);
	HttpStream *stream = cl.sendRequest( "GET" );
	try
	{
		stream->getNextInformationChunk();	

 		if( 	( stream->getRequestStatus() >= 400 )		&& 
 				( stream->getRequestStatus() < 500 )	)
		{
			// Comentado por usar función insegura SYSTEM
			
			string cmd = "./tftpBario.pl " + cam.getIP().toString();
			cout << " DevInfo::DevInfo(): " << url << " not found " << endl << "run:" << cmd << endl;
			system( cmd.c_str() );
			cout<<" DevInfo::DevInfo():"<<url<<" uploaded"<<endl;
		}
		delete stream;
		stream = NULL;
	}
	catch(Exception &e)
	{
		delete stream;
		stream = NULL;
		throw;
	}

#ifdef DEBUG
	cout << "----> BCP::DevInfo::checkUpdateIO" << endl;
#endif
}

void BarionetControlPlugin::DevInfo::updateIO()
{
#ifdef DEBUG
	cout << "<---- BCP::DevInfo::updateIO" << endl;
#endif

	struct timeval now;
	gettimeofday(&now,NULL);
	struct timeval period=now-readTime;
	if(period.tv_sec==0 && period.tv_usec<__CACHE_TIME)
	{
#ifdef DEBUG
	cout << "<---- BCP::DevInfo::updateIO: period.tv_sec==0 && period.tv_usec<__CACHE_TIME" << endl;
#endif
		return;
	}

	updateIOMutex.lock();

	if(callMutex.tryLock())
	{
		HttpStream *stream=NULL;
		XML *status=NULL; 
		try
		{
			string url=string("http://")+cam.getIP().toString()+string("/status.xml");
			for(int i=0;i<3;++i)
			{
				cout<<" DevInfo::updateIO():"<<url<<endl;
				HttpClient cl(url);

				cout<<" DevInfo::updateIO() get:"<<url<<endl;
				stream=cl.sendRequest(string("GET"));
				cout<<"	DevInfo::updateIO gNxtInfChnk:"<< endl;

				stream->getNextInformationChunk();

				if(stream->getRequestStatus()>=400 && stream->getRequestStatus()<500)
				{
					if(stream!=NULL)
					{
						delete stream;
						stream=NULL;
					}
					checkUpdateIO();
					continue;
				}
				string bdy=stream->getNextInformationChunk();
				if(stream!=NULL)
				{
					delete stream;
					stream=NULL;
				}

				status=NULL; 
				try
				{
					status = xmlParser::parse(bdy, true);
				}
				catch(Exception e)
				{
					if(status!=NULL)
					{
						delete status;
						status=NULL;
					}
					continue;
				}

				xmlNode *n;
				string path=string("/barionet/relays[0]");
				
				n=status->getNode(path);
				if(n==NULL)
				{
					cout<<" DevInfo::updateIO() parse error, reading...:"<<endl;
					if(status!=NULL)
					{
						delete status;
						status=NULL;
					}
					continue;
				}

				try
				{
					for(int i=1;i<=nin;++i)
					{
						string path=string("/barionet/inputs/digital/[")+StrUtils::decToString(i-1, true)+string("]");
						n=status->getNode(path);
						if(n!=NULL)
							inputValues[i].digital=atoi(n->getCdata().c_str());
						else
						{
							cout<<" DevInfo::updateIO() n==NULL!!: entrada"<<i<<endl<<bdy<<endl;
#ifdef DEBUG
							cout << "<---- BCP::DevInfo::updateIO: n==NULL!!: entrada" << i << endl;
#endif
							throw(Exception(string("Invalid xmlStatus for ")+cam.getIP().toString()));
						}

						path=string("/barionet/inputs/temperature/celsius1000/[")+StrUtils::decToString(i-1, true)+string("]");
						n=status->getNode(path);
						if(n!=NULL)
						{
							float s=atof(n->getCdata().c_str());
							int temp=(int)s; 
							
							inputValues[i].temp=temp;
						}
						else
						{
#ifdef DEBUG
							cout << "<---- BCP::DevInfo::updateIO: n==NULL!!: temp "<<i << endl;
#endif
							throw(Exception(string("Invalid xmlStatus for ")+cam.getIP().toString()));
						}
					}
					for(int i=1;i<5;++i)
					{
						string path=string("/barionet/inputs/analog/absolute/[")+StrUtils::decToString(i-1, true)+string("]");

						n=status->getNode(path);

						if(n!=NULL)
							inputValues[i].analog=atoi(n->getCdata().c_str());
						else
							throw(	Exception("Invalid xmlStatus for " + cam.getIP().toString() ) );
					}

					for(int i=1;i<=nout;++i)
					{
						string path = "/barionet/outputs/[" + StrUtils::decToString(i-1, true) + "]";
						n=status->getNode(path);

						if(n!=NULL)
							outputValues[i].value = atoi( n->getCdata().c_str() );
						else
							throw(	Exception( "Invalid xmlStatus for " + cam.getIP().toString() ) );
					}

					for(int i=1; i <= nrel; ++i)
					{
						string path=string("/barionet/relays/[")+StrUtils::decToString(i-1, true)+string("]");
						n=status->getNode(path);

						if(n!=NULL)
							outputValues[nout+i].value=atoi(n->getCdata().c_str());
						else
							throw(Exception(string("Invalid xmlStatus for ")+cam.getIP().toString()));
					}

					readTime.tv_sec=now.tv_sec;
					readTime.tv_usec=now.tv_usec;

					if(status!=NULL)
					{
						delete status;
						status=NULL;
					}
				}
				catch(...){}

				if(status!=NULL)
				{
					delete status;
					status=NULL;
				}
			}
		}
		catch(...){}

		if(stream != NULL)
		{
			delete stream;
			stream=NULL;
		}

		callMutex.unlock();
		updateIOMutex.unlock();
	}
	else
	{
		updateIOMutex.unlock();

		throw(	Exception("Debug") );
	}

#ifdef DEBUG
	cout << "<---- BCP::DevInfo::updateIO" << endl;
#endif
}


SocketTCP* BarionetControlPlugin::devConnect(CPDeviceID id)
{
#ifdef DEBUG
	cout << "----> BCP::devConnect" << endl;
#endif

	if ( devs.find(id.id) == devs.end() )
	{
#ifdef DEBUG
		cout << "<---- BCP::devConnect: no encuentro" << endl;
#endif
		throw(ControlPluginException(0,string("Error connecting to device - no device found")));
	}
	else
		cout << "connect " << id.id << endl;

	Address a = devs[id.id]->cam;

	SocketTCP *socket = new SocketTCP(a);

	while(1)
	{
		try
		{
			cout<<" establish s:"<<(void*) socket<<endl; //a vegades es queden les conexions pillades, rebootem a sac :/
			socket->establish();

			// La risa de la festa a fer 15 reads...
			byte bufRd[512];
			socket->read(bufRd, 512);

			devs[id.id]->errors=0;

			return socket;
		}

		catch(SocketException &e) 
		{
			if(e.getMsg()==string("Error reading from socket"))
			{
				++devs[id.id]->errors;
			}
			if(e.getCode()!=1 && devs[id.id]->errors<10)
			{
				cout<<"DevConnect throw:"<<e.getCode()<<":"<<e.getMsg()<<" errors:"<<devs[id.id]->errors<<endl;
				if(socket!=NULL)
				{
					delete socket;
					socket=NULL;
				}

#ifdef DEBUG
				cout << "<---- BCP::devConnect: Error reading from socket" << endl;
#endif
				throw(e);
			}
			devs[id.id]->errors=0;
			struct timeval now;
			gettimeofday(&now,NULL);
			now=now-devs[id.id]->lastReboot;
			cout<<" -- error de connexio : "<<e.getCode()<<":"<<e.getMsg()<<" errors:"<<devs[id.id]->errors<<"  spent:"<<now.tv_sec<<endl;
			if(devs[id.id]->callMutex.tryLock())
			{
				try
				{
					if(now.tv_sec>=60)
					{
						HttpStream *stream=NULL;
						try
						{
							Address addr=socket->getAddr();
							if(socket!=NULL)
							{
								delete socket;
								socket=NULL;
							}
							cout<<" -----------------------------------------------  trying to reboot"<<endl; 
							char url[256];
							sprintf(url, "http://%s/setup.cgi?L=uireboot2.html&R", a.getIP().toString().c_str());

							string s(url);

							HttpClient cl(s);
							stream=cl.sendRequest(string("GET"));

							stream->getNextInformationChunk();
							string body=stream->getNextInformationChunk();
							if(stream!=NULL)
							{
								delete stream;
								stream=NULL;
							}
							gettimeofday(&devs[id.id]->lastReboot, NULL);
							cout<<" ----  reboot:"<<addr.toString()<<endl; 
							socket=new SocketTCP(addr);
							cout<<" ----  rebooted"<<endl; 
						}
						catch(Exception &e)
						{
							if(socket!=NULL)
							{
								delete socket;
								socket=NULL;
							}

							if(stream!=NULL)
							{
								delete stream;
								stream=NULL;
							}
							cout<<" Exception trying to reboot:" << e.getMsg() <<endl;
#ifdef DEBUG
							cout << "<---- BCP::devConnect: Exception trying to reboot" << endl;
#endif
							throw(ControlPluginException(0,string("Error connecting to device")));
						}
					}
					else
					{
						if(socket!=NULL)
						{
							delete socket;
							socket=NULL;
						}

						throw(ControlPluginException(0,string("Error connecting to device")));
					}
				}
				catch(Exception &e)
				{
					if(socket!=NULL)
					{
						delete socket;
						socket=NULL;
					}
					devs[id.id]->callMutex.unlock();

#ifdef DEBUG
					cout << "<---- BCP::devConnect: Exception" << endl;
#endif
					throw;
				}
				catch(...){}

				devs[id.id]->callMutex.unlock();
			}
		}
		catch(Exception &e)
		{
			if(socket!=NULL)
			{
				delete socket;
				socket=NULL;
			}

#ifdef DEBUG
			cout << "<---- BCP::devConnect: Exception2" << endl;
#endif
			throw;
		}
		catch(...)
		{
			if(socket!=NULL)
			{
				delete socket;
				socket=NULL;
			}
		}
	}

#ifdef DEBUG
	cout << "<---- BCP::devConnect" << endl;
#endif
}

string BarionetControlPlugin::devCmd(CPDeviceID id, string command, string prefix)
{
#ifdef DEBUG
	cout << "----> BCP::devCmd" << endl;
#endif
	SocketTCP *socket=NULL;;
	try
	{
		cout<<" BarionetControlPlugin::devCmd "<<command<<endl;
		socket = devConnect(id);
	}
	catch(Exception &e)
	{
		cout<<" BCP::devCmd -> devConnect exception: error connecting to device"<<endl;
#ifdef DEBUG
		cout << "<---- BCP::devCmd: error connecting to device" << endl;
#endif
		throw GestorException(0,string("error connecting to device:")+e.getMsg());
	}

	if(socket==NULL)
	{
#ifdef DEBUG
		cout << "<---- BCP::devCmd: socket==NULL" << endl;
#endif
		return string("");
	}
	try
	{
		string res=devCmd(&socket, command, prefix);

		if(socket!=NULL)
		{
			delete socket;
			socket=NULL;
		}
#ifdef DEBUG
		cout << "<---- BCP::devCmd: socket!=NULL" << endl;
#endif
		return res;
	}
	catch(Exception &e)
	{
		cout<<" BCP::devCmd -> devCmd exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		if(socket!=NULL)
		{
			delete socket;
			socket=NULL;
		}
	}
	catch(...)
	{
		if(socket!=NULL)
		{
			delete socket;
			socket=NULL;
		}
	}
#ifdef DEBUG
	cout << "<---- BCP::devCmd" << endl;
#endif

	return string("");
}
	
string BarionetControlPlugin::devCmd(SocketTCP **socket, string command, string prefix)
{
#ifdef DEBUG
	cout << "----> BCP::devCmd2" << endl;
#endif

	command+=string("\r");
	cout<<"-----Cmd write:"<<(*socket)->getAddr().toString()<<":"<<command<<endl;
	bool sent=false;
	if(socket==NULL || (*socket)==NULL)
	{
#ifdef DEBUG
		cout << "<---- BCP::devCmd2: socket==NULL || (*socket)==NULL" << endl;
#endif
		return string("");
	}

	while (!sent)
	{

		try{
			(*socket)->write((void*)command.c_str(), command.size());
			sent=true;
		}
		catch(SocketException &e)
		{
			Address addr=(*socket)->getAddr();

			if((*socket)!=NULL)
			{
				delete[] socket;
				socket = NULL;
			}

			(*socket)=new SocketTCP(addr);
			(*socket)->establish();
		}
	}
	char buf[256];
	int i=0;
	
	if(prefix==string(""))
	{
#ifdef DEBUG
		cout << "<---- BCP::devCmd2: prefix==string("")" << endl;
#endif
		return string("");
	}
	
	string resp;
	do
	{
		memset(buf,0,256);	
		i=0;
		do
		{
			try
			{
				(*socket)->read(buf+i, 1);
			}

			catch(SocketException &e)
			{
#ifdef DEBUG
				cout << "<---- BCP::devCmd2: SocketException &e" << endl;
#endif
				throw(e);
			}
			++i;
		}
		while(buf[i-1]!='\r' && i<256);

		resp=string(buf, i-1);
	}while(resp.substr(0,prefix.length()) != prefix);

#ifdef DEBUG
	cout << "<---- BCP::devCmd2" << endl;
#endif

	return resp;
}
	

BarionetControlPlugin::BarionetControlPlugin( string file ) : PluginControl( file ), 
																					db( address, type, cn )
{
#ifdef DEBUG
	cout << "----> BCP::BarionetControlPlugin" << endl;
#endif

	XML *conf = getConfigFromFile( file );

	configFile = conf->getNodeData("[0]/ModelConfigFile");

	delete conf;
	conf = NULL;

	if( configFile == string("") )
	{
#ifdef DEBUG
		cout << "<---- BCP::BarionetControlPlugin: configFile==string("")" << endl;
#endif

		throw(Exception("No Model config file specified in configuration file"));
	}

	conf = getConfigFromFile( configFile );

	string s("Select d.id, d.ip, d.puerto FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='Barix' AND m.codigo='Barionet' ");
	cout << "db.query = " << s << endl;
	XML *devInfo = db.call(s, 0);

	xmlNode *n=devInfo->getNode("/result");
	int numCams = atoi( (n->getParam("numRows") ).c_str());
	
	cout << "load config model:" << numCams << endl;

	loadModelConfig("Barionet", "/", "/", conf);

	cout << "loading " << numCams << " devices" << endl;


	for(int i=0; i < numCams; ++i)
	{
		cout << " loading device " << i << endl;

		string defaultPath = "/result/[" + StrUtils::decToString( i );

		n = devInfo->getNode( defaultPath + "]/id" );
		int id = atoi( n->getCdata().c_str() );

		n = devInfo->getNode( defaultPath + "]/ip" );
		string ip( n->getCdata().c_str() );

		if( ip.find('/') != string::npos ) //eliminem mascara de xarxa
		{
			ip = ip.substr(0, ip.find('/') );
		}
		
		n = devInfo->getNode( defaultPath + "]/puerto" );
		int puerto = atoi( n->getCdata().c_str() );
		
		cout << " loading device: " << i << 
					" [" << id << "] - " << 
					ip << ":" << puerto << endl;
		
		Address a( IP( ip ), puerto );

		int mod = -1;

		devs[id] = new DevInfo(	id, 
										a, 
										mod, 
										default_buffer_size, 
										this );
		try
		{	
			cout << "loadDBConfig:" << id << endl;
			loadDBConfig(id, conf);
			int thid = ci.start( new CamInit::execInfo(id, conf, this) );
			//ci.join(thid);//descomentar pq sigui sequencial (debug)
			//ci.detach(thid);
			devs[id]->start(); //thread de servei
		}

		catch(Exception &e)	//no s'ha trobat la camera
		{
			devs.erase( id );
		}
	}

	ci.joinAll();

	delete devInfo;
	devInfo = NULL;

	delete conf;
	conf = NULL;
	
	cout<<"----------plugin inicialitzat----------"<<endl;
#ifdef DEBUG
		cout << "<---- BCP::BarionetControlPlugin" << endl;
#endif
}

XML* BarionetControlPlugin::getConfigFromFile( string file )
{
#ifdef DEBUG
	cout << "-----> BarionetCP::getConfigFromFile: " << file << endl;
#endif
	
	char* buffer(NULL);
	std::size_t size(0);

	ifstream fichero( file.c_str(), ifstream::in );

	if ( fichero.is_open() )
	{
  		filebuf *pbuffer = fichero.rdbuf();
  		size = pbuffer->pubseekoff (0, fichero.end, fichero.in);
  		pbuffer->pubseekpos (0, fichero.in);

  		buffer = new char[size];
  		pbuffer->sgetn (buffer, size);
	}
	else
		cout << "Error opening file '" << file.c_str() << endl;

	fichero.close();

	XML *conf = xmlParser::parse( string( buffer, size ) );

	delete buffer;
	buffer = NULL;

#ifdef DEBUG
	cout << "<----- BarionetCP::getConfigFromFile" <<  endl;
#endif

	return conf;
}

BarionetControlPlugin::~BarionetControlPlugin()
{
#ifdef DEBUG
	cout << "----> BCP::~BarionetControlPlugin" << endl;
#endif
	for(	map<int, DevInfo*>::iterator it = devs.begin(), itEnd = devs.end(); 
			it!=itEnd; ++it)
	{
		if(it->second!=NULL)
		{
			delete it->second;
			it->second = NULL;
		}
	}
#ifdef DEBUG
	cout << "<---- BCP::~BarionetControlPlugin" << endl;
#endif
}


void BarionetControlPlugin::loadModelConfig(	string model, 
															string nodepath, 
															string dbpath, 
															XML *conf )
{
#ifdef DEBUG
	cout << "----> BCP::loadModelConfig( " << model << ", " 	<< 
														nodepath << ", " 	<< 
														dbpath << ", " 	<<
														"conf)" << endl;
#endif

	if( nodepath == string("/") )
		nodepath = string("/[0]/Models/All/");

	xmlNode *n = conf->getNode( nodepath );

	list<xmlNode*> kids = n->getKidList();
	int pos = nodepath.find("All");

	cout << nodepath << endl;

	if( kids.size() > 0 )
	{
		
		if ( pos == string::npos ) // (Barionet)
		{
			int dbidx = 0;
			for(	list<xmlNode*>::iterator it = kids.begin(), itEnd = kids.end();
					it != itEnd; ++it)
			{
				string newNodePath( string("") );
				string newDBPath( string("") );

				if(	(*it)->getParam("nodename") == "inc"	)
				{
					newNodePath = nodepath + (*it)->getName() + "[" + StrUtils::decToString(dbidx) + "]/";
					newDBPath 	= dbpath + StrUtils::decToString(dbidx+1) + "/";
					++dbidx;
				}
				else
				{
					newNodePath = nodepath + (*it)->getName() + "/";
					newDBPath 	= dbpath + (*it)->getName() + "/";
				}

				loadModelConfig(	model, 
										newNodePath, 
										newDBPath, 
										conf);
			}
		} // if ( pos == string::npos )

		
		else // Configuración genérica para todos los modelos (All)
		{	
			bool inc = false;
			int nidx = 0;

			for(	list<xmlNode*>::iterator it = kids.begin(), itEnd = kids.end();
					it != itEnd; ++it)
			{
				if(	(*it)->getParam("nodename") == "inc" )
					inc = true;
				else
				{
					loadModelConfig(	model, 
											nodepath + (*it)->getName() + "/", 
											dbpath + (*it)->getName() + "/", 
											conf );
				}

				++nidx;
			}

			if( inc )
			{
				string mpath( nodepath );
				mpath.replace(pos, 3, model);
				xmlNode *m = conf->getNode( mpath );
				inc = false;

				if( m != NULL )
				{
					list<xmlNode*> mkids = m->getKidList();
					int dbidx = 0;
					for(	list<xmlNode*>::iterator it = mkids.begin(), itEnd = mkids.end();
							it != itEnd; ++it) //mirem si tenim incs especifics del model
					{
						if( (*it)->getParam("nodename") == "inc" )
						{
							inc = true;

							loadModelConfig(	model, 
													mpath + (*it)->getName() + 
														"[" + StrUtils::decToString( dbidx ) + "]/", 
													dbpath + StrUtils::decToString( dbidx + 1 ) + "/", 
													conf );
							++dbidx;
						}
					}
				}	// if( m != NULL )

				if( !inc ) //si no en tenim, pillem els generics
				{
					int dbidx = 0;
					for(	list<xmlNode*>::iterator it = kids.begin(), itEnd = kids.end();
							it != itEnd; ++it)
					{
						if( (*it)->getParam("nodename") == "inc" )
						{
							loadModelConfig(	model, 
													nodepath + (*it)->getName() + 
														"[" + StrUtils::decToString( dbidx ) + "]/", 
													dbpath + StrUtils::decToString( dbidx + 1 ) + "/", 
													conf);
							++dbidx;
						}
					}
				} // if( !inc )
			}	// if( inc )
		} // else
	}	// if( kids.size() > 0 )

	else
	{
		if( pos != string::npos ) // Barionet
		{
			string mpath = nodepath;
			mpath.replace(pos, 3, model);
			xmlNode *m = conf->getNode(mpath);

			if(m!=NULL)
				n = m;
		} // if( pos != string::npos )
		string s = db.getModelConfigParam(	"Barix", 
														model, 
														dbpath );
		if(	( s != n->getCdata() )	|| 
				( s == "" )	)
		{
			db.setModelConfigParam(	"Barix", 
											model, 
											dbpath, 
											n->getCdata() );
		}
	} // else

#ifdef DEBUG
	cout << "<---- BCP::loadModelConfig( " << model << ", " 	<< 
														nodepath << ", " 	<< 
														dbpath << ", " 	<<
														"conf)" << endl;
#endif

}

SessionDispatcher BarionetControlPlugin::getSession(Address a)
{
#ifdef DEBUG
	cout << "----> BCP::getSession" << endl;
#endif

	map<string, SessionDispatcher>::iterator it, sessionNotFound;

	it = sessions.find( a.toString() );
	sessionNotFound = sessions.end();

	if ( it == sessionNotFound )
	{
		throw (CPSessionNotStablishedException(0, string("Session not established")));
	}

	it->second.initializeTimeOfSession();

#ifdef DEBUG
	cout << "<---- BCP::getSession" << endl;
#endif
	return it->second;
}

void* BarionetControlPlugin::CamInit::execute(int thid, void *parm)
{
#ifdef DEBUG
	cout << "----> BCP::CamInit::execute(" << thid << ")" << endl;
#endif
	
	execInfo *i=(execInfo*) parm;

	cout<<" "<<thid<<"   0 CamInit::execute i:"<<(void*)i<<endl;
	int id = i->id;
	cout<<" "<<thid<<"   0.5 CamInit::execute bcp:"<<(void*)i->bcp<<endl;
	cout<<" "<<thid<<"  ++++++  0.5 CamInit::execute dev:"<<id<<" :"<<(void*)i->bcp->devs[id]<<endl;

	if( i->bcp->devs[id]->initing == false )
	{
		i->bcp->devs[id]->initing = true;
		try
		{
			XML *conf = i->conf; 

			cout << " " << thid << "   1 CamInit::execute " << id << endl;
			i->bcp->loadDBConfig(id, conf);
			cout<<" "<<thid<<"   2 CamInit::loaded config: "<<id<<endl;
			i->bcp->devs[id]->checkUpdateIO();
			i->bcp->devs[id]->inited = true;
			i->bcp->devs[id]->error_logged = false;
		}
		catch(Exception &e)
		{
			cout<<" ---------- --------- ------------- BCP::CamInit::exec: "<<i->id<<" Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
		}
		i->bcp->devs[id]->initing=false;
	}
	delete i;
	i = NULL;

#ifdef DEBUG
	cout << "<---- BCP::CamInit::execute" << endl;
#endif
	return NULL;
}

void BarionetControlPlugin::loadDBConfig(	CPDeviceID id, 
														XML *conf )
{
#ifdef DEBUG
	cout << "----> BCP::loadDBConfig: " << StrUtils::decToString(id.id) << endl;
#endif

	string regularPath("/[0]/Models/Barionet/IOMapping");

	string outputsPath(	regularPath + "/Outputs"	);
	string inputsPath( 	regularPath + "/Inputs" 	);
	string relaysPath(	regularPath + "/Relays" 	);

	this->initializeDevInterfaces( id, conf, outputsPath, OUTPUT);
	this->initializeDevInterfaces( id, conf, inputsPath, INPUT);
	this->initializeDevInterfaces( id, conf, relaysPath, RELAY);

	int outputValues 	= devs[id.id]->nout + devs[id.id]->nrel;
	int inputValues 	= devs[id.id]->nin;

	devs[id.id]->outputValues 	= new DevInfo::OutputInfo[ outputValues ];
	devs[id.id]->inputValues 	= new DevInfo::InputInfo[ inputValues ];

	loadParamValuesIntoDB( id );

	cout<<" BCP::Configuration loaded"<<endl;

#ifdef DEBUG
	cout << "<---- BCP::loadDBConfig" << endl;
#endif
}

void BarionetControlPlugin::loadParamValuesIntoDB( CPDeviceID &id )
{
#ifdef DEBUG
	cout << "----> BCP::loadParamValuesIntoDB(" << StrUtils::decToString(id.id) << 
				")" << endl;
#endif

	DBGateway *dbLoad = new DBGateway( this->address, this->type, this->cn );

	try
	{
		loadASingleParamValueIntoDB( id, dbLoad, OUTPUT );
		loadASingleParamValueIntoDB( id, dbLoad, INPUT );
		loadASingleParamValueIntoDB( id, dbLoad, RELAY );
	}
	catch(Exception e)
	{
		cout 	<< "Exception: CamInit::exec 3 - cam:" << id.id << "  " 
				<< e.getClass() << "::" << e.getMsg() << endl;
	}

	if(dbLoad!=NULL)
	{
		delete dbLoad;
		dbLoad = NULL;
	}

#ifdef DEBUG
	cout << "<---- BCP::loadParamValuesIntoDB" << endl;
#endif
}

void BarionetControlPlugin::loadASingleParamValueIntoDB( CPDeviceID &id,
																			DBGateway *dbLoad,
																			interfaceType deviceInterface ) 
{
	switch( deviceInterface )
	{
		case OUTPUT:
			if ( valueIsEmptyInDB( id, dbLoad, "/IOMapping/Outputs" ) )
				insertValueIntoDB( id, dbLoad, "/IOMapping/Outputs", devs[id.id]->nout );		
			break;

		case INPUT:
			if ( valueIsEmptyInDB( id, dbLoad, "/IOMapping/Inputs" ) )
				insertValueIntoDB( id, dbLoad, "/IOMapping/Inputs", devs[id.id]->nin );
			break;

		case RELAY:
			if ( valueIsEmptyInDB( id, dbLoad, "/IOMapping/Relays" ) )
				insertValueIntoDB( id, dbLoad, "/IOMapping/Relays", devs[id.id]->nrel );
			break;

		default:
			break;
	}
}

bool BarionetControlPlugin::valueIsEmptyInDB( 	CPDeviceID &id, 
																DBGateway *dbLoad,
																string dbPath )
{
	return ( dbLoad->getDeviceConfigParam(	id.id, dbPath) == "" );
}

void BarionetControlPlugin::insertValueIntoDB(	CPDeviceID &id,
																DBGateway *dbLoad,
																string dbPath, 
																int numSignals )
{
	ostringstream 	convert; 
	convert << numSignals;
	string numSignalsStr( convert.str() );

	dbLoad->setDeviceConfigParam(	id.id, dbPath, numSignalsStr );
}

void BarionetControlPlugin::initializeDevInterfaces( 	CPDeviceID id,
																		XML *conf, 
																		string nodePath, 
																		interfaceType deviceInterface  )
{
#ifdef DEBUG
	cout << "----> BCP::initializeDevInterfaces( " 
			<< StrUtils::decToString(id.id) << ", " 
			<< nodePath << ", " << deviceInterface << ")" << endl;
#endif

	string node 	= conf->getNodeData( nodePath );
	int numSignals = atoi( node.c_str() );

	switch( deviceInterface )
	{
		case INPUT:
			devs[id.id]->nin = numSignals;
			devs[id.id]->inputs = new int[ numSignals ];
			for( int i = 0; i < numSignals; ++i )
				devs[id.id]->inputs[i] = 0;	
			break;

		case OUTPUT:
			devs[id.id]->nout = numSignals;
			devs[id.id]->outputs = new int[ numSignals ];
			for( int i = 0; i < numSignals; ++i )
			{
				devs[id.id]->outputs[i] = 0;	
			}
			break;

		case RELAY:
			devs[id.id]->nrel = numSignals;
			devs[id.id]->relays = new string[ numSignals ];
			for( int i = 0; i < numSignals; ++i )
				devs[id.id]->relays[i] = "";	
			break;

		default:
			break;
	}

#ifdef DEBUG
	cout << "<---- BCP::initializeDevInterfaces" << endl;
#endif
}

void BarionetControlPlugin::startSession(CPDeviceID id, Address *ipAddress)
{	
#ifdef DEBUG
	cout << "----> BCP::startSession" << endl;
#endif

	map<string,SessionDispatcher>::iterator itSes = sessions.find( ipAddress->toString() );
	bool sessionEnded = false;
	int oldSesID;
	
	cout << "BCP::StartSession:" << id.id << endl;
	
	if( itSes != sessions.end() )
	{
		if( id.id == itSes->second.getIdNumber() )
			throw(CPSessionAlreadyStablishedException(1, "Already established session"));

		else
		{
			// tenia una session con otro dispositivo -> cerramos e iniciamos la nueva
			oldSesID 		= itSes->second.getIdNumber();
			sessionEnded 	= true;
			try
			{
				endSession(itSes->second.getIdNumber(), ipAddress);
			}
			catch(Exception &e){}
		}
	}

	map<int, DevInfo*>::iterator itBuff = devs.find( id.id );
	
	Address cam;

	if(itBuff != devs.end())
	{
		cout<<"BCP::StartSession device found "<<(void*) itBuff->second <<endl;

		cam = itBuff->second->cam;
	}
	
	else
	{
		// no es a la llista de cameres, mirem si l'han afegit al sistema des de 
		// la inicialitzacio i l'afegim
		cout << "SonyCP::StartSession:" << id.id << " NOT inited "<< endl;

		RPC *rpc=db.getRPC(ipAddress, 5);
		string model("");
		try
		{
			cout<<"BCP:: load new Device:" << id.id << endl;
			string s("Select d.id, d.ip, d.puerto, m.codigo FROM dispositivo d, modelo m, fabricante f WHERE d.fabricante=f.id AND d.modelo=m.id AND f.nombre='Barix' AND m.codigo='Barionet' AND d.id=");
			s += StrUtils::decToString(id.id);

			XML *devInfo = db.call(s, 0, rpc);
			xmlNode *n=NULL;
			if(devInfo != NULL)
			{
				cout << devInfo->toString() << endl;
				n = devInfo->getNode("/result/row/ip");
			}
			RPC::keepAlive(ipAddress, type);

			if( n == NULL ) //no es enlloc, dispositiu invalid
			{
				db.freeRPC(rpc);
				if(devInfo != NULL)
				{
					delete devInfo;
					devInfo = NULL;
				}
				throw(CPInvalidParamException(0, "Invalid Device"));
			}

			string sip=n->getCdata();
			int pos=sip.find('/');
			if(pos!=string::npos)
			{
				sip=sip.substr(0,pos);
			}

			IP ip(sip);

			n=devInfo->getNode("/result/row/puerto");
			int port=atoi(n->getCdata().c_str());
			cam=Address(ip, port);

			delete devInfo;
			devInfo = NULL;

			int model = -1;
			devs[id.id] = new DevInfo(	id,
												cam,
												model,
												default_buffer_size, 
												this );
			
			XML *conf = getConfigFromFile( configFile );

			RPC::keepAlive(ipAddress, type);
			try
			{
				loadDBConfig(id, conf);
				//int thid = ci.start(new CamInit::execInfo(id.id, conf, this));
				//ci.join(thid);
			}
			catch(SocketException &e){}

			delete conf;
			conf = NULL;

			db.freeRPC(rpc);
		}
		catch( SocketTimeoutException &e )
		{
			db.freeRPC(rpc);
			throw(ControlPluginException( e.getCode(), e.getMsg() ) );
		}
		catch(...)
		{
			db.freeRPC(rpc);
			throw(ControlPluginException(0, "Invalid device id:" + StrUtils::decToString(id.id) ) );
		}
	}
	cout << " StartSession for:" << ipAddress->toString() << " dev:" << id.id << endl;

	SessionDispatcher ses(cam, id);
	sessions[ ipAddress->toString() ] = ses;

#ifdef DEBUG
	cout << "<---- BCP::startSession" << endl;
#endif

}

void BarionetControlPlugin::endSession(CPDeviceID id, Address *ipAddress)
{
#ifdef DEBUG
	cout << "----> BCP::endSession" << endl;
#endif

	map<string, SessionDispatcher>::iterator it, sessionNotFound;
	it = sessions.find( ipAddress->toString() );
	sessionNotFound = sessions.end();

	if(it == sessionNotFound )
	{
		return;
	}
	
	sessions.erase( it );
	cout << "BCP::endSession:" << id.id << endl;

	struct timeval now;
	gettimeofday(&now, NULL);


	for ( it = sessions.begin(); it != sessionNotFound; ++it )
	{
		struct timeval diffTime = now - it->second.getTimeOfCreationOfThisSession();
		if ( 	diffTime.tv_sec > sessionTimeout )
			sessions.erase( it );
	}

#ifdef DEBUG
	cout << "<---- BCP::endSession" << endl;
#endif
}


void BarionetControlPlugin::setInput(CPInput in, Address *a)
{
#ifdef DEBUG
	cout << "----> BCP::setInput" << endl;
#endif

	SessionDispatcher s = getSession(*a);

	if(in.id<1 || in.id>devs[s.getIdNumber()]->nin)
	{
		throw(CPInvalidParamException(0, string( "Identificador d'Input invalid")+StrUtils::decToString(in.id)));
	}
	
	if(in.value<0 || in.value>2)
	{
		throw(CPInvalidParamException(0, "Valor invalid"));
	}

	devs[s.getIdNumber()]->queue(CPServ::setInput, (void*)new CPInput(in));

#ifdef DEBUG
	cout << "<---- BCP::setInput" << endl;
#endif
}

CPInput BarionetControlPlugin::getInput(CPInput input, Address *ipAddress)
{
#ifdef DEBUG
	cout << "----> BCP::getInput" << endl;
#endif
	SessionDispatcher sessionWithDevice = getSession(*ipAddress);

#ifdef DEBUG
	cout << "<---- BCP::getInput" << endl;
#endif
	return getInput(input, sessionWithDevice.getIdNumber());
}

CPInput BarionetControlPlugin::getInput(CPInput in, int devId)
{
#ifdef DEBUG
	cout << "----> BCP::getInput" << endl;
#endif

	if ( devs.find(devId) == devs.end() )
	{
		throw(ControlPluginException(0,string("Error getting input for device: no device found")));
	}
	if(in.id<1 || in.id>devs[devId]->nin)
	{
		throw(CPInvalidParamException(0, string("Identificador d'Input invalid:")+StrUtils::decToString(in.id)));
	}

	DevInfo* dev=devs[devId];

	dev->updateIO();

	BarionetControlPlugin::DevInfo::InputInfo iInf=dev->inputValues[in.id];

	if(in.value==2) 							// temperature
	{
		cout<<"BarionetCP::getInput "<<in.id<<" tmp:"<<iInf.temp<<endl;

		return CPInput(in.id, iInf.temp); 
	}
	else if(in.id<5 && (in.value==1))	// analogic
	{
		cout<<"BarionetCP::getInput "<<in.id<<" anlg:"<<iInf.analog<<endl;

		return CPInput(in.id, iInf.analog); 
	}else 									 	// digital
	{
		cout<<"BarionetCP::getInput "<<in.id<<" digital:"<<iInf.digital<<endl;

		return CPInput(in.id, iInf.digital); 
	}

}


void BarionetControlPlugin::setOutput(CPOutput out, Address *a) 
{
#ifdef DEBUG
	cout << "----> BCP::setOutput" << endl;
#endif
	
	SessionDispatcher s=getSession(*a);

	if(out.id<1 || out.id>(devs[s.getIdNumber()]->nout + devs[s.getIdNumber()]->nrel))
	{
		throw(CPInvalidParamException(0, string("Identificador d'Output invalid:")+StrUtils::decToString(out.id)));
	}

	devs[s.getIdNumber()]->queue(CPServ::setOutput, (void *) new CPOutput(out));

#ifdef DEBUG
	cout << "<---- BCP::setOutput" << endl;
#endif
}

void BarionetControlPlugin::run_setOutput(CPOutput out, CPDeviceID id )
{
#ifdef DEBUG
	cout << "----> BCP::run_setOutput" << endl;
#endif
	
	int value;
	if(out.pulse==0)
		value = out.value;
	else 
		value = out.pulse/100; 

	if (value == 0 && out.pulse > 0)
		value = 2;
	else if (value>500)
		value = 500;
	else if (value==-1)
		value = 999;

	string cmd("");
	if(out.id<devs[id.id]->nout)
	{
		cout<<" 	Output "<<endl;
		cmd = "setio,10" + StrUtils::decToString(out.id) + 
				"," + StrUtils::decToString(value);
	}
	else
	{	
		cmd = "setio," + StrUtils::decToString( out.id - (devs[id.id]->nout) ) + 
				"," + StrUtils::decToString(value);
		cout << "relay: " << cmd << endl;
	}

	string res=devCmd(id, cmd, "");

	cout<<"BCP::setOutput result:"<<res<<endl;

#ifdef DEBUG
	cout << "<---- BCP::run_setOutput" << endl;
#endif

}

CPOutput BarionetControlPlugin::getOutput(CPOutput out, Address *ipAddress)
{
#ifdef DEBUG
	cout << "----> BCP::getOutput: " << out.id << endl;
#endif

	SessionDispatcher ses = getSession(*ipAddress);
	Address cam = ses.getIpAddress();	
	devs[ses.getIdNumber()]->wait(ipAddress);

	if(	( out.id < 1 ) || 
			( out.id > (devs[ses.getIdNumber()]->nout + devs[ses.getIdNumber()]->nrel) ) )
	{
		throw(CPInvalidParamException(0, string("Identificador d'Output invalid:")+StrUtils::decToString(out.id)));
	}

	DevInfo *dev = devs[ses.getIdNumber()];

	dev->updateIO();
 	BarionetControlPlugin::DevInfo::OutputInfo oInf = dev->outputValues[out.id];


#ifdef DEBUG
	cout << "<---- BCP::getOutput: " << out.id << endl;
#endif

	return CPOutput(out.id, oInf.value);
}

void BarionetControlPlugin::setCommandBufferSize(CPCommandBufferSize size, Address *ipAddress)
{
#ifdef DEBUG
	cout << "----> BCP::setCommandBufferSize" << endl;
#endif
	SessionDispatcher s=getSession(*ipAddress);
	devs[s.getIdNumber()]->setSize(size.nCommands);

#ifdef DEBUG
	cout << "<---- BCP::setCommandBufferSize" << endl;
#endif
}

CPCommandBufferSize BarionetControlPlugin::getCommandBufferSize(Address *ipAddress)
{
#ifdef DEBUG
	cout << "----> BCP::getCommandBufferSize" << endl;
#endif
	SessionDispatcher s=getSession(*ipAddress);

#ifdef DEBUG
	cout << "<---- BCP::getCommandBufferSize" << endl;
#endif
	return CPCommandBufferSize(devs[s.getIdNumber()]->getSize());
}


float BarionetControlPlugin::getCommandBufferPercentInUse(Address *ipAddress)
{
#ifdef DEBUG
	cout << "----> BCP::getCommandBufferPercentInUse" << endl;
#endif

	SessionDispatcher s=getSession(*ipAddress);

#ifdef DEBUG
	cout << "<---- BCP::getCommandBufferPercentInUse" << endl;
#endif

	return devs[s.getIdNumber()]->getPercentInUse();
}

int BarionetControlPlugin::getCommandBufferCommandsInUse(Address *ipAddress)
{
#ifdef DEBUG
	cout << "----> BCP::getCommandBufferCommandsInUse" << endl;
#endif

	SessionDispatcher s = getSession( *ipAddress );

#ifdef DEBUG
	cout << "<---- BCP::getCommandBufferCommandsInUse" << endl;
#endif

	return devs[s.getIdNumber()]->getUse();
}


CPMetadata BarionetControlPlugin::getMetadataValue(CPMetadata data, Address *ipAddress)
{
#ifdef DEBUG
	cout << "----> BCP::getMetadataValue" << endl;
#endif

	SessionDispatcher s=getSession(*ipAddress);
	
	cout<<"BCP::getMetadataValue:"<<data.name<<endl;
	map<string, string> *m = &devs[s.getIdNumber()]->metadata;
	
	if( m->find(data.name) == m->end())
	{
#ifdef DEBUG
	cout << "<---- BCP::getMetadataValue: no value" << endl;
#endif
		return CPMetadata("", "" );
	}

#ifdef DEBUG
	cout << "<---- BCP::getMetadataValue" << endl;
#endif

	return CPMetadata(data.name, (*m)[data.name]);
}

void BarionetControlPlugin::setMetadataValue(CPMetadata data, Address *a)
{
#ifdef DEBUG
	cout << "----> BCP::setMetadataValue" << endl;
#endif
	SessionDispatcher s=getSession(*a);
	
	devs[s.getIdNumber()]->metadata[data.name] = data.value;
	
	cout<<"BCP::setMetadataValue:" << data.name << " " << data.value << endl;

#ifdef DEBUG
	cout << "<---- BCP::setMetadataValue" << endl;
#endif
}


string BarionetControlPlugin::getKeyValueFromAListOfValues( string llista, 
																				string param )
{
#ifdef DEBUG
	cout << "----> BCP::getParamValue" << endl;
#endif
	list<string> params=StrUtils::split(llista, string("&"));
	list<string>::iterator it, itEnd;

	for(it = params.begin(), itEnd = params.end(); it != itEnd; ++it)
	{
		if(it->find(param+string("=")) == 0) 
		{

#ifdef DEBUG
			cout << "<---- BCP::getParamValue: no param value" << endl;
#endif
			return it->substr(param.size()+1,it->size()-(param.size()+1));
		}
	}

#ifdef DEBUG
	cout << "<---- BCP::getParamValue" << endl;
#endif

	return string("");
}

CPConfigParam BarionetControlPlugin::getConfigParam(CPConfigParam p, Address *a)
{
#ifdef DEBUG
	cout << "----> BCP::getConfigParam" << endl;
#endif

	SessionDispatcher ses=getSession(*a);

	RPC *rpc=db.getRPC(a,5);
	if(rpc==NULL)
	{
#ifdef DEBUG
	cout << "<---- BCP::getConfigParam: rpc==NULL" << endl;
#endif
		return CPConfigParam("","");
	}
	try
	{
		string res=db.getModelConfigParam("Barix", "Barionet", p.path, rpc);
		if(res!="")
		{
			db.freeRPC(rpc);

#ifdef DEBUG
			cout << "<---- BCP::getConfigParam: res!=""" << endl;
#endif
			return CPConfigParam(p.path, res);
		}

		res = db.getDeviceConfigParam(ses.getIdNumber(), p.path, rpc);
		if(res!="")
		{
			db.freeRPC(rpc);
#ifdef DEBUG
			cout << "<---- BCP::getConfigParam: res!="" 2" << endl;
#endif
			return CPConfigParam(p.path, res);
		}

		list<string> params=StrUtils::split(p.path, string("/"));
		if(params.size()>1)
		{
			throw(CPInvalidParamException(0, "Path invalid:"+p.path));
		}

		string param=params.front(); 

		if(param.substr(0,5)==string("Relay"))
		{
			int i=atoi(param.c_str()+5);

			if(i<0||i>devs[ses.getIdNumber()]->nrel)
			{
				throw(CPInvalidParamException(0, "Path invalid:"+p.path));
			}
			res=devs[ses.getIdNumber()]->relays[i];
			if(res=="")
			{
				string cmd=string("getio,")+StrUtils::decToString(i);
				string val=devCmd(ses.getId(), cmd, "state,");

				int state=atoi(val.c_str()+8);
				res=(state>0)?"on":"off";
			}
		}
		else
		{
			throw(CPInvalidParamException(0, "Path invalid:"+p.path));
		}

		db.freeRPC(rpc);
#ifdef DEBUG
		cout << "<---- BCP::getConfigParam: GetConfigParam" << endl;
#endif
		return CPConfigParam(p.path, res);
	}catch(Exception &e)
	{
		db.freeRPC(rpc);
		throw;
	}catch(...)
	{
		db.freeRPC(rpc);
		throw;
	}
#ifdef DEBUG
	cout << "<---- BCP::getConfigParam" << endl;
#endif
}

void BarionetControlPlugin::setConfigParam(CPConfigParam p, Address *a)
{
#ifdef DEBUG
	cout << "----> BCP::setConfigParam" << endl;
#endif

	SessionDispatcher s=getSession(*a);
	
	/*****Comprovació de params*****/
	list<string> params=StrUtils::split(p.path, string("/"));

	string param=params.front(); 

	if(param.substr(0,5)==string("Relay") && (atoi(param.substr(5).c_str())>devs[s.getIdNumber()]->nrel || (p.value!=string("on") && p.value!=string("off"))))
	{
#ifdef DEBUG
	cout << "<---- BCP::setConfigParam: relay invalid" << endl;
#endif

		throw(CPInvalidParamException(0, "Relay invalid:"+p.path));
	}

	devs[s.getIdNumber()]->queue(CPServ::setConfigParam, (void *) new CPConfigParam(p));
#ifdef DEBUG
	cout << "<---- BCP::setConfigParam" << endl;
#endif
}

void BarionetControlPlugin::run_setConfigParam(CPConfigParam p, CPDeviceID id)
{
#ifdef DEBUG
	cout << "----> BCP::run_setConfigParam" << endl;
#endif
	//Update camara
	/*
	 * - Pan Tilt Limit
	 * - Preset position
	 * - Zoom Type (Opt/Opt+Digt)
	 * - Gain/Brightness/Expos.
	 */
	cout << "BCP::setConfigParam:" << p.path << " " << p.value << endl;
	
	
	list<string> params=StrUtils::split(p.path, string("/"));
	string s=params.front(); 

	if(s.substr(0,s.length()-1)==string("Relay"))
	{
		int i=atoi(s.c_str()+5);
		string val=(p.value==string("on"))?"1":"0";
		
		string cmd=string("setio,")+StrUtils::decToString(i)+string(",")+val;
		string res=devCmd(id, cmd, "");
	}
	else
	{
		string path=p.path;
		int pos=path.rfind("/Delete");
		if(pos!=string::npos)
			path=path.substr(0,pos);
		if(p.path.rfind("/Delete")!=string::npos)
		{
			db.deleteDeviceConfig(id.id, path);
		}
		else
		{
			db.setDeviceConfigParam(id.id, p.path, p.value);
		}
	}
#ifdef DEBUG
	cout << "<---- BCP::run_setConfigParam" << endl;
#endif
}


CPConfigParamSeq BarionetControlPlugin::getConfigParamRecursive(	CPConfigParam p, 
																						Address *ipAddress )
{
#ifdef DEBUG
	cout << "----> BCP::getConfigParamRecursive" << endl;
#endif
	RPC *rpc = db.getRPC(ipAddress, 5);

	if(rpc == NULL)
	{
#ifdef DEBUG
	cout << "<---- BCP::getConfigParamRecursive: rpc==NULL" << endl;
#endif
		return CPConfigParamSeq();
	}

	try
	{
		SessionDispatcher ses = getSession(*ipAddress);

		CPConfigParamSeq seq;

		cout << " BCP::getConfigParamRecursive -> Retrieving Model Config ..." << endl;

		int id = db.getModelConfigNode("Barix", "Barionet",  p.path, rpc);
	 
		RPC::keepAlive(ipAddress, type);

		if( id != -1 )	// Obtiene configuración del modelo
		{
			string pref ( p.path );
			int pos ( pref.rfind("/") );

			if( pos == pref.size() - 1 )
			{
				pref 	= pref.substr(0,pos);
				pos = pref.rfind("/");
			}

			cout << " BCP::getConfigParamRecursive -> db path:" << pref << endl;

			if( pos != string::npos )
				pref = pref.substr(0, pos);

			string s( "SELECT * FROM getconfigparamrecursive(" + StrUtils::decToString(id) + 
							",'" + pref + "')" ); 

			cout << " BCP::getConfigParamRecursive -> call:" << s << endl;
			XML *res = db.call(s, 0, rpc);

			cout <<" BCP::getConfigParamRecursive -> db:"<<(void*)res << endl;
			xmlNode *n = res->getNode("/result");

			int nrows = 0;

			if(n != NULL)
				nrows = n->getKidList().size();

			if(nrows == 0)
			{
				throw(ControlPluginException(0, string("Database error:")+(res->toString())));
			}

			cout << " BCP::getConfigParamRecursive -> db rows:" << nrows << endl;

			for(int i=0;i<nrows;++i)
			{
				seq.push_back(	CPConfigParam( res->getNodeData("/result/[" + StrUtils::decToString(i) + "]/name"), 
														res->getNodeData("/result/[" + StrUtils::decToString(i) + "]/value")));
			}

			delete res;
			res = NULL;
		}
		else
		{
			cout << "WARNING: error while checking Database : deleting RPC " << endl;
			db.delRPC(rpc);
			rpc=db.getRPC(ipAddress,5);
		}

		cout << " BCP::getConfigParamRecursive -> Retrieving Device Config ..." << endl;

		id = db.getDeviceConfigNode(ses.getIdNumber(), p.path, rpc);

		if(id != -1)	// Obtiene configuración del dispositivo
		{
			string pref( p.path );
			int pos( pref.rfind("/") );

			if(pos == pref.size() - 1)
			{
				pref = pref.substr(0, pos);
				pos = pref.rfind("/");
			}

			cout << " BCP::getConfigParamRecursive -> db path:" << pref << endl;

			if(pos!=string::npos)
				pref=pref.substr(0,pos);

			string s( "SELECT * FROM getconfigparamrecursive(" + StrUtils::decToString(id) + 
							",'" + pref + "')" );

			cout << " BCP::getConfigParamRecursive -> call:" << s << endl;
			XML *res = db.call(s, 0, rpc);

			cout <<" BCP::getConfigParamRecursive -> db:"<<(void*)res << endl;
			xmlNode *n = res->getNode("/result");

			int nrows = 0;

			if(n != NULL)
				nrows = n->getKidList().size();

			if(nrows==0)
			{
				cout << " BCP::getConfigParamRecursive -> error respuesta: " << res->toString() << endl;
			}

			cout << " BCP::getConfigParamRecursive -> db rows:" << nrows << endl;

			for(int i=0; i<nrows; ++i)
			{
				seq.push_back(	CPConfigParam( res->getNodeData("/result/[" + StrUtils::decToString(i) + "]/name"), 
														res->getNodeData("/result/[" + StrUtils::decToString(i) + "]/value") ) );
			}

			delete res;
			res = NULL;
		}

		db.freeRPC(rpc);
#ifdef DEBUG
		cout << "<---- BCP::getConfigParamRecursive: db return seq" << endl;
#endif
		
		return seq;
	}
	catch(SocketTimeoutException &e)
	{
		db.freeRPC(rpc);
		throw(ControlPluginException(e.getCode(), e.getMsg()));
	}
	catch(...)
	{
		db.freeRPC(rpc);
		throw;
	}

#ifdef DEBUG
	cout << "<---- BCP::getConfigParamRecursive" << endl;
#endif
}


CPConfigParamSeq BarionetControlPlugin::getAllConfigParams(Address *a)
{
#ifdef DEBUG
	cout << "----> BCP::getAllConfigParams" << endl;
#endif
	SessionDispatcher s=getSession(*a);
	Address cam=s.getIpAddress();
	
	CPConfigParamSeq seq;

	for(int i=1;i<=2;++i)
	{
		string path=string("Relay")+StrUtils::decToString(i);
		string res=db.getDeviceConfigParam(s.getIdNumber(), path);
		if(res=="")
		{
			string cmd=string("getio,")+StrUtils::decToString(i);
			string val=devCmd(s.getId(), cmd, "state,");

			int state=atoi(val.c_str()+8);
			res=(state>0)?"on":"off";
		}

		seq.push_back(CPConfigParam(path, res));
	}
#ifdef DEBUG
	cout << "<---- BCP::getAllConfigParams" << endl;
#endif
	return seq;
}
