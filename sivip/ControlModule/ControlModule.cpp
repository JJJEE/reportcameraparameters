#include <XML/XML.h>
#include <XML/xmlNode.h>
#include <XML/xmlParser.h>
#include <Utils/RPC.h>
#include <Utils/RPCPacket.h>
#include <Utils/Exception.h>
#include <Sockets/SocketTimeoutException.h>
#include <Sockets/SocketException.h>
#include <ControlModule.h>
#include <Utils/debugStackTrace.h>
#include <base/ModuleInterface/ModuleInterfaceGestor.h>
#include <iostream>

using namespace std;

ControlModule::ControlModule(Address camAddr, short camType, Canis *cn) : ModuleGestor(camAddr, camType, cn) {
	STACKTRACE_INSTRUMENT();
	__class=string("ControlModule");
	moduleInterface=(ModuleInterfaceGestor*)new ControlModuleInterface(this->address, this->type, this->cn);
}
ControlModule::ControlModule(string xmlFile) : ModuleGestor(xmlFile) {
	STACKTRACE_INSTRUMENT();
	__class=string("ControlModule");
	moduleInterface=(ModuleInterfaceGestor*)new ControlModuleInterface(this->address, this->type, this->cn, xmlFile);
}


