/*
 *  NullPointerException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/NullPointerException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

NullPointerException::NullPointerException(string msg, int code): Exception(msg, code)
{

}

NullPointerException::NullPointerException(int code, string msg): Exception(msg, code)
{

}

NullPointerException::NullPointerException(SerializedException &se): Exception(se)
{

}

NullPointerException::~NullPointerException()
{
   
}

string NullPointerException::getClass()
{
	string c=string("NullPointerException");
	return c;
}

