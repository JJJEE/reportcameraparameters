/*
 *  NotEnoughMemoryException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class NotEnoughMemoryException : public Exception
{
 public:
	NotEnoughMemoryException(string msg, int code=0);
	NotEnoughMemoryException(int code, string msg);
	NotEnoughMemoryException(SerializedException &se);

	~NotEnoughMemoryException();
	
	virtual string getClass();
};

