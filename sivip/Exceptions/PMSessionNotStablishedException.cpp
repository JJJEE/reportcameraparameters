/*
 *  PMSessionNotStablishedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/PMSessionNotStablishedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

PMSessionNotStablishedException::PMSessionNotStablishedException(string msg, int code): Exception(msg, code)
{

}

PMSessionNotStablishedException::PMSessionNotStablishedException(int code, string msg): Exception(msg, code)
{

}

PMSessionNotStablishedException::PMSessionNotStablishedException(SerializedException &se): Exception(se)
{

}

PMSessionNotStablishedException::~PMSessionNotStablishedException()
{
   
}

string PMSessionNotStablishedException::getClass()
{
	string c=string("PMSessionNotStablishedException");
	return c;
}

