/*
 *  CameraNotConnectableException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class CameraNotConnectableException : public Exception
{
 public:
	CameraNotConnectableException(string msg, int code=0);
	CameraNotConnectableException(int code, string msg);
	CameraNotConnectableException(SerializedException &se);

	~CameraNotConnectableException();
	
	virtual string getClass();
};

