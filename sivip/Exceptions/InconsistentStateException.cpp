/*
 *  InconsistentStateException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/InconsistentStateException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

InconsistentStateException::InconsistentStateException(string msg, int code): Exception(msg, code)
{

}

InconsistentStateException::InconsistentStateException(int code, string msg): Exception(msg, code)
{

}

InconsistentStateException::InconsistentStateException(SerializedException &se): Exception(se)
{

}

InconsistentStateException::~InconsistentStateException()
{
   
}

string InconsistentStateException::getClass()
{
	string c=string("InconsistentStateException");
	return c;
}

