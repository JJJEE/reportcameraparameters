/*
 *  PMNoServicePermissionException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class PMNoServicePermissionException : public Exception
{
 public:
	PMNoServicePermissionException(string msg, int code=0);
	PMNoServicePermissionException(int code, string msg);
	PMNoServicePermissionException(SerializedException &se);

	~PMNoServicePermissionException();
	
	virtual string getClass();
};

