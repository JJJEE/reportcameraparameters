/*
 *  IdNotFoundException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/IdNotFoundException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

IdNotFoundException::IdNotFoundException(string msg, int code): Exception(msg, code)
{

}

IdNotFoundException::IdNotFoundException(int code, string msg): Exception(msg, code)
{

}

IdNotFoundException::IdNotFoundException(SerializedException &se): Exception(se)
{

}

IdNotFoundException::~IdNotFoundException()
{
   
}

string IdNotFoundException::getClass()
{
	string c=string("IdNotFoundException");
	return c;
}

