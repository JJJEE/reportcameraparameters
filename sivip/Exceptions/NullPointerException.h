/*
 *  NullPointerException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class NullPointerException : public Exception
{
 public:
	NullPointerException(string msg, int code=0);
	NullPointerException(int code, string msg);
	NullPointerException(SerializedException &se);

	~NullPointerException();
	
	virtual string getClass();
};

