/*
 *  IdAlreadyInUseException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/IdAlreadyInUseException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

IdAlreadyInUseException::IdAlreadyInUseException(string msg, int code): Exception(msg, code)
{

}

IdAlreadyInUseException::IdAlreadyInUseException(int code, string msg): Exception(msg, code)
{

}

IdAlreadyInUseException::IdAlreadyInUseException(SerializedException &se): Exception(se)
{

}

IdAlreadyInUseException::~IdAlreadyInUseException()
{
   
}

string IdAlreadyInUseException::getClass()
{
	string c=string("IdAlreadyInUseException");
	return c;
}

