/*
 *  InvalidStateException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class InvalidStateException : public Exception
{
 public:
	InvalidStateException(string msg, int code=0);
	InvalidStateException(int code, string msg);
	InvalidStateException(SerializedException &se);

	~InvalidStateException();
	
	virtual string getClass();
};

