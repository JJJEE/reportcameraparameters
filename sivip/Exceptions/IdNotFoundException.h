/*
 *  IdNotFoundException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__EXCEPTIONS__IDNOTFOUNDEXCEPTION_H
#define __SIRIUS__BASE__EXCEPTIONS__IDNOTFOUNDEXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class IdNotFoundException : public Exception
{
 public:
	IdNotFoundException(string msg, int code=0);
	IdNotFoundException(int code, string msg);
	IdNotFoundException(SerializedException &se);

	~IdNotFoundException();
	
	virtual string getClass();
};

#endif
