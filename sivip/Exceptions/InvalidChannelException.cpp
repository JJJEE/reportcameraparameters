/*
 *  InvalidChannelException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/InvalidChannelException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

InvalidChannelException::InvalidChannelException(string msg, int code): Exception(msg, code)
{

}

InvalidChannelException::InvalidChannelException(int code, string msg): Exception(msg, code)
{

}

InvalidChannelException::InvalidChannelException(SerializedException &se): Exception(se)
{

}

InvalidChannelException::~InvalidChannelException()
{
   
}

string InvalidChannelException::getClass()
{
	string c=string("InvalidChannelException");
	return c;
}

