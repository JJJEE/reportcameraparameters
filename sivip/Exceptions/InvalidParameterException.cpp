/*
 *  InvalidParameterException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/InvalidParameterException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

InvalidParameterException::InvalidParameterException(string msg, int code): Exception(msg, code)
{

}

InvalidParameterException::InvalidParameterException(int code, string msg): Exception(msg, code)
{

}

InvalidParameterException::InvalidParameterException(SerializedException &se): Exception(se)
{

}

InvalidParameterException::~InvalidParameterException()
{
   
}

string InvalidParameterException::getClass()
{
	string c=string("InvalidParameterException");
	return c;
}

