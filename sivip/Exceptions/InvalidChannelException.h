/*
 *  InvalidChannelException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__EXCEPTIONS__INVALIDCHANNELEXCEPTION_H
#define __SIRIUS__BASE__EXCEPTIONS__INVALIDCHANNELEXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class InvalidChannelException : public Exception
{
 public:
	InvalidChannelException(string msg, int code=0);
	InvalidChannelException(int code, string msg);
	InvalidChannelException(SerializedException &se);

	~InvalidChannelException();
	
	virtual string getClass();
};

#endif
