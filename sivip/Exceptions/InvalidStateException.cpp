/*
 *  InvalidStateException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/InvalidStateException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

InvalidStateException::InvalidStateException(string msg, int code): Exception(msg, code)
{

}

InvalidStateException::InvalidStateException(int code, string msg): Exception(msg, code)
{

}

InvalidStateException::InvalidStateException(SerializedException &se): Exception(se)
{

}

InvalidStateException::~InvalidStateException()
{
   
}

string InvalidStateException::getClass()
{
	string c=string("InvalidStateException");
	return c;
}

