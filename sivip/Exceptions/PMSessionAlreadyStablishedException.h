/*
 *  PMSessionAlreadyStablishedException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class PMSessionAlreadyStablishedException : public Exception
{
 public:
	PMSessionAlreadyStablishedException(string msg, int code=0);
	PMSessionAlreadyStablishedException(int code, string msg);
	PMSessionAlreadyStablishedException(SerializedException &se);

	~PMSessionAlreadyStablishedException();
	
	virtual string getClass();
};

