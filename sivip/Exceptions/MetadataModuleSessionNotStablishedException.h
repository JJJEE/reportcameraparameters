/*
 *  MetadataModuleSessionNotStablishedException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class MetadataModuleSessionNotStablishedException : public Exception
{
 public:
	MetadataModuleSessionNotStablishedException(string msg, int code=0);
	MetadataModuleSessionNotStablishedException(int code, string msg);
	MetadataModuleSessionNotStablishedException(SerializedException &se);

	~MetadataModuleSessionNotStablishedException();
	
	virtual string getClass();
};

