/*
 *  UnknownSerializedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/UnknownSerializedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

UnknownSerializedException::UnknownSerializedException(string msg, int code): Exception(msg, code)
{

}

UnknownSerializedException::UnknownSerializedException(int code, string msg): Exception(msg, code)
{

}

UnknownSerializedException::UnknownSerializedException(SerializedException &se): Exception(se)
{

}

UnknownSerializedException::~UnknownSerializedException()
{
   
}

string UnknownSerializedException::getClass()
{
	string c=string("UnknownSerializedException");
	return c;
}

