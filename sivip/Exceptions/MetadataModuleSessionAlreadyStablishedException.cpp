/*
 *  MetadataModuleSessionAlreadyStablishedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/MetadataModuleSessionAlreadyStablishedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

MetadataModuleSessionAlreadyStablishedException::MetadataModuleSessionAlreadyStablishedException(string msg, int code): Exception(msg, code)
{

}

MetadataModuleSessionAlreadyStablishedException::MetadataModuleSessionAlreadyStablishedException(int code, string msg): Exception(msg, code)
{

}

MetadataModuleSessionAlreadyStablishedException::MetadataModuleSessionAlreadyStablishedException(SerializedException &se): Exception(se)
{

}

MetadataModuleSessionAlreadyStablishedException::~MetadataModuleSessionAlreadyStablishedException()
{
   
}

string MetadataModuleSessionAlreadyStablishedException::getClass()
{
	string c=string("MetadataModuleSessionAlreadyStablishedException");
	return c;
}

