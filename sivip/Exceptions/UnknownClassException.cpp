/*
 *  UnknownClassException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/UnknownClassException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

UnknownClassException::UnknownClassException(string msg, int code): Exception(msg, code)
{

}

UnknownClassException::UnknownClassException(int code, string msg): Exception(msg, code)
{

}

UnknownClassException::UnknownClassException(SerializedException &se): Exception(se)
{

}

UnknownClassException::~UnknownClassException()
{
   
}

string UnknownClassException::getClass()
{
	string c=string("UnknownClassException");
	return c;
}

