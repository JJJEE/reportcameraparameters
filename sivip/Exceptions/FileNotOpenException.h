/*
 *  FileNotOpenException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__EXCEPTIONS__FILENOTOPENEXCEPTION_H
#define __SIRIUS__BASE__EXCEPTIONS__FILENOTOPENEXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class FileNotOpenException : public Exception
{
 public:
	FileNotOpenException(string msg, int code=0);
	FileNotOpenException(int code, string msg);
	FileNotOpenException(SerializedException &se);

	~FileNotOpenException();
	
	virtual string getClass();
};

#endif
