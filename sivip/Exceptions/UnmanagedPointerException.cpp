/*
 *  UnmanagedPointerException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/UnmanagedPointerException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

UnmanagedPointerException::UnmanagedPointerException(string msg, int code): Exception(msg, code)
{

}

UnmanagedPointerException::UnmanagedPointerException(int code, string msg): Exception(msg, code)
{

}

UnmanagedPointerException::UnmanagedPointerException(SerializedException &se): Exception(se)
{

}

UnmanagedPointerException::~UnmanagedPointerException()
{
   
}

string UnmanagedPointerException::getClass()
{
	string c=string("UnmanagedPointerException");
	return c;
}

