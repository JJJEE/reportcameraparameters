/*
 *  UnmanagedPointerException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class UnmanagedPointerException : public Exception
{
 public:
	UnmanagedPointerException(string msg, int code=0);
	UnmanagedPointerException(int code, string msg);
	UnmanagedPointerException(SerializedException &se);

	~UnmanagedPointerException();
	
	virtual string getClass();
};

