/*
 *  NotEnoughMemoryException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/NotEnoughMemoryException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

NotEnoughMemoryException::NotEnoughMemoryException(string msg, int code): Exception(msg, code)
{

}

NotEnoughMemoryException::NotEnoughMemoryException(int code, string msg): Exception(msg, code)
{

}

NotEnoughMemoryException::NotEnoughMemoryException(SerializedException &se): Exception(se)
{

}

NotEnoughMemoryException::~NotEnoughMemoryException()
{
   
}

string NotEnoughMemoryException::getClass()
{
	string c=string("NotEnoughMemoryException");
	return c;
}

