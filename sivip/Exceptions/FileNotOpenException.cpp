/*
 *  FileNotOpenException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/FileNotOpenException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

FileNotOpenException::FileNotOpenException(string msg, int code): Exception(msg, code)
{

}

FileNotOpenException::FileNotOpenException(int code, string msg): Exception(msg, code)
{

}

FileNotOpenException::FileNotOpenException(SerializedException &se): Exception(se)
{

}

FileNotOpenException::~FileNotOpenException()
{
   
}

string FileNotOpenException::getClass()
{
	string c=string("FileNotOpenException");
	return c;
}

