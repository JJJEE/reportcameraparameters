/*
 *  PMNoServicePermissionException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/PMNoServicePermissionException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

PMNoServicePermissionException::PMNoServicePermissionException(string msg, int code): Exception(msg, code)
{

}

PMNoServicePermissionException::PMNoServicePermissionException(int code, string msg): Exception(msg, code)
{

}

PMNoServicePermissionException::PMNoServicePermissionException(SerializedException &se): Exception(se)
{

}

PMNoServicePermissionException::~PMNoServicePermissionException()
{
   
}

string PMNoServicePermissionException::getClass()
{
	string c=string("PMNoServicePermissionException");
	return c;
}

