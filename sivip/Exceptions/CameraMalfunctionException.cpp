/*
 *  CameraMalfunctionException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/CameraMalfunctionException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

CameraMalfunctionException::CameraMalfunctionException(string msg, int code): Exception(msg, code)
{

}

CameraMalfunctionException::CameraMalfunctionException(int code, string msg): Exception(msg, code)
{

}

CameraMalfunctionException::CameraMalfunctionException(SerializedException &se): Exception(se)
{

}

CameraMalfunctionException::~CameraMalfunctionException()
{
   
}

string CameraMalfunctionException::getClass()
{
	string c=string("CameraMalfunctionException");
	return c;
}

