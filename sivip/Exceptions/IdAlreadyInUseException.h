/*
 *  IdAlreadyInUseException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __SIRIUS__BASE__EXCEPTIONS__IDALREADYINUSEEXCEPTION_H
#define __SIRIUS__BASE__EXCEPTIONS__IDALREADYINUSEEXCEPTION_H

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class IdAlreadyInUseException : public Exception
{
 public:
	IdAlreadyInUseException(string msg, int code=0);
	IdAlreadyInUseException(int code, string msg);
	IdAlreadyInUseException(SerializedException &se);

	~IdAlreadyInUseException();
	
	virtual string getClass();
};

#endif
