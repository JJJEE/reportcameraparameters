/*
 *  PMSessionNotStablishedException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class PMSessionNotStablishedException : public Exception
{
 public:
	PMSessionNotStablishedException(string msg, int code=0);
	PMSessionNotStablishedException(int code, string msg);
	PMSessionNotStablishedException(SerializedException &se);

	~PMSessionNotStablishedException();
	
	virtual string getClass();
};

