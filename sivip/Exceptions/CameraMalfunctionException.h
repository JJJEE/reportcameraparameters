/*
 *  CameraMalfunctionException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class CameraMalfunctionException : public Exception
{
 public:
	CameraMalfunctionException(string msg, int code=0);
	CameraMalfunctionException(int code, string msg);
	CameraMalfunctionException(SerializedException &se);

	~CameraMalfunctionException();
	
	virtual string getClass();
};

