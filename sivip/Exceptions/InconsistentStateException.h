/*
 *  InconsistentStateException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class InconsistentStateException : public Exception
{
 public:
	InconsistentStateException(string msg, int code=0);
	InconsistentStateException(int code, string msg);
	InconsistentStateException(SerializedException &se);

	~InconsistentStateException();
	
	virtual string getClass();
};

