/*
 *  CameraNotConnectableException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/CameraNotConnectableException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

CameraNotConnectableException::CameraNotConnectableException(string msg, int code): Exception(msg, code)
{

}

CameraNotConnectableException::CameraNotConnectableException(int code, string msg): Exception(msg, code)
{

}

CameraNotConnectableException::CameraNotConnectableException(SerializedException &se): Exception(se)
{

}

CameraNotConnectableException::~CameraNotConnectableException()
{
   
}

string CameraNotConnectableException::getClass()
{
	string c=string("CameraNotConnectableException");
	return c;
}

