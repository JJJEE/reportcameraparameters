/*
 *  PMSessionAlreadyStablishedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/PMSessionAlreadyStablishedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

PMSessionAlreadyStablishedException::PMSessionAlreadyStablishedException(string msg, int code): Exception(msg, code)
{

}

PMSessionAlreadyStablishedException::PMSessionAlreadyStablishedException(int code, string msg): Exception(msg, code)
{

}

PMSessionAlreadyStablishedException::PMSessionAlreadyStablishedException(SerializedException &se): Exception(se)
{

}

PMSessionAlreadyStablishedException::~PMSessionAlreadyStablishedException()
{
   
}

string PMSessionAlreadyStablishedException::getClass()
{
	string c=string("PMSessionAlreadyStablishedException");
	return c;
}

