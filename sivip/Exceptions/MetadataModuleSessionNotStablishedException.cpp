/*
 *  MetadataModuleSessionNotStablishedException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/MetadataModuleSessionNotStablishedException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

MetadataModuleSessionNotStablishedException::MetadataModuleSessionNotStablishedException(string msg, int code): Exception(msg, code)
{

}

MetadataModuleSessionNotStablishedException::MetadataModuleSessionNotStablishedException(int code, string msg): Exception(msg, code)
{

}

MetadataModuleSessionNotStablishedException::MetadataModuleSessionNotStablishedException(SerializedException &se): Exception(se)
{

}

MetadataModuleSessionNotStablishedException::~MetadataModuleSessionNotStablishedException()
{
   
}

string MetadataModuleSessionNotStablishedException::getClass()
{
	string c=string("MetadataModuleSessionNotStablishedException");
	return c;
}

