/*
 *  DeviceNotFoundException.cpp
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <Exceptions/DeviceNotFoundException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

DeviceNotFoundException::DeviceNotFoundException(string msg, int code): Exception(msg, code)
{

}

DeviceNotFoundException::DeviceNotFoundException(int code, string msg): Exception(msg, code)
{

}

DeviceNotFoundException::DeviceNotFoundException(SerializedException &se): Exception(se)
{

}

DeviceNotFoundException::~DeviceNotFoundException()
{
   
}

string DeviceNotFoundException::getClass()
{
	string c=string("DeviceNotFoundException");
	return c;
}

