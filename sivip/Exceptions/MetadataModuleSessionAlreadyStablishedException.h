/*
 *  MetadataModuleSessionAlreadyStablishedException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class MetadataModuleSessionAlreadyStablishedException : public Exception
{
 public:
	MetadataModuleSessionAlreadyStablishedException(string msg, int code=0);
	MetadataModuleSessionAlreadyStablishedException(int code, string msg);
	MetadataModuleSessionAlreadyStablishedException(SerializedException &se);

	~MetadataModuleSessionAlreadyStablishedException();
	
	virtual string getClass();
};

