/*
 *  InvalidParameterException.h
 *  
 *
 *  Created by David Marí Larrosa on 18/04/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class InvalidParameterException : public Exception
{
 public:
	InvalidParameterException(string msg, int code=0);
	InvalidParameterException(int code, string msg);
	InvalidParameterException(SerializedException &se);

	~InvalidParameterException();
	
	virtual string getClass();
};

