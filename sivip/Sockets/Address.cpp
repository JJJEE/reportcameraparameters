#include <Sockets/Address.h>
#include <Utils/StrUtils.h>
#include <string.h>
#include <iostream>
#include <Utils/debugStackTrace.h>

using namespace std;

// Constructores
Address::Address() : ip(), port(0)
{
}

Address::Address(IP ip, word port) : ip(ip.toDWord()), port(port)
{
}

Address::Address(Address *a) : ip(a->ip), port(a->port)
{
}

Address::Address(const Address &a) : ip(a.ip), port(a.port)
{
}

Address::~Address()
{
}

// Metodes
IP Address::getIP()
{
   return ip;
}

word Address::getPort()
{
   return port;
}

string Address::toString()
{
	string res=ip.getHostname();
	string aux=string(":")+StrUtils::decToString(port);
	res+=aux;
	return res;
}

bool Address::operator == (const Address &a)
{
	return ip.toDWord() == a.ip.toDWord() && port == a.port;
}

bool Address::operator != (const Address &a)
{
	return ip.toDWord() != a.ip.toDWord() || port != a.port;
}

