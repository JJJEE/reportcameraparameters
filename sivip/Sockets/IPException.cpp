#include <Sockets/IPException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

IPException::IPException(string msg, int code): Exception(msg, code)
{

}

IPException::IPException(int code, string msg): Exception(msg, code)
{

}

IPException::IPException(SerializedException &se): Exception(se)
{

}

IPException::~IPException()
{
   
}

string IPException::getClass()
{
	string c=string("IPException");
	return c;
}

