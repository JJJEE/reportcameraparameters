#include <Sockets/SocketException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

SocketException::SocketException(string msg, int code): Exception(msg, code)
{

}

SocketException::SocketException(int code, string msg): Exception(msg, code)
{

}

SocketException::SocketException(SerializedException &se): Exception(se)
{

}

SocketException::~SocketException()
{
   
}

string SocketException::getClass()
{
   string c=string("SocketException");
	return c;
}

