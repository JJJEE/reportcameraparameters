#include <Sockets/IP.h>
#include <sys/types.h>
#ifdef WIN32
#include <Utils/WindowsInclude.h>
#include <WinSock2.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#endif
#include <string.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <Sockets/IPException.h>
#include <Utils/debugStackTrace.h>


// Constructores
IP::IP() : ip(0), name(""), multicast(false)
{
}


IP::IP(string ip) : ip(0), name(""), multicast(false)
{
	struct in_addr ipaddr;
	struct hostent *he=NULL;
	unsigned long addr;
	this->multicast=false;

	memset(&ipaddr, 0, sizeof(struct in_addr));

	if ((addr=inet_addr(ip.c_str()))!=INADDR_NONE)
	{

		this->ip=addr;

		string sip=toString();
		int pos=sip.find(".",0);
		if(pos!=string::npos)
		{
			sip=sip.substr(0,pos);
			int m=atoi(sip.c_str());
			if(m>=224 && m<=239)
				this->multicast=true;
		}

		// resolucio inversa
		if(this->multicast || this->ip==0)
		{
			he=NULL;
		}		
		else
		{
			he=gethostbyaddr((char*)&this->ip, sizeof(dword), AF_INET);
		}
		if (he!=NULL)
		{
			//		string *strtmp=new string(he->h_name);
			//		if (strtmp!=NULL)
			//		{
			//			name=*strtmp;
			//			delete strtmp;
			//		 }
			//		else
			//	    name=ip;
			name=string(he->h_name);
		}
		else
			name=ip;
		return;
	}
	
	printf("IP::IP(): %s \n", ip.c_str());
	he=gethostbyname(ip.c_str());

	if (he!=NULL)
	{
		this->ip=*((dword*)(he->h_addr_list[0]));
		this->name=ip;
	}
	else
	{
		throw IPException(string("Invalid IP Address ")+ip);
	}
}

IP::IP(dword ip) : ip(0), name(""), multicast(false)
{
	struct in_addr ipaddr;
	struct hostent *he=NULL;
	this->multicast=false;

	memset(&ipaddr, 0, sizeof(struct in_addr));

	this->ip=ip;

	string sip=toString();
	int pos=sip.find(".",0);
	if(pos!=string::npos)
	{
		sip=sip.substr(0,pos);
		int m=atoi(sip.c_str());
		if(m>=224 && m<=239)
			this->multicast=true;
	}

	// resolucio inversa
	if (this->multicast || this->ip==0 || this->ip==0xffffffff)
		he=NULL;
	else
		// TODO: "posar en marxa" resolucio inversa
		he=NULL; //gethostbyaddr((const char*)&ip, sizeof(dword), AF_INET);

	if (he!=NULL)
	{
		name=toString();
/*		char* a=he->h_name;
		{
			string aux(a);
			name=aux;
		}
*/

		//string *strtmp=new string(he->h_name);
	/*	if (strtmp!=NULL)
		{
			name=*strtmp;
			delete strtmp;
		}
		else
			name=toString();
	*/
	}
	else
		name=toString();
}

IP::IP(const IP &ip) : ip(ip.ip), name(ip.name), multicast(ip.multicast)
{
}

IP::~IP()
{
}

// Metodes
string IP::getHostname()
{
	return name;
}

dword IP::toDWord() const
{
	return ip;
}

bool IP::isMulticast()
{
	return multicast;
}

string IP::toString()
{
	string sip;
	char aux[4];

	for (int i=0; i<4; i++)
	{
		sprintf(aux, "%d", *(((byte*)(&this->ip))+i));
		sip.append(aux);
		if (i!=3)
			sip.append(".");
	}

	return (sip);
}
