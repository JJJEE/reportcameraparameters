#include <Sockets/SocketUDP.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <errno.h>
#include <Utils/StrUtils.h>
#include <Utils/debugStackTrace.h>
/*#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
*/
#ifdef WIN32
#include <memory.h>
#include <Utils/windowsDefs.h>
#include <Winsock2.h>
#include <Ws2tcpip.h>
#else
#include <unistd.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <errno.h>
#include <iostream>

using namespace std;

// Privades
#define MAX_IP 65536
char initAuxData[MAX_IP*2];

dword SocketUDP::recvBuf=64*1024;

void SocketUDP::init()
{
	STACKTRACE_INSTRUMENT();
	// Comencem posant-li un valor pq no s'executi init() en bucle infinit
	maxPkt=1;
	
	int cnt=0;
	int lastBad=MAX_IP*2;
	int lastGood=0;

	maxPkt=1440;
	return;

retry:
	try
	{
		SocketUDP ss(Address(IP("127.0.0.1"), 9876), SOCK_SERVE);
		SocketUDP sc(Address(IP("127.0.0.1"), 9876));
	
		while (lastBad-lastGood>1)
		{
			int w;
			try
			{
				w=(lastBad+lastGood)>>1;
				w=sc.write(initAuxData, w);
				lastGood=w;
	//			cout << "(" << lastGood << ", " << lastBad << ")" << endl;
				int r=ss.read(initAuxData, w);
			}
			catch (Exception &e)
			{
				lastBad=w;
	//			cout << "(" << lastGood << ", " << lastBad << ")" << endl;
			}
		}
		
		int countShifts=0;
		while (lastGood>1)
		{
			lastGood>>=1;
			countShifts++;
		}	
	
		maxPkt=lastGood<<countShifts;
		
	//	cout << " maxpkt:" << lastGood << " lastbad:<" << lastBad << ")" << endl;
			
		int msgsz=0;
		int sz=sizeof(msgsz);
	//	cout<<" crida:"<<getsockopt(sd, SOL_SOCKET, SO_MAX_MSG_SIZE,(dataType)&msgsz, &sz)<<endl;
	
	//	cout<<" maxPacket get err:"<<error<<" sz: "<<msgsz<<endl;
	
	
		// Indiquem tamany de buffer, per 32 paquets
		recvBuf=32*maxPkt;
	}
	catch (Exception &e)
	{
		if (cnt++<10)
		{
			usleep(100);	// sleep molt curt, per sortir de cua d'execucio
			goto retry;
		}
		else
        {
			closeSocket();
   			throw e;
   	    }
	}
}

// Constructores
SocketUDP::SocketUDP(SocketUDP &sudp):Socket(sudp)
{
	STACKTRACE_INSTRUMENT();
	
	type=sudp.type;
	addr=sudp.addr;
	sd=sudp.sd;
	preSD=-1;
	tc=sudp.tc;
	multicast=sudp.multicast;
	tc.reset();
	memmove(&saddrl, &sudp.saddrl, sizeof(struct sockaddr_in));
	memmove(&saddrr, &sudp.saddrr, sizeof(struct sockaddr_in));
}

SocketUDP::SocketUDP(Address addr, enum SType type, Address *multicastInt)
{
	STACKTRACE_INSTRUMENT();

	this->type=type;
	this->addr=addr;
	sd=socket(PF_INET, SOCK_DGRAM, 0);
	preSD=-1;
		
//#ifdef WITH_DEBUG_STACKTRACE
//	cerr << "\n\nSocket::Socket() DUMP ----------" << endl;
//	STACKTRACE_DUMP();
//	cerr << "Assigned: " << sd << endl;
//#endif


	if (maxPkt==0)
		init();

	if (sd<0)
	{
		lastErrno=errno;
		closeSocket();
		throw SocketException(2, string("Unable to create socket"));
	}
	multicast=addr.getIP().isMulticast();
	enableBroadcast();

	switch (type)
	{
	case SOCK_CONNECT:

		memset(&saddrl, 0, sizeof(struct sockaddr_in));
		memset(&saddrr, 0, sizeof(struct sockaddr_in));
#ifdef BSD
		saddrr.sin_len=sizeof(struct sockaddr_in);
#endif
		saddrr.sin_family=PF_INET;
		saddrr.sin_port=htons(addr.getPort());
		saddrr.sin_addr.s_addr=addr.getIP().toDWord();

		if(multicast)
		{
			char ucyes=1;
			char ucno=0;
			int iyes=1;
			int ino=0;
			int ttlValue=64;

			if (setsockopt(sd, IPPROTO_IP, IP_MULTICAST_LOOP, (dataType)&ucyes, sizeof(ucyes)) < 0)
			{
				lastErrno=errno;
				closeSocket();
				throw SocketException(string("Unable to enable multicast loopback"));
			}

			if (setsockopt(sd, IPPROTO_IP, IP_MULTICAST_TTL, (dataType)&ttlValue, sizeof(ttlValue)) < 0)
			{
				lastErrno=errno;
				closeSocket();
				throw SocketException(string("Unable to set multicast TTL"));
			}

			if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, (dataType)&iyes, sizeof(iyes)) < 0)
			{
				lastErrno=errno;
				closeSocket();
				throw SocketException(string("Unable to set adress reuse "));
			}
			
#ifndef LINUX
#ifndef WIN32
			// Rancietats linux, comentari a bits/socket.h, amb SO_REUSEPORT 
			// pendent d'afegir...
			// if (setsockopt(sd,SOL_SOCKET,15,&i,sizeof(i)) < 0)
			// *** SEMBLA que Linux ja ho fa sol... ???
			if (setsockopt(sd, SOL_SOCKET, SO_REUSEPORT, (dataType)&iyes, sizeof(iyes)) < 0)
			{
				lastErrno=errno;
				closeSocket();
				throw SocketException(string("Unable to reuse port"));
			}
#endif
#endif
			
			struct ip_mreq mreq;
			memset(&mreq, 0, sizeof(mreq));
			
			if(multicastInt==NULL)
			{
//				cout << "Socket UDP: multicastInt NULL!!" << endl;
				mreq.imr_interface.s_addr=htonl(INADDR_ANY);
			}
			else
			{
//				cout << "Socket UDP: multicastInt " << multicastInt->getIP().toString() << endl;
				mreq.imr_interface.s_addr=multicastInt->getIP().toDWord();

				saddrr.sin_port=htons(multicastInt->getPort());
			}
			mreq.imr_multiaddr.s_addr=saddrr.sin_addr.s_addr;

			saddrr.sin_addr.s_addr=htonl(INADDR_ANY);
			if (bind(sd, (struct sockaddr*)&saddrr, sizeof(struct sockaddr_in))!=0)
			{
				lastErrno=errno;
				closeSocket();
#ifdef WIN32
				cout<<"bind error:"<<WSAGetLastError()<<endl;
#endif
				throw SocketException(string("Unable to bind socket to ").
									  append(addr.toString()));
			}
			cout<<"socket:"<<sd<<" bound"<<endl;
			
			if(setsockopt (sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (dataType)&mreq, sizeof(mreq))<0)
			{
				lastErrno=errno;
				closeSocket();
				throw SocketException(string("Error adding to multicast group"));
			}
			
	/*	saddrl.sin_len=saddrr.sin_len;
		saddrl.sin_family=saddrr.sin_family;
		saddrl.sin_port=saddrr.sin_port;
		saddrl.sin_addr=saddrr.sin_addr;
	*/
			// per poder enviar...
#ifdef BSD
			saddrr.sin_len=sizeof(struct sockaddr_in);
#endif
			saddrl.sin_family=PF_INET;
			saddrl.sin_port=htons(addr.getPort());
			saddrl.sin_addr.s_addr=addr.getIP().toDWord();
		}

		break;

    case SOCK_SERVE:
		memset(&saddrr, 0, sizeof(struct sockaddr_in));
		memset(&saddrl, 0, sizeof(struct sockaddr_in));
#ifdef BSD
		saddrr.sin_len=sizeof(struct sockaddr_in);
#endif
		saddrl.sin_family=PF_INET;
		saddrl.sin_port=htons(addr.getPort());
		saddrl.sin_addr.s_addr=addr.getIP().toDWord();

		if(!multicast)
		{
			if (bind(sd, (struct sockaddr*)&saddrl, sizeof(struct sockaddr_in))!=0)
			{
				this->closeSocket();
				string s=addr.getIP().toString()+string(" - ")+addr.toString();
				s=string("Unable to bind socket to ")+s;
				lastErrno=errno;
#ifdef WIN32
				cout<<"bind error serve:"<<WSAGetLastError()<<endl;
#endif
				throw SocketException(s);
			}
			cout<<"socket serve:"<<sd<<" bound"<<endl;
		}
		else
		{
#ifdef BSD
			saddrr.sin_len=saddrl.sin_len;
#endif			
			saddrr.sin_family=saddrl.sin_family;
			saddrr.sin_port=saddrl.sin_port;
			saddrr.sin_addr=saddrl.sin_addr;
		}
		break;
	}
	
	recvBuf=this->setRecvBuffer(recvBuf);
	recvBuf=this->setSendBuffer(recvBuf);

	int size;
	socklen_t d=sizeof(int);
#ifndef WIN32
	//getsockopt(sd, SOL_SOCKET, SO_RCVBUF, &size, &d);
	//cout<<" rcv Buf::"<<size<<endl;
	//getsockopt(sd, SOL_SOCKET, SO_SNDBUF, &size, &d);
	//cout<<" snd Buf::"<<size<<endl;
#endif
}

SocketUDP::~SocketUDP()
{
	STACKTRACE_INSTRUMENT();

	closeSocket();
}

// Metodes
/*int SocketUDP::read(void *buf, size_t n)
{
	STACKTRACE_INSTRUMENT();
	int b;
	socklen_t slen=sizeof(struct sockaddr_in);

	struct sockaddr_in saddr_read;
	memmove(&saddr_read, &saddrr, sizeof(struct sockaddr_in));
  
	if ((b=recvfrom(sd, (char*)buf, n, 0, (struct sockaddr*)&saddr_read,
					&slen))<0 || errno!=0)
	{
//		cout << "SocketUDP errno: " << errno << " b: " << b << endl;
		if (errno==EAGAIN)	// Timeout i no s'ha rebut res
			throw SocketTimeoutException(string("No bytes to read"));
		else if (b<0 && errno!=EINTR)
			throw SocketException(string("Error reading from socket:")+string(strerror(errno)));
	}
	
	tc.addR(b);
	return b;
}
*/

int SocketUDP::read(void *buf, size_t n, Address *srcAddr)
{
	STACKTRACE_INSTRUMENT();

	if(sd==-1)
	{
		lastErrno=errno;
		throw SocketException(string("Socket no inicialitzat"));
	}

	int b;
	socklen_t slen=sizeof(struct sockaddr_in);

	struct sockaddr_in saddr_read;
	memmove(&saddr_read, &saddrr, sizeof(struct sockaddr_in));

	if ((b=recvfrom(sd, (char*)buf, n, 0, (struct sockaddr*)&saddr_read,
					&slen))<0)// || errno!=0)
	{
//		cout << "SocketUDP errno: " << errno << " b: " << b << endl;
#ifdef WIN32
		errno=WSAGetLastError();
#endif
		if (errno==EAGAIN)	// Timeout i no hi ha dades
		{
			lastErrno=errno;
			throw SocketTimeoutException(string("No bytes to read"));
		}
		else // if (b<0 && errno!=EINTR)
		{
			lastErrno=errno;
			throw SocketException(string("Error reading from socket")+string(strerror(errno)));
		}
	}
	
	tc.addR(b);
//	cout << "SocketUDP: " << IP(saddrr.sin_addr.s_addr).toString() << ", " << ntohs(saddrr.sin_port) << endl;
	if (srcAddr!=NULL)
	{
        IP *ip=new IP(saddr_read.sin_addr.s_addr);
        
		Address *srcAd=new Address(*ip, ntohs(saddr_read.sin_port));

		*srcAddr=*srcAd;
		delete srcAd;
		delete ip;
	}

//	cout << "SocketUDP: " << recv->toString() << endl;

	return b;
}
int SocketUDP::peek(void *buf, size_t n)
{
	STACKTRACE_INSTRUMENT();
	if(sd==-1)
	{
		lastErrno=errno;
		throw SocketException(string("Socket no inicialitzat"));
	}
//	cout<<" peek 0"<<endl;
	memset(buf, 0, n);

//	cout<<" peek 1"<<endl;
	int b;
	socklen_t slen=sizeof(struct sockaddr_in);
	
	struct sockaddr_in saddr_read;
	memmove(&saddr_read, &saddrr, sizeof(struct sockaddr_in));
  
//	cout<<" peek 2 sd:"<<sd<<" buf:"<<buf<<" n:"<<n<<" slen:"<< slen <<" errno:"<<errno<<endl;
	if ((b=recvfrom(sd, (char*)buf, n, MSG_PEEK, (struct sockaddr*)&saddr_read,
					&slen))<0)// || errno!=0)
	{
//	cout<<" peek 3 errno:"<<errno<<endl;
#ifdef WIN32
		errno=WSAGetLastError();
//		cout << "SocketUDP errno: " << errno << " b: " << b << endl;
		if(errno==10060)
#else
		if (errno==EAGAIN)	// Timeout i no hi ha dades
#endif
		{
			lastErrno=errno;
			throw SocketTimeoutException(string("No bytes to read"));
		}
		else // if (b<0 && errno!=EINTR)
		{
			b=n;
#ifdef WIN32
			if(errno!=10040)
#endif
			{
				lastErrno=errno;
				throw SocketException(string("Error reading from socket peek b:")+StrUtils::decToString(b)+string(" errno:")+StrUtils::decToString(errno)+string(" sd:")+StrUtils::decToString(sd)+(" (prev:")+StrUtils::decToString(preSD)+(") maxPktSize:")+StrUtils::decToString(maxPkt)+string(" recvBuf:")+StrUtils::decToString(recvBuf));
			}
		}
	}
//	cout<<" peek 4"<<endl;

	tc.addR(b);
	return b;
}

int SocketUDP::write(void *buf, size_t n)
{
	STACKTRACE_INSTRUMENT();
	if(sd==-1)
	{
		lastErrno=errno;
		throw SocketException(string("Socket no inicialitzat"));
	}
	int flags=0;
//	if (broadcastEnabled)
//		flags=MSG_DONTROUTE;

	struct sockaddr *saddr_send=NULL;
	if(multicast)
		saddr_send=(struct sockaddr *)&saddrl;
	else
		saddr_send=(struct sockaddr *)&saddrr;

	int b=sendto(sd, (char*)buf, n, flags, saddr_send,
		sizeof(struct sockaddr_in));

	if (errno==ENOBUFS)
	{
		cout << "SocketUDP::write(): ENOBUFS! ENOBUFS! ENOBUFS!" << endl;
		cerr << "SocketUDP::write(): ENOBUFS! ENOBUFS! ENOBUFS!" << endl;
	}

	dword lastErr=this->getLastError();
	if (lastErr!=0)
	{
		cout << "SocketUDP::write(): lastErr: " << lastErr << endl;
		cout << "SocketUDP::write(): lastErr: " << lastErr << endl;
	}

	if (b<0)
	{
		lastErrno=errno;
		cout << string("SocketUDP::write(): Error writing to socket (errno ")+
				StrUtils::decToString(errno)+string(")") << endl;
		throw SocketException(string("SocketUDP::write(): Error writing to socket (errno ")+
				StrUtils::decToString(errno)+string(")"));
	}

	tc.addW(b);
	return b;
}

int SocketUDP::write(Address addr, void *buf, size_t n)
{
	STACKTRACE_INSTRUMENT();
	if(sd==-1)
	{
		lastErrno=errno;
		throw SocketException(string("Socket no inicialitzat"));
	}
	int flags=0;
	//	if (broadcastEnabled)
	//		flags=MSG_DONTROUTE;

	struct sockaddr_in saddr;
#ifdef BSD
	saddr.sin_len=sizeof(struct sockaddr_in);
#endif
	saddr.sin_family=PF_INET;
	saddr.sin_port=htons(addr.getPort());
	saddr.sin_addr.s_addr=addr.getIP().toDWord();

	int b=sendto(sd, (char*)buf, n, flags, (struct sockaddr*)&saddr,
		sizeof(struct sockaddr_in));

	if (errno==ENOBUFS)
	{
		cout << "SocketUDP::write(addr): ENOBUFS! ENOBUFS! ENOBUFS!" << endl;
		cerr << "SocketUDP::write(addr): ENOBUFS! ENOBUFS! ENOBUFS!" << endl;
	}

	dword lastErr=this->getLastError();
	if (lastErr!=0)
	{
		cout << "SocketUDP::write(addr): lastErr: " << lastErr << endl;
		cout << "SocketUDP::write(addr): lastErr: " << lastErr << endl;
	}

	if (b<0)
	{
		lastErrno=errno;
		throw SocketException(string("SocketUDP::write(addr): Error writing to socket (errno ")+
				StrUtils::decToString(errno)+string(")"));
	}

	tc.addW(b);
	return b;
}

void SocketUDP::closeSocket()
{
	STACKTRACE_INSTRUMENT();
	if (sd!=-1)
	{
		preSD=sd;
#ifdef WIN32
		closesocket(sd);
#else
		close(sd);
#endif

//#ifdef WITH_DEBUG_STACKTRACE
//		cerr << "\n\nSocketUDP::closeSocket() DUMP ----------" << endl;
//		STACKTRACE_DUMP();
//		cerr << "Closed: " << sd << endl;
//#endif

		sd=-1;
	}
}

void SocketUDP::setDefaultDestination(Address &a)
{
	this->addr=a;
#ifdef BSD
	this->saddrr.sin_len=sizeof(struct sockaddr_in);
#endif
	this->saddrr.sin_family=PF_INET;
	this->saddrr.sin_port=htons(this->addr.getPort());
	this->saddrr.sin_addr.s_addr=this->addr.getIP().toDWord();
}

dword SocketUDP::getMaxPacketSize()
{
	STACKTRACE_INSTRUMENT();
	return maxPkt;
}

