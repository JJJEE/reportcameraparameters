#include <Sockets/TransferCount.h>
#include <Utils/debugStackTrace.h>

TransferCount::TransferCount() : r(0), w(0)
{   
}

TransferCount::~TransferCount()
{  
}

void TransferCount::addR(int n)
{
   r+=n;
}

void TransferCount::addW(int n)
{
   w+=n;
}

qword TransferCount::getR()
{
   return r;
}

qword TransferCount::getW()
{
   return w;
}

qword TransferCount::getTotal()
{
   return r+w;
}

void TransferCount::reset()
{
   r=w=0;
}

