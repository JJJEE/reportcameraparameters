#pragma once
#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>
#include <Sockets/SocketException.h>

using namespace std;

class SocketTimeoutException : public SocketException
{
 public:
   SocketTimeoutException(string msg, int code=0);
   SocketTimeoutException(int code, string msg);
   SocketTimeoutException(SerializedException &se);
   
   ~SocketTimeoutException();
   
   virtual string getClass();
};

