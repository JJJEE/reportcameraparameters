#pragma once
#include <Utils/debugNew.h>
///#include <Utils/Types.h>
#include <Sockets/Address.h>
#include <Sockets/TransferCount.h>

#ifdef WIN32
  #include <Utils/WindowsInclude.h>
  #include <afxsock.h>         // For socket(), connect(), send(), and recv()
  typedef int socklen_t;
  typedef char* dataType;
#else
  #include <sys/types.h>       // For data types
  #include <sys/socket.h>      // For socket(), connect(), send(), and recv()
  #include <netinet/in.h>      // For sockaddr_in
  typedef void* dataType; 
#endif

class Socket
{
protected:
	static dword maxPkt;
	bool broadcastEnabled;
	Address addr;
	TransferCount tc;
	SOCKET sd;
	//DEBUG
	SOCKET preSD;

	int lastErrno;

#ifdef WIN32
	unsigned int currentTimeout;
#else
	timeval currentTimeout;
#endif	
public:
	Socket();
	virtual ~Socket();
	
	static void finalize();

//	virtual int read(void* buf, size_t n=1);
	virtual int read(void *buf, size_t n=1, Address *srcAddr=NULL);
	virtual int peek(void *buf, size_t n);
	virtual int write(void* buf, size_t n=1);
	virtual void setTimeout(dword ms);
	virtual dword getTimeout(void);
	Address getAddr();
	Address getLocalAddr();
	TransferCount getTC();
	virtual void closeSocket();
	virtual void enableBroadcast();
	virtual void setLinger(bool on, int time=1);
	virtual int getDescriptor();
	virtual int getLastErrno();
	virtual dword setRecvBuffer(dword bytes);
	virtual dword setSendBuffer(dword bytes);
	virtual dword getRecvBuffer();
	virtual dword getSendBuffer();
	virtual dword getLastError();
};

