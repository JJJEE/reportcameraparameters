#pragma once

#include <Utils/debugNew.h>
#include <Utils/Types.h>

class TransferCount
{
 private:
   qword r, w;
   
 public:
   TransferCount();
   ~TransferCount();
   
   void addR(int n=1);
   void addW(int n=1);
   qword getR();
   qword getW();
   qword getTotal();
   void reset();
   
};

