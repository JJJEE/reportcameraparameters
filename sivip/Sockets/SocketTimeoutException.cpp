#include <Sockets/SocketTimeoutException.h>
#include <Utils/Exception.h>
#include <Utils/debugStackTrace.h>

#include <string>
using namespace std;

SocketTimeoutException::SocketTimeoutException(string msg, int code): SocketException(msg, code)
{

}

SocketTimeoutException::SocketTimeoutException(int code, string msg): SocketException(msg, code)
{

}

SocketTimeoutException::SocketTimeoutException(SerializedException &se): SocketException(se)
{

}

SocketTimeoutException::~SocketTimeoutException()
{
   
}

string SocketTimeoutException::getClass()
{
	string c=string("SocketTimeoutException");
	return c;
}

