#include <Sockets/Socket.h>
#include <Sockets/SocketException.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>

/*
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
*/
#include <iostream>

dword Socket::maxPkt=0;

using namespace std;

#ifdef WIN32
static bool initialized = false;
#endif

Socket::Socket() : sd(-1), lastErrno(0)
{
	STACKTRACE_INSTRUMENT();

	memset(&currentTimeout, 0, sizeof(currentTimeout));

#ifdef WIN32
	if (!initialized) 
	{
		WORD wVersionRequested;
		WSADATA wsaData;

		wVersionRequested = MAKEWORD(2, 0);              // Request WinSock v2.0
		if (WSAStartup(wVersionRequested, &wsaData) != 0) 
		{  // Load WinSock DLL
			throw SocketException("Unable to load WinSock DLL");
		}
		initialized = true;
	}
#endif

}

Socket::~Socket()
{
}

void Socket::finalize()
{
	STACKTRACE_INSTRUMENT();

}

Address Socket::getAddr()
{
	STACKTRACE_INSTRUMENT();

   return addr;
}

Address Socket::getLocalAddr()
{
	STACKTRACE_INSTRUMENT();

#ifndef WIN32
	struct sockaddr_in saddr;
	socklen_t slen=sizeof(struct sockaddr_in);

	memset(&saddr, 0, sizeof(struct sockaddr_in));
#ifdef BSD
	saddr.sin_len=sizeof(struct sockaddr_in);
#endif

	if (getsockname(sd, (struct sockaddr*)&saddr, &slen)<0)
	{
		cout<<"getSockName params:"<<sd<<" "<<(void*)&saddr<<" "<<slen<<" Fam:"<<endl;
		throw SocketException("Cannot get local address");
	}
	Address local(IP((dword)saddr.sin_addr.s_addr), ntohs(saddr.sin_port));
	return local;
#else
	return Address(string("0.0.0.0"),1);
	struct sockaddr_storage saddr;
	socklen_t slen=sizeof(saddr);
	if (getsockname(sd, (LPSOCKADDR)&addr, &slen)!=0)
	{
		int lasterr= WSAGetLastError();
		cout<< "getSockName error, return"<<endl;
		//throw SocketException("Cannot get local address gethostname:"+StrUtils::decToString(lasterr));
	}
	char AddrName[NI_MAXHOST];
	 if (getnameinfo((LPSOCKADDR) &addr, slen, AddrName, sizeof(AddrName), NULL, 0, NI_NUMERICHOST) != 0)
	{
		int lasterr= WSAGetLastError();
		throw SocketException("Cannot get local address getnameinfo:"+StrUtils::decToString(lasterr));
	}
	Address local(IP(string(AddrName)), ntohs(SS_PORT(&addr)));
#endif
}

TransferCount Socket::getTC()
{
	STACKTRACE_INSTRUMENT();

   return tc;
}
/*
int Socket::read(void *bf, size_t n)
{
	STACKTRACE_INSTRUMENT();

  return -44;
}
*/
int Socket::read(void *buf, size_t n, Address *srcAddr)
{
	STACKTRACE_INSTRUMENT();

	return -44;
}

int Socket::peek(void *buf, size_t n)
{
	STACKTRACE_INSTRUMENT();

	return -44;
}

int Socket::write(void *bf, size_t n)
{
	STACKTRACE_INSTRUMENT();

   return -45;
}

void Socket::setTimeout(dword ms)
{
	STACKTRACE_INSTRUMENT();

	// Nomes timeouts de read, els de write "no tenen sentit"
#ifdef WIN32
	currentTimeout=ms;
	if (currentTimeout!=0)
#else
	currentTimeout.tv_usec=(ms%1000)*1000;
	currentTimeout.tv_sec=ms/1000;
	if (currentTimeout.tv_usec!=0 || currentTimeout.tv_sec!=0)
#endif
	{
		setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO,(dataType)&currentTimeout,
				   sizeof(currentTimeout));//<<endl;
/*		cout<<"Socket::setTimeout:"<<ms<<" res:"<<
		setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO,(dataType)&currentTimeout,
				   sizeof(currentTimeout))<<endl;
		timeval tout;
		int sz=sizeof(tout);
		getsockopt(sd, SOL_SOCKET, SO_RCVTIMEO,(dataType)&tout,
				   &sz);
		cout<<"Socket::setTimeout: get:"<<tout.tv_sec<<" secs, "<<tout.tv_usec<<" usecs"<<endl;
*/	}
	else
	{
		// Desactivem timeouts
		setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, NULL, 0);
	}
}

dword Socket::getTimeout(void)
{
	STACKTRACE_INSTRUMENT();

#ifdef WIN32
	return currentTimeout;
#else
	return currentTimeout.tv_sec*1000+currentTimeout.tv_usec/1000;
#endif
}

void Socket::closeSocket()
{
	STACKTRACE_INSTRUMENT();

}

void Socket::enableBroadcast()
{
	STACKTRACE_INSTRUMENT();

	int i=1;
	if (setsockopt(sd, SOL_SOCKET, SO_BROADCAST, (dataType)&i, sizeof(i))<0)
		throw SocketException("Cannot activate broadcast on socket");
	broadcastEnabled=true;
}

void Socket::setLinger(bool on, int time)
{
	struct linger l;
	if (on)
	{
		l.l_onoff=1;
		l.l_linger=time;
	}
	else
	{
		l.l_onoff=0;
		l.l_linger=0;
	}
	
	if (setsockopt(sd, SOL_SOCKET, SO_LINGER, (dataType)&l, sizeof(l))<0)
		throw SocketException("Cannot modify linger settings on socket");
}

int Socket::getDescriptor()
{
	STACKTRACE_INSTRUMENT();

	return sd;
}

int Socket::getLastErrno()
{
	STACKTRACE_INSTRUMENT();

	return lastErrno;
}

dword Socket::setRecvBuffer(dword bytes)
{
	dword setBuf=bytes;
	
	if (setsockopt(sd,SOL_SOCKET,SO_RCVBUF,(dataType)&setBuf,sizeof(setBuf)) != 0)
	{
		setBuf-=maxPkt;
		// Anem baixant fins a com a minim dos packets;
		while (errno==55 && setBuf>=2*maxPkt && setsockopt(sd,SOL_SOCKET,SO_RCVBUF,(dataType)&setBuf,sizeof(setBuf))<0)
			setBuf-=maxPkt;

	cout<<"Socket::setRecvBuf("<<bytes<<")->"<<setBuf<<" maxPkt:"<<maxPkt<<endl;
		if (setBuf<2*maxPkt)
		{
			lastErrno=errno;
			throw SocketException(errno, string("Unable to set Receive Buffer Size"));
		}
	}

	return setBuf;
}

dword Socket::setSendBuffer(dword bytes)
{
	dword setBuf=bytes;
	
	if (setsockopt(sd, SOL_SOCKET, SO_SNDBUF, (dataType)&setBuf,
		sizeof(setBuf)) < 0)
	{
		setBuf-=maxPkt;
		// Anem baixant fins a com a minim dos packets;
		while (errno==55 && setBuf>=2*maxPkt &&
			setsockopt(sd, SOL_SOCKET, SO_SNDBUF, (dataType)&setBuf,
				sizeof(setBuf))<0)
			setBuf-=maxPkt;

		if (setBuf<2*maxPkt)
		{
			lastErrno=errno;
			throw SocketException(errno, string("Unable to set Send Buffer Size"));
		}
	}

	return setBuf;
}

dword Socket::getRecvBuffer()
{
	dword getBuf=0;
	dword size=sizeof(getBuf);
	
	if (getsockopt(sd,SOL_SOCKET,SO_RCVBUF,(dataType)&getBuf,(socklen_t*)&size) < 0)
		throw SocketException(errno, string("Unable to get Receive Buffer Size"));

	return getBuf;
}

dword Socket::getSendBuffer()
{
	dword getBuf=0;
	dword size=sizeof(getBuf);
	
	if (getsockopt(sd, SOL_SOCKET, SO_SNDBUF, (dataType)&getBuf, (socklen_t*)&size) < 0)
		throw SocketException(errno, string("Unable to get Send Buffer Size"));

	return getBuf;
}

dword Socket::getLastError()
{
	dword err=0;
	socklen_t sz=sizeof(err);
	getsockopt(sd, SOL_SOCKET, SO_ERROR, (dataType)&err, &sz);
	return err;
}
