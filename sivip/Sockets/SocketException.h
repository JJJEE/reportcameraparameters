#pragma once
#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <Utils/Exception.h>

using namespace std;

class SocketException : public Exception
{
 public:
	 /*
	  *	Used Codes:
	  *		1 - Socket TCP - Unable to connect to *addr*
	  *		2 - Unable to create Cocket
	  */
   SocketException(string msg, int code=0);
   SocketException(int code, string msg);
   SocketException(SerializedException &se);
   ~SocketException();
   
   virtual string getClass();
};

