#pragma once
#include <Utils/debugNew.h>
#include <string>
#include <Utils/Types.h>
#include <iostream>

#ifndef IP_H_
#define IP_H_
using namespace std;

class IP
{
protected:
   dword ip;
   string name;
   bool multicast;
   
public:
	IP();
	IP(string ip);
	IP(dword ip);
	IP(const IP &ip);
	virtual ~IP();
	
	string toString();
	dword toDWord() const;
	string getHostname();
	bool isMulticast();
};

#endif
