#pragma once
#include <Utils/debugNew.h>
#include <string>
#include <Utils/Exception.h>

class IPException : public Exception
{
 public:
   IPException(string msg, int code=0);
   IPException(int code, string msg);
   IPException(SerializedException &se);
   ~IPException();
   
   virtual string getClass();
};

