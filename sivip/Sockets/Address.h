#ifndef ADDRESS_H_
#define ADDRESS_H_

#include <Utils/debugNew.h>
#include <Sockets/IP.h>
#include <Utils/Types.h>


class Address
{
protected:
	word port;
	IP ip;
	
public:
	Address();
	Address(IP ip, word port);
	Address(Address *a);
	Address(const Address &a);
	virtual ~Address();
	
	IP getIP();
	word getPort();
	string toString();
	
	bool operator == (const Address &a);
	bool operator != (const Address &a);
};

#endif
