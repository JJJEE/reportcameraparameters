#include <Sockets/SocketTCP.h>
#include <Sockets/Address.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#ifndef WIN32
#include <sys/select.h>
#include <unistd.h>
#endif
#include <Utils/debugStackTrace.h>
//#include <Utils/Timer.h>
#include <Utils/StrUtils.h>
#include <iostream>
using namespace std;


// Constructores
SocketTCP::SocketTCP(SocketTCP &stcp):Socket(stcp)
{
	type=stcp.type;
	addr=stcp.addr;
	sd=stcp.sd;
	tc=stcp.tc;
	tc.reset();
	memmove(&saddrl, &stcp.saddrl, sizeof(struct sockaddr_in));
	memmove(&saddrr, &stcp.saddrr, sizeof(struct sockaddr_in));
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "[" << pthread_self() << "] [CONIA]\tSD=" << sd << "\terrno=" << errno << "\tthis=" << this << endl;
}

SocketTCP::SocketTCP(Address addr, enum SType type, int qlen)
{
	this->type=type;
	this->addr=addr;

	sd=socket(PF_INET, SOCK_STREAM, 0);
	if (sd<0)
	{
		closeSocket();
		throw SocketException(2, string("Unable to create socket"));
	}

	// blocking per collons
/*   int fl=fcntl(sd, F_GETFL);
   fl&=~O_NONBLOCK;
   fcntl(sd, F_SETFL, fl);*/

	switch (type)
	{
    case SOCK_CONNECT:
		memset(&saddrl, 0, sizeof(struct sockaddr_in));
		memset(&saddrr, 0, sizeof(struct sockaddr_in));
#ifdef BSD
		saddrr.sin_len=sizeof(struct sockaddr_in);
#endif
		saddrr.sin_family=AF_INET;
		saddrr.sin_port=htons(addr.getPort());
		saddrr.sin_addr.s_addr=addr.getIP().toDWord();
		break;

    case SOCK_SERVE:
		memset(&saddrr, 0, sizeof(struct sockaddr_in));
		memset(&saddrl, 0, sizeof(struct sockaddr_in));
#ifdef BSD
		saddrl.sin_len=sizeof(struct sockaddr_in);
#endif
		saddrl.sin_family=AF_INET;
		saddrl.sin_port=htons(addr.getPort());
		saddrl.sin_addr.s_addr=addr.getIP().toDWord();
		int i=1;
    	if (setsockopt(sd,SOL_SOCKET,SO_REUSEADDR,(dataType)&i,sizeof(i)) < 0) 
	  	{
    		closeSocket();
			throw SocketException(string("Unable to set address reuse"));
		}

		if (bind(sd, (struct sockaddr*)&saddrl, sizeof(struct sockaddr_in))<0)
		{
    		closeSocket();
			throw SocketException(string("Unable to bind socket to ").
								  append(addr.toString()));
		}

		if (listen(sd, qlen)<0)
		{
    		closeSocket();
			throw SocketException(string("Unable to listen on socket"));
		}

		break;

	}

	//   cout << __FILE__ << " (" << __LINE__ << ") " << "[" << pthread_self() << "] [CONTR]\tSD=" << sd << "\terrno=" << errno << "\tthis=" << this << endl;
	//
}

SocketTCP::~SocketTCP()
{
	closeSocket();
}

// Metodes
/*int SocketTCP::read(void *buf, size_t n)
{

	//   cout << __FILE__ << " (" << __LINE__ << ") " << "-> [" << pthread_self() << "] SocketTCP::Read; this=" << this << ", sd=" << sd << endl;
	int b;

	do
	{
		b=recv(sd, (char*)buf, n, 0);
		//      cout << __FILE__ << " (" << __LINE__ << ") " << "errno = " << errno << ", sd= " << sd << endl;
	}
	while (errno==EINTR);

	if (b<0 && (errno!=0 && errno!=EAGAIN))
		throw SocketException(string("Error reading from socket"));

	//   tc.addR(b);
	//
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "<- [" << pthread_self() << "] SocketTCP::Read; this=" << this << ", sd=" << sd << endl;
	//
	//
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "[" << pthread_self() << "] [READ]\tSD=" << sd << "\terrno=" << errno << "\tthis=" << this << "\tb=" << b << "\nBUF=" << (char*)buf << endl;
	//
	return b;
}
*/

int SocketTCP::read(void *buf, size_t n, Address *srcAddr)
{
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "-> [" << pthread_self() << "] SocketTCP::Read; this=" << this << ", sd=" << sd << endl;
	int b;

	do
	{
		b=recv(sd, (char*)buf, n, 0);
#ifdef WIN32
		errno=WSAGetLastError();
#endif
		//      cout << __FILE__ << " (" << __LINE__ << ") " << "errno = " << errno << ", sd= " << sd << endl;
	}
	while (errno==EINTR);

	if (b<=0) //  && errno!=0)
	{
		cout << __FILE__ << " (" << __LINE__ << ") errno: " << errno << ", sd: " << sd << " b: " << b << endl;
		
#ifdef WIN32
		if (errno==10060)
#else
		if (errno==EAGAIN)	// Timeout i no hi ha dades
#endif
			throw SocketTimeoutException(string("No bytes to read"));
#ifdef WIN32
		else if (errno!=10040)
#else
		else
#endif
			throw SocketException(string("Error reading from socket, errno: ") + 
				StrUtils::decToString(errno));
	}
	//   tc.addR(b);
	//
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "<- [" << pthread_self() << "] SocketTCP::Read; this=" << this << ", sd=" << sd << endl;
	//
	//
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "[" << pthread_self() << "] [READ]\tSD=" << sd << "\terrno=" << errno << "\tthis=" << this << "\tb=" << b << "\nBUF=" << (char*)buf << endl;
	//
	return b;
}

int SocketTCP::peek(void *buf, size_t n)
{
	STACKTRACE_INSTRUMENT();
	if(sd==-1)
	{
		throw SocketException(string("Socket no inicialitzat"));
	}

	memset(buf, 0, n);

	int b;
	socklen_t slen=sizeof(struct sockaddr_in);
	
	struct sockaddr_in saddr_read;
	memmove(&saddr_read, &saddrr, sizeof(struct sockaddr_in));
  
  	b=recvfrom(sd, (char*)buf, n, MSG_PEEK, (struct sockaddr*)&saddr_read,
					&slen);
#ifdef WIN32
	errno=WSAGetLastError();
#endif
	if (b<=0) //  && errno!=0)
	{
		cout << "SocketTCP:peek() errno: " << errno << " sd: " << sd << " b: " << b << endl;
#ifdef WIN32
		if(errno==10060)
#else
		if (errno==EAGAIN)	// Timeout i no hi ha dades
#endif
		{
			throw SocketTimeoutException(string("No bytes to read"));
		}
#ifdef WIN32
		else if(b<0 && errno!=10040)
#else
		else if (b<0 && errno!=EINTR)
#endif
		{
			b=n;
			throw SocketException(string("Error reading from socket peek b: ")
				+ StrUtils::decToString(b) + string(" errno:") + 
				StrUtils::decToString(errno));
		}
	}
	
//	cout<<" peek 4"<<endl;

	tc.addR(b);
	return b;
}


int SocketTCP::write(void *buf, size_t n)
{
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "-> [" << pthread_self() << "] SocketTCP::Write; this=" << this << ", sd=" << sd << endl;
	int b;


	do
	{
		b=send(sd, (char*)buf, n, 0);

#ifdef WIN32
		if(b<0)
			errno=WSAGetLastError();
#endif
		//      cout << __FILE__ << " (" << __LINE__ << ") " << "errno = " << errno << endl;
	}
	while (errno==EINTR);
	
	if (b<=0) //  && (errno!=0 && errno!=EAGAIN))
	{
		cout << __FILE__ << " (" << __LINE__ << ") errno = " << errno << ", sd= " << sd << endl;
		throw SocketException(string("Error writing to socket:")+StrUtils::decToString(errno)+string(" ")+string(strerror(errno)));
	}

	tc.addW(b);
	// cout << __FILE__ << " (" << __LINE__ << ") " << "<- [" << pthread_self() << "] SocketTCP::Write; this=" << this << ", sd=" << sd << endl;
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "[" << pthread_self() << "] [WRITE]\tSD=" << sd << "\terrno=" << errno << "\tthis=" << this << "\tb=" << b << "\nBUF=" << (char*)buf << endl;
	//


	return b;
}

SocketTCP *SocketTCP::establish()
{
	switch (type)
	{
		case SOCK_CONNECT:
		{
			int saveflags,ret,back_err;
			//fd_set fd_w;
#ifndef WIN32
			saveflags=fcntl(sd,F_GETFL,0);
			if(saveflags<0) 
			{
				throw SocketException(string("Unable to get file descriptor flags"));
			}
#endif
			// Activem el NonBlocking pels timeouts de connect
#ifdef WIN32
			unsigned long nonblock=0;
			ioctlsocket(sd, FIONBIO, &nonblock);
			int error=connect(sd, (struct sockaddr*)&saddrr, sizeof(struct sockaddr_in));
			if (error<0) //TODO: in progress???
			{
				errno=WSAGetLastError();
				cout<<"establish errno:"<<errno<<" ip:"<<addr.toString()<<endl;
				throw SocketException(1,string("Unable to connect to ").
								  append(addr.toString()));
			}
#else
			if(fcntl(sd,F_SETFL,saveflags|O_NONBLOCK)<0) 
			{
				throw SocketException(string("Unable to set file descriptor flags"));
			}

			if (connect(sd, (struct sockaddr*)&saddrr, sizeof(struct sockaddr_in)) < 0 && errno!=EINPROGRESS) 
			{
					
				throw SocketException(string("Unable to connect to ").
								  append(addr.toString()));
			}
#endif

			fd_set fds;
			FD_ZERO(&fds);
			FD_SET(sd,&fds);
			
			timeval t;
#ifdef WIN32
			if (currentTimeout!=0)
			{
				t.tv_sec = currentTimeout/1000;
				t.tv_usec = (currentTimeout%1000)*1000;
			}
			else
			{
				t.tv_sec = 10;
				t.tv_usec = 0;
			}
#else
			if (currentTimeout.tv_usec!=0 || currentTimeout.tv_sec!=0)
			{
				//cout<<" CurrentTimeout:" <<  currentTimeout.tv_sec << "," <<  currentTimeout.tv_usec<< endl;
				t.tv_sec = currentTimeout.tv_sec;
				t.tv_usec = currentTimeout.tv_usec;
			}
			else
			{
				t.tv_sec = 10;
				t.tv_usec = 0;
			}
#endif


			
			int res = select(FD_SETSIZE, NULL, &fds, NULL, &t);//timeout);


			


			
			if(res<0) 
			{
				throw SocketException(string("Unable to select socket file descriptor"));
			}

			if(res==0)
			{
#ifdef WIN32
				cout<<"EstablishTimeOut "<<addr.toString()<<" time:" << t.tv_sec << "," << t.tv_usec<<" CurrentTimeout:" <<  currentTimeout<<" ms"<< endl;
#else
//				cout<<"EstablishTimeOut "<<addr.toString()<<" time:" << t.tv_sec << "," << t.tv_usec<<" CurrentTimeout:" <<  currentTimeout.tv_sec << "," <<  currentTimeout.tv_usec<< endl;
#endif
				throw SocketTimeoutException(1,string("Establish timed out"));//Aquest 1 el fem servir :P
			}

			// Redesactivem el NonBlocking
#ifdef WIN32
			nonblock=0;
			ioctlsocket(sd, FIONBIO, &nonblock);
#else
			if(fcntl(sd,F_SETFL,saveflags)<0) 
			{
				throw SocketException(string("Unable to set file descriptor flags"));
			}
#endif


			return this;
			break;

 		}
		case SOCK_SERVE:
		{
			socklen_t t=sizeof(struct sockaddr_in);
			SOCKET s;
			struct sockaddr_in saddrrnew;
			memset(&saddrrnew, 0, sizeof(struct sockaddr_in));

#ifdef BSD
			do
			{
#endif
				if ((s=accept(sd, (struct sockaddr*)&saddrrnew, &t))<0)
				{
					//	    cout << __FILE__ << " (" << __LINE__ << ") " << "ERROR " << errno << endl;
#ifdef BSD
					if(errno!=ECONNABORTED && errno!=EINTR)
					{
#endif
						throw SocketException(string("Unable to accept connections on ").
								append(addr.toString()));
#ifdef BSD
					}
#endif
				}
#ifdef BSD
			}while(s<0 && (errno==ECONNABORTED || errno==EINTR));
#endif

			IP iptmp(saddrrnew.sin_addr.s_addr);
			Address addrtmp(iptmp, htons(saddrrnew.sin_port));

			SocketTCP *stcp=new SocketTCP(*this);
			stcp->sd=s;
			stcp->addr=addrtmp;

			memmove(&stcp->saddrr, &saddrrnew, sizeof(struct sockaddr_in));

			return stcp;
		}
		break;
	}
	//   cout << __FILE__ << " (" << __LINE__ << ") " << "[" << pthread_self() << "] [ESTABLISH]\tSD=" << sd << "\terrno=" << errno << "\tthis=" << this << endl;
	//
	return NULL;
}

void SocketTCP::closeSocket()
{
	int z = 100;
	if (sd!=-1)
	{
#ifdef WIN32
		closesocket(sd);
#else
	z = close(sd);
#endif
	//cout << "Cierre: " << z << endl;
		      //cout << __FILE__ << " (" << __LINE__ << ") " << "[" << pthread_self() << "] [CLOSE]\tSD=" << sd << "\terrno=" << errno << string(strerror(errno)) <<  "\tthis=" << this << endl;
		sd=-1;
	}
}



