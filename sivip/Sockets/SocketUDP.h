#pragma once
#include <Sockets/Socket.h>
#include <Utils/WindowsInclude.h>
#include <Utils/debugNew.h>
//#include <Sockets/Address.h>
#include <Utils/Types.h>
#ifndef WIN32
#include <netinet/in.h>
#endif

class Socket;

class SocketUDP : public Socket
{
 private:
	struct sockaddr_in saddrl, saddrr;
	bool multicast;
	enum SType type;
	
	static dword recvBuf;
	void init();
   
 public:
	SocketUDP(SocketUDP &sUDP);
	SocketUDP(Address addr, enum SType type=SOCK_CONNECT, Address *multicastInt=NULL);
	virtual ~SocketUDP();
	
	virtual int read(void *buf, size_t n, Address *srcAddr=NULL);
	virtual int peek(void *buf, size_t n);
	virtual int write(void *buf, size_t n);
	virtual int write(Address addr, void *buf, size_t n);
	virtual void closeSocket();
	
	virtual void setDefaultDestination(Address &addr);
	
	virtual dword getMaxPacketSize(); 
};

