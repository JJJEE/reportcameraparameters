#pragma once

#include <Utils/debugNew.h>
#include <Sockets/Address.h>
//#include <Utils/Types.h>
#include <Sockets/Socket.h>
#ifndef WIN32
#include <arpa/inet.h>
#endif
 
class SocketTCP : public Socket
{
private:
	enum SType type;
	struct sockaddr_in saddrl, saddrr;
	
public:
	SocketTCP(SocketTCP &stcp);
	SocketTCP(Address addr, enum SType type=SOCK_CONNECT, int qlen=768);
	virtual ~SocketTCP();
	
//	int read(void *buf, size_t n);
	virtual int read(void *buf, size_t n, Address *srcAddr=NULL);
	virtual int peek(void *buf, size_t n);
	virtual int write(void *buf, size_t n);
	virtual SocketTCP *establish();
	virtual void closeSocket();
};

