#pragma once
#include <Utils/debugNew.h>
#include <Endian/Endian.h>
#include <Utils/Types.h>
#include <string>
#include <list>
#include <Sockets/SocketUDP.h>
using namespace std;

class AMDeviceID
{
	public:
		int id;
		AMDeviceID(int id);
		AMDeviceID();
		void toLocalEndian();
		void toNetworkEndian();
};

struct AMAlarmId
{
	public:
		static const int Input=0;
		static const int Output=1;
		static const int Filter=2;

		int devId;
		int type;
		int intId;
		string strId;
		
		AMAlarmId();
		AMAlarmId(int type, int id);
		AMAlarmId(int type, string id);
		AMAlarmId(int devId, int type, int id);
		AMAlarmId(int devId, int type, string id);
		void toLocal(char* buf);
		char* toNetwork(char* buf=NULL);
		int size();
		bool operator==(AMAlarmId);
		//bool operator<(AMAlarmId) const ;
		friend bool operator<(const AMAlarmId, const AMAlarmId);
};

class Alarm
{
	public:
		// Valors base de comparacio
		static const int VALUE_EQUAL=1;
		static const int VALUE_ABOVE=2;
		static const int VALUE_BELOW=4;

		// Valors derivats
		static const int VALUE_NEVER=0;
		static const int VALUE_ALWAYS=VALUE_EQUAL|VALUE_ABOVE|VALUE_BELOW;

		static const int VALUE_DIFFERENT=VALUE_ABOVE|VALUE_BELOW;
		static const int VALUE_EQUAL_OR_ABOVE=VALUE_EQUAL|VALUE_ABOVE;
		static const int VALUE_BELOW_OR_EQUAL=VALUE_EQUAL|VALUE_BELOW;

		int intBaseValue, baseMethod;
		double doubleBaseValue;
		Alarm();
		Alarm(int method, int value);
		Alarm(int method, double value);
		void toLocal(char* buf);
		char* toNetwork(char* buf=NULL);
		int size();
};

struct AMAlarmConfig
{
	public:
		struct AMAlarmId alarmId;
		Alarm data;
		Address addr;
		AMAlarmConfig();
		void toLocal(char* buf);
		char* toNetwork(char* buf=NULL);
		int size();
};

struct AMAlarmValue
{
	public:
		struct AMAlarmId alarmId;
		double value;
		bool raised;
		int64_t timestampMillis;
		
		AMAlarmValue();
		void toLocal(char* buf, uint32_t bufLen=0);
		char* toNetwork(char* buf=NULL);
		int size();
		bool operator==(AMAlarmValue);
		bool operator==(AMAlarmId aid);
};


class AMServ {
	public:
	
		static const int startSession=0; 
		static const int endSession=1; 
		static const int configureAlarm= 2;
		static const int listAlarms= 3;
		static const int subscribeAlarm= 4;
		static const int setAlarm= 5;

		static const int NumServ = 6; 
};

