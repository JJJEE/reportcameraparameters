#include <version.h>
#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <AlarmModule/AlarmModuleTypes.h>
#include <AlarmModule/AlarmModuleException.h>
#include <Sockets/Address.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <iostream>

#include <signal.h>

using namespace std;

int main(int argc, char** arg)
{

	try{
			
		//		Address a(IP("192.168.0.50"),4242);
		AlarmModuleAccess am("GestorCanisConfig.xml");

		cout<<"ControlModuleAcces::session start"<<endl;
		am.startSession(6);	//CS50

		AMAlarmConfig ac;

		ac.alarmId.devId=6;
		ac.alarmId.type=AMAlarmId::Input;
		ac.alarmId.intId=1;
		ac.data.intBaseValue=0;
		ac.data.doubleBaseValue=0.0;
		ac.data.baseMethod=Alarm::VALUE_DIFFERENT;

		cout <<"---- configAlarm"<<endl;
		am.configureAlarm(ac);

		cout <<" configured"<<endl;
/*		for(;;)
		{
			RPCPacket *p=am.getAsyncResult(60000);
			if(p==NULL)
			{
				cout<<" -";
				cout.flush();
				continue;
			}
			cout<<" Received:"<<(void*)p<<endl;
			AMAlarmValue av;
			av.toLocal((char*)p->data);
			cout<<"Alarm cam "<<av.alarmId.devId<<" type:"<<av.alarmId.type<<" num:"<<av.alarmId.intId<<endl;
			cout<<"      value:"<<av.value<<endl;
		}
*/
	}catch(Exception &e)
	{
		cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
	}
}

