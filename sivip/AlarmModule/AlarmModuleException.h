#pragma once
#include <Utils/debugNew.h>
#include <Utils/Exception.h>

class AlarmModuleException: public Exception
{
	public:
		AlarmModuleException(int code, string msg);
		AlarmModuleException(SerializedException &se);
		virtual string getClass();
};

class AMInvalidParamException: public AlarmModuleException
{
	public:
		AMInvalidParamException(int code, string msg);
		AMInvalidParamException(SerializedException &se);
		virtual string getClass();
};

class AMSessionAlreadyStablishedException: public AlarmModuleException
{
	public:
		AMSessionAlreadyStablishedException(int code, string msg);
		AMSessionAlreadyStablishedException(SerializedException &se);
		virtual string getClass();
};

class AMSessionNotStablishedException: public AlarmModuleException
{
	public:
		AMSessionNotStablishedException(int code, string msg);
		AMSessionNotStablishedException(SerializedException &se);
		virtual string getClass();
};

