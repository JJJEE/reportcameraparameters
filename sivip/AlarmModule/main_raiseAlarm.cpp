#include <version.h>
#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <AlarmModule/AlarmModuleTypes.h>
#include <AlarmModule/AlarmModuleException.h>
#include <Sockets/Address.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <iostream>

#include <signal.h>
#include <stdlib.h>

using namespace std;

int main(int argc, char** argv)
{
	if(argc < 5)
	{
		cout<<"us:"<<string(argv[0])<<" devId stringId value raise"<<endl;
		exit(0);
	}

	int devId = atoi(argv[1]);
	string strId(argv[2]);
	int value = atoi(argv[3]);
	bool raise = false;
	if(string(argv[4]) == string("true"))
	{
		raise = true;
	}

	try{
			
		//		Address a(IP("192.168.0.50"),4242);
		AlarmModuleAccess am("AlarmModuleClientConfig.xml");

		cout<<"AMA::session start"<<endl;
		am.startSession(devId);	//CS50

		AMAlarmId alm;
		alm.devId=devId;
		alm.type=AMAlarmId::Filter;
		alm.strId=strId;
		alm.intId=-1;

		AMAlarmValue av;
		av.value = value;
		av.alarmId = alm;
		av.raised = raise;

		cout<<"RecordingModuleInterface::raising system alarm:"<<alm.strId<<endl;
		am.startSession(-1);
		am.setAlarm(av);

	}catch(Exception &e)
	{
		cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
	}
}

