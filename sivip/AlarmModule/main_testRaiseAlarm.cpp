#include <version.h>
#include <Utils/debugNew.h>
#include <Plugins/Gestor.h>
#include <AlarmModule/AlarmModuleTypes.h>
#include <AlarmModule/AlarmModuleException.h>
#include <Sockets/Address.h>
#include <XML/xmlParser.h>
#include <Utils/StrUtils.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Http/HttpClient.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <iostream>

#include <signal.h>

using namespace std;

int main(int argc, char** arg)
{

	try{
			
		//		Address a(IP("192.168.0.50"),4242);
		AlarmModuleAccess am("GestorCanisConfig.xml");

		cout<<"ControlModuleAcces::session start"<<endl;
		am.startSession(6);	//CS50

		AMAlarmValue av;

		av.alarmId.devId=6;
		av.alarmId.type=AMAlarmId::Input;
		av.alarmId.intId=1;

		av.value=0;
		cout <<"---- Alarm 0"<<endl;
		am.setAlarm(av);
		sleep(2);

		av.value=0;
		cout <<"---- Alarm 0"<<endl;
		am.setAlarm(av);
		sleep(2);

		av.value=1;
		cout <<"---- Alarm 1"<<endl;
		am.setAlarm(av);
		sleep(2);

		av.value=0;
		cout <<"---- Alarm 0"<<endl;
		am.setAlarm(av);
		sleep(2);

		av.value=1;
		cout <<"---- Alarm 1"<<endl;
		am.setAlarm(av);
		sleep(2);

		av.value=1;
		cout <<"---- Alarm 1"<<endl;
		am.setAlarm(av);
		sleep(2);

		av.value=1;
		cout <<"---- Alarm 1"<<endl;
		am.setAlarm(av);
		sleep(2);

		av.value=0;
		cout <<"---- Alarm 0"<<endl;
		am.setAlarm(av);
		sleep(2);

	}catch(Exception &e)
	{
		cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
	}
}

