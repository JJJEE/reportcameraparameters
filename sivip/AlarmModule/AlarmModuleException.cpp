#include <AlarmModule/AlarmModuleException.h>
#include <Utils/debugStackTrace.h>

AlarmModuleException::AlarmModuleException(int code, string msg): Exception(code, msg)
{}

AlarmModuleException::AlarmModuleException(SerializedException &se): Exception(se)
{}

string AlarmModuleException::getClass()
{
	string c=string("AlarmModuleException");
	return c;
}


AMInvalidParamException::AMInvalidParamException(int code, string msg):AlarmModuleException(code,msg)
{}	

AMInvalidParamException::AMInvalidParamException(SerializedException &se):AlarmModuleException(se)
{}	

string AMInvalidParamException::getClass()
{
	string c=string("AMInvalidParamException");
	return c;
}


AMSessionAlreadyStablishedException::AMSessionAlreadyStablishedException(int code, string msg):AlarmModuleException(code,msg)
{}

AMSessionAlreadyStablishedException::AMSessionAlreadyStablishedException(SerializedException &se):AlarmModuleException(se)
{}

string AMSessionAlreadyStablishedException::getClass()
{
	string c=string("AMSessionAlreadyStablishedException");
	return c;
}


AMSessionNotStablishedException::AMSessionNotStablishedException(int code, string msg):AlarmModuleException(code,msg)
{}

AMSessionNotStablishedException::AMSessionNotStablishedException(SerializedException &se):AlarmModuleException(se)
{}

string AMSessionNotStablishedException::getClass()
{
	string c=string("AMSessionNotStablishedException");
	return c;
}

