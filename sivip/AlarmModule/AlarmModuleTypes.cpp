#include <AlarmModule/AlarmModuleTypes.h>
#include <iostream>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>

using namespace std;


AMDeviceID::AMDeviceID(int id): id(id)
{}

AMDeviceID::AMDeviceID()
{
	id=-1;
}

void AMDeviceID::toLocalEndian()
{
	Endian e;
	e.desendiana(&id, sizeof(int));
}

void AMDeviceID::toNetworkEndian()
{
	Endian e;
	e.endiana(&id, sizeof(int));
}


AMAlarmId::AMAlarmId()
{
	this->type=-1;
	this->intId=-1;
	this->strId="";
}

AMAlarmId::AMAlarmId(int type, int id)
{
	this->type=type;
	this->intId=id;
	this->strId="";
}

AMAlarmId::AMAlarmId(int type, string id)
{
	this->type=type;
	this->intId=-1;
	this->strId=id;
}

AMAlarmId::AMAlarmId(int devId, int type, int id)
{
	this->devId=devId;
	this->type=type;
	this->intId=id;
	this->strId="";
}

AMAlarmId::AMAlarmId(int devId, int type, string id)
{
	this->devId=devId;
	this->type=type;
	this->intId=-1;
	this->strId=id;
}

void AMAlarmId::toLocal(char* buf)
{
	Endian e;
	char *p=buf;

	devId= *((int*)p);
	e.desendiana(&devId, sizeof(int));
	p+=sizeof(int);

	type= *((int*)p);
	e.desendiana(&type, sizeof(int));
	p+=sizeof(int);

	intId= *((int*)p);
	e.desendiana(&intId, sizeof(int));
	p+=sizeof(int);

	int strSize= *((int*)p);
	e.desendiana(&strSize, sizeof(int));
	p+=sizeof(int);

	strId = string(p, strSize);	
}

char* AMAlarmId::toNetwork(char* buf)
{
	int sz=this->size();
	char *v;
	if(buf==NULL)
		v=new char[sz];
	else
		v=buf;
	char *p=v;
	memset(v, 0, sz);
	Endian e;

	*((int*)p) = devId;
	e.endiana(p, sizeof(int));
	p+=sizeof(int);

	*((int*)p) = type;
	e.endiana(p, sizeof(int));
	p+=sizeof(int);

	*((int*)p) = intId;
	e.endiana(p, sizeof(int));
	p+=sizeof(int);

	*((int*)p) = strId.length();
	e.endiana(p, sizeof(int));
	p+=sizeof(int);
	

	string str = strId;

	sprintf(p,"%s", str.c_str());
	return v;
}

int AMAlarmId::size()
{
	return strId.length()+4*sizeof(int);
}

bool AMAlarmId::operator==(AMAlarmId aid)
{
	return  (this->devId==aid.devId) && (this->type==aid.type) && (this->intId==aid.intId) && (this->strId==aid.strId);
}

/*bool AMAlarmId::operator<(AMAlarmId aid) const
{
	return  (this->devId<aid.devId) || 
		(this->devId==aid.devId && this->type<aid.type) ||
		(this->devId==aid.devId && this->type==aid.type && this->intId<aid.intId) ||
		(this->devId==aid.devId && this->type==aid.type && this->intId==aid.intId && this->strId<aid.strId);
}
*/
bool operator<(const AMAlarmId id, const AMAlarmId aid)
{
	return  (id.devId<aid.devId) || 
		(id.devId==aid.devId && id.type<aid.type) ||
		(id.devId==aid.devId && id.type==aid.type && id.intId<aid.intId) ||
		(id.devId==aid.devId && id.type==aid.type && id.intId==aid.intId && id.strId<aid.strId);
}

Alarm::Alarm()
{
	baseMethod=VALUE_DIFFERENT;
	intBaseValue=0;
	doubleBaseValue=0.0;
}

Alarm::Alarm(int method, int value)
{
	baseMethod=method;
	intBaseValue=value;
	doubleBaseValue=value;
}

Alarm::Alarm(int method, double value)
{
	baseMethod=method;
	intBaseValue=(int)value;
	doubleBaseValue=value;
}

void Alarm::toLocal(char* buf)
{
	Endian e;
	char *p=buf;

	baseMethod= *((int*)p);
	e.desendiana(&baseMethod, sizeof(int));
	p+=sizeof(int);

	intBaseValue= *((int*)p);
	e.desendiana(&intBaseValue, sizeof(int));
	p+=sizeof(int);

	doubleBaseValue= *((double*)p);
	e.desendiana(&doubleBaseValue, sizeof(double));
	//p+=sizeof(double);
}

char* Alarm::toNetwork(char* buf)
{
	int sz=this->size();
	char *v;
	if(buf==NULL)
		v=new char[sz];
	else
		v=buf;
	char *p=v;
	memset(v, 0, sz);
	Endian e;

	*((int*)p) = baseMethod;
	e.endiana(p, sizeof(int));
	p+=sizeof(int);

	*((int*)p) = intBaseValue;
	e.endiana(p, sizeof(int));
	p+=sizeof(int);

	*((double*)p) = doubleBaseValue;
	e.endiana(p, sizeof(double));
//	p+=sizeof(double);

	return v;
}

int Alarm::size()
{
	return 2*sizeof(int)+sizeof(double);
}

AMAlarmConfig::AMAlarmConfig()
{
}

void AMAlarmConfig::toLocal(char* buf)
{
	Endian e;
	char *p=buf;

	alarmId.toLocal(p);
	p+=alarmId.size();

	data.toLocal(p);
	p+=data.size();

	dword ip=*((dword*)p);
	p+=4;
	word port=*((word*)p);
	e.desendiana(&port, sizeof(word));
	addr=new Address(IP(ip), port);

}

char* AMAlarmConfig::toNetwork(char* buf)
{
	Endian e;
	int sz=this->size();
	char *v;
	if(buf==NULL)
		v=new char[sz];
	else
		v=buf;
	char *p=v;
	memset(v, 0, sz);

	alarmId.toNetwork(p);
	p+=alarmId.size();

	data.toNetwork(p);
	p+=data.size();

	*((dword*)p)=addr.getIP().toDWord();
	p+=4;
	*((word*)(p))=addr.getPort();
	e.endiana(p, sizeof(word));

	return v;
}

int AMAlarmConfig::size()
{
	return alarmId.size()+data.size()+6;
}

AMAlarmValue::AMAlarmValue() : value(0), raised(false)
{
	struct timeval now;
	gettimeofday(&now,NULL);
	this->timestampMillis=(now.tv_sec*1000 + now.tv_usec/1000);
}

void AMAlarmValue::toLocal(char* buf, uint32_t bufLen)
{
	Endian e;
	char *p=buf;

	alarmId.toLocal(p);
	p+=alarmId.size();

	value= *((double*)p);
	e.desendiana(&value, sizeof(double));
	p+=sizeof(double);
	char b = *p;
	if(b!=0)
		raised=true;
	else
		raised=false;
	p+=sizeof(char);

	if (bufLen==0 || bufLen > p-buf)
	{
		this->timestampMillis = *((int64_t*)p);
		Endian::from(Endian::xarxa, &this->timestampMillis, sizeof(int64_t));
		p+=sizeof(int64_t);
	}
}

char* AMAlarmValue::toNetwork(char* buf)
{
	int sz=this->size();
	char *v;
	if(buf==NULL)
		v=new char[sz];
	else
		v=buf;
	char *p=v;
	memset(v, 0, sz);
	Endian e;

	alarmId.toNetwork(p);
	p+=alarmId.size();

	*((double*)p) = value;
	e.endiana(p, sizeof(double));
	p+=sizeof(double);

	if(raised)
		*p = -1;
	else
		*p = 0;
	p+=sizeof(char);

	*((int64_t*)p) = this->timestampMillis;
	Endian::to(Endian::xarxa, p, sizeof(int64_t));
	p+=sizeof(int64_t);

	return v;
}

int AMAlarmValue::size()
{
	return alarmId.size() + sizeof(double) + sizeof(char) + sizeof(int64_t);
}

bool AMAlarmValue::operator==(AMAlarmValue av)
{
	return (this->alarmId == av.alarmId);
}

bool AMAlarmValue::operator==(AMAlarmId aid)
{
	return (this->alarmId == aid);
}

