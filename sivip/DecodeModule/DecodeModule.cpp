/*
 *  DecodeModule.cpp
 *  
 *
 *  Created by David Marí Larrosa on 17/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "DecodeModule.h"
#include <Utils/Exception.h>
#include <XML/xmlNode.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Sockets/SocketException.h>
#include <Sockets/SocketTimeoutException.h>
#include <Utils/ServiceException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/Timer.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


using namespace std;

DecodeModule::DecodeModule(Address camAddr, short camType, Canis *cn) : ModuleGestor(camAddr, camType, cn)
{
	STACKTRACE_INSTRUMENT();
	__class=string("DecodeModule");
	moduleInterface=(ModuleInterfaceGestor*)new DecodeModuleInterface(this->address, this->type, this->cn);
	
	dword set=this->serviceSocket->setRecvBuffer(512*1024);
	if (set!=512*1024)
	{
		cout << "WARNING: " << __FILE__ << ", line " << __LINE__
		<< ": set receive buffer to " << set << " bytes, "
		<< 512*1024 << " requested." << endl;
	}
	set=this->serviceSocket->setSendBuffer(512*1024);
	if (set!=512*1024)
	{
		cout << "WARNING: " << __FILE__ << ", line " << __LINE__
		<< ": set send buffer to " << set << " bytes, "
		<< 512*1024 << " requested." << endl;
	}
}

DecodeModule::DecodeModule(string xmlFile) : ModuleGestor(xmlFile)
{
	STACKTRACE_INSTRUMENT();
	__class=string("DecodeModule");
	moduleInterface=(ModuleInterfaceGestor*)new DecodeModuleInterface(this->address, this->type, this->cn);
	
	dword set=this->serviceSocket->setRecvBuffer(512*1024);
	if (set!=512*1024)
	{
		cout << "WARNING: " << __FILE__ << ", line " << __LINE__
		<< ": set receive buffer to " << set << " bytes, "
		<< 512*1024 << " requested." << endl;
	}
	set=this->serviceSocket->setSendBuffer(512*1024);
	if (set!=512*1024)
	{
		cout << "WARNING: " << __FILE__ << ", line " << __LINE__
		<< ": set send buffer to " << set << " bytes, "
		<< 512*1024 << " requested." << endl;
	}
}


