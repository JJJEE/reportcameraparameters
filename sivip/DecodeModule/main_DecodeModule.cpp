#include <Utils/Types.h>
#include <Plugins/PluginImage.h>
#include <Utils/Canis.h>
#include <DecodeModule.h>
#include <signal.h>
#include <Utils/debugStackTrace.h>
#include <Runtime/Runtime.h>
#include <SoftwareUpdate/client/CheckForSoftwareUpdatesThread.h>
#include <version.h>

#include <signal.h>

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMP();
	abort();
}

#endif

#ifndef WIN32
void sigpipe(int)
{
	cout << "SegPipe" << endl;
}
#endif

int main(int argc, char *argv[])
{
#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif
#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
#endif
#ifndef WIN32
	signal(SIGPIPE, sigpipe);
#endif

	STACKTRACE_INSTRUMENT();

	cout << siriusRelease << endl;

	Runtime::init(argc, argv);
	CheckForSoftwareUpdatesThread *softUpdTh=NULL;
	
	bool exc=false;

	do
	{
		exc=false;
		try
		{
#ifdef WIN32
			DecodeModule dm("\\\\.PSF\\Untitled\\aena\\devel\\src\\Projecte VC++\\Sirius\\debug\\DecodeModuleConfig.xml");
#else
			DecodeModule dm("DecodeModuleConfig.xml");
#endif

			try
			{
				if (softUpdTh!=NULL)
				{
					softUpdTh->stopChecking();
					delete softUpdTh;
					softUpdTh=NULL;
					
					sleep(1);
				}
				
				// que comprovi cada hora
				softUpdTh=new CheckForSoftwareUpdatesThread(string("DecodeModule"), dm.getCanis(), 3600);
				
				softUpdTh->start();
			}
			catch (Exception &e)
			{
				cout << "Exception ocurred in creating CheckForSoftwareUpdatesThread: " << e.getMsg() << endl;
			}
			
			dm.serve();
		}
		catch (Exception &e)
		{
			exc=true;
			cout << "Exception ocurred in DecodeModule main thread: " << e.getMsg() << endl;
		}
		if(exc)
			cout<<"----------- end decmod, no exception ->restart"<<endl;
	}while(exc);

	return 0;
}

