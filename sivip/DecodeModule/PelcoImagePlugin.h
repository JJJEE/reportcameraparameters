/*
 * PelcoImagePlugin.h
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_DECODEMODULE_PELCOIMAGEPLUGIN_H_
#define SIRIUS_DECODEMODULE_PELCOIMAGEPLUGIN_H_


#include <Utils/debugNew.h>
#include <Plugins/PluginImage.h>
#include <Sockets/Address.h>
#include <Endian/Endian.h>
#include <Utils/Types.h>
#include <Utils/RPC.h>
#include <Utils/Timer.h>
#include <Utils/RPCPacket.h>
#include <Utils/ServiceException.h>
#include <Utils/PtrPool.h>
#include <Utils/StrUtils.h>
#include <Utils/DBGateway.h>
#include <Http/HttpStream.h>
#include <Threads/Thread.h>
#include <Threads/Mutex.h>
#include <ModuleAccess/AlarmModuleAccess.h>
#include <map>
#ifdef WIN32
extern "C" {
#include <jpeglib.h>
}
#endif
using namespace std;

class PelcoImagePluginStreamThread : public Thread
{
public:
	struct imageThreadArgs
	{
		void *s;
		Address a;
		void *plugin;
		Timer *timeoutTimer;
	};

	virtual void* execute(int id, void *args) = 0;
};

class PelcoImagePluginGenericStreamThread : public PelcoImagePluginStreamThread
{
public:
	virtual void* execute(int id, void *args);
};

class PelcoImagePlugin;
class PelcoImagePluginAlarmRaiseThread: public Thread
{
	static const int ALARM_TIMEOUT=30;
	static const int ALARM_ABSTIMEOUT=3600;
	static const int LOG_ERROR=200;
	static const int LOG_LIMIT=10;

	PelcoImagePlugin *sip;
	AlarmModuleAccess *ama;
	//DBGateway *db;
	public:
		class device
		{
			public:
				AMAlarmId alm;
				int devId;
				bool error, raised, firstRun;
				string errorMsg;
				Timer timeout, absTimeout;
			
			device(int devId, bool error=false, string errorMsg=""):devId(devId), error(error), raised(false), errorMsg(errorMsg), firstRun(true)
			{
				this->alm.devId=-1;
				this->alm.type=AMAlarmId::Filter;
				this->alm.strId="Device."+StrUtils::decToString(devId);
				this->alm.intId=-1;
				absTimeout.start();
			}
		};
	protected:
		map<int, device*> devStatus;
		RWLock devLock;
	public:
		PelcoImagePluginAlarmRaiseThread(PelcoImagePlugin *sip);
		void* execute(int, void*);
		void setDevStatus(int devId, bool error, string errorMsg);
};

class PelcoImagePlugin : public PluginImage
{
	friend class PelcoImagePluginJPEGStreamThread;
	friend class PelcoImagePluginMPEG4StreamThread;
	friend class PelcoImagePluginGenericStreamThread;
	friend class PelcoImagePluginAlarmRaiseThread;
	
protected:
	static dword dbSocketProblemCounter;
	static PtrPool ptrPool;
	static RPC* getRPC(Address *a);

	// resolucions i modes de video etc
public:
	static const int NUM_VIDEOSTD=2;
	static const int NUM_RESOLUTIONS=9;
	static const int maxSocketProblemCount=2;
	static const double defaultFrameAgeMS;
	static const double snc_v704FrameAgeMS;
	static const int maxFramesWaitMS;
	
	enum Model {UNKNOWN=0L, IM10};
	enum {CODEC_JPEG=CODEC_FOURCC('j','p','e','g'), CODEC_MPEG4=CODEC_FOURCC('m','p','4','v'),
			CODEC_H264=CODEC_FOURCC('h','2','6','4')};
	enum {PAL=0L, NTSC};
	enum {RES_AUTO_763x480=0L, RES_FRAME_763x480, RES_FIELD_763x480, RES_AUTO_640x480, RES_FRAME_640x480, 
			RES_FIELD_640x480, RES_320x240, RES_160x120, RES_OPTION, RES_AUTO_736x544=0L, RES_FRAME_736x544,
			RES_FIELD_736x544};
		
	static const IPFrameInfo resolutions[NUM_VIDEOSTD][NUM_RESOLUTIONS];
	int currentTvMode, currentResolution;

public:
	struct receivedFrameInfo
	{
		Timer recvdTime;
		bool isKey;
		dword frameLen;
		void *frame;
		
		receivedFrameInfo() : recvdTime(), isKey(false), frameLen(0),
			frame(NULL)
		{
			recvdTime.start();
		}
		
		receivedFrameInfo(void *frm, dword frmLen, bool key) : recvdTime(),
			isKey(key), frameLen(frmLen), frame(frm)
		{
			recvdTime.start();
		}
		
		~receivedFrameInfo()
		{
			if (frame!=NULL)
			{
				delete [] (byte*)frame;
				frame=NULL;
				frameLen=0;
			}
		}
		
	};

	struct streamInfo
	{
		Exception *lastStreamException;
		Mutex lseMutex;

		bool stopThread;	
		IPDeviceInfo devInfo;
		Model model;
		string ip;
		int port;
		int channel;
		int stream;
		IPStreamingMode streamingMode;
		IPFrameInfo frameInfo;
		IPCodecInfo codec;
		Timer lastAccessToFB;		// Temporitzador des de l'ultim acces a l'stream de video, si es que s'esta fent streaming
		double streamTimeout;		// Timeout de cancel·lacio d'stream, si no hi ha accessos :)
		int threadId;				// Id de thread que fa l'streaming. Si es -1, es que no s'esta fent streaming :P
		Mutex *imageThreadLock;
		struct
		{
			word whole;
			word frac;
		} fps;
		
		//single frame (JPEG)
		byte *lastFrame;
		dword lastFrameLen;
		
		//frame stream (MPEG4)
		double minimumFrameAgeMS;
		list<receivedFrameInfo*> recvdFrames;
		
		string streamFrames;
		string frameStreamHeader;
		int nframes;
		unsigned int keyFrame; // numero de key frames rebuts

		bool isKey;
		bool stillDefaultFPS;
		bool fpsChanged;
				
		Mutex modifLock;

#ifdef WIN32
		struct jpeg_compress_struct *cinfo;
		struct jpeg_error_mgr *jerr;
		struct jpeg_destination_mgr *dst;
		JSAMPROW row;
#endif

		streamInfo() : lastStreamException(NULL), stopThread(true),
			threadId(-1), ip(string("")), 
			port(0), lastAccessToFB(25.0f), streamTimeout(60.0),	lastFrame(NULL),
			lastFrameLen(0), isKey(false), fpsChanged(false), stillDefaultFPS(true),
			minimumFrameAgeMS(PelcoImagePlugin::defaultFrameAgeMS), 
			 streamFrames(string("")), frameStreamHeader(string("")), nframes(0), keyFrame(0),
			channel(1), stream(0)
#ifdef WIN32
			,cinfo(NULL), jerr(NULL), dst(NULL)
#endif
		{
			fps.whole=25;
			fps.frac=0; 
		};
	};
protected:

	class sessionInfo
	{
		public:
			IPDeviceInfo devInfo;
			streamInfo *stream;
			unsigned int lastFrame; // ultim key frame rebut
			int frameIndex; //index dins els P frames
			
			Timer lastStreamedTimer;
			
			sessionInfo() : stream(NULL), lastFrame(0), frameIndex(0),
				lastStreamedTimer()
			{
				this->lastStreamedTimer.start();
				this->lastStreamedTimer.setStartSeconds(
					this->lastStreamedTimer.getStartSeconds() -
					PelcoImagePlugin::defaultFrameAgeMS);
			}

			sessionInfo(const sessionInfo &si)
			{
				this->stream=si.stream;
				this->lastFrame=si.lastFrame;
				this->frameIndex=si.frameIndex;
				this->devInfo=si.devInfo;
				this->lastStreamedTimer=si.lastStreamedTimer;
			}
	};

	Mutex sessionLock;
	map<string,sessionInfo*> sessions;
	
	Mutex streamsLock;
	map<IPDeviceID,streamInfo*> streams;
	
	string getBasicAuthStrForDevice(int devId);
		
	sessionInfo* getSessionForAddress(Address *a);
	streamInfo* getStreamInfoForDevice(IPDeviceID id);

	// Xixes internes de funcionament, temes de cerca de subsistemes, etc
	static const int centralDirectoryTypeId=-1;

	Address centralDirectoryAddr;

	string configFile;

	PelcoImagePluginGenericStreamThread genericStreamThread;
	
	PelcoImagePluginAlarmRaiseThread *arThread;
	AlarmModuleAccess *ama;
	
	void createStreamThread(streamInfo *s, Address *a);
public:
	static const int DEFAULT_FBSIZE=1;
/*
#ifdef WIN32
	void setFrame(PelcoImagePlugin::streamInfo* s, BITMAPINFO bmpInfo, char* bmp);
#endif
*/
	
	static int SIGUSR1TraceCameraId, SIGUSR2TraceCameraId;
	
public:
	PelcoImagePlugin(Address addr);
	PelcoImagePlugin(string file);

#ifndef WIN32
	// Atencio a SIGUSR
	static void registerSigUSR();
	static void sigUSRAttn(int sig);
#endif

	virtual IPDeviceInfo getDeviceInformation(IPDeviceInfo dev, Address *a);
	virtual void startSession(IPDeviceID id, Address *a);
	virtual void endSession(IPDeviceID id, Address *a);

	virtual IPCodecInfo getCodecInUse(Address *a);
	virtual IPCodecInfo getCodecInUse(Address *a, double msTimeout);
	virtual void setCodecInUse(IPCodecInfo codec, Address *a);

	virtual IPFrameBufferSize getFrameBufferSize(Address *a);
	virtual void setFrameBufferSize(IPFrameBufferSize size, Address *a);
	virtual float getFrameBufferPercentInUse(Address *a);
	virtual int getFrameBufferFramesInUse(Address *a);

	virtual IPStreamingMode getStreamingMode(Address *a);
	virtual void setStreamingMode(IPStreamingMode mode, Address *a);

	virtual IPFrame getCompressedNextFrame(Address *a);
	virtual IPFrame getDecompressedNextFrame(Address *a);

	virtual IPFramesPerSecond getFPS(Address *a);
	virtual void setFPS(IPFramesPerSecond fps, Address *a);
	
	string getConfigFileName();
};
#endif
