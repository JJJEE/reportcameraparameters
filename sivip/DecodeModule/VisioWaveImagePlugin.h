#ifdef WIN32

/*
 * VisioWaveImagePlugin.h
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_DECODEMODULE_VISIOWAVEIMAGEPLUGIN_H_
#define SIRIUS_DECODEMODULE_VISIOWAVEIMAGEPLUGIN_H_

#include <Utils/debugNew.h>
#include <Plugins/PluginImage.h>
#include <Sockets/Address.h>
#include <Endian/Endian.h>
#include <Utils/Types.h>
#include <Utils/RPC.h>
#include <Utils/Timer.h>
#include <Utils/RPCPacket.h>
#include <Utils/ServiceException.h>
#include <Utils/PtrPool.h>
#include <Http/HttpStream.h>
#include <Threads/Thread.h>
#include <Threads/Mutex.h>
#include <map>
#ifdef WIN32
#include <vfw.h>
#include <Resource.h>
#include <winuser.h>
#include <Windows.h>
#include <Utils/WindowsDefs.h>
#include <Utils/WindowsInclude.h>
#endif
using namespace std;
/*
class VisioWaveImagePluginStreamThread : public Thread
{
public:
	struct imageThreadArgs
	{
		void *s;
		Address a;
		void *plugin;
		Timer *timeoutTimer;
	};

	virtual void* execute(int id, void *args) = 0;
};
*/

class VisioWaveImagePluginJPEGStreamThread : /*public VisioWaveImagePluginStreamThread, */public Thread, public CDialog
{
	HWND hWndC;    //capture window
	CDC dcMem;//virtual memory
	HBITMAP m_hBmp; //handle to bitmap
	CRect m_rectFrame; //capture frame inside main window

	// Dialog Data
	//{{AFX_DATA(VisioWaveImagePluginJPEGStreamThread)
	enum { IDD = IDD_VISIOWAVE_DIALOG};// };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(VisioWaveImagePluginJPEGStreamThread)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL


public:
	struct imageThreadArgs
	{
		void *s;
		Address a;
		void *plugin;
		Timer *timeoutTimer;
	};

public:
	VisioWaveImagePluginJPEGStreamThread(CWnd* pParent = NULL);
	virtual void* execute(int id, void *args);

		// Generated message map functions

	//{{AFX_MSG(CCameraToolKitDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	//}}AFX_MSG
//	afx_msg void OnTimer(UINT nIDEvent);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}


class VisioWaveImagePlugin : public PluginImage, public CWinApp
{

	friend class VisioWaveImagePluginJPEGStreamThread;

protected:
	static PtrPool ptrPool;
	static RPC* getRPC(Address *a);

	// resolucions i modes de video etc
public:
	static const int NUM_VIDEOSTD=2;
	static const int NUM_RESOLUTIONS=9;
	
	enum Model{EVOLUTION_16, EVOLUTION_28, POWERPLUS, VISIOBOX, CCS};// Central Configuration Server -> ?
	enum {CODEC_JPEG=CODEC_FOURCC('j','p','e','g'), CODEC_MPEG4=CODEC_FOURCC('m','p','4','v'),
		CODEC_H264=CODEC_FOURCC('h','2','6','4')};
	enum {PAL=0L, NTSC};
	enum {RES_AUTO_763x480=0L, RES_FRAME_763x480, RES_FIELD_763x480, RES_AUTO_640x480, RES_FRAME_640x480, 
			RES_FIELD_640x480, RES_320x240, RES_160x120, RES_OPTION, RES_AUTO_736x544=0L, RES_FRAME_736x544,
			RES_FIELD_736x544};
		
	static const IPFrameInfo resolutions[NUM_VIDEOSTD][NUM_RESOLUTIONS];
	int currentTvMode, currentResolution;

protected:
	public:

	class serviceThread: public Thread
	{
			PluginImage *plg;
		public:
			serviceThread(PluginImage *plg):plg(plg){}
			void *execute(int it, void* args)
			{
				plg->serve();
				return NULL;
			}
	};

	serviceThread serve;

	struct streamInfo
	{
		bool stopThread;	
		IPDeviceInfo devInfo;
		Model model;
		string ip;
		int port;
		IPStreamingMode streamingMode;
		IPFrameInfo frameInfo;
		IPCodecInfo codec;
		Timer *lastAccessToFB;		// Temporitzador des de l'ultim acces a l'stream de video, si es que s'esta fent streaming
		double streamTimeout;		// Timeout de cancel·lacio d'stream, si no hi ha accessos :)
		int threadId;				// Id de thread que fa l'streaming. Si es 0, es que no s'esta fent streaming :P
		Mutex *imageThreadLock;
		struct
		{
			word whole;
			word frac;
		} fps;
		
		byte *lastFrame;
		dword lastFrameLen;
		bool isKey;
		bool stillDefaultFPS;
		bool fpsChanged;
				
		Mutex modifLock;
				
		streamInfo() : stopThread(false), threadId(-1), ip(string("")), 
			port(0), lastAccessToFB(NULL), streamTimeout(15.0),
			lastFrame(NULL), lastFrameLen(0), isKey(false), fpsChanged(false),
			stillDefaultFPS(true)
		{
			fps.whole=25;
			fps.frac=0; 
		};
	};

	struct sessionInfo
	{
		IPDeviceInfo devInfo;
		streamInfo *stream;
		
		sessionInfo() : stream(NULL) {};
	};

	Mutex sessionLock;
	map<string,sessionInfo> sessions;
	
	Mutex streamsLock;
	map<int,streamInfo*> streams;
	
	sessionInfo getSessionForAddress(Address *a);
	streamInfo* getStreamInfoForDevice(int id);

	// Xixes internes de funcionament, temes de cerca de subsistemes, etc
	static const int centralDirectoryTypeId=-1;

	Address centralDirectoryAddr;

	string configFile;

	VisioWaveImagePluginJPEGStreamThread jpegThread;
	
	void createStreamThread(streamInfo *s, Address *a);
public:
	static const int DEFAULT_FBSIZE=1;

public:
	VisioWaveImagePlugin(Address addr);
	VisioWaveImagePlugin(string file);

	virtual IPDeviceInfo getDeviceInformation(IPDeviceInfo dev, Address *a);
	virtual void startSession(IPDeviceID id, Address *a);
	virtual void endSession(IPDeviceID id, Address *a);

	virtual IPCodecInfo getCodecInUse(Address *a);
	virtual void setCodecInUse(IPCodecInfo codec, Address *a);

	virtual IPFrameBufferSize getFrameBufferSize(Address *a);
	virtual void setFrameBufferSize(IPFrameBufferSize size, Address *a);
	virtual float getFrameBufferPercentInUse(Address *a);
	virtual int getFrameBufferFramesInUse(Address *a);

	virtual IPStreamingMode getStreamingMode(Address *a);
	virtual void setStreamingMode(IPStreamingMode mode, Address *a);

	virtual IPFrame getCompressedNextFrame(Address *a);
	virtual IPFrame getDecompressedNextFrame(Address *a);

	virtual IPFramesPerSecond getFPS(Address *a);
	virtual void setFPS(IPFramesPerSecond fps, Address *a);
	
	string getConfigFileName();

		
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(VisioWaveImagePlugin)
	public:
	virtual BOOL InitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(VisioWaveImagePlugin)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()


};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.
#endif
#endif
