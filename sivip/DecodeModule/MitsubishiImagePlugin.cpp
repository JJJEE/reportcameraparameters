/*
 *  MitsubishiImagePlugin.cpp
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "MitsubishiImagePlugin.h"
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Utils/Canis.h>
#include <Utils/StrUtils.h>
#include <Plugins/GestorImageExceptions.h>
#include <Http/HttpClient.h>
#include <Http/HttpStream.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <ServiceFinder/ServiceFinder.h>
#include <Utils/FileException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <Exceptions/CameraMalfunctionException.h>
#include <Exceptions/CameraNotConnectableException.h>
#include <net/Sockets/SocketException.h>
#include <net/Sockets/SocketTimeoutException.h>
#include <exceptions/RTSPException.h>
#include <math.h>

#ifndef WIN32
#ifndef WIN32
#include <unistd.h>
#endif
#else

#include <Utils/WindowsDefs.h>
#include <Utils/WindowsInclude.h>
#include "sample/sample06/resource.h"
#endif

extern struct timeval operator + (struct timeval a, struct timeval b);

#pragma mark *** Pool
PtrPool MitsubishiImagePlugin::ptrPool;

RPC* MitsubishiImagePlugin::getRPC(Address *a)
{
	STACKTRACE_INSTRUMENT();

	RPC *rpc=(RPC*)ptrPool.get();
	
	if (rpc==NULL && a!=NULL)
	{
		rpc=new RPC(*a);
		if (rpc==NULL)
		{
			throw (Exception("FATAL: Not enough memory"));
		}
		ptrPool.add(rpc, true);
	}
	
	return rpc;
}

#pragma mark *** Privades
const IPFrameInfo MitsubishiImagePlugin::resolutions[MitsubishiImagePlugin::NUM_VIDEOSTD][MitsubishiImagePlugin::NUM_RESOLUTIONS]={
		{IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)},
		{IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)}};

MitsubishiImagePlugin::sessionInfo MitsubishiImagePlugin::getSessionForAddress(Address *a)
{
	STACKTRACE_INSTRUMENT();

	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
		throw (IPSessionNotStablishedException(0, string("Session not stablished with any device ")+
												   string("from client ")+a->toString()));
	
	return (*itSes).second;
}

MitsubishiImagePlugin::streamInfo* MitsubishiImagePlugin::getStreamInfoForDevice(int id)
{
	STACKTRACE_INSTRUMENT();

	map<IPDeviceID,streamInfo*>::iterator itStr=streams.find(id);
	
	if (itStr==streams.end())
		return NULL;
	
	return itStr->second;
}

#pragma mark *** Threads de recepció imatges

void MitsubishiImagePluginStreamThread::processRTPPacket(RTPPacket *pk,
	void *data)
{
	STACKTRACE_INSTRUMENT();

	MitsubishiImagePlugin::streamInfo *s=
		(MitsubishiImagePlugin::streamInfo*)data;

//	cout << "p"; cout.flush();
	
	// Proces real del paquet
	s->pproc->processPacket(pk);

	//if(s->pproc)
	//{
	//	cma.startSession(s.devInfo.id);
	//	cma.setMetaDataValue("alarm","true");
	//	cma.endSession(s.devInfo.id);
	//}

	// 20090724: Certament, modifiquem l'streamInfo, pero la part que modifiquem
	// i que cal protegir es nomes a dins de canStream	
//	s->modifLock.lock();

//	cout<<"new CMA:"<<configFile<<endl;

	// Mentre tinguem frames disponibles, cal anar actualitzant la info
	// Ens guardem els frames partits de la seguent manera:
	// a) B0 + el que sigui + 1 B6 
	// b) El que sigui + 1 B6
	while (s->pproc->isFrameAvailable())
	{
		MPEG4VOP *vop = s->pproc->getFrame();

		if (vop->getType()==MPEG4VOP::VOP_B0)
		{
			s->ppHasB0=true;
			s->ppCanStream=false;
			s->ppCleanupBuffer=false;
		}
		else if (vop->getType()==MPEG4VOP::VOP_B6)
		{
			s->ppHasB6=true;
		
			if (s->ppHasB0)
				s->ppCleanupBuffer=true;
			else		
				s->ppCleanupBuffer=false;
		
			s->ppCanStream=true;
		}

		dword dataLen=vop->getVOPSize();
		byte *data=(byte*)vop->getVOPData();

		// Llegim l'estat d'alarma del frame
		if (vop->getType()==MPEG4VOP::VOP_B2 && dataLen>=27)
		{
			// TODO 20090308: Comprovar si el 27 inclou o no el codi de VOb
			// (de moment assumim que si), ja que dataLen ho inclou
			byte alm=data[6];
			if (alm != 0)
				s->alarmStatus=true;		
			else
				s->alarmStatus=false;		

			// I notifiquem si toca.
			if(s->alarmStatus && (!s->alarmReported || (s->nframes%15==0)))
			{
				try
				{
					AMAlarmValue av;
					av.alarmId=s->aid;
					av.value=1;
					av.raised=true;
					cout<<" Movement alarm raised, setAlarm"<<endl;
					s->ama.setAlarm(av);
					s->alarmReported=true;
				}
				catch(Exception e)
				{
					cout<<" Movement alarm raised, setAlarm exc: "<<e.getClass()<<": "<<e.getMsg()<<endl;
				}
			}
			else if(s->alarmReported && !s->alarmStatus)
			{
				try
				{
					AMAlarmValue av;
					av.alarmId=s->aid;
					av.value=0;
					av.raised=false;
					cout<<" Movement alarm stopped, setAlarm"<<endl;
					s->ama.setAlarm(av);
					s->alarmReported=false;
				}catch(Exception e)
				{
					cout<<" Movement alarm stopped, setAlarm exc: "<<e.getClass()<<": "<<e.getMsg()<<endl;
				}
			}

		}
	
		s->ppKey=s->ppKey || s->ppHasB0; // vop->isKeyFrame();
		s->ppFrameBuf+=string((char*)data, dataLen);

		delete [] data;
		delete vop;
		
		if (s->ppCanStream)
		{
			dword vopSize=s->ppFrameBuf.length();
	
			dword sz=vopSize+sizeof(int)*3;
			byte *aux=new byte[sz];
			byte *w=aux;
	
			*((unsigned int*)w)=2;
			w+=sizeof(unsigned int);
	
			// Per l'extradata d'AVCodec
			*((unsigned int*)w)=0;
			w+=sizeof(unsigned int);
	
			*((unsigned int*)w)=vopSize;
			w+=sizeof(unsigned int);
			memmove(w, s->ppFrameBuf.c_str(), vopSize);
			w+=vopSize;
	
			// 20090724: En principi nomes cal protegir aquest tram
			s->modifLock.lock();

			if(s->ppCleanupBuffer)
			{
				s->isKey=s->ppKey;
				s->nframes=1;
				s->keyFrame++;
				s->frameStreamHeader=string((char*)&sz, sizeof(dword));
				s->streamFrames=string((char*)aux, sz);
			}
			else
			{
				s->isKey=s->ppKey;
				s->nframes++;
				s->frameStreamHeader+=string((char*)&sz, sizeof(dword));
				s->streamFrames+=string((char*)aux, sz);
			}

			// 20090724: En principi nomes cal protegir aquest tram
			s->modifLock.unlock();

	
			delete[] aux;
	
			// Acabem d'streamar, Reinicialitzem valors, encara que en realitat
			// no caldria
			s->ppFrameBuf=string("");
			s->ppKey=false;
			s->ppCanStream=false;
			s->ppCleanupBuffer=false;
			s->ppHasB6=false;
			s->ppHasB0=false;
		}			
	}


//		if (s->pproc->isFrameAvailable())
//		{
//			while (s->pproc->isFrameAvailable())
//			{
//				MPEG4VOP *vop = s->pproc->getFrame();
//		
//				if (vop->getType()==MPEG4VOP::VOP_B0)
//				{
//					// Si trobem un B0, reinicialitzem (es netejara el buffer)
//		
//					s->ppCleanupBuffer=true;
//					s->ppKey=false;
//					s->ppCanStream=s->ppHasB6;
//					s->ppHasB0=true;
//		
//		//			cout << "|";
//		//			cout.flush();
//				}
//				else if (vop->getType()==MPEG4VOP::VOP_B6 && s->ppHasB0)
//				{
//					s->ppHasB6=true;
//					s->ppCanStream=true;
//		//			cout << "s";
//		//			cout.flush();
//				}
//				else if (vop->getType()==MPEG4VOP::VOP_B6 && s->ppHasB6)
//				{
//					s->ppCanStream=true;
//		//			cout << "s";
//		//			cout.flush();
//				}
//				else if (vop->getType()==MPEG4VOP::VOP_B6)
//				{
//					s->ppHasB6=true;
//		//			cout << ".";
//		//			cout.flush();
//		//			cout << "MBIPProcPk: B6 set" << endl;
//				}
//				
//				dword dataLen=vop->getVOPSize();
//				char *data=(char*)vop->getVOPData();
//				
//				// TODO 20090308: Comprovar si el 27 inclou o no el codi de VOb
//				// (de moment assumim que si), ja que dataLen ho inclou
//				if (vop->getType()==MPEG4VOP::VOP_B2 && dataLen>=27)
//				{
//					byte alm=((byte*)data)[6];
//					if (alm != 0)
//						s->alarmStatus=true;		
//					else
//						s->alarmStatus=false;		
//				}
//			
//				if (s->ppCanStream && !s->ppHasB0)
//				{
//					tempKey = tempKey || s->ppHasB0; // vop->isKeyFrame();
//					tempFrameBuf+=string(data, dataLen);
//				}
//				else
//				{
//					s->ppKey=s->ppKey || s->ppHasB0; // vop->isKeyFrame();
//					s->ppFrameBuf+=string(data, dataLen);
//				}
//				delete [] data;
//		
//				delete vop;
//			}
//		
//			if (s->ppCanStream && s->ppHasB6)
//			{
//				s->ppHasB0=false;
//		//		cout << "S";
//		//		cout.flush();
//		//		cout << "MBIPProcPk: B6 send" << endl;
//				dword vopSize=s->ppFrameBuf.length();
//		
//				dword sz=vopSize+sizeof(int)*3;
//				byte *aux=new byte[sz];
//				byte *w=aux;
//		
//				*((unsigned int*)w)=2;
//				w+=sizeof(unsigned int);
//		
//				// Per l'extradata d'AVCodec
//				*((unsigned int*)w)=0;
//				w+=sizeof(unsigned int);
//		
//				*((unsigned int*)w)=vopSize;
//				w+=sizeof(unsigned int);
//				memmove(w, s->ppFrameBuf.c_str(), vopSize);
//				w+=vopSize;
//		
//				if(s->ppCleanupBuffer)
//				{
//					s->isKey=s->ppKey;
//					s->nframes=1;
//					s->keyFrame++;
//					s->frameStreamHeader=string((char*)&sz, sizeof(dword));
//					s->streamFrames=string((char*)aux, sz);
//				}
//				else
//				{
//					s->isKey=s->ppKey;
//					s->nframes++;
//					s->frameStreamHeader+=string((char*)&sz, sizeof(dword));
//					s->streamFrames+=string((char*)aux, sz);
//				}
//		
//				delete[] aux;
//		
//				// Acabem d'streamar, reinicialitzar
//		//		cout << "MBIPProcPk: B6 clean" << endl;
//				s->ppFrameBuf=tempFrameBuf;
//				s->ppKey=tempKey;
//				s->ppCleanupBuffer=false;
//		//		s->ppHasB6=true; // Tenim un B6, perque si no canStream no hagues pogut ser true
//				s->ppHasB6=!s->ppHasB0; // Nomes tenim un B6 si no tenim un B0
//				s->ppHasB0=false; // Segur que si hem streamat ja no tenim B0
//				// I canviem l'estat de canStream d'acord amb aixo.
//				s->ppCanStream=s->ppHasB6;
//				
//				if(s->alarmStatus && (!s->alarmReported || (s->nframes%15==0)))
//				{
//					try
//					{
//						AMAlarmValue av;
//						av.alarmId=s->aid;
//						av.value=1;
//						av.raised=true;
//						cout<<" Movement alarm raised, setAlarm"<<endl;
//						s->ama.setAlarm(av);
//						s->alarmReported=true;
//					}
//					catch(Exception e)
//					{
//						cout<<" Movement alarm raised, setAlarm exc: "<<e.getClass()<<": "<<e.getMsg()<<endl;
//					}
//				}
//				else if(s->alarmReported && !s->alarmStatus)
//				{
//					try
//					{
//						AMAlarmValue av;
//						av.alarmId=s->aid;
//						av.value=0;
//						av.raised=false;
//						cout<<" Movement alarm stopped, setAlarm"<<endl;
//						s->ama.setAlarm(av);
//						s->alarmReported=false;
//					}catch(Exception e)
//					{
//						cout<<" Movement alarm stopped, setAlarm exc: "<<e.getClass()<<": "<<e.getMsg()<<endl;
//					}
//				}
//			}
//		}

	// 20090724: Certament, modifiquem l'streamInfo, pero la part que modifiquem
	// i que cal protegir es nomes a dins de canStream	
//	s->modifLock.unlock();
}


MitsubishiImagePluginStreamThread::MitsubishiImagePluginStreamThread() 
{
	STACKTRACE_INSTRUMENT();

}


void* MitsubishiImagePluginStreamThread::execute(int id, void *args)
{
	STACKTRACE_INSTRUMENT();

	MitsubishiImagePluginStreamThread::imageThreadArgs *ita=(MitsubishiImagePluginStreamThread::imageThreadArgs *)args;
	this->currentArgs=ita;
	
	MitsubishiImagePlugin *plugin=(MitsubishiImagePlugin *)ita->plugin;

	MitsubishiImagePlugin::streamInfo *s=(MitsubishiImagePlugin::streamInfo*)ita->s;

	cout<<"MitsubishiImagePluginStreamThread::execute"<<endl;
	string puText("");
	bool popupText=false;
	bool popupTextSent=false;

	s->frameInfo.x = 640;
	s->frameInfo.y = 480;

	string uri=string("rtsp://")+s->ip+string(":")+StrUtils::decToString(s->port);
	
	ControlModuleAccess *controlModule=NULL;
	try
	{	
		int retries=3;
		//	cout << "SonyImagePluginJPEGStreamThread create CMA: " << plugin->getConfigFileName() << endl;

		CPConfigParamSeq res;
		
		while (retries>0)
		{
			try
			{
				controlModule=new ControlModuleAccess(plugin->cn);
			//	cout << "SonyImagePluginJPEGStreamThread created CMA: " << plugin->getConfigFileName() << endl;
				CPDeviceID devId(s->devInfo.id.id);
				controlModule->startSession(devId);
				res=controlModule->getConfigParamRecursive(CPConfigParam("/Video/1/Resolution",""));
				delete controlModule;
				controlModule=NULL;
				
				if (!res.empty())
					break;

				cout << "MitsubishiIPStreamThread::execute(): get Resolution " << uri << " - empty - retry: " << retries << endl;
				retries--;
			}	
			catch (Exception &e)
			{
				if(controlModule != NULL)
				{
					delete controlModule;
					controlModule=NULL;
				}

				cout << "MitsubishiIPStreamThread::execute(): get Resolution - Exception: " << e.getClass() << ": " << e.getMsg() << endl;
	
				retries--;
			}
		}
		
		if (retries<=0)		
		{
			if (controlModule!=NULL)
			{
				delete controlModule;
				controlModule = NULL;
			}
			s->stopThread=true;
							
			s->modifLock.lock();
	
			if(s->lastAccessToFB!=NULL)
				delete s->lastAccessToFB;
			s->lastAccessToFB=NULL;
	
			s->modifLock.unlock();
	
			if (popupTextSent)
				popupTextSent=false;

			this->currentArgs=NULL;
			delete ita;
			
			return NULL;
		}
				
		s->frameInfo.x=0;
		s->frameInfo.y=0;
		
		while(!res.empty())
		{
			cout << "SonyImagePluginJPEGStreamThread::execute() " <<res.front().path<<" "<<res.front().value<<endl;
			if(res.front().path==string("/Video/1/Resolution/x"))
				s->frameInfo.x=atoi(res.front().value.c_str());
			else if(res.front().path==string("/Video/1/Resolution/y"))
				s->frameInfo.y=atoi(res.front().value.c_str());
	
			res.pop_front();
		}
	}	
	catch (Exception &e)
	{
		if (controlModule!=NULL)
			delete controlModule;
		cout << "MitsubishiIPStreamThread::execute(): get Resolution - Exception: " << e.getClass() << ": " << e.getMsg() << endl;
		s->frameInfo.x = 640;
		s->frameInfo.y = 480;
	}

	cout<<" url:"<<uri<<" res: "<<s->frameInfo.x<<"x"<<s->frameInfo.y<<endl;

	RTSPSession *rtsp=NULL;
	RTPSession *rtpSes=NULL;
	
	try
	{
		s->firstFrame=true;
		cout<<"MitsubishiImagePluginStreamThread::execute(): RTSP::connect() url:"<<uri<<endl;
		try
		{		
			rtsp=RTSP::connect(uri);
			Exception *lse=s->lastStreamException;
			s->lastStreamException=NULL;
			if (lse!=NULL)
				delete lse;
		}
		catch (Exception &se)	// Agafa SocketExc i SocketTimeoutExc (que hereda de SocketExc)
		{
			Exception *lse=s->lastStreamException;

			if (lse==NULL || lse->getClass()!=string("CameraMalfunctionException"))
			{
				s->lastStreamException = new CameraNotConnectableException("Timeout");
				
				if (lse!=NULL)
					delete lse;
			}
			
			s->stopThread=true;
			
			cout << "MitsubishiImagePluginStreamThread::execute(): Exception connecting to device (" << uri << "): "<< se.getClass() << ": " << se.getMsg() << endl;
			cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;
			s->modifLock.lock();
					
			if(s->lastAccessToFB!=NULL)
			{
				delete s->lastAccessToFB;
				s->lastAccessToFB=NULL;
			}
//			s->stopThread=false;
//			s->threadId=-1;
	
			s->modifLock.unlock();
	
			if (popupTextSent)
			{
				popupTextSent=false;
			}

			this->currentArgs=NULL;
			delete ita;
			
			return NULL;
		}
		
		cout<<"MitsubishiImagePluginStreamThread::execute(): Connected url: "<<uri<<endl;
		
		RTSPMediaDescription mediaDesc=rtsp->describeMedia();

		cout<<"MitsubishiImagePluginStreamThread::execute(): described url: "<<uri<<endl;

		list<RTSPMediaDescription::mediaDesc> videoStreams;
		list<RTSPMediaDescription::mediaDesc>::iterator mIt = 
			mediaDesc.streams.begin();
			
		for (; mIt != mediaDesc.streams.end(); mIt++)
		{
			if ((*mIt).desc.mediaType == string("video"))
			{
				videoStreams.push_back(*mIt);
				break;	// Nomes volem un stream de video ;)
			}
		}
		
		rtpSes=RTP::listen(IP(s->listenIP));
		
		try
		{		
			cout << "AAAAAAAAAAAAAAAAAAAAAA" << endl;
			rtsp->setup(rtpSes, videoStreams);
			cout << "BBBBBBBBBBBBBBBBBBBBBB" << endl;
			rtsp->play();
			Exception *lse=s->lastStreamException;
			s->lastStreamException=NULL;
			if (lse!=NULL)
				delete lse;
			cout << "CCCCCCCCCCCCCCCCCCCCCC" << endl;
		}
		catch (RTSPException &rtspex)
		{
			int code=rtspex.getCode();
			if (code!=0)
			{
				s->stopThread=false;
				Exception *lse=s->lastStreamException;

				switch (code/100)
				{
					case 2:
						s->stopThread=false;
						s->lastStreamException=NULL;
						break;
						
					case 5:
						s->stopThread=true;
						s->lastStreamException = new CameraMalfunctionException(rtspex.getMsg());
						break;
						
					default:
						s->stopThread=true;
						s->lastStreamException = new CameraNotConnectableException(rtspex.getMsg());
						break;
				}
				
				if (lse!=NULL)
					delete lse;
					
				if (s->stopThread)
				{
					cout << "MitsubishiImagePluginStreamThread::execute(): Exception (" << code << "): "<< rtspex.getClass() << ": " << rtspex.getMsg() << endl;
					cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;
					s->modifLock.lock();
							
					if(s->lastAccessToFB!=NULL)
					{
						delete s->lastAccessToFB;
						s->lastAccessToFB=NULL;
					}
//					s->stopThread=false;
//					s->threadId=-1;
			
					s->modifLock.unlock();
			
					if (popupTextSent)
					{
						popupTextSent=false;
					}

					cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTSP" << endl;
					delete rtsp;
					cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTP" << endl;
					delete rtpSes;
					
					this->currentArgs=NULL;
					delete ita;
					
					return NULL;
				}
			}
		}
		
		rtpSes->startRTPThread(
			MitsubishiImagePluginStreamThread::processRTPPacket, s);
		rtpSes->startRTCPSendThread();

		while (!s->stopThread)
		{
			sleep(5);
			TimerInstant ti=ita->timeoutTimer->time();
			if (ti.seconds() > s->streamTimeout)
			{
				cout << "MitsubishiImagePluginStreamThread::execute(): Thread Timeout occurred, stream is not being used!" << endl;
				cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;
				s->stopThread=true;
				break;
			}
			
            try
            {
				rtsp->options();
				Exception *lse=s->lastStreamException;
				s->lastStreamException=NULL;
				if (lse!=NULL)
					delete lse;
            }
			catch (RTSPException &rtspex)
			{
				int code=rtspex.getCode();
				if (code!=0)
				{
					s->stopThread=false;
					Exception *lse=s->lastStreamException;
	
					switch (code/100)
					{
						case 2:
							s->stopThread=false;
							s->lastStreamException=NULL;
							break;
							
						case 5:
							s->stopThread=true;
							s->lastStreamException = new CameraMalfunctionException(rtspex.getMsg());
							break;
							
						default:
							s->stopThread=true;
							s->lastStreamException = new CameraNotConnectableException(rtspex.getMsg());
							break;
					}
					
					if (lse!=NULL)
						delete lse;
						
					if (s->stopThread)
					{
						cout << "MitsubishiImagePluginStreamThread::execute(): Exception (" << code << "): "<< rtspex.getClass() << ": " << rtspex.getMsg() << endl;
						cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;
						s->modifLock.lock();
								
						if(s->lastAccessToFB!=NULL)
						{
							delete s->lastAccessToFB;
							s->lastAccessToFB=NULL;
						}
//						s->stopThread=false;
//						s->threadId=-1;
				
						s->modifLock.unlock();
				
						if (popupTextSent)
						{
							popupTextSent=false;
						}

						cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTSP" << endl;
						delete rtsp;
						cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTP" << endl;
						delete rtpSes;
						
						this->currentArgs=NULL;
						delete ita;
						return NULL;
					}
				}
			}
			catch (SocketException &sex)
			{
				Exception *lse=s->lastStreamException;
				
				if (lse==NULL || lse->getClass()!=string("CameraMalfunctionException"))
					s->lastStreamException =
						new CameraNotConnectableException("Timeout");
				else
					lse=NULL;
					
				s->lastStreamException = new CameraNotConnectableException("Timeout (options)");
				if (lse!=NULL)
					delete lse;

				s->stopThread=true;

				cout << "MitsubishiImagePluginStreamThread::execute(): Exception: "<< sex.getClass() << ": " << sex.getMsg() << endl;
				cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;
				s->modifLock.lock();
						
				if(s->lastAccessToFB!=NULL)
				{
					delete s->lastAccessToFB;
					s->lastAccessToFB=NULL;
				}
//				s->stopThread=false;
//				s->threadId=-1;
		
				s->modifLock.unlock();
		
				if (popupTextSent)
				{
					popupTextSent=false;
				}

				cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTSP" << endl;
				delete rtsp;
				cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTP" << endl;
				delete rtpSes;
				
				this->currentArgs=NULL;
				delete ita;
				return NULL;
			}
            catch (Exception &e)
            {
                cout << e.getClass() << ": " << e.getMsg() << endl;
            }
   		}

		cout << "MitsubishiImagePluginStreamThread::execute(): Ending thread..." << endl;
		try
		{
			rtsp->teardown();
		}
		catch (Exception &e)
		{
			cout << e.getClass() << ": " << e.getMsg() << endl;
		}

		s->modifLock.lock();
				
		s->keyFrame=0;
		s->nframes=0;
		if(s->lastAccessToFB!=NULL)
		{
			delete s->lastAccessToFB;
			s->lastAccessToFB=NULL;
		}
//		s->stopThread=false;
//		s->threadId=-1;

		s->modifLock.unlock();

		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTSP" << endl;
		delete rtsp;
		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTP" << endl;
		delete rtpSes;
//		delete s;

		
	}
	catch (SocketException &sex)
	{
		// Simulem un timeout....
		Exception *lse=s->lastStreamException;
		
		if (lse==NULL || lse->getClass()!=string("CameraMalfunctionException"))
			s->lastStreamException =
				new CameraNotConnectableException("Timeout");
		else
			lse=NULL;
			
		if (lse!=NULL)
			delete lse;

		s->stopThread=true;

		cout << "MitsubishiImagePluginStreamThread::execute(): Exception: "<< sex.getClass() << ": " << sex.getMsg() << endl;
		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;
		s->modifLock.lock();
				
		if(s->lastAccessToFB!=NULL)
		{
			delete s->lastAccessToFB;
			s->lastAccessToFB=NULL;
		}
//		s->stopThread=false;
//		s->threadId=-1;

		s->modifLock.unlock();

		if (popupTextSent)
		{
			popupTextSent=false;
		}

		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTSP" << endl;
		delete rtsp;
		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTP" << endl;
		delete rtpSes;
		
		this->currentArgs=NULL;
		delete ita;
		return NULL;
	}
	catch (Exception &e)
	{
		// Simulem un timeout....
		cout << "MitsubishiImagePluginStreamThread::execute(): Exception: " << e.getClass() << ": " << e.getMsg() << endl;
			cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;

		s->stopThread=true;
						
		s->modifLock.lock();

		if(s->lastAccessToFB!=NULL)
		{
			delete s->lastAccessToFB;
			s->lastAccessToFB=NULL;
		}
//		s->stopThread=false;
//		s->threadId=-1;

		s->modifLock.unlock();

		if (popupTextSent)
		{
			popupTextSent=false;
		}
//			cout << "MitsubishiImagePluginStreamThread::execute() 6" << endl;
//			cout << "Stopping thread" << endl;
		this->currentArgs=NULL;
		delete ita;
//		delete controlModule;

		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTSP" << endl;
		delete rtsp;
		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTP" << endl;
		delete rtpSes;
		
		return NULL;
	}
	catch (std::exception &stde)
	{
		// Simulem un timeout....
		cout << "MitsubishiImagePluginStreamThread::execute(): std::exception: " << stde.what() << endl;
			cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;

		s->stopThread=true;
						
		s->modifLock.lock();
				
		if(s->lastAccessToFB!=NULL)
		{
			delete s->lastAccessToFB;
			s->lastAccessToFB=NULL;
		}
//		s->stopThread=false;
//		s->threadId=-1;

		s->modifLock.unlock();

		if (popupTextSent)
		{
			popupTextSent=false;
		}
//			cout << "MitsubishiImagePluginStreamThread::execute() 6" << endl;
//			cout << "Stopping thread" << endl;
		this->currentArgs=NULL;
		delete ita;
//		delete controlModule;
		
		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTSP" << endl;
		delete rtsp;
		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTP" << endl;
		delete rtpSes;
		
		return NULL;
	}
	catch (...)
	{
		// Simulem un timeout....
		cout << "MitsubishiImagePluginStreamThread::execute(): unknown exception" << endl;
			cout << "MitsubishiImagePluginStreamThread::execute(): Stopping thread..." << endl;

		s->stopThread=true;
						
		s->modifLock.lock();
				
		if(s->lastAccessToFB!=NULL)
		{
			delete s->lastAccessToFB;
			s->lastAccessToFB=NULL;
		}
//		s->stopThread=false;
//		s->threadId=-1;

		s->modifLock.unlock();

		if (popupTextSent)
		{
			popupTextSent=false;
		}
//			cout << "MitsubishiImagePluginStreamThread::execute() 6" << endl;
//			cout << "Stopping thread" << endl;
		this->currentArgs=NULL;
		delete ita;
//		delete controlModule;
		
		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTSP" << endl;
		delete rtsp;
		cout << "MitsubishiImagePluginStreamThread::execute(): Stopping RTP" << endl;
		delete rtpSes;
		
		return NULL;
	}

//	cout << "MitsubishiImagePluginStreamThread::execute() 9" << endl;
	cout << "MitsubishiImagePluginStreamThread finishing by normal stopThread... mi:"<<(void*)(s->modifLock.mi) << endl;
//	cout << "MitsubishiImagePluginStreamThread finishing... m:"<<(void*)s->modifLock.mi->m << endl;

	s->stopThread=true;

	this->currentArgs=NULL;
	cout<<"MitsubishiImagePluginStreamThread::execute delete ita:"<<(void*) ita<<endl;
//	delete ita;
	cout<<"MitsubishiImagePluginStreamThread::execute return"<<endl;
//	delete controlModule;
	return NULL;
}



/*
#endif
*/

#pragma mark *** Constructores

void MitsubishiImagePlugin::createStreamThread(MitsubishiImagePlugin::streamInfo *s, Address *a)
{
	STACKTRACE_INSTRUMENT();

//	cout << "---> MitsubishiImagePlugin::createStreamThread() thId: " << s->threadId << endl;
	if (!s->stopThread)
		return;

//	cout << "cST mL lock" << endl;
	s->modifLock.lock();
//	cout << "cST mL locked" << endl;

	if (s->imageThreadLock!=NULL)
	{
//		cout << "createStreamThread: delete s->imageThreadLock" << endl;
		delete s->imageThreadLock;
//		cout << "createStreamThread: /delete s->imageThreadLock" << endl;
	}

	s->imageThreadLock=new Mutex();
	s->listenIP = this->listenIP;
	s->lastAccessToFB=new Timer();
	
	s->modifLock.unlock();
	
	s->lastAccessToFB->start();
//	sessions[a->toString()]=*s;
	
	// Iniciem el thread amb una serie de params
	MitsubishiImagePluginStreamThread::imageThreadArgs *ita=new MitsubishiImagePluginStreamThread::imageThreadArgs();
	
	ita->s=s;
	ita->a=*a;
	ita->plugin=this;
	ita->timeoutTimer=s->lastAccessToFB;

	s->stopThread=false;
	if (s->threadId!=-1)
	{
		streamThread.join(s->threadId);
		s->threadId=-1;
	}
	s->threadId=streamThread.start(ita, Thread::Thread::OnExceptionExitThread);
//	streamThread.detach(s->threadId);
	
	cout << "MitsubishiImagePlugin::createStreamThread() thId: " << s->threadId << endl;

//	sessions[a->toString()]=*s;
}




MitsubishiImagePlugin::MitsubishiImagePlugin(Address addr) : PluginImage(addr, "ImagePlugin::Mitsubishi"),
	centralDirectoryAddr(Address(IP("0.0.0.0"), 0))
{
	STACKTRACE_INSTRUMENT();

	this->listenIP=this->address.getIP().toString();
}

MitsubishiImagePlugin::MitsubishiImagePlugin(string file) : configFile(file), PluginImage(file),
	centralDirectoryAddr(Address(IP("0.0.0.0"), 0)), streamThread()
{
	STACKTRACE_INSTRUMENT();

	this->listenIP=this->address.getIP().toString();
}

 

IPDeviceInfo MitsubishiImagePlugin::getDeviceInformation(IPDeviceInfo dev, Address *a)
{
	STACKTRACE_INSTRUMENT();

//	cout << "--> MitsubishiImagePlugin::getDeviceInformation()" << endl;
	string query=string("SELECT d.id, f.nombre, m.codigo FROM dispositivo d, fabricante f, modelo m WHERE d.id='")+
		StrUtils::decToString(dev.id.id)+
		string("' AND d.fabricante=m.fabricante AND d.modelo=m.id AND m.fabricante=f.id AND f.nombre='Mitsubishi'");
	RPCPacket bdQueryPk(this->address, 0, (byte*)query.c_str(), query.length(), this->type, -2);

//	cout << "bdQuery packet assembled" << endl;
	
	// Trobem la BD
	RPC *rpc=MitsubishiImagePlugin::getRPC(NULL);
	RPCPacket *result=NULL;

	try
	{
		if (rpc==NULL)
		{	
			RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn);
			rpc=MitsubishiImagePlugin::getRPC(bd->a);
			delete bd;
		}

//		cout << "Sending keepAlive to " << a->toString() << endl;
		RPC::keepAlive(a, this->type);
				
		result=rpc->call(bdQueryPk);
		MitsubishiImagePlugin::ptrPool.release(rpc);
	}
	catch(Exception &e)
	{
		if(rpc != NULL)
			MitsubishiImagePlugin::ptrPool.release(rpc);
		//e.serialize()->materializeAndThrow(true);
		throw;
	}
	
//	cout << "Sending keepAlive to " << a->toString() << endl;
	RPC::keepAlive(a, this->type);

//	cout << "RPC call done" << endl;

	string resXML((char*)result->getData(), (size_t)result->getSize());
	delete result;
	
//	cout << "Response: " << endl << resXML << endl;
	
	XML *xml=xmlParser::parse(resXML);

//	cout << "XML parsed" << endl;

	xmlNode *row=xml->getNode("/result/[0]");
	
	if (row==NULL)
	{
		cout <<  "Device with specified ID " << StrUtils::decToString(dev.id.id) << " does not exist or it is not an Mitsubishi device:"<<xml->toString()<<endl;
		delete xml;
		throw (IPInvalidParamException(0, string("Device with specified ID ") + StrUtils::decToString(dev.id.id) + string("does not exist or it is not an Mitsubishi device")));
	}

//	cout << "Creating IPDeviceInfo" << endl;
	
	// TODO afegir resolucions i codecs suportats
	IPDeviceInfo di(atoi(xml->getNode("/result/[0]/id")->getCdata().c_str()),
					xml->getNode("/result/[0]/nombre")->getCdata(),
					xml->getNode("/result/[0]/codigo")->getCdata(),
					IPFrameInfoSeq(), IPCodecInfoSeq());

	delete xml;

//	cout << "<-- MitsubishiImagePlugin::getDeviceInformation()" << endl;
	return di;
}

void MitsubishiImagePlugin::startSession(IPDeviceID id, Address *a)
{
	STACKTRACE_INSTRUMENT();

	// cout  << "--> MitsubishiImagePlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;

	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes!=sessions.end())
	{
		throw (IPSessionAlreadyStablishedException(0, string("Session already stablished for device ")+
													StrUtils::decToString(id.id)+string(" from client ")+
														a->toString()));
	}
	
	sessionInfo s;
	s.devInfo.id=id.id;

	// cout  << "local call" << endl;
	s.devInfo=this->getDeviceInformation(s.devInfo, a);
	
	if (s.devInfo.make!="Mitsubishi")
	{
		throw (IPInvalidParamException(0, "Device with specified ID is not a Mitsubishi device"));
	}

	

	streamInfo *sInfo=getStreamInfoForDevice(s.devInfo.id.id);
	
	if (sInfo==NULL)
	{
		sInfo=new streamInfo(configFile, this->cn);

		sInfo->devInfo=s.devInfo;
		sInfo->fps.whole=25;
		sInfo->fps.frac=0;
//		try{
//			sInfo->cma.startSession(s.devInfo.id.id);
//		}catch(Exception e){}
		try{
			sInfo->ama.startSession(AMDeviceID(s.devInfo.id.id));
		}catch(Exception e){}
		sInfo->aid.devId=s.devInfo.id.id;
		sInfo->aid.type=AMAlarmId::Filter;
		sInfo->aid.strId="MotionAlarm";
		sInfo->aid.intId=-1;
			
		// Model, a lo guarro q es lo q toca :P
		if (s.devInfo.model==string("NM-C130FD")) 
		{
			sInfo->model=NMC130FD;
			s.devInfo.model=string("NM-C130FD");
		}
		else if (s.devInfo.model==string("NM-C150SD"))
		{
			sInfo->model=NMC150SD;
			s.devInfo.model=string("NM-C150SD");
		}
		else if (s.devInfo.model==string("NM-C110"))
		{
			sInfo->model=NMC110;
			s.devInfo.model=string("NM-C110");
		}
		else
		{
			sInfo->model=NMC130FD;
			s.devInfo.model=string("NM-C130FD");
		}

		sInfo->imageThreadLock=new Mutex();
		if (sInfo->imageThreadLock==NULL)
		{
			throw (ImagePluginException(0, "Not enough memory to allocate image thread mutex"));
		}	
		
		// cout  << "Querying DB to find device " << endl;
		string query=string("SELECT host(d.ip) AS ip, d.puerto FROM dispositivo d, modelo m, fabricante f WHERE d.id='")+
			StrUtils::decToString(s.devInfo.id.id)+
			string("' AND d.modelo=m.id AND d.fabricante=m.fabricante AND m.fabricante=f.id AND f.nombre='Mitsubishi'");
		RPCPacket bdQueryPk(this->address, 0, (byte*)query.c_str(), query.length(), this->type, 
							ServiceFinder::dbGatewayTypeId);
		// Trobem la BD
		RPC *rpc=MitsubishiImagePlugin::getRPC(NULL);
		RPCPacket *result=NULL;

		try
		{
			if (rpc==NULL)
			{	
				RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn);
				rpc=MitsubishiImagePlugin::getRPC(bd->a);
				delete bd;
			}

			result=rpc->call(bdQueryPk);
			MitsubishiImagePlugin::ptrPool.release(rpc);
		}
		catch(Exception &e)
		{
			if(rpc != NULL)
				MitsubishiImagePlugin::ptrPool.release(rpc);
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
		
		string resXML((char*)result->getData(), (size_t)result->getSize());
		delete result;
		
		// cout  << "XML result " << endl << resXML << endl;
		
		XML *xml=xmlParser::parse(resXML);
	
		xmlNode *row=xml->getNode("/result/[0]");
		
		if (row==NULL)
		{
			delete xml;
	//		delete [] s.fb;
	//		delete [] s.fbLengths;
	//		delete [] s.fbKeyFrames;
			throw (IPInvalidParamException(0, "Device with specified ID does not exist"));
		}
		
	
		sInfo->ip=xml->getNode("/result/[0]/ip")->getCdata();
		sInfo->port=atoi(xml->getNode("/result/[0]/puerto")->getCdata().c_str());
	
		delete xml;

		cout<<"MitsubishiImagePlugin:: call "<<(void*)sInfo<<endl;

		XML *config=NULL;
		{
			cout<<"MitsubishiImagePlugin:: read file :"<<this->getConfigFileName()<<endl;
			FILE *f=fopen(this->getConfigFileName().c_str(),"rb");

			if (f==NULL)
				throw FileException(string("File ")+this->getConfigFileName()+string(" not found"));
			cout<<"MitsubishiImagePlugin:: len"<<endl;

			fseek(f,0,SEEK_END);
			int len=ftell(f);
			fseek(f,0,SEEK_SET);
			cout<<"MitsubishiImagePlugin:: len :"<<len<<endl;

			char *buf=new char[len+1];
			fread(buf,len,1,f);
			buf[len]='\0';
			fclose(f);
			cout<<"MitsubishiImagePlugin:: parse:"<<endl;

			config=xmlParser::parse(string(buf, len));
		}
	
		cout<<"MitsubishiImagePlugin:: data:"<<endl;
/*		for(int i=0;;i++)
		{
			string path=string("/[0]/Cameras/[")+StrUtils::decToString(i)+string("]");
				
			if(config->getNode(path)==NULL)
				throw (IPInvalidParamException(0, "Device with specified ID does not have cameras"));

			xmlNode *n=config->getNode(path+string("/id"));
			int camId=atoi(n->getCdata().c_str());
			cout<<"MitsubishiImagePlugin:: camera ID:"<<camId<<" startSession:"<<id.id<<endl;
			if(camId==id.id)
			{
				n=config->getNode(path+string("/recordingID"));
				sInfo->recording.id=atoi(n->getCdata().c_str());
				n=config->getNode(path+string("/start"));
				sInfo->recording.start=atol(n->getCdata().c_str());
				n=config->getNode(path+string("/length"));
				sInfo->recording.length=atoi(n->getCdata().c_str());
				break;
			}
		}
*/		sInfo->streamingMode=IPStreamingMode(IPStreamingMode::STREAM);
		sInfo->pproc=new MPEG4RTPPacketProcessor();


		cout<<"MitsubishiImagePlugin:: save:"<<endl;
		streamsLock.lock();
		streams[s.devInfo.id]=sInfo;
		streamsLock.unlock();
	}

	s.stream=sInfo;

	sessionLock.lock();
	sessions[a->toString()]=s;
	sessionLock.unlock();

/*	sInfo->codec=getCodecInUse(a);
	ControlModuleAccess cma(getConfigFileName());
	cma.startSession(s.devInfo.id);
		string cmCodec=cma.getConfigParam(string("/Video/1/Codec/Fourcc"));
		if(cmCodec.size()>=4)
		{
			const char *c=cmCodec.c_str();
			sInfo->codec.fourcc=CODEC_FOURCC(c[0],c[1],c[2],c[3]);
		}
//		else
//		{
//			sInfo->codec=getCodecInUse(a);
//		}
		cma.endSession(s.devInfo.id);
*/	
	// cout  << "<-- MitsubishiImagePlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;
}

void MitsubishiImagePlugin::endSession(IPDeviceID id, Address *a)
{
	STACKTRACE_INSTRUMENT();

	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
	{
		throw (IPSessionNotStablishedException(0, string("Session not stablished for device ")+
														StrUtils::decToString(id.id)+string(" from client ")+
														a->toString()));
	}

	sessions.erase(itSes);
}

IPCodecInfo MitsubishiImagePlugin::getCodecInUse(Address *a)
{
	STACKTRACE_INSTRUMENT();

	sessionInfo s=getSessionForAddress(a);
	
	return IPCodecInfo(CODEC_MPEG4, 0, 1536);
}

void MitsubishiImagePlugin::setCodecInUse(IPCodecInfo codec, Address *a)
{
	STACKTRACE_INSTRUMENT();

	sessionInfo s=getSessionForAddress(a);
	
//	throw ImagePluginException(0,"Error: unable to change codec in use");
}

IPFrameBufferSize MitsubishiImagePlugin::getFrameBufferSize(Address *a)
{
	STACKTRACE_INSTRUMENT();

	cout<<"GFBS"<<endl;
	sessionInfo s=getSessionForAddress(a);

	IPFrameBufferSize fbSize;
	fbSize.nFrames=1; //s.fbSize;


	cout<<"/GFBS"<<endl;
	return fbSize;
}

void MitsubishiImagePlugin::setFrameBufferSize(IPFrameBufferSize size, Address *a)
{
	STACKTRACE_INSTRUMENT();


}

float MitsubishiImagePlugin::getFrameBufferPercentInUse(Address *a)
{
	STACKTRACE_INSTRUMENT();

	return 0.0f;
}

int MitsubishiImagePlugin::getFrameBufferFramesInUse(Address *a)
{
	STACKTRACE_INSTRUMENT();

	return 0;
}

IPStreamingMode MitsubishiImagePlugin::getStreamingMode(Address *a)
{
	STACKTRACE_INSTRUMENT();

	sessionInfo s=getSessionForAddress(a);

	return s.stream->streamingMode;
}

void MitsubishiImagePlugin::setStreamingMode(IPStreamingMode mode, Address *a)
{
	STACKTRACE_INSTRUMENT();

	sessionInfo s=getSessionForAddress(a);
	
	//cout << "SetStreamingMode: " << mode << ", id: " << s.devInfo.id << endl;
	//if(s.stream->streamingMode!=STREAM)
	if(mode.mode!=IPStreamingMode::STREAM)
		throw (ImagePluginException(0, string("Invalid streaming mode: ")+StrUtils::decToString(mode.mode)));
	s.stream->modifLock.lock();
	s.stream->streamingMode=mode;
	s.stream->modifLock.unlock();
	
}

IPFrame MitsubishiImagePlugin::getCompressedNextFrame(Address *a)
{
	STACKTRACE_INSTRUMENT();

	sessionInfo s=getSessionForAddress(a);

	IPCodecInfo codecInfo;
	codecInfo.fourcc= CODEC_MPEG4;//CODEC_JPEG;//s.stream->codec; //getCodecInUse(a);
	codecInfo.quality=5;
//	cout << "MitsubishiImagePlugin::getCompressedNextFrame() ini" << endl;

			// Com que accedim al frame buffer, comptem l'acces
//			cout << "MitsubishiImagePlugin::getCompressedNextFrame()  Stream + "<< string((char*)&codecInfo.fourcc, sizeof(codecInfo.fourcc)) << endl;
			s.stream->modifLock.lock();
			if (s.stream->lastAccessToFB!=NULL)		// Thread en marxa
			{
				try{
					s.stream->lastAccessToFB->start();
				}catch(...){}
			}
			s.stream->modifLock.unlock();

			if (s.stream->stopThread)		// El thread s'ha parat
			{
//				s.fbUse=0;
				cout << "MitsubishiImagePlugin::getCompressedNextFrame()  Thread stopped -> CREATING" << endl;
				createStreamThread(s.stream, a);
				s.lastFrame=0;
				s.frameIndex=0;
			}

			if (s.stream->lastStreamException!=NULL)
			{
				if(s.stream->lastStreamException->getClass() == string("CameraMalfunctionException") || s.stream->lastStreamException->getClass() == string("CameraNotConnectableException"))
				{

				}

				s.stream->lastStreamException->serialize()->materializeAndThrow();
			}

//			if (codecInfo.fourcc == CODEC_JPEG)
			{
//				cout << "MitsubishiImagePlugin::getCompressedNextFrame()  Stream JPEG " << endl;

//				while (s.stream->nframes==0)
				int totalWaited=0;
//				while(s.stream->nframes==0 || (s.stream->keyFrame==s.lastFrame && s.frameIndex==s.stream->nframes))
				while((s.stream->nframes==0 || (s.stream->keyFrame==s.lastFrame
					&& s.stream->nframes <= s.frameIndex)) && totalWaited<2000)
				{
					// Esperem "1/2" frame (@25fps)
					//				cout << " -> frame buffer empty" << endl;
					usleep(2000);
					totalWaited+=2;
					//				s=getSessionForAddress(a);
				}
//				while(/*s.stream->keyFrame<=10 &&*/ s.stream->nframes<2)
//				{
//				//	cout<<"primer frame, esperém..."<<endl;
//					usleep(2000);
//				//	cout<<"primer frame, fr:"<<s.stream->nframes<<endl;
//				// cout<<".";cout.flush();
//				}

				s.stream->firstFrame=false;

				s.stream->modifLock.lock();

				bool isKey=true;//s.stream->isKey; //fbKeyFrames[0];
				byte *buf;
				int len=0;;
				if(s.stream->keyFrame==s.lastFrame) 
				{	//ja havia rebut part del frame, enviem la resta
//					cout << " **** SonyImagePlugin::getCompressedNextFrame() s.stream->keyFrame==s.lastFrame partial frame : "<<s.stream->keyFrame<<"=="<<s.lastFrame <<" frames:"<<s.stream->nframes<<" part:"<<s.frameIndex<<endl;
//					cout << " gknt  sending:"<<s.stream->keyFrame <<" frames:"<<s.stream->nframes<<" part:"<<s.frameIndex<<endl;
					int begin=0;
					int dataLen=0;
					int *header=((int*)s.stream->frameStreamHeader.c_str());
					for(int i=0; i<s.stream->nframes; i++)
					{
						if(i<s.frameIndex)
							begin += *(header+i);
						else
							dataLen += *(header+i);
					}
					int sendFrames = (s.stream->nframes - s.frameIndex);
					len=dataLen+sizeof(int)+sendFrames*sizeof(int);
					buf=new byte[len];
					isKey=false;
					
					*((unsigned int*)buf) = sendFrames;
					memmove(buf+sizeof(int), 
							s.stream->frameStreamHeader.c_str() + s.frameIndex*sizeof(int),
							sendFrames*sizeof(int));
					memmove(buf+sizeof(int)+sendFrames*sizeof(int),
							s.stream->streamFrames.c_str() + begin,
							dataLen);

					s.frameIndex=s.stream->nframes;
				}
				else
				{	//cal enviar tot el frame
//					cout << "SonyImagePlugin::getCompressedNextFrame() s.stream->keyFrame!=s.lastFrame full      frame : "<<s.stream->keyFrame<<"!="<<s.lastFrame <<" frames:"<<s.stream->nframes<<" part:"<<s.frameIndex<<endl;
//					cout << " gcnt  sending:"<<s.stream->keyFrame <<" frames:"<<s.stream->nframes<<" full"<<endl;
					len=s.stream->frameStreamHeader.length()+s.stream->streamFrames.length()+sizeof(int);
					buf=new byte[len];
					isKey=true;
					
					*((unsigned int*)buf)=s.stream->nframes;
		//			cout<<"getFrame header:"<<StrUtils::hexDump(s.stream->frameStreamHeader)<<endl;
					memmove(buf+sizeof(int), s.stream->frameStreamHeader.c_str(), s.stream->frameStreamHeader.length());
					memmove(buf+sizeof(int)+s.stream->frameStreamHeader.length(), s.stream->streamFrames.c_str(), s.stream->streamFrames.length());

					s.lastFrame=s.stream->keyFrame;
					s.frameIndex=s.stream->nframes;
				}
//*****//

//			cout << "Frame read from buffer 0, size " << len << " bytes" << endl;

				{ // single client :P
//					s.stream->nframes=0;
//					s.stream->frameStreamHeader=string("");
//					s.stream->streamFrames=string("");
				}

				sessions[a->toString()]=s;
//				sessions.find(a->toString())->second=s;;
//				sessionInfo s=getSessionForAddress(a);
//				cout<< "			-- gcnf message  size:"<< len<<endl;


				s.stream->modifLock.unlock();
//				cout<<"-- getCNF "<<len<<StrUtils::hexDump(string((char*)buf, len))<<endl;

//				cout << "IPFrameLen " << len << endl;
				return IPFrame(s.stream->frameInfo, codecInfo, isKey, len, buf);
			}
	throw (ImagePluginException(0, string("Invalid streaming mode: ")+StrUtils::decToString(s.stream->streamingMode.mode)));
}

IPFrame MitsubishiImagePlugin::getDecompressedNextFrame(Address *a)
{
	STACKTRACE_INSTRUMENT();

//	sessionInfo s=getSessionForAddress(a);

	throw(ImagePluginException(-1, "Not implemented"));
}

IPFramesPerSecond MitsubishiImagePlugin::getFPS(Address *a)
{
	STACKTRACE_INSTRUMENT();

	sessionInfo s=getSessionForAddress(a);

	s.stream->modifLock.lock();
	IPFramesPerSecond fps(s.stream->fps.whole, s.stream->fps.frac);
	s.stream->modifLock.unlock();
	
	return fps;
}

void MitsubishiImagePlugin::setFPS(IPFramesPerSecond fps, Address *a)
{
	STACKTRACE_INSTRUMENT();

	sessionInfo s=getSessionForAddress(a);

	s.stream->modifLock.lock();

//	cout << "setFPS: current fps: " << s.stream->fps.whole << ", requested: " << fps.whole << endl;

	// Canviem si estem augmentant els FPS o si es el primer canvi.
	if (s.stream->stillDefaultFPS || fps.whole>s.stream->fps.whole || (fps.whole==s.stream->fps.whole && fps.frac>s.stream->fps.frac))
	{
		
		//s.stream->fps.whole=fps.whole;
		//s.stream->fps.frac=fps.frac;
		if(fps.whole < 5 || (fps.whole == 5 && fps.frac==0))
		{
			s.stream->fps.whole=5;
			s.stream->fps.frac=0;
		}else if(fps.whole < 10 || (fps.whole == 10 && fps.frac==0))
		{
			s.stream->fps.whole=10;
			s.stream->fps.frac=0;
		}else if(fps.whole < 15 || (fps.whole == 15 && fps.frac==0))
		{
			s.stream->fps.whole=15;
			s.stream->fps.frac=0;
		}else if(fps.whole < 25 || (fps.whole == 25 && fps.frac==0))
		{
			s.stream->fps.whole=25;
			s.stream->fps.frac=0;
		}
		s.stream->stillDefaultFPS=false;
		s.stream->fpsChanged=true;
	}
	string command=string("http://")+ s.stream->ip+string("/admin/mpeg4.html?mpeg4_fps=")+StrUtils::decToString(s.stream->fps.whole);
	try{
		HttpClient cl(command);
		HttpStream *stream=cl.sendRequest(string("GET"));
		delete stream;
	}catch(Exception e){
		cout<<"command Exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
	}

	s.stream->modifLock.unlock();
}

string MitsubishiImagePlugin::getConfigFileName()
{
	STACKTRACE_INSTRUMENT();

	return configFile;
}

