/*
 * VisioWaveImagePlugin.h
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#pragma once

#include <Utils/debugNew.h>
#include <Plugins/PluginImage.h>
#include <Sockets/Address.h>
#include <Endian/Endian.h>
#include <Utils/Types.h>
#include <Utils/RPC.h>
#include <Utils/Timer.h>
#include <Utils/RPCPacket.h>
#include <Utils/ServiceException.h>
#include <Utils/PtrPool.h>
#include <Http/HttpStream.h>
#include <Threads/Thread.h>
#include <Threads/Mutex.h>
#include <map>
#ifdef WIN32
#include <ddraw.h>
#include <VWActivexComponent1.h>
#endif

using namespace std;

#ifdef WIN32

#define IDC_VWACTIVEXCOMPONENT1	1000
#define IDC_VWACTIVEXWND 1001
class VisioWaveImagePlugin;


/*
class VWActivexWnd: public CDialog//CFrameWnd
{

public:
	CVWActiveXComponent1 m_Ctrl;
	void DoDataExchange(CDataExchange* pDX);

	VWActivexWnd (CWnd *Parent);//();

	//{{AFX_MSG (CMyWindow)
//	afx_msg void OnClear ();
//	afx_msg void OnLButtonDown (UINT nFlags, CPoint point);
	//}}AFX_MSG
	void DDrawInit();

	DECLARE_MESSAGE_MAP()
};

*/


class VisioWaveImagePluginStreamThread : public Thread
{
public:
	struct imageThreadArgs
	{
		void *s;
		Address a;
		void *plugin;
		Timer *timeoutTimer;
	};

	virtual void* execute(int id, void *args) = 0;
};

class VisioWaveImagePluginJPEGStreamThread : public VisioWaveImagePluginStreamThread , public CDialog
{

//	CVWActiveXComponent1 m_Ctrl;
	VisioWaveImagePlugin *imgPlg;
	HWND capWnd;
	//VWActivexWnd Wnd;
	//LPDIRECTDRAW7 lpDD;
	//LPVOID lpDD;
	CDataExchange pDX;
	//void DDrawInit();
	//void VWCompInit();
	static void frameCallback(HWND hWnd, LPVIDEOHDR lpVideoHdr);
public:
	VisioWaveImagePluginJPEGStreamThread(VisioWaveImagePlugin *imgPlg);
	virtual void* execute(int id, void *args);
};
#endif
