/*
 *  DecodeModule.h
 *
 *  Created by David Marí Larrosa on 17/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 */

#ifndef SIRIUS_DECODEMODULE_DECODEMODULE_H_
#define SIRIUS_DECODEMODULE_DECODEMODULE_H_

#include <Utils/debugNew.h>
#include <Utils/RPCPacket.h>
#include <Utils/RPC.h>
#include <Utils/Canis.h>
#include <Threads/Thread.h>
#include <Sockets/Address.h>
#include <Sockets/SocketUDP.h>
#include <Module/ModuleGestor.h>
#include <DecodeModule/DecodeModuleInterface.h>


class DecodeModule : public ModuleGestor
{
public:
	DecodeModule(Address addr, short type, Canis *cn);
	DecodeModule(string file);
};

#endif
