/*
 *  DecodeModuleInterface.h
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_DECODEMODULE_DECODEMODULEINTERFACE_H_
#define SIRIUS_DECODEMODULE_DECODEMODULEINTERFACE_H_

#ifdef WIN32
#include <Utils/windowsDefs.h>
#endif

#ifdef LINUX
#include <time.h>
#include <sys/time.h>
#endif

#include <Utils/debugNew.h>
#include <Endian/Endian.h>
#include <Sockets/Address.h>
#include <Utils/RPCPacket.h>
#include <Plugins/GestorImage.h>
#include <Plugins/GestorImageTypes.h>
#include <ModuleInterface/ModuleInterfaceGestor.h>
#include <string>
#include <map>

using namespace std;


class DecodeModuleInterface: public ModuleInterfaceGestor
{
public:
	static const int serviceCount=15;

	static const int getDeviceInformationServiceId=0;
	static const int startSessionServiceId=1;
	static const int endSessionServiceId=2;
	static const int getCodecInUseServiceId=3;
	static const int setCodecInUseServiceId=4;
	static const int getFrameBufferSizeServiceId=5;
	static const int setFrameBufferSizeServiceId=6;
	static const int getFrameBufferPercentInUseServiceId=7;
	static const int getFrameBufferFramesInUseServiceId=8;
	static const int getStreamingModeServiceId=9;
	static const int setStreamingModeServiceId=10;
	static const int getCompressedNextFrameServiceId=11;
	static const int getDecompressedNextFrameServiceId=12;
	static const int getFPSServiceId=13;
	static const int setFPSServiceId=14;
	
public:
	DecodeModuleInterface(Address decodeModuleAddress, short decodeModuleType, Canis *cn=NULL);
	~DecodeModuleInterface();

	SerializedException* getSessionNotStablished();
	Gestor* createGestor(Address orig, int type, Canis *cn=NULL);

//	bool deleteGestor(int dels=5);

	static RPCPacket* getDeviceInformation(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* startSession(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* endSession(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* getCodecInUse(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* setCodecInUse(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* getFrameBufferSize(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* setFrameBufferSize(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* getFrameBufferPercentInUse(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* getFrameBufferFramesInUse(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* getStreamingMode(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* setStreamingMode(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* getCompressedNextFrame(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* getDecompressedNextFrame(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* getFPS(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	static RPCPacket* setFPS(DecodeModuleInterface *_this, GestorImage *gi, void *params);
	
};
#endif
