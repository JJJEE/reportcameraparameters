/*
 *  PelcoImagePlugin.cpp
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "PelcoImagePlugin.h"
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <Utils/Canis.h>
#include <Utils/StrUtils.h>
#include <Plugins/GestorImageExceptions.h>
#include <Http/HttpClient.h>
#include <Http/HttpStream.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <Utils/Base64.h>
#include <ServiceFinder/ServiceFinder.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <video/MPEG4ChunkProcessor.h>
#include <Exceptions/CameraMalfunctionException.h>
#include <Exceptions/CameraNotConnectableException.h>
#include <net/Sockets/SocketException.h>

#ifndef WIN32
#include <signal.h>
#include <unistd.h>
#else
extern "C" {
#include <jpeglib.h>
}
#include <Utils/WindowsDefs.h>
#include <Utils/WindowsInclude.h>
#include "sample/sample06/resource.h"
#endif

// Escollim si volem el nou thread generic o que

dword PelcoImagePlugin::dbSocketProblemCounter=0;
const double PelcoImagePlugin::defaultFrameAgeMS=0.3;
const double PelcoImagePlugin::snc_v704FrameAgeMS=1.0;
const int PelcoImagePlugin::maxFramesWaitMS=200;
int PelcoImagePlugin::SIGUSR1TraceCameraId=0;
int PelcoImagePlugin::SIGUSR2TraceCameraId=0;

#pragma mark *** Pool

PtrPool PelcoImagePlugin::ptrPool;

RPC* PelcoImagePlugin::getRPC(Address *a)
{
	STACKTRACE_INSTRUMENT();
	RPC *rpc=(RPC*)ptrPool.get();
	
	if (rpc==NULL && a!=NULL)
	{
		rpc=new RPC(*a);
		if (rpc==NULL)
		{
			throw (Exception("FATAL: Not enough memory"));
		}
		dword set=rpc->setRecvBuffer(512*1024);
		if (set!=512*1024)
		{
			cout << "WARNING: " << __FILE__ << ", line " << __LINE__
			<< ": set receive buffer to " << set << " bytes, "
			<< 512*1024 << " requested." << endl;
		}
		
		set=rpc->setSendBuffer(512*1024);
		if (set!=512*1024)
		{
			cout << "WARNING: " << __FILE__ << ", line " << __LINE__
			<< ": set send buffer to " << set << " bytes, "
			<< 512*1024 << " requested." << endl;
		}

		ptrPool.add(rpc, true);
	}
	
	return rpc;
}

#pragma mark *** Privades

const IPFrameInfo PelcoImagePlugin::resolutions[PelcoImagePlugin::NUM_VIDEOSTD][PelcoImagePlugin::NUM_RESOLUTIONS]={
		{IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)},
		{IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)}};


string PelcoImagePlugin::getBasicAuthStrForDevice(int devId)
{
	string str("");
	DeviceAuthentication::AuthInfo ai;
	
	if (this->devAuth!=NULL)
	{
		try
		{
			ai=this->devAuth->getAuthByCamId(devId);
		}
		catch (InvalidParameterException &ipe)
		{
		}

		if (ai.username.length()>0)
			str=ai.username + string(":") + ai.password;
		else
			str=string("admin:admin");
	}
	else
		str=string("admin:admin");
		
	if (str.length()>0)
		return string("Basic ")+base64encode(str);

	return string("");
}

PelcoImagePlugin::sessionInfo *PelcoImagePlugin::getSessionForAddress(Address *a)
{
	STACKTRACE_INSTRUMENT();
	sessionLock.lock();
	map<string,sessionInfo*>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
	{
		sessionLock.unlock();
		cout<<"PIP:: Session not stablished with any device from client "<<a->toString()<<endl;
		throw (IPSessionNotStablishedException(0, string("Session not stablished with any device ")+
												   string("from client ")+a->toString()));
	}
	
	PelcoImagePlugin::sessionInfo *ses=(*itSes).second;
	sessionLock.unlock();
	
	return ses;
}

PelcoImagePlugin::streamInfo* PelcoImagePlugin::getStreamInfoForDevice(IPDeviceID id)
{
	STACKTRACE_INSTRUMENT();
	streamsLock.lock();	
	map<IPDeviceID,streamInfo*>::iterator itStr=streams.find(id);
	
	if (itStr==streams.end())
	{
		streamsLock.unlock();	
		return NULL;
	}
	
	PelcoImagePlugin::streamInfo *stream = itStr->second;
	streamsLock.unlock();	
	
	return stream;
}

#pragma mark *** Threads de recepció imatges

#pragma mark *** StreamThread Independent
void *PelcoImagePluginGenericStreamThread::execute(int id, void *args)
{
	STACKTRACE_INSTRUMENT();
	PelcoImagePluginStreamThread::imageThreadArgs *ita=(PelcoImagePluginStreamThread::imageThreadArgs *)args;
	PelcoImagePlugin *plugin=(PelcoImagePlugin *)ita->plugin;
	PelcoImagePlugin::streamInfo* s=(PelcoImagePlugin::streamInfo*)ita->s;

	string codecName("");
	
	switch (s->codec.fourcc)
	{
		case PelcoImagePlugin::CODEC_JPEG:
			codecName=string("JPEG");
			break;
		case PelcoImagePlugin::CODEC_MPEG4:
			codecName=string("MPEG4");
			break;
			
		default:
			codecName=string("Unknown");
		case PelcoImagePlugin::CODEC_H264:
			codecName=string("H264");
			cout << codecName << "SteamThread: ini " << id <<" "<< s->devInfo.id.id << ", unsupported codec, stopping thread" << endl;
			s->stopThread=true;
			break;
	}	

	if (s->stopThread)
	{
		// ATENCIO: Desinicialitzacio simplificada perque acabem de
		// començar el thread

		// Ho deixem tot llest pq el proxim get(De)compressedNextFrame
		// pugui iniciar el thread
		s->modifLock.lock();

		// "reinicialitzem" :P
		s->lastFrame=NULL;
		s->lastFrameLen=0;
		s->keyFrame=0;
		s->nframes=0;
		s->frameStreamHeader=string("");
		s->streamFrames=string("");

		s->modifLock.unlock();

		if (ita!=NULL)
			delete ita;
		ita=NULL;
		
		return NULL;
	}


	cout<< codecName << "SteamThread: ini " << id <<" "<< s->devInfo.id.id <<endl;

	string puText("");
	bool popupText=false;
	bool popupTextSent=false;

	ControlModuleAccess *controlModule=NULL;
	int width=0, height=0;
	try
	{	
		controlModule=new ControlModuleAccess(plugin->cn);
		CPDeviceID devId(s->devInfo.id.id);
		controlModule->startSession(devId);
		CPConfigParamSeq res =
			controlModule->getConfigParamRecursive(
				CPConfigParam("/Video/1/Resolution",""));

		// TODO 20090627: llegir de l'stream M4V o del fitxer JPEG
		s->frameInfo.x=0;
		s->frameInfo.y=0;
		
		while(!res.empty())
		{
			if(res.front().path==string("/Video/1/Resolution/x"))
				s->frameInfo.x = atoi(res.front().value.c_str());
			else if(res.front().path==string("/Video/1/Resolution/y"))
				s->frameInfo.y = atoi(res.front().value.c_str());
	
			res.pop_front();
		}

	}
	catch (Exception &e)
	{
		cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin" << codecName << "StreamThread::execute(): Control Module Access  Exception: " << e.getClass() << ": " << e.getMsg() << endl;
		cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin" << codecName << "StreamThread::execute(): Stopping thread..." << endl;

		if(controlModule!=NULL)
		{
			delete controlModule;
			controlModule=NULL;
		}

		// Ho deixem tot llest pq el proxim get(De)compressedNextFrame
		// pugui iniciar el thread

		s->modifLock.lock();

		// "reinicialitzem" :P
		s->lastFrame=NULL;
		s->lastFrameLen=0;
		s->keyFrame=0;
		s->nframes=0;
		s->frameStreamHeader=string("");
		s->streamFrames=string("");
		
		// Eliminem tots els frames pendents
		list<PelcoImagePlugin::receivedFrameInfo*>::iterator lIt;
		for (lIt=s->recvdFrames.begin(); lIt!=s->recvdFrames.end(); lIt++)
		{
			PelcoImagePlugin::receivedFrameInfo *ri=*lIt;
			delete ri;
		}
		// Ens asegurem que queda neta :P
		s->recvdFrames.clear();

//		delete s->lastAccessToFB;
//		s->lastAccessToFB=NULL;
		s->stopThread=true;
//		s->threadId=-1;

		s->modifLock.unlock();

		if (ita!=NULL)
			delete ita;
		ita=NULL;

		return NULL;
	}

	// Si es MPEG4, no tornem a necessitar el controlModule
	if (s->codec.fourcc==PelcoImagePlugin::CODEC_MPEG4)
	{
		if(controlModule!=NULL)
		{
			delete controlModule;
			controlModule=NULL;
		}
	}
	
	s->modifLock.lock();
	string url("");
	try
	{
		url=string("http://")+s->ip+string(":")+StrUtils::decToString(s->port);
//		if (s->devInfo.model==string("SNT-V704") || s->devInfo.model==string("SNT-V704-L9EMB"))
//			url += string("/command/image.cgi"
//				"?speed=") + StrUtils::decToString(s->fps.whole)
//				+ string("&ChannelNO=") + StrUtils::decToString(s->stream+1);
//		else
//			url += string("/image"
//				"?speed=") + StrUtils::decToString(s->fps.whole);
		url+=string("/jpeg");
	}
	catch (...)
	{
		// No passara mai, pero per no deixar petat el thread...
		s->modifLock.unlock();
		
		throw;
	}
	
	// Si canvien els FPS, ja sera despres d'haver-los usat aqui :)
	s->fpsChanged=false;

	s->modifLock.unlock();

	cout<< "get stream:id:" << s->devInfo.id << 
		" - thr Id:"<< id << " - " << url << endl;

	cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "]: " << url << " " << __LINE__ << endl;
	HttpClient cli(url); 
	cli.setHeader("Authorization", plugin->getBasicAuthStrForDevice(s->devInfo.id.id));
	cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "]: " << url << " " << __LINE__ << " Auth: " << plugin->getBasicAuthStrForDevice(s->devInfo.id.id) << endl;
	HttpStream *stream=NULL;

	try
	{	
		cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "]: " << url << " " << __LINE__ << endl;
		stream=cli.sendRequest();
	//	stream->setDebug(true);
		cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "]: " << url << " " << __LINE__ << endl;
		string hdr=stream->getNextInformationChunk();
		cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "]: " << url << " " << __LINE__ << endl;
		
		int status=stream->getRequestStatus();
		if (status/100 != 2)
			throw CameraMalfunctionException(string("Camera "
				"returned no video stream (HTTP Status: ") +
				StrUtils::decToString(status) + string(")")); //TODO: mes abaix es cambiarà per una CNCE() :/
	
		s->lseMutex.lock();
		Exception *lse=s->lastStreamException;
		s->lastStreamException=NULL;
		s->lseMutex.unlock();
		if (lse!=NULL)
			delete lse;
	}
	catch (Exception &ex)
	{
		s->lseMutex.lock();
		Exception *lse=s->lastStreamException;
		string error;
		if (ex.getClass()==string("CameraMalfunctionException"))
		{
			s->lastStreamException = new CameraMalfunctionException(ex.getMsg());
			error="malfunction";
		}else
		{
			s->lastStreamException = new CameraNotConnectableException(ex.getMsg());
			error="not_connectable";
		}
		s->lseMutex.unlock();
		if (lse!=NULL)
			delete lse;
			
		cout<<" get stream exc:id:"<<s->devInfo.id<<" - thr Id:"<<id <<" - "<<url<<endl;

		// Aixequem l'alarma perque no connecta
		cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "](" << __LINE__ << "): Raising alarm: id:"<<s->devInfo.id<<" - thr Id:"<<id <<" - "<<url<<endl;
		plugin->arThread->setDevStatus(s->devInfo.id.id, true, error);
		s->stopThread=true;
	}

	// Si ha connectat be, abaixem l'alarma que podria haver-se pujat :P
	//if (stream!=NULL)
	//{
	//	cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "](" << __LINE__ << "): Lowering alarm: id:"<<s->devInfo.id<<" - thr Id:"<<id <<" - "<<url<<endl;
	//	plugin->arThread->setDevStatus(s->devInfo.id.id, false, "");
	//}
		
	MPEG4ChunkProcessor proc;

	string rec("");

	string frameBuf("");
	bool hasB0=false;
	bool hasB6=false;
	bool key=false;
	bool cleanupBuffer=false;
	bool canStream=false;
	bool readFPSFromStream=false;
	
	bool deinited=false;
	bool lowerAlarm=true;
	
	Timer receivedFPSTimer;
	Timer now;
	double currentReceivedFPS=(double)(s->fps.whole);
	word receivedFrameCounter=0xffff;
	receivedFPSTimer.start();
	double lastFrameSecs=receivedFPSTimer.getStartSeconds();
	double secsPerFrameOffset=0.0;
	unsigned long nFrames =0;
	byte *jpegNewFrame=NULL;
	
	Timer netTimer;
	netTimer.start();

	cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "]: Starting Thread: id:"<<s->devInfo.id<<" - thr Id:"<<id <<" - "<<url<<endl;
	while (!s->stopThread && !deinited)
	{
		// UPDATE HTTP SI HAN CANVIAT ELS FPS
		if (s->fpsChanged)
		{
		/*	url=string("http://")+s->ip+string(":")+StrUtils::decToString(s->port);
				
			s->modifLock.lock();
			try
			{	
				if (s->devInfo.model==string("SNT-V704") || s->devInfo.model==string("SNT-V704-L9EMB"))
					url+=string("/command/image.cgi?speed=")+
					StrUtils::decToString(s->fps.whole)+string("&ChannelNO=")+StrUtils::decToString(s->stream+1);
				else
					url+=string("/image?speed=")+
					StrUtils::decToString(s->fps.whole);

				cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "] FPS Change: " << url << __LINE__ << endl;
			}
			catch (...)
			{
				// No passara mai, pero per no deixar petat el thread...
				s->modifLock.unlock();
				
				throw;
			}
			
			s->modifLock.unlock();		

			if (stream!=NULL)
				delete stream;
			stream=NULL;

			cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "] FPS Change: " << url << " " << __LINE__ << endl;
			cli.setURL(url);
			cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "] FPS Change: " << url << " " << __LINE__ << endl;
			
			try
			{
				cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "] FPS Change: " << url << " " << __LINE__ << endl;
				stream=cli.sendRequest();
				cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "] FPS Change: " << url << " " << __LINE__ << endl;
				string hdr=stream->getNextInformationChunk();
				cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "] FPS Change: " << url << " " << __LINE__ << endl;

				int status=stream->getRequestStatus();
				if (status/100 != 2)
					throw CameraMalfunctionException(string("Could "
						"not reset FPS in camera (HTTP Status: ") +
						StrUtils::decToString(status) + string(")"));
				s->fpsChanged=false;
	
				s->lseMutex.lock();
				Exception *lse=s->lastStreamException;
				s->lastStreamException=NULL;
				s->lseMutex.unlock();
				if (lse!=NULL)
					delete lse;
			}
			catch (Exception &ex)
			{
				s->lseMutex.lock();
				Exception *lse=s->lastStreamException;
				string error;
				if (ex.getClass()==string("CameraMalfunctionException"))
				{
					s->lastStreamException = new CameraMalfunctionException(ex.getMsg());
					error="malfunction";
				}else
				{	
					s->lastStreamException = new CameraNotConnectableException(ex.getMsg());
					error="not_connectable";
				}
				s->lseMutex.unlock();
				if (lse!=NULL)
					delete lse;
				
				// BUGFIX 20100218: No podem llençar-la sense mes, perque no
				// es catcha!!
//				ex.serialize()->materializeAndThrow(true);
				s->stopThread=true;
				cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "](" << __LINE__ << "): Raising alarm: id:"<<s->devInfo.id<<" - thr Id:"<<id <<" - "<<url<<endl;
				plugin->arThread->setDevStatus(s->devInfo.id.id, true,error);
				break;
			}
			*/
		}
		s->imageThreadLock->lock();
		try
		{
				
			string frameHdr;
			string *frameHdrPtr=&frameHdr;

			try
			{
				netTimer.start();
				
				rec=stream->getNextInformationChunk(&frameHdrPtr);

				switch (s->codec.fourcc)
				{
					case PelcoImagePlugin::CODEC_MPEG4:
						proc.processChunk((void*)rec.c_str(), rec.length());
						break;
				}
	
				TimerInstant netTimerTI=netTimer.time();
//				cout <<"N: " << netTimerTI.useconds() << " (" << rec.length() << ") " << endl;	

				if(s->lastStreamException != NULL)
				{
					s->lseMutex.lock();
					Exception *lse=s->lastStreamException;
					s->lastStreamException=NULL;
					s->lseMutex.unlock();
					if (lse!=NULL)
						delete lse;
				}
			}
			catch (Exception &ex) 
			{
				s->lseMutex.lock();
				Exception *lse=s->lastStreamException;
				string error;
				if (ex.getClass()==string("CameraMalfunctionException"))
				{
					s->lastStreamException = new CameraMalfunctionException(ex.getMsg());
					error="malfunction";
				}else
				{
					s->lastStreamException = new CameraNotConnectableException(ex.getMsg());
					error="not_connectable";
				}
				s->lseMutex.unlock();
				if (lse!=NULL)
					delete lse;

				cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "](" << __LINE__ << "): Raising alarm: id:"<<s->devInfo.id<<" - thr Id:"<<id <<" - "<<url<<endl;
				plugin->arThread->setDevStatus(s->devInfo.id.id, true, error);
				//ex.serialize()->materializeAndThrow(true);
				throw;
			}

			//fins que no hem aconseguit conectar i rebre els primers frames no podém realment baixar les possibles alarmes
			if (lowerAlarm)
			{
			//	cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "](" << __LINE__ << "): Lowering alarm: id:"<<s->devInfo.id<<" - thr Id:"<<id <<" - "<<url<<endl;
				plugin->arThread->setDevStatus(s->devInfo.id.id, false, ""); //si ja ha esta baixada (respecte al PIP, no a l'AM), no fara realment res
				lowerAlarm=false;
			}

			string tempFrameBuf("");
			bool tempKey=false;
			jpegNewFrame=NULL;
			
			switch (s->codec.fourcc)
			{
				case PelcoImagePlugin::CODEC_JPEG:
				{
					
					jpegNewFrame=new byte[rec.length()];
					memmove(jpegNewFrame, rec.c_str(), rec.length());
					nFrames++;
					if(nFrames%25 == 0)
						lowerAlarm=true;
				}
				break;
					
				case PelcoImagePlugin::CODEC_MPEG4:
					while (proc.isFrameAvailable())
					{
						MPEG4VOP *vop = proc.getFrame();
						byte *data=(byte*)vop->getVOPData();
						dword dataLen=vop->getVOPSize();
						
						if (vop->getType()==MPEG4VOP::VOP_B6)
							receivedFrameCounter++;
		
						if (vop->getType()==MPEG4VOP::VOP_B2)
						{
							string srch("FrmRate: ");
							string vopStr((char*)data, dataLen);
							
							int pos=vopStr.find(srch);
							if (pos!=string::npos)
							{
								string sub=vopStr.substr(pos + srch.length());
								currentReceivedFPS = (double)atoi(sub.c_str());
								readFPSFromStream=true;
		//						if (s->devInfo.id.id==64)
		//							cout << "read fps: " << currentReceivedFPS << endl;
							}
						}
		
						if (vop->getType()==MPEG4VOP::VOP_B0) // MPEG4VOP::VOP_B6 && hasB0)
		//    			if (receivedFrameCounter >= s->fps.whole)
						{
							TimerInstant recvTI=receivedFPSTimer.time();
							receivedFPSTimer.start();
							double currSecs=receivedFPSTimer.getStartSeconds();
							receivedFPSTimer.setStartSeconds(lastFrameSecs);
							double lastSecsPerFrameOffset=secsPerFrameOffset;
							
							// Tenim un keyframe. Hem de suposar que ens arriba en el
							// moment que toca i resincronitzem, en funcio del 
							// desplaçament que portem
							double timeOff = lastFrameSecs - currSecs;
		
							double timeOffRatio = timeOff/s->minimumFrameAgeMS;
							double absTimeOffRatio = ABS(timeOff);
							lowerAlarm=true; 
		
							if (absTimeOffRatio > 0.5)
							{
								// Cal resincronitzar. En el sentit invers de l'offset,
								// i intentant arribar a sincronisme en 1s
								secsPerFrameOffset=-timeOff/currentReceivedFPS;
							}
							else if (absTimeOffRatio >= 0.2)
							{
								// Cal resincronitzar. En el sentit invers de l'offset,
								// i intentant arribar a sincronisme en 2s
								secsPerFrameOffset=-timeOff/(2.0*currentReceivedFPS);
							}
							else if (absTimeOffRatio < 0.2)
							{
								// Si estem en franja acceptable
								secsPerFrameOffset=0.0;
							}
							
							// Limitem l'offset a 1 segon per segon a 1FPS
							double fpsDivisor = currentReceivedFPS;
							if (fpsDivisor < 1.0)
								fpsDivisor = 1.0;
							if (ABS(secsPerFrameOffset) >= 1.0/fpsDivisor)
							{
								if (secsPerFrameOffset>0.0)
									secsPerFrameOffset=0.999/fpsDivisor;
								else
									secsPerFrameOffset=-0.999/fpsDivisor;
							}
								
							if ((secsPerFrameOffset!=0.0 ||
								lastSecsPerFrameOffset!=secsPerFrameOffset)
								&& (s->devInfo.id.id == 
										PelcoImagePlugin::SIGUSR1TraceCameraId
									|| s->devInfo.id.id == 
										PelcoImagePlugin::SIGUSR2TraceCameraId))
							{
								now.start();
								cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "][devId:" << s->devInfo.id << "]: "
									"Resyncing @ " << fixed << 
									now.getStartSeconds() << ". New offset per "
									"frame: " << fixed << secsPerFrameOffset << 
									", lastFrameSecs: " << fixed << 
									lastFrameSecs << endl;
							}
							
							// Fem la mitja de l'anterior i l'actual, i arrodonim
							// FIX 20091111: Pels codecs no ho fem, assumim sempre
							// Entregaran els XX FPS (solen fer-ho)
							if (!readFPSFromStream)//(&& s->model!=PelcoImagePlugin::SNT_V704 && s->model!=PelcoImagePlugin::SNT_V704_L9EMB)
							{
								double fps=((double)receivedFrameCounter)/recvTI.seconds();
								currentReceivedFPS+=fps;
								currentReceivedFPS/=2.0;
								currentReceivedFPS=round(currentReceivedFPS);
		
								/*if (s->model==PelcoImagePlugin::SNT_V704 || s->model==PelcoImagePlugin::SNT_V704_L9EMB)
								{
									cout << "\nAdapting SNT V704 [" << 
									s->devInfo.id << "] to " << currentReceivedFPS
										<< " FPS " << endl;
									cout.flush();
								}*/
							}
							
							receivedFrameCounter=0;
						}
					
						if (vop->getType()==MPEG4VOP::VOP_B0)
						{
							hasB0=true;
							canStream=false;
							cleanupBuffer=false;
						}
						else if (vop->getType()==MPEG4VOP::VOP_B6)
						{
							hasB6=true;
						
							if (hasB0)
								cleanupBuffer=true;
							else		
								cleanupBuffer=false;
						
							canStream=true;
						}
		
						key=key || hasB0; // vop->isKeyFrame();
						frameBuf+=string((char*)data, dataLen);
				
						delete [] data;
						delete vop;
		
						if (canStream)
						{
							dword vopSize=frameBuf.length();
					
							dword sz=vopSize+sizeof(int)*3;
							byte *aux=new byte[sz];
							byte *w=aux;
					
							*((unsigned int*)w)=2;
							w+=sizeof(unsigned int);
					
							// Per l'extradata d'AVCodec
							*((unsigned int*)w)=0;
							w+=sizeof(unsigned int);
					
							*((unsigned int*)w)=vopSize;
							w+=sizeof(unsigned int);
							memmove(w, frameBuf.c_str(), vopSize);
							w+=vopSize;
					
							PelcoImagePlugin::receivedFrameInfo *recvFr=
								new PelcoImagePlugin::receivedFrameInfo(aux, sz, key);
								
							// Moment de recepcio que ens hauria d'haver tocat,
							// independentment del real...
							double secsPerFrame=1.0/currentReceivedFPS;
							double start=receivedFPSTimer.getStartSeconds();
							double frameTime=start + ((secsPerFrame + secsPerFrameOffset) * (double)receivedFrameCounter);
							recvFr->recvdTime.setStartSeconds(frameTime);
							lastFrameSecs=frameTime;
							
							if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
							|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
							{
								now.start();
								cout << "[" << StrUtils::getDateString() << 
									"] PIPST[" << codecName << "][devId:" << 
									s->devInfo.id << "]: "
									"Tagging " << (key?"KEY ":"")
									<< "frame received @ " << fixed <<
									now.getStartSeconds() << " with [" << 
									fixed << frameTime << "]" << endl;
							}
							
							s->modifLock.lock();
							
							s->recvdFrames.push_back(recvFr);
		
							double chivatoSS=recvFr->recvdTime.getStartSeconds()-floor(recvFr->recvdTime.getStartSeconds());
		
		//					if (s->devInfo.id.id==64)
		//    					cout << "newFrame (" << sz << " bytes): " << chivatoSS << " (" << (secsPerFrame * (double)receivedFrameCounter) << "), " << s->recvdFrames.size() << " frames" << endl;
							
							list<PelcoImagePlugin::receivedFrameInfo*> copyList;
							list<PelcoImagePlugin::receivedFrameInfo*> clearList;	
		
							if (cleanupBuffer)
							{
								if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
								|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
								{
									now.start();
									cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "][devId:" << s->devInfo.id << "]: "
										"Cleaning up => recvdFrames: " <<
										s->recvdFrames.size() << endl;
								}
								
								// Deixem nomes dues tongades Key + diffs, comptant
								// el key q acabem d'afegir
								list<PelcoImagePlugin::receivedFrameInfo*>::reverse_iterator lIt;
								int keyCount=0;
								for (lIt=s->recvdFrames.rbegin();
									lIt!=s->recvdFrames.rend(); lIt++)
								{
									if (keyCount>=2)
										clearList.push_front(*lIt);
									else
										copyList.push_front(*lIt);
										
									if ((*lIt)->isKey)
										keyCount++;
								}
								
								s->recvdFrames=copyList;

								if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
								|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
								{
									now.start();
									cout << "[" << StrUtils::getDateString() << "] PIPST[" << codecName << "][devId:" << s->devInfo.id << "]: "
										"Cleaned up => recvdFrames: "
										<< s->recvdFrames.size() << ", copyList: "
										<< copyList.size() << ", clearList: "
										<< clearList.size() << endl;
								}
							}

							s->modifLock.unlock();
		
							copyList.clear();
						
							list<PelcoImagePlugin::receivedFrameInfo*>::iterator clIt;
							for (clIt=clearList.begin(); clIt!=clearList.end(); clIt++)
							{
								delete (*clIt);
							}
							clearList.clear();
							
							// *NO* fem el delete, perque ja es fara quan es destrueixi
							// receivedFrameInfo
							// delete[] aux;
					
							// Acabem d'streamar, Reinicialitzem valors, encara que en realitat
							// no caldria
							frameBuf=string("");
							key=false;
							canStream=false;
							cleanupBuffer=false;
							hasB6=false;
							hasB0=false;
						}			
					}
					break;
			}			

			plugin->sessionLock.lock();

			TimerInstant ti=ita->timeoutTimer->time();
			if (ti.seconds() > s->streamTimeout)
			{
				// Evitem l'aparicio d'N errors ara que encara no ho gestionem
				// TODO 20090618: ¿Que vol dir aquest comentari de dalt? XD
				cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin" << codecName << "StreamThread::execute(): Thread Timeout occurred, stream is not being used! (" << ti.seconds() << ", " << s->streamTimeout << ")" << endl;
				cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin" << codecName << "StreamThread::execute(): Stopping thread... devId:" << s->devInfo.id << " (" << url << ")" << endl;
				s->stopThread=true;
			}

			if (s->stopThread)
			{
				deinited=true;
				// Ho deixem tot llest pq el proxim get(De)compressedNextFrame
				// pugui iniciar el thread
				s->modifLock.lock();

				// "reinicialitzem" :P
				s->lastFrame=NULL;
				s->lastFrameLen=0;
				s->keyFrame=0;
				s->nframes=0;
				s->frameStreamHeader=string("");
				s->streamFrames=string("");

				// Eliminem tots els frames pendents
				list<PelcoImagePlugin::receivedFrameInfo*>::iterator lIt;
				for (lIt=s->recvdFrames.begin(); lIt!=s->recvdFrames.end(); lIt++)
				{
					PelcoImagePlugin::receivedFrameInfo *ri=*lIt;
					delete ri;
				}
				// Ens asegurem que queda neta :P
				s->recvdFrames.clear();
				s->modifLock.unlock();

				plugin->sessionLock.unlock();

		
				if (jpegNewFrame!=NULL)
				{
					delete [] jpegNewFrame;
					jpegNewFrame=NULL;
				}	

				if (popupTextSent)
				{
					popupTextSent=false;
					// Fem el guarro i sudem de la resposta ;)
					controlModule->ignoreAsyncResult();
				}

				if(controlModule!=NULL)
				{
					delete controlModule;
					controlModule=NULL;
				}


				if (ita!=NULL)
					delete ita;
				ita=NULL;
				
				if (stream!=NULL)
					delete stream;
				stream=NULL;

				s->imageThreadLock->unlock();
				return NULL;
			}

			if (s->codec.fourcc==PelcoImagePlugin::CODEC_JPEG)
			{
				dword len=rec.length();

				s->modifLock.lock();

				byte *f=s->lastFrame;
				s->isKey=true;		// Com que es JPEG, sempre es key.
				s->lastFrameLen=len;
				s->isKey=true;
				s->lastFrame=jpegNewFrame;
	
				s->modifLock.unlock();
	
				if (f!=NULL)
				{
					delete [] f;
				}

				if (popupTextSent)
				{
					popupTextSent=false;
					// Fem el guarro i sudem de la resposta ;)
					controlModule->ignoreAsyncResult();
				}
			}

			plugin->sessionLock.unlock();
		}
		catch (Exception &e)
		{
			cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin" << codecName << "StreamThread::execute(): Loop exception: " << e.getClass() << ": " << e.getMsg() << endl;

			s->stopThread=true;

			if (s->modifLock.isHeldByCurrentThread())
				s->modifLock.unlock();

			if (plugin->sessionLock.isHeldByCurrentThread())
				plugin->sessionLock.unlock();
		}
		catch (std::exception &stde)
		{
			cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin" << codecName << "StreamThread::execute(): Loop std::exception: " << stde.what() << endl;
				
			s->stopThread=true;
			
			if (s->modifLock.isHeldByCurrentThread())
				s->modifLock.unlock();

			if (plugin->sessionLock.isHeldByCurrentThread())
				plugin->sessionLock.unlock();
		}
		catch (...)
		{
			cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin" << codecName << "StreamThread::execute(): Unknown loop exception." << endl;
				
			s->stopThread=true;
			
			if (s->modifLock.isHeldByCurrentThread())
				s->modifLock.unlock();

			if (plugin->sessionLock.isHeldByCurrentThread())
				plugin->sessionLock.unlock();
		}
		s->imageThreadLock->unlock();
	}

			
	cout<<"ending stream:id:"<<s->devInfo.id<<" - thr Id:"<<id <<" - "<<url<<endl;

	// Ens assegurem que desinicialitzem dos cops :P
	if (!deinited)
	{
		deinited=true;
		// Ho deixem tot llest pq el proxim get(De)compressedNextFrame
		// pugui iniciar el thread
		s->modifLock.lock();
	
		// "reinicialitzem" :P
		s->lastFrame=NULL;
		s->lastFrameLen=0;
		s->keyFrame=0;
		s->nframes=0;
		s->frameStreamHeader=string("");
		s->streamFrames=string("");
	
		// Eliminem tots els frames pendents
		list<PelcoImagePlugin::receivedFrameInfo*>::iterator lIt;
		for (lIt=s->recvdFrames.begin(); lIt!=s->recvdFrames.end(); lIt++)
		{
			PelcoImagePlugin::receivedFrameInfo *ri=*lIt;
			delete ri;
		}
		// Ens asegurem que queda neta :P
		s->recvdFrames.clear();

//		delete s->lastAccessToFB;
//		s->lastAccessToFB=NULL;
		s->stopThread=true;
	
		s->modifLock.unlock();
	
		if (jpegNewFrame!=NULL)
		{
			delete [] jpegNewFrame;
			jpegNewFrame=NULL;
		}	

		if (popupTextSent)
		{
			popupTextSent=false;
			// Fem el guarro i sudem de la resposta ;)
			controlModule->ignoreAsyncResult();
		}

		if(controlModule!=NULL)
		{
			delete controlModule;
			controlModule=NULL;
		}


		if (ita!=NULL)
			delete ita;
		ita=NULL;
		if (stream!=NULL)
			delete stream;
		stream=NULL;
		
		if (s->imageThreadLock->isHeldByCurrentThread())
			s->imageThreadLock->unlock();
	}
	cout << "<-- PIPST[" << codecName << "]: " << url << " " << __LINE__ << endl;
	s->stopThread=true;
	return NULL;
}

#pragma mark *** Constructores

void PelcoImagePlugin::createStreamThread(PelcoImagePlugin::streamInfo *s, Address *a)
{
	STACKTRACE_INSTRUMENT();
	if (!s->stopThread) // threadId!=-1)
	{
		cout << "[" << StrUtils::getDateString() << "] PIP::createST: not starting, stopThread==false"<<endl;
		return;
	}

	// Abans de fer canvis a l'stream pel nou, etc ens assegurem que es para
	// el vell...
	if (s->threadId!=-1)
	{
		string codecName("Unknown");
		switch (s->codec.fourcc)
		{
			case CODEC_JPEG:
				codecName=string("JPEG");
				break;
			case CODEC_MPEG4:
				codecName=string("MPEG4");
				break;
			case CODEC_H264:
				codecName=string("H264");
				break;
		}
		cout << "[" << StrUtils::getDateString() << "] PIP::createGENERICST[" << codecName << "]: joining previous stream "<<s->devInfo.id.id<<endl;
		genericStreamThread.join(s->threadId);
		cout << "[" << StrUtils::getDateString() << "] PIP::createGENERICST[" << codecName << "]: joined "<<s->devInfo.id.id<<endl;
		s->threadId=-1;

	}

//	cout << "cST mL lock" << endl;
	s->modifLock.lock();
//	cout << "cST mL locked" << endl;

	if (s->imageThreadLock!=NULL)
	{
//		cout << "createStreamThread: delete s->imageThreadLock" << endl;
		delete s->imageThreadLock;
//		cout << "createStreamThread: /delete s->imageThreadLock" << endl;
	}
	
	s->imageThreadLock=new Mutex();

	// 20090811: Ja no es un punter :P
//	if(s->lastAccessToFB!= NULL)
//		delete s->lastAccessToFB;
//	s->lastAccessToFB=new Timer();
	
	s->modifLock.unlock();
	
	s->lastAccessToFB.start();
//	sessions[a->toString()]=*s;
	
	// Iniciem el thread amb una serie de params
	PelcoImagePluginStreamThread::imageThreadArgs *ita=new PelcoImagePluginStreamThread::imageThreadArgs();
	
	ita->s=s;
	ita->a=*a;
	ita->plugin=this;
	ita->timeoutTimer=&s->lastAccessToFB;


	// Si ja estava en excepcio, ja ho estava i no te perque haver deixat
	// d'estar-ho.
//	Exception *lse=s->lastStreamException;
//	s->lastStreamException=NULL;
//	if (lse!=NULL)
//		delete lse;

	
	s->stopThread=false;

	string codecName("Unknown");
	switch (s->codec.fourcc)
	{
		case CODEC_JPEG:
			codecName=string("JPEG");
			break;
		case CODEC_MPEG4:
			codecName=string("MPEG4");
			break;
		case CODEC_H264:
			codecName=string("H264");
			break;
	}

	cout << "[" << StrUtils::getDateString() << "] PIP::createGENERICST[" << codecName << "]: start:"<<s->devInfo.id.id<<endl;
	s->threadId=genericStreamThread.start(ita);


//	cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin::createStreamThread() thId: " << s->threadId << endl;

//	sessions[a->toString()]=*s;
}
 

PelcoImagePluginAlarmRaiseThread::PelcoImagePluginAlarmRaiseThread(PelcoImagePlugin *sip):sip(sip)
{
	STACKTRACE_INSTRUMENT();
	ama=new AlarmModuleAccess(sip->getAddress(), sip->getType(), sip->getCanis());
//	ama->startSession(AMDeviceID(-1));
/*	db = NULL;
	try
	{
		db = new DBGateway(sip->address, sip->type, sip->cn);
	}catch(Exception &e)
	{
		cout<<"PelcoImagePluginAlarmRaiseThread::PelcoImagePluginAlarmRaiseThread: Exception while creating DBGateway:"<< e.getClass()<<":"<<e.getMsg()<<endl;
	}
*/
}

void PelcoImagePluginAlarmRaiseThread::setDevStatus(int devId, bool error, string errorMsg)
{
	STACKTRACE_INSTRUMENT();
	devLock.rlock();
	map<int, device*>::iterator aIt = devStatus.find(devId);
	if(aIt == devStatus.end() || aIt->second->error != error)
	{
		devLock.unlock();
		devLock.wlock();
		aIt = devStatus.find(devId);
		if(aIt == devStatus.end())
		{
			cout << "[" << StrUtils::getDateString() << "] PIPART::setDevStatus:"<<devId<<" (new device) error:"<<error<<":"<<errorMsg<<endl;
			devStatus[devId] = new device(devId, error, errorMsg);
		}
		else
		{
			cout << "[" << StrUtils::getDateString() << "] PIPART::setDevStatus:"<<devId<<" error:"<<error<<":"<<errorMsg<<endl;
			aIt->second->error = error;
			if(error)
			{
				aIt->second->errorMsg = errorMsg;
			}
			aIt->second->absTimeout.start();
		}
	}else if(aIt != devStatus.end())//( && error)
	{
	//	devLock.unlock();
	//	devLock.wlock();  //no el necessitém, perqué no modifiquém el map, i l'altre lloc que toca l'absTimeout si que el pilla :P
		aIt = devStatus.find(devId);
		if(aIt != devStatus.end())
		{
			aIt->second->absTimeout.start();
		}
	}
	devLock.unlock();
}

void* PelcoImagePluginAlarmRaiseThread::execute(int, void* parm)
{
	STACKTRACE_INSTRUMENT();
	Timer loopTime;
	while(true)
	{
		loopTime.start();

		devLock.rlock();
		cout << "[" << StrUtils::getDateString() << "] PIPART::start alarm check loop:"<<devStatus.size()<<endl;
		list<device*> toRaise;
		for (map<int, device*>::iterator it=devStatus.begin() ; it != devStatus.end(); it++ )
		{
			device *d= it->second; 
			if(d->firstRun || d->error!= d->raised || (d->error && d->timeout.time().seconds() > ALARM_TIMEOUT))
			{
				cout<<"toRaise:"<<d->devId<<" error:"<<d->error<<", firstRun:"<<d->firstRun<<endl;
				toRaise.push_back(d);
			}
		}
		devLock.unlock();


		int ncalls=0;
		cout << "[" << StrUtils::getDateString() << "] PIPART::start raising "<<toRaise.size()<<" Alarms"<<endl;
		for (list<device*>::iterator it=toRaise.begin(); it!=toRaise.end(); it++ )
		{
			device *d = *it; 
			try
			{
				devLock.rlock();
				AMAlarmValue av;
				av.value = d->devId;
				av.alarmId = d->alm;
				string errorMsg = d->errorMsg;
				av.raised = d->error;
				d->timeout.start();
				devLock.unlock();

				cout << "[" << StrUtils::getDateString() << "] PIPART::execute, "<<(av.raised?string("raising:"):string("lowering:"))<<d->devId<<":"<<av.alarmId.strId<<endl;
				if(d->error)
				{
					av.alarmId.strId = string("ModuleAlarm.DecodeModule.")+av.alarmId.strId+string(":")+sip->getAddress().getIP().toString()+string("|")+StrUtils::decToString(sip->getAddress().getPort())+string(":")+errorMsg+string(".")+StrUtils::decToString(d->devId);
					ncalls++;
					ama->setAlarm(av);
				}
				else
				{
					cout<<"raise, !error"<<av.alarmId.strId<<endl;
			//		string msg = av.alarmId.strId;
			//		av.alarmId.strId = msg + string(":") + string("not_connectable");
					av.alarmId.strId = string("ModuleAlarm.DecodeModule.")+av.alarmId.strId+string(":")+sip->getAddress().getIP().toString()+string("|")+StrUtils::decToString(sip->getAddress().getPort())+string(":");
					ncalls++;
					ama->setAlarm(av);
			//		av.alarmId.strId = msg + string(":") + string("malfunction");
			//		ncalls++;
			//		ama->setAlarm(av);
				}
				d->raised = av.raised;
				d->firstRun = false;
			}catch(Exception &e)
			{
				cout<<e.getClass()<<":"<<e.getMsg()<<" while "<<(d->error?string("raising"):string("lowering"))<<" for:"<<d->devId<<endl;
			}
			if(ncalls%10 == 0)
			{
				usleep(10000);
			}
		}
		

		cout << "[" << StrUtils::getDateString() << "] PIPART::alarms raised"<<endl;

		devLock.wlock();
		cout << "[" << StrUtils::getDateString() << "] PIPART::start alarm timeout loop:"<<devStatus.size()<<endl;
		for (map<int, device*>::iterator it=devStatus.begin() ; it != devStatus.end();)
		{
			device *d = it->second; 
			if(d->absTimeout.time().seconds() > ALARM_ABSTIMEOUT)
			{
				cout << "[" << StrUtils::getDateString() << "] PIPART::removing timed out alarm:"<<d->devId<<" error:"<<d->error<<" raised:"<<d->raised<<endl;
				devStatus.erase(it++);
				delete d;
			}
			else
				it++;
		}
		devLock.unlock();

		TimerInstant ti=loopTime.time();
		if(ti.seconds()<10) 
		{
			sleep(10-ti.seconds());
		}
	}
}


PelcoImagePlugin::PelcoImagePlugin(Address addr) : PluginImage(addr, "ImagePlugin::Pelco"),
	centralDirectoryAddr(Address(IP("0.0.0.0"), 0))
{
	STACKTRACE_INSTRUMENT();
//	ama=new AlarmModuleAccess(this->address, this->type, this->cn);
//	ama->startSession(AMDeviceID(-1));
	arThread= new PelcoImagePluginAlarmRaiseThread(this);
	arThread->start();
}

PelcoImagePlugin::PelcoImagePlugin(string file) : configFile(file), PluginImage(file),
	centralDirectoryAddr(Address(IP("0.0.0.0"), 0))
{
	STACKTRACE_INSTRUMENT();
//	ama=new AlarmModuleAccess(this->address, this->type, this->cn);
//	ama->startSession(AMDeviceID(-1));
	arThread= new PelcoImagePluginAlarmRaiseThread(this);
	arThread->start();
}

#ifndef WIN32
void PelcoImagePlugin::registerSigUSR()
{
	STACKTRACE_INSTRUMENT();
	cout << "\t\tPreparing USR signal attention..." << endl;
	signal(SIGUSR1, PelcoImagePlugin::sigUSRAttn);
	signal(SIGUSR2, PelcoImagePlugin::sigUSRAttn);
}

void PelcoImagePlugin::sigUSRAttn(int sig)
{
	STACKTRACE_INSTRUMENT();
	char id[16];
	FILE *f=fopen("/tmp/pip.trace.id", "rb");
	if (f==NULL)
		return;
	fseek(f, 0, SEEK_END);
	long size=ftell(f);
	if (size==0 || size>10)
		return;
	memset(id, 0, sizeof(id));
	fseek(f, 0, SEEK_SET);
	fread(id, size, 1, f);
	int iid = atoi(id);
	
	if (sig==SIGUSR1)
	{
		cout << "\t\tSIGUSR1: Tracing id: " << iid << endl;
		PelcoImagePlugin::SIGUSR1TraceCameraId=iid;
	}
	else if (sig==SIGUSR2)
	{
		cout << "\t\tSIGUSR2: Tracing id: " << iid << endl;
		PelcoImagePlugin::SIGUSR2TraceCameraId=iid;
	}
	
	// Com que hem agafat les SIGUSR*, fem el proces de new i StackTrace si toca

	if (sig==SIGUSR1)
	{
		NEW_DUMPALLOCATEDMEMORYMAP();
	}
	else if (sig==SIGUSR2)
	{
		STACKTRACE_DUMPALL();
	}
}
#endif

IPDeviceInfo PelcoImagePlugin::getDeviceInformation(IPDeviceInfo dev, Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout << "--> PelcoImagePlugin::getDeviceInformation()" << endl;
	string query=string("SELECT d.id, f.nombre, m.codigo FROM dispositivo d, fabricante f, modelo m WHERE d.id='")+
		StrUtils::decToString(dev.id.id)+
		string("' AND d.fabricante=m.fabricante AND d.modelo=m.id AND m.fabricante=f.id AND f.nombre='Pelco'");
	RPCPacket bdQueryPk(this->address, 0, (byte*)query.c_str(), query.length(), this->type, -2);

	//cout << "bdQuery packet assembled" << endl;
	
	// Trobem la BD
	RPC *rpc=PelcoImagePlugin::getRPC(NULL);
	
	
	if (rpc==NULL)
	{	
		RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn, true);
		rpc=PelcoImagePlugin::getRPC(bd->a);
		delete bd;
	}

//	cout << "Sending keepAlive to " << a->toString() << endl;
	RPCPacket *result=NULL;
	int retryCount=2;
	while (retryCount>0)
	{
		try
		{
			result=rpc->call(bdQueryPk);
			PelcoImagePlugin::ptrPool.release(rpc);
			retryCount=0;
		}
		catch (SocketTimeoutException &ste)
		{
			try
			{
				RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn, true);
				rpc->setDefaultDestination(*bd->a);
				delete bd;
			}
			catch (Exception &e)
			{
			}
			
			PelcoImagePlugin::dbSocketProblemCounter++;
			if (PelcoImagePlugin::dbSocketProblemCounter >
				PelcoImagePlugin::maxSocketProblemCount)
			{
				PelcoImagePlugin::dbSocketProblemCounter=0;
				retryCount--;
			}
			else
			{
				retryCount=0;
				PelcoImagePlugin::ptrPool.release(rpc);
				throw ste;
			}
		}
		catch(Exception e)
		{
			PelcoImagePlugin::ptrPool.release(rpc);
			//e.serialize()->materializeAndThrow(true);
			throw;
		}
	}
	
	string resXML((char*)result->getData(), (size_t)result->getSize());
	delete result;
	
	//cout << "Response: " << endl << resXML << endl;
	
	XML *xml=xmlParser::parse(resXML);

//	cout << "XML parsed" << endl;

	xmlNode *row=xml->getNode("/result/[0]");
	
	if (row==NULL)
	{
		cout <<  "Device with specified ID " << StrUtils::decToString(dev.id.id) << " does not exist or it is not a Pelco device:"<<xml->toString()<<endl;
		delete xml;
		throw (IPInvalidParamException(0, string("Device with specified ID ") + StrUtils::decToString(dev.id.id) + string("does not exist or it is not a Pelco device")));
	}

//	cout << "Creating IPDeviceInfo" << endl;
	
	// TODO afegir resolucions i codecs suportats
	IPDeviceInfo di(atoi(xml->getNode("/result/[0]/id")->getCdata().c_str()),
					xml->getNode("/result/[0]/nombre")->getCdata(),
					xml->getNode("/result/[0]/codigo")->getCdata(),
					IPFrameInfoSeq(), IPCodecInfoSeq());

	delete xml;

	cout << "<-- PelcoImagePlugin::getDeviceInformation()" << endl;
	return di;
}

void PelcoImagePlugin::startSession(IPDeviceID id, Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout  << "--> PelcoImagePlugin::startSession(" << id << ", " << a->toString() << ")" << endl;

	sessionLock.lock();
	map<string,sessionInfo*>::iterator itSes=sessions.find(a->toString());
	
	if (itSes!=sessions.end())
	{
		IPDeviceID devid=itSes->second->devInfo.id;
		sessionLock.unlock();

		if(id.id != devid.id)
		{
			// tenia sessio amb un altre dispositiu -> tanquem i iniciem
			// la nova :P
			try{
				cout<<"PelcoImagePlugin::startSession() switching session ID from:"<<devid.id<<" to:"<<id.id<<endl;
				endSession(devid, a);
			}catch(Exception &e){}
		}
		else
			cout<<"PelcoImagePlugin::startSession: already started for:"<<id.id<<endl;
	}
	else
		sessionLock.unlock();

//	cout  << "\tPelcoImagePlugin::startSession checkpoint 0" << endl;
	
	sessionInfo* s=new sessionInfo();
	s->devInfo.id=id;

//	cout  << "\t--> local getDeviceInfo Call" << endl;
	s->devInfo=this->getDeviceInformation(s->devInfo, a);
//	cout  << "\t<-- local getDeviceInfo Call" << endl;
	
	if (s->devInfo.make!="Pelco")
	{
		throw (IPInvalidParamException(0, "Device with specified ID is not a Pelco device"));
	}

//	cout  << "\tPelcoImagePlugin::startSession checkpoint 1" << endl;

	streamInfo *sInfo=getStreamInfoForDevice(s->devInfo.id);
	
	if (sInfo==NULL)
	{
//		cout  << "\tPelcoImagePlugin::startSession checkpoint 2" << endl;
		sInfo=new streamInfo();

		sInfo->devInfo=s->devInfo;
		sInfo->fps.whole=25;
		sInfo->fps.frac=0;
		sInfo->streamingMode=IPStreamingMode(IPStreamingMode::STREAM);
	
			
		// Model, a lo guarro q es lo q toca :P
		if (s->devInfo.model=="im10") sInfo->model=IM10;
		else
		{
			s->devInfo.model="im10"; sInfo->model=IM10;
		}

		sInfo->imageThreadLock=new Mutex();
		if (sInfo->imageThreadLock==NULL)
		{
			throw (ImagePluginException(0, "Not enough memory to allocate image thread mutex"));
		}	
		
//		cout  << "\tPelcoImagePlugin::startSession checkpoint 3" << endl;
		// cout  << "Querying DB to find device " << endl;
		string query=string("SELECT host(d.ip) AS ip, d.puerto, d.stream FROM dispositivo d, modelo m, fabricante f WHERE d.id='")+
			StrUtils::decToString(s->devInfo.id.id)+
			string("' AND d.modelo=m.id AND d.fabricante=m.fabricante AND m.fabricante=f.id AND f.nombre='Pelco'");
		RPCPacket bdQueryPk(this->address, 0, (byte*)query.c_str(), query.length(), this->type, 
							ServiceFinder::dbGatewayTypeId);

//		cout  << "\tPelcoImagePlugin::startSession checkpoint 4" << endl;
		// Trobem la BD
		RPC *rpc=PelcoImagePlugin::getRPC(NULL);
		
		if (rpc==NULL)
		{	
			RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn, true);
			rpc=PelcoImagePlugin::getRPC(bd->a);
			delete bd;
		}
	
//		cout  << "\tPelcoImagePlugin::startSession checkpoint 5" << endl;

		RPCPacket *result=NULL;
		int retryCount=2;
		while (retryCount>0)
		{
			try
			{
				result=rpc->call(bdQueryPk);
				PelcoImagePlugin::ptrPool.release(rpc);
				retryCount=0;
			}
			catch (SocketTimeoutException &ste)
			{
				try
				{
					RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn, true);
					rpc->setDefaultDestination(*bd->a);
					delete bd;
				}
				catch (Exception &e)
				{
				}
				
				PelcoImagePlugin::dbSocketProblemCounter++;
				if (PelcoImagePlugin::dbSocketProblemCounter >
					PelcoImagePlugin::maxSocketProblemCount)
				{
					PelcoImagePlugin::dbSocketProblemCounter=0;
					retryCount--;
				}
				else
				{
					retryCount=0;
					PelcoImagePlugin::ptrPool.release(rpc);
					throw ste;
				}
			}
			catch(Exception e)
			{
				PelcoImagePlugin::ptrPool.release(rpc);
				//e.serialize()->materializeAndThrow(true);
				throw;
			}
		}
		
//		cout  << "\tPelcoImagePlugin::startSession checkpoint 6" << endl;

		string resXML((char*)result->getData(), (size_t)result->getSize());
		delete result;
		
		// cout  << "XML result " << endl << resXML << endl;
		
		XML *xml=xmlParser::parse(resXML);
	
		xmlNode *row=xml->getNode("/result/[0]");
		
		if (row==NULL)
		{
			delete xml;
	//		delete [] s->fb;
	//		delete [] s->fbLengths;
	//		delete [] s->fbKeyFrames;
			throw (IPInvalidParamException(0, "Device with specified ID does not exist"));
		}
		
	
		sInfo->ip=xml->getNode("/result/[0]/ip")->getCdata();
		sInfo->port=atoi(xml->getNode("/result/[0]/puerto")->getCdata().c_str());
		sInfo->stream=atoi(xml->getNode("/result/[0]/stream")->getCdata().c_str());
	
		delete xml;

		streamsLock.lock();
		streams[s->devInfo.id]=sInfo;
		streamsLock.unlock();
	}

//	cout  << "\tPelcoImagePlugin::startSession checkpoint 7" << endl;

	s->stream=sInfo;

	sessionLock.lock();

	// TODO 20100409: ATENCIO: S'hauria de borrar la vella, pero per poder-ho
	// fer, caldria que la vella no estigues referenciada des d'enlloc, i
	// tot ben lockat i tal (p.e. podriem estar petant aixo i que s'estigues
	// executant p.e. un getCodecInUse que fa un getSession i tal i qual)...
	// Caldra per tant un ObjectManager o similar per fer reference counting

	sessions[a->toString()]=s;
	sessionLock.unlock();

	try
	{
		cout<<"PIP:startSes.getCodecInUse"<<endl;
		sInfo->codec=getCodecInUse(a, 2000.0);
		cout<<"PIP:startSes.getCodecInUse ok"<<endl;
	}
	catch (CameraMalfunctionException &ex)
	{
		sInfo->lseMutex.lock();
		if(sInfo->stopThread) //si esta streamant, ja ho rebrà el thread...
		{
			Exception *lse=sInfo->lastStreamException;
			sInfo->lastStreamException = new CameraMalfunctionException(ex.getMsg());
			sInfo->lseMutex.unlock();
			if (lse!=NULL)
				delete lse;

			cout << "[" << StrUtils::getDateString() << "] PIP::StartSession(" << __LINE__ << "): CameraMalfunctionException:  Raising alarm: id:"<<s->devInfo.id<<"(from:"<<ex.getClass()<<":"<<ex.getMsg()<<")" <<endl;
			this->arThread->setDevStatus(s->devInfo.id.id, true, "malfunction");
		}
		else
			sInfo->lseMutex.unlock();
	}
	catch (CameraNotConnectableException &ex)
	{
		sInfo->lseMutex.lock();
		if(sInfo->stopThread) //si esta streamant, ja ho rebrà el thread...
		{
			Exception *lse=sInfo->lastStreamException;
			sInfo->lastStreamException = new CameraNotConnectableException(ex.getMsg());
			sInfo->lseMutex.unlock();
			if (lse!=NULL)
				delete lse;

			cout << "[" << StrUtils::getDateString() << "] PIP::StartSession(" << __LINE__ << "): CameraNotConnectableException: Raising alarm: id:"<<s->devInfo.id<<"(from:"<<ex.getClass()<<":"<<ex.getMsg()<<")" <<endl;
			this->arThread->setDevStatus(s->devInfo.id.id, true, "not_connectable");
		}
		else
			sInfo->lseMutex.unlock();
	}
	catch (Exception &ex)
	{
		cout << "[" << StrUtils::getDateString() << "] PIP::StartSession(" << __LINE__ << "): PIP::getCodecInUse() Not Raising alarm: id:"<<s->devInfo.id<<"(from:"<<ex.getClass()<<":"<<ex.getMsg()<<")" <<endl;
//		this->arThread->setDevStatus(s->devInfo.id.id, true, error);
	}

	if(sInfo->codec.fourcc !=  CODEC_JPEG && sInfo->codec.fourcc !=  CODEC_MPEG4)
	{
		cout << "[" << StrUtils::getDateString() << "] PIP::StartSession(): could not get Codec in use from Camera "<<s->devInfo.id.id<<" ("<<string((char*)&(sInfo->codec.fourcc), sizeof(sInfo->codec.fourcc))<<"), trying ControlModule"<<endl;
		try
		{
			ControlModuleAccess cma(this->cn);
			cma.startSession(sInfo->devInfo.id.id);
			string cmCodec=cma.getConfigParam(string("/Video/1/Codec/Fourcc"));

			cout  << "PIP::cmcodec for:" << id.id << ":" << cmCodec<< endl;
			if(cmCodec.size()>=4)
			{
				const char *c=cmCodec.c_str();
				int fourcc = CODEC_FOURCC(c[0],c[1],c[2],c[3]);
				sInfo->codec.fourcc = fourcc;
				cout  << "PIP::startSession codec for:" << id.id << " from ControlModule:"<< string((char*)&fourcc, sizeof(fourcc)) << endl;
			}

			cma.endSession(s->devInfo.id.id);
		}
		catch (Exception &ex)
		{
			cout << "[" << StrUtils::getDateString() << "] PIP::StartSession(" << __LINE__ << "): ControlModule Exception id:"<<s->devInfo.id<<" :: "<<ex.getClass()<<":"<<ex.getMsg()<<endl;
			//		this->arThread->setDevStatus(s->devInfo.id.id, true, error);
		}
	}
	cout  << "<-- PelcoImagePlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;
}

void PelcoImagePlugin::endSession(IPDeviceID id, Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout  << "--> PelcoImagePlugin::endSession(" << id << ", " << a->toString() << ")" << endl;
	sessionLock.lock();
	map<string,sessionInfo*>::iterator itSes=sessions.find(a->toString());
	
	sessionInfo *s=itSes->second;
	if (itSes==sessions.end())
	{
		sessionLock.unlock();
//		throw (IPSessionNotStablishedException(0, string("Session not stablished for device ")+
//														StrUtils::decToString(id.id)+string(" from client ")+
//														a->toString()));
		return;
	}

	sessions.erase(itSes);
	sessionLock.unlock();
	delete s;
}

IPCodecInfo PelcoImagePlugin::getCodecInUse(Address *a)
{
	cout<<"PIP::getCodecInUse"<<endl;
	return this->getCodecInUse(a, 0.0);
}

IPCodecInfo PelcoImagePlugin::getCodecInUse(Address *a, double msTimeout)
{
	sessionInfo *s=getSessionForAddress(a);
	return IPCodecInfo(CODEC_JPEG, 50, 0);
	
/*	switch (s->stream->model)
	{
		case SNC_Z20:
		case SNC_RZ30:
		case SNC_CS3:
		case SNT_V501:
		case SNT_V504:
		{
			string url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/inquiry.cgi?inq=camera");

			HttpStream *stream=NULL;
			string hdr("");
			string body("");
			try
			{
				HttpClient cli(url);
				cli.setHeader("Authorization", this->getBasicAuthStrForDevice(s->devInfo.id.id));
				if(msTimeout > 0)
				{
					cli.setConnectionTimeout(msTimeout);
				}

				stream=cli.sendRequest();
				hdr=stream->getNextInformationChunk();
				int status=stream->getRequestStatus();
				if (status/100 != 2)
					throw CameraMalfunctionException(string("Could "
						"not get codec in use from camera (HTTP Status: ") +
						StrUtils::decToString(status) + string(")"));
			
				body=stream->getNextInformationChunk();
	
				delete stream;
			}catch (Exception &e)
			{
				if(stream!=NULL)
					delete stream;
				throw CameraNotConnectableException(e.getMsg());
			}

			string::size_type q=body.find("&Quality=");
			
			if (q==string::npos)
			{
				throw (CameraMalfunctionException(2, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
				//throw (ImagePluginException(2, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
			}
			
			string qualityStr=body.substr(q+9);
			int quality=atoi(qualityStr.c_str());
		
			return IPCodecInfo(CODEC_JPEG, quality*10, 0);
		}
		break;

		case SNT_V704:
		case SNT_V704_L9EMB:
		{
			string url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/inquiry.cgi?inq=videoinfo");

			HttpStream *stream=NULL;
			string hdr("");
			string body("");
			try
			{
				HttpClient cli(url);
				cli.setHeader("Authorization", this->getBasicAuthStrForDevice(s->devInfo.id.id));
				if(msTimeout > 0)
				{
					cli.setConnectionTimeout(msTimeout);
				}
				stream=cli.sendRequest();
				hdr=stream->getNextInformationChunk();
				int status=stream->getRequestStatus();
				if (status/100 != 2)
					throw CameraMalfunctionException(string("Could "
						"not get codec in use from camera (HTTP Status: ") +
						StrUtils::decToString(status) + string(")"));
			
				body=stream->getNextInformationChunk();
	
				delete stream;
			}catch (Exception &e)
			{
				if(stream!=NULL)
					delete stream;
				throw CameraNotConnectableException(e.getMsg());
			}

			string imCodecKey=string("Vi") + StrUtils::decToString(s->stream->channel) + string("ImageCodec=");

			string::size_type codec=body.find(imCodecKey);
			
			if (codec==string::npos)
			{
				throw (CameraMalfunctionException(3, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
				//throw (ImagePluginException(3, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
			}
			
			string tvstd=body.substr(codec+imCodecKey.length());
			if (tvstd.substr(0, 1)=="0")
			{
				string jpQualityKey=string("Vi") + StrUtils::decToString(s->stream->channel) + string("JpQuality=");

				string::size_type q=body.find(jpQualityKey);
				
				if (q==string::npos)
				{
					throw (CameraMalfunctionException(4, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
					//throw (ImagePluginException(4, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
				}
				string qualityStr=body.substr(q+jpQualityKey.length());
				int quality=atoi(qualityStr.c_str());
			
				return IPCodecInfo(CODEC_JPEG, quality*10, 0);
			}
			else if (tvstd.substr(0, 1)=="1")
			{
				string m4BitrateKey=string("Vi") + StrUtils::decToString(s->stream->channel) + string("M4BitRate=");

				string::size_type br=body.find(m4BitrateKey);
				
				if (br==string::npos)
				{
					throw (CameraMalfunctionException(5, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
					//throw (ImagePluginException(5, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
				}
				string bitrateStr=body.substr(br+m4BitrateKey.length());
				int bitrate=atoi(bitrateStr.c_str());
			
				return IPCodecInfo(CODEC_MPEG4, 0, bitrate);
			}
			else
			{
				throw (ImagePluginException(0, "Unknown codec in use"));
			}
		}
		
		default:
		{
			string url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/inquiry.cgi?inq=camera");

			HttpStream *stream=NULL;
			string hdr("");
			string body("");
			try{
				HttpClient cli(url);
				cli.setHeader("Authorization", this->getBasicAuthStrForDevice(s->devInfo.id.id));
				if(msTimeout > 0)
				{
					cli.setConnectionTimeout(msTimeout);
				}
				stream=cli.sendRequest();
				hdr=stream->getNextInformationChunk();
				int status=stream->getRequestStatus();
				if (status/100 != 2)
					throw CameraMalfunctionException(string("Could "
						"not get codec in use from camera (HTTP Status: ") +
						StrUtils::decToString(status) + string(")"));

				body=stream->getNextInformationChunk();

				delete stream;
			}catch (Exception &e)
			{
				if(stream!=NULL)
					delete stream;
				throw CameraNotConnectableException(e.getMsg());
			}

			string::size_type codec=body.find("&ImageCodec=");
			
			if (codec==string::npos)
			{
				codec=body.find("ImageCodec=");
				if (codec!=0)
					throw (CameraMalfunctionException(3, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
				//throw (ImagePluginException(3, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
			}
			
			string tvstd="";
			if(codec == 0)
				tvstd=body.substr(codec+11);
			else
				tvstd=body.substr(codec+12);
			if (tvstd.substr(0, 4)=="jpeg")
			{
				string::size_type q=body.find("&JpQuality=");
				
				if (q==string::npos)
				{
					q=body.find("JpQuality=");
					if(q != 0)
						throw (CameraMalfunctionException(4, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
					//throw (ImagePluginException(4, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
				}
				string qualityStr="";
				if(q == 0)
					qualityStr=body.substr(q+8);
				else
					qualityStr=body.substr(q+9);
				int quality=atoi(qualityStr.c_str());
			
				return IPCodecInfo(CODEC_JPEG, quality*10, 0);
			}
			else if (tvstd.substr(0, 5)=="mpeg4")
			{
				string::size_type br=body.find("&M4BitRate=");
				
				if (br==string::npos)
				{
					string::size_type br=body.find("M4BitRate=");
					if (br!=0)
						throw (CameraMalfunctionException(5, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
					//throw (ImagePluginException(5, string("Unexpected device response for device id ")+StrUtils::decToString(s->devInfo.id.id)+string(": ")+body));
				}
				string bitrateStr="";
				if(br == 0)
					bitrateStr=body.substr(br+10);
				else
					bitrateStr=body.substr(br+11);
				int bitrate=atoi(bitrateStr.c_str());
			
				return IPCodecInfo(CODEC_MPEG4, 0, bitrate);
			}
			else
			{
				throw (ImagePluginException(0, "Unknown codec in use"));
			}
		}
		break;
	}*/
}

void PelcoImagePlugin::setCodecInUse(IPCodecInfo codec, Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout  << "--> PelcoImagePlugin::setCodecInUse(" << a->toString() << ")" << endl;
	sessionInfo *s=getSessionForAddress(a);
	
/*	if (s->stream->streamingMode==STREAM)
	{
		throw (ImagePluginException(0, "Codec can not be changed while in STREAM mode"));
	}
*/		
	return;
	/*
	bool found=false;
	try
	{
		ControlModuleAccess cma(this->cn);
		cma.startSession(s->devInfo.id.id);
		for(int i=1;;i++)
		{
			string cmCodec=cma.getConfigParam(string("/Video/")+StrUtils::decToString(i)+string("/Codec/Fourcc"));
			if(cmCodec==string("")||cmCodec.size()<4)
				break;
			const char *c=cmCodec.c_str();
			unsigned int fourcc=CODEC_FOURCC(c[0],c[1],c[2],c[3]);
			if(fourcc==codec.fourcc)
			{
				found=true;
				break;
			}
		}
		cma.endSession(s->devInfo.id.id);
	}
	catch (Exception &e)
	{
		cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin::setCodecInUse(): Control Module Access Exception: " << e.getClass() << ": " << e.getMsg() << endl;
		throw CameraNotConnectableException(e.getMsg());
	}	
	
	if(!found)
		throw ImagePluginException(0,"Error: codec not set in camera");
	
	string url;
	switch (codec.fourcc)
	{
		case CODEC_JPEG:
			if (s->stream->model==SNT_V704 || s->stream->model==SNT_V704_L9EMB)
				url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/videoselect.cgi?ImageCodec=0");
			else
				url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/camera.cgi?ImageCodec=jpeg");
			break;
			
		case CODEC_MPEG4:
			if (s->stream->model==SNT_V704 || s->stream->model==SNT_V704_L9EMB)
				url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/videoselect.cgi?ImageCodec=1");
			else
				url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/camera.cgi?ImageCodec=mpeg4");
			break;

		case CODEC_H264:
			if (s->stream->model==SNT_V704 || s->stream->model==SNT_V704_L9EMB)
				url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/videoselect.cgi?ImageCodec=1");
			else
				url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/command/camera.cgi?ImageCodec=h264");
			break;
	}
	
	if(codec.fourcc==CODEC_JPEG)
	{
		switch (s->stream->model)
		{
			case SNC_Z20:
			case SNC_RZ30:
			case SNC_CS3:
			case SNT_V501:
			case SNT_V504:
				break;

			default:
			{
				HttpClient cli(url);
				cli.setHeader("Authorization", this->getBasicAuthStrForDevice(s->devInfo.id.id));
				HttpStream *stream=cli.sendRequest();
				string hdr=stream->getNextInformationChunk();
				string body;

				int status=stream->getRequestStatus();

				// Si no es 200 (ok) o 204 (no content)
				if (status!=200 && status!=204)
				{
					string str=string("HTTP Error: ")+stream->getRequestStatusDesc(status);
					delete stream;

					// Deixem la sessio una mica consistent :D
					s->stream->codec=getCodecInUse(a);

					sessionLock.lock();
					sessions[a->toString()]=s;
					sessionLock.unlock();

					throw ImagePluginException(status, str);
				}
				else
					body=stream->getNextInformationChunk();

				delete stream;
			}
			break;
		}
	}

	// Ens podem permetre pillar l'actual
	s->stream->modifLock.lock();
	s->stream->codec.fourcc=codec.fourcc;//getCodecInUse(a);
	s->stream->modifLock.unlock();
	
//	sessionLock.lock();
//	sessions[a->toString()]=s;
//	sessionLock.unlock();
	*/
}

IPFrameBufferSize PelcoImagePlugin::getFrameBufferSize(Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout<<"GFBS"<<endl;
	sessionInfo *s=getSessionForAddress(a);

	IPFrameBufferSize fbSize;
	fbSize.nFrames=1; //s->fbSize;


	cout<<"/GFBS"<<endl;
	return fbSize;
}

void PelcoImagePlugin::setFrameBufferSize(IPFrameBufferSize size, Address *a)
{
	STACKTRACE_INSTRUMENT();
//	sessionInfo *s=getSessionForAddress(a);
//
////	cout << "--> PelcoImagePlugin::setFrameBufferSize(" << size.nFrames << ")" << endl;
//
//	byte **fb=new byte*[size.nFrames];
//	dword *fbL=new dword[size.nFrames];
//	bool *fbK=new bool[size.nFrames];
//	
//	if (fb==NULL)
//	{
//		throw (ImagePluginException(0, "Not enough memory to allocate frame buffer"));
//	}
//	
//	memset(fb, 0, sizeof(byte*)*size.nFrames);
//	memset(fbL, 0, sizeof(dword)*size.nFrames);
//	memset(fbK, false, sizeof(bool)*size.nFrames);
//	
//	int i=0;
//
//	for (; i<size.nFrames && i<s->fbSize; i++)
//	{
//		fb[i]=s->fb[i];
//		fbL[i]=s->fbLengths[i];
//		fbK[i]=s->fbKeyFrames[i];
//	}
//
//	// Nomes s'executara un dels dos seguents bucles
//	int j=i;
//	for (; i<size.nFrames; i++)
//	{
//		fb[i]=new byte[1]; //s->frameInfo.x*s->frameInfo.y*s->frameInfo.bpp/8];
//		if (fb[i]==NULL)
//		{
//			for (int k=j; k<i; k++)
//				delete [] fb[k];
//				
//			delete [] fb;
//			delete [] fbL;
//			throw (ImagePluginException(0, "Not enough memory to allocate frame buffer frame"));
//		}
//	}
//
//	sessionLock.lock();
//	for (; i<s->fbSize; i++)
//		if (s->fb[i]!=NULL)
//			delete [] s->fb[i];
//
//	delete [] s->fb;
//	delete [] s->fbLengths;
//	delete [] s->fbKeyFrames;
//	s->fbSize=size.nFrames;
//	s->fb=fb;
//	s->fbLengths=fbL;
//	s->fbKeyFrames=fbK;
//
//	sessions[a->toString()]=s;
//	sessionLock.unlock();


}

float PelcoImagePlugin::getFrameBufferPercentInUse(Address *a)
{
	STACKTRACE_INSTRUMENT();
//	sessionInfo s=getSessionForAddress(a);

	return 0.0f; //(float)s->fbUse/(float)s->fbSize;
}

int PelcoImagePlugin::getFrameBufferFramesInUse(Address *a)
{
	STACKTRACE_INSTRUMENT();
//	sessionInfo s=getSessionForAddress(a);

	return 0; //s->fbUse;
}

IPStreamingMode PelcoImagePlugin::getStreamingMode(Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout  << "--> PelcoImagePlugin::getStreamingMode(" << a->toString() << ")" << endl;
	sessionInfo *s=getSessionForAddress(a);

	return s->stream->streamingMode;
}

void PelcoImagePlugin::setStreamingMode(IPStreamingMode mode, Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout  << "--> PelcoImagePlugin::setStreamingMode(" << a->toString() << ")" << endl;
	sessionInfo *s=getSessionForAddress(a);
	

	cout << "SetStreamingMode id: stream?"<<(mode.mode == IPStreamingMode::STREAM)<<" act:"<<(s->stream->streamingMode.mode == IPStreamingMode::STREAM)<<" id:"<< s->devInfo.id.id << endl;
	// Fem un kill del thread, pero nomes si ens demanen passar a NOSTREAM i estaven en STREAM
	if (mode.mode==IPStreamingMode::NOSTREAM && s->stream->streamingMode.mode==IPStreamingMode::STREAM)
	{
/*		switch (s->codec.fourcc)
		{
			case CODEC_JPEG:
				jpegThread.kill(s->threadId);
				break;
			case CODEC_MPEG4:
				mpeg4Thread.kill(s->threadId);
				break;
		}
*/
		if (s->stream->threadId!=-1)
		{
//			cout << "setStrMode lock" << endl;
			if (s->stream->imageThreadLock!=NULL)
				s->stream->imageThreadLock->lock();
//			cout << "setStrMode locked" << endl;
			s->stream->stopThread=true;
	//		sessions[a->toString()]=s;
			if (s->stream->imageThreadLock!=NULL)
				s->stream->imageThreadLock->unlock();
	//		cout << "joining " << s->threadId << endl;
	
			string codecName("Unknown");
			switch (s->stream->codec.fourcc)
			{
				case CODEC_JPEG:
					codecName=string("JPEG");
					break;
				case CODEC_MPEG4:
					codecName=string("MPEG4");
					break;
				case CODEC_H264:
					codecName=string("H264");
					break;
			}
		
			genericStreamThread.join(s->stream->threadId);
			
	//		cout << "joined" << endl;
	
			s->stream->modifLock.lock();
			s->stream->stopThread=false;
//			delete s->stream->lastAccessToFB;
//			s->stream->lastAccessToFB=NULL;
			s->stream->threadId=-1;
			s->stream->modifLock.unlock();
		}
	}
	else if (mode.mode==IPStreamingMode::STREAM && s->stream->streamingMode.mode==IPStreamingMode::NOSTREAM)
	{
		// I si fem el pas a la inversa, iniciem el thread amb una serie de params
			cout << "[" << StrUtils::getDateString() << "] PIP::setStreamingMode Thread id:"<<s->stream->threadId<<endl;
		if (s->stream->threadId==-1)
		{
//			cout << "ssm cst " << (void*)s->stream << endl;
			cout << "[" << StrUtils::getDateString() << "] PIP::setStreamingMode -> createStreamThread:"<<s->devInfo.id.id<<":"<<s->stream->ip<<endl;
			createStreamThread(s->stream, a);
		}
/*		s->lastAccessToFB=new Timer();
		s->lastAccessToFB.start();
		sessions[a->toString()]=s;
		
		// I si fem el pas a la inversa, iniciem el thread amb una serie de params
		PelcoImagePluginStreamThread::imageThreadArgs *ita=new PelcoImagePluginStreamThread::imageThreadArgs();
		
		ita->a=*a;
		ita->plugin=this;
		ita->timeoutTimer=s->lastAccessToFB;
		ita->threadTimeout=s->streamTimeout;
		
		switch (s->codec.fourcc)
		{
			case CODEC_JPEG:
				s->threadId=jpegThread.start(ita);
				break;
			case CODEC_MPEG4:
				s->threadId=mpeg4Thread.start(ita);
				break;
		}
*/

	}

	if (mode.mode!=IPStreamingMode::STREAM && mode.mode!=IPStreamingMode::NOSTREAM)
	{
		cout << "SetStreamingMode: throw:"<<string("Invalid streaming mode: ")<<StrUtils::decToString(s->stream->streamingMode.mode)<< endl;
		throw (ImagePluginException(0, string("Invalid streaming mode: ")+StrUtils::decToString(s->stream->streamingMode.mode)));
	}
	s->stream->modifLock.lock();
	s->stream->streamingMode=mode;
	s->stream->modifLock.unlock();
	
}

IPFrame PelcoImagePlugin::getCompressedNextFrame(Address *a)
{
	STACKTRACE_INSTRUMENT();
	Timer t, tparc;
	TimerInstant tpi;
	t.start();
	tparc.start();

	sessionInfo *s;
	try
	{
		s=getSessionForAddress(a);
	}
	catch(Exception &e)
	{
		cout  << "PelcoImagePlugin::getStreamingMode(" << a->toString() << ") "<< e.getClass() << "::" << e.getMsg() << endl;
		throw;
	}

	if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
	|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
	{
		tpi=tparc.time();
		tparc.start();
		
		cout << "[" << StrUtils::getDateString() <<
			"]\t\tPIP::gCNF(" << a->toString() << ")[devId:"
			<< s->devInfo.id << "] @ line " << __LINE__ << ", "
			<< fixed << tpi.seconds() << "secs"	<< endl;
	}

	IPCodecInfo codecInfo=s->stream->codec; //getCodecInUse(a);

//	cout << "--> [" << t.getStartSeconds() << "] PIP::getCompressedNextFrame(" << a->toString() << ") ini" << endl;

	switch (s->stream->streamingMode.mode)
	{
		case IPStreamingMode::NOSTREAM:
		{
//			cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin::getCompressedNextFrame() NoStream " << endl;
			string url;
			
			switch (codecInfo.fourcc)
			{
				case CODEC_JPEG:
					cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin::getCompressedNextFrame() NoStream JPEG " << endl;
					url=string("http://")+s->stream->ip+string(":")+StrUtils::decToString(s->stream->port)+string("/oneshotimage.jpg");
//					cout << "NOSTREAM & JPEG " << s->devInfo.id << endl; 
					break;
					
				default:
					cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin::getCompressedNextFrame()  Exception  NoStream + "<< string((char*)&codecInfo.fourcc, sizeof(codecInfo.fourcc)) << endl;
					throw (ImagePluginException(0, string("Invalid codec in use ")+
													string((char*)&codecInfo.fourcc, sizeof(codecInfo.fourcc))+
													string(" for NOSTREAM mode")));
					break;
			}

			HttpClient cli(url);
			cli.setHeader("Authorization", this->getBasicAuthStrForDevice(s->devInfo.id.id));
			HttpStream *stream=cli.sendRequest();

			string hdr=stream->getNextInformationChunk();
			string body;

			int status=stream->getRequestStatus();

			// Si no es 200 (ok) o 204 (no content)
			if (status!=200 && status!=204)
			{
				string str=string("HTTP Error: ")+stream->getRequestStatusDesc(status);
				delete stream;
				throw ImagePluginException(status, str);
			}
			else
				body=stream->getNextInformationChunk();

			delete stream;
			
			byte *buf=new byte[body.length()];
			memmove(buf, body.c_str(), body.length());
			
			return IPFrame(s->stream->frameInfo, codecInfo, true, body.length(), buf);
		}
		break;
			
		case IPStreamingMode::STREAM:
		{
			// Com que accedim al frame buffer, comptem l'acces
//			cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin::getCompressedNextFrame()  Stream + "<< string((char*)&codecInfo.fourcc, sizeof(codecInfo.fourcc)) << endl;

			if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
			|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
				tparc.start();

			s->stream->modifLock.lock();
//			if (s->stream->lastAccessToFB!=NULL)		// Thread en marxa
//			{
				try{
					s->stream->lastAccessToFB.start();
				}catch(...){}
//			}
			s->stream->modifLock.unlock();

			if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
			|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
			{
				tpi=tparc.time();
				tparc.start();
				
				cout << "[" << StrUtils::getDateString() <<
					"]\t\tPIP::gCNF(" << a->toString() << ")[devId:"
					<< s->devInfo.id << "] @ line " << __LINE__ << ", "
					<< fixed << tpi.seconds() << "secs"	<< endl;
			}

			if (s->stream->stopThread==true)		// El thread s'ha parat
			{
//				s->fbUse=0;
				cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin::getCompressedNextFrame()  Thread stopped" << endl;
				if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
				|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
					tparc.start();

				createStreamThread(s->stream, a);

				if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
				|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
				{
					tpi=tparc.time();
					tparc.start();
					
					cout << "[" << StrUtils::getDateString() <<
						"]\t\tPIP::gCNF(" << a->toString() << ")[devId:"
						<< s->devInfo.id << "] @ line " << __LINE__ << ", "
						<< fixed << tpi.seconds() << "secs"	<< endl;
				}
			}

			if (s->stream->lastStreamException!=NULL)
			{
				s->stream->lseMutex.lock();
				
				if (s->stream->lastStreamException!=NULL)
				{
					cout << "\t" <<  Log::getDateLogTag() << " devId: " << s->devInfo.id.id << " PIP::gCNF(" << a->toString() << "): throw last stream Exception:"<<s->stream->lastStreamException->getClass()<<endl;
					SerializedException *se = s->stream->lastStreamException->serialize();
					s->stream->lseMutex.unlock();
					se->materializeAndThrow(true);
				}
				else
					s->stream->lseMutex.unlock();
			}

			int totalWaited=0;
			if (codecInfo.fourcc == CODEC_JPEG)
			{
//				cout << "[" << StrUtils::getDateString() << "] PelcoImagePlugin::getCompressedNextFrame()  Stream JPEG " << endl;

				while (s->stream->lastFrame==NULL && totalWaited<PelcoImagePlugin::maxFramesWaitMS)
				{
					// Esperem "1/20" frame (@25fps)
					usleep(2000);
					totalWaited+=2;
//					cout << "gcnf: -> frame buffer empty" << endl;
					//				s=getSessionForAddress(a);
				}

				//sleep(1);
				if (totalWaited>=PelcoImagePlugin::maxFramesWaitMS)
    			{
    				double preTime = t.getStartSeconds();
    				t.start();
    				cout << "\t\t" <<  Log::getDateLogTag() << " devId: " << s->devInfo.id.id << " PIP::gCNF(" << a->toString() << "): STILL no data, but waited " << PelcoImagePlugin::maxFramesWaitMS << "ms (threadId:"<<s->stream->threadId<<", stop:"<<s->stream->stopThread<<", started: " << fixed << preTime << ", now: " << fixed << t.getStartSeconds() << ", JPEG)" <<endl;
    			}

				s->stream->modifLock.lock();

				bool isKey=s->stream->isKey; //fbKeyFrames[0];

				dword len=s->stream->lastFrameLen;
				byte *buf=new byte[len+2*sizeof(int)];
				*((unsigned int*)buf) = 1;
				*((unsigned int*)(buf+sizeof(int))) = len;

				
				memmove(buf+2*sizeof(int), s->stream->lastFrame, len);
//				cout<<"  fra,e:"<<StrUtils::hexDump(string((char*)s->stream->lastFrame, 24*sizeof(int)))<<endl;
//cout<<"  IPFrame buf:"<<StrUtils::hexDump(string((char*)buf, 24*sizeof(int)))<<endl;

				s->stream->modifLock.unlock();
				return IPFrame(s->stream->frameInfo, codecInfo, isKey, len+2*sizeof(int), buf);
			}
			else
			{
				// Hem de veure quins frames son streamables.
				list<receivedFrameInfo*> streamableFrames;
				dword totalStreamableDataLen=0;
				
				bool isKey=false; // true;//s->stream->isKey; //fbKeyFrames[0];

				// Entrem al bucle amb s->stream->modifLock.lock();
				// i sortim igual, pero durant les esperes s'allibera :P

				if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
				|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
					tparc.start();

				s->stream->modifLock.lock();
				double lastRecvdTime=0.0, nextFrameTime=0.0;
				
				list<receivedFrameInfo*>::iterator lIt;
				while (streamableFrames.empty() && totalWaited<PelcoImagePlugin::maxFramesWaitMS)
				{
//					if (s->devInfo.id.id==64)
//					{
//						cout << "\n------------------------------";
//						cout.flush();
//					}

//					cout << "\t" <<  Log::getDateLogTag() << " devId: " << s->devInfo.id.id << " PIP::gCNF(" << a->toString() << "): streamable:"<<streamableFrames.size()<<endl;
					for (lIt=s->stream->recvdFrames.begin();
						lIt!=s->stream->recvdFrames.end(); lIt++)
					{
						TimerInstant ti=(*lIt)->recvdTime.time();
						
//						if (s->devInfo.id.id==64)
//						{
//							cout << "\n" << ti.seconds() << " -- "
//								<< (*lIt)->recvdTime.getStartSeconds() << "--"
//								<< s->lastStreamedTimer.getStartSeconds();
//							cout.flush();
//						}

						// Stats
						if ((*lIt)->recvdTime.getStartSeconds()>lastRecvdTime)
							lastRecvdTime=(*lIt)->recvdTime.getStartSeconds();

						if (nextFrameTime==0.0 && (*lIt)->recvdTime.getStartSeconds()>s->lastStreamedTimer.getStartSeconds())
							nextFrameTime=(*lIt)->recvdTime.getStartSeconds();
						
						// Els frames han de tenir com a minim 
						// minimumFrameAge ms, per 
						// eliminar el jitter de la camera/codec i ha de ser
						// mes nou que l'ultim que hem enviat.
						if (ti.seconds()>=s->stream->minimumFrameAgeMS &&
							(*lIt)->recvdTime.getStartSeconds() >
							s->lastStreamedTimer.getStartSeconds())
						{
							if ((*lIt)->isKey)
							{
								// Si algun es key, tot ho es, i podem
								// eliminar la resta d'streamable :P
								isKey=true;
								streamableFrames.clear();
								totalStreamableDataLen=0;
							}
							
							streamableFrames.push_back(*lIt);
							totalStreamableDataLen+=(*lIt)->frameLen;
						}
					}

					// TODO: Fins i tot es podria calcular l'espera optima ;)
					if (streamableFrames.empty())
					{
						s->stream->modifLock.unlock();
						double shouldWait = 0.0;
						
						if (nextFrameTime!=0.0)
						{
							// Esperem a que next tingui minimumAge
							// nextFrameTime - (now - 300)
							shouldWait = nextFrameTime - (t.getStartSeconds() - s->stream->minimumFrameAgeMS);
						}
						// Si no tenim nextFrame, es pot rebre en qualsevol
						// moment, esperem en increments de 5ms
						
						if ((shouldWait + (float)totalWaited/1000.0f) > PelcoImagePlugin::maxFramesWaitMS)
						{
							// Sortim del while sense frames perque el seguent
							// queda massa lluny!
							// Deixem el lock lockat que és com ha d'estar a
							// fora del while :)
							s->stream->modifLock.lock();
							break;
						}	
						if (lastRecvdTime > 0.0 
							&& shouldWait > 0.0)
//							&& shouldWait < s->stream->minimumFrameAgeMS)
						{
							dword usWait = (dword)(shouldWait*1000000.0);
//							cout << "STILL, waiting " << usWait << "us" << " devId: " << s->devInfo.id.id << endl;
							if (usWait>=1000) // minim 1ms
								usleep(usWait);
							// i tb sumem minim 1
							totalWaited+=(usWait>=1000?usWait/1000:1);
						}
						else if (shouldWait <= 0.0)
						// si havia sortit negatiu (de  (0..1]ms
						// no esperem), o be si no tenim frame seguent
						{
//							cout << "STILL, waiting 5ms" << " devId: " << s->devInfo.id.id << " (shouldWait: " << shouldWait << ")" << endl;
							usleep(5000);
							totalWaited+=5;
						}
							
						s->stream->modifLock.lock();
					}
				}

				if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
				|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
				{
					tpi=tparc.time();
					tparc.start();
					
					cout << "[" << StrUtils::getDateString() <<
						"]\t\tPIP::gCNF(" << a->toString() << ")[devId:"
						<< s->devInfo.id << "] @ line " << __LINE__ << ", "
						<< fixed << tpi.seconds() << "secs"	<< endl;
				}
	
				// Iniciem a PelcoImagePlugin::maxFramesWaitMS pq si fps.whole fos 0 no volem que entri a cap
				dword totalWaitedLimitByFPS = PelcoImagePlugin::maxFramesWaitMS;
				if (s->stream->fps.whole>0)
					totalWaitedLimitByFPS=1000/s->stream->fps.whole;
				
				if (totalWaited>=PelcoImagePlugin::maxFramesWaitMS) // && s->devInfo.id.id==64)
    			{
    				double preTime=t.getStartSeconds();
    				t.start();
    				cout << "\t\t" <<  Log::getDateLogTag() << " devId: " << s->devInfo.id.id << " PIP::gCNF(" << a->toString() << "): STILL no data, but waited " << PelcoImagePlugin::maxFramesWaitMS << "ms (threadId:"<<s->stream->threadId<<", stop:"<<s->stream->stopThread<<", # frames in recvd: " << s->stream->recvdFrames.size()<<", started: " << fixed << preTime << ", now: " << fixed << t.getStartSeconds() <<", lastRecvdTime: " << fixed << lastRecvdTime << ")" <<endl;
    			}
//    			else if (totalWaited > totalWaitedLimitByFPS)
//    			{
//    				// Ens estan demanant mes FPS dels q toca
//    				t.start();
//    				cout << "\t\t" <<  Log::getDateLogTag() << " devId: " << s->devInfo.id.id << " PIP::gCNF(" << a->toString() << "): FOUND data after waiting " << totalWaited << " ms @ " << s->stream->fps.whole << " FPS (threadId:"<<s->stream->threadId<<", stop:"<<s->stream->stopThread<<", # frames in recvd: " << s->stream->recvdFrames.size()<<", now: " << fixed << t.getStartSeconds() <<", lastRecvdTime: " << fixed << lastRecvdTime << ")" <<endl;
//    			}
				
				// Hem de fer l'streaming de tots els streamable frames
				int nFrames=streamableFrames.size();
				dword len = totalStreamableDataLen + nFrames*sizeof(int) +
					sizeof(int);
				 
				byte *buf=new byte[len];
				byte *b=buf;
				
				*(dword*)b=nFrames;
				b+=sizeof(dword);
				
				dword partialLen=nFrames*sizeof(int)+sizeof(int);
				
				for (lIt=streamableFrames.begin();
					lIt!=streamableFrames.end(); lIt++)
				{
					if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
					|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
					{
						double preTime = t.getStartSeconds();
						t.start();
						
						cout << "[" << StrUtils::getDateString() <<
							"]\t\tPIP::gCNF(" << a->toString() << ")[devId:"
							<< s->devInfo.id << "] @ ("
							<< fixed << preTime << " ... " << fixed << 
							t.getStartSeconds() << "): Packing frame [" 
							<< fixed << (*lIt)->recvdTime.getStartSeconds() 
							<< "] after " << fixed << 
							floor((t.getStartSeconds() - preTime)*1000.0f) 
							<< "ms"	<< endl;
					}
					*(dword*)b=(*lIt)->frameLen;
					b+=sizeof(dword);
					
					memmove(&buf[partialLen], (*lIt)->frame, (*lIt)->frameLen);
					partialLen+=(*lIt)->frameLen;

					// Anem updatant l'ultim enviat :P
					s->lastStreamedTimer=(*lIt)->recvdTime;
				}
				
				s->stream->modifLock.unlock();

				if (s->devInfo.id.id==PelcoImagePlugin::SIGUSR1TraceCameraId
				|| s->devInfo.id.id==PelcoImagePlugin::SIGUSR2TraceCameraId)
				{
					tpi=tparc.time();
					tparc.start();
					
					cout << "[" << StrUtils::getDateString() <<
						"]\t\tPIP::gCNF(" << a->toString() << ")[devId:"
						<< s->devInfo.id << "] @ line " << __LINE__ << ", "
						<< fixed << tpi.seconds() << "secs"	<< endl;
				}
				
//				if (s->devInfo.id.id==64)
//				{
//					string recCOUT = string("\n") + Log::getDateLogTag() + 
//						string(" [CAM ") + StrUtils::decToString(s->devInfo.id.id) + 
//						string("] streaming ") +
//						StrUtils::decToString(nFrames) + string(" frames (")+
//						StrUtils::decToString(len) + string(" bytes)");
//					cout << recCOUT;
//					cout.flush();
//				}				


				return IPFrame(s->stream->frameInfo, codecInfo, isKey, len, buf);
			}
			
		}
		break;
			
		default:
			cout << "Invalid streaming mode: " << s->stream->streamingMode.mode << endl;
			break;
	}

	throw (ImagePluginException(0, string("Invalid streaming mode: ")+StrUtils::decToString(s->stream->streamingMode.mode)));
}

IPFrame PelcoImagePlugin::getDecompressedNextFrame(Address *a)
{
	STACKTRACE_INSTRUMENT();
//	sessionInfo s=getSessionForAddress(a);

	throw(ImagePluginException(-1, "Not implemented"));
}

IPFramesPerSecond PelcoImagePlugin::getFPS(Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout  << "--> PelcoImagePlugin::getFPS(" << a->toString() << ")" << endl;
	sessionInfo *s=getSessionForAddress(a);

	s->stream->modifLock.lock();
	IPFramesPerSecond fps(s->stream->fps.whole, s->stream->fps.frac);
	s->stream->modifLock.unlock();
	
	return fps;
}

void PelcoImagePlugin::setFPS(IPFramesPerSecond fps, Address *a)
{
	STACKTRACE_INSTRUMENT();
	cout  << "--> PelcoImagePlugin::setFPS(" << a->toString() << ")" << endl;
	sessionInfo *s=getSessionForAddress(a);

	s->stream->modifLock.lock();

//	cout << "setFPS: current fps: " << s->stream->fps->whole << ", requested: " << fps.whole << endl;

	// Canviem si estem augmentant els FPS o si es el primer canvi.
	if (s->stream->stillDefaultFPS || fps.whole>s->stream->fps.whole || (fps.whole==s->stream->fps.whole && fps.frac>s->stream->fps.frac))
	{
		bool defFPS=(s->stream->stillDefaultFPS && s->stream->fps.whole==fps.whole && s->stream->fps.frac==fps.frac);
		bool fpsChgd=(s->stream->fps.whole!=fps.whole || s->stream->fps.frac!=fps.frac);
		s->stream->fps.whole=fps.whole;
		s->stream->fps.frac=fps.frac;
	
		s->stream->stillDefaultFPS=defFPS;
		s->stream->fpsChanged=fpsChgd;
	}

	s->stream->modifLock.unlock();
}

string PelcoImagePlugin::getConfigFileName()
{
	STACKTRACE_INSTRUMENT();
	return configFile;
}
