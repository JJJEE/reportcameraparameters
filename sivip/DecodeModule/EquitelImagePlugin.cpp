/*
 *  EquitelImagePlugin.cpp
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "EquitelImagePlugin.h"
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Utils/Canis.h>
#include <Utils/StrUtils.h>
#include <Plugins/GestorImageExceptions.h>
#include <Http/HttpClient.h>
#include <Http/HttpStream.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <ServiceFinder/ServiceFinder.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <Utils/FileException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <math.h>


extern "C" {
#include <avcodec.h>
#include <avformat.h>
// #include <swscale.h>
}

#if LIBAVCODEC_VERSION_MAJOR < 53

#define AV_PKT_FLAG_KEY PKT_FLAG_KEY
#define AVMEDIA_TYPE_VIDEO CODEC_TYPE_VIDEO

#endif

#ifndef WIN32
#ifndef WIN32
#include <unistd.h>
#endif
#else
extern "C" {
#include <jpeglib.h>
}

#include <Utils/WindowsDefs.h>
#include <Utils/WindowsInclude.h>
#include "sample/sample06/resource.h"
#endif

extern struct timeval operator + (struct timeval a, struct timeval b);

#pragma mark *** Pool
PtrPool EquitelImagePlugin::ptrPool;

RPC* EquitelImagePlugin::getRPC(Address *a)
{
	RPC *rpc=(RPC*)ptrPool.get();
	
	if (rpc==NULL && a!=NULL)
	{
		rpc=new RPC(*a);
		if (rpc==NULL)
		{
			throw (Exception("FATAL: Not enough memory"));
		}
		ptrPool.add(rpc, true);
	}
	
	return rpc;
}

#pragma mark *** Privades
const IPFrameInfo EquitelImagePlugin::resolutions[EquitelImagePlugin::NUM_VIDEOSTD][EquitelImagePlugin::NUM_RESOLUTIONS]={
		{IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)},
		{IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)}};

int GetNextFrame(AVFormatContext *pFormatCtx, AVCodecContext *pCodecCtx, 
    int videoStream, AVFrame *pFrame, byte **frame, AVPacket *packet, int (*bytesRemaining))
{
    while(true)
    {
		int status=av_read_frame(pFormatCtx, packet);

		if (status>=0 && packet->stream_index==videoStream)
		{
			int extradataSize=pCodecCtx->extradata_size;
			if (extradataSize<0)
				extradataSize=0;
			else if (extradataSize>128)
				extradataSize=128;

			int sz=packet->size+extradataSize+sizeof(int)*3;
			byte *aux=new byte[sz];
			byte *w=aux;

			*((unsigned int*)w)=2;
			w+=sizeof(unsigned int);

			*((unsigned int*)w)=pCodecCtx->extradata_size;
			w+=sizeof(unsigned int);
			memmove(w, pCodecCtx->extradata, pCodecCtx->extradata_size);
			w+=pCodecCtx->extradata_size;

			*((unsigned int*)w)=packet->size;
			w+=sizeof(unsigned int);
			memmove(w, packet->data, packet->size);
			w+=packet->size;

			(*frame)=aux;

//				cout << "." << packet->size << "-" << pCodecCtx->extradata_size << "-" << (packet->flags&PKT_FLAG_KEY);

			if ((packet->flags&AV_PKT_FLAG_KEY)!=0) //PKT_FLAG_KEY
				pFrame->key_frame=1;
			else
				pFrame->key_frame=0;

			av_free_packet(packet);
//			cout.flush();

			return sz;
		}
		else
		{
			av_free_packet(packet);
		}
    }
}

EquitelImagePlugin::sessionInfo EquitelImagePlugin::getSessionForAddress(Address *a)
{
	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
		throw (IPSessionNotStablishedException(0, string("Session not stablished with any device ")+
												   string("from client ")+a->toString()));
	
	return (*itSes).second;
}

EquitelImagePlugin::streamInfo* EquitelImagePlugin::getStreamInfoForDevice(int id)
{
	map<int,streamInfo*>::iterator itStr=streams.find(id);
	
	if (itStr==streams.end())
		return NULL;
	
	return itStr->second;
}

#pragma mark *** Threads de recepció imatges

void* EquitelImagePluginStreamThread::execute(int id, void *args)
{
	EquitelImagePluginStreamThread::imageThreadArgs *ita=(EquitelImagePluginStreamThread::imageThreadArgs *)args;
	EquitelImagePlugin *plugin=(EquitelImagePlugin *)ita->plugin;

	EquitelImagePlugin::streamInfo *s=(EquitelImagePlugin::streamInfo*)ita->s;

	cout<<"EquitelImagePluginStreamThread::execute"<<endl;
	string puText("");
	bool popupText=false;
	bool popupTextSent=false;
	
	// Comencem a agafar excepcions des d'aqui, pq pot petar el tema control module :P
	ControlModuleAccess *controlModule=NULL;
	string url=string("rtsp://")+s->ip+string(":")+/*StrUtils::decToString(s->port)*/string("554")+string("/s=1");
	cout<<" url:"<<url<<endl;


   	AVPacket packet;
   	av_init_packet(&packet);
    int      bytesRemaining=0;


    AVFormatContext *pFormatCtx;
    int             i, videoStream;
    AVCodecContext  *pCodecCtx;
    AVCodec         *pCodec;
    AVFrame         *pFrame; 
    AVFrame         *pFrameRGB;
    int             numBytes;
    uint8_t         *buffer;
	

	try
	{
		cout<<"open"<<endl;
		int err=0;
		//Daniel: avformat_open_input()-->Ultimo parametro 'NULL' inventado
		if((err=avformat_open_input(&pFormatCtx, url.c_str(), NULL, NULL))!=0)
			throw Exception(string("Couldn't open stream - ffmpeg: ")+StrUtils::decToString(err));

		// Retrieve stream information
		cout<<"find"<<endl;
		//if(av_find_stream_info(pFormatCtx)<0)
		//	throw Exception("Couldn't find stream information");

		// Dump information about file onto standard error
		cout<<"dump - pktBuf: " << (void*) pFormatCtx->packet_buffer <<endl;
		//dump_format(pFormatCtx, 0, url.c_str(), false);

		// Find the first video stream
		videoStream=-1;
		for(i=0; i<pFormatCtx->nb_streams; i++)
		{
			if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO)//CODEC_TYPE_VIDEO
			{
				videoStream=i;
				break;
			}
		}
		if(videoStream==-1)
			throw Exception("Didn't find a video stream");
		cout<<"stream:"<<videoStream<<endl;

		// Get a pointer to the codec context for the video stream
		pCodecCtx=pFormatCtx->streams[videoStream]->codec;
		
		//pCodecCtx->parse_only=1;  Comentado para que compile
		
		s->frameInfo.x= pCodecCtx->width;
		s->frameInfo.y= pCodecCtx->height;

		// Find the decoder for the video stream
		cout<<"find dec"<<endl;
		pCodec=avcodec_find_decoder(pCodecCtx->codec_id);
		cout<<" codec id:" << pCodecCtx->codec_id << endl;
		cout<<" iformat name:" << pFormatCtx->iformat->name << endl;
		cout<<" iformat long name:" << pFormatCtx->iformat->long_name << endl;
		if(pCodec==NULL)
			throw Exception("Decoder for stream codec not found");
			
		cout<<" libav codec name:" << pCodec->name << endl;
		cout<<" libav codec id:" << pCodec->id << endl;

		// Open codec
		cout<<"open dec"<<endl;
		if(avcodec_open2(pCodecCtx, pCodec,NULL)<0)
			throw Exception("Could not open codec");

		if(pCodec->capabilities & CODEC_CAP_TRUNCATED)
			pCodecCtx->flags|=CODEC_FLAG_TRUNCATED;

		// Allocate video frame
		cout<<"alloc frame"<<endl;
		pFrame=avcodec_alloc_frame();

		Timer t;
		TimerInstant ti;
		t.start();
		bool first=true;

		cout<<" ---------- start streaming ------------"<<endl;
		while (true)
		{
			// UPDATE HTTP SI HAN CANVIAT ELS FPS
			//			if (s->fpsChanged)
			//			{}

			s->imageThreadLock->lock();

			//	TimerInstant ti=ita->timeoutTimer->time();
			TimerInstant ti=ita->timeoutTimer->time();
			if (ti.seconds() > s->streamTimeout)
			{
				// Evitem l'aparicio d'N errors ara que encara no ho gestionem
				//			ita->timeoutTimer->start(); // TODO: Eliminar aquesta linia quan es gestioni el tema dels timeouts

				cout << "EquitelImagePluginStreamThread::execute(): Thread Timeout occurred, stream is not being used!" << endl;
				s->stopThread=true;
//			}
//
//			if (s->stopThread)
//			{
				cout << "EquitelImagePluginStreamThread::execute(): Stopping thread..." << endl;
				// Si estem parant per timeout, cal deixar-ho tot llest pq el proxim get(De)compressedNextFrame el pugui iniciar

				//				if (ti.seconds() > s->streamTimeout)
				//				{
				s->stopThread=false;
				delete s->lastAccessToFB;
				s->lastAccessToFB=NULL;
				s->threadId=-1;
				//				}


				if(pCodecCtx!=NULL)
					avcodec_close(pCodecCtx);
				if(pFormatCtx!=NULL)
					//av_close_input_file(pFormatCtx);  Deprecated

				s->imageThreadLock->unlock();

				delete ita;
				delete controlModule;

				cout << "EquitelImagePluginStreamThread::execute(): Stopped..." << endl;
				return NULL;
			}


			byte *newFrame=NULL;
			int len;
    		if((len=GetNextFrame(pFormatCtx, pCodecCtx, videoStream, pFrame, &newFrame, &packet, &bytesRemaining))>0)
			{

				s->modifLock.lock();
				first=false;
				if(pFrame->key_frame)
				{
					s->isKey=true;
					s->nframes=1;
					s->keyFrame++;
					s->frameStreamHeader=string((char*)&len, sizeof(unsigned int));
					s->streamFrames=string((char*)newFrame, len);

				}
				else
				{
					s->nframes++;
					s->frameStreamHeader+=string((char*)&len, sizeof(unsigned int));
					s->streamFrames+=string((char*)newFrame, len);

				}
				s->modifLock.unlock();
			}

			if (newFrame!=NULL)
			{
				delete [] newFrame;
				newFrame=NULL;
			}

			if (popupTextSent)
			{
				popupTextSent=false;
				//			try
				//			{
				//				Fem el guarro i sudem de la resposta ;)
				controlModule->ignoreAsyncResult();
				//			}
				//			catch (Exception &e)
				//			{
				//				cout << "controlModule->getAsyncResult: " << e.getClass() 
				//					<< ":" << e.getMsg() << endl;
				//			}
			}

			s->imageThreadLock->unlock();
//				usleep(40000);

			ti=t.time();
//			cout << "sf: " << ti.seconds() << endl;
			t.start();
		}
	}
	catch (Exception &e)
	{
		// Simulem un timeout....
		cout << "EquitelImagePluginStreamThread::execute(): Exception: " << e.getClass() << ": " << e.getMsg() << endl;
			cout << "EquitelImagePluginStreamThread::execute(): Stopping thread..." << endl;

		if (!s->modifLock.isHeldByCurrentThread())
			s->modifLock.lock();
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		s->modifLock.unlock();
		if (s->imageThreadLock->isHeldByCurrentThread())
			s->imageThreadLock->unlock();

		if (popupTextSent)
		{
			popupTextSent=false;
//				try
//				{
//				Fem el guarro i sudem de la resposta ;)
				controlModule->ignoreAsyncResult();
//				}
//				catch (Exception &e)
//				{
//					cout << "controlModule->getAsyncResult: " << e.getClass() 
//						<< ":" << e.getMsg() << endl;
//				}
		}
//			cout << "EquitelImagePluginStreamThread::execute() 6" << endl;
//			cout << "Stopping thread" << endl;
		delete ita;
		delete controlModule;
		
		return NULL;
	}
	catch (...)
	{
		// Simulem un timeout....
		cout << "EquitelImagePluginStreamThread::execute(): unknown exception" << endl;
			cout << "EquitelImagePluginStreamThread::execute(): Stopping thread..." << endl;

		if (!s->modifLock.isHeldByCurrentThread())
			s->modifLock.lock();
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		s->modifLock.unlock();
		if (s->imageThreadLock->isHeldByCurrentThread())
			s->imageThreadLock->unlock();

		if (popupTextSent)
		{
			popupTextSent=false;
//				try
//				{
//				Fem el guarro i sudem de la resposta ;)
				controlModule->ignoreAsyncResult();
//				}
//				catch (Exception &e)
//				{
//					cout << "controlModule->getAsyncResult: " << e.getClass() 
//						<< ":" << e.getMsg() << endl;
//				}
		}
//			cout << "EquitelImagePluginStreamThread::execute() 6" << endl;
//			cout << "Stopping thread" << endl;
		delete ita;
		delete controlModule;
		
		return NULL;
	}

//	cout << "EquitelImagePluginStreamThread::execute() 9" << endl;
	cout << "EquitelImagePluginStreamThread finishing..." << endl;

		if (!s->modifLock.isHeldByCurrentThread())
			s->modifLock.lock();
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		s->modifLock.unlock();
		if (s->imageThreadLock->isHeldByCurrentThread())
			s->imageThreadLock->unlock();

	delete ita;
	delete controlModule;
	return NULL;
}



/*
#endif
*/

#pragma mark *** Constructores

void EquitelImagePlugin::createStreamThread(EquitelImagePlugin::streamInfo *s, Address *a)
{
	if (s->threadId!=-1)
		return;

//	cout << "cST mL lock" << endl;
	s->modifLock.lock();
//	cout << "cST mL locked" << endl;

	if (s->imageThreadLock!=NULL)
	{
//		cout << "createStreamThread: delete s->imageThreadLock" << endl;
		delete s->imageThreadLock;
//		cout << "createStreamThread: /delete s->imageThreadLock" << endl;
	}

	s->imageThreadLock=new Mutex();

	s->lastAccessToFB=new Timer();
	
	s->modifLock.unlock();
	
	s->lastAccessToFB->start();
//	sessions[a->toString()]=*s;
	
	// Iniciem el thread amb una serie de params
	EquitelImagePluginStreamThread::imageThreadArgs *ita=new EquitelImagePluginStreamThread::imageThreadArgs();
	
	ita->s=s;
	ita->a=*a;
	ita->plugin=this;
	ita->timeoutTimer=s->lastAccessToFB;
	
	s->threadId=streamThread.start(ita);

	cout << "EquitelImagePlugin::createStreamThread() thId: " << s->threadId << endl;

//	sessions[a->toString()]=*s;
}




EquitelImagePlugin::EquitelImagePlugin(Address addr) : PluginImage(addr, "ImagePlugin::SONY"),
	centralDirectoryAddr(Address(IP("0.0.0.0"), 0))
{
	avcodec_register_all();
	av_register_all();
	avcodec_register_all();
	cout<<"registered codecs"<<endl;
}

EquitelImagePlugin::EquitelImagePlugin(string file) : configFile(file), PluginImage(file),
	centralDirectoryAddr(Address(IP("0.0.0.0"), 0)), streamThread(file)
{
	avcodec_register_all();
	av_register_all();
	avcodec_register_all();
 cout<<"registered codecs"<<endl;
}

 

IPDeviceInfo EquitelImagePlugin::getDeviceInformation(IPDeviceInfo dev, Address *a)
{
//	cout << "--> EquitelImagePlugin::getDeviceInformation()" << endl;
	string query=string("SELECT d.id, f.nombre, m.codigo FROM dispositivo d, fabricante f, modelo m WHERE d.id='")+
		StrUtils::decToString(dev.id.id)+
		string("' AND d.fabricante=m.fabricante AND d.modelo=m.id AND m.fabricante=f.id AND f.nombre='Equitel'");
	RPCPacket bdQueryPk(this->address, 0, (byte*)query.c_str(), query.length(), this->type, -2);

//	cout << "bdQuery packet assembled" << endl;
	
	// Trobem la BD
	RPC *rpc=EquitelImagePlugin::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn);
		rpc=EquitelImagePlugin::getRPC(bd->a);
		delete bd;
	}

//	cout << "Sending keepAlive to " << a->toString() << endl;
	RPC::keepAlive(a, this->type);
				
	RPCPacket *result=rpc->call(bdQueryPk);
	EquitelImagePlugin::ptrPool.release(rpc);

//	cout << "Sending keepAlive to " << a->toString() << endl;
	RPC::keepAlive(a, this->type);

//	cout << "RPC call done" << endl;

	string resXML((char*)result->getData(), (size_t)result->getSize());
	delete result;
	
//	cout << "Response: " << endl << resXML << endl;
	
	XML *xml=xmlParser::parse(resXML);

//	cout << "XML parsed" << endl;

	xmlNode *row=xml->getNode("/result/[0]");
	
	if (row==NULL)
	{
		cout <<  "Device with specified ID " << StrUtils::decToString(dev.id.id) << " does not exist or it is not an Equitel device:"<<xml->toString()<<endl;
		delete xml;
		throw (IPInvalidParamException(0, string("Device with specified ID ") + StrUtils::decToString(dev.id.id) + string("does not exist or it is not an Equitel device")));
	}

//	cout << "Creating IPDeviceInfo" << endl;
	
	// TODO afegir resolucions i codecs suportats
	IPDeviceInfo di(atoi(xml->getNode("/result/[0]/id")->getCdata().c_str()),
					xml->getNode("/result/[0]/nombre")->getCdata(),
					xml->getNode("/result/[0]/codigo")->getCdata(),
					IPFrameInfoSeq(), IPCodecInfoSeq());

	delete xml;

//	cout << "<-- EquitelImagePlugin::getDeviceInformation()" << endl;
	return di;
}

void EquitelImagePlugin::startSession(IPDeviceID id, Address *a)
{
	// cout  << "--> EquitelImagePlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;

	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes!=sessions.end())
	{
		throw (IPSessionAlreadyStablishedException(0, string("Session already stablished for device ")+
														StrUtils::decToString(id.id)+string(" from client ")+
														a->toString()));
	}
	
	sessionInfo s;
	memset(&s, 0, sizeof(s));
	s.devInfo.id=id.id;

	// cout  << "local call" << endl;
	s.devInfo=this->getDeviceInformation(s.devInfo, a);
	
	if (s.devInfo.make!="Equitel")
	{
		throw (IPInvalidParamException(0, "Device with specified ID is not an emulated device"));
	}

	

	streamInfo *sInfo=getStreamInfoForDevice(s.devInfo.id.id);
	
	if (sInfo==NULL)
	{
		sInfo=new streamInfo();

		sInfo->devInfo=s.devInfo;
		sInfo->fps.whole=25;
		sInfo->fps.frac=0;
		sInfo->cn=this->cn;
			
		// Model, a lo guarro q es lo q toca :P
		if (s.devInfo.model==string("C104T") || s.devInfo.model==string("C104-T")) 
		{
			sInfo->model=C104T;
			s.devInfo.model=string("C104-T");
		}
		else
		{
			sInfo->model=C104T;
			s.devInfo.model=string("C104-T");
			//throw (IPInvalidParamException(0, "Unknown camera model for specified Camera ID"));
		}

		sInfo->imageThreadLock=new Mutex();
		if (sInfo->imageThreadLock==NULL)
		{
			throw (ImagePluginException(0, "Not enough memory to allocate image thread mutex"));
		}	
		
		// cout  << "Querying DB to find device " << endl;
		string query=string("SELECT host(d.ip) AS ip, d.puerto FROM dispositivo d, modelo m, fabricante f WHERE d.id='")+
			StrUtils::decToString(s.devInfo.id.id)+
			string("' AND d.modelo=m.id AND d.fabricante=m.fabricante AND m.fabricante=f.id AND f.nombre='Equitel'");
		RPCPacket bdQueryPk(this->address, 0, (byte*)query.c_str(), query.length(), this->type, 
							ServiceFinder::dbGatewayTypeId);
		// Trobem la BD
		RPC *rpc=EquitelImagePlugin::getRPC(NULL);
		
		if (rpc==NULL)
		{	
			RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn);
			rpc=EquitelImagePlugin::getRPC(bd->a);
			delete bd;
		}
	
		RPCPacket *result=rpc->call(bdQueryPk);
		EquitelImagePlugin::ptrPool.release(rpc);
		
		string resXML((char*)result->getData(), (size_t)result->getSize());
		delete result;
		
		// cout  << "XML result " << endl << resXML << endl;
		
		XML *xml=xmlParser::parse(resXML);
	
		xmlNode *row=xml->getNode("/result/[0]");
		
		if (row==NULL)
		{
			delete xml;
	//		delete [] s.fb;
	//		delete [] s.fbLengths;
	//		delete [] s.fbKeyFrames;
			throw (IPInvalidParamException(0, "Device with specified ID does not exist"));
		}
		
	
		sInfo->ip=xml->getNode("/result/[0]/ip")->getCdata();
		sInfo->port=atoi(xml->getNode("/result/[0]/puerto")->getCdata().c_str());
	
		delete xml;

		cout<<"EquitelImagePlugin:: call "<<(void*)sInfo<<endl;

		XML *config=NULL;
		{
			cout<<"EquitelImagePlugin:: read file :"<<this->getConfigFileName()<<endl;
			FILE *f=fopen(this->getConfigFileName().c_str(),"rb");

			if (f==NULL)
				throw FileException(string("File ")+this->getConfigFileName()+string(" not found"));
			cout<<"EquitelImagePlugin:: len"<<endl;

			fseek(f,0,SEEK_END);
			int len=ftell(f);
			fseek(f,0,SEEK_SET);
			cout<<"EquitelImagePlugin:: len :"<<len<<endl;

			char *buf=new char[len+1];
			fread(buf,len,1,f);
			buf[len]='\0';
			fclose(f);
			cout<<"EquitelImagePlugin:: parse:"<<endl;

			config=xmlParser::parse(string(buf, len));
		}
	
		cout<<"EquitelImagePlugin:: data:"<<endl;
/*		for(int i=0;;i++)
		{
			string path=string("/[0]/Cameras/[")+StrUtils::decToString(i)+string("]");
				
			if(config->getNode(path)==NULL)
				throw (IPInvalidParamException(0, "Device with specified ID does not have cameras"));

			xmlNode *n=config->getNode(path+string("/id"));
			int camId=atoi(n->getCdata().c_str());
			cout<<"EquitelImagePlugin:: camera ID:"<<camId<<" startSession:"<<id.id<<endl;
			if(camId==id.id)
			{
				n=config->getNode(path+string("/recordingID"));
				sInfo->recording.id=atoi(n->getCdata().c_str());
				n=config->getNode(path+string("/start"));
				sInfo->recording.start=atol(n->getCdata().c_str());
				n=config->getNode(path+string("/length"));
				sInfo->recording.length=atoi(n->getCdata().c_str());
				break;
			}
		}
*/		sInfo->streamingMode=IPStreamingMode(IPStreamingMode::STREAM);


		cout<<"EquitelImagePlugin:: save:"<<endl;
		streamsLock.lock();
		streams[s.devInfo.id.id]=sInfo;
		streamsLock.unlock();
	}

	s.stream=sInfo;

	sessionLock.lock();
	sessions[a->toString()]=s;
	sessionLock.unlock();

/*	sInfo->codec=getCodecInUse(a);
	ControlModuleAccess cma(getConfigFileName());
	cma.startSession(s.devInfo.id);
		string cmCodec=cma.getConfigParam(string("/Video/1/Codec/Fourcc"));
		if(cmCodec.size()>=4)
		{
			const char *c=cmCodec.c_str();
			sInfo->codec.fourcc=CODEC_FOURCC(c[0],c[1],c[2],c[3]);
		}
//		else
//		{
//			sInfo->codec=getCodecInUse(a);
//		}
		cma.endSession(s.devInfo.id);
*/	
	// cout  << "<-- EquitelImagePlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;
}

void EquitelImagePlugin::endSession(IPDeviceID id, Address *a)
{
	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
	{
		throw (IPSessionNotStablishedException(0, string("Session not stablished for device ")+
														StrUtils::decToString(id.id)+string(" from client ")+
														a->toString()));
	}
//	sessionInfo s=(*itSes).second;
//	if (s.stream->streamingMode==STREAM)
//	{
//		s.stream->stopThread=true;
//		cout << "locloclocloc" << endl;
//		s.stream->imageThreadLock->lock();
//		cout << "/locloclocloc" << endl;
//		sessions[a->toString()]=s;
//		cout << "unlocunlocunlocunloc" << endl;
//		s.stream->imageThreadLock->unlock();
//		cout << "/unlocunlocunlocunloc" << endl;
//		itSes=sessions.find(a->toString());
////		cout << "joining " << (*itSes).second.threadId << endl;
//		s=(*itSes).second;
//		
//		streamThread.join(s.threadId);
//		switch (s.codec.fourcc)
//		{
//			case CODEC_JPEG:
//				jpegThread.join(s.threadId);
//				break;
//		
//			case CODEC_MPEG4:
//				mpeg4Thread.join(s.threadId);
//				break;
//		}
////		cout << "joined" << endl;
//	}

	sessions.erase(itSes);
}

IPCodecInfo EquitelImagePlugin::getCodecInUse(Address *a)
{
	sessionInfo s=getSessionForAddress(a);
	
	return IPCodecInfo(CODEC_JPEG, 10, 0);
}

void EquitelImagePlugin::setCodecInUse(IPCodecInfo codec, Address *a)
{
	sessionInfo s=getSessionForAddress(a);
	
/*	if (s.stream->streamingMode==STREAM)
	{
		throw (ImagePluginException(0, "Codec can not be changed while in STREAM mode"));
	}
*/		
	throw ImagePluginException(0,"Error: unable to change codec in use");
}

IPFrameBufferSize EquitelImagePlugin::getFrameBufferSize(Address *a)
{
	cout<<"GFBS"<<endl;
	sessionInfo s=getSessionForAddress(a);

	IPFrameBufferSize fbSize;
	fbSize.nFrames=1; //s.fbSize;


	cout<<"/GFBS"<<endl;
	return fbSize;
}

void EquitelImagePlugin::setFrameBufferSize(IPFrameBufferSize size, Address *a)
{
//	sessionInfo s=getSessionForAddress(a);
//
////	cout << "--> EquitelImagePlugin::setFrameBufferSize(" << size.nFrames << ")" << endl;
//
//	byte **fb=new byte*[size.nFrames];
//	dword *fbL=new dword[size.nFrames];
//	bool *fbK=new bool[size.nFrames];
//	
//	if (fb==NULL)
//	{
//		throw (ImagePluginException(0, "Not enough memory to allocate frame buffer"));
//	}
//	
//	memset(fb, 0, sizeof(byte*)*size.nFrames);
//	memset(fbL, 0, sizeof(dword)*size.nFrames);
//	memset(fbK, false, sizeof(bool)*size.nFrames);
//	
//	int i=0;
//
//	for (; i<size.nFrames && i<s.fbSize; i++)
//	{
//		fb[i]=s.fb[i];
//		fbL[i]=s.fbLengths[i];
//		fbK[i]=s.fbKeyFrames[i];
//	}
//
//	// Nomes s'executara un dels dos seguents bucles
//	int j=i;
//	for (; i<size.nFrames; i++)
//	{
//		fb[i]=new byte[1]; //s.frameInfo.x*s.frameInfo.y*s.frameInfo.bpp/8];
//		if (fb[i]==NULL)
//		{
//			for (int k=j; k<i; k++)
//				delete [] fb[k];
//				
//			delete [] fb;
//			delete [] fbL;
//			throw (ImagePluginException(0, "Not enough memory to allocate frame buffer frame"));
//		}
//	}
//
//	sessionLock.lock();
//	for (; i<s.fbSize; i++)
//		if (s.fb[i]!=NULL)
//			delete [] s.fb[i];
//
//	delete [] s.fb;
//	delete [] s.fbLengths;
//	delete [] s.fbKeyFrames;
//	s.fbSize=size.nFrames;
//	s.fb=fb;
//	s.fbLengths=fbL;
//	s.fbKeyFrames=fbK;
//
//	sessions[a->toString()]=s;
//	sessionLock.unlock();


}

float EquitelImagePlugin::getFrameBufferPercentInUse(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	return 0.0f; //(float)s.fbUse/(float)s.fbSize;
}

int EquitelImagePlugin::getFrameBufferFramesInUse(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	return 0; //s.fbUse;
}

IPStreamingMode EquitelImagePlugin::getStreamingMode(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	return s.stream->streamingMode;
}

void EquitelImagePlugin::setStreamingMode(IPStreamingMode mode, Address *a)
{
	sessionInfo s=getSessionForAddress(a);
	

	//cout << "SetStreamingMode: " << mode << ", id: " << s.devInfo.id << endl;
	if(s.stream->streamingMode.mode!=IPStreamingMode::STREAM)
		throw (ImagePluginException(0, string("Invalid streaming mode: ")+StrUtils::decToString(s.stream->streamingMode.mode)));
//	s.stream->modifLock.lock();
//	s.stream->streamingMode=mode;
//	s.stream->modifLock.unlock();
	
}

IPFrame EquitelImagePlugin::getCompressedNextFrame(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	IPCodecInfo codecInfo;
	codecInfo.fourcc= CODEC_MPEG4;//CODEC_JPEG;//s.stream->codec; //getCodecInUse(a);
	codecInfo.quality=5;
//	cout << "EquitelImagePlugin::getCompressedNextFrame() ini" << endl;

			// Com que accedim al frame buffer, comptem l'acces
//			cout << "EquitelImagePlugin::getCompressedNextFrame()  Stream + "<< string((char*)&codecInfo.fourcc, sizeof(codecInfo.fourcc)) << endl;
			s.stream->modifLock.lock();
			if (s.stream->lastAccessToFB!=NULL)		// Thread en marxa
			{
				try{
					s.stream->lastAccessToFB->start();
				}catch(...){}
			}
			s.stream->modifLock.unlock();

			if (s.stream->threadId==-1)		// El thread s'ha parat
			{
//				s.fbUse=0;
//				cout << "EquitelImagePlugin::getCompressedNextFrame()  Thread stopped" << endl;
				createStreamThread(s.stream, a);
			}

//			if (codecInfo.fourcc == CODEC_JPEG)
			{
//				cout << "EquitelImagePlugin::getCompressedNextFrame()  Stream JPEG " << endl;

//				while (s.stream->nframes==0)
				while(s.stream->nframes==0 || (s.stream->keyFrame==s.lastFrame && s.frameIndex==s.stream->nframes))
				{
					// Esperem "1/2" frame (@25fps)
					//				cout << " -> frame buffer empty" << endl;
					usleep(20000);
					//				s=getSessionForAddress(a);
				}

				s.stream->modifLock.lock();

				bool isKey=true;//s.stream->isKey; //fbKeyFrames[0];
				byte *buf;
				int len=0;;
				if(s.stream->keyFrame==s.lastFrame) 
				{	//ja havia rebut part del frame, enviem la resta
//					cout << " **** SonyImagePlugin::getCompressedNextFrame() s.stream->keyFrame==s.lastFrame partial frame : "<<s.stream->keyFrame<<"=="<<s.lastFrame <<" frames:"<<s.stream->nframes<<" part:"<<s.frameIndex<<endl;
					int begin=0;
					int dataLen=0;
					int *header=((int*)s.stream->frameStreamHeader.c_str());
					for(int i=0; i<s.stream->nframes; i++)
					{
						if(i<s.frameIndex)
							begin += *(header+i);
						else
							dataLen += *(header+i);
					}
					int sendFrames = (s.stream->nframes - s.frameIndex);
					len=dataLen+sizeof(int)+sendFrames*sizeof(int);
					buf=new byte[len];
					isKey=false;
					
					*((unsigned int*)buf) = sendFrames;
					memmove(buf+sizeof(int), 
							s.stream->frameStreamHeader.c_str() + s.frameIndex*sizeof(int) ,
							sendFrames*sizeof(int));
					memmove(buf+sizeof(int)+sendFrames*sizeof(int),
							s.stream->streamFrames.c_str() + begin,
							dataLen);

					s.frameIndex=s.stream->nframes;
				}
				else
				{	//cal enviar tot el frame
//					cout << "SonyImagePlugin::getCompressedNextFrame() s.stream->keyFrame!=s.lastFrame full      frame : "<<s.stream->keyFrame<<"!="<<s.lastFrame <<" frames:"<<s.stream->nframes<<" part:"<<s.frameIndex<<endl;
					len=s.stream->frameStreamHeader.length()+s.stream->streamFrames.length()+sizeof(int);
					buf=new byte[len];
					isKey=true;
					
					*((unsigned int*)buf)=s.stream->nframes;
		//			cout<<"getFrame header:"<<StrUtils::hexDump(s.stream->frameStreamHeader)<<endl;
					memmove(buf+sizeof(int), s.stream->frameStreamHeader.c_str(), s.stream->frameStreamHeader.length());
					memmove(buf+sizeof(int)+s.stream->frameStreamHeader.length(), s.stream->streamFrames.c_str(), s.stream->streamFrames.length());

					s.lastFrame=s.stream->keyFrame;
					s.frameIndex=s.stream->nframes;
				}
//*****//

//			cout << "Frame read from buffer 0, size " << len << " bytes" << endl;

				{ // single client :P
//					s.stream->nframes=0;
//					s.stream->frameStreamHeader=string("");
//					s.stream->streamFrames=string("");
				}

				sessions[a->toString()]=s;
//				sessions.find(a->toString())->second=s;;
//				sessionInfo s=getSessionForAddress(a);
//				cout<< "			-- gcnf message  size:"<< len<<endl;


				s.stream->modifLock.unlock();
//				cout<<"-- getCNF "<<len<<StrUtils::hexDump(string((char*)buf, len))<<endl;

//				cout << "IPFrameLen " << len << endl;
				return IPFrame(s.stream->frameInfo, codecInfo, isKey, len, buf);
			}
	throw (ImagePluginException(0, string("Invalid streaming mode: ")+StrUtils::decToString(s.stream->streamingMode.mode)));
}

IPFrame EquitelImagePlugin::getDecompressedNextFrame(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	throw(ImagePluginException(-1, "Not implemented"));
}

IPFramesPerSecond EquitelImagePlugin::getFPS(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	s.stream->modifLock.lock();
	IPFramesPerSecond fps(s.stream->fps.whole, s.stream->fps.frac);
	s.stream->modifLock.unlock();
	
	return fps;
}

void EquitelImagePlugin::setFPS(IPFramesPerSecond fps, Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	s.stream->modifLock.lock();

//	cout << "setFPS: current fps: " << s.stream->fps.whole << ", requested: " << fps.whole << endl;

	// Canviem si estem augmentant els FPS o si es el primer canvi.
	if (s.stream->stillDefaultFPS || fps.whole>s.stream->fps.whole || (fps.whole==s.stream->fps.whole && fps.frac>s.stream->fps.frac))
	{
		s.stream->fps.whole=fps.whole;
		s.stream->fps.frac=fps.frac;
	
		s.stream->stillDefaultFPS=false;
		s.stream->fpsChanged=true;
	}

	s.stream->modifLock.unlock();
}

string EquitelImagePlugin::getConfigFileName()
{
	return configFile;
}

