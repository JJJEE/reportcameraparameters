/*
 *  RecordingCameraPlugin.cpp
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "RecordingCameraPlugin.h"
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <Utils/Canis.h>
#include <Utils/StrUtils.h>
#include <Plugins/GestorImageExceptions.h>
#include <Http/HttpClient.h>
#include <Http/HttpStream.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <ServiceFinder/ServiceFinder.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <math.h>
#ifndef WIN32
#ifndef WIN32
#include <unistd.h>
#endif
#else
extern "C" {
#include <jpeglib.h>
}
#include <Utils/WindowsDefs.h>
#include <Utils/WindowsInclude.h>
#include "sample/sample06/resource.h"
#endif

extern struct timeval operator + (struct timeval a, struct timeval b);

#pragma mark *** Pool

PtrPool RecordingCameraPlugin::ptrPool;

RPC* RecordingCameraPlugin::getRPC(Address *a)
{
	RPC *rpc=(RPC*)ptrPool.get();
	
	if (rpc==NULL && a!=NULL)
	{
		rpc=new RPC(*a);
		if (rpc==NULL)
		{
			throw (Exception("FATAL: Not enough memory"));
		}
		ptrPool.add(rpc, true);
	}
	
	return rpc;
}

#pragma mark *** Privades

const IPFrameInfo RecordingCameraPlugin::resolutions[RecordingCameraPlugin::NUM_VIDEOSTD][RecordingCameraPlugin::NUM_RESOLUTIONS]={
		{IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)},
		{IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)}};


RecordingCameraPlugin::sessionInfo RecordingCameraPlugin::getSessionForAddress(Address *a)
{
	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
		throw (IPSessionNotStablishedException(0, string("Session not stablished with any device ")+
												   string("from client ")+a->toString()));
	
	return (*itSes).second;
}

RecordingCameraPlugin::streamInfo* RecordingCameraPlugin::getStreamInfoForDevice(int id)
{
	map<IPDeviceID,streamInfo*>::iterator itStr=streams.find(id);
	
	if (itStr==streams.end())
		return NULL;
	
	return itStr->second;
}

#pragma mark *** Threads de recepció imatges
void RecordingCameraPluginStreamThread::starRecordingtSession(RecordingModuleAccess *rma, int devId)
{
	IPCodecInfo codec;
//	dma.startSession(devId);
//	IPCodecInfo codec=dma.getCodecInUse();
//	dma.endSession(devId);

	cout<<"RecordingCameraPluginStreamThread::starRecordingtSession"<<endl;
	RMStartSessionParams ssParams;
	ssParams.devId=devId;
//	ssParams.date=new RecordingFileDateMetadataChunk();
//	ssParams.date->setCurrentDate();
	ssParams.recType=RecordingFileHeaderChunk::REC_HOWMANY;//SCHEDULED;
	ssParams.fpswhole=25;
	ssParams.fpsfrac=0;//mili fps

	try
	{
		rma->startSession(ssParams);
	}
	catch (Exception &e)
	{
		cout <<"Error:SCPSchedulingThread::startSession: "<< e.getClass() << ": " << e.getMsg() << endl;
		e.serialize()->materializeAndThrow();
	}

	cout<<"RecordingCameraPluginStreamThread::/starRecordingtSession"<<endl;
}

void* RecordingCameraPluginStreamThread::execute(int id, void *args)
{
	RecordingCameraPluginStreamThread::imageThreadArgs *ita=(RecordingCameraPluginStreamThread::imageThreadArgs *)args;
	RecordingCameraPlugin *plugin=(RecordingCameraPlugin *)ita->plugin;

	RecordingCameraPlugin::streamInfo *s=(RecordingCameraPlugin::streamInfo*)ita->s;

	cout<<"RecordingCameraPluginStreamThread::execute"<<endl;
	string puText("");
	bool popupText=false;
	bool popupTextSent=false;
	
	// Comencem a agafar excepcions des d'aqui, pq pot petar el tema control module :P
	ControlModuleAccess *controlModule=NULL;
	RecordingModuleAccess rma(plugin->cn);
	try
	{
		try
		{	
			//	cout << "RecordingCameraPluginStreamThread create CMA: " << plugin->getConfigFileName() << endl;
			RMStartSessionParams ssParams;
			ssParams.devId=s->recording.id;//devInfo.id;
			ssParams.date=new RecordingFileDateMetadataChunk();
			ssParams.date->setCurrentDate();
			//ssParams.date->setDate(s->recording.start, 0);
			ssParams.recType=RecordingFileHeaderChunk::REC_HOWMANY;
				ssParams.fourcc=0;
				ssParams.fpswhole=25;
				ssParams.fpsfrac=0;//mili fps
				ssParams.isRecoded=false;
				ssParams.isCyclic=false;

			//		ssParams.id=s->recording.id;//devInfo.id;
			//ssParams.date=new RecordingFileDateMetadataChunk();

			cout<<"RecordingCameraPluginStreamThread::execute rma.startSession "<<ssParams.devId<<endl;
			rma.startSession(ssParams);
			cout<<"RecordingCameraPluginStreamThread::execute started"<<endl;
		}
		catch (Exception &e)
		{
			cout <<"Error:SCPSchedulingThread::startSession: "<< e.getClass() << ": " << e.getMsg() << endl;
			//delete buf;		
			e.serialize()->materializeAndThrow();
		}

//		s->frameInfo.x=0;
//		s->frameInfo.y=0;
//		s->frameInfo.fourCC=0;

		Timer t;

		t.start();

		while (true)
		{
			// UPDATE HTTP SI HAN CANVIAT ELS FPS
			//			if (s->fpsChanged)
			//			{}

			s->imageThreadLock->lock();

			//		cout << "RecordingCameraPluginStreamThread::execute() 3" << endl;

			//	cout << "[JPEG] Read frame Header (len " << frameHdr.length() << "): " << frameHdr <<" -- "<< endl;

			TimerInstant inst=t.time();

			struct timeval spent;
			spent.tv_sec=(int)floor(inst.seconds());
			spent.tv_usec=inst.useconds()%1000000;

			struct timeval frameTime;
			//			frameTime.tv_sec=s->recording.startDate.secs;
			//			frameTime.tv_usec=s->recording.startDate.millis*1000;
			frameTime.tv_sec=s->recording.start;
			frameTime.tv_usec=0;
//			cout<<" frameTime:"<<frameTime.tv_sec<<" "<<frameTime.tv_usec<<endl;
			spent.tv_sec = spent.tv_sec % s->recording.length;
			frameTime.tv_sec = frameTime.tv_sec+spent.tv_sec;
			frameTime.tv_usec = frameTime.tv_usec+spent.tv_usec;
			if (frameTime.tv_usec>1000000)
			{
				frameTime.tv_sec++;
				frameTime.tv_usec-=1000000;
			}
//			cout<<" spent:"<<spent.tv_sec<<" "<<spent.tv_usec<<endl;
//			cout<<" frameTime:"<<frameTime.tv_sec<<" "<<frameTime.tv_usec<<endl;

			struct RMReadFrameParams rfp;
			rfp.chunk=new RecordingFileFrameChunk();
			RecordingFileDateMetadataChunk *dataChunk=new RecordingFileDateMetadataChunk();
			dataChunk->setDate(frameTime.tv_sec, frameTime.tv_usec/1000);
			rfp.chunk->delSubChunk((dword)0);
			rfp.chunk->addSubChunk(dataChunk);

/*			cout<<"RecordingCameraPluginStreamThread::execute rma.readFrame "<<(void*)dataChunk<<endl;
	cout<<StrUtils::decToString(dataChunk->getHumanReadableYear())<<endl;
	cout<<StrUtils::decToString(dataChunk->getHumanReadableMonth())<<endl;
	cout<<StrUtils::decToString(dataChunk->getHumanReadableDay())<<endl;
	cout<<StrUtils::decToString(dataChunk->getHumanReadableHour())<<endl;
	cout<<StrUtils::decToString(dataChunk->getHumanReadableMinute())<<endl;
	cout<<StrUtils::decToString(dataChunk->getHumanReadableSecond())<<endl;
	cout<<StrUtils::decToString(dataChunk->getHumanReadableMillisecond())<<endl;
*/			RecordingFileFrameChunk* res=rma.readFrame(rfp);
//			cout<<"RecordingCameraPluginStreamThread::execute /read"<<endl;


			s->modifLock.lock();

			//	TimerInstant ti=ita->timeoutTimer->time();
			TimerInstant ti=ita->timeoutTimer->time();
			if (ti.seconds() > s->streamTimeout)
			{
				// Evitem l'aparicio d'N errors ara que encara no ho gestionem
				//			ita->timeoutTimer->start(); // TODO: Eliminar aquesta linia quan es gestioni el tema dels timeouts

				cout << "RecordingCameraPluginStreamThread::execute(): Thread Timeout occurred, stream is not being used!" << endl;
				s->stopThread=true;
//			}
//
//			if (s->stopThread)
//			{
				cout << "RecordingCameraPluginStreamThread::execute(): Stopping thread..." << endl;
				// Si estem parant per timeout, cal deixar-ho tot llest pq el proxim get(De)compressedNextFrame el pugui iniciar

				//				if (ti.seconds() > s->streamTimeout)
				//				{
				s->stopThread=false;
				delete s->lastAccessToFB;
				s->lastAccessToFB=NULL;
				s->threadId=-1;
				//				}


				s->modifLock.unlock();
				s->imageThreadLock->unlock();

				delete ita;
				delete controlModule;

				cout << "RecordingCameraPluginStreamThread::execute(): Stopped..." << endl;
				return NULL;
			}

			dword len=res->getFrameSize();//frame.length();

//			cout<<" set frame:"<<len<<endl;
			byte *newFrame=new byte[len];
			memmove(newFrame, res->getFrame(), len);

			byte *f=s->lastFrame;
			s->lastFrameLen=len;
			s->lastFrame=newFrame;

			s->modifLock.unlock();

			if (f!=NULL)
			{
				delete [] f;
			}

			if (popupTextSent)
			{
				popupTextSent=false;
				//			try
				//			{
				//				Fem el guarro i sudem de la resposta ;)
				controlModule->ignoreAsyncResult();
				//			}
				//			catch (Exception &e)
				//			{
				//				cout << "controlModule->getAsyncResult: " << e.getClass() 
				//					<< ":" << e.getMsg() << endl;
				//			}
			}

			s->imageThreadLock->unlock();
				usleep(40000);
		}
	}
	catch (Exception &e)
	{
		// Simulem un timeout....
		cout << "RecordingCameraPluginStreamThread::execute(): Exception: " << e.getClass() << ": " << e.getMsg() << endl;
			cout << "RecordingCameraPluginStreamThread::execute(): Stopping thread..." << endl;

		if (!s->modifLock.isHeldByCurrentThread())
			s->modifLock.lock();
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		s->modifLock.unlock();
		if (s->imageThreadLock->isHeldByCurrentThread())
			s->imageThreadLock->unlock();

		if (popupTextSent)
		{
			popupTextSent=false;
//				try
//				{
//				Fem el guarro i sudem de la resposta ;)
				controlModule->ignoreAsyncResult();
//				}
//				catch (Exception &e)
//				{
//					cout << "controlModule->getAsyncResult: " << e.getClass() 
//						<< ":" << e.getMsg() << endl;
//				}
		}
//			cout << "RecordingCameraPluginStreamThread::execute() 6" << endl;
//			cout << "Stopping thread" << endl;
		delete ita;
		delete controlModule;
		
		return NULL;
	}
	catch (...)
	{
		// Simulem un timeout....
		cout << "RecordingCameraPluginStreamThread::execute(): unknown exception" << endl;
			cout << "RecordingCameraPluginStreamThread::execute(): Stopping thread..." << endl;

		if (!s->modifLock.isHeldByCurrentThread())
			s->modifLock.lock();
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		s->modifLock.unlock();
		if (s->imageThreadLock->isHeldByCurrentThread())
			s->imageThreadLock->unlock();

		if (popupTextSent)
		{
			popupTextSent=false;
//				try
//				{
//				Fem el guarro i sudem de la resposta ;)
				controlModule->ignoreAsyncResult();
//				}
//				catch (Exception &e)
//				{
//					cout << "controlModule->getAsyncResult: " << e.getClass() 
//						<< ":" << e.getMsg() << endl;
//				}
		}
//			cout << "RecordingCameraPluginStreamThread::execute() 6" << endl;
//			cout << "Stopping thread" << endl;
		delete ita;
		delete controlModule;
		
		return NULL;
	}

//	cout << "RecordingCameraPluginStreamThread::execute() 9" << endl;
	cout << "RecordingCameraPluginStreamThread finishing..." << endl;

		if (!s->modifLock.isHeldByCurrentThread())
			s->modifLock.lock();
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		s->modifLock.unlock();
		if (s->imageThreadLock->isHeldByCurrentThread())
			s->imageThreadLock->unlock();

	delete ita;
	delete controlModule;
	return NULL;
}


/*
#ifdef WIN32

class waitInfo
{
public:
	Condition c;
	bool finished;

	waitInfo():finished(false){}
};

void init_destination(j_compress_ptr cinfo){}
boolean empty_output_buffer(j_compress_ptr cinfo){return true;}
void term_destination(j_compress_ptr cinfo){
	waitInfo *w=(waitInfo*)cinfo->client_data;
	w->c.lock();
	w->finished=true;
	w->c.signal();
	w->c.unlock();

}

void error_exit (j_common_ptr cinfo)
{
	cout<<"JPEGLIB fatal error"<<endl;
}
void format_message (j_common_ptr cinfo, char * buffer) 
{
	cout<<"JPEGLIB format error"<<endl;
}
void output_message (j_common_ptr cinfo) 
{
	cout<<"JPEGLIB output msg"<<endl;
}
void emit_message (j_common_ptr cinfo, int msg_level) 
{
	cout<<"JPEGLIB emit msg"<<endl;
}


extern LRESULT CALLBACK DialogSample06( HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam );
RecordingCameraPlugin::streamInfo *s;

// globals win
extern HWND screenWindowHandle;
extern HWND WindowHandle;
extern HINSTANCE instanceHandle;

void RecordingCameraPlugin::setFrame(BITMAPINFO bmpInfo, char* bmp)
{
/ *	bmpInfo.bmiHeader.biSize = sizeof( bmpInfo.bmiHeader );
	bmpInfo.bmiHeader.biWidth = pVideoInfo->VideoCodecInfo.Width;
	bmpInfo.bmiHeader.biHeight = pVideoInfo->VideoCodecInfo.Height;
	bmpInfo.bmiHeader.biPlanes = 1;
	bmpInfo.bmiHeader.biBitCount = 24;
	bmpInfo.bmiHeader.biCompression = BI_RGB;
*/
/*
	int Width = bmpInfo.bmiHeader.biWidth;
	int Height = bmpInfo.bmiHeader.biHeight;

	int bmpSize= Width*Height*bmpInfo.bmiHeader.biBitCount/8;//bmpInfo.bmiHeader.biSizeImage;
//	LPVOID lpvBits = new char[bmpSize];//un cop temin el tamany, alocatem i torném a cridar

//	byte *jpeg=new byte[bmpSize];
	if(s->lastFrame==NULL)
	{
		s->lastFrame=new byte[bmpSize];
	}
	byte *jpeg=s->lastFrame;
	s->modifLock.lock();

	try{

		waitInfo w;
	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;
	struct jpeg_destination_mgr dst;
	memset(&cinfo, 0, sizeof(cinfo));
	memset(&jerr, 0, sizeof(jerr));
	memset(&dst, 0, sizeof(dst));
	memset(jpeg, 0, sizeof(bmpSize));

	cinfo.err = jpeg_std_error(&jerr);
	cinfo.err->error_exit = error_exit;
	cinfo.err->format_message = format_message;
	cinfo.err->output_message = output_message;
	cinfo.err->emit_message = emit_message;

	jpeg_create_compress(&cinfo);  

	cinfo.dest=&dst;
	cinfo.dest->empty_output_buffer = &empty_output_buffer;
	cinfo.dest->init_destination = &init_destination;
	cinfo.dest->term_destination = &term_destination;
	cinfo.dest->next_output_byte=(JOCTET*)jpeg;
	cinfo.dest->free_in_buffer=bmpSize;


	cinfo.image_width = Width; 	
	cinfo.image_height = Height;
	cinfo.input_components = 3;	
	cinfo.in_color_space = JCS_RGB; 
	cinfo.client_data=&w;
	jpeg_set_defaults(&cinfo);

	//---- reorganització win
	//	char first_byte = bmp[0];
/ *	for (int posy = 0; posy < Height; posy++)
	{
		int i1 = 0, i2 = 02 ;
		for (int posx = 0; posx < Width; posx++) {
			bmp[posy*Width+i1++] = bmp[posy*Width+i2+0];        // Red
			bmp[posy*Width+i1++] = bmp[posy*Width+i2+1];        // Green
			bmp[posy*Width+i1++] = bmp[posy*Width+i2+2]; i2 += 4; // Blue
		}
	}*/
/*	for (int posy = 0; posy < Height; posy++)
	{
		int i1 = 0, i2 = 0 ;
		for (int posx = 0; posx < Width; posx++) {
			int b=bmp[posy*Width*3+i1];
			int g=bmp[posy*Width*3+i1+1];
			int r=bmp[posy*Width*3+i1+2];
			bmp[posy*Width*3+i1]   = r;
			bmp[posy*Width*3+i1+1] = g;
			bmp[posy*Width*3+i1+2] = b;
			i1+=3;i2 +=3;
		}
	}


	//---- compressio jpeg
	jpeg_start_compress(&cinfo,true);
	for(int i=0;i<Height;i++)
	//for(int i=Height-1;i>=0;i--)
	{
//		JSAMPROW row=(JSAMPROW)(( sDesc.lpSurface)+(i*(Width*sDesc.lPitch)));
		JSAMPROW row=(JSAMPROW)(( bmp)+(i*(Width*3)));
		jpeg_write_scanlines(&cinfo, &row,1);
	}

	jpeg_finish_compress(&cinfo);

	w.c.lock();
	while(!w.finished)
		w.c.wait();
	w.c.unlock();


	//	cout << "[JPEG] Read frame Header (len " << frameHdr.length() << "): " << frameHdr <<" -- "<< endl;
	dword len=(bmpSize)-(cinfo.dest->free_in_buffer);//lpbi->bmiHeader.biSizeImage;
		s->lastFrameLen=len;

//	delete cinfo->dest;
//	cinfo->dest=NULL;
	jpeg_destroy_compress(&cinfo);
//	delete cinfo;
//	delete jerr;
/ *
	s->modifLock.lock();

	try{
		byte *f=s->lastFrame;
		//			cout<<"set frame:"<<(void*)s<<" lastFrame:"<<(void*)&(s->lastFrame)<<" frame:"<<(void*) newFrame<<endl;
		s->lastFrameLen=len;
		s->lastFrame=jpeg;
		if (f!=NULL)
		{
			delete [] f;
		}
		}
*/	
/*	}catch(...){}

	s->modifLock.unlock();

}	


/ *
	int Width = bmpInfo.bmiHeader.biWidth;
	int Height = bmpInfo.bmiHeader.biHeight;

	int bmpSize= Width*Height*bmpInfo.bmiHeader.biBitCount/8;//bmpInfo.bmiHeader.biSizeImage;
//	LPVOID lpvBits = new char[bmpSize];//un cop temin el tamany, alocatem i torném a cridar

	byte *jpeg=new byte[bmpSize];

	struct jpeg_compress_struct *cinfo;
	cinfo=new jpeg_compress_struct;
	struct jpeg_error_mgr *jerr;
	jerr=new jpeg_error_mgr;
	cinfo->err = jpeg_std_error(jerr);
	jpeg_create_compress(cinfo);  
	cinfo->input_components = 3;	
	cinfo->in_color_space = JCS_RGB; 
	jpeg_set_defaults(cinfo);
	cinfo->dest=new jpeg_destination_mgr;
	cinfo->dest->empty_output_buffer = &empty_output_buffer;
	cinfo->dest->init_destination = &init_destination;
	cinfo->dest->term_destination = &term_destination;


	cinfo->image_width = Width; 	
	cinfo->image_height = Height;
	cinfo->dest->next_output_byte=(JOCTET*)jpeg;
	cinfo->dest->free_in_buffer=bmpSize;

	//---- reorganització win
	//	char first_byte = bmp[0];

	for (int posy = 0; posy < Height; posy++)
	{
		int i1 = 0, i2 = 0 ;
		for (int posx = 0; posx < Width; posx++) {
			int b=bmp[posy*Width*3+i1];
			int g=bmp[posy*Width*3+i1+1];
			int r=bmp[posy*Width*3+i1+2];
			bmp[posy*Width*3+i1]   = r;
			bmp[posy*Width*3+i1+1] = g;
			bmp[posy*Width*3+i1+2] = b;
			i1+=3;i2 +=3;
		}
	}


	//---- compressio jpeg
	jpeg_start_compress(cinfo,true);
	for(int i=0;i<Height;i++)
	//for(int i=Height-1;i>=0;i--)
	{

		JSAMPROW row=(JSAMPROW)(( bmp)+(i*(Width*3)));
		jpeg_write_scanlines(cinfo, &row,1);
	}
	jpeg_finish_compress(cinfo);

	//	cout << "[JPEG] Read frame Header (len " << frameHdr.length() << "): " << frameHdr <<" -- "<< endl;
	dword len=(bmpSize)-(cinfo->dest->free_in_buffer);//lpbi->bmiHeader.biSizeImage;
	s->modifLock.lock();

	try{
		byte *f=s->lastFrame;
		//			cout<<"set frame:"<<(void*)s<<" lastFrame:"<<(void*)&(s->lastFrame)<<" frame:"<<(void*) newFrame<<endl;
		s->lastFrameLen=len;
		s->lastFrame=jpeg;
		if (f!=NULL)
		{
			delete [] f;
		}
	}catch(...){}
	s->modifLock.unlock();

	delete cinfo->dest;
	cinfo->dest=NULL;
	jpeg_destroy_compress(cinfo);
	delete cinfo;

	delete jerr;

*/
/*
#endif
*/

#pragma mark *** Constructores

void RecordingCameraPlugin::createStreamThread(RecordingCameraPlugin::streamInfo *s, Address *a)
{
	if (s->threadId!=-1)
		return;

//	cout << "cST mL lock" << endl;
	s->modifLock.lock();
//	cout << "cST mL locked" << endl;

	if (s->imageThreadLock!=NULL)
	{
//		cout << "createStreamThread: delete s->imageThreadLock" << endl;
		delete s->imageThreadLock;
//		cout << "createStreamThread: /delete s->imageThreadLock" << endl;
	}

	s->imageThreadLock=new Mutex();

	s->lastAccessToFB=new Timer();
	
	s->modifLock.unlock();
	
	s->lastAccessToFB->start();
//	sessions[a->toString()]=*s;
	
	// Iniciem el thread amb una serie de params
	RecordingCameraPluginStreamThread::imageThreadArgs *ita=new RecordingCameraPluginStreamThread::imageThreadArgs();
	
	ita->s=s;
	ita->a=*a;
	ita->plugin=this;
	ita->timeoutTimer=s->lastAccessToFB;
	
	s->threadId=streamThread.start(ita);

	cout << "RecordingCameraPlugin::createStreamThread() thId: " << s->threadId << endl;

//	sessions[a->toString()]=*s;
}



struct RMListRecordingsParams::recording RecordingCameraPlugin::getRecording(streamInfo *sInfo)
{	//timeout -> comprovem que les coses encara estiguin gravant :P
	cout<<"RecordingCameraPlugin::getRecording"<<endl;

	RMListRecordingsParams lrp;
	struct timeval date;
	struct tm t;//(const time_t*)(&aux))
	
	gettimeofday(&date, NULL);
#ifdef WIN32
	unsigned long long secs=(date.tv_sec);
	struct tm *ptm;
	ptm= localtime((const time_t*)&secs);
	t= *ptm;
#else
	t= *localtime((const time_t*)&date.tv_sec);
#endif
	cout<<"RecordingCameraPlugin::getRecording 2"<<endl;
//	lrp.absStartDate.secs=date.tv_sec-60; //les ultimes només
//	lrp.absStartDate.millis=date.tv_usec/1000;
	lrp.absStartDate.isValid=false;
//	lrp.absEndDate.secs=date.tv_sec+60; //les ultimes només
//	lrp.absEndDate.millis=date.tv_usec/1000;
	lrp.absEndDate.isValid=false;
//	lrp.nRecsFilter=0;
//	lrp.nRecordings=0;
//	lrp.recordings=NULL;
	lrp.nRecsFilter=1;
	lrp.nRecordings=0;
	lrp.recordings=new RMListRecordingsParams::recording[1];


	lrp.recordings[0].id=0xffffffffffffffffll; //qualsevol
	lrp.recordings[0].devId=sInfo->port;//devInfo.id;//0xffffffff;
	lrp.recordings[0].recType=RecordingFileHeaderChunk::REC_HOWMANY;
	lrp.recordings[0].startDate.isValid=false;
	lrp.recordings[0].endDate.isValid=false;
/*	lrp.recordings[0].startDate.secs=date.tv_sec-60; //les ultimes només
	lrp.recordings[0].startDate.millis=date.tv_usec/1000;
	lrp.recordings[0].startDate.isValid=true;
	lrp.recordings[0].endDate.secs=date.tv_sec+60; //les ultimes només
	lrp.recordings[0].endDate.millis=date.tv_usec/1000;
	lrp.recordings[0].endDate.isValid=true;
*/
	cout<<"RecordingCameraPlugin::getRecording 3"<<endl;
	RMListRecordingsParams *recs=NULL;
	RecordingModuleAccess rma(this->cn);
	cout<<"RecordingCameraPlugin::getRecording 4"<<endl;
	try{
		recs=rma.listRecordings(lrp);
	}catch(Exception &e)
	{
		cout<<" listRecordings exception:"<<e.getClass()<<":"<<e.getMsg()<<endl;
	}
	

	if(recs != NULL && recs->nRecordings>0)
	{
		cout<<"RecordingCameraPlugin::getRecording return rec:"<<recs->recordings[0].rmAddress.toString()<<" "<<recs->recordings[0].id<<" "<<recs->recordings[0].devId<<" "<<recs->recordings[0].startDate.secs<<"--"<<recs->recordings[0].endDate.secs<<" "<<endl;
		RMListRecordingsParams::recording res;
		cout<<"RecordingCameraPlugin::getRecording copy"<<endl;
		res.rmAddress=recs->recordings[0].rmAddress;
		res.id=recs->recordings[0].id;
		res.devId=recs->recordings[0].devId;
		res.recType=recs->recordings[0].recType;
		res.fps.whole=recs->recordings[0].fps.whole;
		res.fps.frac=recs->recordings[0].fps.frac;
		res.startDate=recs->recordings[0].startDate;
		res.endDate=recs->recordings[0].endDate;
//		cout<<"RecordingCameraPlugin::getRecording delete"<<endl;
//		delete recs->recordings;
//		recs->recordings=NULL;
		cout<<"RecordingCameraPlugin::getRecording return"<<endl;
		return res;
	}else
	{
		cout<<"RecordingCameraPlugin::getRecording return NULL"<<endl;
		struct RMListRecordingsParams::recording rec;
		rec.id=-1;
		return rec;
	}
/*	for(int recIdx=0;recIdx<recs->nRecordings;recIdx++)
	{
		for(list<Schedule>::iterator it=recordings.begin(); it!=recordings.end(); it++)
		{
			int tstart;
			tstart = it->start.waitTime(it->dayMask, t); 
			int tend;
			tend = it->end.waitTime(it->dayMask, t); 
			if(tend<tstart && it->devId==recs->recordings[recIdx].devId) // hauria de gravar
			{
				cout<< "   trobat entre schedules programats:"<<tstart<< "->"<<tend<<"  ("<<it->start.toString()<<" -> "<<it->end.toString()<<") type:"<<it->type<<endl;
				it->active=true;
				found=true;
				break;
			}
		}	
	}
*/
}

RecordingCameraPlugin::RecordingCameraPlugin(Address addr) : PluginImage(addr, "ImagePlugin::SONY"),
	centralDirectoryAddr(Address(IP("0.0.0.0"), 0))
{
}

RecordingCameraPlugin::RecordingCameraPlugin(string file) : configFile(file), PluginImage(file),
	centralDirectoryAddr(Address(IP("0.0.0.0"), 0))
{
}

 

IPDeviceInfo RecordingCameraPlugin::getDeviceInformation(IPDeviceInfo dev, Address *a)
{
//	cout << "--> RecordingCameraPlugin::getDeviceInformation()" << endl;
	string query=string("SELECT d.id, f.nombre, m.codigo FROM dispositivo d, fabricante f, modelo m WHERE d.id='")+
		StrUtils::decToString(dev.id.id)+
		string("' AND d.fabricante=m.fabricante AND d.modelo=m.id AND m.fabricante=f.id AND f.nombre='Emulated'");
	RPCPacket bdQueryPk(this->address, 0, (byte*)query.c_str(), query.length(), this->type, -2);

//	cout << "bdQuery packet assembled" << endl;
	
	// Trobem la BD
	RPC *rpc=RecordingCameraPlugin::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn);
		rpc=RecordingCameraPlugin::getRPC(bd->a);
		delete bd;
	}

//	cout << "Sending keepAlive to " << a->toString() << endl;
	RPC::keepAlive(a, this->type);
				
	RPCPacket *result=rpc->call(bdQueryPk);
	RecordingCameraPlugin::ptrPool.release(rpc);

//	cout << "Sending keepAlive to " << a->toString() << endl;
	RPC::keepAlive(a, this->type);

//	cout << "RPC call done" << endl;

	string resXML((char*)result->getData(), (size_t)result->getSize());
	delete result;
	
//	cout << "Response: " << endl << resXML << endl;
	
	XML *xml=xmlParser::parse(resXML);

//	cout << "XML parsed" << endl;

	xmlNode *row=xml->getNode("/result/[0]");
	
	if (row==NULL)
	{
		cout <<  "Device with specified ID " << StrUtils::decToString(dev.id.id) << " does not exist or it is not a SONY device:"<<xml->toString()<<endl;
		delete xml;
		throw (IPInvalidParamException(0, string("Device with specified ID ") + StrUtils::decToString(dev.id.id) + string("does not exist or it is not an Emulated device")));
	}

//	cout << "Creating IPDeviceInfo" << endl;
	
	// TODO afegir resolucions i codecs suportats
	IPDeviceInfo di(atoi(xml->getNode("/result/[0]/id")->getCdata().c_str()),
					xml->getNode("/result/[0]/nombre")->getCdata(),
					xml->getNode("/result/[0]/codigo")->getCdata(),
					IPFrameInfoSeq(), IPCodecInfoSeq());

	delete xml;

//	cout << "<-- RecordingCameraPlugin::getDeviceInformation()" << endl;
	return di;
}

void RecordingCameraPlugin::startSession(IPDeviceID id, Address *a)
{
	// cout  << "--> RecordingCameraPlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;

	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes!=sessions.end())
	{
		throw (IPSessionAlreadyStablishedException(0, string("Session already stablished for device ")+
														StrUtils::decToString(id.id)+string(" from client ")+
														a->toString()));
	}
	
	sessionInfo s;
	s.devInfo.id=id.id;

	// cout  << "local call" << endl;
	s.devInfo=this->getDeviceInformation(s.devInfo, a);
	
	if (s.devInfo.make!="Emulated")
	{
		throw (IPInvalidParamException(0, "Device with specified ID is not an emulated device"));
	}

	

	streamInfo *sInfo=getStreamInfoForDevice(s.devInfo.id.id);
	
	if (sInfo==NULL)
	{
		sInfo=new streamInfo();

		sInfo->devInfo=s.devInfo;
		sInfo->fps.whole=25;
		sInfo->fps.frac=0;
			
		// Model, a lo guarro q es lo q toca :P
		if (s.devInfo.model=="Recording") sInfo->model=RECORDING;
		else
		{
			throw (IPInvalidParamException(0, "Unknown camera model for specified Camera ID"));
		}

		sInfo->imageThreadLock=new Mutex();
		if (sInfo->imageThreadLock==NULL)
		{
			throw (ImagePluginException(0, "Not enough memory to allocate image thread mutex"));
		}	
		
		// cout  << "Querying DB to find device " << endl;
		string query=string("SELECT host(d.ip) AS ip, d.puerto FROM dispositivo d, modelo m, fabricante f WHERE d.id='")+
			StrUtils::decToString(s.devInfo.id.id)+
			string("' AND d.modelo=m.id AND d.fabricante=m.fabricante AND m.fabricante=f.id AND f.nombre='Emulated'");
		RPCPacket bdQueryPk(this->address, 0, (byte*)query.c_str(), query.length(), this->type, 
							ServiceFinder::dbGatewayTypeId);
		// Trobem la BD
		RPC *rpc=RecordingCameraPlugin::getRPC(NULL);
		
		if (rpc==NULL)
		{	
			RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn);
			rpc=RecordingCameraPlugin::getRPC(bd->a);
			delete bd;
		}
	
		RPCPacket *result=rpc->call(bdQueryPk);
		RecordingCameraPlugin::ptrPool.release(rpc);
		
		string resXML((char*)result->getData(), (size_t)result->getSize());
		delete result;
		
		// cout  << "XML result " << endl << resXML << endl;
		
		XML *xml=xmlParser::parse(resXML);
	
		xmlNode *row=xml->getNode("/result/[0]");
		
		if (row==NULL)
		{
			delete xml;
	//		delete [] s.fb;
	//		delete [] s.fbLengths;
	//		delete [] s.fbKeyFrames;
			throw (IPInvalidParamException(0, "Device with specified ID does not exist"));
		}
		
	
		sInfo->ip=xml->getNode("/result/[0]/ip")->getCdata();
		sInfo->port=atoi(xml->getNode("/result/[0]/puerto")->getCdata().c_str());
	
		delete xml;

		cout<<"RecordingCameraPlugin:: call "<<(void*)sInfo<<endl;

		XML *config=NULL;
		{
			cout<<"RecordingCameraPlugin:: read file :"<<this->getConfigFileName()<<endl;
			FILE *f=fopen(this->getConfigFileName().c_str(),"rb");

			if (f==NULL)
				throw FileException(string("File ")+this->getConfigFileName()+string(" not found"));
			cout<<"RecordingCameraPlugin:: len"<<endl;

			fseek(f,0,SEEK_END);
			int len=ftell(f);
			fseek(f,0,SEEK_SET);
			cout<<"RecordingCameraPlugin:: len :"<<len<<endl;

			char *buf=new char[len+1];
			fread(buf,len,1,f);
			buf[len]='\0';
			fclose(f);
			cout<<"RecordingCameraPlugin:: parse:"<<endl;

			config=xmlParser::parse(string(buf, len));
		}
	
		cout<<"RecordingCameraPlugin:: data:"<<endl;
		for(int i=0;;i++)
		{
			string path=string("/[0]/Cameras/[")+StrUtils::decToString(i)+string("]");
				
			if(config->getNode(path)==NULL)
				throw (IPInvalidParamException(0, "Device with specified ID does not have cameras"));

			xmlNode *n=config->getNode(path+string("/id"));
			int camId=atoi(n->getCdata().c_str());
			cout<<"RecordingCameraPlugin:: camera ID:"<<camId<<" startSession:"<<id.id<<endl;
			if(camId==id.id)
			{
				n=config->getNode(path+string("/recordingID"));
				sInfo->recording.id=atoi(n->getCdata().c_str());
				n=config->getNode(path+string("/start"));
				sInfo->recording.start=atol(n->getCdata().c_str());
				n=config->getNode(path+string("/length"));
				sInfo->recording.length=atoi(n->getCdata().c_str());
				break;
			}
		}
		sInfo->streamingMode=IPStreamingMode(IPStreamingMode::STREAM);


		cout<<"RecordingCameraPlugin:: save:"<<endl;
		streamsLock.lock();
		streams[s.devInfo.id.id]=sInfo;
		streamsLock.unlock();
	}

	s.stream=sInfo;

	sessionLock.lock();
	sessions[a->toString()]=s;
	sessionLock.unlock();

/*	sInfo->codec=getCodecInUse(a);
	ControlModuleAccess cma(getConfigFileName());
	cma.startSession(s.devInfo.id);
		string cmCodec=cma.getConfigParam(string("/Video/1/Codec/Fourcc"));
		if(cmCodec.size()>=4)
		{
			const char *c=cmCodec.c_str();
			sInfo->codec.fourcc=CODEC_FOURCC(c[0],c[1],c[2],c[3]);
		}
//		else
//		{
//			sInfo->codec=getCodecInUse(a);
//		}
		cma.endSession(s.devInfo.id);
*/	
	// cout  << "<-- RecordingCameraPlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;
}

void RecordingCameraPlugin::endSession(IPDeviceID id, Address *a)
{
	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
	{
		throw (IPSessionNotStablishedException(0, string("Session not stablished for device ")+
														StrUtils::decToString(id.id)+string(" from client ")+
														a->toString()));
	}
//	sessionInfo s=(*itSes).second;
//	if (s.stream->streamingMode==STREAM)
//	{
//		s.stream->stopThread=true;
//		cout << "locloclocloc" << endl;
//		s.stream->imageThreadLock->lock();
//		cout << "/locloclocloc" << endl;
//		sessions[a->toString()]=s;
//		cout << "unlocunlocunlocunloc" << endl;
//		s.stream->imageThreadLock->unlock();
//		cout << "/unlocunlocunlocunloc" << endl;
//		itSes=sessions.find(a->toString());
////		cout << "joining " << (*itSes).second.threadId << endl;
//		s=(*itSes).second;
//		
//		streamThread.join(s.threadId);
//		switch (s.codec.fourcc)
//		{
//			case CODEC_JPEG:
//				jpegThread.join(s.threadId);
//				break;
//		
//			case CODEC_MPEG4:
//				mpeg4Thread.join(s.threadId);
//				break;
//		}
////		cout << "joined" << endl;
//	}

	sessions.erase(itSes);
}

IPCodecInfo RecordingCameraPlugin::getCodecInUse(Address *a)
{
	sessionInfo s=getSessionForAddress(a);
	
	return IPCodecInfo(CODEC_JPEG, 10, 0);
}

void RecordingCameraPlugin::setCodecInUse(IPCodecInfo codec, Address *a)
{
	sessionInfo s=getSessionForAddress(a);
	
/*	if (s.stream->streamingMode==STREAM)
	{
		throw (ImagePluginException(0, "Codec can not be changed while in STREAM mode"));
	}
*/		
	throw ImagePluginException(0,"Error: unable to change codec in use");
}

IPFrameBufferSize RecordingCameraPlugin::getFrameBufferSize(Address *a)
{
	cout<<"GFBS"<<endl;
	sessionInfo s=getSessionForAddress(a);

	IPFrameBufferSize fbSize;
	fbSize.nFrames=1; //s.fbSize;


	cout<<"/GFBS"<<endl;
	return fbSize;
}

void RecordingCameraPlugin::setFrameBufferSize(IPFrameBufferSize size, Address *a)
{
//	sessionInfo s=getSessionForAddress(a);
//
////	cout << "--> RecordingCameraPlugin::setFrameBufferSize(" << size.nFrames << ")" << endl;
//
//	byte **fb=new byte*[size.nFrames];
//	dword *fbL=new dword[size.nFrames];
//	bool *fbK=new bool[size.nFrames];
//	
//	if (fb==NULL)
//	{
//		throw (ImagePluginException(0, "Not enough memory to allocate frame buffer"));
//	}
//	
//	memset(fb, 0, sizeof(byte*)*size.nFrames);
//	memset(fbL, 0, sizeof(dword)*size.nFrames);
//	memset(fbK, false, sizeof(bool)*size.nFrames);
//	
//	int i=0;
//
//	for (; i<size.nFrames && i<s.fbSize; i++)
//	{
//		fb[i]=s.fb[i];
//		fbL[i]=s.fbLengths[i];
//		fbK[i]=s.fbKeyFrames[i];
//	}
//
//	// Nomes s'executara un dels dos seguents bucles
//	int j=i;
//	for (; i<size.nFrames; i++)
//	{
//		fb[i]=new byte[1]; //s.frameInfo.x*s.frameInfo.y*s.frameInfo.bpp/8];
//		if (fb[i]==NULL)
//		{
//			for (int k=j; k<i; k++)
//				delete [] fb[k];
//				
//			delete [] fb;
//			delete [] fbL;
//			throw (ImagePluginException(0, "Not enough memory to allocate frame buffer frame"));
//		}
//	}
//
//	sessionLock.lock();
//	for (; i<s.fbSize; i++)
//		if (s.fb[i]!=NULL)
//			delete [] s.fb[i];
//
//	delete [] s.fb;
//	delete [] s.fbLengths;
//	delete [] s.fbKeyFrames;
//	s.fbSize=size.nFrames;
//	s.fb=fb;
//	s.fbLengths=fbL;
//	s.fbKeyFrames=fbK;
//
//	sessions[a->toString()]=s;
//	sessionLock.unlock();


}

float RecordingCameraPlugin::getFrameBufferPercentInUse(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	return 0.0f; //(float)s.fbUse/(float)s.fbSize;
}

int RecordingCameraPlugin::getFrameBufferFramesInUse(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	return 0; //s.fbUse;
}

IPStreamingMode RecordingCameraPlugin::getStreamingMode(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	return s.stream->streamingMode;
}

void RecordingCameraPlugin::setStreamingMode(IPStreamingMode mode, Address *a)
{
	sessionInfo s=getSessionForAddress(a);
	

	//cout << "SetStreamingMode: " << mode << ", id: " << s.devInfo.id << endl;
	if(s.stream->streamingMode.mode!=IPStreamingMode::STREAM)
		throw (ImagePluginException(0, string("Invalid streaming mode: ")+StrUtils::decToString(s.stream->streamingMode.mode)));
//	s.stream->modifLock.lock();
//	s.stream->streamingMode=mode;
//	s.stream->modifLock.unlock();
	
}

IPFrame RecordingCameraPlugin::getCompressedNextFrame(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	IPCodecInfo codecInfo;
	codecInfo.fourcc=CODEC_JPEG;//s.stream->codec; //getCodecInUse(a);
	codecInfo.quality=5;
//	cout << "RecordingCameraPlugin::getCompressedNextFrame() ini" << endl;

			// Com que accedim al frame buffer, comptem l'acces
//			cout << "RecordingCameraPlugin::getCompressedNextFrame()  Stream + "<< string((char*)&codecInfo.fourcc, sizeof(codecInfo.fourcc)) << endl;
			s.stream->modifLock.lock();
			if (s.stream->lastAccessToFB!=NULL)		// Thread en marxa
			{
				try{
					s.stream->lastAccessToFB->start();
				}catch(...){}
			}
			s.stream->modifLock.unlock();

			if (s.stream->threadId==-1)		// El thread s'ha parat
			{
//				s.fbUse=0;
//				cout << "RecordingCameraPlugin::getCompressedNextFrame()  Thread stopped" << endl;
				createStreamThread(s.stream, a);
			}

//			if (codecInfo.fourcc == CODEC_JPEG)
			{
//				cout << "RecordingCameraPlugin::getCompressedNextFrame()  Stream JPEG " << endl;

				while (s.stream->lastFrame==NULL)
				{
					// Esperem "1/2" frame (@25fps)
					//				cout << " -> frame buffer empty" << endl;
					usleep(20000);
					//				s=getSessionForAddress(a);
				}

				s.stream->modifLock.lock();

				bool isKey=true; //fbKeyFrames[0];

				dword len=s.stream->lastFrameLen;
				byte *buf=new byte[len];
				memmove(buf, s.stream->lastFrame, len);


				s.stream->modifLock.unlock();
				return IPFrame(s.stream->frameInfo, codecInfo, isKey, len, buf);
			}
	throw (ImagePluginException(0, string("Invalid streaming mode: ")+StrUtils::decToString(s.stream->streamingMode.mode)));
}

IPFrame RecordingCameraPlugin::getDecompressedNextFrame(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	throw(ImagePluginException(-1, "Not implemented"));
}

IPFramesPerSecond RecordingCameraPlugin::getFPS(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	s.stream->modifLock.lock();
	IPFramesPerSecond fps(s.stream->fps.whole, s.stream->fps.frac);
	s.stream->modifLock.unlock();
	
	return fps;
}

void RecordingCameraPlugin::setFPS(IPFramesPerSecond fps, Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	s.stream->modifLock.lock();

//	cout << "setFPS: current fps: " << s.stream->fps.whole << ", requested: " << fps.whole << endl;

	// Canviem si estem augmentant els FPS o si es el primer canvi.
	if (s.stream->stillDefaultFPS || fps.whole>s.stream->fps.whole || (fps.whole==s.stream->fps.whole && fps.frac>s.stream->fps.frac))
	{
		s.stream->fps.whole=fps.whole;
		s.stream->fps.frac=fps.frac;
	
		s.stream->stillDefaultFPS=false;
		s.stream->fpsChanged=true;
	}

	s.stream->modifLock.unlock();
}

string RecordingCameraPlugin::getConfigFileName()
{
	return configFile;
}
