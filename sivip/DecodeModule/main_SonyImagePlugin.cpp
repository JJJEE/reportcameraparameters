#include <Utils/Types.h>
#include "SonyImagePlugin.h"
#include <signal.h>
#include <Utils/debugStackTrace.h>
#include <version.h>

#include <signal.h>

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMP();
	abort();
}

#endif

#ifndef WIN32
void sigpipe(int)
{
	cout << "SegPipe" << endl;
}
#endif

int main()
{
#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif

#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
//	signal(SIGABRT, sigsegfault);
#endif
#ifndef WIN32
	signal(SIGPIPE, sigpipe);
	SonyImagePlugin::registerSigUSR();
#endif

	STACKTRACE_INSTRUMENT();

	cout << siriusRelease << endl;

	bool exception;
//	do
//	{
		exception=false;
		try{
			SonyImagePlugin plugin(string("SonyImagePluginConfig.xml"));
			cout << "Plugin created. Going to serve..." << endl;
			plugin.serve();
		}catch(Exception &e)
		{
			exception=true;
			cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
		}
//	}while(exception);
	
	return 0;
}
