#ifdef WIN32

#include <Utils/Types.h>
#include "VisioWaveImagePlugin.h"
#include <signal.h>
#include <Utils/debugStackTrace.h>

#include <signal.h>
#include <version.h>

#ifdef WITH_DEBUG_NEW

void sigint(int)
{
	cout << "SigInt" << endl;
	dumpAllocatedMemoryMap();
	STACKTRACE_DUMPALL();
	abort();
}

#endif

#ifdef WITH_DEBUG_STACKTRACE

void sigsegfault(int)
{
	cout << "SegFault" << endl;
	STACKTRACE_DUMP();
	abort();
}

#endif

#ifndef WIN32
void sigpipe(int)
{
	cout << "SegPipe" << endl;
}
#endif

VisioWaveImagePlugin plugin(string("VisioWaveImagePluginConfig.xml"));

int main()
{
#ifdef WITH_DEBUG_NEW
	signal(SIGINT, sigint);
#endif

#ifdef WITH_DEBUG_STACKTRACE
	signal(SIGBUS, sigsegfault);
	signal(SIGSEGV, sigsegfault);
//	signal(SIGABRT, sigsegfault);
#endif
#ifndef WIN32
	signal(SIGPIPE, sigpipe);
#endif

	bool exception;
	do
	{
		exception=false;
		try{
#ifdef WIN32
//			VisioWaveImagePlugin plugin(string("VisioWaveImagePluginConfig.xml"));
//			cout << "Plugin created. Going to serve..." << endl;
//			plugin.serve();
#endif
			cout<<"main"<<endl;
			sleep(1);
			exception=true;
		}catch(Exception &e)
		{
			exception=true;
			cout<<"Exception: "<<e.getClass()<<" "<<e.getMsg()<<endl;
		}
	}while(exception);
	
	return 0;
}
#else
#include <version.h>
int main(){}
#endif