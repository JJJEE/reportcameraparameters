#ifdef WIN32

/*
 *  VisioWaveImagePlugin.cpp
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "VisioWaveImagePlugin.h"
#include <string.h>
#include <Utils/Canis.h>
#include <Utils/StrUtils.h>
#include <Plugins/GestorImageExceptions.h>
#include <Http/HttpClient.h>
#include <Http/HttpStream.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <ServiceFinder/ServiceFinder.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <Utils/debugStackTrace.h>
#include "SplashWnd.h"
#ifndef WIN32
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#else
#include <Utils/WindowsDefs.h>
#include <Utils/WindowsInclude.h>
#endif

#pragma mark *** Pool

PtrPool VisioWaveImagePlugin::ptrPool;

RPC* VisioWaveImagePlugin::getRPC(Address *a)
{
	RPC *rpc=(RPC*)ptrPool.get();
	
	if (rpc==NULL && a!=NULL)
	{
		rpc=new RPC(*a);
		if (rpc==NULL)
		{
			throw (Exception("FATAL: Not enough memory"));
		}
		ptrPool.add(rpc, true);
	}
	
	return rpc;
}

#pragma mark *** Privades

const IPFrameInfo VisioWaveImagePlugin::resolutions[VisioWaveImagePlugin::NUM_VIDEOSTD][VisioWaveImagePlugin::NUM_RESOLUTIONS]={
		{IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(736, 544, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)},
		{IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(763, 480, 24), IPFrameInfo(640, 480, 24),
		 IPFrameInfo(640, 480, 24), IPFrameInfo(640, 480, 24), IPFrameInfo(320, 240, 24), IPFrameInfo(160, 120, 24),
		 IPFrameInfo(0, 0, 0)}};


VisioWaveImagePlugin::sessionInfo VisioWaveImagePlugin::getSessionForAddress(Address *a)
{
	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
		throw (IPSessionNotStablishedException(0, string("Session not stablished with any device ")+
												   string("from client ")+a->toString()));
	
	return (*itSes).second;
}

VisioWaveImagePlugin::streamInfo* VisioWaveImagePlugin::getStreamInfoForDevice(int id)
{
	map<int,streamInfo*>::iterator itStr=streams.find(id);
	
	if (itStr==streams.end())
		return NULL;
	
	return itStr->second;
}

#pragma mark *** Threads de recepció imatges

VisioWaveImagePluginJPEGStreamThread::VisioWaveImagePluginJPEGStreamThread(CWnd* pParent): CDialog(VisioWaveImagePluginJPEGStreamThread::IDD, pParent)
{
	m_hBmp=NULL;
	hWndC=NULL;

	//{{AFX_DATA_INIT(VisioWaveImagePluginJPEGStreamThread)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
//	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

BEGIN_MESSAGE_MAP(VisioWaveImagePluginJPEGStreamThread, CDialog)
	//{{AFX_MSG_MAP(VisioWaveImagePluginJPEGStreamThread)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
//	ON_WM_TIMER()


BOOL VisioWaveImagePluginJPEGStreamThread::OnInitDialog()
{
	CDialog::OnInitDialog();

	/*--------------------------------------------------------------*/

	CSplashWnd::ShowSplashScreen(3000, IDB_MyLOGO, this);

	// Add "About..." menu item to system menu.
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);
	this->m_hWnd;
	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	//Iniatilize frame's details
	CStatic* pst=(CStatic*) GetDlgItem(IDC_STATIC_FRAME);
	pst->GetWindowRect(&m_rectFrame);
	ScreenToClient(&m_rectFrame);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void VisioWaveImagePluginJPEGStreamThread::OnSysCommand(UINT nID, LPARAM lParam)
{
/*	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
*/
	CDialog::OnSysCommand(nID, lParam);
//	}
}

void VisioWaveImagePluginJPEGStreamThread::OnPaint() 
{
	CDialog::OnPaint();
}

void VisioWaveImagePluginJPEGStreamThread::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(VisioWaveImagePluginJPEGStreamThread)
	//}}AFX_DATA_MAP
//	DDX_Control(pDX, IDC_AVICAP, m_RecordButton);
}

void *VisioWaveImagePluginJPEGStreamThread::execute(int id, void *args)
{
//	VisioWaveImagePluginStreamThread::imageThreadArgs *ita=(VisioWaveImagePluginStreamThread::imageThreadArgs *)args;
	VisioWaveImagePluginJPEGStreamThread::imageThreadArgs *ita=(VisioWaveImagePluginJPEGStreamThread::imageThreadArgs *)args;
	VisioWaveImagePlugin *plugin=(VisioWaveImagePlugin *)ita->plugin;

	VisioWaveImagePlugin::streamInfo *s=(VisioWaveImagePlugin::streamInfo*)ita->s;

	HttpStream *stream;
	string hdr=string(""), firstFrame=string("");

	// Comencem a agafar excepcions des d'aqui, pq pot petar el tema control module :P
	ControlModuleAccess *controlModule=NULL;
	try
	{	
/*		//	cout << "VisioWaveImagePluginJPEGStreamThread create CMA: " << plugin->getConfigFileName() << endl;
		controlModule=new ControlModuleAccess(plugin->getConfigFileName());
	//	cout << "VisioWaveImagePluginJPEGStreamThread created CMA: " << plugin->getConfigFileName() << endl;
		CPDeviceID devId(s->devInfo.id);
		controlModule->startSession(devId);
		CPConfigParamSeq res=controlModule->getConfigParamRecursive(CPConfigParam("/Video/1/Resolution",""));
	//	string height=controlModule->getConfigParam("/Alarms/ActivityDetection/1/PopUpText");
		s->frameInfo.x=0;
		s->frameInfo.y=0;
		
		while(!res.empty())
		{
			cout << "VisioWaveImagePluginJPEGStreamThread::execute() " <<res.front().path<<" "<<res.front().value<<endl;
			if(res.front().path==string("/Video/1/Resolution/x"))
				s->frameInfo.x=atoi(res.front().value.c_str());
			else if(res.front().path==string("/Video/1/Resolution/y"))
				s->frameInfo.y=atoi(res.front().value.c_str());
	
			res.pop_front();
		}
	
	
*/
		s->modifLock.lock();
	
//		string url=string("http://")+s->ip+string(":")+StrUtils::decToString(s->port)+string("/image?speed=")+
//			StrUtils::decToString(s->fps.whole);
//		s->fpsChanged=false;

		BITMAP bm;      //bitmap struct
		CBitmap m_bmp; //bitmap object 

		//Set a capture window.

	string cw("Capture Window");
	CA2T p(cw.c_str());
	/*(LPTSTR)s->ip.c_str;*/ 
	//(LPTSTR)(_tcschr(wc,(wchar_t)_T('\0')));

		HWND wHnd=GetSafeHwnd();

		hWndC = capCreateCaptureWindow (p , WS_CHILD /*|  
							WS_VISIBLE /*| WS_THICKFRAME */|WS_DLGFRAME /*|WS_EX_DLGMODALFRAME*/,
							m_rectFrame.TopLeft().x, m_rectFrame.TopLeft().y,
							320, 240, wHnd/*hwndParent*/, 11011);

//		if(hWndC)
//			capDriverConnect(hWndC, i);

		WCHAR szDeviceName[80];
		WCHAR szDeviceVersion[80];

		for (int wIndex = 0; wIndex < 10; wIndex++) 
		{
			if (capGetDriverDescription( wIndex, 
				szDeviceName, sizeof (szDeviceName), 
				szDeviceVersion, sizeof (szDeviceVersion))) //(LPWSTR)
			{
				CW2A name(szDeviceName);
				CW2A ver(szDeviceVersion);
				cout<<"driver i:"<<wIndex<<" name:"<<name<<" vers:"<<ver<<endl;
			}
		} 

		bool res=capDriverConnect (hWndC, 1); // 0 ->means default driver.


		s->modifLock.unlock();
			
		while (true)
		{
			// UPDATE HTTP SI HAN CANVIAT ELS FPS
//			if (s->fpsChanged)
//			{
//				string url=string("http://")+s->ip+string(":")+StrUtils::decToString(s->port)+string("/image?speed=")+
//					StrUtils::decToString(s->fps.whole);
//				cli.setURL(url);
//			}

	//		cout << "itl lock" << endl;
			s->imageThreadLock->lock();
	//		cout << "itl locked" << endl;


capGrabFrame(hWndC); // simple macro that sample a single frame from the camera.
capEditCopy(hWndC);  // simple macro that edit a copy of the frame.
OpenClipboard();     //like virtual memory.

//m_hBmp is a Handle to Bitmap.
m_hBmp = (HBITMAP)::GetClipboardData(CF_BITMAP); 
CloseClipboard();

m_bmp.Detach();       //cleaning the bitmap.
m_bmp.Attach(m_hBmp); //connecting the bitmap throw the handle.


//InvalidateRect(m_rectFrame,false); // m_rectFrame is the frame that the 
//// video stream will be present in.
//OnPaint();            // calling to paint function to paint the bitmap 
//// into the frame on the dialog.





			string frame("");
			byte *newFrame=new byte[frame.length()];
			memmove(newFrame, frame.c_str(), frame.length());
			
			s->modifLock.lock();
			
			TimerInstant ti=ita->timeoutTimer->time();
			if (ti.seconds() > s->streamTimeout)
			{
				cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Thread Timeout occurred, stream is not being used!" << endl;
				s->stopThread=true;
			}
		
			if (s->stopThread)
			{
				cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Stopping thread..." << endl;
				s->stopThread=false;
				delete s->lastAccessToFB;
				s->lastAccessToFB=NULL;
				s->threadId=-1;
				s->modifLock.unlock();
				s->imageThreadLock->unlock();

				delete ita;
				delete stream;
				delete [] newFrame;
				delete controlModule;
				
				cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Stopped..." << endl;
				return NULL;
			}
			
			dword len=frame.length();
			byte *f=s->lastFrame;
			s->lastFrameLen=len;
			s->lastFrame=newFrame;

			s->modifLock.unlock();

			if (f!=NULL)
			{
				delete [] f;
			}
	
			s->imageThreadLock->unlock();
		}
	}
	catch (Exception &e)
	{
		// Simulem un timeout....
		s->modifLock.lock();
		
		cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Exception: " << e.getClass() << ": " << e.getMsg() << endl;
			cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Stopping thread..." << endl;
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		delete ita;
		delete stream;
		delete controlModule;
		
		s->modifLock.unlock();
		s->imageThreadLock->unlock();
		return NULL;
	}
	catch (...)
	{
		// Simulem un timeout....
		s->modifLock.lock();
		
		cout << "VisioWaveImagePluginJPEGStreamThread::execute(): unknown exception" << endl;
			cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Stopping thread..." << endl;
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

//			cout << "VisioWaveImagePluginJPEGStreamThread::execute() 6" << endl;
//			cout << "Stopping thread" << endl;
		delete ita;
		delete stream;
		delete controlModule;
		
		s->modifLock.unlock();
		s->imageThreadLock->unlock();
		return NULL;
	}

//	cout << "VisioWaveImagePluginJPEGStreamThread::execute() 9" << endl;
//	cout << "VisioWaveImagePluginJPEGStreamThread finishing..." << endl;

	delete ita;
	delete controlModule;
	return NULL;
}







void VisioWaveImagePlugin::createStreamThread(VisioWaveImagePlugin::streamInfo *s, Address *a)
{
	if (s->threadId!=-1)
		return;

//	cout << "cST mL lock" << endl;
	s->modifLock.lock();
//	cout << "cST mL locked" << endl;

	if (s->imageThreadLock!=NULL)
	{
//		cout << "createStreamThread: delete s->imageThreadLock" << endl;
		delete s->imageThreadLock;
//		cout << "createStreamThread: /delete s->imageThreadLock" << endl;
	}

	s->imageThreadLock=new Mutex();
	s->lastAccessToFB=new Timer();
	s->modifLock.unlock();
	s->lastAccessToFB->start();
//	sessions[a->toString()]=*s;
	
	// Iniciem el thread amb una serie de params
//	VisioWaveImagePluginStreamThread::imageThreadArgs *ita=new VisioWaveImagePluginStreamThread::imageThreadArgs();
	VisioWaveImagePluginJPEGStreamThread::imageThreadArgs *ita=new VisioWaveImagePluginJPEGStreamThread::imageThreadArgs();
	
	ita->s=s;
	ita->a=*a;
	ita->plugin=this;
	ita->timeoutTimer=s->lastAccessToFB;
	
//	switch (s->codec.fourcc)
//	{
//		case CODEC_JPEG:
			s->threadId=jpegThread.start(ita);
//			break;
//		case CODEC_MPEG4:
//			s->threadId=mpeg4Thread.start(ita);
//			break;
//	}

//	cout << "VisioWaveImagePlugin::createStreamThread() thId: " << s->threadId << endl;

//	sessions[a->toString()]=*s;
}


/////////////////////////////////////////////////////////////////////////////
// CCameraToolKitApp

BEGIN_MESSAGE_MAP(VisioWaveImagePlugin, CWinApp)
	//{{AFX_MSG_MAP(VisioWaveImagePlugin)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
END_MESSAGE_MAP()

//	ON_COMMAND(ID_HELP, CWinApp::OnHelp)


/////////////////////////////////////////////////////////////////////////////
// The one and only VisioWaveImagePlugin object
// VisioWaveImagePlugin theApp;


VisioWaveImagePlugin::VisioWaveImagePlugin(Address addr) : PluginImage(addr, "ImagePlugin::VisioWave"), 
centralDirectoryAddr(Address(IP("0.0.0.0"), 0)), serve(this)//, jpegThread(this)
{
}

VisioWaveImagePlugin::VisioWaveImagePlugin(string file) : configFile(file), PluginImage(file),
centralDirectoryAddr(Address(IP("0.0.0.0"), 0)), serve(this)//, jpegThread(this)
{
}


BOOL VisioWaveImagePlugin::InitInstance()
{
	cout<<" -- InitInstance --"<<endl;
	AfxEnableControlContainer();
	
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
	/*------------Splash-Wnd-------------------*/
	// Enable the splash screen component based on the command line info.
//	CCommandLineInfo cmdInfo;
//	ParseCommandLine(cmdInfo);
//	CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);
	/*------------Dialog's-Colour--------------*/
	//Set Bkgrd colour
//	SetDialogBkColor(RGB(192,192,192),RGB(180,50,50));
//	SetDialogBkColor(RGB(50,50,200),RGB(190,190,190));
	/*-----------------------------------------*/

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	//VisioWaveImagePluginJPEGStreamThread dlg;
	serve.start();
	m_pMainWnd = &jpegThread;//&dlg;
	int nResponse = jpegThread.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

BOOL VisioWaveImagePlugin::PreTranslateMessage(MSG* pMsg) 
{
	// Route messages to the splash screen while it is visible
//	if (CSplashWnd::PreTranslateAppMessage(pMsg)) {
//		return TRUE;
//	}
    return CWinApp::PreTranslateMessage(pMsg);
}



IPDeviceInfo VisioWaveImagePlugin::getDeviceInformation(IPDeviceInfo dev, Address *a)
{
//	cout << "--> VisioWaveImagePlugin::getDeviceInformation()" << endl;
	string query=string("SELECT d.id, f.nombre, m.codigo FROM dispositivo d, fabricante f, modelo m WHERE d.id='")+
		StrUtils::decToString(dev.id)+
		string("' AND d.fabricante=m.fabricante AND d.modelo=m.id AND m.fabricante=f.id AND f.nombre='VisioWave'");
	RPCPacket bdQueryPk(this->socket->getAddr(), 0, (byte*)query.c_str(), query.length(), this->type, -2);

//	cout << "bdQuery packet assembled" << endl;
	
	// Trobem la BD
	RPC *rpc=VisioWaveImagePlugin::getRPC(NULL);
	
	if (rpc==NULL)
	{	
		RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn);
		rpc=VisioWaveImagePlugin::getRPC(bd->a);
		delete bd;
	}

//	cout << "Sending keepAlive to " << a->toString() << endl;
	RPC::keepAlive(a, this->type);
				
	RPCPacket *result=rpc->call(bdQueryPk);
	VisioWaveImagePlugin::ptrPool.release(rpc);

//	cout << "Sending keepAlive to " << a->toString() << endl;
	RPC::keepAlive(a, this->type);

//	cout << "RPC call done" << endl;

	string resXML((char*)result->getData(), (size_t)result->getSize());
	delete result;
	
//	cout << "Response: " << endl << resXML << endl;
	
	XML *xml=xmlParser::parse(resXML);

//	cout << "XML parsed" << endl;

	xmlNode *row=xml->getNode("/result/[0]");
	
	if (row==NULL)
	{
		cout <<  "Device with specified ID does not exist or it is not a VisioWave device:"<<xml->toString()<<endl;
		delete xml;
		throw (IPInvalidParamException(0, "Device with specified ID does not exist or it is not a VisioWave device"));
	}

//	cout << "Creating IPDeviceInfo" << endl;
	
	// TODO afegir resolucions i codecs suportats
	IPDeviceInfo di(atoi(xml->getNode("/result/[0]/id")->getCdata().c_str()),
					xml->getNode("/result/[0]/nombre")->getCdata(),
					xml->getNode("/result/[0]/codigo")->getCdata(),
					IPFrameInfoSeq(), IPCodecInfoSeq());

	delete xml;

//	cout << "<-- VisioWaveImagePlugin::getDeviceInformation()" << endl;
	return di;
}

void VisioWaveImagePlugin::startSession(IPDeviceID id, Address *a)
{
	// cout  << "--> VisioWaveImagePlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;

	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes!=sessions.end())
	{
		throw (IPSessionAlreadyStablishedException(0, string("Session already stablished for device ")+
														StrUtils::decToString(id.id)+string(" from client ")+
														a->toString()));
	}
	
	sessionInfo s;
	memset(&s, sizeof(s), 0);
	s.devInfo.id=id.id;

	// cout  << "local call" << endl;
	s.devInfo=this->getDeviceInformation(s.devInfo, a);
	
	if (s.devInfo.make!="VisioWave")
	{
		throw (IPInvalidParamException(0, "Device with specified ID is not a VisioWave device"));
	}

	streamInfo *sInfo=getStreamInfoForDevice(s.devInfo.id);
	
	if (sInfo==NULL)
	{
		sInfo=new streamInfo();

		sInfo->devInfo=s.devInfo;
		sInfo->fps.whole=25;
		sInfo->fps.frac=0;
			
		// Model, a lo guarro q es lo q toca :P
		if (s.devInfo.model=="Evolution 16") sInfo->model=EVOLUTION_16;
		else if (s.devInfo.model=="Evolution 28") sInfo->model=EVOLUTION_28;
		else if (s.devInfo.model=="PowerPlus") sInfo->model=POWERPLUS;
		else if (s.devInfo.model=="VisioBox") sInfo->model=VISIOBOX;
		else if (s.devInfo.model=="Central Configuration Server") sInfo->model=CCS;
		else
		{
			throw (IPInvalidParamException(0, "Unknown camera model for specified Camera ID"));
		}

		sInfo->imageThreadLock=new Mutex();
		if (sInfo->imageThreadLock==NULL)
		{
			throw (ImagePluginException(0, "Not enough memory to allocate image thread mutex"));
		}	
		
		// cout  << "Querying DB to find device " << endl;
		string query=string("SELECT host(d.ip) AS ip, d.puerto FROM dispositivo d, modelo m, fabricante f WHERE d.id='")+
			StrUtils::decToString(s.devInfo.id)+
			string("' AND d.modelo=m.id AND d.fabricante=m.fabricante AND m.fabricante=f.id AND f.nombre='VisioWave'");
		RPCPacket bdQueryPk(this->socket->getAddr(), 0, (byte*)query.c_str(), query.length(), this->type, 
							ServiceFinder::dbGatewayTypeId);
		// Trobem la BD
		RPC *rpc=VisioWaveImagePlugin::getRPC(NULL);
		
		if (rpc==NULL)
		{	
			RPCPacket *bd=ServiceFinder::getBestSubsys(ServiceFinder::dbGatewayTypeId, this->cn);
			rpc=VisioWaveImagePlugin::getRPC(bd->a);
			delete bd;
		}
	
		RPCPacket *result=rpc->call(bdQueryPk);
		VisioWaveImagePlugin::ptrPool.release(rpc);
		
		string resXML((char*)result->getData(), (size_t)result->getSize());
		delete result;
		
		// cout  << "XML result " << endl << resXML << endl;
		
		XML *xml=xmlParser::parse(resXML);
	
		xmlNode *row=xml->getNode("/result/[0]");
		
		if (row==NULL)
		{
			delete xml;
	//		delete [] s.fb;
	//		delete [] s.fbLengths;
	//		delete [] s.fbKeyFrames;
			throw (IPInvalidParamException(0, "Device with specified ID does not exist"));
		}
		
	
		sInfo->ip=xml->getNode("/result/[0]/ip")->getCdata();
		sInfo->port=atoi(xml->getNode("/result/[0]/puerto")->getCdata().c_str());
	
		delete xml;

		streamsLock.lock();
		streams[s.devInfo.id]=sInfo;
		streamsLock.unlock();
	}

	s.stream=sInfo;

	sessionLock.lock();
	sessions[a->toString()]=s;
	sessionLock.unlock();

	sInfo->codec=getCodecInUse(a);

	// cout  << "<-- VisioWaveImagePlugin::startSession(" << id.id << ", " << a->toString() << ")" << endl;
}

void VisioWaveImagePlugin::endSession(IPDeviceID id, Address *a)
{
	map<string,sessionInfo>::iterator itSes=sessions.find(a->toString());
	
	if (itSes==sessions.end())
	{
		throw (IPSessionNotStablishedException(0, string("Session not stablished for device ")+
														StrUtils::decToString(id.id)+string(" from client ")+
														a->toString()));
	}
//	sessionInfo s=(*itSes).second;
//	if (s.stream->streamingMode==STREAM)
//	{
//		s.stopThread=true;
////		cout << "locloclocloc" << endl;
//		s.imageThreadLock->lock();
////		cout << "/locloclocloc" << endl;
//		sessions[a->toString()]=s;
////		cout << "unlocunlocunlocunloc" << endl;
//		s.imageThreadLock->unlock();
////		cout << "/unlocunlocunlocunloc" << endl;
//		itSes=sessions.find(a->toString());
////		cout << "joining " << (*itSes).second.threadId << endl;
//		s=(*itSes).second;
//		
//		switch (s.codec.fourcc)
//		{
//			case CODEC_JPEG:
//				jpegThread.join(s.threadId);
//				break;
//		
//			case CODEC_MPEG4:
//				mpeg4Thread.join(s.threadId);
//				break;
//		}
////		cout << "joined" << endl;
//	}

	sessions.erase(itSes);
}

IPCodecInfo VisioWaveImagePlugin::getCodecInUse(Address *a)
{
	sessionInfo s=getSessionForAddress(a);
	
	return IPCodecInfo(CODEC_JPEG, 50, 0);
}

void VisioWaveImagePlugin::setCodecInUse(IPCodecInfo codec, Address *a)
{
	sessionInfo s=getSessionForAddress(a);
}

IPFrameBufferSize VisioWaveImagePlugin::getFrameBufferSize(Address *a)
{
	cout<<"GFBS"<<endl;
	sessionInfo s=getSessionForAddress(a);

	IPFrameBufferSize fbSize;
	fbSize.nFrames=1; //s.fbSize;


	cout<<"/GFBS"<<endl;
	return fbSize;
}

void VisioWaveImagePlugin::setFrameBufferSize(IPFrameBufferSize size, Address *a)
{
//	sessionInfo s=getSessionForAddress(a);
//
////	cout << "--> VisioWaveImagePlugin::setFrameBufferSize(" << size.nFrames << ")" << endl;
//
//	byte **fb=new byte*[size.nFrames];
//	dword *fbL=new dword[size.nFrames];
//	bool *fbK=new bool[size.nFrames];
//	
//	if (fb==NULL)
//	{
//		throw (ImagePluginException(0, "Not enough memory to allocate frame buffer"));
//	}
//	
//	memset(fb, 0, sizeof(byte*)*size.nFrames);
//	memset(fbL, 0, sizeof(dword)*size.nFrames);
//	memset(fbK, false, sizeof(bool)*size.nFrames);
//	
//	int i=0;
//
//	for (; i<size.nFrames && i<s.fbSize; i++)
//	{
//		fb[i]=s.fb[i];
//		fbL[i]=s.fbLengths[i];
//		fbK[i]=s.fbKeyFrames[i];
//	}
//
//	// Nomes s'executara un dels dos seguents bucles
//	int j=i;
//	for (; i<size.nFrames; i++)
//	{
//		fb[i]=new byte[1]; //s.frameInfo.x*s.frameInfo.y*s.frameInfo.bpp/8];
//		if (fb[i]==NULL)
//		{
//			for (int k=j; k<i; k++)
//				delete [] fb[k];
//				
//			delete [] fb;
//			delete [] fbL;
//			throw (ImagePluginException(0, "Not enough memory to allocate frame buffer frame"));
//		}
//	}
//
//	sessionLock.lock();
//	for (; i<s.fbSize; i++)
//		if (s.fb[i]!=NULL)
//			delete [] s.fb[i];
//
//	delete [] s.fb;
//	delete [] s.fbLengths;
//	delete [] s.fbKeyFrames;
//	s.fbSize=size.nFrames;
//	s.fb=fb;
//	s.fbLengths=fbL;
//	s.fbKeyFrames=fbK;
//
//	sessions[a->toString()]=s;
//	sessionLock.unlock();


}

float VisioWaveImagePlugin::getFrameBufferPercentInUse(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	return 0.0f; //(float)s.fbUse/(float)s.fbSize;
}

int VisioWaveImagePlugin::getFrameBufferFramesInUse(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	return 0; //s.fbUse;
}

IPStreamingMode VisioWaveImagePlugin::getStreamingMode(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	return s.stream->streamingMode;
}

void VisioWaveImagePlugin::setStreamingMode(IPStreamingMode mode, Address *a)
{
	sessionInfo s=getSessionForAddress(a);
	
	s.stream->modifLock.lock();
	s.stream->streamingMode=mode;
	s.stream->modifLock.unlock();
}

IPFrame VisioWaveImagePlugin::getCompressedNextFrame(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	IPCodecInfo codecInfo=s.stream->codec; //getCodecInUse(a);

//	cout << "VisioWaveImagePlugin::getCompressedNextFrame() mode " << s.streamingMode << endl;

	// Com que accedim al frame buffer, comptem l'acces
	s.stream->modifLock.lock();

	if (s.stream->lastAccessToFB!=NULL)		// Thread en marxa
	{
		s.stream->lastAccessToFB->start();
		s.stream->modifLock.unlock();
	}
	else if (s.stream->threadId==-1)		// El thread s'ha parat
	{
//		s.fbUse=0;
		s.stream->modifLock.unlock();
//		cout << "gcnf cst " << (void*)s.stream << endl;
		createStreamThread(s.stream, a);
	}
	else
		s.stream->modifLock.unlock();

	while (s.stream->lastFrame==NULL)
	{
		// Esperem "1/2" frame (@25fps)
		usleep(20000);
//		cout << " -> frame buffer empty" << endl;
//		s=getSessionForAddress(a);
	}

	s.stream->modifLock.lock();

	bool isKey=s.stream->isKey; //fbKeyFrames[0];

	dword len=s.stream->lastFrameLen;
	byte *buf=new byte[len];
	memmove(buf, s.stream->lastFrame, len);

//	cout << "Frame read from buffer 0, " << s.fbUse+1 << " in buffer, " << len << " bytes" << endl;

	s.stream->modifLock.unlock();

	return IPFrame(s.stream->frameInfo, codecInfo, isKey, len, buf);
}

IPFrame VisioWaveImagePlugin::getDecompressedNextFrame(Address *a)
{
//	sessionInfo s=getSessionForAddress(a);

	throw(ImagePluginException(-1, "Not implemented"));
}

IPFramesPerSecond VisioWaveImagePlugin::getFPS(Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	s.stream->modifLock.lock();
	IPFramesPerSecond fps(s.stream->fps.whole, s.stream->fps.frac);
	s.stream->modifLock.unlock();
	
	return fps;
}

void VisioWaveImagePlugin::setFPS(IPFramesPerSecond fps, Address *a)
{
	sessionInfo s=getSessionForAddress(a);

	s.stream->modifLock.lock();

//	cout << "setFPS: current fps: " << s.stream->fps.whole << ", requested: " << fps.whole << endl;

	// Canviem si estem augmentant els FPS o si es el primer canvi.
	if (s.stream->stillDefaultFPS || fps.whole>s.stream->fps.whole || (fps.whole==s.stream->fps.whole && fps.frac>s.stream->fps.frac))
	{
		s.stream->fps.whole=fps.whole;
		s.stream->fps.frac=fps.frac;
	
		s.stream->stillDefaultFPS=false;
		s.stream->fpsChanged=true;
	}

	s.stream->modifLock.unlock();
}

string VisioWaveImagePlugin::getConfigFileName()
{
	return configFile;
}


#endif
