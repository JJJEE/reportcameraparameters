/*
 *  DecodeModuleInterface.cpp
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include <DecodeModule/DecodeModuleInterface.h>
#include <Sockets/SocketException.h>
#include <Utils/ServiceException.h>
#include <Utils/SerializedException.h>
#include <Utils/debugStackTrace.h>
#include <Utils/StrUtils.h>
#include <Utils/Timer.h>
#include <exception>
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

using namespace std;

extern bool operator < (struct timeval a, struct timeval b);
extern struct timeval operator - (struct timeval a, struct timeval b);

DecodeModuleInterface::DecodeModuleInterface(Address decodeModuleAddress, short decodeModuleType, Canis *cn): ModuleInterfaceGestor(decodeModuleAddress, decodeModuleType, cn, DecodeModuleInterface::startSessionServiceId, DecodeModuleInterface::endSessionServiceId)
{
	STACKTRACE_INSTRUMENT();

	numServices=serviceCount;
	services=new serviceDef[serviceCount];
	int id=DecodeModuleInterface::getDeviceInformationServiceId;
	sessionlessId.push_back(id);
	id=DecodeModuleInterface::startSessionServiceId;
	sessionlessId.push_back(id);
	startSessionId = DecodeModuleInterface::startSessionServiceId;
	

	services[DecodeModuleInterface::getDeviceInformationServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getDeviceInformation;
	services[DecodeModuleInterface::startSessionServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::startSession;
	services[DecodeModuleInterface::endSessionServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::endSession;
	services[DecodeModuleInterface::getCodecInUseServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getCodecInUse;
	services[DecodeModuleInterface::setCodecInUseServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::setCodecInUse;
	services[DecodeModuleInterface::getFrameBufferSizeServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getFrameBufferSize;
	services[DecodeModuleInterface::setFrameBufferSizeServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::setFrameBufferSize;
	services[DecodeModuleInterface::getFrameBufferPercentInUseServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getFrameBufferPercentInUse;
	services[DecodeModuleInterface::getFrameBufferFramesInUseServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getFrameBufferFramesInUse;
	services[DecodeModuleInterface::getStreamingModeServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getStreamingMode;
	services[DecodeModuleInterface::setStreamingModeServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::setStreamingMode;
	services[DecodeModuleInterface::getCompressedNextFrameServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getCompressedNextFrame;
	services[DecodeModuleInterface::getDecompressedNextFrameServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getDecompressedNextFrame;
	services[DecodeModuleInterface::getFPSServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::getFPS;
	services[DecodeModuleInterface::setFPSServiceId].call=(DefaultModuleGestorCall)DecodeModuleInterface::setFPS;

}


DecodeModuleInterface::~DecodeModuleInterface()
{
	STACKTRACE_INSTRUMENT();
}

SerializedException* DecodeModuleInterface::getSessionNotStablished()
{
	STACKTRACE_INSTRUMENT();

	IPSessionNotStablishedException sne(0,"Sessió no establerta");
	return sne.serialize();
}

Gestor* DecodeModuleInterface::createGestor(Address orig, int type, Canis *cn)
{
	STACKTRACE_INSTRUMENT();

	return new GestorImage(orig, type, cn);
}



RPCPacket* DecodeModuleInterface::getDeviceInformation(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPDeviceInfo di;
	di.toLocal((char*)params);
	
	IPDeviceInfo diRes=gi->getDeviceInformation(di);
	
	byte *res=(byte*)diRes.toNetwork();
	

	Address addr=_this->address;
	short type=_this->type;
	dword size=diRes.serializationSize();
	RPCPacket *p=new RPCPacket(addr, (word)-1, res, size, type, 0, true);
	//RPCPacket *p=new RPCPacket(_this->address, (word)-1, res, diRes.size(), _this->type, 0, true);
	
//	cout << "DecodeModuleInterface diRes" << endl << StrUtils::hexDump(string((char*)res, diRes.size())) << endl;
	
	delete [] res;
	
	return p;
}

RPCPacket* DecodeModuleInterface::startSession(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPDeviceID id(params);
	
	cout << "--> DMI::startSession:" << id.id << endl;
	gi->startSession(id);

	Address addr=_this->address;
	short type=_this->type;
	RPCPacket *p=new RPCPacket(addr, (word)-1, NULL, 0, type, 0, true);
	//RPCPacket *p=new RPCPacket(_this->address, (word)-1, NULL, 0, _this->type, 0, true);
	
	cout << "<-- DMI::StartSession ret" << endl;
	return p;
}

RPCPacket* DecodeModuleInterface::endSession(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPDeviceID id(params);

	cout << "DMI::endSession:" << id.id << endl;
	
	gi->endSession(id);
	

	Address addr=_this->address;
	short type=_this->type;
	RPCPacket *p=new RPCPacket(addr, (word)-1, NULL, 0, type, 0, true);
	//RPCPacket *p=new RPCPacket(_this->address, (word)-1, NULL, 0, _this->type, 0, true);
	
	return p;
}

RPCPacket* DecodeModuleInterface::getCodecInUse(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPCodecInfo ci=gi->getCodecInUse();
	
	byte *res=(byte*)ci.toNetwork();
	
	Address addr=_this->address;
	short type=_this->type;
	dword size=ci.serializationSize();
	
	RPCPacket *p=new RPCPacket(addr, (word)-1, res, size, type, 0, true);
	
	delete [] res;
	//RPCPacket *p=new RPCPacket(_this->address, (word)-1, (byte*)&ci, sizeof(ci), _this->type, 0, true);

	return p;
}

RPCPacket* DecodeModuleInterface::setCodecInUse(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPCodecInfo ci(params);
	
	gi->setCodecInUse(ci);
		

	Address addr=_this->address;
	short type=_this->type;
	RPCPacket *p=new RPCPacket(addr, (word)-1, NULL, 0, type, 0, true);
	//RPCPacket *p=new RPCPacket(_this->address, (word)-1, NULL, 0, _this->type, 0, true);

	return p;
}

RPCPacket* DecodeModuleInterface::getFrameBufferSize(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPFrameBufferSize fbs=gi->getFrameBufferSize();
	
//	cout << "getFBSize RPCData " << StrUtils::hexDump(string((char *)&fbs, sizeof(fbs))) << endl;
	
	
	Address addr=_this->address;
	short type=_this->type;
	byte *b=(byte*)fbs.toNetwork();
	dword size=fbs.serializationSize();
	RPCPacket *p=new RPCPacket(addr, (word)-1, b, size, type, 0, true);
	//	RPCPacket *p=new RPCPacket(_this->address, (word)-1, (byte*)&fbs, sizeof(fbs), _this->type, 0, true);
	
	delete [] b;
	
	return p;
}

RPCPacket* DecodeModuleInterface::setFrameBufferSize(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPFrameBufferSize fbs(params);
	
	gi->setFrameBufferSize(fbs);
		

	Address addr=_this->address;
	short type=_this->type;
	RPCPacket *p=new RPCPacket(addr, (word)-1, NULL, 0, type, 0, true);
	//RPCPacket *p=new RPCPacket(_this->address, (word)-1, NULL, 0, _this->type, 0, true);

	return p;
}

RPCPacket* DecodeModuleInterface::getFrameBufferPercentInUse(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	float fbpct=gi->getFrameBufferPercentInUse();
	
	Endian::to(Endian::xarxa, &fbpct, sizeof(fbpct));
	
	Address addr=_this->address;
	short type=_this->type;
	byte *b=(byte*)&fbpct;
	dword size=sizeof(fbpct);
	RPCPacket *p=new RPCPacket(addr, (word)-1, b, size, type, 0, true);
	//	RPCPacket *p=new RPCPacket(_this->address, (word)-1, (byte*)&fbpct, sizeof(fbpct), _this->type, 0, true);

	return p;
}

RPCPacket* DecodeModuleInterface::getFrameBufferFramesInUse(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	int fbfiu=gi->getFrameBufferFramesInUse();
	Endian::to(Endian::xarxa, &fbfiu, sizeof(fbfiu));
	
	
	Address addr=_this->address;
	short type=_this->type;
	byte *b=(byte*)&fbfiu;
	dword size=sizeof(fbfiu);
	RPCPacket *p=new RPCPacket(addr, (word)-1, b, size, type, 0, true);
	//	RPCPacket *p=new RPCPacket(_this->address, (word)-1, (byte*)&fbfiu, sizeof(fbfiu), _this->type, 0, true);

	return p;
}

RPCPacket* DecodeModuleInterface::getStreamingMode(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPStreamingMode sm=gi->getStreamingMode();

	Address addr=_this->address;
	short type=_this->type;
	byte *b=(byte*)sm.toNetwork();
	dword size=sm.serializationSize();
	RPCPacket *p=new RPCPacket(addr, (word)-1, b, size, type, 0, true);
	//	RPCPacket *p=new RPCPacket(_this->address, (word)-1, (byte*)&sm, sizeof(sm), _this->type, 0, true);

	delete [] b;
	
	return p;
}

RPCPacket* DecodeModuleInterface::setStreamingMode(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPStreamingMode sm(params);
	
	gi->setStreamingMode(sm);
		

	Address addr=_this->address;
	short type=_this->type;
	RPCPacket *p=new RPCPacket(addr, (word)-1, NULL, 0, type, 0, true);
	//RPCPacket *p=new RPCPacket(_this->address, (word)-1, NULL, 0, _this->type, 0, true);

	return p;
}

RPCPacket* DecodeModuleInterface::getCompressedNextFrame(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	try
	{
//		Timer t;
//		t.start();
//		cout << "--> [" << t.getStartSeconds() << "] DMI::gCNF[" << gi->getLocalAddr().toString() << " -> " << gi->getCurrentPluginAddress().toString() << "]" << endl; 
		IPFrame sm=gi->getCompressedNextFrame();
//		TimerInstant ti=t.time();
//		t.start();
//		cout << "\t/ [" << t.getStartSeconds() << "] GI::gCNF[" << gi->getLocalAddr().toString() << " -> " << gi->getCurrentPluginAddress().toString() << "]: " << ti.seconds() << "s" << endl;
		byte *res=(byte*)sm.toNetwork();
		
		Address addr=_this->address;
		short type=_this->type;
		dword size=sm.serializationSize();
	//	cout << "gcnf: " << size << endl;
		
		RPCPacket *p=new RPCPacket(addr, (word)-1, res, size, type, 0, true);
		//	RPCPacket *p=new RPCPacket(_this->address, (word)-1, res, sm.size(), _this->type, 0, true);
	
		delete [] res;
	
//		t.start();
//		cout << "<-- [" << t.getStartSeconds() << "] DMI::gCNF[" << gi->getLocalAddr().toString() << " -> " << gi->getCurrentPluginAddress().toString() << "]" << endl;
		return p;
	}
	catch (Exception &e)
	{
		cout << "\t\tDMI::gCNF[" << gi->getLocalAddr().toString() << " -> " << gi->getCurrentPluginAddress().toString() << "] " << e.getClass() << ": " << e.getMsg() << endl; 
		throw;
	}
	catch (std::exception &stde)
	{
		cout << "\t\tDMI::gCNF[" << gi->getLocalAddr().toString() << " -> " << gi->getCurrentPluginAddress().toString() << "] " << stde.what() << endl; 
		throw;
	}
	catch (...)
	{
		cout << "\t\tDMI::gCNF[" << gi->getLocalAddr().toString() << " -> " << gi->getCurrentPluginAddress().toString() << "] Unknown Exception" << endl; 
		throw;
	}
}

RPCPacket* DecodeModuleInterface::getDecompressedNextFrame(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPFrame sm=gi->getDecompressedNextFrame();
	byte *res=(byte*)sm.toNetwork();
	 
	
	Address addr=_this->address;
	short type=_this->type;
	dword size=sm.serializationSize();
	RPCPacket *p=new RPCPacket(addr, (word)-1, res, size, type, 0, true);
	//	RPCPacket *p=new RPCPacket(_this->address, (word)-1, res, sm.size(), _this->type, 0, true);

	delete [] res;

	return p;
}

RPCPacket* DecodeModuleInterface::getFPS(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPFramesPerSecond fps=gi->getFPS();
	
//	cout << "getFBSize RPCData " << StrUtils::hexDump(string((char *)&fbs, sizeof(fbs))) << endl;
	
	
	Address addr=_this->address;
	short type=_this->type;
	byte *b=(byte*)fps.toNetwork();
	dword size=fps.serializationSize();
	RPCPacket *p=new RPCPacket(addr, (word)-1, b, size, type, 0, true);
	//	RPCPacket *p=new RPCPacket(_this->address, (word)-1, (byte*)&fps, sizeof(fps), _this->type, 0, true);
	
	delete [] b;
	
	return p;
}

RPCPacket* DecodeModuleInterface::setFPS(DecodeModuleInterface *_this, GestorImage *gi, void *params)
{
	STACKTRACE_INSTRUMENT();

	IPFramesPerSecond fps(params);
	
	gi->setFPS(fps);

	Address addr=_this->address;
	short type=_this->type;
	RPCPacket *p=new RPCPacket(addr, (word)-1, NULL, 0, type, 0, true);
	//RPCPacket *p=new RPCPacket(_this->address, (word)-1, NULL, 0, _this->type, 0, true);

	return p;
}
