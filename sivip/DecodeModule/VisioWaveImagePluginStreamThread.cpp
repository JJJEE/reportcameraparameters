/*
 *  VisioWaveImagePlugin.cpp
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "VisioWaveImagePlugin.h"
#include <string.h>
#include <Utils/Canis.h>
#include <Utils/StrUtils.h>
#include <Plugins/GestorImageExceptions.h>
#include <Http/HttpClient.h>
#include <Http/HttpStream.h>
#include <XML/XML.h>
#include <XML/xmlParser.h>
#include <ServiceFinder/ServiceFinder.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <Utils/debugStackTrace.h>
#ifndef WIN32
#ifndef WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#else
extern "C" {
#include <jpeglib.h>
}
#include <Utils/WindowsDefs.h>
#include <Utils/WindowsInclude.h>
#include <Vfw.h>
#endif


#pragma mark *** Pool

#ifdef WIN32


BEGIN_MESSAGE_MAP (VWActivexWnd, CDialog)//CFrameWnd)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//{{AFX_MSG_MAP (CMyWindow)
//	ON_WM_LBUTTONDOWN ()
//	ON_COMMAND (ID_BUTTON_CLEAR, OnClear)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP ()

VWActivexWnd::VWActivexWnd(CWnd *Parent): CDialog(IDC_VWACTIVEXWND, Parent)//()
{
	// Make the main window and a button
	CRect rect (1, 1, 70, 40);

	cout<<"VWIP::Create this"<<endl;
	//Create (NULL, TEXT("Test"), WS_OVERLAPPEDWINDOW,
	//	rectDefault, NULL, NULL);
	cout<<"VWIP::new CVwactivexcomponent1"<<endl;
	//VWCtrl = new CVWActiveXComponent1;
	cout<<"VWIP::Create VWCtrl"<<endl;
//	VWCtrl->Create(TEXT("Clear"),WS_VISIBLE | WS_CHILD,
//		rect, this, IDC_VWACTIVEXCOMPONENT1);
	AfxEnableControlContainer();
	m_Ctrl.InitCOleCtrl(this);
}
/*
void VWActivexWnd::DDrawInit()
{
}
*/

void VWActivexWnd::DoDataExchange(CDataExchange* pDX)
{
	/*CFrameWnd*/ CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_VWACTIVEXCOMPONENT1, m_Ctrl);
}

void VisioWaveImagePluginJPEGStreamThread::frameCallback(HWND hWnd, LPVIDEOHDR lpVideoHdr)
{
	lpVideoHdr->lpData;
	lpVideoHdr->dwBytesUsed;
}

VisioWaveImagePluginJPEGStreamThread::VisioWaveImagePluginJPEGStreamThread(VisioWaveImagePlugin *imgPlg)
	:pDX(this, true), imgPlg(imgPlg), Wnd(imgPlg->m_pMainWnd)
{
	cout<<"VWIPJpeg::VWIPJpeg"<<endl;
	capWnd=capCreateCaptureWindow("Capture Window", 0, 0, 320, 240, this,0); 
	capSetCallbackOnFrame(capWnd, frameCallback);
//	DDrawInit();
//	VWCompInit();
}

/*
void VisioWaveImagePluginJPEGStreamThread::DDrawInit()
{
	HRESULT ddrval;
	// Create the main Direct Draw object 
	cout<<" DDrawCreate"<<endl;
	ddrval = DirectDrawCreateEx(NULL, (void **)&lpDD,IID_IDirectDraw7, NULL);
	if(ddrval != DD_OK) 
	{ 
		throw Exception(" Error creating DirectDraw object");
//		return(false);
	}
	
	// Set Cooperative level to allow Direct Draw to run full screen  
	cout<<"lpDD->setCooperativeLevel "<<endl;
*/
//	ddrval = lpDD->SetCooperativeLevel(imgPlg->m_pMainWnd->m_hWnd/*vwipwnd.m_hWnd ->wnd,DDSCL_ALLOWMODEX |*/,DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN);//DDSCL_NOWINDOWCHANGES|DDSCL_NORMAL );
/*	ddrval = lpDD->SetCooperativeLevel(imgPlg->m_pMainWnd->m_hWnd,DDSCL_NORMAL );
	cout<<" lpDD->setCooperativeLevel end "<<endl;
	if(ddrval != DD_OK) 
	{

		if(ddrval == DDERR_EXCLUSIVEMODEALREADYSET){
			cout<<" error DDERR_EXCLUSIVEMODEALREADYSET :"<<DDERR_EXCLUSIVEMODEALREADYSET<<endl;
	HRESULT ddrval2 = lpDD->SetCooperativeLevel(imgPlg->m_pMainWnd->m_hWnd,DDSCL_NOWINDOWCHANGES|DDSCL_NORMAL );
*/
//	HRESULT ddrval3 = lpDD->SetCooperativeLevel(imgPlg->m_pMainWnd->m_hWnd/*vwipwnd.m_hWnd ->wnd*/,DDSCL_ALLOWMODEX |DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN);//DDSCL_NOWINDOWCHANGES|DDSCL_NORMAL );
/*			cout<<" error set?"<<endl;
		}
		else
		{
		if(ddrval == DDERR_HWNDALREADYSET){cout<<" error DDERR_HWNDALREADYSET"<<endl;}
		if(ddrval == DDERR_HWNDSUBCLASSED){cout<<" error DDERR_HWNDSUBCLASSED"<<endl;}
		if(ddrval == DDERR_INVALIDOBJECT){cout<<" error DDERR_INVALIDOBJECT"<<endl;}
		if(ddrval == DDERR_INVALIDPARAMS){cout<<" error DDERR_INVALIDPARAMS"<<endl;}
		if(ddrval == DDERR_OUTOFMEMORY){cout<<" error DDERR_OUTOFMEMORY"<<endl;}
		lpDD->Release();
		throw Exception(" Error Setting DirectDraw object options");
		}
//		return(false);
	} // Set display mode to 320x240x16 

	/*	ddrval = lpDD->SetDisplayMode(320, 240, 16);
		if(ddrval != DD_OK) 
		{
			lpDD->Release();
			throw Exception(" Error Setting DirectDraw object options");
	//		return(false);
		} 
	*/	
//}


void init_destination(j_compress_ptr cinfo){}
boolean empty_output_buffer(j_compress_ptr cinfo){return true;}
void term_destination(j_compress_ptr cinfo){}


void *VisioWaveImagePluginJPEGStreamThread::execute(int id, void *args)
{
	HRESULT ddrval;
	int Width = 640;// ddsd2.dwWidth;
    int Height = 480;//ddsd2.dwHeight;


	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;
	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);  
	cinfo.input_components = 3;	
	cinfo.in_color_space = JCS_RGB; 
	jpeg_set_defaults(&cinfo);
	cinfo.dest=new jpeg_destination_mgr;
	cinfo.dest->empty_output_buffer = &empty_output_buffer;
	cinfo.dest->init_destination = &init_destination;
	cinfo.dest->term_destination = &term_destination;


	VisioWaveImagePluginStreamThread::imageThreadArgs *ita=(VisioWaveImagePluginStreamThread::imageThreadArgs *)args;
    VisioWaveImagePlugin *plugin=(VisioWaveImagePlugin *)ita->plugin;
    VisioWaveImagePlugin::streamInfo *s=(VisioWaveImagePlugin::streamInfo*)ita->s;
	string str=string("ipm://")+s->ip+string(":")+StrUtils::decToString(s->port);
	//const wchar_t *wc=(const wchar_t *)str.c_str();
	const char *wc=str.c_str();
	CA2T p(wc);
	/*(LPTSTR)s->ip.c_str;*/ 
	//(LPTSTR)(_tcschr(wc,(wchar_t)_T('\0')));

	long iId=0;

	Wnd.DoModal();
	iId = Wnd.m_Ctrl.AddSource(p);
	if(iId>=0 ) 
	{ 
		Wnd.m_Ctrl.SetActiveSourceId(iId);
		Wnd.m_Ctrl.ConnectId(iId, Wnd.m_Ctrl.GetSourceType(iId), FALSE, 3000); 
		//m_Conn.EnableWindow(FALSE); 
	} 
	else	
	{
		DWORD res = Wnd.m_Ctrl.GetLastError();
		CString cstr=Wnd.m_Ctrl.GetErrorText(res);
		cout<<" VWIP_JPEGStrThr GetDC error:"<<res<<":"<<cstr<<endl;
		throw Exception(" VisioWaveImagePluginJPEGStreamThread::Error addSource");
	}		

	s->frameInfo.x=Width ;
	s->frameInfo.y=Height;

	try
	{
		while (true)
		{
			s->imageThreadLock->lock();
	
			string frameHdr;
			string *frameHdrPtr=&frameHdr;
			

			int size=Width*Height*4;
			char *jpeg=new char[size];
			char *lpvBits;

			cinfo.dest->next_output_byte=(JOCTET*)jpeg;
			cinfo.image_width = Width; 	
			cinfo.image_height = Height;
			cinfo.dest->free_in_buffer=size;

//			cout<<" sizes w:"<<Width<<"h:"<<Height<<"t:"<<lpbi->bmiHeader.biSizeImage<<endl;
			jpeg_start_compress(&cinfo,true);
			for(int i=0;i<Height;i++)
			{
				JSAMPROW row=(JSAMPROW)(((char*)lpvBits)+(i*Width)*2);
				jpeg_write_scanlines(&cinfo, &row,1);
			}
			jpeg_finish_compress(&cinfo);
			

		//	cout << "[JPEG] Read frame Header (len " << frameHdr.length() << "): " << frameHdr <<" -- "<< endl;
		//	byte *newFrame=new byte[frame.length()];
		//	memmove(newFrame, frame.c_str(), frame.length());
			byte *newFrame=(byte*)jpeg;//lpvBits; 
			dword len=/*lpbi->bmiHeader.biSizeImage*/size-cinfo.dest->free_in_buffer;//lpbi->bmiHeader.biSizeImage;
			s->modifLock.lock();
			
			TimerInstant ti=ita->timeoutTimer->time();
			if (ti.seconds() > s->streamTimeout)
			{
				// Evitem l'aparicio d'N errors ara que encara no ho gestionem
				cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Thread Timeout occurred, stream is not being used!" << endl;
				cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Stopping thread..." << endl;
				s->stopThread=true;
			}
		
			if (s->stopThread)
			{
				// Si estem parant per timeout, cal deixar-ho tot llest pq el proxim get(De)compressedNextFrame el pugui iniciar
				s->stopThread=false;
				delete s->lastAccessToFB;
				s->lastAccessToFB=NULL;
				s->threadId=-1;
				
				delete ita;
//				delete stream;
//				delete [] newFrame;
				
				s->modifLock.unlock();
				s->imageThreadLock->unlock();
				return NULL;
			}
			
//			dword len=frame.length();
			byte *f=s->lastFrame;

//			cout<<"set frame:"<<(void*)s<<" lastFrame:"<<(void*)&(s->lastFrame)<<" frame:"<<(void*) newFrame<<endl;

			s->lastFrameLen=len;
			s->lastFrame=newFrame;
			if (f!=NULL)
			{
				delete [] f;
			}
	
			s->modifLock.unlock();
	
			s->imageThreadLock->unlock();
		}
	}
	catch (Exception &e)
	{
		// Simulem un timeout....
		s->modifLock.lock();
		
		cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Exception: " << e.getClass() << ": " << e.getMsg() << endl;
			cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Stopping thread..." << endl;
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		Wnd.m_Ctrl.DisconnectId(iId,Wnd.m_Ctrl.GetSourceType(iId));
		Wnd.m_Ctrl.RemoveSourceId(iId);

		delete ita;
//		delete stream;
		
		s->modifLock.unlock();
		s->imageThreadLock->unlock();
		return NULL;
	}
	catch (...)
	{
		// Simulem un timeout....
		s->modifLock.lock();
		
		cout << "VisioWaveImagePluginJPEGStreamThread::execute(): unknown exception" << endl;
			cout << "VisioWaveImagePluginJPEGStreamThread::execute(): Stopping thread..." << endl;
				
		s->stopThread=false;
		delete s->lastAccessToFB;
		s->lastAccessToFB=NULL;
		s->threadId=-1;

		Wnd.m_Ctrl.DisconnectId(iId, Wnd.m_Ctrl.GetSourceType(iId));
		Wnd.m_Ctrl.RemoveSourceId(iId);

		delete ita;
//		delete stream;
		
		s->modifLock.unlock();
		s->imageThreadLock->unlock();
		return NULL;
	}

	Wnd.m_Ctrl.DisconnectId(iId, Wnd.m_Ctrl.GetSourceType(iId));
	Wnd.m_Ctrl.RemoveSourceId(iId);


	delete ita;
	return NULL;
}
#endif
