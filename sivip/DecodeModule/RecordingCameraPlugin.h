/*
 * SonyImagePlugin.h
 *  
 *
 *  Created by David Marí Larrosa on 16/03/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SIRIUS_DECODEMODULE_RECORDINGCAMERAPLUGIN_H_
#define SIRIUS_DECODEMODULE_RECORDINGCAMERAPLUGIN_H_


#include <Utils/debugNew.h>
#include <Plugins/PluginImage.h>
#include <Sockets/Address.h>
#include <Endian/Endian.h>
#include <Utils/Types.h>
#include <Utils/RPC.h>
#include <Utils/Timer.h>
#include <Utils/RPCPacket.h>
#include <Utils/ServiceException.h>
#include <Utils/PtrPool.h>
#include <Http/HttpStream.h>
#include <Threads/Thread.h>
#include <Threads/Mutex.h>
#include <ModuleAccess/RecordingModuleAccess.h>
#include <map>

using namespace std;

class RecordingCameraPluginStreamThread : public Thread
{
public:
	struct imageThreadArgs
	{
		void *s;
		Address a;
		void *plugin;
		Timer *timeoutTimer;
	};

	void starRecordingtSession(RecordingModuleAccess *rma, int devId);

	void* execute(int id, void *args);
};



class RecordingCameraPlugin : public PluginImage
{
	friend class RecordingCameraPluginStreamThread;
	
protected:
	static PtrPool ptrPool;
	static RPC* getRPC(Address *a);

	// resolucions i modes de video etc
public:
	static const int NUM_VIDEOSTD=2;
	static const int NUM_RESOLUTIONS=9;
	
	enum Model {UNKNOWN=0L, RECORDING};
	enum {CODEC_JPEG=CODEC_FOURCC('j','p','e','g'), CODEC_MPEG4=CODEC_FOURCC('m','p','4','v'),
			CODEC_H264=CODEC_FOURCC('h','2','6','4')};
	enum {PAL=0L, NTSC};
	enum {RES_AUTO_763x480=0L, RES_FRAME_763x480, RES_FIELD_763x480, RES_AUTO_640x480, RES_FRAME_640x480, 
			RES_FIELD_640x480, RES_320x240, RES_160x120, RES_OPTION, RES_AUTO_736x544=0L, RES_FRAME_736x544,
			RES_FIELD_736x544};
		
	static const IPFrameInfo resolutions[NUM_VIDEOSTD][NUM_RESOLUTIONS];
	int currentTvMode, currentResolution;

public:
	struct streamInfo
	{
		bool stopThread;	
		IPDeviceInfo devInfo;
		Model model;
		string ip;
		int port;
		IPStreamingMode streamingMode;
		IPFrameInfo frameInfo;
		IPCodecInfo codec;
		Timer *lastAccessToFB;		// Temporitzador des de l'ultim acces a l'stream de video, si es que s'esta fent streaming
		double streamTimeout;		// Timeout de cancel·lacio d'stream, si no hi ha accessos :)
		int threadId;				// Id de thread que fa l'streaming. Si es 0, es que no s'esta fent streaming :P
		Mutex *imageThreadLock;
	//	struct RecordingModuleInterface::listRecordingsParams::recording recording;
		struct
		{
			int id;
			unsigned long start;
			int length;
		} recording;
		struct
		{
			word whole;
			word frac;
		} fps;
		
		//single frame (JPEG)
		byte *lastFrame;
		dword lastFrameLen;
		
		//frame stream (MPEG4)
		string streamFrames;
		int nframes;
		unsigned int keyFrame; // numero de key frames rebuts
		string frameStreamHeader;

		bool isKey;
		bool stillDefaultFPS;
		bool fpsChanged;
				
		Mutex modifLock;

#ifdef WIN32
		struct jpeg_compress_struct *cinfo;
		struct jpeg_error_mgr *jerr;
		struct jpeg_destination_mgr *dst;
		JSAMPROW row;
#endif

		streamInfo() : stopThread(false), threadId(-1), ip(string("")), 
			port(0), lastAccessToFB(NULL), streamTimeout(15.0),	lastFrame(NULL),
			lastFrameLen(0), isKey(false), fpsChanged(false), stillDefaultFPS(true),
			streamFrames(string("")), frameStreamHeader(string("")), nframes(0), keyFrame(1)
#ifdef WIN32
			,cinfo(NULL), jerr(NULL), dst(NULL)
#endif
		{
			fps.whole=25;
			fps.frac=0; 
		};
	};
protected:

	class sessionInfo
	{
		public:
			IPDeviceInfo devInfo;
			streamInfo *stream;
			unsigned int lastFrame; // ultim key frame rebut
			int frameIndex; //index dins els P frames
			
			sessionInfo() : stream(NULL), lastFrame(0), frameIndex(0) {}
			sessionInfo(const sessionInfo &si)
			{
				this->stream=si.stream;
				this->lastFrame=si.lastFrame;
				this->frameIndex=si.frameIndex;
				this->devInfo=si.devInfo;
			}
	};

	Mutex sessionLock;
	map<string,sessionInfo> sessions;
	
	Mutex streamsLock;
	map<IPDeviceID,streamInfo*> streams;
	
	sessionInfo getSessionForAddress(Address *a);
	streamInfo* getStreamInfoForDevice(int id);

	// Xixes internes de funcionament, temes de cerca de subsistemes, etc
	static const int centralDirectoryTypeId=-1;

	Address centralDirectoryAddr;

	string configFile;

	 RecordingCameraPluginStreamThread streamThread;
	
	void createStreamThread(streamInfo *s, Address *a);
	struct RMListRecordingsParams::recording getRecording(streamInfo *sinfo);
public:
	static const int DEFAULT_FBSIZE=1;
/*
#ifdef WIN32
	void setFrame(SonyImagePlugin::streamInfo* s, BITMAPINFO bmpInfo, char* bmp);
#endif
*/
public:
	RecordingCameraPlugin(Address addr);
	RecordingCameraPlugin(string file);

	virtual IPDeviceInfo getDeviceInformation(IPDeviceInfo dev, Address *a);
	virtual void startSession(IPDeviceID id, Address *a);
	virtual void endSession(IPDeviceID id, Address *a);

	virtual IPCodecInfo getCodecInUse(Address *a);
	virtual void setCodecInUse(IPCodecInfo codec, Address *a);

	virtual IPFrameBufferSize getFrameBufferSize(Address *a);
	virtual void setFrameBufferSize(IPFrameBufferSize size, Address *a);
	virtual float getFrameBufferPercentInUse(Address *a);
	virtual int getFrameBufferFramesInUse(Address *a);

	virtual IPStreamingMode getStreamingMode(Address *a);
	virtual void setStreamingMode(IPStreamingMode mode, Address *a);

	virtual IPFrame getCompressedNextFrame(Address *a);
	virtual IPFrame getDecompressedNextFrame(Address *a);

	virtual IPFramesPerSecond getFPS(Address *a);
	virtual void setFPS(IPFramesPerSecond fps, Address *a);
	
	string getConfigFileName();
};
#endif

