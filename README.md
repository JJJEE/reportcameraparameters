# ReportCameraParameters
#Este proyecto include dos aplicativos , ActualizaBDCamaras y CameraParametersServiceBBDD, que sirven para obtener los datos técnicos de 
#las cámaras de la instalación (codec, resx y resy). ActualizaBDCamaras actua en el background, accediendo a cada cámara cada 2 segundos
#(parametrizable) y refrescando en la BD video los valores de cada parámetro. CameraParametersServiceBBDD está construido como un servidor
#al que se dirigen las consultas hechas desde otros equipos (desde un browser, o desde una aplicación) utilizando como identificador de la
#cámara la ID con que está guardada en la BD y que son respondidas con un mensaje con formato xml en el que se incluyen los datos técnicos
#de la cámara en cuestión.
