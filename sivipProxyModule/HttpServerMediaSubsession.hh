#ifndef _HTTP_SERVER_MEDIA_SUBSESSION_HH
#define _HTTP_SERVER_MEDIA_SUBSESSION_HH

#include <OnDemandServerMediaSubsession.hh>
#include <HttpInput.hh>

class HttpServerMediaSubsession: public OnDemandServerMediaSubsession 
{

public:
static HttpServerMediaSubsession* 
	createNew( UsageEnvironment& env, HttpInput& httpInput, unsigned estimatedBitrate,
	   		 	Boolean iFramesOnly = False,
            	double vshPeriod = 5.0 /* how often (in seconds) to inject a Video_Sequence_Header, if one doesn't already appear in the stream */
			  );


protected: // we're a virtual base class
  HttpServerMediaSubsession(UsageEnvironment& env, HttpInput& httpInput, unsigned estimatedBitrate, Boolean iFramesOnly, double vshPeriod);
  virtual ~HttpServerMediaSubsession();

protected:
  HttpInput& fHttpInput;
  unsigned fEstimatedKbps;

private:
  Boolean fIFramesOnly;
  double fVSHPeriod;

  // redefined virtual functions
  virtual FramedSource* createNewStreamSource(unsigned clientSessionId, unsigned& estBitrate);
  virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock, unsigned char rtpPayloadTypeIfDynamic, FramedSource* inputSource);

};

#endif
