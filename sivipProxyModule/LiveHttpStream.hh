#include <Http/HttpClient.h>	
#include <Http/HttpStream.h>
#include <video/H264FrameQueue.h>
#include <video/MPEG4FrameQueue.h>
#include <time.h>
#include <sys/time.h>	
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/stat.h>
#include <InternalThread.hh>
#include <Live555Event.hh>
#include <FramedSource.hh>

#ifndef _LiveHttpStream_HH
#define _LiveHttpStream_HH

class LiveHttpStream: public InternalThread, public Live555Event
{
	private: 
		HttpClient 	m_httpCli;			// Cliente para establecer conexion http
		HttpStream* m_stream;			// Obtiene el streaming por http
		FrameQueue* m_codecFrame;		// Colad de Frames completos, mpeg, h264
		string 		m_codec;			// Tipo de codec que se esta usando: mpeg4, h264
		uint32_t	m_status;			// 200 si la conexion con la camara fue correcta
		
		void run();

	public:
		LiveHttpStream();
		LiveHttpStream(string codec);
		~LiveHttpStream();	
		
		void setUrl(string url);
		const Frame *getFrame();
		const uint32_t getStatus();
		const char* getCodec() const;
		void getChunk();	
		const uint32_t getQueueSize() const;
		bool frameIsReady();	
		
};

#endif
