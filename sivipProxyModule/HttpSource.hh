#ifndef _HTTP_SOURCE_HH
#define _HTTP_SOURCE_HH

#ifndef _FRAMED_SOURCE_HH
#include <FramedSource.hh>
#endif

#include <LiveHttpStream.hh>

class HttpSource: public FramedSource {
public:
	static HttpSource* createNew(UsageEnvironment& env, LiveHttpStream* E_Source);
	static unsigned GetRefCount();
  
public:
	EventTriggerId eventTriggerId;

protected:
	HttpSource(UsageEnvironment& env, LiveHttpStream* source);
	// called only by createNew(), or by subclass constructors
	virtual ~HttpSource();

private:
	// redefined virtual functions:
	virtual void doGetNextFrame();
	virtual void doStopGettingFrames();
	//virtual void doStopGettingFrames();

private:
	static void deliverFrame0(void* clientData);
	void deliverFrame();
  	
private:
	static unsigned referenceCount; // used to count how many instances of this class currently exist
	LiveHttpStream  *liveSource;
	const Frame * currentFrame;
	uint64_t lastFrameId;
};

#endif
