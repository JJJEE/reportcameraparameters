#include <SivipSource.hh>
#include <GroupsockHelper.hh> // for "gettimeofday()"
#include <SivipStream.hh>


unsigned SivipSource::referenceCount 		= 0;

SivipSource* SivipSource::createNew(UsageEnvironment& env, SivipStream* E_Source, unsigned clientSessionId) 
{
	//printf("SivipSource::createNew()\n");
	return new SivipSource(env, E_Source, clientSessionId);
}

SivipSource::SivipSource(UsageEnvironment& env, SivipStream* E_Source, unsigned clientSessionId) : FramedSource(env)
{
	//printf("SivipSource::SivipSource()\n");
	sivipStream 	=  E_Source;

	currentFrame 	= 0;

	if (referenceCount == 0) { }
	++referenceCount;

	eventTriggerId = envir().taskScheduler().createEventTrigger(deliverFrame0);
	
	m_clientSessionId 	= clientSessionId;

	if(m_clientSessionId != 0)
	{
		TaskScheduler* scheduler;
		sivipStream->setEventId(eventTriggerId);
		scheduler 	= &envir().taskScheduler();
		sivipStream->setScheduler(scheduler);
		sivipStream->setSource(this);

		if(	sivipStream->isLiveL9()	||
				sivipStream->isLiveL9EMB()	)
		{
			sivipStream->start();
		}		
	}

}

SivipSource::~SivipSource() 
{
	//printf("SivipSource::~SivipSource()\n");
	--referenceCount;

	envir().taskScheduler().deleteEventTrigger(eventTriggerId);
	eventTriggerId = 0;

	if( currentFrame != 0 ) 	delete currentFrame;
	handleClosure(this);
}

SivipStream* SivipSource::getStreamObject() const
{
	//printf("SivipSource::getStreamObject()\n");
	return sivipStream;
}

unsigned SivipSource::GetRefCount() 
{
	//printf("SivipSource::GetRefCount()\n");
	return referenceCount;
}

void SivipSource::doGetNextFrame()
{
	//printf("SivipSource::doGetNextFrame()\n");

    if( sivipStream->frameIsReady() )            
    {
        deliverFrame();
    }
}

void SivipSource::doStopGettingFrames()
{
   // printf("SivipSource::doStopGettingFrames() \n");
	if(	sivipStream->isLiveL9()		|| 
			sivipStream->isLiveL9EMB()	)
	{
		sivipStream->stop();
	}	
	else
	{
		sivipStream->signalPause();
	}
	//printf("SivipSource::doStopGettingFrames() end\n");
	fflush(stdout);
}


void SivipSource::deliverFrame0(void* clientData)
{
	//printf("SivipSource::deliverFrame0()\n");
  ((SivipSource*)clientData)->deliverFrame();
}

void SivipSource::deliverFrame()
{
	//std::cout << "SivipSource::deliverFrame()" << std::endl;

    if (!isCurrentlyAwaitingData())
    {
    	return; // we're not ready for the data yet
    }
    if (!sivipStream->frameIsReady())
    {
    	return;
    }

	sivipStream->setFrameState(false);
	
	currentFrame			= sivipStream->getFrame();	

	if ( currentFrame != 0)
	{
		u_int8_t* newFrameDataStart 	= (uint8_t *) currentFrame->getData();
		unsigned  newFrameSize 		= currentFrame->getSize();	

		if (newFrameDataStart != 0) 
		{
			/* This should never happen, but check anyway.. */
			if (newFrameSize > fMaxSize) 
			{
				printf("SivipSource::deliverFrame(): WARNING - Frame too big (%u)\n", newFrameSize);
				fFrameSize = fMaxSize-1;
				fNumTruncatedBytes = newFrameSize - fMaxSize;
			} 
			else
			{
				fFrameSize = newFrameSize;
			
				if( 	sivipStream->isLiveL9() 	|| 
						sivipStream->isLiveL9EMB()	) 
				{
					fisLiveFrame = True;
					gettimeofday(&fPresentationTime, NULL);
				}
				else if( sivipStream->isReplay() )
				{
					fisLiveFrame = False;
					sivipStream->getReplayTime(&fPresentationTime);
				}

				memmove(fTo, newFrameDataStart, fFrameSize);
				//printf("SivipSource::deliverFrame(): fTo Pointer = 0x%08x \n", fTo);
				//printf("SivipSource::deliverFrame(): FramedSource = 0x%08x \n", this);
			}
			if( currentFrame != 0) 
			{ 
				delete currentFrame;
				currentFrame = 0;		
			}
		}
		else 
		{
			fFrameSize	= 0;
			fTo			= NULL;
			printf("SivipSource::deliverFrame(): NO DATA!\n ");		
			//handleClosure(this);
		}
	}

	FramedSource::afterGetting(this);
}

