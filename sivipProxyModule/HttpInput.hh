#ifndef _HTTP_INPUT_HH
#define _HTTP_INPUT_HH

#include <MediaSink.hh>
#include <LiveHttpStream.hh>
#include <HttpInput.hh>
#include <HttpSource.hh>

#define VIDEO_MAX_FRAME_SIZE 10000000
class HttpInput: public Medium 
{
public:
	static HttpInput* createNew(UsageEnvironment& env, LiveHttpStream* phttpStream);
	FramedSource* videoSource();
	const char* getCodec() const;

	// Functions to set the optimal buffer size for RTP sink objects.
	// These should be called before each RTPSink is created.
	inline void setVideoRTPSinkBufferSize() { OutPacketBuffer::maxSize = VIDEO_MAX_FRAME_SIZE; }

private:
	HttpInput(UsageEnvironment& env, LiveHttpStream* phttpStream); // called only by createNew()
	virtual ~HttpInput();

private:
	Boolean fHaveInitialized;
	FramedSource* fOurVideoSource;
	LiveHttpStream *httpStream;
};



#endif
