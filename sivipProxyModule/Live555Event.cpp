#include <Http/HttpClient.h>	
#include <Http/HttpStream.h>
#include <video/H264FrameQueue.h>
#include <video/MPEG4FrameQueue.h>
#include <time.h>
#include <sys/time.h>	
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>
#include <Live555Event.hh>

Live555Event::Live555Event()
{
	m_eventId 		= 0;
	m_scheduler 	= 0;
	m_device 		= 0;
}

Live555Event::~Live555Event()
{

}	

void Live555Event::setEventId( uint32_t id )
{
	m_eventId 	= id;
}

void Live555Event::setScheduler(TaskScheduler* scheduler)
{
	m_scheduler 	= scheduler;
}

void Live555Event::setSource(FramedSource* device)
{
	m_device 	= device;
}

const uint32_t Live555Event::getEventId() const
{
	return m_eventId;
}

const TaskScheduler* Live555Event::getScheduler()
{
	return m_scheduler;
}

void Live555Event::notifyNewFrame()
{
	//printf("Live555Event::notifyNewFrame()\n");
	if(m_scheduler !=0 && m_device != 0 && m_eventId !=0)
	{
		m_scheduler->triggerEvent( m_eventId, m_device);
	}
	else
	{
		printf("Live555Event::notifyNewFrame() - params are not set\n");
	}
}

