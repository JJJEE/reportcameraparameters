#ifndef _DYNAMIC_RTSP_SIVIP_SERVER_HH
#define _DYNAMIC_RTSP_SIVIP_SERVER_HH

#ifndef _RTSP_SERVER_SUPPORTING_HTTP_STREAMING_HH
#include "RTSPServerSupportingHTTPStreaming.hh"
#endif
#include <iostream>
#include <string.h>
#include <map>
#include <set>
#include <stdio.h>
#include <stdint.h>

/**
* Responde a las peticiones de RTSP
*/
class DynamicRTSPSivipServer: public RTSPServer 
{

protected:
  DynamicRTSPSivipServer(	UsageEnvironment& env, 
  									int ourSocket, 
									Port ourPort,
		    						UserAuthenticationDatabase* authDatabase, 
		    						unsigned reclamationTestSeconds	);

  virtual ~DynamicRTSPSivipServer();

protected: 
  virtual ServerMediaSession*
  lookupServerMediaSession(	char const* streamName, 
  										Boolean isFirstLookupInSession	);

public:
	static DynamicRTSPSivipServer* createNew(	UsageEnvironment& env, 
															Port ourPort,
					  										UserAuthenticationDatabase* authDatabase,
					  										unsigned reclamationTestSeconds = 65	);

};

#endif
