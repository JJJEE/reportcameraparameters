#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <stdio.h>
#include <unistd.h>

class XmlParser
{
	private:
		std::string m_xml;
		std::string *m_from;
		int m_sizeFrom;
		std::string *m_tagContent;
		int m_sizeContent;

		void resize(std::string * &array, int &size);
		void prueba_borrado(std::string * &array);

	public:
		XmlParser();
		XmlParser(const char* p_xml , int size);
		~XmlParser();

		void setXml(const char*, int size);
		const char* getXml() const;
		void addTag(const char*);
		void cleanTag();
		int read();
		const char* getTag(int tagNum);

};
