#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>
#include <LiveHttpStream.hh>
#include <HttpInput.hh>
#include <HttpServerMediaSubsession.hh>
#include <SivipStream.hh>
#include <SivipInput.hh>
#include <SivipServerMediaSubsession.hh>
#include <DynamicRTSPSivipServer.hh>
#include <fstream>
#include <XmlParser.h>
#include <Utils/DBConnection.hh>

UsageEnvironment* env;

const char* readFile(const char* name); 

int main(int argc, char** argv) 
{
	string confFile  = "SivipRtspProxyServerConfig.xml";
	Canis* myCanis = 0;
	int port	= 0;

	string xmlConf 	= readFile(confFile.c_str());
	XmlParser myXML(xmlConf.c_str(), xmlConf.size());
	myXML.addTag("<Rtsp>");
	myXML.addTag("<Port>");

	myXML.read();
	string strPort 	= myXML.getTag(0);
	myXML.cleanTag();

	port 	= atoi(strPort.c_str());
	printf("Rtsp Port: %d\n", port );
	
	myXML.addTag("<Rtsp>");
	myXML.addTag("<IP>");
	myXML.read();
	string ip 	= myXML.getTag(0);
	myXML.cleanTag();

	printf("Server Ip: %s\n", ip.c_str() );

	TaskScheduler* scheduler 	= BasicTaskScheduler::createNew(1000);	//1000 us
	env = BasicUsageEnvironment::createNew(*scheduler);

	UserAuthenticationDatabase* authDB = NULL;

	RTSPServer* rtspServer = DynamicRTSPSivipServer::createNew(	*env, 
																					port, 
																					authDB	);	
	if (rtspServer == NULL) 
	{
		*env << "Failed to create RTSP server: " << env->getResultMsg() << "\n";
		exit(1);
	}

	string url;

	url 	= "rtsp://" + ip + ":" + strPort.c_str() + "/ci_cameraID_live";
	*env << "Play live streams from this server using the URL:\n\t" << url.c_str() << "\n";

	url 	= "rtsp://" + ip + ":" + strPort.c_str() + "/ci_cameraID";
	*env << "Ask for a replay stream from this server using the URL\n\t" << url.c_str() << "\n";

	char const* descriptionString = "Session streamed by \"SIVIP Rtsp Proxy\"";

	env->taskScheduler().doEventLoop(); 

	return 0; 
}



const char* readFile(const char* name)
{
	string strFile, line;
	string newline = "\n";
	fstream thisFile;

	thisFile.open(name, ios::in); 	

	if(thisFile.is_open() )
	{
		while( getline(thisFile, line) )
		{
			strFile += line + newline;
		}
	}
	else
	{
		printf("Error abriendo fichero de configuración %s\n", name);
		return 0;
	}
	
	thisFile.close();

	return strFile.c_str();
}
