#include <iostream>
#include <string>
//#include <cstring>
#include <stdio.h>
#include <unistd.h>
#include <XmlParser.h>

XmlParser::XmlParser()
{
	m_from 			= 0;
	m_tagContent 	= 0;
	m_sizeFrom 		= 0;
	m_sizeContent 	= 0;
}

XmlParser::XmlParser(const char* p_xml , int size)
{
	//printf("XmlParser::XmlParser() \n");
	m_from 			= 0;
	m_tagContent 	= 0;
	m_sizeFrom 		= 0;
	m_sizeContent 	= 0;
	std::string aux(p_xml, size);
	m_xml 	= aux;
}

XmlParser::~XmlParser()
{
	if(m_from != 0) delete[] m_from;
	if(m_tagContent != 0) delete[] m_tagContent;
}

void XmlParser::cleanTag()
{
	//m_from 			= 0;
	//m_tagContent 	= 0;
	m_sizeFrom 		= 0;
	m_sizeContent 	= 0;


	if(m_from != 0) prueba_borrado(m_from);
	if(m_tagContent != 0) prueba_borrado(m_tagContent);
	m_from 			= 0;
	m_tagContent 	= 0;
	//if(m_from != 0) delete[] m_from;
	//if(m_tagContent != 0) delete[] m_tagContent;
}

void XmlParser::setXml(const char* p_xml, int size)
{
	cleanTag();
	std::string aux(p_xml, size);
	m_xml 	= aux;
}

const char* XmlParser::getXml() const
{
	return m_xml.c_str();
}

void XmlParser::addTag(const char* p_tag)
{
	//printf("XmlParser::addTag() \n");
	resize(m_from, m_sizeFrom);

	m_from[m_sizeFrom - 1] 	= p_tag;
}

int XmlParser::read()
{
	//printf("XmlParser::read() \n");
	int start 		= 0;
	int stop		= 0;
	int lastStop	= 0;
	int i			= 0;

	std::string to 		= "</" + m_from[m_sizeFrom - 1].substr(1);  // Fin del ultimo tag
	
	if( m_sizeFrom < 1)
	{
		printf("XmlParser::read() - Error: Tag was not set.\n");
		return 0;
	}

	while(1) 
	{
		// Buscamos configuracion
		start 		= m_xml.find( m_from[0].c_str(), lastStop + to.size() );

		if(start != std::string::npos) // Si encontramos el tag padre
		{
			start			+=  m_from[0].size();
			for(i = 1; i < m_sizeFrom; i++) // Buscamos los tag hijos
			{
				start 		= m_xml.find( m_from[i].c_str(), start);
				if(start != std::string::npos)
				{
					start			+=  m_from[i].size(); // Posición del ultimo hijo encontrado
				}

			}
		}
		
		if(start != std::string::npos) // Almacenamos el contenido del hijo final
		{
			stop			= m_xml.find(to.c_str(), start); // Posición del fin del ultimo hijo encontrado	

			if(stop != std::string::npos && stop > lastStop)
			{
				lastStop	 = stop;
				if(start <= stop)
				{
					//printf("XmlParser::read() \n");
					resize(m_tagContent, m_sizeContent);
					m_tagContent[m_sizeContent - 1] 	= m_xml.substr(start, (stop-start)) ;
				}
			}
			else 
			{ 
				//printf("XmlParser::read() - read %d tags.\n", m_sizeContent);			
				break;
			}
		}
		else 
		{ 
			//printf("XmlParser::read() - read %d tags.\n", m_sizeContent);			
			break;
		}
	}

	return m_sizeContent;
}

const char* XmlParser::getTag(int tagNum)
{
	//printf("XmlParser::getTag() \n");
	if(tagNum < m_sizeContent )
	{
		return m_tagContent[tagNum].c_str();
	}
	else
	{
		return 0;
	}
}

void XmlParser::resize(std::string * &array, int &size)
{
	//printf("XmlParser::resize() \n");
	if( size > 0 )
	{
		size_t newSize 	= size + 1;
		std::string* newArr = new std::string[newSize];

		int j;
		for(j = 0; j<size; j++)
		{
			newArr[j] = array[j];
		}

		size = newSize;
		delete [] array;
		array = newArr;
	}
	else
	{
		size_t newSize 	= size + 1;
		std::string* newArr = new std::string[newSize];
		array 	= newArr;
		size 	= newSize;
	}

}


void XmlParser::prueba_borrado(std::string * &array)
{
		delete [] array;
}


