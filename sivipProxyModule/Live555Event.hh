#include <Http/HttpClient.h>	
#include <Http/HttpStream.h>
#include <video/H264FrameQueue.h>
#include <video/MPEG4FrameQueue.h>
#include <time.h>
#include <sys/time.h>	
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <liveMedia.hh>
#include <BasicUsageEnvironment.hh>

#ifndef _LIVE555_EVENT_HH
#define _LIVE555_EVENT_HH

class Live555Event
{
	private: 
		uint32_t		m_eventId;
		TaskScheduler* 	m_scheduler;
		FramedSource*   m_device;
		
	public:
		Live555Event();
		virtual ~Live555Event();	
		
		void setEventId( uint32_t id);
		void setScheduler(TaskScheduler* scheduler);
		void setSource(FramedSource* device);

		const uint32_t getEventId() const;
		const TaskScheduler* getScheduler();
				
		void notifyNewFrame();
};

#endif
