#include <H264VideoRTPSink.hh>
#include <H264VideoStreamFramer.hh>
#include <H264VideoStreamDiscreteFramer.hh>
#include <MPEG4VideoStreamDiscreteFramer.hh>
#include <MPEG4ESVideoRTPSink.hh>

#include <SivipServerMediaSubsession.hh>

SivipServerMediaSubsession* SivipServerMediaSubsession::createNew(	UsageEnvironment& env, 
																							SivipInput& sivipInput, 
																							unsigned estimatedBitrate,
																							Boolean reuseBuffer,
																							Boolean iFramesOnly,
																							double vshPeriod	)
{
	printf("SivipServerMediaSubsession::SivipServerMediaSubsession()\n");
	return new SivipServerMediaSubsession(	env, 
														sivipInput, 
														estimatedBitrate, 
														reuseBuffer, 
														iFramesOnly, 
														vshPeriod	);
}

SivipServerMediaSubsession::SivipServerMediaSubsession(	UsageEnvironment& env, 
																			SivipInput& sivipInput, 
																			unsigned estimatedBitrate, 
																			Boolean reuseBuffer, 
																			Boolean iFramesOnly, 
																			double vshPeriod	)
																				:	OnDemandServerMediaSubsession(env, reuseBuffer), 
																					fInput(sivipInput), 
																					fIFramesOnly(iFramesOnly), 
																					fVSHPeriod(vshPeriod) 
{
	fEstimatedKbps = (estimatedBitrate + 500)/1000;
}

SivipServerMediaSubsession::~SivipServerMediaSubsession() 
{
	printf("SivipServerMediaSubsession::~SivipServerMediaSubsession()\n");
}

void SivipServerMediaSubsession::deleteStream(	unsigned clientSessionId, 
																void*& streamToken	)
{ 
	printf("SivipServerMediaSubsession::~deleteStream()\n");
	OnDemandServerMediaSubsession::deleteStream(	clientSessionId, 
																streamToken	);
}

FramedSource* SivipServerMediaSubsession::createNewStreamSource(	unsigned clientSessionId, 
																						unsigned& estBitrate	) 
{
	printf("SivipServerMediaSubsession::createNewStreamSource() - clienteSessionId: %u\n", clientSessionId);

	estBitrate = fEstimatedKbps;
	fInput.setClientSessionId( clientSessionId );
	string codec 	= fInput.getCodec();

	if( codec == "H264")
	{	
		printf("SivipServerMediaSubsession::createNewStreamSource(): H264\n");
		return H264VideoStreamDiscreteFramer::createNew(	envir(), 
																			fInput.videoSource()	);
	}

	if(codec == "MPEG4")
	{
		printf("SivipServerMediaSubsession::createNewStreamSource(): MPEG4\n");
		return MPEG4VideoStreamDiscreteFramer::createNew(	envir(), 
																			fInput.videoSource()	);
	}

	return NULL;
}

RTPSink* SivipServerMediaSubsession::createNewRTPSink(	Groupsock* rtpGroupsock, 
																			unsigned char /*rtpPayloadTypeIfDynamic*/, 
																			FramedSource* /*inputSource*/	) 
{
	printf("SivipServerMediaSubsession::createNewRTPSink()\n");
	fInput.setVideoRTPSinkBufferSize();

	string codec 	= fInput.getCodec();

	if( codec == "H264")
	{	
		printf("SivipServerMediaSubsession::createNewRTPSink(): H264\n");
		return H264VideoRTPSink::createNew(	envir(), 
														rtpGroupsock, 
														96	);
	}

	if(codec == "MPEG4")
	{
		printf("SivipServerMediaSubsession::createNewRTPSink(): MPEG4\n");
		return MPEG4ESVideoRTPSink::createNew(	envir(), 
															rtpGroupsock, 
															96	);
	}

	printf("SivipServerMediaSubsession::createNewRTPSink() bye\n");
}

void SivipServerMediaSubsession::seekStream(	unsigned clientSessionId, 
															void* streamToken, 
															char*& absStart, 
															char*& absEnd	) 
{
	printf("SivipServerMediaSubsession::seekStream()--------> \n");

	printf("\t absStart: %s \t absEnd: %s\n", absStart, absEnd);
	int year = 0;
	int month = 0;
	int day = 0;
	int hour = 0;
	int min = 0;
	int sec = 0;
	int ms = 0;
	float scale = 1;

	// Parsemos el char en enteros
	parseClockParam(	absStart, 
							year, 
							month, 
							day, 
							hour, 
							min, 
							sec, 
							ms	);

	// En principio el cliente nos da la hora en UTC
	// Calculamos la diferencia de UTC con la hora local
	time_t now 			= time(0);
	struct tm * local 	= localtime( & now );
	int localHour 		= local->tm_hour;

	struct tm * utc 	= gmtime ( & now );
	int utcHour 		= utc->tm_hour;

	int gmt 			= localHour - utcHour;

	// Convertimos la fecha a segundos desde 1970
	struct tm videoInitTime;
		videoInitTime.tm_sec 	 	= sec;
		videoInitTime.tm_min 	 	= min;
		videoInitTime.tm_hour 		= hour;
		videoInitTime.tm_mday 		= day;
		videoInitTime.tm_mon 		= month - 1;
		videoInitTime.tm_year 		= year  - 1900;
		videoInitTime.tm_isdst 		= -1;

	uint32_t videoInitSeconds = (uint32_t) mktime(&videoInitTime);

	printf(	"Sec: %d, Min: %d, Hour: %d, Day: %d, Mon: %d, Year: %d\n", 
				sec, 
				min, 
				hour, 
				day, 
				month, 
				year	);

	printf("\tlocal-time = UTC ( +%d:00 )\n", gmt);

	videoInitSeconds 	+= gmt*3600;
	SivipStream* siv 	=  fInput.getStreamObject();
	siv->setReplayInit(videoInitSeconds);

	setStreamScale(	clientSessionId, 
							streamToken, 
							scale	);

	if(absEnd != NULL)
	{
	    year = month = day = hour = min = sec = ms = 0;

		parseClockParam(	absEnd, 
								year, 
								month, 
								day, 
								hour, 
								min, 
								sec, 
								ms	);

		// Convertimos la fecha a segundos desde 1970
		struct tm videoEndTime;
			videoEndTime.tm_sec 	 	= sec;
			videoEndTime.tm_min 	 	= min;
			videoEndTime.tm_hour 		= hour;
			videoEndTime.tm_mday 		= day;
			videoEndTime.tm_mon 		= month - 1;
			videoEndTime.tm_year 		= year  - 1900;
			videoEndTime.tm_isdst 		= -1;

		uint32_t videoEndSeconds = (uint32_t) mktime(&videoEndTime);

		videoEndSeconds 	+= gmt*3600;

		siv->setReplayEnd(videoEndSeconds);
		scale = 12.0;
		setStreamScale(clientSessionId, streamToken, scale);
		
		printf("\tIniciando descarga ...\n");
		printf("VideoInitSeconds: %d\n", videoInitSeconds);
		printf("VideoEndSeconds: %d\n", videoEndSeconds);
		printf("Sec: %d, Min: %d, Hour: %d, Day: %d, Mon: %d, Year: %d\n", sec, min, hour, day, month, year);
	}

	siv->start();
	printf("<------------ SivipServerMediaSubsession::seekStream() \n");
}


void SivipServerMediaSubsession::setStreamScale(	unsigned clientSessionId, 
																	void* streamToken, 
																	float scale	)
{
	printf("SivipServerMediaSubsession::setStreamScale() \n");
	double sivScale 	= (double) scale;

	SivipStream* siv =  fInput.getStreamObject();
	siv->setScale((double)scale);

	OnDemandServerMediaSubsession::setStreamScale( clientSessionId, streamToken, scale );
	
	printf("SivipServerMediaSubsession::setStreamScale() - scale: %f \n", scale);
	printf("SivipServerMediaSubsession::setStreamScale() end \n");
}

void SivipServerMediaSubsession::pauseStream(unsigned clientSessionId, void* streamToken)
{
	printf("SivipServerMediaSubsession::pauseStream() \n");
 
	SivipStream* siv =  fInput.getStreamObject();
	siv->signalPause();

	OnDemandServerMediaSubsession::pauseStream( clientSessionId, streamToken );

	printf("SivipServerMediaSubsession::pauseStream() end \n");
}

void SivipServerMediaSubsession::startStream(	unsigned clientSessionId,
																void* streamToken,
																TaskFunc* rtcpRRHandler,
																void* rtcpRRHandlerClientData,
																unsigned short& rtpSeqNum,
																unsigned& rtpTimestamp,
																ServerRequestAlternativeByteHandler* serverRequestAlternativeByteHandler,
																void* serverRequestAlternativeByteHandlerClientData	) 
{
	printf("SivipServerMediaSubsession::startStream() \n");
 
	SivipStream* siv =  fInput.getStreamObject();
	siv->signalPlay();

 	StreamState* streamState = (StreamState*)streamToken;
  	Destinations* destinations = (Destinations*)(fDestinationsHashTable->Lookup((char const*)clientSessionId));

  	if (streamState != NULL) 
	{
    	streamState->startPlaying(	destinations,
											rtcpRRHandler, 
											rtcpRRHandlerClientData,
											serverRequestAlternativeByteHandler, 
											serverRequestAlternativeByteHandlerClientData	);

    	RTPSink* rtpSink = streamState->rtpSink(); // alias

    	if (rtpSink != NULL) 
		{
   		rtpSeqNum 		= rtpSink->currentSeqNo();
			rtpTimestamp 	= rtpSink->presetNextTimestamp();
    	}
  	}

	printf("SivipServerMediaSubsession::startStream() end \n");
}

void SivipServerMediaSubsession::stopStream()
{
	printf("SivipServerMediaSubsession::stopStream() \n");
 
	SivipStream* siv =  fInput.getStreamObject();
	siv->stop();

	printf("SivipServerMediaSubsession::stopStream() end \n");
}

/**
 * Extrae la fecha de un absTime
 * @param  			fecha representada por absTime
 * @param month		Mes de la fecha representada por absTime
 * @param day		Día de la fecha representada por absTime
 * @param hour		Hora de la fecha representada por absTime
 * @param min		Minuto de la fecha representada por absTime
 * @param sec		Segundo de la fecha representada por absTime
 * @param ms		Milisegundo de la fecha representada por absTime
 */
Boolean SivipServerMediaSubsession::parseClockParam(	char const* absTime,
																		int& year,
																		int& month,
																		int& day,
																		int& hour,
																		int& min,
																		int& sec,
																		int& ms	)
{
	int ret = sscanf(absTime,"%4d%2d%2dT%2d%2d%2d.%iZ", &year, &month, &day, &hour, &min, &sec, &ms);

	if( ret == 7 )
		return true;

	else if( ret == 6)
	{
		ms=0;
		return true;
	}

	return false;

}



