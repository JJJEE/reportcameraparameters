#include <LiveHttpStream.hh>
#include <video/H264VOP.h>
#include <video/H264ChunkProcessor.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/stat.h>
#include <InternalThread.hh>
#include <Live555Event.hh>
#include <FramedSource.hh>
#include <fstream>
#include <string>

LiveHttpStream::LiveHttpStream()
{
	m_codecFrame = new H264FrameQueue();
	m_codecFrame->setMaxSize(2);
	m_stream 		= 0;
}

LiveHttpStream::LiveHttpStream(string codec)
{
	if(codec == "H264" || codec== "h264" || codec=="264" || codec==".264")
	{
		printf("h264\n");
		m_codecFrame 	= new H264FrameQueue();
		m_codecFrame->setMaxSize(2);
		m_codec 		= "H264";
	}
	else if(codec == "MPEG4" || codec== "mpeg4" || codec=="mpg" || codec==".mpg" || codec=="mpeg" || codec==".mpeg" || codec==".m4e")
	{
		printf("mpeg4\n");
		m_codecFrame = new MPEG4FrameQueue();
		m_codecFrame->setMaxSize(2);
		m_codec 		= "MPEG4";
	}
	else
	{
		printf("LiveHttpStream::LiveHttpStream(): Codec Unknown. Using default codec (H264)\n");
		m_codecFrame = new H264FrameQueue();
		m_codecFrame->setMaxSize(2);
		m_codec 		= "H264";
	}
	m_stream 		= NULL;
}

LiveHttpStream::~LiveHttpStream()
{
	if(m_codecFrame != 0) { delete m_codecFrame; }
	if(m_stream != 0) { delete m_stream; }
}

void LiveHttpStream::setUrl(string url)
{
	m_httpCli.setURL(url); 
	m_httpCli.setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");

	m_stream 		= NULL;
	m_stream		= m_httpCli.sendRequest();

	string hdr				= m_stream->getNextInformationChunk();
	m_status			= m_stream->getRequestStatus();

	printf( "url: %s \n", url.c_str() );
	printf( "hdr: %s \n", hdr.c_str() );
	printf( "status: %d \n", m_status );
}

const uint32_t LiveHttpStream::getStatus()
{
	return m_status;
}

const char* LiveHttpStream::getCodec() const
{
	return m_codec.c_str();
}

bool LiveHttpStream::frameIsReady()
{
	lock();
		int size 	= m_codecFrame->getSize();
	unlock();

	if( size > 0 )
	{
		return true;
	}	
	else
	{ 
		return false;
	}
}	

const uint32_t LiveHttpStream::getQueueSize() const
{
	return m_codecFrame->getSize();
}

void LiveHttpStream::getChunk()
{
	// printf("LiveHttpStream::getChunk()\n");
	lock();
		string rec 		= m_stream->getNextInformationChunk();
		m_codecFrame->addVOPChunk( (void *) rec.c_str(), rec.length() );
	unlock();
}

const Frame *LiveHttpStream::getFrame()
{	
	// printf("LiveHttpStream::getFrame()\n");
	lock();
		int size 	= m_codecFrame->getSize();
	unlock();

	if( size > 0 )
	{
		lock();
			const Frame* nextFrame 	= m_codecFrame->getNextFrame();
		unlock();
		return nextFrame;
	}	
	else
	{
		return 0;
	}
}

void LiveHttpStream::run()
{
	printf("LiveHttpStream::run() - thread id: %u \n", getThreadId());
	fflush(stdout);
	bool 		isStop 			= false;
	uint64_t 	lastId 			= 0;

	while(1)
    {
		if( frameIsReady() )
		{
			lock();
				const uint64_t currentId 	= m_codecFrame->getNextFrameId();
			unlock();
			if( currentId >= lastId)
			{				
				notifyNewFrame();
				lastId 		= currentId;
				usleep(40000);	
			}			

		}
		else
		{
			getChunk();
		}

		lock();
			isStop 	= m_stopSignal;
		unlock();
		
		if( isStop ) 
		{ 
			break; 
		}	
    }
	printf("LiveHttpStream::run() - work finished, thread id: %u \n", (unsigned int) pthread_self());
	fflush(stdout);

	/* For testing
	string rec;
	ofstream video("live.264", ios::out | ios::binary); 	

	uint8_t* data;
	int dataLen;

	
	while(1)
	{
		if(frameIsReady() )
		{
			const Frame *thisFrame	= getFrame();
			dataLen 	= thisFrame->getSize();	
			data 		= (uint8_t* ) thisFrame->getData();
			printf( "frameId: %llu \n", thisFrame->getId() );
			rec = "";
			for(int i =0; i < dataLen; i++)	
			{				
				rec += data[i];
			}	

			video << rec;

			delete thisFrame;
		}
		else
		{
			getChunk();
		}
		pthread_mutex_lock(&internalMutex);
			isStop 	= m_stopSignal;
		pthread_mutex_unlock(&internalMutex);
		if( isStop ) 
		{ 
			video.close(); 
			break; 
		}	
	}
	*/

}


