#include <SivipChunk.hh>
#include <video/H264VOP.h>
#include <video/H264ChunkProcessor.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/stat.h>
#include <InternalThread.hh>
#include <Live555Event.hh>
#include <FramedSource.hh>
#include <iostream>
#include <fstream>
#include <string>


SivipChunk::SivipChunk(): 
	m_canis(0),
	m_recordingAsker(0),
	m_decodeAsker(0),
	m_sessionParams(0),
	m_initSecond(0),
	m_downloadProgress(0),
	m_currentSecond(0),
	m_endSecond(0),
	m_scale(1),
	m_camId(0),	
	m_lastMili(0),
	m_retries(0),
	m_isLiveL9Session(false),
	m_isLiveL9EMBSession(false),
	m_isReplaySession(false),
	m_isPlay(false), 
	m_syncMili(0), 
	m_fps(0)
{
	//printf("SivipChunk::SivipChunk()\n");
	initCodec();
}

SivipChunk::SivipChunk(string codec):
	m_canis(0),
	m_recordingAsker(0),
	m_decodeAsker(0),
	m_sessionParams(0),
	m_initSecond(0),
	m_downloadProgress(0),
	m_currentSecond(0),
	m_endSecond(0),
	m_scale(1),
	m_camId(0),
	m_lastMili(0),
	m_retries(0),
	m_isLiveL9Session(false),    
	m_isLiveL9EMBSession(false), 
	m_isReplaySession(false),   
	m_isPlay(false),
	m_syncMili(0),
	m_fps(0)
{
	//printf("SivipChunk::SivipChunk(string codec)\n");
	initCodec(codec);
}

SivipChunk::~SivipChunk()
{
	//printf("SivipChunk::~SivipChunk()\n");

	if( m_canis !=0) 			
		delete m_canis;
	if( m_recordingAsker !=0) 	
		delete m_recordingAsker;
	if( m_decodeAsker != 0) 	
		delete m_decodeAsker;
	if( m_codecFrame !=0)  		
		delete m_codecFrame;
	if( m_sessionParams !=0)    
		delete  m_sessionParams;

	//printf("SivipChunk::~SivipChunk() bye\n");
}

void SivipChunk::initCodec( string codec )
{
	//printf("SivipChunk::initCodec()\n");

	if(	codec == "H264" 	|| 
			codec == "h264" 	|| 
			codec == "264"		|| 
			codec == ".264"	)
	{
		m_codecFrame 	= new H264FrameQueue();
		m_codec 		= "H264";
	}
	else if(	codec == "MPEG4" 	|| 
				codec == "mpeg4" 	|| 
				codec == "mpg"   	|| 
				codec == "mp4v"	|| 
				codec == ".mpg"  	|| 
				codec == "mpeg"   || 
				codec == ".mpeg" 	|| 
				codec == ".m4e"	)
	{
		m_codecFrame 	= new MPEG4FrameQueue();
		m_codec 		= "MPEG4";
	}
	else
	{
		printf("SivipChunk::initCodec(): Codec Unknown. Using default codec (MPEG4)\n");
		m_codecFrame 	= new MPEG4FrameQueue();
		m_codec 		= "MPEG4";
	}
}

void SivipChunk::setMaxQueueSize(uint32_t maxFrames)
{
	//printf("SivipChunk::setMaxQueueSize()\n");

	lock();
		m_codecFrame->setMaxSize(maxFrames);
	unlock();
}

void SivipChunk::setCamId(uint32_t p_camid)
{
	//printf("SivipChunk::setCamId()\n");

	lock();
		m_camId 	= p_camid;
	unlock();
}

void SivipChunk::setTrainId(uint32_t p_trainid)
{
	//printf("SivipChunk::setTrainId()\n");

	lock();
		m_trainId 	= p_trainid;
	unlock();
}

void SivipChunk::setRealCamId(uint32_t p_realcamid)
{
	//printf("SivipChunk::setRealCamId()\n");

	lock();
		m_realCamId 	= p_realcamid;
	unlock();
}

void SivipChunk::setReplayInit(uint32_t seconds)
{
	//printf("SivipChunk::setReplayInit()\n");

	lock();
		m_initSecond 	= seconds;
		m_currentSecond = seconds,
	unlock(); 
}

void SivipChunk::setReplayEnd(uint32_t seconds)
{
	//printf("SivipChunk::setReplayEnd()\n");

	lock();
		m_endSecond 	= seconds;
	unlock();
}

void SivipChunk::setScale(double scale) 
{ 
	//printf("SivipChunk::setScale()\n");

	lock();
		m_scale 	= scale;
	unlock();
}	

void SivipChunk::setReplaySession()
{
	//printf("SivipChunk::setReplaySession()\n");

	lock();
		m_isReplaySession 	= true;
		m_isLiveL9Session 	= false;
		m_isLiveL9EMBSession 	= false;
	unlock();
}

void SivipChunk::setLiveL9Session()
{
	//printf("SivipChunk::setLiveL9Session()\n");

	lock();
		m_isLiveL9Session 	= true;
		m_isLiveL9EMBSession 	= false;
		m_isReplaySession 	= false;
	unlock();
}

void SivipChunk::setLiveL9EMBSession()
{
	//printf("SivipChunk::setLiveL9EMBSession()\n");

	lock();
		m_isLiveL9EMBSession 	= true;
		m_isLiveL9Session 	= false;
		m_isReplaySession 	= false;
	unlock();
}

void SivipChunk::setFps(uint32_t fps)
{
	//printf("SivipChunk::setFps()\n");

	lock();
		m_fps 	= fps;
	unlock();
}

const uint32_t SivipChunk::getFps( )
{
	//printf("SivipChunk::getFps()\n");

	uint32_t fps;
	lock();
		fps 	= m_fps;
	unlock();

	return fps;	
}

const char* SivipChunk::getCodec()
{
	//printf("SivipChunk::getCodec()\n");

	lock();
		const char* codec 	= m_codec.c_str();
	unlock();
	
	return codec;
}

uint32_t SivipChunk::getMaxQueueSize()
{
	//printf("SivipChunk::getMaxQueueSize()\n");

	lock();
		uint32_t size 	= m_codecFrame->getMaxSize();
	unlock();

	return size;
}

uint32_t SivipChunk::getQueueSize()
{
	//printf("SivipChunk::getQueueSize()\n");

	lock();
		uint32_t size 	= m_codecFrame->getSize();
	unlock();

	return size;
}

uint32_t SivipChunk::getCamId()
{
	//printf("SivipChunk::getCamId()\n");

	lock();
		uint32_t id 	= m_camId;
	unlock();

	return id;
}

void SivipChunk::getReplayTime(struct timeval * time)
{
	//printf("SivipChunk::getReplayTime()\n");

	lock();
		time->tv_sec		= m_currentSecond;
		time->tv_usec		= m_lastMili*1000;
	unlock();
	
}

uint32_t SivipChunk::getReplayInit()
{
	//printf("SivipChunk::getReplayInit()\n");

	lock();
		uint32_t time 	= 	m_initSecond;
	unlock();

	return time;
}

uint32_t SivipChunk::getReplayEnd()
{
	//printf("SivipChunk::getReplayEnd()\n");

	lock();
		uint32_t time 	= 	m_endSecond;
	unlock();

	return time;
}

double SivipChunk::getScale()
{
	//printf("SivipChunk::getScale()\n");

	lock();
		double scale 	= 	m_scale;
	unlock();

	return scale;
}

const Frame *SivipChunk::getFrame()
{	
	//printf("SivipChunk::getFrame()\n");

	lock();
		int size 	= m_codecFrame->getSize();
	unlock();

	if( size > 0 )
	{
		if( m_scale > 0)
		{
			lock();
				const Frame* nextFrame 	= m_codecFrame->getNextFrame();
			unlock();

			return nextFrame;
		}
		else if( m_scale < 0)
		{
			printf("m_scale: %f \n", m_scale); 

				lock();
					const Frame* nextFrame 	= m_codecFrame->getNextKeyFrame();
					if(nextFrame != 0)
					{				
						printf("key frame size: %d \n", nextFrame->getSize());
					}			
				unlock();
				printf("SivipStream::getFrame() end \n");
				return nextFrame;		
		}		
		else
		{
			return 0;		
		}
	}	

	return 0;
}

bool SivipChunk::bufferIsReady()
{
//	printf("SivipChunk::bufferIsReady()\n");

	lock();
		bool isLiveL9		= m_isLiveL9Session;
		bool isLiveL9EMB		= m_isLiveL9EMBSession;
		bool isReplay	= m_isReplaySession;
		uint32_t size 	=  m_codecFrame->getSize();
		uint32_t delay 	= 0;
	 	if(m_scale == 0) m_scale =1;
	    delay = (MS_PER_FRAME/m_scale);							// Retardo entre frames
	unlock();
	
	if(isLiveL9)
	{
		if(size >=  LIVEL9_FRAME_BUFFER ) 
			return true;
	}

	if(isLiveL9EMB)
	{
		if(size >=  LIVEL9EMB_FRAME_BUFFER ) 
			return true;
	}

	if(isReplay)
	{
		if(size >=  REPLAY_FRAME_BUFFER ) 
			return true;
	}

	return false;
}

bool SivipChunk::frameIsReady()
{
	//printf("SivipChunk::frameIsReady()\n");

	lock();
		int size 	= m_codecFrame->getSize();
	unlock();
  
	if( size > 0 )
	{ 
		return true;
	}	
	else
	{ 
		return false;
	}
}

bool SivipChunk::isReplay() 
{ 
	//printf("SivipChunk::isReplay()\n");

	lock();
		bool ret 	= m_isReplaySession;
	unlock();

	return ret; 
}

bool SivipChunk::isLiveL9() 
{ 
	//printf("SivipChunk::isLiveL9()\n");

	lock();
		bool ret 	= m_isLiveL9Session;
	unlock();

	return ret;
}

bool SivipChunk::isLiveL9EMB() 
{ 
	//printf("SivipChunk::isLiveL9EMB()\n");

	lock();
		bool ret 	= m_isLiveL9EMBSession;
	unlock();

	return ret;
}

void SivipChunk::signalPause()
{
	//printf("SivipChunk::signalPause()\n");

	lock();
		m_isPlay 	= false;
	unlock();
}

void SivipChunk::signalPlay()
{
	//printf("SivipChunk::signalPlay()\n");

	lock();
		m_isPlay 	= true;
	unlock();
}

bool SivipChunk::isPlay() 
{ 
	//printf("SivipChunk::isPlay()\n");

	lock();
		bool ret 	= m_isPlay;
	unlock();

	return ret;
}

void SivipChunk::startReplaySession(uint32_t p_camid)
{
	printf("SivipChunk::startReplaySession\n");

    if( p_camid != 0 )
	{
		lock();
			m_camId 						= 	p_camid;	
		unlock();
	}	

	m_sessionParams->recType	= 	RecordingFileHeaderChunk::REC_HOWMANY;
	m_sessionParams->devId 		= 	m_camId;
	m_sessionParams->fpswhole 	= 	25;
	m_sessionParams->fpsfrac 	= 	0;
	m_sessionParams->isCyclic 	= 	false;
	m_sessionParams->isRecoded 	=	false;
	m_sessionParams->fourcc		=	0;
	m_sessionParams->date		= 	new RecordingFileDateMetadataChunk();
	m_sessionParams->date->setDate(m_initSecond,0/*millis*/);

	// Iniciamos sesion
	try
	{
		lock();
			m_recordingAsker->startSession(*m_sessionParams, true);
			m_isReplaySession 				= true;
			m_isLiveL9Session 				= false;
			m_isLiveL9EMBSession 				= false;
		unlock();
		printf("SivipChunk::startReplaySession(): Init session successful\n");
	}
	catch(...)
	{
		m_isLiveL9Session 	= false;
		m_isLiveL9EMBSession		= false;
		m_isReplaySession 	= false;
		unlock();
		printf("SivipChunk::startReplaySession(): Cannot init session\n");
		if(m_recordingAsker != 0)
		{
			delete m_recordingAsker;
			m_recordingAsker = 0;
			m_recordingAsker		= 	new RecordingModuleAccess(m_canis);
		}						
		startReplaySession();
	}    

}

void SivipChunk::startLiveL9Session(uint32_t p_camid)
{
	printf("SivipChunk::startLiveL9Session\n");

    if( p_camid != 0 )
	{
		lock();
			m_camId 						= 	p_camid;	
		unlock();
	}	
	
	lock();
		IPDeviceID  ipDevice( (int) m_camId);
	unlock();

	// Iniciamos sesion
	try
	{
		lock();
		printf("SivipChunk::startLiveL9Session - Sesion iniciada\n");
			m_decodeAsker->startSession( ipDevice );
			m_isLiveL9Session 	= true;
			m_isLiveL9EMBSession 	= false;
			m_isReplaySession 	= false;
		unlock();	
	}
	catch(...)
	{
		m_isLiveL9Session 	= false;
		m_isLiveL9EMBSession 	= false;
		m_isReplaySession 	= false;
		unlock();
		printf("SivipChunk::startLiveL9Session(): No se pudo iniciar sesion\n");
		stop();
	}    
}

void SivipChunk::startLiveL9EMBSession(uint32_t p_camid)
{
	printf("SivipChunk::startLiveL9EMBSession\n");

   if( p_camid != 0 )
	{
		lock();
			m_camId 						= 	p_camid;	
		unlock();
	}	
	
	lock();
		IPDeviceID  ipDevice( (int) m_camId );
		CPDeviceID  cpDevice( (int) m_camId );
	unlock();

	// Iniciamos sesion
	try
	{
		lock();
			printf("SivipChunk::startLiveL9EMBSession - Sesion iniciada\n");

			m_decodeAsker->startSession( ipDevice );

			// Enviamos al L9EMBControlPlugin la peticion para que conmute al tren y la camara que le pedimos
			static const int L9EMBInputID = 100;
			m_controlAsker->startSession( cpDevice );
			m_controlAsker->setInput( L9EMBInputID, m_trainId * 1000 + m_realCamId );

			m_isLiveL9EMBSession 	= true;
			m_isLiveL9Session 	= false;
			m_isReplaySession 	= false;
		unlock();	
	}
	catch(...)
	{
		m_isLiveL9EMBSession 	= false;
		m_isLiveL9Session 	= false;
		m_isReplaySession 	= false;
		unlock();
		printf("SivipChunk::startLiveL9EMBSession(): No se pudo iniciar sesion\n");
		stop();
	}    
}

void SivipChunk::endReplaySession()
{
	printf("SivipChunk::endReplaySession\n");
	lock();
		if( m_isReplaySession )
		{
			RMEndSessionParams esParams;		
			esParams.devId 			= 	m_camId;
			m_recordingAsker->endSession(esParams);
			m_isReplaySession 		= false;	
		}
	unlock();
}

void SivipChunk::endLiveL9Session()
{
	printf("SivipChunk::endLiveL9Session\n");
	lock();
		if( m_decodeAsker != 0)
		{
			IPDeviceID  ipDevice( (int) m_camId);
			m_decodeAsker->endSession(ipDevice);
			m_isLiveL9Session 	= false;
		}
	unlock();
}

void SivipChunk::endLiveL9EMBSession()
{
	printf("SivipChunk::endLiveL9EMBSession\n");
	lock();
		if( m_decodeAsker != 0)
		{
			IPDeviceID  ipDevice( (int) m_camId);
			m_decodeAsker->endSession(ipDevice);
			m_isLiveL9EMBSession 	= false;
		}
		if( m_controlAsker != 0)
		{
			CPDeviceID  cpDevice( (int) m_camId);
			m_controlAsker->endSession( cpDevice );
		}
		
	unlock();
}

RecordingFileFrameChunk* SivipChunk::readFrame(uint32_t sec, uint32_t millis)
{
	//printf("SivipChunk::readFrame()\n");

	RMReadFrameParams *rfParams 			= 	new RMReadFrameParams();
	RecordingFileFrameChunk* rffc 			= 	new RecordingFileFrameChunk();
	RecordingFileDateMetadataChunk* rfdmc 	= 	new RecordingFileDateMetadataChunk();
	rfdmc->setDate(sec,millis);
	rffc->replaceSubChunk(0,rfdmc);
	rfParams->chunk 	= 	rffc;

	RecordingFileFrameChunk* theRecord 		= 	m_recordingAsker->readFrame(*rfParams);

	return theRecord;
}

void SivipChunk::stop()
{
	//printf("SivipChunk::stop()\n");

	fflush(stdout);
	// Mandamos señal para salir del bucle
	lock();
		m_stopSignal 	= true;

	unlock();
}

RMListRecordingsParams* SivipChunk::getRecordingList( qword startDate_ )
{
	//printf("SivipChunk::getRecordingList()\n");

	RecordingModuleAccess* recordingAsker		= 	new RecordingModuleAccess(m_canis);

	time_t now 					= time(0);
	struct tm * local 			= localtime( & now );
	uint32_t currentSeconds 	= (uint32_t) mktime(local);

	RMListRecordingsParams lrp 	= RMListRecordingsParams();
	lrp.absStartDate.isValid 	= true;
	lrp.absStartDate.secs		= startDate_;
	lrp.absStartDate.millis 	= 0;
	lrp.absEndDate.isValid 		= true;
	lrp.absEndDate.secs 		= currentSeconds;
	lrp.absEndDate.millis 		= 0;
		
	RMListRecordingsParams *theRecordingList	= 	recordingAsker->listRecordings(lrp);
	
	delete recordingAsker;
	return theRecordingList;
}

const void SivipChunk::printRecordingList(RMListRecordingsParams *list_) const
{
	//printf("SivipChunk::printRecordingList()\n");

	time_t seconds;
	struct tm	*date;

	std::cout << " nRecsFilter:  "<< list_->nRecsFilter << std::endl;
	std::cout << " nRecordings:  "<< list_->nRecordings << std::endl;
	std::cout << " absStartDate: "<< list_->absStartDate.isValid << " - " << list_->absStartDate.secs << " - " << list_->absStartDate.millis << std::endl;
	std::cout << " absEndDate:   "<< list_->absEndDate.isValid << " - " << list_->absEndDate.secs << " - " << list_->absEndDate.millis << std::endl;

	for(int i=0; i<list_->nRecordings; i++)
	{
		std::cout << std::endl;
		std::cout << " camera Id:     "<< list_->recordings[i].devId << std::endl;
		std::cout << " rec id:        "<< list_->recordings[i].id << std::endl;
		if (list_->recordings[i].recType == 0)
		{
			std::cout << " m/a:           "<< "manual" << std::endl;
		}
		else
		{
			std::cout << " m/a:           "<< "automatica" << std::endl;
		}
		std::cout << " servidor:      "<< list_->recordings[i].rmAddress.getIP().getHostname()<< std::endl;
		std::cout << " fecha inicio:  " << list_->recordings[i].startDate.secs << " secs  " << list_->recordings[i].startDate.millis << " ms " << std::endl;
		seconds = list_->recordings[i].startDate.secs;
		date = gmtime ( &seconds );
		printf(      "                %i/%i/%i %i:%i:%i \n", date->tm_mday, date->tm_mon+1, date->tm_year+1900, date->tm_hour+1, date->tm_min, date->tm_sec );

		std::cout << " fecha fin:     " << list_->recordings[i].endDate.secs << " secs  " << list_->recordings[i].endDate.millis << " ms " << std::endl;
		seconds = list_->recordings[i].endDate.secs;
		date = gmtime ( &seconds );
		printf(      "                %i/%i/%i %i:%i:%i \n", date->tm_mday, date->tm_mon+1, date->tm_year+1900, date->tm_hour+1, date->tm_min, date->tm_sec );
	}
}

const char* SivipChunk::getRecordingServerIp(RMListRecordingsParams *list_, uint32_t currentSeconds, uint32_t camId) 
{
	//printf("SivipChunk::getRecordingServerIp()\n");

	for(int i=0; i<list_->nRecordings; i++)
	{
		if( list_->recordings[i].devId == camId )
		{
			if( list_->recordings[i].startDate.secs <= currentSeconds )
			{
				string ipServer = list_->recordings[i].rmAddress.getIP().getHostname();
				printf("SivipChunk::getRecordingServerIp(): recording found in %s", ipServer.c_str());
				return ipServer.c_str();
			}
		}
	}
	printf("SivipChunk::getRecordingServerIp(): recording not found \n");
	return "255.255.255.255";
}

void SivipChunk::videoSync(uint32_t startPoint)
{
	//printf("SivipChunk::videoSync()\n");

		RecordingFileFrameChunk* theRecord;
		char* charFrame 		= NULL;
		int  sizeFrame 			= 0;
		uint32_t currentSecond 	= 0;
 
		lock();
			currentSecond 	= m_currentSecond;
			m_syncMili 		= startPoint;	
		unlock();

		// Sincronizamos tiempo actual con tiempo de grabación
		bool isSync 	= false;
		try
		{
			while( !isSync)
			{
				try
				{
					theRecord	= readFrame(currentSecond, m_syncMili);
				}
				catch(...)
				{
					printf("SivipStream::videoSync(): Cannot read frames\n");
					break;
				}	
		
				sizeFrame 	= theRecord->getFrameSize();

				charFrame	= new char[sizeFrame];
				memcpy(charFrame, theRecord->getFrame(), sizeFrame);

				lock();
					m_codecFrame->addVOPChunk( (void *) charFrame, sizeFrame );
				unlock();

				setFps(m_codecFrame->getSize());
				printf("SivipStream::videoSync(): Sincronizando... %u fps - sync %u ms \n", m_codecFrame->getSize(), m_syncMili);

				lock();
					m_codecFrame->resetQueue();
				unlock();

				if(getFps() > 25) 
				{
					++currentSecond;
					continue;
				}
				if( getFps() >= 24)
				{
					isSync 	= true;
					printf("SivipStream::videoSync(): Grabacion sincronizada en +%u ms\n", m_syncMili);
				}
				else if(getFps() >= 22)
				{
					m_syncMili += 10; // 80 ms -> El doble de 40ms (tiempo entre frame y frame)
					m_syncMili	= m_syncMili % 1000;
					++currentSecond;
				}
				else
				{
					m_syncMili += (22-getFps())*40; // 10 ms -> Un cuarto de 40ms 
					//m_syncMili	+= 80;
					m_syncMili	= m_syncMili % 1000;
					++currentSecond;
				}
				
				delete theRecord;
				theRecord 	= 0;
				delete charFrame;
				charFrame 	= 0;
			}		
		}
		catch(...)
		{
			printf("SivipStream::videoSync(): Unexpected Error\n");
		}
}


string hex_to_ascii(const string& input)
{
    static const char* const lut = "0123456789ABCDEF";
    size_t len = input.length();

    if (len & 1) throw std::invalid_argument("odd length");

    std::string output;
    output.reserve(len / 2);
    for (size_t i = 0; i < len; i += 2)
    {
        char a = input[i];
        const char* p = std::lower_bound(lut, lut + 16, a);
        if (*p != a) throw std::invalid_argument("not a hex digit");

        char b = input[i + 1];
        const char* q = std::lower_bound(lut, lut + 16, b);
        if (*q != b) throw std::invalid_argument("not a hex digit");

        output.push_back(((p - lut) << 4) | (q - lut));
    }
    return output;
}

void SivipChunk::run()
{
	printf("SivipChunk::run() - thread id: %08X \n", (unsigned int) pthread_self());

	string confFile 		= "SivipRtspProxyServerConfig.xml";
	// Conectamos
	bool connectionOk;
	while(1)
	{
		try
		{
			if(m_canis == 0)
			{
				m_canis		= new Canis(confFile.c_str(),true);
			}
			if(m_recordingAsker == 0 && isReplay())
			{
				RMListRecordingsParams* recList 	= getRecordingList(m_initSecond);
				string ip 	= getRecordingServerIp(recList, m_initSecond, m_camId);	

				if( ip != "255.255.255.255" )
				{
					IP modIp( ip.c_str() );

					Address modAddr(modIp, 4012);
					m_recordingAsker		= 	new RecordingModuleAccess(modAddr, m_canis);
				}	
				else
				{
					m_recordingAsker		= 	new RecordingModuleAccess(m_canis);
				}
			
				delete recList;
				recList = 0;
			}

			if(m_sessionParams == 0 && isReplay())
			{
				m_sessionParams 		= 	new RMStartSessionParams();
				m_sessionParams->date	=	new RecordingFileDateMetadataChunk();
			}

			if(m_decodeAsker == 0 && isLiveL9())
			{
				m_decodeAsker 		=   new DecodeModuleAccess(m_canis);
			}

			if(m_decodeAsker == 0 && isLiveL9EMB())
			{
				m_decodeAsker		=   new DecodeModuleAccess( m_canis );
				m_controlAsker		=   new ControlModuleAccess( m_canis );
			}

			connectionOk 		= true;
		}
		catch(...)
		{
			connectionOk 		= false;
			printf("SivipChunk::SivipChunk(): Error en la conexion\n");
			sleep(1);
			printf("SivipChunk::SivipChunk(): Reintentando...\n\n");
		}
		if( connectionOk == true) break;
	}

	printf("SivipChunk::run(): Conexion establecida con servidor\n\n");

	char* charFrame 		= NULL;
	int  sizeFrame 			= 0;
	uint32_t initSecond		= 0;
	uint32_t endSecond		= 0;
	uint32_t currentSecond 	= 0; 
	uint32_t delay 			= 0;
	uint32_t size 			= 0;
	double scale 			= 0;
	bool isStop 			= false;

	uint64_t currentMilis1  = 0;
	uint64_t currentMilis2  = 0;
	bool noVideo		 	= false;
	bool noVideoLoaded		= false;
	uint32_t iter 			= 0;

	struct timeval now;

	lock();
		initSecond 		= m_initSecond;
		currentSecond 	= m_currentSecond;	
		endSecond 		= m_endSecond;
		scale			= m_scale;
	unlock();

	setFps(0);	

	if( isReplay() )
	{
		RecordingFileFrameChunk* theRecord;
		startReplaySession();

		videoSync(0);   // Find the ms where a keyFrame start

		usleep(1500000);

		m_lastMili = m_syncMili;

		while( !isStop )
		{
			lock();
				scale	= m_scale;
			unlock();

			gettimeofday(&now, NULL);
			currentMilis1 	= now.tv_sec*1000 + now.tv_usec/1000;

			try
			{
				// Segundo de reproducción
				lock();
					currentSecond 	= m_currentSecond;
				unlock();

				// Petición de vídeo a sivip
				if( noVideo == false)
				{
					theRecord	= readFrame(currentSecond, m_lastMili);
					sizeFrame 	= theRecord->getFrameSize();
					charFrame	= new char[sizeFrame];
					memcpy(charFrame, theRecord->getFrame(), sizeFrame);
				
					delete theRecord;
					theRecord 	= 0;
				}
				else
				{
					// Mostrar "NO VIDEO"
					try
					{
						theRecord	= readFrame(currentSecond, m_lastMili); // Si no existe, salta la excepcion
						sizeFrame 	= theRecord->getFrameSize();

						if(charFrame !=0)
						{
							delete[] charFrame;
							charFrame 	= 0;	
							noVideoLoaded = false;	
						}

						charFrame	= new char[sizeFrame];
						memcpy(charFrame, theRecord->getFrame(), sizeFrame);
				
						delete theRecord;
						theRecord 	= 0;

						printf("SivipChunk::getChunk(): Video encontrado\n");
						//Video encontrado
						noVideo = false;
					}
					catch(...)
					{
						printf("SivipChunk::getChunk(): Video NO encontrado\n");
						// Reproducir video "NOVIDEO"

						if( noVideoLoaded == false)
						{
							streampos begin,end;
							string name;
							if(m_codec == "H264" )
							{
								name = "novideo.264";
							}
							else
							{
								name = "novideo.mpeg";	
							}

							ifstream video(name.c_str(), ios::in | ios::binary);
							begin = video.tellg();
							video.seekg (0, ios::end);
							end = video.tellg();
							sizeFrame	= end-begin;

							// Dejaremos esta variable con el contenido del vídeo
							// hasta que se encuentre una grabación. Asi evitamos
							// leer repetidas veces el fichero
							charFrame	= new char[sizeFrame];

							video.seekg(0, ios::beg);
							video.read(charFrame, sizeFrame);
							video.close();
							noVideoLoaded = true;
						}
					}
				}
	
				// Numero de fallos consecutivos a 0
				m_retries 	= 0;	

				// Tiempo despueś de la lectura de frame
				gettimeofday(&now, NULL);
				currentMilis2 	= now.tv_sec*1000 + now.tv_usec/1000;

				// Sentido de la reproducción
				if( scale > 0 || endSecond != 0)
				{
					lock();
						++m_currentSecond;
						m_lastMili		= m_syncMili;
					unlock();
				}
				else if(endSecond == 0)
				{
					lock();
						--m_currentSecond;
						m_lastMili		= m_syncMili;
					unlock();				
				}
					
				// Comprobación de tiempo de respuesta
				if( (currentMilis2 - currentMilis1) > 1000 /*40ms x 25 frames (pedimos de segundo en segundo*/ )
				{
					printf("SivipChunk::getChunk(): WARNING - Comunicación con SIVIP lenta. Tiempo de respuesta = %u ms \n", (unsigned int) (currentMilis2 - currentMilis1) );
				}
				
				// Comprobación de fin de descarga
				if (currentSecond >= endSecond && endSecond !=0)
				{
					printf("SivipChunk::getChunk(): Descarga Finalizada\n");
					fflush(stdout);
					stop();
				}
		
				// Log de porcentaje de descarga completado
				if(endSecond != 0)
				{
					double completeDownload = (double)endSecond - (double)m_initSecond;
					if(completeDownload == 0) completeDownload = 0.1;
					double downloadProgress = ( (completeDownload - ( (double)endSecond - (double)currentSecond ) ) /completeDownload)*100;
					if(downloadProgress > m_downloadProgress + 5)
					{
						printf("completado: %d\n", (int)downloadProgress);
						fflush(stdout);
						m_downloadProgress 	= downloadProgress;
					}
				}

				// Procesado de frame
				lock();
					m_codecFrame->addVOPChunk( (void *) charFrame, sizeFrame );
					size 	=  m_codecFrame->getSize();
				unlock();

				// Eliminación de buffer auxiliar
				if(charFrame != 0 && noVideo == false) 
				{
					delete[] charFrame;
					charFrame 	= 0;
				}

				gettimeofday(&now, NULL);
				uint64_t currentMilis3 	= now.tv_sec*1000 + now.tv_usec/1000;
				// Esparamos hasta que la cola alcance el tamaño minimo para hacer otra peticion
				while(size > REPLAY_FRAME_BUFFER && !isStop )
				{
					lock();
						size 	=  m_codecFrame->getSize();
						isStop 	=  m_stopSignal;
					unlock();
					usleep(1000);

					lock();
						uint32_t temp = m_initSecond;
					unlock();	
				
					if(initSecond != temp)
					{
						initSecond = temp;
	 					uint32_t startPoint;
						lock();					
							startPoint = (m_syncMili - 120) % 1000; // 40ms * 3
						unlock();			
						videoSync(startPoint);   // Find the ms where a keyFrame start
					}
				}
				gettimeofday(&now, NULL);
				uint64_t currentMilis4 	= now.tv_sec*1000 + now.tv_usec/1000;

				++iter;
				iter = iter % 10;
				if(iter == 0)
				{
					printf("SivipChunk::run() - thread %08X transmited %llu seconds for camId %u\n", (unsigned int) pthread_self(), currentSecond - m_initSecond, m_camId);
				}	
						
			}
			catch(...)
			{
				printf("SivipChunk::getChunk(): Cannot get frame. retries = %d\n", m_retries);
				fflush(stdout);
				++m_retries;

				if(m_retries <= 4)
				{
					// No hacemos nada. Seguimos en el bucle y pedimos 
					// el siguiente segundo, por si es un corte pequeño 
					// en el vídeo.
				}
				else if(m_retries <= 5)
				{
					// Probamos a cerrar y abrir sesión de nuevo
					try
					{
						endReplaySession();
						startReplaySession();
					}
					catch(...)
					{
						noVideo = true;
					}
				}

				else
				{
					noVideo = true;
				}
			}

			//printf("SivipChunk::getChunk(): queueSize2 %d \n", size);
			lock();
				isStop 	= m_stopSignal;
			unlock();
		}
		endReplaySession();
	}
	else if( isLiveL9() )
	{
		IPFrameBufferSize buffer;
		
		startLiveL9Session();
		delay 	= MS_PER_FRAME;

		gettimeofday(&now, NULL);
		uint64_t startMilis 	= now.tv_sec*1000 + now.tv_usec/1000;
	   
		while( !isStop )
		{
			gettimeofday(&now, NULL);
			currentMilis1 	= now.tv_sec*1000 + now.tv_usec/1000;

			try
			{
				IPFrame theFrame	= m_decodeAsker->getCompressedNextFrame();
				sizeFrame 	= theFrame.frameLength;
				charFrame	= new char[sizeFrame];

				string theCodec;

				if ( theFrame.codecInfo.fourcc == dword( 1836070006 )  )
					theCodec = "mpeg4";
				else if ( theFrame.codecInfo.fourcc == dword( 1748121140 ) )
					theCodec = "h264";
				else
					theCodec = theFrame.codecInfo.fourcc;

				//std::cout << ">> x, y, bpp: " << theFrame.frameInfo.x << ", " << theFrame.frameInfo.y << ", " << theFrame.frameInfo.bpp << std::endl;
				//std::cout << ">> fourcc, quality, bitrate: " << theCodec << ", " << theFrame.codecInfo.quality << ", " << theFrame.codecInfo.bitrate << std::endl;
				//std::cout << ">> isKey: " << theFrame.isKey << std::endl;
				//std::cout << ">> frameLength: " << theFrame.frameLength << std::endl;

				memcpy(charFrame, theFrame.frame, sizeFrame);

				m_retries 	= 0;

				gettimeofday(&now, NULL);
				currentMilis2 	= now.tv_sec*1000 + now.tv_usec/1000;

				if(currentMilis2 - currentMilis1 > delay)
				{
					printf("SivipChunk::getChunk(): WARNING - Comunicación con SIVIP lenta. Tiempo de respuesta = %llu ms\n", currentMilis2 - currentMilis1);
				}

				lock();
					m_codecFrame->addVOPChunk( (void *) charFrame, sizeFrame );
					size 	=  m_codecFrame->getSize();
				unlock();

				if(charFrame != 0) 
				{
					delete[] charFrame;
					charFrame 	= 0;
				}

				if(currentMilis2-currentMilis1 < delay)
				{
					usleep((delay - (currentMilis2-currentMilis1))*1000);
				}

			}
			catch(...)
			{
				printf("SivipChunk::getChunk(): Cannot get frame\n");
				fflush(stdout);
				++m_retries;

				if(m_retries < 8)
				{
					try
					{
						endLiveL9Session();
						startLiveL9Session();
						m_codecFrame->resetQueue();
					}
					catch(...)
					{
						continue;
					}
				}
				else
				{
					stop();
				}
			}

			lock();			
				size 	=  m_codecFrame->getSize();
				isStop 	= m_stopSignal;
			unlock();

			++iter;
			iter = iter % 1000;
			if(iter == 0)
			{
				printf("SivipChunk::run() - thread %08X transmited %llu seconds for camId %u\n", (unsigned int) pthread_self(), (currentMilis1 - startMilis)/1000 , m_camId);
			}	

			//printf("SivipChunk::getChunk(): queueSize %d \n", size);
		}
		endLiveL9Session();
	}	
	else if( isLiveL9EMB() )
	{
		IPFrameBufferSize buffer;
		
		startLiveL9EMBSession();
		delay 	= MS_PER_FRAME;

		gettimeofday(&now, NULL);
		uint64_t startMilis 	= now.tv_sec*1000 + now.tv_usec/1000;

		while( !isStop )
		{
			gettimeofday(&now, NULL);
			currentMilis1 	= now.tv_sec*1000 + now.tv_usec/1000;

			try
			{
				IPFrame theFrame	= m_decodeAsker->getCompressedNextFrame();
				sizeFrame 	= theFrame.frameLength;
				charFrame	= new char[sizeFrame];

				memcpy(charFrame, theFrame.frame, sizeFrame);

				m_retries 	= 0;

				gettimeofday(&now, NULL);
				currentMilis2 	= now.tv_sec*1000 + now.tv_usec/1000;

				if(currentMilis2 - currentMilis1 > delay)
				{
					printf("SivipChunk::getChunk(): WARNING - Comunicación con SIVIP lenta. Tiempo de respuesta = %llu ms\n", currentMilis2 - currentMilis1);
				}

				lock();
					m_codecFrame->addVOPChunk( (void *) charFrame, sizeFrame );
					size 	=  m_codecFrame->getSize();
				unlock();

				if(charFrame != 0) 
				{
					delete[] charFrame;
					charFrame 	= 0;
				}

				if(currentMilis2-currentMilis1 < delay)
				{
					usleep((delay - (currentMilis2-currentMilis1))*1000);
				}

			}
			catch(...)
			{
				printf("SivipChunk::getChunk(): Cannot get frame\n");
				fflush(stdout);
				++m_retries;

				if(m_retries < 8)
				{
					try
					{
						endLiveL9EMBSession();
						startLiveL9EMBSession();
						m_codecFrame->resetQueue();
					}
					catch(...)
					{
						continue;
					}
				}
				else
				{
					stop();
				}
			}

			lock();			
				size 	=  m_codecFrame->getSize();
				isStop 	= m_stopSignal;
			unlock();

			++iter;
			iter = iter % 1000;
			if(iter == 0)
			{
				printf("SivipChunk::run() - thread %08X transmited %llu seconds for camId %u\n", (unsigned int) pthread_self(), (currentMilis1 - startMilis)/1000 , m_camId);
			}	

		}
		endLiveL9EMBSession();
	}	
	else
	{
		printf("SivipChunk::getChunk(): No session avaible\n");
		fflush(stdout);
		stop();
	}
	
	
	m_codecFrame->resetQueue();
	
	printf("SivipChunk::getChunk(): queueSize %d \n", size);
	m_threadIsInit  = false;
	printf("SivipChunk::run() - work finished, thread id:  %08X \n", (unsigned int) pthread_self());
}


