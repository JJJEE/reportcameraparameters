#ifndef _SIVIP_CHUNK_HH
#define _SIVIP_CHUNK_HH
#include <SivipStream.hh>
#include <video/H264FrameQueue.h>
#include <video/MPEG4FrameQueue.h>
#include <time.h>
#include <sys/time.h>	
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/stat.h>
#include <InternalThread.hh>
#include <Live555Event.hh>
#include <FramedSource.hh>
#include <ModuleAccess/RecordingModuleAccess.h>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <ModuleAccess/ControlModuleAccess.h>
#include <Utils/DBGateway.h>
#include <Sockets/Address.h>
#include <Utils/Canis.h>
#include <stdexcept>

#define REPLAY_FRAME_BUFFER 25 	// No tocar este valor a no ser que sepas lo que haces 
#define LIVEL9_FRAME_BUFFER 11 	// Este lo puedes tocar sin miedo, pero afecta al retardo del video en directo
#define LIVEL9EMB_FRAME_BUFFER 11 	// Este lo puedes tocar sin miedo, pero afecta al retardo del video en directo

class SivipChunk: public InternalThread
{
	private: 
		Canis* 					m_canis;
		FrameQueue* 			m_codecFrame;		// Cola de Frames completos, mpeg, h264
		string				 	m_codec;			// codec de la cámara

		RecordingModuleAccess* 	m_recordingAsker;
		RMStartSessionParams* 	m_sessionParams;
		DecodeModuleAccess*		m_decodeAsker;
		ControlModuleAccess*		m_controlAsker;

		uint32_t			 	m_camId;			// Id de la cámara ( en el caso de video embarcado sera la id del canal del codec )
		uint32_t			 	m_trainId;			// Id del tren ( Caso de video embarcado )
		uint32_t			 	m_realCamId;			// Id real de la cámara  ( Caso de video embarcado )
		uint32_t			 	m_initSecond;		// Segundos de inicio de la grabacion
		uint32_t			 	m_currentSecond;	// Segundo actual de transmision de diferido
		uint32_t			 	m_downloadProgress; // Porcentaje de descarga completado		
		uint32_t			 	m_lastMili;			// Ultimo milisegundo de grabacion recuperado	
		uint32_t			 	m_syncMili;					
		uint32_t			 	m_endSecond;		// Segundos de fin de la descarga
		double				 m_scale;			// Factor de escala de velocidad de reproducción

		uint32_t			 	m_fps;				// fps del ultimo segundo

		uint32_t			 	m_isReplaySession;	// Sesion en diferido
		uint32_t			 	m_isLiveL9Session;	// Sesion en directo L9
		uint32_t			 	m_isLiveL9EMBSession;	// Sesion en directo L9 Embarcado
		uint32_t			 	m_isPlay;			// Señal de play
		uint32_t				m_retries;

	private:	
		void run();

		RecordingFileFrameChunk* readFrame(uint32_t sec, uint32_t millis);
		void startReplaySession(uint32_t p_camid = 0);
		void startLiveL9Session(uint32_t p_camid = 0);
		void startLiveL9EMBSession(uint32_t p_camid = 0);
		void endReplaySession();
		void endLiveL9Session();
		void endLiveL9EMBSession();

		RMListRecordingsParams* getRecordingList( qword startDate_ );
		const char* getRecordingServerIp(RMListRecordingsParams *list_, uint32_t currentSeconds, uint32_t camId) ;
		const void printRecordingList(RMListRecordingsParams *list_) const;
		void initCodec(string codec = "mp4v");
		void videoSync(uint32_t startPoint=0);		

	public:
		SivipChunk();
		SivipChunk(string codec);
		~SivipChunk();	
		
		void stop();		

		void setMaxQueueSize(uint32_t maxFrames);
		void setCamId(uint32_t p_camid);
		void setTrainId(uint32_t p_trainid);
		void setRealCamId(uint32_t p_realcamid);
		void setReplayInit(uint32_t seconds);
		void setReplayEnd(uint32_t seconds);
		void setScale(double scale);
		void setReplaySession();
		void setLiveL9Session();
		void setLiveL9EMBSession();
		void setFps(uint32_t fps);

		uint32_t getReferenceCount() const;
		const char* getCodec();
		uint32_t getMaxQueueSize();
		uint32_t getQueueSize();
		uint32_t getCamId();
		const uint32_t getFps();
		void getReplayTime(struct timeval * time);
		uint32_t getReplayInit();
		uint32_t getReplayEnd();
		double	 getScale();
		const Frame *getFrame();

		bool bufferIsReady();	
		bool frameIsReady();	

		bool isReplay();
		bool isLiveL9(); 
		bool isLiveL9EMB();

		void signalPause();
		void signalPlay();
		bool isPlay();
};

#endif
