#ifndef _SIVIP_SOURCE_HH
#define _SIVIP_SOURCE_HH

#ifndef _FRAMED_SOURCE_HH
#include <FramedSource.hh>
#endif

#include <SivipStream.hh>

class SivipSource: public FramedSource {
public:
	static SivipSource* createNew(UsageEnvironment& env, SivipStream* E_Source, unsigned clientSessionId);
	static unsigned GetRefCount();
  	SivipStream* getStreamObject() const;

public:
	EventTriggerId eventTriggerId;

protected:
	SivipSource(UsageEnvironment& env, SivipStream* source, unsigned clientSessionId);
	// called only by createNew(), or by subclass constructors
	virtual ~SivipSource();

private:
	// redefined virtual functions:
	virtual void doGetNextFrame();
	virtual void doStopGettingFrames();

private:
	static void deliverFrame0(void* clientData);
	void deliverFrame();
  	
private:
	static unsigned referenceCount; // used to count how many instances of this class currently exist
	SivipStream* sivipStream;
	const Frame* currentFrame;
	unsigned m_clientSessionId;
};

#endif
