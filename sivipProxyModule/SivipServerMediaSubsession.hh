#ifndef _SIVIP_SERVER_MEDIA_SUBSESSION_HH
#define _SIVIP_SERVER_MEDIA_SUBSESSION_HH

#include <OnDemandServerMediaSubsession.hh>
#include <SivipInput.hh>

class SivipServerMediaSubsession: public OnDemandServerMediaSubsession 
{

public:
static SivipServerMediaSubsession* 
	createNew( UsageEnvironment& env, SivipInput& sivipInput, unsigned estimatedBitrate,
				Boolean reuseBuffer = False,
	   		 	Boolean iFramesOnly = False,
            	double vshPeriod = 120.0 /* how often (in seconds) to inject a Video_Sequence_Header, if one doesn't already appear in the stream */
			  );


protected: // we're a virtual base class
	SivipServerMediaSubsession(UsageEnvironment& env, SivipInput& sivipInput, unsigned estimatedBitrate, Boolean reuseBuffer,Boolean iFramesOnly, double vshPeriod);
	virtual ~SivipServerMediaSubsession();

	virtual void deleteStream(unsigned clientSessionId, void*& streamToken);
	virtual void seekStream(unsigned clientSessionId, void* streamToken, char*& absStart, char*& absEnd);
    virtual void setStreamScale(unsigned clientSessionId, void* streamToken, float scale);

	virtual void pauseStream(unsigned clientSessionId, void* streamToken);
	virtual void startStream(unsigned clientSessionId,
							void* streamToken,
							TaskFunc* rtcpRRHandler,
							void* rtcpRRHandlerClientData,
							unsigned short& rtpSeqNum,
							unsigned& rtpTimestamp,
							ServerRequestAlternativeByteHandler* serverRequestAlternativeByteHandler,
							void* serverRequestAlternativeByteHandlerClientData);

	virtual void stopStream();

protected:
	SivipInput& fInput;
	unsigned fEstimatedKbps;

private:
  	Boolean fIFramesOnly;
  	double fVSHPeriod;

  	// redefined virtual functions
	virtual FramedSource* createNewStreamSource(unsigned clientSessionId, unsigned& estBitrate);
  	virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock, unsigned char rtpPayloadTypeIfDynamic, FramedSource* inputSource);
	Boolean parseClockParam(char const* absTime, int& year, int& month, int& day, int& hour, int& min, int& sec,int& ms);
};

#endif
