#include "HttpSource.hh"
#include <GroupsockHelper.hh> // for "gettimeofday()"
#include <LiveHttpStream.hh>


unsigned HttpSource::referenceCount 		= 0;

HttpSource* HttpSource::createNew(UsageEnvironment& env, LiveHttpStream* E_Source) 
{
	printf("HttpSource::createNew()\n");
	return new HttpSource(env, E_Source);
}


HttpSource::HttpSource(UsageEnvironment& env, LiveHttpStream* E_Source) : FramedSource(env)
{
	printf("HttpSource::HttpSource()\n");
	char const* cam 		= "http://192.168.0.158/image1?speed=25";

	liveSource 		=  E_Source;

	lastFrameId 	= 0;
	currentFrame 	= 0;

	if (referenceCount == 0) { }
	++referenceCount;

	eventTriggerId = envir().taskScheduler().createEventTrigger(deliverFrame0);
	
	TaskScheduler* scheduler;

	liveSource->setEventId(eventTriggerId);

	scheduler 	= &envir().taskScheduler();
	liveSource->setScheduler(scheduler);
	liveSource->setSource(this);
	if( !liveSource->start() )
	{
		liveSource->stop();
		liveSource->wait();
		handleClosure(this);
	}
}

HttpSource::~HttpSource() 
{
	printf("HttpSource::~HttpSource()\n");
	--referenceCount;
	lastFrameId 	= 0;
	//liveSource->stop();

	if( currentFrame != 0 ){
		delete currentFrame;
	}

	envir().taskScheduler().deleteEventTrigger(eventTriggerId);
	eventTriggerId = 0;

}

unsigned HttpSource::GetRefCount() 
{
	return referenceCount;
}

void HttpSource::doGetNextFrame()
{
	//printf("HttpSource::doGetNextFrame() \n");
	
}

void HttpSource::doStopGettingFrames()
{
	printf("HttpSource::doStopGettingFrames() \n");
	// Stop
	liveSource->stop();
	liveSource->wait();
}


void HttpSource::deliverFrame0(void* clientData) 
{
  ((HttpSource*)clientData)->deliverFrame();
}

void HttpSource::deliverFrame() 
{
	//printf("HttpSource::deliverFrame() \n");
	if (!isCurrentlyAwaitingData()) return; // we're not ready for the data yet

	uint64_t frameId;

	currentFrame			= liveSource->getFrame();	
	frameId					= currentFrame->getId();

	if (lastFrameId < frameId && currentFrame != 0)
	{
		u_int8_t* newFrameDataStart 	= (uint8_t *) currentFrame->getData();
		unsigned  newFrameSize 			= currentFrame->getSize();	

		if (newFrameDataStart != NULL) 
		{
			/* This should never happen, but check anyway.. */
			if (newFrameSize > fMaxSize) 
			{
				printf("HttpSource::deliverFrame(): WARNING - Frame too big.\n");
				fFrameSize = fMaxSize;
				fNumTruncatedBytes = newFrameSize - fMaxSize;
			} 
			else
			{
				fFrameSize = newFrameSize;
			}
			gettimeofday(&fPresentationTime, NULL);
			memmove(fTo, newFrameDataStart, fFrameSize);
			if( currentFrame != 0) 
			{ 
				delete currentFrame;
				currentFrame = 0;		
			}
		}
		/*else 
		{
			fFrameSize	= 0;
			fTo			= NULL;
			//handleClosure(this);
		}*/
	}
	
	FramedSource::afterGetting(this);
}

