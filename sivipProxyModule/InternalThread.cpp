#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <InternalThread.hh>

InternalThread::InternalThread()
{
	m_threadIsInit 	= false;
	m_stopSignal 	= false;
	m_threadId 		= 0;
	m_internalMutex 	= PTHREAD_MUTEX_INITIALIZER;
}

InternalThread::~InternalThread()
{
	m_stopSignal 	= true;
	m_threadIsInit  = true;
}

void InternalThread::lock()
{
	pthread_mutex_lock(&m_internalMutex);
}

void InternalThread::unlock()
{
	pthread_mutex_unlock(&m_internalMutex);
}

bool InternalThread::start()
{
	m_stopSignal 	= false;
	if( m_threadIsInit == false )
	{
		if( pthread_create(&m_thread, NULL, entryFunc, this) == 0)
		{
			printf("InternalThread::start() - Thread ok!\n");
			m_threadIsInit 	= true;
		}
		return m_threadIsInit;
	}
	else
	{
		printf("InternalThread::start() - Thread is currently running with id: %08X\n", m_threadId);
		return false;
	}
}
	
void InternalThread::wait()
{
	lock();
		bool isInit 	= m_threadIsInit;
	unlock();

	if( isInit )
	{
		(void) pthread_join(m_thread, NULL);
	}
}

void InternalThread::stop()
{
	printf("InternalThread::stop()\n");
	fflush(stdout);
	lock();
		m_stopSignal 	= true;
	unlock();
}

uint32_t InternalThread::getThreadId() const
{
	if( m_threadIsInit )
	{
		return m_threadId;
	}
	else
	{
		return 0;
	}
}

bool InternalThread::getStopSignalStatus() 
{
	lock();
		bool stopSignal 	= m_stopSignal;
	unlock();

	return stopSignal;
}	

void * InternalThread::entryFunc( void * obj )
{
	((InternalThread *)obj)->lock();
		((InternalThread *)obj)->m_threadId 	= (unsigned int) pthread_self();
	((InternalThread *)obj)->unlock();	

	printf("InternalThread::start() - Thread id: %08X\n", ((InternalThread *)obj)->m_threadId);

	((InternalThread *)obj)->run(); 
	return 0;
}
