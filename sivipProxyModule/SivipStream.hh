#ifndef _SIVIP_STREAM_HH
#define _SIVIP_STREAM_HH
#include <SivipChunk.hh>
#include <video/H264FrameQueue.h>
#include <video/MPEG4FrameQueue.h>
#include <time.h>
#include <sys/time.h>	
#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/stat.h>
#include <InternalThread.hh>
#include <Live555Event.hh>
#include <FramedSource.hh>
#include <ModuleAccess/RecordingModuleAccess.h>
#include <ModuleAccess/DecodeModuleAccess.h>
#include <Utils/DBGateway.h>
#include <Sockets/Address.h>
#include <Utils/Canis.h>
#include <stdexcept>


#define MS_PER_FRAME 40
class SivipChunk; //fwd

class SivipStream: public InternalThread, public Live555Event
{
	private: 
		static uint32_t		m_referenceCount;
		SivipChunk* 		m_sivipChunk;		
		bool		 		m_frameReady;
		
		void run();
	
	public:
		SivipStream();
		SivipStream(string codec);
		~SivipStream();	
		
		void stop();
	
		void setMaxQueueSize(uint32_t maxFrames);
		void setCamId(uint32_t p_camid);
		void setTrainId(uint32_t p_camid);
		void setRealCamId(uint32_t p_camid);
		void setReplayInit(uint32_t seconds);
		void setReplayEnd(uint32_t seconds);
		void setScale(double scale);
		void setReplaySession();
		void setLiveL9Session();
		void setLiveL9EMBSession();

		uint32_t getReferenceCount() const;
		const char* getCodec();
		uint32_t getMaxQueueSize();
		uint32_t getQueueSize();
		uint32_t getCamId();
		void getReplayTime(struct timeval * time);
		uint32_t getReplayInit();
		uint32_t getReplayEnd();
		double   getScale();
		const Frame *getFrame();

		bool bufferIsReady();	
		bool frameIsReady();	

		bool isReplay();
		bool isLiveL9(); 
		bool isLiveL9EMB(); 

		void signalPause();
		void signalPlay();
		bool isPlay();

		void setFrameState(bool state);
};

#endif
