#include <HttpInput.hh>
#include <HttpSource.hh>


HttpInput* HttpInput::createNew(UsageEnvironment& env, LiveHttpStream* phttpStream) 
{
	printf("HttpInput::createNew()\n");

	return new HttpInput(env, phttpStream);
}

FramedSource* HttpInput::videoSource() 
{
	printf("HttpInput::videoSource()\n");

	fOurVideoSource = HttpSource::createNew(envir(), httpStream);
	
	return fOurVideoSource;
}

const char* HttpInput::getCodec() const
{
	return httpStream->getCodec();
}

HttpInput::HttpInput(UsageEnvironment& env, LiveHttpStream* phttpStream): Medium(env)
{
	httpStream = phttpStream;
}

HttpInput::~HttpInput() 
{
	printf("HttpInput::~HttpInput()\n");
	HttpSource::handleClosure(fOurVideoSource);
}


