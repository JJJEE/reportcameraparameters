#include <SivipChunk.hh>
#include <SivipStream.hh>
#include <video/H264VOP.h>
#include <video/H264ChunkProcessor.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/stat.h>
#include <InternalThread.hh>
#include <Live555Event.hh>
#include <FramedSource.hh>
#include <fstream>
#include <string>


uint32_t SivipStream::m_referenceCount 	= 0;

SivipStream::SivipStream(): m_frameReady(false)
{
	//printf("SivipStream::SivipStream()\n");
	m_sivipChunk 	= new SivipChunk();
	++m_referenceCount;
}

SivipStream::SivipStream(string codec): m_frameReady(false)
{
	//printf("SivipStream::SivipStream(string codec)\n");
	m_sivipChunk 	= new SivipChunk(codec);
	++m_referenceCount;
}

SivipStream::~SivipStream()
{
//	printf("SivipStream::~SivipStream()\n");
	--m_referenceCount;

	m_sivipChunk->stop();
	m_sivipChunk->wait();
	if( m_sivipChunk !=0) 		delete m_sivipChunk;

	printf("SivipStream::~SivipStream() bye\n");
}

void SivipStream::stop()
{
	//printf("SivipStream::stop()\n");
	fflush(stdout);

	// Mandamos señal para salir del bucle
	lock();
		m_sivipChunk->stop();
		m_stopSignal 	= true;
	unlock();
}

void SivipStream::setMaxQueueSize(uint32_t maxFrames)
{
	//printf("SivipStream::setMaxQueueSize(uint32_t maxFrames)\n");
	lock();
		m_sivipChunk->setMaxQueueSize(maxFrames);
	unlock();
}

void SivipStream::setCamId(uint32_t p_camid)
{
	//printf("SivipStream::setCamId(uint32_t p_camid)\n");
	lock();
		m_sivipChunk->setCamId(p_camid);
	unlock();
}

void SivipStream::setTrainId(uint32_t p_trainid)
{
	//printf("SivipStream::setTrainId(uint32_t p_trainid)\n");
	lock();
		m_sivipChunk->setTrainId( p_trainid );
	unlock();
}

void SivipStream::setRealCamId(uint32_t p_realcamid)
{
	//printf("SivipStream::setRealCamId(uint32_t p_realcamid)\n");
	lock();
		m_sivipChunk->setRealCamId( p_realcamid );
	unlock();
}

void SivipStream::setReplayInit(uint32_t seconds)
{
	//printf("SivipStream::setReplayInit(uint32_t seconds)\n");
	lock();
		m_sivipChunk->setReplayInit( seconds);
	unlock();
}

void SivipStream::setReplayEnd(uint32_t seconds)
{
	//printf("SivipStream::setReplayEnd(uint32_t seconds)\n");
	lock();
		m_sivipChunk->setReplayEnd( seconds);
	unlock();
}

void SivipStream::setScale(double scale) 
{ 
	//printf("SivipStream::setScale(double scale) \n");
	lock();
		m_sivipChunk->setScale(scale); 
	unlock();
}

void SivipStream::setReplaySession()
{
	//printf("SivipStream::setReplaySession()\n");
	lock();
		m_sivipChunk->setReplaySession();
	unlock();
}

void SivipStream::setLiveL9Session()
{
	//printf("SivipStream::setLiveL9Session()\n");
	lock();
		m_sivipChunk->setLiveL9Session();
	unlock();
}

void SivipStream::setLiveL9EMBSession()
{
	//printf("SivipStream::setLiveL9EMBSession()\n");
	lock();
		m_sivipChunk->setLiveL9EMBSession();
	unlock();
}

uint32_t SivipStream::getReferenceCount() const
{
	//printf("SivipStream::getReferenceCount()\n");
	return m_referenceCount;
}


const char* SivipStream::getCodec()
{
	//printf("SivipStream::getCodec()\n");

	lock();
		const char* codec 	= 	m_sivipChunk->getCodec();
	unlock();

	return codec;
}

uint32_t SivipStream::getMaxQueueSize()
{
	//printf("SivipStream::getMaxQueueSize()\n");

	lock();
		uint32_t size 	= 	m_sivipChunk->getMaxQueueSize();
	unlock();

	return size;
}

uint32_t SivipStream::getQueueSize()
{
	//printf("SivipStream::getQueueSize()\n");

	lock();
		uint32_t size 	= 	m_sivipChunk->getQueueSize();
	unlock();

	return size;
}

uint32_t SivipStream::getCamId()
{
	//printf("SivipStream::getCamId()\n");

	lock();
		uint32_t id 	= 	m_sivipChunk->getCamId();
	unlock();

	return id;
}

void SivipStream::getReplayTime(struct timeval * time)
{
	//printf("SivipStream::getReplayTime(struct timeval * time)\n");

	lock();
		m_sivipChunk->getReplayTime(time);
	unlock();
}

uint32_t SivipStream::getReplayInit()
{
	//printf("SivipStream::getReplayInit()\n");

	lock();
		uint32_t time 	= 	m_sivipChunk->getReplayInit();
	unlock();

	return time;
}

uint32_t SivipStream::getReplayEnd()
{
	//printf("SivipStream::getReplayEnd()\n");

	lock();
		uint32_t time 	= 	m_sivipChunk->getReplayEnd();
	unlock();

	return time;
}

double SivipStream::getScale()
{
	//printf("SivipStream::getScale()\n");

	lock();
		double scale 	= 	m_sivipChunk->getScale();
	unlock();

	return scale;
}

const Frame *SivipStream::getFrame()
{	
	//printf("SivipStream::getFrame()\n");

	lock();
		const Frame* frame	= m_sivipChunk->getFrame();
	unlock();

	return frame;
}

bool SivipStream::bufferIsReady()
{
//	printf("SivipStream::bufferIsReady()\n");

	lock();
		bool ret 	= 	m_sivipChunk->bufferIsReady(); 
	unlock();
	return ret; 
}

bool SivipStream::frameIsReady()
{
	//printf("SivipStream::frameIsReady()\n");

	bool ret 		= false;
	lock();
		int queueSize 	= m_sivipChunk->getQueueSize();
	unlock();	
	if( queueSize > 0 )
	{
		lock();
			if(m_frameReady == true) ret = true;
		unlock();	
	}

  	return ret; 
}
	
bool SivipStream::isReplay() 
{ 
	//printf("SivipStream::isReplay() \n");

	lock();
		bool ret 	= 	m_sivipChunk->isReplay(); 
	unlock();
	return ret; 
}
     
bool SivipStream::isLiveL9() 
{ 
	//printf("SivipStream::isLiveL9() \n");

	lock();
		bool ret 	= m_sivipChunk->isLiveL9();
	unlock();

	return ret;
}
     
bool SivipStream::isLiveL9EMB() 
{ 
	//printf("SivipStream::isLiveL9EMB() \n");

	lock();
		bool ret 	= m_sivipChunk->isLiveL9EMB();
	unlock();

	return ret;
}

void SivipStream::signalPause()
{
	//printf("SivipStream::signalPause()\n");

	lock();
		m_sivipChunk->signalPause(); 
	unlock();
}

void SivipStream::signalPlay()
{
	//printf("SivipStream::signalPlay()\n");

	lock();
		m_sivipChunk->signalPlay(); 
	unlock();
}

bool SivipStream::isPlay() 
{ 
	//printf("SivipStream::isPlay() \n");

	lock();
		bool ret 	= m_sivipChunk->isPlay();
	unlock();
	return ret;
}

void SivipStream::setFrameState(bool state) 
{ 
	//printf("SivipStream::setFrameState(bool state) \n");

	lock();
		m_frameReady	= state;
	unlock();
}

void SivipStream::run()
{
	//printf("SivipStream::run()\n");

	// Ejecutamos otro hilo para pedir los frames
	// en este los mandamos.
	m_sivipChunk->start();  	 
						
	double delay 			= 40;
	uint32_t queueSize		= 0;
	bool isStop 			= false;
	uint32_t fps			= 0;
    double acumDelay 		= 0;
	uint32_t numFrames		= 0;
	uint64_t acumBytes		= 0;
	struct timeval start;

	printf("SivipStream::run() - thread id: %08X - count: %u \n", (unsigned int) pthread_self(), getReferenceCount());

	// Esperamos que el buffer este cargado
	while( bufferIsReady() == false && !isStop )
	{
		usleep(10000);
		lock();
			if( m_sivipChunk->getStopSignalStatus() == true || m_stopSignal == true)
				isStop 	= true;
			else 
				isStop 	= false;
		unlock();
	}

	// Bucle de session en diferido
	if( isReplay() )
	{
		while( !isStop )
		{
			fps 	= m_sivipChunk->getFps();

			if (fps != 0)
			{
				delay 	= ((1000.0/(double)m_sivipChunk->getFps())/getScale());							// Retardo entre frames en ms
			}
			else
			{
				delay 	= ( (MS_PER_FRAME)/getScale());							// Retardo entre frames en ms
			}	

			if( isPlay() == true && delay >0 )
			{
				setFrameState(true);
				++numFrames;
				notifyNewFrame();
				queueSize 	= getQueueSize();
								
				if(queueSize >= (REPLAY_FRAME_BUFFER-1))
				{
					usleep(delay*1000);
					acumDelay += delay;	
					//printf("SivipStream::run() delay=%f\n", delay);
				}
				else if(queueSize != 0)
				{
					// Como mucho estiramos el tiempo mas cuanto mas rapida sea la velocidad de reproduccion
					uint32_t extraDelay = (uint32_t)(( ((double)(REPLAY_FRAME_BUFFER-1) - (double)queueSize)/((double)REPLAY_FRAME_BUFFER-1) )*MS_PER_FRAME*2);

					usleep( (delay +  extraDelay )*1000);
					acumDelay += delay;	
					//printf("SivipStream::run() delay=%f\n", delay+extraDelay);
				}
				else
				{
					printf("SivipStream::run() buffer not ready\n");
					while( bufferIsReady() == false && !isStop)
					{
						usleep(delay*1000);
						acumDelay += delay;
						lock();
							if( m_sivipChunk->getStopSignalStatus() == true || m_stopSignal == true)
								isStop 	= true;
							else 
								isStop 	= false;
						unlock();
					}
				}

				if(numFrames % 1000 == 0)
				{
					printf("SivipStream::run() Statistics thread id: %u \n", (unsigned int) pthread_self());
					printf("\tTime between frames = %f ms\n", (double)numFrames/acumDelay);
					printf("\tDownload Speed = x%f\n", ( (double)numFrames/24)*1000/acumDelay );
				}
			}
			else if( isPlay() == true && delay < 0)
			{
				setFrameState(true);
				notifyNewFrame();
				usleep( -1*1000000/getScale() ); // Mandamos key frame multiplo de segundo en funcion de la velocidad	
			}
			else // Pausa
			{
				usleep(40000);
			}

			lock();
				if( m_sivipChunk->getStopSignalStatus() == true || m_stopSignal == true)
					isStop 	= true;
				else 
					isStop 	= false;
			unlock();	
		}

		if( m_sivipChunk->getStopSignalStatus() == false)
		{ 
			lock();
				m_sivipChunk->stop();  	 
			unlock();
		}
	}

	// Bucle de session en directo L9
	else if( isLiveL9() )
	{
		delay 			= MS_PER_FRAME;											// Retardo entre frames (25 fps)
		while( !isStop )
		{
			setFrameState(true);
			notifyNewFrame();
			queueSize 	= getQueueSize();

			if(queueSize >= (LIVEL9_FRAME_BUFFER-1))
			{
				usleep(delay*1000);
				//printf("sleep1: %f\n", delay*1000);
			}
			else if(queueSize != 0)
			{
				// Como mucho estiramos el tiempo por 1
				uint32_t extraDelay = (uint32_t)(( ((double)(LIVEL9_FRAME_BUFFER-1) - (double)queueSize)/((double)LIVEL9_FRAME_BUFFER-1) )*MS_PER_FRAME*2); 
				usleep( (delay +  extraDelay )*1000);
				//printf("sleep2: %f \n",(delay +  extraDelay )*1000); 
			}
			else
			{
				printf("Buffer not ready\n");
				while( bufferIsReady() == false && !isStop)
				{
					usleep(40000);
					lock();
						if( m_sivipChunk->getStopSignalStatus() == true || m_stopSignal == true)
							isStop 	= true;
						else 
							isStop 	= false;
					unlock();
				}
			}

			lock();
				if( m_sivipChunk->getStopSignalStatus() == true || m_stopSignal == true)
					isStop 	= true;
				else 
					isStop 	= false;
			unlock();

		}	

		if( m_sivipChunk->getStopSignalStatus() == false)
		{ 
			lock();
				m_sivipChunk->stop();  	 
			unlock();
		}	
	}

	// Bucle de session en directo EMBARCADO
	else if( isLiveL9EMB() )
	{
		delay 			= MS_PER_FRAME;											// Retardo entre frames (25 fps)
		while( !isStop )
		{
			setFrameState(true);
			notifyNewFrame();
			queueSize 	= getQueueSize();

			if(queueSize >= (LIVEL9EMB_FRAME_BUFFER-1))
			{
				usleep(delay*1000);
				//printf("sleep1: %f\n", delay*1000);
			}
			else if(queueSize != 0)
			{
				// Como mucho estiramos el tiempo por 1
				uint32_t extraDelay = (uint32_t)(( ((double)(LIVEL9EMB_FRAME_BUFFER-1) - (double)queueSize)/((double)LIVEL9EMB_FRAME_BUFFER-1) )*MS_PER_FRAME*2); 
				usleep( (delay +  extraDelay )*1000);
			}
			else
			{
				printf("Buffer not ready\n");
				while( bufferIsReady() == false && !isStop)
				{
					usleep(40000);
					lock();
						if( m_sivipChunk->getStopSignalStatus() == true || m_stopSignal == true)
							isStop 	= true;
						else 
							isStop 	= false;
					unlock();
				}
			}

			lock();
				if( m_sivipChunk->getStopSignalStatus() == true || m_stopSignal == true)
					isStop 	= true;
				else 
					isStop 	= false;
			unlock();

		}	

		if( m_sivipChunk->getStopSignalStatus() == false)
		{ 
			lock();
				m_sivipChunk->stop();  	 
			unlock();
		}	
	}

	m_threadIsInit  = false;
	printf("SivipStream::run() - work finished, thread id: %08X \n", (unsigned int) pthread_self());
}

	

