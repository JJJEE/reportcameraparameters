#include <DynamicRTSPSivipServer.hh>
#include <liveMedia.hh>
#include <string.h>
#include <iostream>
#include <map>
#include <set>
#include <stdio.h>
#include <stdlib.h>  
#include <SivipStream.hh>
#include <SivipInput.hh>
#include <SivipServerMediaSubsession.hh>
#include <XmlParser.h>
#include <Utils/DBConnection.hh>
#include <cstdlib>

static ServerMediaSession* createNewSMS(	UsageEnvironment& env, 
														char const* streamName	); 

Canis* m_canis	= 0;

DynamicRTSPSivipServer* DynamicRTSPSivipServer::createNew(	UsageEnvironment& env, 
																				Port ourPort,
			     																UserAuthenticationDatabase* authDatabase,
			     																unsigned reclamationTestSeconds	)
{
  int ourSocket = setUpOurSocket(env, ourPort);

  if (ourSocket == -1) 
  	return NULL;

  return new DynamicRTSPSivipServer(	env, 	
  													ourSocket, 
													ourPort, 
													authDatabase, 
													reclamationTestSeconds	);
}

DynamicRTSPSivipServer::DynamicRTSPSivipServer(	UsageEnvironment& env, 
																int ourSocket,
			     												Port ourPort,
				     											UserAuthenticationDatabase* authDatabase, 
				     											unsigned reclamationTestSeconds	)	
																	: RTSPServer(	env, 
																						ourSocket, 
																						ourPort, 
																						authDatabase, 
																						reclamationTestSeconds	) 
{}

DynamicRTSPSivipServer::~DynamicRTSPSivipServer() 
{}


ServerMediaSession* DynamicRTSPSivipServer::lookupServerMediaSession(	char const* streamName, 
																								Boolean isFirstLookupInSession	) 
{
  	printf("lookupServerMediaSession --------->\n");

	ServerMediaSession* sms = RTSPServer::lookupServerMediaSession( streamName );

	if (sms == NULL) 
	{
		sms = createNewSMS(	envir(), 
									streamName	); 
		addServerMediaSession(sms);
	}

	printf("<------------ lookupServerMediaSession\n");

	return sms;

}

static ServerMediaSession* createNewSMS(	UsageEnvironment& env,
														char const* streamName	)  
{
	printf("createNewSMS --------->\n");
	std::cout << "streamName: " << streamName << std::endl;

	unsigned int camId = getCamId( streamName );	
	char buffer1[256];
	char buffer2[256];
	XmlParser myXML;
	string response;
	std::string codec;
	std::string bitrate;
	int count;

	string confFile  = "SivipRtspProxyServerConfig.xml";
	char const* descriptionString = "Session streamed by \"SIVIP RTSP PROXY\" (Marina Eye-Cam Tecnologies)";

	if( m_canis == 0)
		m_canis 	= new Canis(confFile.c_str(),true);
	
	printf("Creando cliente BBDD ...\n");

	DBConnection* myDB 	= 0;
	myDB 	= new DBConnection(	m_canis->getAddress(), 
										m_canis->getType(), 
										m_canis	); 	
	
	if( myDB == 0 )
	{
		printf("Error: Sin Memoria \n");
		return 0;
	}
	
	printf("Consultando BBDD... \n");
		sprintf(	buffer1, 
					"SELECT value FROM getallconfigparamsfordevice(%u) WHERE name='/Video/1/Codec/Fourcc'",
					camId	);	

	do
	{	
		response 	= myDB->query(buffer1);
	}
	while( response.size() < 1 );

	printf("response: %s\n", response.c_str());

	myXML.setXml(response.c_str(), response.size());
	myXML.addTag("<row>");
	myXML.addTag("<value>");

	count 	= myXML.read();
	printf("count: %d\n", count);

	if( count != 0)
		codec 		= myXML.getTag(0);
	else
		codec 		= "mpeg4";

	myXML.cleanTag();

		sprintf(	buffer2, 
					"SELECT value FROM getallconfigparamsfordevice(%u) WHERE name='/Video/1/Codec/Bitrate'",
					camId	);

	do
	{	
		response 	= myDB->query(buffer2);
	}
	while( response.size() < 1 );	

	printf("response: %s\n", response.c_str());
	myXML.setXml(response.c_str(), response.size());
	myXML.addTag("<row>");
	myXML.addTag("<value>");

	count 	= myXML.read();

	if( count != 0)
		bitrate 		= myXML.getTag(0);
	else
		bitrate 		= "0";
	
	myXML.cleanTag();
	printf("bitrate: %s \n", bitrate.c_str());
 		
	ServerMediaSession* sms = NULL;
	bool reuse;
	
	printf("stream name: %s \n", streamName);
	printf("cam ID: %u \n", camId);

	SivipStream* camStream;
	camStream 				= new SivipStream( codec.c_str());
	camStream->setCamId( camId ); 

	if ( checkTypeStream( streamName, "_tr" ) )
	{
		string str = streamName;
		
      // Parseamos la ID del tren 
      std::string startToken = "_tr";
      std::string endToken = "_";
      
      std::size_t startTrainId = str.find( startToken ) + startToken.size();
      std::size_t endTrainId = str.find( endToken , startTrainId );
      std::string trainId = str.substr( startTrainId, endTrainId - startTrainId ); 
      unsigned int trainIdNum = std::atoi( trainId.c_str() );

      // Parseamos la ID de la cámara 
      startToken = "_ca";

      std::size_t startCamId = str.find( startToken ) + startToken.size();
      std::size_t endCamId = str.find( endToken , startCamId );
      std::string realCamId = str.substr( startCamId, endCamId - startCamId ); 
      unsigned int realCamIdNum = std::atoi( realCamId.c_str() );

		camStream->setTrainId( trainIdNum );
		camStream->setRealCamId( realCamIdNum );
	}

	int bitrateOffset;

	if( 	checkTypeStream( streamName, "live" )	)	// Video en directo
	{
		if ( checkTypeStream( streamName, "_tr" ) )
			camStream->setLiveL9EMBSession();
		else
			camStream->setLiveL9Session();

		reuse 	= true;
	}
	else	// Video en diferido
	{
		camStream->setReplaySession();
		reuse 	= false;
	}

	camStream->setMaxQueueSize(100);
	bitrateOffset = 0; 

	SivipInput* inputDevice		= SivipInput::createNew(	env, 
																			camStream	);

	setInput(	streamName, 
					inputDevice	);

	sms 	= ServerMediaSession::createNew(	env, 
														streamName, 
														streamName, 
														descriptionString	);

	sms->addSubsession( SivipServerMediaSubsession::createNew(	env, 
																					*inputDevice, 
																					atoi( bitrate.c_str() ) + bitrateOffset, 
																					reuse	) 
							);

	if(myDB != 0)
	{
		delete myDB;
		myDB = 0;		
	}

	printf("<------------ createNewSMS\n");
  	return sms;
}


