#include <H264VideoRTPSink.hh>
#include <H264VideoStreamFramer.hh>
#include <H264VideoStreamDiscreteFramer.hh>
#include <MPEG4VideoStreamDiscreteFramer.hh>
#include <MPEG4ESVideoRTPSink.hh>

#include <HttpServerMediaSubsession.hh>

HttpServerMediaSubsession* HttpServerMediaSubsession::
	createNew(UsageEnvironment& env, HttpInput& httpInput, unsigned estimatedBitrate,
				Boolean iFramesOnly,
				double vshPeriod)
{
	printf("HttpServerMediaSubsession::HttpServerMediaSubsession()\n");
	return new HttpServerMediaSubsession(env, httpInput, estimatedBitrate, iFramesOnly, vshPeriod);
}

HttpServerMediaSubsession::HttpServerMediaSubsession(UsageEnvironment& env, HttpInput& httpInput, unsigned estimatedBitrate, Boolean iFramesOnly, double vshPeriod):
OnDemandServerMediaSubsession(env, True /*reuse the first source*/), fHttpInput(httpInput), fIFramesOnly(iFramesOnly), fVSHPeriod(vshPeriod) 
{
		fEstimatedKbps = (estimatedBitrate + 500)/1000;
}

HttpServerMediaSubsession::~HttpServerMediaSubsession() 
{
	printf("HttpServerMediaSubsession::~HttpServerMediaSubsession()\n");
}

FramedSource* HttpServerMediaSubsession ::createNewStreamSource(unsigned clientSessionId, unsigned& estBitrate) 
{
	printf("HttpServerMediaSubsession::createNewStreamSource() - clienteSessionId: %u\n", clientSessionId);
	estBitrate = fEstimatedKbps;

	string codec 	= fHttpInput.getCodec();
	if( codec == "H264"){	
	printf("HttpServerMediaSubsession::createNewStreamSource(): H264\n");
		return H264VideoStreamDiscreteFramer::createNew(envir(), fHttpInput.videoSource());
	}
	if(codec == "MPEG4"){
	printf("HttpServerMediaSubsession::createNewStreamSource(): MPEG4\n");
		return MPEG4VideoStreamDiscreteFramer::createNew(envir(), fHttpInput.videoSource());
	}
}

RTPSink* HttpServerMediaSubsession ::createNewRTPSink(Groupsock* rtpGroupsock, unsigned char /*rtpPayloadTypeIfDynamic*/, FramedSource* /*inputSource*/) 
{
	printf("HttpServerMediaSubsession::createNewRTPSink()\n");
	fHttpInput.setVideoRTPSinkBufferSize();

	string codec 	= fHttpInput.getCodec();
	if( codec == "H264"){	
	printf("HttpServerMediaSubsession::createNewRTPSink(): H264\n");
		return H264VideoRTPSink::createNew(envir(), rtpGroupsock, 96);
	}
	if(codec == "MPEG4"){
	printf("HttpServerMediaSubsession::createNewRTPSink(): MPEG4\n");
		return MPEG4ESVideoRTPSink::createNew(envir(), rtpGroupsock, 96);
	}
}


