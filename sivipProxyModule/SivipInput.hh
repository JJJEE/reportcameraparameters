#ifndef _SIVIP_INPUT_HH
#define _SIVIP_INPUT_HH

#include <MediaSink.hh>
#include <LiveHttpStream.hh>
#include <SivipSource.hh>

#define VIDEO_MAX_FRAME_SIZE 10000000
class SivipInput: public Medium 
{
public:
	static SivipInput* createNew(UsageEnvironment& env, SivipStream* psivipStream);
	FramedSource* videoSource();
	const char* getCodec() const;
	void setClientSessionId(unsigned clientSessionId);
	// Functions to set the optimal buffer size for RTP sink objects.
	// These should be called before each RTPSink is created.
	inline void setVideoRTPSinkBufferSize() { OutPacketBuffer::maxSize = VIDEO_MAX_FRAME_SIZE; }
	SivipStream* getStreamObject() const;

private:
	SivipInput(UsageEnvironment& env, SivipStream* pStream); // called only by createNew()
	virtual ~SivipInput();

private:
	Boolean fHaveInitialized;
	FramedSource* fOurVideoSource;
	SivipStream* sivipStream;
	unsigned m_clientSessionId;

};

#endif
