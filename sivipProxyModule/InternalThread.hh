#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>

#ifndef _INTERNAL_THREAD_HH
#define _INTERNAL_THREAD_HH

class InternalThread
{
private:
	pthread_t 			m_thread;			// Thread reference
	pthread_mutex_t 	m_internalMutex;	// Internal mutex
	uint32_t 			m_threadId;

   	static void * entryFunc(void * obj);	// Reference to the thread run function

protected:
	virtual void run() = 0;
   	void lock();			// Lock mutex
   	void unlock();			// Unlock mutex
	bool m_stopSignal;		// Flag to stop thread execution
	bool m_threadIsInit;	// Flag for init thread

public:
   InternalThread();
   virtual ~InternalThread();

   	bool start();						// Start a new thread
   	void wait();						// Wait
	
	uint32_t getThreadId() const;		// ID of the new thread
	bool getStopSignalStatus();			// value of m_stopSignal
	virtual void stop();				// Stop thread execution

};

#endif
