#include <SivipInput.hh>
#include <SivipSource.hh>

SivipInput* SivipInput::createNew(	UsageEnvironment& env, 
												SivipStream* psivipStream	) 
{
	printf("SivipInput::createNew()\n");

	return new SivipInput(env, psivipStream);
}

FramedSource* SivipInput::videoSource() 
{
	printf("SivipInput::videoSource()\n");
	// Caso especial con m_clientSessionId = 0 (pre-arranque)
	if (	m_clientSessionId == 0 )
	{
		fOurVideoSource = SivipSource::createNew(	envir(), 
																sivipStream, 
																m_clientSessionId	);
	}

	// Iniciamos de verdad
	if (!fHaveInitialized  && m_clientSessionId != 0 )
	{
		printf("\tSivipInput::videoSource() - New Video Source\n");
		fOurVideoSource = SivipSource::createNew(	envir(), 
																sivipStream, 
																m_clientSessionId	);
		fHaveInitialized 	= true;
	}

	return fOurVideoSource;
}

SivipStream* SivipInput::getStreamObject() const
{
	return sivipStream;
}

const char* SivipInput::getCodec() const
{
	return sivipStream->getCodec();
}

void SivipInput::setClientSessionId(unsigned clientSessionId)
{
	m_clientSessionId 	= clientSessionId;
}

SivipInput::SivipInput(UsageEnvironment& env, SivipStream* psivipStream): Medium(env)
{
	fHaveInitialized 	= false;
	m_clientSessionId = 0;
	fOurVideoSource 	= NULL;
	sivipStream 		= psivipStream;
	sivipStream->getCamId();
}

SivipInput::~SivipInput() 
{
	printf("SivipInput::~SivipInput()\n");

	if(fOurVideoSource != NULL) SivipSource::handleClosure(fOurVideoSource);

	if(	sivipStream != NULL	)
	{
		sivipStream->stop();
		sivipStream->wait();
		delete sivipStream;
		sivipStream 	= 0;
	}
	
	printf("SivipInput::~SivipInput() bye\n");
}


