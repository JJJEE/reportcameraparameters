#include <Http/HttpServer.h>	
#include <Http/HttpClient.h>	
#include <Http/HttpStream.h>
#include <Utils/StrUtils.h>
#include <Utils/DBConnection.hh>
#include <XmlParser.h>
#include <iostream>
#include <exception>
#include <fstream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <stdio.h>
#include <iostream>     // std::cout
#include <algorithm>    // std::transform
#include <stdio.h>
#include <string.h>
#include <ctime>
#include <signal.h>
#include <sys/time.h>
#include <stdlib.h>

//Fichero de configuracion
string confFile  = "ActualizaBDCamarasConfig.xml";

// fwd
const char* readFile(const char* name); 
void cameraparameters(const char* ip, const char* port,int &len, char * respuesta);
void codecparameters(const char* ip,  const char* port, const char * stream ,int &len, char * respuesta);
void consultayActualizaBD();
void sigpipe_handler(int param);
void errorAccesoBD(int param);
int canis_inicializado=0;
long N_peticiones = 0;
struct timeval comienzo, acabado;
string Intervaloms_str,Peticiones_str,PeriodoBD_str;
//Canis* myCanis 		= 0;
//DBConnection* myDB 	= 0;





int main(int argc, char **argv)
{
	// Leemos configuracion

	//int port	= 0;

	XmlParser* myXML;

	int count = 0;

	string xmlConf 	= readFile(confFile.c_str());
	myXML=new XmlParser(xmlConf.c_str(), xmlConf.size());
	myXML->addTag("<Peticiones>");
	count = myXML->read();
	if (count )
		Peticiones_str 	= myXML->getTag(0);
	else
		Peticiones_str = "1000"; //Por defecto 1000 peticiones
	myXML->cleanTag();


	myXML->addTag("<Intervaloms>");
	count = myXML->read();
	if (count )
		Intervaloms_str= myXML->getTag(0);
	else
		Intervaloms_str = "1000"; //Por defecto intervalo de 1000 ms
	myXML->cleanTag();


	myXML->addTag("<PeriodoBD>");
	count = myXML->read();
	if (count )
		PeriodoBD_str 	= myXML->getTag(0);
	else
		PeriodoBD_str = "25"; //Por defecto periodo 25
	myXML->cleanTag();

	//printf("Server Ip: %s\n", ip.c_str() );
	delete myXML;

	gettimeofday(&comienzo,NULL);
	gettimeofday(&acabado,NULL);
	signal(SIGPIPE,sigpipe_handler);

	cout << __DATE__ << endl;
	cout << __TIME__ << endl;
	
	consultayActualizaBD();
}


void cameraparameters(const char* ip, const char* port,int &len, char *respuesta)
{
	cout <<  "cameraparameters -------------->" << endl;
	HttpStream	*cameraStream 	= NULL;
	HttpClient 	*httpCli		= NULL;
	string content;
	string url;
	string cameraIp				= ip;
	string cameraPort= port;

	if( cameraIp.size() < 7)
	{
		len = 0;
		return; //0;
	}

	url 		= "http://"+cameraIp+":"+cameraPort+"/command/inquiry.cgi?inq=camera";
	
	cout << "url: " << url << endl;
	httpCli 	= new HttpClient(url);
	httpCli->setConnectionTimeout(10000);
	httpCli->setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
	httpCli->setHeader("Accept","text/html");
	httpCli->setHeader("Connection","keep-alive");
	//Cuerpo de la conexión
	try {
		

		time_t now = time(0);
		char *dt = ctime(&now);
		//cout << "Time request: " << dt << endl;
		//cout << "cameraparameters: Sending Request " << endl;
		cameraStream = httpCli->sendRequest();
		
		//cout << "cameraparameters: getNextInformationChunk " << endl;
		string hdr			= cameraStream->getNextInformationChunk();

		//cout << "cameraparameters: getRequestStatus " << endl;
		int status			= cameraStream->getRequestStatus();

		if(status == 200)
		{
			//cout << "cameraparameters: getRequestStatus OK " << endl;
			content 		= cameraStream->getNextInformationChunk();
			len 			= content.size();
		}

	}
	//Cualquier error en la conexión
	catch (...) {
		
		time_t now_despres = time(0);
		char *dt_despres = ctime(&now_despres);
		//cout << "Time request: " << dt_despres << endl;

		content = "Camara desconectada";
		memmove(respuesta, content.c_str(), content.size());
		len = 0;
		if (cameraStream != NULL)
		{
			delete cameraStream;
			cameraStream = NULL;
		}
		if (httpCli !=NULL)
		{
			delete httpCli;
			httpCli=NULL;
		}
		cout << "Camara desconectada " << endl;	
		return; 
				
	}

	if(cameraStream != NULL)
	{
		delete cameraStream;
		cameraStream 	= NULL;
	}
	if(httpCli != NULL)
	{
		httpCli->CloseConnection();
		delete httpCli;
		httpCli 	= NULL;
	}


	
	memmove(respuesta, content.c_str(), content.size());

	cout << "<--------------cameraparameters" <<  endl;
	return;// respuesta;
}



void codecparameters(const char* ip,  const char* port,const char * stream_param, int &len, char *respuesta)
{
	cout <<  "codecparameters -------------->" << endl;
	HttpClient 	*httpCli		= 0;		
	HttpStream	*cameraStream 	= 0;
	string content;
	string url;
	string cameraIp				= ip;
	string cameraPort			= port;
	string camera_stream_param = stream_param;
	int z = atoi(camera_stream_param.c_str()) +1;
	char prueba_vid[10];
	
	sprintf(prueba_vid,"%d",z);
	
	camera_stream_param = "video";
	
	camera_stream_param.append(1,prueba_vid[0]);

	if( cameraIp.size() < 7)
	{
		len = 0;
		return ;
	}

	
	url 		= "http://"+cameraIp+":"+cameraPort+"/command/inquiry.cgi?inq="+camera_stream_param;
	
	cout << "url: " << url << endl;
	httpCli 	= new HttpClient(url);
	httpCli->setConnectionTimeout(5000);
	httpCli->setHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
	httpCli->setHeader("Accept","text/html");
	httpCli->setHeader("Connection","keep-alive");
	
	//Cuerpo de la conexión
	try {
		

		//cout << "codecparameters: Sending Request " << endl;
		cameraStream		= httpCli->sendRequest();
	
		
		//cout << "codecparameters: getNextInformationChunk " << endl;
		string hdr			= cameraStream->getNextInformationChunk();

		//cout << "codecparameters: getRequestStatus " << endl;
		int status			= cameraStream->getRequestStatus();

		if(status == 200)
		{
			//cout << "codecparameters: getRequestStatus OK " << endl;
			content 		= cameraStream->getNextInformationChunk();
			len 			= content.size();
		}

	}
	//Cualquier error en la conexión
	catch (...) {
		
		content = "Camara desconectada";
		//char* pic_desconexion 	= new char[content.size()];
		memmove(respuesta, content.c_str(), content.size());
		len = 0;
		if (httpCli != 0)
		{
			delete httpCli;
			httpCli = 0;
		}
	
		return; // pic_desconexion;
				
	}

	if(cameraStream != 0)
	{
		delete cameraStream;
		cameraStream 	= 0;
	}
	if(httpCli != 0)
	{
		delete httpCli;
		httpCli 	= 0;
	}

	memmove(respuesta, content.c_str(), content.size());

	cout << "<--------------codecparameters" <<  endl;
	return;// pic;
}

const char* readFile(const char* name)
{
	string strFile, line;
	string newline = "\n";
	fstream thisFile;
	thisFile.open(name, ios::in); 	
	if(thisFile.is_open() )
	{
		while( getline(thisFile, line) )
		{
			strFile += line + newline;
		}
	}
	else
	{
		printf("Error abriendo fichero de configuración %s\n", name);
		return 0;
	}
	thisFile.close();

	return strFile.c_str();
}



void consultayActualizaBD()
{


	string response;
	int len				= 0;
	Canis* myCanis 		= 0;
	char buffer1[256];
	char buffer2[256];

	DBConnection* myDB 	= 0;
	string dbResponse,dbResponse_modelo, dbResponse_stream;
	int count = 0;
	int count_1 = 0;
	int count_2 = 0;
	int count_3 = 0;
	char *pEnd;

	int camera_Id_count;
	string content_parameters;
	const char *prueba_char = NULL;
	
	
	myCanis 			= new Canis(confFile.c_str(),false);
	if (myCanis == 0)
		printf("Canis no inizializada");
	myDB 	= new DBConnection(myCanis->getAddress(), myCanis->getType(), myCanis);


	long numeroPeticiones = strtol(Peticiones_str.c_str(),&pEnd,10);
	if ((numeroPeticiones == 0L) || (numeroPeticiones == LONG_MAX) || (numeroPeticiones == LONG_MIN)) {
		numeroPeticiones = 1000; // Por defecto 1000 peticiones
	}

	long periodoBD = strtol(PeriodoBD_str.c_str(),&pEnd,10);
	if ((periodoBD == 0L) || (periodoBD == LONG_MAX) || (periodoBD == LONG_MIN)) {
		periodoBD = 25; // Por defecto periodo 25
	}


	while (N_peticiones < numeroPeticiones)
	{
	count = 0;
	count_1 = 0;
	count_2 = 0;
	count_3 = 0;


	
	//Actualizamos la BD video con los registros añadidos/borrados de dispositivos. Chequeamos una de cada 10 peticiones para no sobrecargar la BD

	//Camara inexistente en dispositivo

	if (N_peticiones%periodoBD == 0)  {
		
		//cout << "Prueba: " << N_peticiones << endl;

		sprintf (buffer2, "DELETE FROM video WHERE id=(SELECT id from video EXCEPT SELECT id FROM dispositivo LIMIT 1);");
		try {
			XML *res 	= myDB->update(buffer2);
			string respuesta_bd = res->toString();
			//cout << "Respuesta 1: " << respuesta_bd.c_str() << endl; 
			size_t posicion_texto;
			posicion_texto = respuesta_bd.find("rowsUpdated=\"1\"");
			if (posicion_texto != string::npos )  {
				cout << "Camara inexistente en dispositivo"  << endl << endl ;

			}
			delete res;
		}
		catch (...)  {
			errorAccesoBD(12);
			continue;
		}


		//Camara inexistente en video	
		sprintf (buffer2, "INSERT INTO video SELECT id FROM dispositivo WHERE fabricante=1 EXCEPT SELECT id from video;");
		try {

			XML *res 	= myDB->update(buffer2);
			string respuesta_bd = res->toString();
			//cout  << endl << "Respuesta 2: " << respuesta_bd.c_str() << endl; 
			size_t posicion_texto;
			posicion_texto = respuesta_bd.find("rowsUpdated=\"1\"");
			if (posicion_texto != string::npos)  {
				cout << "Camara inexistente en video" << endl;
			}
			delete res;
		}
		catch (...)  {
			errorAccesoBD(11);
			continue;
		}


	}


	if (N_peticiones%periodoBD == 0) {

		//seleccionamos el registro menos reciente de video, datebd mas antigua
		//cout << "Lectura yes" << endl;
		sprintf (buffer2, "SELECT id FROM video ORDER BY datebd LIMIT 1");	
		//dbResponse 	= myDB->query(buffer2);
		try {

			prueba_char 	= myDB->query(buffer2);
			dbResponse.assign(prueba_char);
			delete [] prueba_char;
		}
		catch (...)  {
			errorAccesoBD(10);
			continue;
		}

		string id_str;

		XmlParser* myXMLBBDD;
		myXMLBBDD=new XmlParser(dbResponse.c_str(), dbResponse.size());

		myXMLBBDD->addTag("<row>");
		myXMLBBDD->addTag("<id>");
		count 	= myXMLBBDD->read();
		if (count) {
			id_str = myXMLBBDD->getTag(0);
			cout << "Actualizacion BD video codec,x,y  Id: " << id_str << endl;		
		}
		myXMLBBDD->cleanTag();

		delete 	myXMLBBDD;
		sprintf (buffer1, "SELECT value from getDeviceConfigParamByPath(%s,'Video/1/Codec/Fourcc')", id_str.c_str() );
		try {

			prueba_char 	= myDB->query(buffer1);
			dbResponse.assign(prueba_char);
			delete [] prueba_char;
		}
		catch (...)  {
			errorAccesoBD(4);
			continue;
		}


		XmlParser* myXMLCodec;
		myXMLCodec=new XmlParser(dbResponse.c_str(), dbResponse.size());
		string cdc_str;

		myXMLCodec->addTag("<row>");
		myXMLCodec->addTag("<value>");
		count 	= myXMLCodec->read();
		if (count) {
			cdc_str = myXMLCodec->getTag(0);
			//cout << "Codec " << cdc_str << endl;		
		}
		else
			cdc_str = "---";
		myXMLCodec->cleanTag();

		delete myXMLCodec;


		sprintf (buffer1, "SELECT value from getDeviceConfigParamByPath(%s,'Video/1/Resolution/y')", id_str.c_str() );	


		try {

			prueba_char 	= myDB->query(buffer1);
			dbResponse.assign(prueba_char);
			delete [] prueba_char;
		}
		catch (...)  {
			errorAccesoBD(5);
			continue;
		}
		XmlParser* myXMLResY;
		myXMLResY=new XmlParser(dbResponse.c_str(), dbResponse.size());
		string resy_str;

		myXMLResY->addTag("<row>");
		myXMLResY->addTag("<value>");
		count 	= myXMLResY->read();
		if (count) {
			resy_str = myXMLResY->getTag(0);
			//cout << "Resy " << resy_str << endl;		
		}
		else
			resy_str="0";
		myXMLResY->cleanTag();

		delete myXMLResY;

		sprintf (buffer1, "SELECT value from getDeviceConfigParamByPath(%s,'Video/1/Resolution/x')", id_str.c_str() );

		try {

			prueba_char 	= myDB->query(buffer1);
			dbResponse.assign(prueba_char);
			delete [] prueba_char;
		}
		catch (...)  {
			errorAccesoBD(6);
			continue;
		}

		XmlParser* myXMLResX;
		myXMLResX=new XmlParser(dbResponse.c_str(), dbResponse.size());

		string resx_str;

		myXMLResX->addTag("<row>");
		myXMLResX->addTag("<value>");
		count 	= myXMLResX->read();
		if (count) {
			resx_str = myXMLResX->getTag(0);
			//cout << "Resx " << resx_str << endl;		
		}
		else
			resx_str ="0";
		myXMLResX->cleanTag();
		delete myXMLResX;
		sprintf (buffer2, "UPDATE video SET  datebd = now() , codec = \'%s\', resx = %s , resy = %s WHERE id=%s",cdc_str.c_str(),resx_str.c_str(),resy_str.c_str(), id_str.c_str());
		//sprintf (buffer2, "UPDATE dispositivo SET date = now() WHERE id=725");
		//cout << " buffer2 " << buffer2 << endl ;


		struct timeval hora_update_inicio,hora_update_final;

		gettimeofday(&hora_update_inicio,NULL);


		try {

			XML *res 	= myDB->update(buffer2);
			delete res;
		}
		catch (...)  {
			errorAccesoBD(7);
			continue;
		}


		gettimeofday(&hora_update_final,NULL);
		if ((hora_update_final.tv_sec + (float)hora_update_final.tv_usec/1000000) - (hora_update_inicio.tv_sec + (float)hora_update_inicio.tv_usec/1000000) >= 10.0 ) {
			printf("Update 1 Final: %.3f " , (hora_update_final.tv_sec + (float)hora_update_final.tv_usec/1000000) );
			//cout << " Update 1 buffer2 " << buffer2 << endl ;
		}	

	}

	//seleccionamos el registro menos reciente de video, date mas antigua


	sprintf (buffer2, "SELECT id FROM video ORDER BY date LIMIT 1");	
	//dbResponse 	= myDB->query(buffer2);

	N_peticiones ++;

	try {

		prueba_char 	= myDB->query(buffer2);
		dbResponse.assign(prueba_char);
		delete [] prueba_char;
	}
	catch (...)  {
		errorAccesoBD(1);
		continue;
	}

	

	
	XmlParser* myXMLBBDD;
	myXMLBBDD=new XmlParser(dbResponse.c_str(), dbResponse.size());




	string id_str;

	myXMLBBDD->addTag("<row>");
	myXMLBBDD->addTag("<id>");
	count 	= myXMLBBDD->read();
	if (count) {
		id_str = myXMLBBDD->getTag(0);
		//cout << "Id " << id_str << endl;		
	}
	myXMLBBDD->cleanTag();

	delete 	myXMLBBDD;
	sprintf (buffer1, "SELECT dis.ip as ip,dis.puerto as port, dis.stream as stream ,mod.codigo as codigo FROM dispositivo dis, modelo mod WHERE dis.modelo=mod.id and dis.id=%s",id_str.c_str());	
	//cout << "Buffer1: " << buffer1 << endl;


	try {

		prueba_char 	= myDB->query(buffer1);
		dbResponse.assign(prueba_char);
		delete [] prueba_char;
	}
	catch (...)  {
		errorAccesoBD(2);
		continue;
	}
	
	// Parseamos el XML de respuesta para leer ip, modelo y stream
	XmlParser* myXML;
	myXML=new XmlParser(dbResponse.c_str(), dbResponse.size());
	

	string ip,port_cam,modelo_cam,stream_cam;
	myXML->addTag("<row>");
	myXML->addTag("<ip>");
	count 	= myXML->read();
	
	if (count) {
		ip 		= myXML->getTag(0);
		myXML->cleanTag();
		//cout << "Ip " << ip << endl;		
	}
	else {
		myXML->cleanTag();
		delete myXML;	
		cout << "ID de camara inexistente en BD dispositivos: " << id_str.c_str() << endl;
		//cout << "Se borra tambien de video: " << id_str.c_str() << endl;

		sprintf (buffer2, "DELETE from video  WHERE id=%s", id_str.c_str());



		try {

			XML *res 	= myDB->update(buffer2);

			delete res;
		}
		catch (...)  {
			errorAccesoBD(3);
			continue;
		}
	        gettimeofday(&acabado,NULL);
	        if  (((acabado.tv_sec + (float)acabado.tv_usec/1000000) - (comienzo.tv_sec + (float)comienzo.tv_usec/1000000)) > 5.0) {
		        printf("Duracion: %.3f " , (acabado.tv_sec + (float)acabado.tv_usec/1000000) - (comienzo.tv_sec + (float)comienzo.tv_usec/1000000));
		        cout <<  endl;
	        }
	        comienzo = acabado;
	        cout << "N Peticiones " << N_peticiones << endl << endl;
		continue; //No existe en la BD dispositivos
	}
	myXML->addTag("<row>");
	myXML->addTag("<codigo>");
	count_1 = myXML->read();
	if (count_1)  {
		modelo_cam = myXML->getTag(0);
		//cout << "Modelo " << modelo_cam << endl;
		
	}
	myXML->cleanTag();
	
	
	myXML->addTag("<row>");
	myXML->addTag("<stream>");
	count_2 = myXML->read();
	if (count_2)  {
		stream_cam = myXML->getTag(0);
		//cout << "Stream " << stream_cam << endl;
		
	}
	myXML->cleanTag();
	
	myXML->addTag("<row>");
	myXML->addTag("<port>");
	count_3 = myXML->read();
	if (count_3)  {
		port_cam = myXML->getTag(0);
		//cout << "Port " << port_cam << endl;
		
	}
	myXML->cleanTag();
	delete myXML;	
	


	int Indice_Parametros1 = 0;
	int Indice_Parametros2 = 0;
	int modelo_16 =0;

	int found 	= ip.find('/');
	if( found != string::npos )
	{
		ip.erase(ip.begin()+found,ip.end()); 
	}
	//cout << "ip: " << ip.c_str() <<endl;


	// Parseamos la información de la camara
	const char* pic;
	char mensaje_respuesta[3000];
	len=0;
	//cout << "Modelo " << modelo_cam << endl;
	//cout << "Stream " << stream_cam << endl;
	//cout << "Ip " << ip << endl;		
	if (modelo_cam.compare(0,8,"SNT-V704") == 0)  {
		codecparameters(ip.c_str(), port_cam.c_str(),stream_cam.c_str(), len, mensaje_respuesta);
		modelo_16=1;
	}
	else
		cameraparameters(ip.c_str(), port_cam.c_str(),  len , mensaje_respuesta);
		//cameraparameters("192.168.0.235",  len , mensaje_respuesta);
		
		
	//Sin error en la conexión
	
	
	if (len != 0)  {
			

		string content(mensaje_respuesta, len) ;


		//int Indice_Pruebas;
		//prueba para verificar el comportamiebto en caso de informacion diferente de la esperada
		//Indice_Pruebas = content.find("ImageCodec=mpeg4");
		//if (Indice_Pruebas != string::npos) {
		//		cout << "Encontrado" << endl;
		//		content.replace(Indice_Pruebas,16,"ImageCodec=jpeg-mpeg4");
		//}



	
			
		//Manipulación de información de cámara
			
		
		content_parameters="<ID>";
		content_parameters.append( id_str.c_str());
		content_parameters.append( "</ID>");
		int ImageCodec1_ausente = 0;
	
		while (Indice_Parametros1 <= content.length())
	
		{
		
			//El parámetro codec se encuentra tras la etiqueta ImageCodec1 o ImageCodec
			string Stream_Operativo;
			if ((modelo_cam.compare(0,4,"SNC-") == 0) && (modelo_cam.compare(6,1,"6") == 0) || (modelo_cam.compare(0,10,"SNC-VM772R") == 0))  {
				int nStream = atoi(stream_cam.c_str()) + 1;
				ostringstream str1;
				str1 << nStream;
				Stream_Operativo= "&ImageCodec";
				Stream_Operativo.append( (str1.str()).c_str());
			}
			else  {
				Stream_Operativo= "&ImageCodec1";
			}
			Indice_Parametros1 = content.find(Stream_Operativo.c_str(),Indice_Parametros1);
			Indice_Parametros2 = content.find('&',Indice_Parametros1 + 1);
				
			string Camera_Codec; 
		
				
		
			
		
			if (Indice_Parametros1 != string::npos) {
				//La información relevante comienza en la posición actual + 13 y acaba en el siguiente &. "&ImageCodec1=" son 13 letras
				string Camera_Codec = content.substr(Indice_Parametros1 + Stream_Operativo.length() + 1 ,(Indice_Parametros2-(Indice_Parametros1 + Stream_Operativo.length() + 1)));
				content_parameters.append("<Codec>");
				content_parameters.append( Camera_Codec.c_str());
				content_parameters.append("</Codec>");
			}
		
		
			else {
				Indice_Parametros1 = 0;
				Indice_Parametros2 = 0;
				Indice_Parametros1 = content.find("&ImageCodec",Indice_Parametros1);
				Indice_Parametros2 = content.find('&',Indice_Parametros1 + 1);
			
				if (Indice_Parametros1 != string::npos) {
					
					//La información relevante comienza en la posición actual + 12 y acaba en el siguiente &. "&ImageCodec=" son 12 letras
					Camera_Codec = content.substr(Indice_Parametros1 + 12,(Indice_Parametros2-(Indice_Parametros1 + 12)));
					content_parameters.append("<Codec>");
					content_parameters.append( Camera_Codec.c_str());
					content_parameters.append("</Codec>");
						
					//cout << "Codec: " << Camera_Codec.c_str() << endl;
						
					if (Camera_Codec.compare("h264") == 0)
						ImageCodec1_ausente = 1;
					else   {
							
						if (Camera_Codec.compare("mpeg4") == 0)
							ImageCodec1_ausente = 2;
						else {
								
							if (Camera_Codec.compare("jpeg") == 0)
								ImageCodec1_ausente = 3;
							else
								ImageCodec1_ausente = 4;
						}
					}
									
				
				}
			
				else  {
						
					if (modelo_16 == 1)  {
						//En caso de Codec modelo 25
						Indice_Parametros1 = 0;
						Indice_Parametros2 = 0;
						Indice_Parametros1 = content.find("ImageCodec",Indice_Parametros1);
						Indice_Parametros2 = content.find('&',Indice_Parametros1 + 1);
						if (Indice_Parametros1 != string::npos) {
					
							//La información relevante comienza en la posición actual + 11 y acaba en el siguiente &. "ImageCodec=" son 11 letras
							Camera_Codec = content.substr(Indice_Parametros1 + 11,(Indice_Parametros2-(Indice_Parametros1 + 11)));
								
							if (Camera_Codec.compare("0") == 0)
								content_parameters.append( "<Codec>jpeg</Codec>");
							else if (Camera_Codec.compare("1") == 0)
								content_parameters.append( "<Codec>mpeg4</Codec>");
							else {
								Indice_Parametros1 = 0;
								Indice_Parametros1 = content.find("<Error>",Indice_Parametros1);
								if (Indice_Parametros1 == string::npos)
									content_parameters = "<Error>0</Error><Description>Información incompleta</Description>" + content_parameters;
								content_parameters.append("<Codec>XXXX</Codec>");
							}
									
						}	
					}
					else {
						Indice_Parametros1 = 0;
						Indice_Parametros1 = content_parameters.find("<Error>",Indice_Parametros1);
						if (Indice_Parametros1 == string::npos)
							content_parameters = "<Error>0</Error><Description>Información incompleta</Description>" + content_parameters;
						content_parameters.append("<Codec>XXXX</Codec>");
							
					}
		
				}
			}
		
			//Para salir del bucle while
			Indice_Parametros1 = content.length() +1;
		
		}
	
	
		Indice_Parametros1 = 0;
		Indice_Parametros2=0;
		int Indice_Horizontal = 0;
		string Camera_ImageSize;
		string Camera_ImageSize_H;
		string Camera_ImageSize_V;
	
		while (Indice_Parametros1 <= content.length())
	
		{
		
			//Los parámetros H,V se encuentran tras la etiqueta ImageSize1 o si no existe esta etiqueta , tras hacer el análisis
			// de las etiquetas H264ImageSize,M4ImageSize o JpImageSize respectivamente para codecs h264, mpeg4 o jpeg.
			string Stream_Operativo;
			if ((modelo_cam.compare(0,4,"SNC-") == 0) && (modelo_cam.compare(6,1,"6") == 0) || (modelo_cam.compare(0,10,"SNC-VM772R") == 0))  {
				int nStream = atoi(stream_cam.c_str()) + 1;
				ostringstream str1;
				str1 << nStream;
				Stream_Operativo= "&ImageSize";
				Stream_Operativo.append( (str1.str()).c_str());
			}
			else  {
				Stream_Operativo= "&ImageSize1";
			}
			Indice_Parametros1 = content.find(Stream_Operativo.c_str(),Indice_Parametros1);

			Indice_Parametros2 = content.find('&',Indice_Parametros1+1);
			if (Indice_Parametros1 != string::npos) {
				//La información relevante comienza en la posición actual + 12 y acaba en el siguiente &. "&ImageSize1=" son 12 letras
				// Es de la forma XXX,YYY con los valores H y V separados por ","
				Camera_ImageSize = content.substr(Indice_Parametros1 + Stream_Operativo.length() + 1,(Indice_Parametros2-(Indice_Parametros1 + Stream_Operativo.length() + 1)));
				
				Indice_Parametros2 = Camera_ImageSize.find(",",0);
				Camera_ImageSize_H = Camera_ImageSize.substr(0,Indice_Parametros2);
				content_parameters.append("<H>");
				content_parameters.append(Camera_ImageSize_H.c_str());
				content_parameters.append("</H>");
					
				Camera_ImageSize_V=Camera_ImageSize.substr(Indice_Parametros2 + 1,Camera_ImageSize.length());
				content_parameters.append("<V>");
				content_parameters.append(Camera_ImageSize_V.c_str() );
				content_parameters.append("</V>");
			}
			else {
				Indice_Parametros1 = 0;
				Indice_Parametros2 = 0;
					
					
				if (modelo_16 == 1)  {
					//En caso de Codec modelo 25
					Indice_Parametros1 = content.find("ImageSize",Indice_Parametros1);
					Indice_Parametros2 = content.find('&',Indice_Parametros1+1);
						
					//La información relevante comienza en la posición actual + 10 y acaba en el siguiente &. "ImageSize=" son 10 letras
					// Es un índice
					Camera_ImageSize = content.substr(Indice_Parametros1 + 10,(Indice_Parametros2-(Indice_Parametros1 + 10)));
					switch (atoi(Camera_ImageSize.c_str())) {
							case 0:
							case 3:
								content_parameters.append("<H>720</H><V>576</V>");
							break;
							case 1:
							case 4:
								content_parameters.append("<H>640</H><V>480</V>");
							break;
							case 2:
								content_parameters.append("<H>320</H><V>240</V>");
							break;
							default:
								Indice_Parametros1 = 0;
								Indice_Parametros1 = content_parameters.find("<Error>",Indice_Parametros1);
								if (Indice_Parametros1 == string::npos)
									content_parameters = "<Error>0</Error><Description>Información incompleta</Description>" + content_parameters;
								content_parameters.append("<H>XXXX</H><V>XXXX</V>");
							break;	
					}
						
				}
				//ImageCodec1_ausente nos informa del codec de la cámara
				else  {
					//cout << "ImageCodec1_ausente: " << ImageCodec1_ausente << endl;
					switch (ImageCodec1_ausente)  {
						case 1:
							Indice_Parametros1 = content.find("&H264ImageSize",Indice_Parametros1);
							Indice_Parametros2 = content.find('&',Indice_Parametros1+1);
							//La información relevante comienza en la posición actual + 15 y acaba en el siguiente &. "&H264ImageSize=" son 15 letras
							Camera_ImageSize = content.substr(Indice_Parametros1 + 15,(Indice_Parametros2-(Indice_Parametros1 + 15)));
								
								
						break;
						case 2:
								Indice_Parametros1 = content.find("&M4ImageSize",Indice_Parametros1);
								Indice_Parametros2 = content.find('&',Indice_Parametros1+1);
								//La información relevante comienza en la posición actual + 13 y acaba en el siguiente &. "&M4ImageSize=" son 13 letras
								Camera_ImageSize = content.substr(Indice_Parametros1 + 13,(Indice_Parametros2-(Indice_Parametros1 + 13)));
								
								
						break;
						case 3:
								Indice_Parametros1 = content.find("&JpImageSize",Indice_Parametros1);
								Indice_Parametros2 = content.find('&',Indice_Parametros1+1);
								//La información relevante comienza en la posición actual + 13 y acaba en el siguiente &. "&JpImageSize=" son 13 letras
								Camera_ImageSize = content.substr(Indice_Parametros1 + 13,(Indice_Parametros2-(Indice_Parametros1 + 13)));
								
								
						break;
						
						default:
								Camera_ImageSize = "XXXX,";
						break;
					}
					
					
					//El valor de H en este caso es el primero de los que aparecen tras las etiquetas
					//H264ImageSize,M4ImageSize o JpImageSize respectivamente para codecs h264, mpeg4 o jpeg
					Indice_Parametros2 = Camera_ImageSize.find(",",0);
					Camera_ImageSize_H = Camera_ImageSize.substr(0,Indice_Parametros2);
					
					if (Camera_ImageSize_H.compare("XXXX") != 0) 
					
						Indice_Horizontal= atoi(Camera_ImageSize_H.c_str());
						
						
					else {
						Indice_Parametros1 = 0;
						Indice_Parametros1 = content_parameters.find("<Error>",Indice_Parametros1);
						if (Indice_Parametros1 == string::npos) 
							content_parameters = "<Error>0</Error><Description>Información incompleta</Description>" + content_parameters;
					}
					content_parameters.append("<H>");
					content_parameters.append(Camera_ImageSize_H.c_str());
					content_parameters.append("</H>");
					
					//El valor de V se obtiene a partir de la relación biunívoca ((704,576), (640,480), (384,288), (320,240), (160,120))
					
					switch (Indice_Horizontal)  {
							case 704:
								Camera_ImageSize_V= "576";
							break;
							case 640:
								Camera_ImageSize_V= "480";
							break;
							case 384:
								Camera_ImageSize_V= "288";
							break;
							case 320:
								Camera_ImageSize_V= "240";
							break;
							case 160:
								Camera_ImageSize_V= "120";
							break;
							default:
						
								Indice_Parametros1 = 0;
								Indice_Parametros1 = content_parameters.find("<Error>",Indice_Parametros1);
								if (Indice_Parametros1 == string::npos)
									content_parameters = "<Error>0</Error><Description>Información incompleta</Description>" + content_parameters;
								Camera_ImageSize_V= "XXXX";
							break;
					}
				
					content_parameters.append("<V>");
					content_parameters.append(Camera_ImageSize_V.c_str() );
					content_parameters.append("</V>");
				}
			}
			//Para salir del bucle while
			Indice_Parametros1 = content.length() +1;
		
		}
	}
	

	//Camara desconectada
	else    {
			
		content_parameters="<Error>2</Error> <Description>Camara  Desconectada</Description>";
		
		cout << "Camara desconectada Actualiza BD" << endl;	
		content_parameters.append("<ID>");
		content_parameters.append( id_str.c_str());
		content_parameters.append("</ID>");
			
		//delete pic;
			
	}
		

		
	// Preparamos cabeceras XML antes de enviar respuesta para analizar el mensaje y actualizar BD

	content_parameters ="<\?xml version=\"1.0\" encoding=\"UTF-8\" \?>  <CameraParameters>" + content_parameters;
	content_parameters.append( "</CameraParameters>");




	// Parseamos el XML de respuesta para leer id, codec, H y V
	XmlParser* myXML_Final;
	myXML_Final=new XmlParser(content_parameters.c_str(), content_parameters.size());
	
	
	string Id_final,Codec_final,H_final,V_final;
	count=0;
	count_1=0;
	count_2=0;
	count_3=0;


	myXML_Final->cleanTag();

	myXML_Final->addTag("<ID>");
	count 	= myXML_Final->read();
	
	if (count) {
		Id_final = myXML_Final->getTag(0);
		//cout << "Id final  " << Id_final << endl;		
	}
	myXML_Final->cleanTag();


	myXML_Final->addTag("<Codec>");
	count_1 	= myXML_Final->read();
	
	if (count_1) {
		
		Codec_final = myXML_Final->getTag(0); 
		if ((Codec_final.compare("h264") != 0 ) && (Codec_final.compare("mpeg4")!= 0 ) && (Codec_final.compare("jpeg")!= 0 ) && (Codec_final.compare("mp4v")!= 0 ))
			count_1 = 0;
		//cout << "Codec final  " << Codec_final << endl;		
	}
	myXML_Final->cleanTag();
	
	
	myXML_Final->addTag("<H>");
	count_2 = myXML_Final->read();
	if (count_2)  {
		H_final = myXML_Final->getTag(0);
		if (H_final.find_first_not_of("0123456789") != string::npos)
			count_2 = 0;
		//cout << "H final " << H_final << endl;
		
	}
	myXML_Final->cleanTag();
	
	
	myXML_Final->addTag("<V>");
	count_3 = myXML_Final->read();
	if (count_3)  {
		V_final = myXML_Final->getTag(0);
		if (V_final.find_first_not_of("0123456789") != string::npos)
			count_3 = 0;
		//cout << "V final " << V_final << endl;
		
	}
	myXML_Final->cleanTag();

	delete myXML_Final;

	//cout << "count: " << count << " count_1: " << count_1 <<" count_2: " << count_2 <<" count_3: " << count_3 ;

	if (count && count_1 && count_2 && count_3 ) {

		sprintf (buffer2, "UPDATE video SET codeccam = \'%s\',resxcam = %s,resycam = %s, datecam = now(), date= now() WHERE id=%s",Codec_final.c_str(),H_final.c_str(),V_final.c_str(), Id_final.c_str());
		//cout << " buffer2 " << buffer2 << endl ;	


	}
	else  {
		//cout << "content_parameters: " << content_parameters;
		sprintf (buffer2, "UPDATE video SET date= now() WHERE id=%s", Id_final.c_str());
		//cout << " buffer2 " << buffer2 << endl ;	

	}
	struct timeval hora_update_inicio,hora_update_final;


	gettimeofday(&hora_update_inicio,NULL);



	try {

			XML *res 	= myDB->update(buffer2);
			delete res;
	}
	catch (...)  {
			errorAccesoBD(8);
			continue;
	}


	gettimeofday(&hora_update_final,NULL);
	if ((hora_update_final.tv_sec + (float)hora_update_final.tv_usec/1000000) - (hora_update_inicio.tv_sec + (float)hora_update_inicio.tv_usec/1000000) >= 5.0 )  {
			printf("Update 2 Final: %.3f " , (hora_update_final.tv_sec + (float)hora_update_final.tv_usec/1000000) );
			//cout << " Update 2 buffer2 " << buffer2 << endl ;
	}	


	//cout << "res  " << (XML *res) << endl;
	//cout << "dbResponse size " << dbResponse.size() << endl;




	
	long Intervaloms = strtol(Intervaloms_str.c_str(),&pEnd,10);
	if ((Intervaloms != 0L) && (Intervaloms != LONG_MAX) && (Intervaloms != LONG_MIN))
		usleep(Intervaloms*1000);
	else
		usleep(1000000);

	gettimeofday(&acabado,NULL);
	if  (((acabado.tv_sec + (float)acabado.tv_usec/1000000) - (comienzo.tv_sec + (float)comienzo.tv_usec/1000000)) > 5.0) {
		printf("Duracion: %.3f " , (acabado.tv_sec + (float)acabado.tv_usec/1000000) - (comienzo.tv_sec + (float)comienzo.tv_usec/1000000));
		cout <<  endl;
	}
	comienzo = acabado;
	cout << "N Peticiones " << N_peticiones << endl << endl ;

	}		

	cout << "Desconexion BD" << endl;

	delete myDB;



	if (myCanis == 0)
		printf("Canis borrado 2\n");
	if (canis_inicializado != 0)
		delete myCanis;
	else
		canis_inicializado = 1;

	cout << "<------ consultayActualiza()" << endl << endl;



}


void errorAccesoBD(int param)
{
	cout << "Error en el acceso a la BD " << param << endl;
	gettimeofday(&acabado,NULL);
	if  (((acabado.tv_sec + (float)acabado.tv_usec/1000000) - (comienzo.tv_sec + (float)comienzo.tv_usec/1000000)) > 5.0) {
		                printf("Duracion: %.3f " , (acabado.tv_sec + (float)acabado.tv_usec/1000000) - (comienzo.tv_sec + (float)comienzo.tv_usec/1000000));
		                cout <<  endl;
	}
	comienzo = acabado;
	cout << "N Peticiones " << N_peticiones << endl << endl;

	//delete myDB;
	//myDB 	= new DBConnection(myCanis->getAddress(), myCanis->getType(), myCanis);

}



void sigpipe_handler(int param)
{
cout << "SIGPIPE detected" << endl;
}

